@echo off
cmake -H. -B.out -G "Visual Studio 12" -T v120_xp -DPOKER_BOT=OFF -DTEST_ALGO=ON -DREALTY=OFF
if %errorlevel% NEQ 0 (
    echo cmake exitcode = %errorlevel%
    pause
)