#include "../cachelib/ServerCache.h"
#include "rpc/Client.h"
#include "net/PipeHost.h"
#include "common/ServiceHolder.h"
#include "Config.h"
#include "cache.pb.h"
#include "common/Log.h"
#include "common/Modules.h"
#include "Hand.h"
#include "Cards.h"

#include <iostream>

#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>

DECLARE_CURRENT_MODULE(Modules::Cache);

class CacheService : public proto::CacheService
{
public:
    CacheService(ILog& logger) : m_Log(logger), m_Cache(logger)
    {
        m_Cache.Load();
    }

private:

    virtual void GetPreflopEquity(const rpc::Request<::proto::EquityCache_Request>::Ptr& request, const rpc::Response<::proto::EquityCache_Response>::Ptr& response) override
    {
        response->set_value(m_Cache.GetPreflopEquity(static_cast<pcmn::Hand::Value>(request->bot().hand()), ExtractHands(*request)));
    }
    virtual void GetFlopEquity(const rpc::Request<::proto::EquityCache_Request>::Ptr& request, const rpc::Response<::proto::EquityCache_Response>::Ptr& response) override
    {
        response->set_value(m_Cache.GetFlopEquity(static_cast<pcmn::Hand::Value>(request->bot().hand()), ExtractHands(*request)));
    }
    virtual void GetTurnEquity(const rpc::Request<::proto::EquityCache_Request>::Ptr& request, const rpc::Response<::proto::EquityCache_Response>::Ptr& response) override
    {
        response->set_value(m_Cache.GetTurnEquity(static_cast<pcmn::Hand::Value>(request->bot().hand()), ExtractHands(*request)));
    }
    virtual void GetParsedHand(const rpc::Request<::proto::HandsCache_Request>::Ptr& request, const rpc::Response<::proto::HandsCache_Response>::Ptr& response) override
    {
        srv::cache::Hands key;
        for (const auto card :request->data().cards())
            key.m_Cards[key.m_Size++] = static_cast<unsigned char>(card);

        response->set_value(m_Cache.GetParsedHand(key));
    }
    
    pcmn::Hand::List ExtractHands(const ::proto::EquityCache::Request& request)
    {
        pcmn::Hand::List opponents;
        opponents.reserve(request.opponents_size());
        for (const auto& opp : request.opponents())
            opponents.push_back(static_cast<pcmn::Hand::Value>(opp.hand()));

        return opponents;
    }

private:
    ILog& m_Log;
    srv::ServerCache m_Cache;
};

int main(int argc, char* argv[])
{
	try 
	{
        cmn::Service::Holder app(1);

        // logger
        Log log;
        log.Open(ILog::Level::Trace);

        // init server
        const auto host = boost::make_shared<net::pipes::Transport>(log); 

        // init request processor
        CacheService service(log);

        // init rpc channel
        const auto channel = rpc::IClient::Instance(log, *host, cfg::CACHE_MEMORY_NAME, "cache client");

        // provide this service
        channel->ProvideService(service);

        // connect to server
        channel->Connect();

        // run
        cmn::Service::Instance().run();
	}
	catch (const std::exception& e)
	{
		std::wcout << cmn::ExceptionInfo(e) << std::endl;
	}

	return 0;
}

