#include "Injector.h"
#include <windows.h>
#include <TlHelp32.h>
#include <sstream>
#include <assert.h>

#include "common/Processes.h"
#include "exception/CheckHelpers.h"

#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>

namespace dll
{

//��������� ��������� ����, � ������� ���������� ��� ���������
struct InjectCode
{
	BYTE  instr_push_loadlibrary_arg; //���������� push
	DWORD loadlibrary_arg;            //�������� push  

	WORD  instr_call_loadlibrary;     //���������� call []  
	DWORD adr_from_call_loadlibrary;

	BYTE  instr_push_exitthread_arg;
	DWORD exitthread_arg;

	WORD  instr_call_exitthread;
	DWORD adr_from_call_exitthread;

	DWORD addr_loadlibrary;
	DWORD addr_exitthread;     //����� ������� ExitTHread
	char  libraryname[100];    //��� � ���� � ����������� ����������  
};

Injector::Injector(std::size_t id) : m_ProcessId(id)
{
}

Injector::Injector(const std::wstring& name) : m_ProcessId(0)
{
    const auto snap = cmn::Process::GetAll();
    const auto it = boost::find_if(snap, boost::bind(&cmn::Process::GetName, _1) == name);
    CHECK(it != snap.end(), cmn::Exception("Unable to find process by name"), name);
    m_ProcessId = it->GetId();
}


Injector::~Injector()
{
}


void Injector::Inject(const std::string& path)
{
	DWORD   dwMemSize;
	HANDLE  hProc;
	LPVOID  lpRemoteMem, lpLoadLibrary;

	if ((hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_ProcessId)) == NULL)
	{
		std::ostringstream oss;
		oss << "Failed to open process, last error: " << GetLastError();
		throw std::runtime_error(oss.str().c_str());
	}
	
	dwMemSize = strlen(path.c_str());
	if ((lpRemoteMem = VirtualAllocEx(hProc, NULL, dwMemSize, MEM_COMMIT, PAGE_READWRITE)) == NULL)
	{
		std::ostringstream oss;
		oss << "Failed to VirtualAllocEx, last error: " << GetLastError();
		throw std::runtime_error(oss.str().c_str());
	}

	if (WriteProcessMemory(hProc, lpRemoteMem, path.c_str(), dwMemSize, NULL))
	{
		lpLoadLibrary = GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
		if (CreateRemoteThread(hProc, NULL, 0, (LPTHREAD_START_ROUTINE)lpLoadLibrary, lpRemoteMem, 0, NULL) == NULL)
		{
			std::ostringstream oss;
			oss << "Failed to create thread, last error: " << GetLastError();
			throw std::runtime_error(oss.str().c_str());
		}
	}
	
	CloseHandle(hProc);

	
// 
// 	if (path.size() > 100)
// 		throw std::invalid_argument("Path too long");
// 
// 	// ������� ������� � ������ ��������
// 	HANDLE hToken; 
// 	TOKEN_PRIVILEGES tp; 
// 	const HANDLE process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_ProcessId); 
// 
// 	tp.PrivilegeCount = 1; 
// 	assert(LookupPrivilegeValueA( NULL, "SeDebugPrivilege", &tp.Privileges[0].Luid )); 
// 	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
// 	assert(OpenProcessToken(process, TOKEN_ADJUST_PRIVILEGES, &hToken)); 
// 
// 	assert(AdjustTokenPrivileges(hToken, FALSE, &tp, NULL, NULL, NULL)); 
// 	CloseHandle(hToken);
// 
// 	if(!process)
// 		throw std::runtime_error("You have not enough rights to attach dlls");
//   
// 	//��������������� ������ � ��������
// 	BYTE* processMemory = reinterpret_cast<BYTE*>(VirtualAllocEx
// 	(
// 		process, 
// 		0, 
// 		sizeof(InjectCode),
// 		MEM_COMMIT, 
// 		PAGE_EXECUTE_READWRITE
// 	));
// 
// 	if (!processMemory)
// 		throw std::runtime_error("Unable to alloc memory in remote process");
// 
// 	//����������������  �������� ���
// 	InjectCode cmds;
// 	cmds.instr_push_loadlibrary_arg = 0x68; //�������� ��� ���������� push
// 	cmds.loadlibrary_arg = (DWORD)((BYTE*)processMemory + offsetof(InjectCode, libraryname));
//   
// 	cmds.instr_call_loadlibrary = 0x15ff; //�������� ��� ���������� call
// 	cmds.adr_from_call_loadlibrary = (DWORD)(processMemory + offsetof(InjectCode, addr_loadlibrary));
//   
// 	cmds.instr_push_exitthread_arg  = 0x68;
// 	cmds.exitthread_arg = 0;
//   
// 	cmds.instr_call_exitthread = 0x15ff; 
// 	cmds.adr_from_call_exitthread = (DWORD)(processMemory + offsetof(InjectCode, addr_exitthread));
//   
// 	cmds.addr_loadlibrary = (DWORD)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
// 	cmds.addr_exitthread  = (DWORD)GetProcAddress(GetModuleHandleA("kernel32.dll"),"ExitThread");
//   
// 	strcpy_s(cmds.libraryname, path.c_str());
//   
// 	/*����� ������������� cmds � ��������� ���������� �������� ���������
// 	�������:
// 		push  adr_library_name               ;�������� �-��� loadlibrary
// 		call dword ptr [loadlibrary_adr]     ; ������� LoadLibrary 
// 		push exit_thread_arg                 ;�������� ��� ExitThread
// 		call dword ptr [exit_thread_adr]     ;������� ExitThread     
// 	*/
//   
// 	//�������� �������� ��� �� ������������������ ������
// 	DWORD wr = 0;
// 	assert(WriteProcessMemory(process, processMemory, &cmds, sizeof(cmds), &wr));
// 	assert(wr == sizeof(cmds));
//   
// 	//��������� �������� ���
// 	DWORD threadId = 0;
// 	HANDLE threadHandle = CreateRemoteThread
// 	(
// 		process, 
// 		NULL, 
// 		0, 
// 		(unsigned long (__stdcall *)(void *))processMemory, 
// 		0, 
// 		0, 
// 		&threadId
// 	);
// 
// 	if (!threadId)
// 	{
// 		std::ostringstream oss;
// 		oss << "Failed to create thread, last error: " << GetLastError();
// 		throw std::runtime_error(oss.str().c_str());
// 	}
// 
// 	//������� ���������� ���������� ������
// 	WaitForSingleObject(threadHandle, INFINITE);
// 
// 	//���������� ������
// 	VirtualFreeEx(process, (void*)processMemory, sizeof(cmds), MEM_RELEASE);
}

} // namespace dll