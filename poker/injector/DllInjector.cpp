// DllInjector.cpp : Defines the entry point for the console application.
//
#include "exception/Exception.h"
#include "Injector.h"
#include "conversion/AnyCast.h"

#include <iostream>

int main(int argc, wchar_t* argv[])
{
	try 
	{
		if (argc < 3)
			return 1;

		dll::Injector injector(argv[1]);

		injector.Inject(conv::cast<std::string>(std::wstring(argv[2])));
	}
	catch (const std::exception& e)
	{
		std::wcout << cmn::ExceptionInfo(e) << std::endl;
	}

	return 0;
}

