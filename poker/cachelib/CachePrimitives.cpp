#include "CachePrimitives.h"
#include "Config.h"

#include <boost/format.hpp>
#include <boost/make_shared.hpp>

namespace srv
{
namespace cache
{

enum { MAX_OPPONENTS_COUNT = 3 };

Game::Game()
{
}

Game::Game(const pcmn::Hand::Value bot, const pcmn::Hand::List& opponents, unsigned flopCards)
    : m_Bot(bot)
    , m_Opponents(opponents)
{
    Init(flopCards);
}

Game::Game(const pcmn::Hand::Value bot, pcmn::Hand::List&& opponents, unsigned flopCards)
    : m_Bot(bot)
    , m_Opponents(std::move(opponents))
{
    Init(flopCards);
}

bool Game::operator==(const Game& other) const
{
    return 
        m_Bot == other.m_Bot
        &&
        m_Opponents == other.m_Opponents;
}

void Game::Init(unsigned flopCards)
{
    std::sort(m_Opponents.begin(), m_Opponents.end());

    // remove lowest hands
    if (m_Opponents.size() > MAX_OPPONENTS_COUNT)
        m_Opponents.resize(MAX_OPPONENTS_COUNT);

    if (!flopCards)
        return;

    static const auto func = [](pcmn::Hand::Value& h) { h = static_cast<pcmn::Hand::Value>(h & (pcmn::Hand::FLOP_HAND_MASK | pcmn::Hand::POWER_MASK)); };
    func(m_Bot);
    std::for_each(m_Opponents.begin(), m_Opponents.end(), func);
}

std::size_t Game::operator()(const Game& key) const
{
    std::size_t seed = 0;
    boost::hash_combine(seed, key.m_Bot);
    boost::hash_range(seed, key.m_Opponents.begin(), key.m_Opponents.end());
    return seed;
}

std::size_t HandHasher::operator()(const Hands& h) const
{
    return boost::hash_range(h.m_Cards, h.m_Cards + h.m_Size);
}

bool HandComparer::operator()(const Hands& lhs, const Hands& rhs) const
{
    return lhs.m_Size == rhs.m_Size && !memcmp(lhs.m_Cards, rhs.m_Cards, lhs.m_Size);
}

Hands::Hands() : m_Size()
{
    memset(&m_Cards, 0, _countof(m_Cards));
}

Hands::Hands(const pcmn::Card::List& bot, const pcmn::Card::List& board) : m_Size(bot.size() + board.size())
{
    const unsigned char first = static_cast<unsigned char>(bot.front().ToEvalFormat());
    const unsigned char second = static_cast<unsigned char>(bot.back().ToEvalFormat());
    m_Cards[0] = std::min(first, second);
    m_Cards[1] = std::max(first, second);

    for (unsigned i = 2; i < board.size() + 2; ++i)
        m_Cards[i] = static_cast<unsigned char>(board[i - 2].ToEvalFormat());

    std::sort(&m_Cards[2], &m_Cards[2] + (m_Size - 2)); 
}

bool Hands::IsDuplicated() const
{
    for (unsigned i = 0; i < m_Size; ++i)
    {
        for (unsigned j = 0; j < m_Size; ++j)
        {
            if (i == j)
                continue;

            if (m_Cards[i] == m_Cards[j])
                return true;
        }
    }
    return false;
}

} // namespace cache
} // namespace srv