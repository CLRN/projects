#include "ClientCache.h"
#include "common/ServiceHolder.h"
#include "common/Modules.h"
#include "Config.h"
#include "Hand.h"
#include "Evaluator.h"
#include "CardsErrorInfo.h"
#include "ServerCache.h"

#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/asio/placeholders.hpp>

namespace srv
{

DECLARE_CURRENT_MODULE(Modules::Cache);

class ClientCache::Impl
{
public:

    Impl(ILog& log) 
        : m_Log(log)
        , m_Cache(log)
    {
        m_Cache.Load();
    }

    ~Impl()
    {
    }


    float GetEquity(const pcmn::Card::List& cards, const pcmn::Card::List& board, const pcmn::Hand::List& opponents)
    {
        assert(cards.size() == 2);

        if (board.size() == 5)
        {
            // all cards known, make comparison
            return pcmn::Evaluator::Instance().GetEquity(cards, board, opponents);
        }

        const auto hand = GetParsedHand(cards, board);

        // get result
        switch (board.size())
        {
        case 0:
            return m_Cache.GetPreflopEquity(hand, opponents);
            break;
        case 3:
            return m_Cache.GetFlopEquity(hand, opponents);
            break;
        case 4:
            return m_Cache.GetTurnEquity(hand, opponents);
            break;
        default: THROW(cmn::Exception("Invalid flop size") << pcmn::CardsErrorInfo(board));
        }
        return 0;
    }

    pcmn::Hand::Value GetParsedHand(const pcmn::Card::List& cards, const pcmn::Card::List& board)
    {
        return pcmn::Hand().Parse(cards, board).GetValue();
        /*
        static cache::Memory memory(cache::Memory(boost::interprocess::open_only, cfg::CACHE_MEMORY_NAME));
        static cache::HandMap& map(*memory.find<cache::HandMap>(cfg::CACHE_MEMORY_NAME).first);

        cache::HandMap::key_type key(cards, board);

        const auto it = map.find(key);
        CHECK(it != map.end(), "failed to find cached hand", cards, board);

        return it->second;*/
    }

 

private:
    ILog& m_Log;
    ServerCache m_Cache;
};

float ClientCache::GetEquity(const pcmn::Card::List& cards, const pcmn::Card::List& board, const pcmn::Hand::List& opponents)
{
    return s_Impl->GetEquity(cards, board, opponents);
}

pcmn::Hand::Value ClientCache::GetParsedHand(const pcmn::Card::List& cards, const pcmn::Card::List& board)
{
    return s_Impl->GetParsedHand(cards, board);
}

void ClientCache::Create(ILog& log)
{
    assert(!s_Impl);
    s_Impl.reset(new Impl(log));
}

void ClientCache::Shutdown()
{
    s_Impl.reset();
}

std::unique_ptr<ClientCache::Impl> ClientCache::s_Impl;

} // namespace srv