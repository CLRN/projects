#include "../cachelib/ClientCache.h"
#include "../cachelib/ServerCache.h"
#include "common/Log.h"
#include "Config.h"
#include "Cards.h"
#include "Hand.h"
#include "Evaluator.h"
#include "Board.h"
#include "common/Modules.h"

#include <gtest/gtest.h>

#include <boost/assign.hpp>
#include <boost/chrono.hpp>

pcmn::Card::List GetRandomCards(bool* dead, unsigned count)
{
    pcmn::Card::List result;
    for (unsigned i = 0 ; i < count; ++i)
        result.push_back(pcmn::Card().FromEvalFormat(pcmn::Evaluator::Instance().GetRandomCard(dead)));
    return result;
}

bool g_DeadCards[cfg::CARD_DECK_SIZE] = {};
const pcmn::Card::List g_BotCards = GetRandomCards(g_DeadCards, 2);
const pcmn::Card::List g_FlopCards = GetRandomCards(g_DeadCards, 4);

void TestRandomCardsEvaluation(unsigned flopCardCnt, unsigned players, unsigned iterations)
{
    pcmn::Board board;

    bool deadCards[cfg::CARD_DECK_SIZE] = {};

    pcmn::Hand parser;
    unsigned success = 0;
    unsigned fails = 0;
    for (unsigned i = 0 ; i < iterations; ++i)
    {
        // get random cards for bot
        const pcmn::Card::List botCards = GetRandomCards(deadCards, 2);

        // get random cards for flop
        const pcmn::Card::List flopCards = GetRandomCards(deadCards, flopCardCnt);

        // get random cards for each player
        pcmn::Hand::List opponents;
        std::vector<pcmn::Card::List> temp;
        for (unsigned opp = 0 ; opp < players; ++opp)
        {
            const pcmn::Card::List cards = GetRandomCards(deadCards, 2);
            opponents.push_back(parser.Parse(cards, flopCards).GetValue());
            temp.push_back(cards);
        }

        memset(deadCards, 0, _countof(deadCards));

        // cache value
        const float cached = srv::ClientCache::GetEquity(botCards, flopCards, opponents);

        // more accurate value
        const float equity = pcmn::Evaluator::Instance().GetEquity(botCards, flopCards, temp);

        if (abs(equity - cached) < 15.0f)
            ++success;
        else
        {
            // get bot hand description
            const pcmn::Hand::Value hand = parser.Parse(botCards, flopCards).GetValue();

            // cache value
            const float c = srv::ClientCache::GetEquity(botCards, flopCards, opponents);

            // more accurate value
            const float e = pcmn::Evaluator::Instance().GetEquity(botCards, flopCards, temp);

            ++fails;
        }
    }

    EXPECT_GE(success, fails);
}

TEST(CacheCards, RandomGeneration)
{
    Log log;
    srv::ClientCache::Create(log);

    TestRandomCardsEvaluation(0, 1, 0);
    TestRandomCardsEvaluation(3, 1, 100);
    TestRandomCardsEvaluation(4, 1, 100);
    TestRandomCardsEvaluation(5, 1, 100);

    srv::ClientCache::Shutdown();
}

class SpeedTester
{
public:

    template<typename T>
    SpeedTester(const T func, unsigned iterations = 1000000) 
        : m_Iterations(iterations)
        , m_Start(boost::chrono::high_resolution_clock::now())
    {
        for (unsigned i = 0 ; i < m_Iterations; ++i)
            func();
    }
     
    ~SpeedTester()
    {
        const boost::chrono::high_resolution_clock::time_point end = boost::chrono::high_resolution_clock::now();
        const auto total = boost::chrono::duration_cast<boost::chrono::milliseconds>(end - m_Start);
        const auto avg = boost::chrono::duration_cast<boost::chrono::microseconds>(end - m_Start);
        std::cout << "total ms: " << total << ", average mks: " << static_cast<float>(avg.count()) / m_Iterations << std::endl;
    }

private:
    unsigned m_Iterations;
    boost::chrono::high_resolution_clock::time_point m_Start;
};

/*
TEST(CacheHands, LocalCache)
{
    Log log;
    log.Open("1", Modules::Cache, ILog::Level::Trace);

    srv::ServerCache cache(log);
    cache.Load();

    const auto func = [&cache]()
    {
        std::vector<unsigned char> cards;
        cards.reserve(g_BotCards.size() + g_FlopCards.size());
        cards.push_back(static_cast<unsigned char>(g_BotCards.front().ToEvalFormat()));
        cards.push_back(static_cast<unsigned char>(g_BotCards.back().ToEvalFormat()));

        for (const auto& card : g_FlopCards)
            cards.push_back(static_cast<unsigned char>(card.ToEvalFormat()));

        cache.GetParsedHand(cards);
    };

    SpeedTester tester(func, 1000000 * 100);
}*/
/*
TEST(CacheHands, Parse)
{
    const auto func = []()
    {
        pcmn::Hand().Parse(g_BotCards, g_FlopCards).GetValue();
    };

    SpeedTester tester(func);
}*/
/*

TEST(CacheHands, RemoteCache)
{
    Log log;
    srv::ClientCache::Create(log);

    const auto func = []()
    {
        srv::ClientCache::GetParsedHand(g_BotCards, g_FlopCards);
    };

    {
        SpeedTester tester(func);
    }
    srv::ClientCache::Shutdown();
}

void TestRandomCardsParse()
{
    Log log;
    srv::ClientCache::Create(log);

    // generate random cards, compare evaluated results with cached values
    for (unsigned i = 0; i < 10000; ++i)
    {
        bool deadCards[cfg::CARD_DECK_SIZE] = {};
        const pcmn::Card::List botCards = GetRandomCards(deadCards, 2);
        const pcmn::Card::List flopCards = GetRandomCards(deadCards, 3);

        const pcmn::Hand::Value parsed = pcmn::Hand().Parse(botCards, flopCards).GetValue();
        const pcmn::Hand::Value cached = srv::ClientCache::GetParsedHand(botCards, flopCards);

        EXPECT_EQ(parsed, cached) << "parsed: " << parsed << ", cached: " << cached;

    }

    srv::ClientCache::Shutdown();
}

TEST(CacheHands, Random)
{
    TestRandomCardsParse();
}*/