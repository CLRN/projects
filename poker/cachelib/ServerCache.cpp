#include "ServerCache.h"
#include "Config.h"
#include "cache.pb.h"
#include "exception/CheckHelpers.h"
#include "Hand.h"
#include "common/Modules.h"
#include "CachePrimitives.h"
#include "exception/ErrorCodeInfo.h"

#include <fstream>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>

#include <boost/unordered_map.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/make_shared.hpp>

namespace srv
{

DECLARE_CURRENT_MODULE(Modules::Cache);

class ServerCache::Impl
{
public:

    //! Flop cache type
    typedef srv::cache::GameMap CacheMap;

    Impl(ILog& logger) 
        : m_Log(logger)
        , m_Preflop()
        , m_Flop()
        , m_Turn()
    {

    }

    void LoadFileToCache(const proto::EquityCache& file, CacheMap& cache, unsigned flopCards)
    {
        const unsigned onePercent = file.data_size() / 100;
        unsigned counter = 0;
        for (const auto& flop : file.data())
        {
            if (cache.empty() || counter++ % onePercent == 0)
                LOG_TRACE("Loading cache [%s]... %s completed") % flopCards % ((counter * 100) / file.data_size());

            const proto::EquityCache::BotData& bot = flop.bot();

            pcmn::Hand::List opponents;
            opponents.reserve(flop.opponents_size());

            for (const auto& opponent : flop.opponents())
                opponents.push_back(static_cast<pcmn::Hand::Value>(opponent.hand()));

            cache::Game game(static_cast<pcmn::Hand::Value>(bot.hand()), std::move(opponents), flopCards);
            CHECK(cache.insert(std::make_pair(std::move(game), flop.equity())).second, cmn::Exception("Failed to insert game"), flop.ShortDebugString());
        }
    }

    void LoadFileToCache(const proto::HandsCache& file, cache::HandMap& cache)
    {
        const unsigned onePercent = file.hands_size() / 100;
        unsigned counter = 0;
        for (const auto& hand : file.hands())
        {
            try 
            {
	            if (cache.empty() || counter++ % onePercent == 0)
	                LOG_TRACE("Loading hands [%s]... %s completed") % hand.data().cards_size() % ((counter * 100) / file.hands_size());
	
	            srv::cache::HandMap::key_type cards;
	            for (const auto card : hand.data().cards())
	                cards.m_Cards[cards.m_Size++] = static_cast<unsigned char>(card);
	
	            CHECK(cache.insert(std::make_pair(std::move(cards), static_cast<pcmn::Hand::Value>(hand.result()))).second, cmn::Exception("Failed to insert game"), hand.ShortDebugString());
            }
            CATCH_PASS(cmn::Exception("Failed to add cards to cache"), hand.data().ShortDebugString())
        }
    }

    void Load()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        LOG_TRACE("Loading cache files from %s ...") % boost::filesystem::system_complete(cfg::CACHE_PREFLOP_FILE_NAME).branch_path().string();

        {
            proto::EquityCache cache;
            LoadFile(cache, cfg::CACHE_PREFLOP_FILE_NAME);
            LoadFileToCache(cache, m_Preflop, 0);
        }

        {
            proto::EquityCache cache;
            LoadFile(cache, cfg::CACHE_FLOP_FILE_NAME);
            LoadFileToCache(cache, m_Flop, 3);
        }

        {
            proto::EquityCache cache;
            LoadFile(cache, cfg::CACHE_TURN_FILE_NAME);
            LoadFileToCache(cache, m_Turn, 4);
        }

        {
            for (unsigned i = 0; ; ++i)
            {
                const std::string name = (boost::format(cfg::CACHE_HANDS_FILE_NAME) % i).str();
                if (!boost::filesystem::exists(name))
                    break;

                proto::HandsCache cache;
                LoadFile(cache, name.c_str());
                LoadFileToCache(cache, m_HandCache);
            }
        }

        LOG_TRACE("Cache loaded, total games: preflop: %s, flop: %s, turn: %s, hands: %s") 
            % m_Preflop.size() 
            % m_Flop.size() 
            % m_Turn.size()
            % m_HandCache.size();
    }

    void SaveCacheToFile(const cache::GameMap& cache, proto::EquityCache& file)
    {
        for (const auto& gameAndEquity : cache)
        {
            proto::EquityCache::GameData& flop = *file.add_data();

            flop.mutable_bot()->set_hand(gameAndEquity.first.m_Bot);
            for (const auto h : gameAndEquity.first.m_Opponents)
            {
                proto::EquityCache::OpponentData& opp = *flop.add_opponents();
                opp.set_hand(h);
            }

            flop.set_equity(gameAndEquity.second);          
        }
    }

    void SaveCacheToFile(cache::HandMap& cache, const char* fileName)
    {
        unsigned counter = 0;
        unsigned fileNumber = 0;
        proto::HandsCache protoFile;

        for (auto it = cache.begin(), end = cache.end(); it != end; it = cache.erase(it), ++counter)
        {
            auto& hand = *protoFile.add_hands();

            for (unsigned i = 0 ; i < it->first.m_Size; ++i)
            {
                const auto card = it->first.m_Cards[i];
                hand.mutable_data()->add_cards(card);
            }
            hand.set_result(it->second);

            // flush
            if (counter == 2000000)
            {
                counter = 0;

                const std::string name = (boost::format(fileName) % fileNumber++).str();

                Save2File(protoFile, name.c_str());
                proto::HandsCache newFile;
                protoFile.Swap(&newFile);
            }
        }      

        const std::string name = (boost::format(fileName) % fileNumber++).str();
        Save2File(protoFile, name.c_str());
    }

    void Save()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        LOG_TRACE("Saving cache");

        {
            proto::EquityCache cache;
            SaveCacheToFile(m_Preflop, cache);
            Save2File(cache, cfg::CACHE_PREFLOP_FILE_NAME);
            m_Preflop.clear();
        }

        {
            proto::EquityCache cache;
            SaveCacheToFile(m_Flop, cache);
            Save2File(cache, cfg::CACHE_FLOP_FILE_NAME);
            m_Flop.clear();
        }

        {
            proto::EquityCache cache;
            SaveCacheToFile(m_Turn, cache);
            Save2File(cache, cfg::CACHE_TURN_FILE_NAME);
            m_Turn.clear();
        }

        {
            SaveCacheToFile(m_HandCache, cfg::CACHE_HANDS_FILE_NAME);
        }

        LOG_TRACE("Cache saved");
    }

    template<typename T>
    void LoadFile(T& cache, const char* name)
    {
        std::ifstream ifs(name, std::ios::binary);
        CHECK(ifs.is_open(), cmn::Exception("Failed to open file"), name);

        google::protobuf::io::IstreamInputStream stream(&ifs);
        google::protobuf::io::CodedInputStream coded(&stream);
        coded.SetTotalBytesLimit(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());

        CHECK(cache.ParseFromCodedStream(&coded), cmn::Exception("Failed to parse file"), name);
    }

    template<typename T>
    void Save2File(const T& cache, const char* name)
    {
        const std::string tempFile = (boost::filesystem::temp_directory_path() / name).string();

        {
            std::ofstream ofs(tempFile.c_str(), std::ios::binary);
            CHECK(ofs.is_open(), cmn::Exception("Failed to save file"), name);
            CHECK(cache.SerializeToOstream(&ofs), cmn::Exception("Failed to serialize to file"), name);
        }

        boost::system::error_code e;
        boost::filesystem::rename(tempFile, name, e);
        CHECK(!e, cmn::Exception("Failed to rename file") << cmn::ErrorCodeInfo(e), tempFile, name);
    }

    float GetPreflopEquity(const pcmn::Hand::Value bot, const pcmn::Hand::List& opponents)
    {
        const cache::Game game(bot, opponents, 0);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        const CacheMap::const_iterator it = m_Preflop.find(game);
        return it != m_Preflop.end() ? it->second : cfg::INVALID_PERCENT;
    }

    void SetPreflopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents, float eq)
    {
        const cache::Game game(bot, opponents, 0);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_Preflop[game] = eq;
    }

    float GetFlopEquity(const pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) 
    {
        const cache::Game game(bot, opponents, 3);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        const CacheMap::const_iterator it = m_Flop.find(game);
        return it != m_Flop.end() ? it->second : cfg::INVALID_PERCENT;
    }

    void SetFlopEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq)
    {
        const cache::Game game(bot, opponents, 3);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_Flop[game] = eq;
    }

    float GetTurnEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) 
    {
        const cache::Game game(bot, opponents, 4);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        const CacheMap::const_iterator it = m_Turn.find(game);
        return it != m_Turn.end() ? it->second : cfg::INVALID_PERCENT;
    }

    void SetTurnEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq)
    {
        const cache::Game game(bot, opponents, 4);
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_Turn[game] = eq;
    }

    pcmn::Hand::Value GetParsedHand(const cache::Hands& cards)
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        cache::HandMap::iterator it = m_HandCache.find(cards);
        if (it == m_HandCache.end())
        {
            lock.unlock();

            pcmn::Card::List botCards(2);
            botCards.front().FromEvalFormat(cards.m_Cards[0]);
            botCards.back().FromEvalFormat(cards.m_Cards[1]);

            pcmn::Card::List flop(cards.m_Size - 2);
            for (int i = 0 ; i < cards.m_Size - 2; ++i)
                flop[i].FromEvalFormat(cards.m_Cards[i + 2]);
            
            const auto value = pcmn::Hand().Parse(botCards, flop).GetValue();
            lock.lock();
            it = m_HandCache.insert(std::make_pair(cards, value)).first;
        }

        return it->second;
    }

private:

    ILog& m_Log;
    cache::GameMap m_Preflop;               //!< preflop cache data
    cache::GameMap m_Flop;                  //!< flop cache
    cache::GameMap m_Turn;                  //!< turn cache
    cache::HandMap m_HandCache;             //!< hands cache
    boost::mutex m_Mutex;                   //!< cache mutex
};

ServerCache::ServerCache(ILog& logger) : m_Impl(new Impl(logger))
{

}

ServerCache::~ServerCache()
{
    delete m_Impl;
}

void ServerCache::Load()
{
    m_Impl->Load();
}

void ServerCache::Save()
{
    m_Impl->Save();
}

float ServerCache::GetPreflopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const
{
    return m_Impl->GetPreflopEquity(bot, opponents);
}

void ServerCache::SetPreflopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents, float eq)
{
    m_Impl->SetPreflopEquity(bot, opponents, eq);
}

float ServerCache::GetFlopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const
{
    return m_Impl->GetFlopEquity(bot, opponents);
}

void ServerCache::SetFlopEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq)
{
    m_Impl->SetFlopEquity(bot, opponents, eq);
}

float ServerCache::GetTurnEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const
{
    return m_Impl->GetTurnEquity(bot, opponents);
}

void ServerCache::SetTurnEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq)
{
    m_Impl->SetTurnEquity(bot, opponents, eq);
}

pcmn::Hand::Value ServerCache::GetParsedHand(const cache::Hands& cards)
{
    return m_Impl->GetParsedHand(cards); 
}


} // namespace srv