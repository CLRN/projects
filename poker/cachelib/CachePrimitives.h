#ifndef CachePrimitives_h__
#define CachePrimitives_h__

#include "Hand.h"

#include <utility>

#include <boost/shared_ptr.hpp>
#include <boost/unordered/unordered_map.hpp>

namespace srv
{
namespace cache
{

// Game descriptor
struct Game
{
    Game();
    Game(const pcmn::Hand::Value bot, const pcmn::Hand::List& opponents, unsigned flopCards);
    Game(const pcmn::Hand::Value bot, pcmn::Hand::List&& opponents, unsigned flopCards);
    bool operator == (const Game& other) const;
    void Init(unsigned flopCards);
    std::size_t operator()(const Game& key) const;

    pcmn::Hand::Value m_Bot;
    pcmn::Hand::List m_Opponents;
};

struct Hands
{
    Hands();
    Hands(const pcmn::Card::List& bot, const pcmn::Card::List& board);
    bool IsDuplicated() const;
    unsigned char m_Cards[6];
    unsigned char m_Size;
};

// Hands hasher
struct HandHasher
{
    std::size_t operator ()(const Hands& h) const;
};

// Hands comparer
struct HandComparer
{
    bool operator() (const Hands& lhs, const Hands& rhs) const;
};

typedef boost::unordered::unordered_map<Hands, pcmn::Hand::Value, HandHasher, HandComparer> HandMap;
typedef boost::unordered::unordered_map<Game, float, Game> GameMap;
} // namespace cache
} // namespace srv
#endif // CachePrimitives_h__
