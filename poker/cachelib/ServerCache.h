#ifndef Cache_h__
#define Cache_h__

#include "Cards.h"
#include "Hand.h"
#include "common/ILog.h"
#include "CachePrimitives.h"

#include <memory>

namespace srv
{

class ServerCache
{
public:

    ServerCache(ILog& logger);
    ~ServerCache();

    //! Load cache from hard drive
    void Load();

    //! Save cache to hard drive
    void Save();

    //! Get preflop equity from cache
    float GetPreflopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const;

    //! Set preflop equity to cache
    void SetPreflopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents, float eq);

    //! Get flop equity from cache
    float GetFlopEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const;

    //! Set flop equity to cache
    void SetFlopEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq);
    
    //! Get turn equity from cache
    float GetTurnEquity(pcmn::Hand::Value bot, const pcmn::Hand::List& opponents) const;

    //! Set turn equity to cache
    void SetTurnEquity(const pcmn::Hand::Value& bot, const pcmn::Hand::List& opponents, float eq);

    //! Get parsed hand
    pcmn::Hand::Value GetParsedHand(const cache::Hands& cards);

private:

    //! Implementation
    class Impl;
    Impl* m_Impl;
};


} // namespace srv

#endif // Cache_h__

