#ifndef ClientCache_h__
#define ClientCache_h__

#include "common/ILog.h"
#include "Cards.h"
#include "Hand.h"

#include <memory>

#include <boost/noncopyable.hpp>

namespace srv
{

class ClientCache : boost::noncopyable
{
    ClientCache();
public:

    static void Create(ILog& log);
    static void Shutdown();

    static float GetEquity(const pcmn::Card::List& cards, const pcmn::Card::List& board, const pcmn::Hand::List& opponents);
    static pcmn::Hand::Value GetParsedHand(const pcmn::Card::List& cards, const pcmn::Card::List& board);

private:

    //! Implementation
    class Impl;
    static std::unique_ptr<Impl> s_Impl;
};

} // namespace srv

#endif // ClientCache_h__
