#include "exception/CheckHelpers.h"
#include "common/Log.h"
#include "common/Modules.h"
#include "common/Window.h"
#include "recognition/Recognition.h"
#include "xml/Node.h"
#include "common/GUID.h"
#include "common/AsioHelpers.h"
#include "common/FileSystem.h"
#include "rpc/Client.h"
#include "net/PipeHost.h"
#include "server.pb.h"

#include "TableLogic.h"
#include "../clientlib/Client.h"
#include "../clientlib/ProtoSerializer.h"
#include "../clientlib/Game.h"
#include "../clientlib/ITableControl.h"

#include "../serverlib/EVDecisionMaker.h"
#include "../serverlib/MongoStatistics.h"

#include "../statslib/StatisticsCache.h"

#include "../cachelib/ClientCache.h"

#include <mutex>

#include <opencv2/highgui/highgui.hpp>

#include <boost/program_options.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign.hpp>

namespace po = boost::program_options;
namespace gp = google::protobuf;

DECLARE_CURRENT_MODULE(Modules::Tools);
Log m_Log;

class EmptyTableControl : public clnt::ITableControl
{
public:
    virtual void Fold()
    {
        std::cout << "FOLD pressed" << std::endl;
    }

    virtual void CheckCall()
    {
        std::cout << "CHECK/CALL pressed" << std::endl;
    }

    virtual void BetRaise(unsigned amount)
    {
        std::cout << "BET/RAISE pressed, amount: " << amount << std::endl;
    }
};

class EmptyDecisionMaker : public pcmn::ITableLogicCallback
{
public:
    //! On send data to server
    virtual void SendRequest(const net::Packet& packet, bool statistics) override
    {

    }

    //! On need decision
    virtual void MakeDecision
    (
        const pcmn::Player& player,
        const pcmn::Player::Queue& activePlayers,
        const pcmn::TableContext& context,
        pcmn::Player::Position::Value position
    ) override
    {
    }

    //! Write statistics to db
    virtual void WriteStatistics(pcmn::TableContext::Data& data) override
    {

    }
};

const bool FAKE_DECISION_MAKER = true;

class EmptyChannel : public rpc::details::IChannel
{
public:

    class EmptyStatistics : public srv::MongoStatistics
    {
    public:
        EmptyStatistics(ILog& log) : srv::MongoStatistics(log) {}
        virtual void Write(pcmn::TableContext::Data& data) override
        {
        }
    };

    EmptyChannel()
        : m_Statistics(m_Log)
    {
        stats::Cache::Create(m_Log);
    }
    virtual rpc::IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, const gp::Message& request, const rpc::IStream&, const rpc::details::CallParams&) override
    {
        CHECK(method.name() == "GetAction" || method.name() == "SaveGame", cmn::Exception("Unknown method called"), method.full_name());

        if (method.name() == "GetAction")
        {
            std::call_once(m_LoadCache, [this]()
            {
                if (!FAKE_DECISION_MAKER)
                    srv::ClientCache::Create(m_Log);
            });
        }

        net::Reply response;
        boost::shared_ptr<pcmn::ITableLogicCallback> maker;
        if (FAKE_DECISION_MAKER)
            maker = boost::make_shared<EmptyDecisionMaker>();
        else
            maker = boost::make_shared<srv::EVDecisionMaker>(m_Log, m_Statistics, response);

        pcmn::TableLogic logic(m_Log, *maker);
        logic.Parse(static_cast<const net::Packet&>(request));

        LOG_TRACE("Server logic response: %s") % response.ShortDebugString();

        const auto size = response.ByteSize();

        const auto stream = boost::make_shared<std::stringstream>();
        stream->write(reinterpret_cast<const char*>(&size), sizeof(size));
        CHECK(response.SerializeToOstream(stream.get()), cmn::Exception("Failed to serialize response"));
        stream->seekg(0);

        const auto future = rpc::IFuture::Instance();
        cmn::Service::Instance().post(boost::bind(&rpc::IFuture::SetData, future, stream));
        return future;
    }


private:
    EmptyStatistics m_Statistics;
    std::once_flag m_LoadCache;
};

class GameCallback : public clnt::IGame::ICallback
{
public:
    GameCallback() : m_IsGameFinished() {}
    virtual void OnPlayerFolded(const std::string& name) override
    {
    }
    virtual void OnNewGame(bool) override
    {
        m_IsGameFinished = true;
    }
    bool IsNeedDecision() const
    {
        return !m_IsGameFinished;
    }
    virtual void OnNewStreet() override
    {
    }

    virtual bool IsPlayerInGame(const std::string& name)
    {
        return true;
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:
    bool m_IsGameFinished;
};

class EmptyClient : public rpc::Endpoint
{
public:
    EmptyClient(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}
    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id&)
    {
        return m_Channel;
    }
private:
    EmptyChannel m_Channel;
};

boost::shared_ptr<rpc::Endpoint> GetChannel(ILog& log, const po::variables_map& vm)
{
    if (vm.count("server"))
    {
        // server connection
        static const auto host = boost::make_shared<net::pipes::Transport>(m_Log);

        // client rpc instance
        const auto client = rpc::IClient::Instance(log, *host, vm["server"].as<std::string>(), "logs player");
        client->Connect();
        return client;
    }
    else
    {
        return boost::make_shared<EmptyClient>(log, "");
    }
}

class Callback : public clnt::IClient::ICallback
{
public:
    template<typename T>
    Callback(T&& files) : m_Files(files) {}

    virtual clnt::ITableEventsReader::Ptr GetNextEvents()
    {
        if (m_Files.empty())
            return clnt::ITableEventsReader::Ptr();

        cmn::xml::Node node(m_Files.front().wstring());

        LOG_TRACE("Processing events file: [%s]") % m_Files.front().wstring();

        m_Files.pop_front();
        const auto events = clnt::ITableEventsReader::Instance();
        events->Deserialize(node);
        return events;
    }

    virtual void OnNewGame()
    {
        
    }

private:
    std::list<boost::filesystem::path> m_Files;
};

int main(int argc, char* argv[])
{
    try 
    {
        m_Log.Open("player.trace.cfg");

        pcmn::Player::ThisPlayer().Name("CLRN");

        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("screens", po::value<std::string>(), "specify screen files input directory")
            ("games", po::value<std::string>(), "specify games xml files input directory")
            ("events", po::value<std::string>(), "specify events xml files input directory")
            ("server", po::value<std::string>(), "specify poker server endpoint, optional");

        po::positional_options_description p;

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help")) 
        {
            std::cout << desc << std::endl;
            return 1;
        }

        // rpc channel to server
        const auto channel = GetChannel(m_Log, vm);

        if (vm.count("games"))
        {
            // get files list  
            const auto files = fs::GetFiles(vm["games"].as<std::string>(), ".xml");
            for (const auto& file : files)
            {
                try
                {
                    const cmn::xml::Node node(file.wstring());

                    // create game and deserialize
                    GameCallback callback;
                    const auto game = clnt::IGame::Instance(m_Log, callback);

                    game->Deserialize(node);

                    // make packet
                    const auto packet = conv::IXml2Proto::Instance()->Convert(node);

                    // send to server
                    if (callback.IsNeedDecision())
                        channel->Channel<net::PokerService>().GetAction(packet).Response();
                    else
                        channel->Channel<net::PokerService>().SaveGame(packet).Response();
                }
                CATCH_PASS(cmn::Exception("Failed to process file"), file);
            }
        }
        else
        if (vm.count("screens"))
        {
            THROW(cmn::Exception("Not implemented"));
        }
        else
        if (vm.count("events"))
        {
            // get files list  
            auto files = fs::GetFiles(vm["events"].as<std::string>(), ".xml");
            Callback cb(std::move(files));
            EmptyTableControl ctrl;
            const auto client = clnt::IClient::Instance(m_Log, cb, *channel, ctrl);
            client->Run();
        }

        m_Log.Close();

        return 0;
	}
	catch (const std::exception& e)
	{
        LOG_ERROR("%s") % cmn::ExceptionInfo(e);
        m_Log.Close();
        return 1;
	}
}


