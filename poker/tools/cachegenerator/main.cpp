#include "../../cachelib/CachePrimitives.h"
#include "../../cachelib/ServerCache.h"
#include "common/Modules.h"
#include "common/Log.h"
#include "Evaluator.h"
#include "common/ServiceHolder.h"

#include <stdexcept>
#include <iostream>
#include <numeric>

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

typedef boost::unordered::unordered_map<srv::cache::Game, std::vector<float>, srv::cache::Game> GameMap;
Log g_Log;
srv::ServerCache g_Cache(g_Log);

pcmn::Card::List GetRandomCards(bool* dead, unsigned count)
{
    pcmn::Card::List result;
    for (unsigned i = 0 ; i < count; ++i)
        result.push_back(pcmn::Card().FromEvalFormat(pcmn::Evaluator::Instance().GetRandomCard(dead)));
    return result;
}

void SaveToCache(const GameMap& map);

const unsigned g_Total = 1000000;

void RunRandomEvaluation(ILog& log, const unsigned players, const unsigned flopCardCnt, GameMap& map, const boost::function<bool(unsigned)>& callback)
{
    bool deadCards[cfg::CARD_DECK_SIZE] = {};

    pcmn::Hand parser;
    for (unsigned i = 0 ; i < g_Total; ++i)
    {
        // get random cards for bot
        const pcmn::Card::List botCards = GetRandomCards(deadCards, 2);

        // get random cards for flop
        const pcmn::Card::List flopCards = GetRandomCards(deadCards, flopCardCnt);

        // get random cards for each player
        pcmn::Hand::List opponents;
        pcmn::Board::HandList opponentCards;
        for (unsigned opp = 0 ; opp < players; ++opp)
        {
            const pcmn::Card::List cards = GetRandomCards(deadCards, 2);
            opponents.push_back(parser.Parse(cards, flopCards).GetValue());
            opponentCards.push_back(cards);
        }

        memset(deadCards, 0, _countof(deadCards));

        // evaluate equity
        const float equity = pcmn::Evaluator::Instance().GetEquity(botCards, flopCards, opponentCards);

        // get bot hand description
        const pcmn::Hand::Value hand = parser.Parse(botCards, flopCards).GetValue();

        // save values
        const srv::cache::Game game(hand, opponents, flopCardCnt);
        map[game].push_back(equity);

        if (i && i % 100 == 0)
        {
            if (callback(i))
                break;
        }
    }
}

void SaveToCache(GameMap& map, unsigned flopCardCnt)
{
    for (const auto& pair : map)
    {
        // accumulate equities
        const auto& equities = pair.second;
        CHECK(!equities.empty(), cmn::Exception("Empty equities"));

        const float avg = std::accumulate(equities.begin(), equities.end(), 0.0f) / equities.size();

        const pcmn::Hand::List opponents(pair.first.m_Opponents.begin(), pair.first.m_Opponents.end());

        switch (flopCardCnt)
        {
        case 0:
            g_Cache.SetPreflopEquity(pair.first.m_Bot, opponents, avg);
            break;
        case 3:         
            g_Cache.SetFlopEquity(pair.first.m_Bot, opponents, avg);
            break;
        case 4:         
            g_Cache.SetTurnEquity(pair.first.m_Bot, opponents, avg);
            break;
        default: throw std::runtime_error("invalid cards count");
        }
    }

    try
    {
        g_Cache.Save();
    }
    catch (const std::exception& e)
    {
        std::wcout << "Failed to save cache: " << cmn::ExceptionInfo(e) << std::endl;
    }
}

void RunStreet(ILog& log, const unsigned playerCount, unsigned flopCount)
{
    for (unsigned opp = 1; opp <= playerCount; ++opp)
    {
        GameMap map;

        const boost::posix_time::ptime startTime = boost::posix_time::microsec_clock::local_time();
        std::size_t lastSize = 0;
        std::deque<std::size_t> deltas;

        const auto callback = [&](const unsigned iteration)
        {
            const boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time(); 
            const auto elapsed = boost::posix_time::time_duration(now - startTime).total_milliseconds();
            const auto millisecPerRun = elapsed / iteration;

            const auto remaining = millisecPerRun * (g_Total - iteration);
            const std::size_t delta = map.size() - lastSize;

            deltas.push_back(delta);

            if (deltas.size() > 10)
                deltas.pop_front();

            const boost::posix_time::ptime completeTime = now + boost::posix_time::milliseconds(remaining);
            std::cout 
                << "run: " << iteration 
                << ", percent: " << iteration * 100 / g_Total 
                << ", delta: " << delta 
                << ", size: " << map.size() 
                << ", opponents: " << opp
                << ", finish at: " << boost::posix_time::to_simple_string(completeTime) << std::endl;

            lastSize = map.size();
            return !std::accumulate(deltas.begin(), deltas.end(), 0); // stop if last 10 iterations has no effect
        };

        RunRandomEvaluation(log, opp, flopCount, map, callback);

        // save to cache
        SaveToCache(map, flopCount);
    }
}

void RunHands(unsigned cardCount)
{
    assert(cardCount == 2 || cardCount == 5 || cardCount == 6);

    const boost::posix_time::ptime startTime = boost::posix_time::microsec_clock::local_time();

    const auto callback = [&startTime](const unsigned iteration, const unsigned total)
    {
        if (iteration % (total / 100) != 0)
            return;

        const boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time(); 
        const auto elapsed = boost::posix_time::time_duration(now - startTime).total_milliseconds();
        const auto millisecPerRun = elapsed / iteration;

        const auto remaining = millisecPerRun * (total - iteration);

        const boost::posix_time::ptime completeTime = now + boost::posix_time::milliseconds(remaining);
        std::cout 
            << "run: " << iteration 
            << ", percent: " << iteration * 100 / total 
            << ", finish at: " << boost::posix_time::to_simple_string(completeTime) << std::endl;
    };

    if (cardCount == 2)
    {
        unsigned total = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
                ++total;

        unsigned counter = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
            {
                srv::cache::Hands hands;
                hands.m_Size = 2;
                hands.m_Cards[0] = first;
                hands.m_Cards[1] = second;

                g_Cache.GetParsedHand(hands);
                callback(++counter, total);
            }
    }
    else
    if (cardCount == 5)
    {
        unsigned total = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
                for (unsigned char third = 0 ; third < cfg::CARD_DECK_SIZE; ++third)
                    for (unsigned char fourth = 0 ; fourth < cfg::CARD_DECK_SIZE; ++fourth)
                        for (unsigned char fifth = 0 ; fifth < cfg::CARD_DECK_SIZE; ++fifth)
                        {
                            srv::cache::Hands hands;
                            hands.m_Size = 5;
                            hands.m_Cards[0] = first;
                            hands.m_Cards[1] = second;
                            hands.m_Cards[2] = third;
                            hands.m_Cards[3] = fourth;
                            hands.m_Cards[4] = fifth;

                            if (hands.IsDuplicated())
                                continue;

                            ++total;
                        }

        unsigned counter = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
                for (unsigned char third = 0 ; third < cfg::CARD_DECK_SIZE; ++third)
                    for (unsigned char fourth = 0 ; fourth < cfg::CARD_DECK_SIZE; ++fourth)
                        for (unsigned char fifth = 0 ; fifth < cfg::CARD_DECK_SIZE; ++fifth)
                        {
                            srv::cache::Hands hands;
                            hands.m_Size = 5;
                            hands.m_Cards[0] = first;
                            hands.m_Cards[1] = second;
                            hands.m_Cards[2] = third;
                            hands.m_Cards[3] = fourth;
                            hands.m_Cards[4] = fifth;

                            if (hands.IsDuplicated())
                                continue;

                            std::sort(&hands.m_Cards[2], &hands.m_Cards[4]);

                            g_Cache.GetParsedHand(hands);
                            callback(++counter, total);
                        }
    }
    else
    if (cardCount == 6)
    {
        unsigned total = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
                for (unsigned char third = 0 ; third < cfg::CARD_DECK_SIZE; ++third)
                    for (unsigned char fourth = 0 ; fourth < cfg::CARD_DECK_SIZE; ++fourth)
                        for (unsigned char fifth = 0 ; fifth < cfg::CARD_DECK_SIZE; ++fifth)
                            for (unsigned char six = 0 ; six < cfg::CARD_DECK_SIZE; ++six)
                            {
                                srv::cache::Hands hands;
                                hands.m_Size = 6;
                                hands.m_Cards[0] = first;
                                hands.m_Cards[1] = second;
                                hands.m_Cards[2] = third;
                                hands.m_Cards[3] = fourth;
                                hands.m_Cards[4] = fifth;
                                hands.m_Cards[5] = six;

                                if (hands.IsDuplicated())
                                    continue;

                                ++total;
                            }

        unsigned counter = 0;
        for (unsigned char first = 0 ; first < cfg::CARD_DECK_SIZE; ++first)
            for (unsigned char second = first + 1 ; second < cfg::CARD_DECK_SIZE; ++second)
                for (unsigned char third = 0 ; third < cfg::CARD_DECK_SIZE; ++third)
                    for (unsigned char fourth = 0 ; fourth < cfg::CARD_DECK_SIZE; ++fourth)
                        for (unsigned char fifth = 0 ; fifth < cfg::CARD_DECK_SIZE; ++fifth)
                            for (unsigned char six = 0 ; six < cfg::CARD_DECK_SIZE; ++six)
                            {
                                srv::cache::Hands hands;
                                hands.m_Size = 6;
                                hands.m_Cards[0] = first;
                                hands.m_Cards[1] = second;
                                hands.m_Cards[2] = third;
                                hands.m_Cards[3] = fourth;
                                hands.m_Cards[4] = fifth;
                                hands.m_Cards[5] = six;

                                if (hands.IsDuplicated())
                                    continue;

                                std::sort(&hands.m_Cards[2], &hands.m_Cards[5]);

                                g_Cache.GetParsedHand(hands);
                                callback(++counter, total);
                            }
    } 
}

int main(int argc, char* argv[])
{
	try 
	{ 

        if (argc != 3 && argc != 2)
        {
            std::cout << "Expected players count and flop cards count, or cards count only for hand parse" << std::endl;
            return 1;
        }

        cmn::Service::Holder app(boost::thread::hardware_concurrency() - 1);

        g_Log.Open(ILog::Level::Trace);

        try
        {
            g_Cache.Load();
        }
        catch (const std::exception& e)
        {
            std::wcout << "Failed to load cache: " << cmn::ExceptionInfo(e) << std::endl;
        }

        if (argc == 2)
        {
            RunHands(boost::lexical_cast<unsigned>(argv[1]));
        }
        else
        {
            const unsigned playerCount = boost::lexical_cast<unsigned>(argv[1]);
            const unsigned street = boost::lexical_cast<unsigned>(argv[2]);

            RunStreet(g_Log, playerCount, street);
        }

        g_Cache.Save();
	}
	catch (const std::exception& e)
	{
		std::wcout << cmn::ExceptionInfo(e) << std::endl;
	}

	return 0;
}


