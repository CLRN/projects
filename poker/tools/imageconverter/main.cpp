#include "exception/CheckHelpers.h"
#include "common/Log.h"
#include "common/Modules.h"
#include "recognition/Recognition.h"
#include "xml/Node.h"
#include "common/FileSystem.h"
#include "rpc/Endpoint.h"

#include "../clientlib/ImageManager.h"
#include "../clientlib/TableRecognizer.h"
#include "../clientlib/TableParser.h"

#include <opencv2/highgui/highgui.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace gp = google::protobuf;

DECLARE_CURRENT_MODULE(Modules::Tools);
Log m_Log;

class EmptyCallback : public clnt::ITableRecognizer::ICallback
{
    virtual void Wait4NewGame(const ReadyFunc& func)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnNewGameReady()
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnNextEventsNeeded(const clnt::IImageManager::Event::List& evnt)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }
};

class EmptyEndpoint : public rpc::Endpoint
{
public:
    EmptyEndpoint(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}
    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id& id)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }
};

int main(int argc, char* argv[])
{
    try 
    {
        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("input", po::value<std::string>(), "specify screen files input directory")
            ("output", po::value<std::string>(), "specify events xml files output directory")
            ("ext", po::value<std::string>(), "specify image extension");

        po::positional_options_description p;

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help")) 
        {
            std::cout << desc << std::endl;
            return 1;
        }

        EmptyCallback cb;

        const auto recognizer = rec::IRecognizer::Instance();
        const auto dealerButton = cv::imread("../../../poker/data/PokerStars/objects/dealer.png");
        const auto cardsBack = cv::imread("../../../poker/data/PokerStars/objects/cards_back.png");
        const auto table = clnt::ITableRecognizer::Instance(m_Log, EmptyEndpoint(m_Log, "converter"), cb);
        const auto parser = clnt::ITableParser::Instance(m_Log, *recognizer, *table);
        const auto adapter = clnt::IImageAdapter::Instance(m_Log, *recognizer, *parser, dealerButton, cardsBack);

        // set up areas
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        table->Deserialize(node);
        const auto params = table->GetAllAreas();
        adapter->Initialize(params);

        const auto folder = boost::filesystem::system_complete(vm["output"].as<std::string>());
        const auto input = boost::filesystem::system_complete(vm["input"].as<std::string>());
        boost::filesystem::create_directories(folder);

        const auto files = fs::GetFiles(input, std::string(".") + vm["ext"].as<std::string>());
        for (auto file : files)
        {
            std::cout << "processing: " << file.string();
            std::cout.flush();

            const auto image = cv::imread(file.string());
            const auto events = adapter->Parse(image);

            cmn::xml::Node node;
            events->Serialize(node);

            const auto path = folder / file.replace_extension(".xml").filename();
            node.Save2File(path.wstring());

            std::cout << ", result: " << path.string() << std::endl;
        }

        m_Log.Close();

        return 0;
	}
	catch (const std::exception& e)
	{
        std::wcout << cmn::ExceptionInfo(e) << std::endl;
        m_Log.Close();
        return 1;
	}
}


