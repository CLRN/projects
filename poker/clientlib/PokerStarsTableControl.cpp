#include "PokerStarsTableControl.h"
#include "common/Modules.h"
#include "common/ServiceHolder.h"

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/random_device.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

namespace clnt
{
namespace ps
{

boost::random::random_device g_Random;

DECLARE_CURRENT_MODULE(Modules::Client);

TableControl::TableControl(ILog& logger, IConfig& cfg, HWND window) : m_Log(logger), m_Window(window)
{
    SCOPED_LOG(m_Log);

    // read config data
    cfg.Read();
    m_Settings = cfg.Get();
}

void TableControl::Fold()
{
    SCOPED_LOG(m_Log);

    const TimerPtr timer(new boost::asio::deadline_timer(cmn::Service::Instance()));

    //boost::random::uniform_int_distribution<> wait(1000, 3000);
    //timer->expires_from_now(boost::posix_time::milliseconds(wait(g_Random)));
    //timer->async_wait();
    boost::system::error_code e;
    boost::bind(&TableControl::TimerCallback, this, _1, m_Window, m_Settings.m_FoldButton, timer, boost::function<void()>())(e);
}

void TableControl::CheckCall()
{
    SCOPED_LOG(m_Log);

    const TimerPtr timer(new boost::asio::deadline_timer(cmn::Service::Instance()));

    //boost::random::uniform_int_distribution<> wait(1000, 2000);
    //timer->expires_from_now(boost::posix_time::milliseconds(wait(g_Random)));
    //timer->async_wait(boost::bind(&TableControl::TimerCallback, this, _1, m_Window, m_Settings.m_CheckCallButton, timer, boost::function<void()>()));
    boost::system::error_code e;
    boost::bind(&TableControl::TimerCallback, this, _1, m_Window, m_Settings.m_CheckCallButton, timer, boost::function<void()>())(e);
}


void TableControl::BetRaise(unsigned amount)
{
    SCOPED_LOG(m_Log);

    const TimerPtr timer(new boost::asio::deadline_timer(cmn::Service::Instance()));

    const auto fn = boost::bind
    (
        &TableControl::TimerCallback,
        this, 
        _1, 
        m_Window, 
        m_Settings.m_RaiseButton, 
        timer, 
        boost::function<void()>
        (
            boost::bind
            (
                &TableControl::SetBetAmount, 
                this, 
                amount
            )
        )
    );
    boost::system::error_code e;
    fn(e);


//     boost::random::uniform_int_distribution<> wait(2000, 4000);
//     timer->expires_from_now(boost::posix_time::milliseconds(wait(g_Random)));
//     timer->async_wait(fn);
}

void TableControl::TimerCallback(const boost::system::error_code& e, HWND window, unsigned key, TimerPtr, const boost::function<void()>& work)
{
    SCOPED_LOG(m_Log);

    try 
    {
	    const HWND lastWindow = GetForegroundWindow();
        if (lastWindow != window)
        {
            ShowWindow(window, SW_HIDE);
            SetForegroundWindow(window);
            ShowWindow(window, SW_SHOWMINIMIZED);
        }
	
	    if (work)
	        work();
	
	    KEYBDINPUT kbinput;
	    ZeroMemory(&kbinput, sizeof(kbinput));
	    kbinput.wVk = key;
	    kbinput.dwFlags = 0; 
	    kbinput.time = 0;
	
	    INPUT input;
	    ZeroMemory(&input, sizeof(input));
	    input.type = INPUT_KEYBOARD;
	    input.ki = kbinput;
	
	    SendInput(1, &input, sizeof(input));

        if (lastWindow != window)
        {
            //ShowWindow(lastWindow, SW_SHOWDEFAULT);
            SetForegroundWindow(lastWindow);
        }
    }
    CATCH_PASS(cmn::Exception("Timer callback failed"), window, key)
}

void TableControl::SetBetAmount(unsigned amount)
{
    SCOPED_LOG(m_Log);

    const HWND slider = FindWindowExA(m_Window, NULL, "PokerStarsSliderClass", NULL);	
    CHECK(slider != NULL, cmn::Exception("Failed to find slider"));
    const HWND editor = FindWindowExA(slider, NULL, "PokerStarsSliderEditorClass", NULL);
    CHECK(editor != NULL, cmn::Exception("Failed to find editor"));

    const std::string value = boost::lexical_cast<std::string>(amount);
    SendMessageA(editor, WM_SETTEXT, NULL, reinterpret_cast<LPARAM>(value.c_str()));
}

} // namespace ps
} // namespace clnt