#include "PokerStarsClient.h"
#include "exception/CheckHelpers.h"
#include "PokerStarsTable.h"
#include "common/Modules.h"
#include "net/UDPHost.h"
#include "Player.h"
#include "Config.h"
#include "PokerStarsTableControl.h"
#include "PokerStarsConfig.h"
#include "common/ServiceHolder.h"
#include "recognition/Recognition.h"

#include <windows.h>

#include <opencv2/highgui/highgui.hpp>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>

namespace clnt
{

const wchar_t POKERSTARS_TABLE_WINDOW_CLASS[] = L"PokerStarsTableFrameClass";
DECLARE_CURRENT_MODULE(Modules::Client);

IClient* CreateClient()
{
	return new ps::Client();
}

namespace ps
{

void Client::HandleMessage(HWND hWnd, UINT Msg, WPARAM /*wParam*/, LPARAM lParam)
{
	SCOPED_LOG(m_Log);

	if (Msg != 0x8002)
		return;

	const std::wstring& name = GetWindowClass(hWnd);

	if (name != POKERSTARS_TABLE_WINDOW_CLASS)
		return; // not a poker window

	try 
	{
		boost::mutex::scoped_lock lock(m_Mutex);
		Tables::const_iterator it = m_Tables.find(hWnd);
		if (it == m_Tables.end())
		{
			const net::IConnection::Ptr connection = m_Server->Connect(cfg::DEFAULT_CONNECTION);
            std::auto_ptr<ITableControl> ctrl(new TableControl(m_Log, *m_Cfg, hWnd));
			it = m_Tables.insert(std::make_pair(hWnd, ITable::Ptr(new ps::Table(m_Log, hWnd, connection, ctrl.get())))).first;
            ctrl.release();
		}
	
		lock.unlock();
	
		const dasm::WindowMessage& message = *reinterpret_cast<const dasm::WindowMessage*>(lParam);
	
		it->second->HandleMessage(message);
	}
	catch (const std::exception& e)
	{
		if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Error))
			boost::thread(boost::bind(&Client::SaveScreenThread, this, hWnd, conv::cast<std::string>(cmn::ExceptionInfo(e))));
	}
	catch (...)
	{
		if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Error))
			boost::thread(boost::bind(&Client::SaveScreenThread, this, hWnd, std::string("Unhandled error")));
	}
}

Client::Client() : m_Server(new net::UDPHost(m_Log)), m_Cfg(new Config(m_Log))
{
	try 
	{
		SCOPED_LOG(m_Log);

        m_Log.Open(ILog::Level::Trace);

		// player name
		pcmn::Player::ThisPlayer().Name("CLRN");

        // add worker
        m_ServiceHolder.reset(new cmn::Service::Holder(1));
	}
	CATCH_PASS(cmn::Exception("Failed to construct client"))
}

void Client::SaveScreenThread(HWND handle, const std::string& message)
{
	SCOPED_LOG(m_Log);
		
	try 
	{
        auto path = boost::filesystem::initial_path() / "logs/";
		boost::filesystem::create_directories(path);

		std::wstring time = boost::posix_time::to_iso_extended_wstring(boost::posix_time::microsec_clock().local_time());
		boost::algorithm::replace_all(time, L":", L".");
		boost::algorithm::replace_all(time, L"T", L"_");

		path += time;
		path = boost::filesystem::system_complete(path);

		LOG_ERROR("Handle message failed: [%s], file: [%s]") % message % path;

        const auto recognizer = rec::IRecognizer::Instance();

		ShowWindow(handle, SW_SHOW);
		for (unsigned i = 0 ; i < 10; ++i)
		{
            const std::wstring temp = path.wstring() + std::wstring(L"_") + conv::cast<std::wstring>(i) + L".bmp";
            const auto image = recognizer->CaptureWindow(reinterpret_cast<unsigned>(handle));
            cv::imwrite(boost::filesystem::system_complete(temp).string(), image);

			boost::this_thread::interruptible_wait(200);
		}
	}
	catch (const std::exception& e)
	{
		LOG_ERROR("Failed to save screenshot, error: [%s]") % cmn::ExceptionInfo(e);
	}
}
const std::wstring& Client::GetWindowClass(HWND handle)
{
	WindowClasses::const_iterator it = m_WindowClasses.find(handle);
	if (it == m_WindowClasses.end())
	{
		wchar_t name[512] = {0};
		CHECK_LE(GetClassNameW(handle, name, _countof(name)), cmn::Exception("Failed to get class name"));
		it = m_WindowClasses.insert(std::make_pair(handle, name)).first;
	}

	return it->second;
}


} // namespace ps
} // namespace clnt

