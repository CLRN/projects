﻿#include "Game.h"
#include "common/Modules.h"
#include "xml/Node.h"
#include "common/GUID.h"
#include "exception/CheckHelpers.h"
#include "serialization/Helpers.h"
#include "serialization/Serialization.h"
#include "GameRound.h"
#include "TableSnapshot.h"

#include <map>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/filesystem.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/optional.hpp>

namespace clnt
{

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

DECLARE_CURRENT_MODULE(Modules::Logic);

class Game 
    : public IGame
    , public cmn::srl::Serializable
    , boost::noncopyable
    , public IGameRound::ICallback
{
public:
 
    typedef std::map<std::string, pcmn::Player> Players;

    Game(ILog& log, IGame::ICallback& cb)
        : SERIALIZABLE("game", FIELD(m_Rounds, boost::bind(&IGameRound::Instance, boost::ref(m_Log), boost::ref(*this))))
        , m_Log(log)
        , m_Callback(cb)
        , m_Snapshot(ITableSnapshot::Instance(m_Log))
    {
    }

private:

    virtual void Clear() override
    {
        m_Rounds.clear();
        m_Snapshot->Clear();
        LOG_TRACE("Game cleared");
    }

    virtual bool IsEmpty() const override
    {
        return m_Rounds.empty();
    }

    virtual void Flush() override
    {
        try 
        {
            LOG_TRACE("Flushing game, rounds: %s, snapshot: %s") % m_Rounds.size() % m_Snapshot->GetLogInfo();

            if (m_Rounds.empty())
                CreateNewRound();

            // push actions to game round
            const auto round = m_Rounds.back();
            round->Play(*m_Snapshot);
        }
        CATCH_PASS(cmn::Exception("Failed to flush all actions"))
    }

    virtual void Load(const std::string& fileName) override
    {
        cmn::xml::Node node(fileName);
        Deserialize(node);
    }

    virtual void SetPlayers(const IGameRound::PlayerQueue& players) override
    {
        m_Snapshot->SetSequence(players);
    }

    virtual void PushAction(const std::string& name, pcmn::Action::Value value, unsigned amount, unsigned stack) override
    {
        const auto last = m_Snapshot->GetBigBlind();
        if (value == pcmn::Action::SmallBlind)
            m_Snapshot->SetBigBlind(amount * 2);
        if (value == pcmn::Action::BigBlind)
            m_Snapshot->SetBigBlind(amount);

        m_Snapshot->PushAction(name, value, amount, stack);
    }

    virtual void SetButton(const std::string& name) override
    {
        m_Snapshot->SetButton(name);
    }

    virtual void PushPlayerCard(const std::string& name, const pcmn::Card& card) override
    {
        m_Snapshot->PushPlayerCard(name, card);
        if (!m_Rounds.empty())
            m_Rounds.front()->Play(*m_Snapshot);
    }

    virtual void PushFlopCard(const pcmn::Card& card) override
    {
        m_Snapshot->PushFlopCard(card);
    }

    virtual void SetPlayerParams(const pcmn::Player& player) override
    {
        m_Snapshot->SetPlayer(player);
    }

    virtual void OnPlayerFolded(const std::string& name) override
    {
        m_Callback.OnPlayerFolded(name);
    }

    virtual void OnStreetFinished(bool isLast, const IGameRound::Action::List& remaining, bool wait4Button) override
    {
        if (isLast)
        {
            m_Callback.OnNewGame(wait4Button);
            if (m_Rounds.empty())
                CreateNewRound();
        }
        else
        {
            m_Callback.OnNewStreet();
            m_Snapshot->Reset();

            // push remaining actions to snapshot
            for (const auto& a : remaining)
                m_Snapshot->PushAction(a->m_Name, a->m_Action, a->m_Amount, a->m_StackAfter);

            CreateNewRound();
        }
    }

    void CreateNewRound()
    {
        const auto round = IGameRound::Instance(m_Log, *this);
    
        // initialize round here only if next round cards available
        if (!m_Rounds.empty() && m_Rounds.back()->GetStreet() != pcmn::Table::Phase::FromCardsCount(m_Snapshot->GetFlop().size()))
        {
            LOG_TRACE("Creating and initializing new round, existing size: '%s'") % m_Rounds.size();
            round->Initialize(*m_Snapshot);
        }
        else
        {
            LOG_TRACE("Creating new round, existing size: '%s'") % m_Rounds.size();
            if (!m_Rounds.empty())
            {
                const auto street = m_Rounds.back()->GetStreet();
                if (street != pcmn::Table::Phase::River)
                    m_Snapshot->SetStreet(static_cast<pcmn::Table::Phase::Value>(street + 1));
            }
        }

        m_Rounds.push_back(round);
    }

    virtual const std::string& GetLastPlayer() override
    {
        static const std::string empty;
        if (m_Rounds.empty())
            return empty;

        const auto& queue = m_Rounds.back()->GetQueue(*m_Snapshot);
        CHECK(!queue.empty(), cmn::Exception("Unable to get last player, queue is empty"));
        return queue.back();
    }

    virtual const std::string& GetSmallBlindPlayer() override
    {
        static const std::string empty;
        if (m_Rounds.empty())
            return empty;

        const auto& queue = m_Rounds.back()->GetQueue(*m_Snapshot);
        CHECK(!queue.empty(), cmn::Exception("Unable to get SB player, queue is empty"));
        if (queue.size() == 1)
            return empty;

        return queue[queue.size() - 2];
    }

    virtual bool IsPlayerInGame(const std::string& name) override
    {
        return m_Callback.IsPlayerInGame(name);
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        return m_Callback.ObtainBet(name);
    }

    virtual pcmn::Table::Phase::Value GetPhase() const override
    {
        return m_Snapshot->GetStreet();
    }

private:

    ILog& m_Log;
    IGame::ICallback& m_Callback;
    Rounds m_Rounds;
    ITableSnapshot::Ptr m_Snapshot;
};

#pragma warning(pop)

IGame::Ptr IGame::Instance(ILog& log, IGame::ICallback& cb)
{
    return boost::make_shared<Game>(log, cb);
}


} // namespace clnt
