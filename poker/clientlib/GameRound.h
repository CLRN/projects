#ifndef GameRound_h__
#define GameRound_h__

#include "common/ILog.h"
#include "Actions.h"
#include "Cards.h"
#include "ITableLogic.h"
#include "serialization/ISerializable.h"
#include "Player.h"
#include "Table.h"
#include "TableSnapshot.h"

#include <deque>
#include <string>

#include <boost/shared_ptr.hpp>

namespace cmn
{
namespace xml
{
    class Node;
}
}

namespace clnt
{

class IGameRound : public virtual cmn::srl::ISerializable
{
public:
    typedef boost::shared_ptr<IGameRound> Ptr;
    typedef std::deque<std::string> PlayerQueue;

    struct Action
    {
        typedef boost::shared_ptr<Action> Ptr;
        typedef std::list<Ptr> List;
        Action(const std::string& name = "", pcmn::Action::Value v = pcmn::Action::Unknown, unsigned amount = 0, unsigned stackAfter = 0)
            : m_Name(name)
            , m_Action(v)
            , m_Amount(amount)
            , m_StackAfter(stackAfter)
        {
        }

        std::string m_Name;
        pcmn::Action::Value m_Action;
        unsigned m_Amount;
        unsigned m_StackAfter;
    };

    __declspec(novtable) class ICallback
    {
    public:
        virtual void OnPlayerFolded(const std::string& name) = 0;
        virtual void OnStreetFinished(bool isLast, const Action::List& remainingActions, bool wait4Button) = 0;
        virtual bool IsPlayerInGame(const std::string& name) = 0;
        virtual unsigned ObtainBet(const std::string& name) = 0;
    };

    virtual ~IGameRound() {}

    virtual void Initialize(ITableSnapshot& snap) = 0;
    virtual void Play(ITableSnapshot& snap) = 0;
    virtual void SetPlayerCards(const std::string& name, const pcmn::Card::List& cards) = 0;

    virtual void Compare(const IGameRound& other) const = 0;
    virtual const PlayerQueue& GetQueue(ITableSnapshot& snap) = 0;
    virtual pcmn::Table::Phase::Value GetStreet() const = 0;

    static Ptr Instance(ILog& log, ICallback& cb);
};

} // namespace clnt

#endif // GameRound_h__
