﻿#include "TableEvents.h"
#include "recognition/Recognition.h"
#include "common/Modules.h"
#include "xml/Node.h"
#include "common/ILog.h"

#include <boost/variant.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/make_shared.hpp>

namespace clnt
{
namespace
{

struct PlayerState
{
    cv::Rect m_Area;
    bool m_IsInGame;
};

struct PlayerBet
{
    cv::Rect m_Area;
    unsigned m_Bet;
};

typedef boost::variant
<
    rec::IRecognizer::Result,   // recognize result
    cv::Rect,                   // button rectangle
    cv::Size,                   // image size
    PlayerState,                // player state
    PlayerBet                   // player bet size
> Event;

DECLARE_CURRENT_MODULE(Modules::Client);

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

class EventsWriter : public ITableEvents
{
public:

    EventsWriter() {}

    class Serializer : public boost::static_visitor<>
    {
    public:
        Serializer(cmn::xml::Node& n) : m_Root(n) 
        {
            m_Root.Name("events");
        }
        void operator()(const rec::IRecognizer::Result& arg) const
        {
            m_Root.Add("recognize")->
                Set("@x", arg.m_Rect.x).
                Set("@y", arg.m_Rect.y).
                Set("@w", arg.m_Rect.width).
                Set("@h", arg.m_Rect.height).
                Set("@text", arg.m_Text);
        }
        void operator()(const cv::Rect& arg) const
        {
            m_Root.Add("button")->
                Set("@x", arg.x).
                Set("@y", arg.y).
                Set("@w", arg.width).
                Set("@h", arg.height);
        }
        void operator()(const cv::Size& arg) const
        {
            m_Root.Add("size")->Set("@w", arg.width).Set("@h", arg.height);
        }
        void operator()(const PlayerState& arg) const
        {
            m_Root.Add("state")->
                Set("@x", arg.m_Area.x).
                Set("@y", arg.m_Area.y).
                Set("@w", arg.m_Area.width).
                Set("@h", arg.m_Area.height).
                Set("@value", arg.m_IsInGame);
        }
        void operator()(const PlayerBet& arg) const
        {
            m_Root.Add("bet")->
                Set("@x", arg.m_Area.x).
                Set("@y", arg.m_Area.y).
                Set("@w", arg.m_Area.width).
                Set("@h", arg.m_Area.height).
                Set("@value", arg.m_Bet);
        }
    private:
        cmn::xml::Node& m_Root;
    };

    EventsWriter(ILog& log, rec::IRecognizer& recognizer)
    {
    }

    virtual void Serialize(cmn::xml::Node& root) const override
    {
        boost::for_each(m_Events, boost::apply_visitor(Serializer(root)));
    }

    virtual void OnTableSize(int width, int height) override
    {
        m_Events.push_back(cv::Size(width, height));
    }

    virtual void OnRegionRecognized(const cv::Rect& rect, const std::wstring& text) override
    {
        rec::IRecognizer::Result res;
        res.m_Rect = rect;
        res.m_Text = text;
        m_Events.push_back(std::move(res));
    }

    virtual void OnButtonFound(const cv::Rect& rect) override
    {
        m_Events.push_back(rect);
    }

    virtual void OnPlayerState(const cv::Rect& rect, bool isInGame) override
    {
        PlayerState state = { rect, isInGame };
        m_Events.push_back(std::move(state));
    }

    virtual void OnPlayerBet(const cv::Rect& player, unsigned bet) override
    {
        PlayerBet betSize;
        betSize.m_Area = player;
        betSize.m_Bet = bet;
        m_Events.push_back(std::move(betSize));
    }

    virtual void Deserialize(const cmn::xml::Node& node)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual std::string GetNodeName() const override
    {
        return "events";
    }

    virtual bool IsEmpty() const
    {
        return m_Events.empty();
    }

private:
    mutable std::vector<Event> m_Events;
};

class EventsReader : public ITableEventsReader
{
public:

    EventsReader() {}

    class DeSerializer
    {
    public:
        Event operator()(const cmn::xml::Node& arg) const
        {
            
            // extract type
            const auto type = arg.Name<std::string>();
            if (type == "recognize")
            {
                rec::IRecognizer::Result rec;
                rec.m_Text = arg.Get<std::wstring>("@text");
                rec.m_Rect.x = arg.Get<int>("@x");
                rec.m_Rect.y = arg.Get<int>("@y");
                rec.m_Rect.width = arg.Get<int>("@w");
                rec.m_Rect.height = arg.Get<int>("@h");
                return rec;
            }
            else
            if (type == "button")
            {
                cv::Rect result;
                result.x = arg.Get<int>("@x");
                result.y = arg.Get<int>("@y");
                result.width = arg.Get<int>("@w");
                result.height = arg.Get<int>("@h");
                return result;
            }
            else
            if (type == "state")
            {
                PlayerState result;
                result.m_Area.x = arg.Get<int>("@x");
                result.m_Area.y = arg.Get<int>("@y");
                result.m_Area.width = arg.Get<int>("@w");
                result.m_Area.height = arg.Get<int>("@h");
                result.m_IsInGame = arg.Get<bool>("@value");
                return result;
            }
            else
            if (type == "bet")
            {
                PlayerBet result;
                result.m_Area.x = arg.Get<int>("@x");
                result.m_Area.y = arg.Get<int>("@y");
                result.m_Area.width = arg.Get<int>("@w");
                result.m_Area.height = arg.Get<int>("@h");
                result.m_Bet = arg.Get<unsigned>("@value");
                return result;
            }
            else
            if (type == "size")
            {
                cv::Size result;
                result.width = arg.Get<int>("@w");
                result.height = arg.Get<int>("@h");
                return result;
            }       

            assert(!"Unexpected item type");
            return Event();
        }
    };

    class Player : public boost::static_visitor<>
    {
    public:
        Player(ITableEvents& reader) : m_Reader(reader) {}
        void operator()(const rec::IRecognizer::Result& arg) const
        {
            m_Reader.OnRegionRecognized(arg.m_Rect, arg.m_Text);
        }
        void operator()(const cv::Rect& arg) const
        {
            m_Reader.OnButtonFound(arg);
        }
        void operator()(const cv::Size& arg) const
        {
            m_Reader.OnTableSize(arg.width, arg.height);
        }
        void operator()(const PlayerState& arg) const
        {
            m_Reader.OnPlayerState(arg.m_Area, arg.m_IsInGame);
        }
        void operator()(const PlayerBet& arg) const
        {
            m_Reader.OnPlayerBet(arg.m_Area, arg.m_Bet);
        }
    private:
        ITableEvents& m_Reader;
    };

    class Checker : public boost::static_visitor<bool>
    {
    public:
        Checker(const cv::Rect& rect) : m_Rect(rect) {}
        bool operator()(const rec::IRecognizer::Result& arg) const
        {
            return arg.m_Rect == m_Rect;
        }
        bool operator()(const cv::Rect& arg) const 
        { 
            return arg == m_Rect;
        }
        bool operator()(const cv::Size& arg) const { return false; }
        bool operator()(const PlayerState& arg) const 
        { 
            return arg.m_Area == m_Rect;
        }
        bool operator()(const PlayerBet& arg) const { return false; }
    private:
        const cv::Rect& m_Rect;
    };

    EventsReader(ILog& log, rec::IRecognizer& recognizer)
    {
    }

    virtual void Deserialize(const cmn::xml::Node& root) override
    {
        boost::transform(root, std::back_inserter(m_Events), DeSerializer());
    }

    virtual void Play(ITableEvents& parser) override
    {
        boost::for_each(m_Events, boost::apply_visitor(Player(parser)));
    }

    bool IsExists(const Areas& areas)
    {
        for (const auto& a : areas)
        {
            for (const auto& e : m_Events)
            {
                const bool result = boost::apply_visitor(Checker(a), e);
                if (result)
                    return true;
            }
        }
        return false;
    }

    virtual void OnTableSize(int width, int height)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnRegionRecognized(const cv::Rect& rect, const std::wstring& text)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnButtonFound(const cv::Rect& rect)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnPlayerState(const cv::Rect& rect, bool isInGame)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnPlayerBet(const cv::Rect& player, unsigned bet)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void Serialize(cmn::xml::Node& node) const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual std::string GetNodeName() const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual bool IsEmpty() const
    {
        return m_Events.empty();
    }

private:
    mutable std::list<Event> m_Events;
};


#pragma warning(pop)


} // anonymous namespace


ITableEventsReader::Ptr ITableEventsReader::Instance()
{
    return boost::make_shared<EventsReader>();
}

ITableEvents::Ptr ITableEvents::Instance()
{
    return boost::make_shared<EventsWriter>();
}

} // namespace clnt