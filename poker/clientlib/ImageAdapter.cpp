﻿#include "ImageAdapter.h"
#include "ImageManager.h"
#include "TableParser.h"

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <mutex>

#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>

namespace clnt
{
namespace
{

class ImageAdapter : public IImageAdapter
{
    struct RectCompare
    {
        bool operator () (const cv::Rect& lhs, const cv::Rect& rhs) const
        {
            if (lhs.x == rhs.x)
                return lhs.y < rhs.y;
            return lhs.x < rhs.x;
        }
    };

    typedef std::map<cv::Rect, std::wstring, RectCompare> TextMap;
public:
    ImageAdapter(ILog& log, rec::IRecognizer& recognizer, ITableParser& parser, const cv::Mat& button, const cv::Mat& inGameSign)
        : m_Log(log) 
        , m_Recognizer(recognizer)
        , m_Manager(IImageManager::Instance(log, recognizer))
        , m_Button(button)
        , m_InGameSign(inGameSign)
        , m_PlayersArea()
        , m_InGameSignIntensity(cv::mean(inGameSign))
        , m_Parser(parser)
    {
    }

    virtual void Initialize(const Params& params) override
    {
        m_InGameStates  = params.m_InGameSigns;
        m_Areas         = params.m_AllAreas;
        m_PlayersArea   = params.m_PlayersArea;
        m_PlayerAreas   = params.m_PlayerAreas;
        m_CardsArea     = params.m_CardsArea;

        m_Parser.SetPlayersArea(m_PlayersArea);

        Areas all;
        all.reserve(m_Areas.size() + m_InGameStates.size());
        boost::copy(m_Areas, std::back_inserter(all));
        boost::copy(m_InGameStates, std::back_inserter(all));

        m_Manager->SetRegions(all);
    }

    virtual ITableEvents::Ptr Parse(const cv::Mat& image) override
    {
        m_Events = ITableEvents::Instance();
        if (image.size().area() < m_PlayersArea.area())
            return m_Events; // wrong image, do not try to process it, this may happen due to window capture error

        std::call_once(m_TableSizeFlag, [this, &image](){ m_Events->OnTableSize(image.size().width, image.size().height); });

        const auto diffs = m_Manager->GetChanges(image);
        boost::for_each(diffs, boost::bind(&ImageAdapter::ProcessDiff, this, _1));

        cv::Rect button = m_Recognizer.FindObject(image(m_PlayersArea), m_Button);
        if (button != cv::Rect())
        {
            button.x += m_PlayersArea.x;
            button.y += m_PlayersArea.y;
        }
        if (button != cv::Rect() && m_ButtonArea != button)
        {
            if (m_ButtonArea != cv::Rect())
                m_Events->OnRegionRecognized(m_ButtonArea, std::wstring());
            m_ButtonArea = button;

            m_Events->OnButtonFound(button);
        }

        std::call_once(m_ParseBetsFlag, [this, &image](){
            boost::for_each(m_PlayerAreas, boost::bind(&ImageAdapter::ProcessBet, this, boost::ref(image), _1));
        });

        return m_Events;
    }

    void ProcessDiff(const IImageManager::Diff& diff)
    {
        // find where is diff located
        const auto it = boost::range::find(m_InGameStates, diff.m_Area);
        if (it == m_InGameStates.end())
        {
            const auto text = m_Recognizer.RecognizeText(diff.m_Image);
            if (!text.empty())
            {
                auto& previous = m_Texts[diff.m_Area];
                if (previous != text)
                {
                    previous = text;
                    m_Events->OnRegionRecognized(diff.m_Area, text);
                }
            }
            else
            {
                m_Texts.erase(diff.m_Area);
            }
        }
        else
        {
            m_Events->OnPlayerState(diff.m_Area, IsSimilar2CardsBack(diff.m_Image));
        }
    }

    void ProcessBet(const cv::Mat& image, const Polygon& playerArea)
    {
        // get image without cards area
        cv::Mat region = image.clone();

        // fill cards area with zeros to ensure that this area will not be parsed
        cv::fillConvexPoly(region, m_CardsArea, cv::Scalar::all(0));

        // fill all regions with zero
        boost::for_each(m_Areas, boost::bind(&cv::rectangle, region, _1, cv::Scalar::all(0), -1, cv::LINE_8, 0));

        const cv::Rect rect = cv::boundingRect(playerArea);
        region = region(rect);
        if (region.channels() == 4)
            cv::cvtColor(region, region, cv::COLOR_BGRA2BGR);

        cv::Mat cropped(region.rows, region.cols, CV_8UC3);

        {
            cv::Mat mask = cv::Mat::zeros(region.size(), CV_8UC3);

            Polygon modifiedPolygon;

            // shift polygon to rect offset
            boost::transform(playerArea, std::back_inserter(modifiedPolygon), [&rect](const cv::Point& p){
                return cv::Point(p.x - rect.x, p.y - rect.y);
            });

            cv::fillConvexPoly(mask, modifiedPolygon, cv::Scalar::all(255));
            region.copyTo(cropped, mask);
        }


        cv::Point center(m_PlayersArea.x + m_PlayersArea.width / 2, m_PlayersArea.y + m_PlayersArea.height / 2);

        // minus cropped area coordinates
        center.x -= rect.x;
        center.y -= rect.y;

        const auto result = m_Recognizer.Recognize(cropped);
        const auto bet = m_Parser.ObtainBet(result, center);

        if (bet)
            m_Events->OnPlayerBet(rect, bet);
    }

    bool IsSimilar2CardsBack(const cv::Mat& image)
    {
        // определить средний цвет, сравнить со средним цветом рубашки карт
        const cv::Scalar intensity = cv::mean(image);
        return abs(intensity.val[0] - m_InGameSignIntensity.val[0]) < 20.0f &&
            abs(intensity.val[1] - m_InGameSignIntensity.val[1]) < 20.0f &&
            abs(intensity.val[2] - m_InGameSignIntensity.val[2]) < 20.0f;
    }

private:
    ILog& m_Log;
    rec::IRecognizer& m_Recognizer;
    cv::Rect m_PlayersArea;
    Areas m_Areas;
    Areas m_InGameStates;
    Polygons m_PlayerAreas;
    Polygon m_CardsArea;
    IImageManager::Ptr m_Manager;
    ITableEvents::Ptr m_Events;
    const cv::Mat m_Button;
    const cv::Mat m_InGameSign;
    const cv::Scalar m_InGameSignIntensity;
    ITableParser& m_Parser;
    cv::Rect m_ButtonArea;
    std::once_flag m_TableSizeFlag;
    std::once_flag m_ParseBetsFlag;
    TextMap m_Texts;

};

} // anonymous namespace


IImageAdapter::Ptr IImageAdapter::Instance(ILog& log, rec::IRecognizer& recognizer, ITableParser& parser, const cv::Mat& button, const cv::Mat& inGameSign)
{
    return boost::make_shared<ImageAdapter>(log, recognizer, parser, button, inGameSign);
}

} // namespace clnt