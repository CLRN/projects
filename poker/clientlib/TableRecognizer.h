#ifndef TableRecognizer_h__
#define TableRecognizer_h__

#include "recognition/Recognition.h"
#include "common/ILog.h"
#include "Cards.h"
#include "Actions.h"
#include "ITableLogic.h"
#include "serialization/ISerializable.h"
#include "rpc/Endpoint.h"
#include "ImageManager.h"
#include "ImageAdapter.h"

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

namespace clnt
{

    class ITableControl;
class ITableRecognizer : public virtual cmn::srl::ISerializable
{
public:
    typedef boost::shared_ptr<ITableRecognizer> Ptr;

    __declspec(novtable) struct ICallback
    {
        typedef boost::function<bool()> ReadyFunc;
        virtual void Wait4NewGame(const ReadyFunc& func) = 0;
        virtual void OnNewGameReady() = 0;
        virtual void OnNextEventsNeeded(const IImageManager::Event::List& evnt) = 0;
    };

    virtual ~ITableRecognizer() {}
    virtual void OnTableSize(int width, int height) = 0;
    virtual void OnPlayerChanged(const cv::Rect& area, const std::string& name, unsigned stack) = 0;
    virtual void OnPlayerAction(const cv::Rect& area, pcmn::Action::Value action, unsigned stack) = 0;
    virtual void OnPlayerAfk(const cv::Rect& area, const std::string& name) = 0;
    virtual void OnCardChanged(const cv::Rect& area, const pcmn::Card& card) = 0;
    virtual void OnPotChanged(const cv::Rect& area, unsigned amount) = 0;
    virtual void OnBetChanged(const cv::Rect& area, unsigned amount) = 0;
    virtual void OnDealerButtonChanged(const cv::Rect& area) = 0;
    virtual void OnActionButtonChanged(const cv::Rect& area, pcmn::Action::Value action, unsigned amount) = 0;
    virtual void OnPlayerState(const cv::Rect& area, bool isInGame) = 0;
    virtual void Flush() = 0;
    virtual void SetTableControl(ITableControl& ctrl) = 0;

    virtual IImageAdapter::Params GetAllAreas() const = 0;

    static Ptr Instance(ILog& log, rpc::Endpoint& channel, ICallback& callback);
};

} // namespace clnt

#endif // TableRecognizer_h__
