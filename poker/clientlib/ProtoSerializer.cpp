﻿#include "ProtoSerializer.h"
#include "exception/CheckHelpers.h"
#include "common/ProtoHelpers.h"
#include "Player.h"
#include "Table.h"

#include <boost/make_shared.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>

namespace conv
{

class Xml2ProtoConverter : public IXml2Proto
{
public:

    pcmn::Card::List GetCards(const cmn::xml::Node& node) const
    {
        pcmn::Card::List result;

        boost::transform(node, std::back_inserter(result), [](const cmn::xml::Node& card){
            pcmn::Card result;
            result.m_Value = card.Get<pcmn::Card::Value>("item[@name='value']");
            result.m_Suit = card.Get<pcmn::Suit::Value>("item[@name='suit']");
            return result;
        });

        return result;
    }

    virtual net::Packet Convert(const cmn::xml::Node& root) const
    {
        net::Packet result;

        // get first round
        const auto firstRound = root.Get<cmn::xml::Node::Ptr>("//round");
        CHECK(firstRound, cmn::Exception("Game without rounds"));

        // set up start phase
        const auto phase = firstRound->Get<pcmn::Table::Phase::Value>("item[@name='phase']");
        result.mutable_info()->set_phase(phase);

        // make players map
        const auto playersRoot = firstRound->Get<cmn::xml::Node::Ptr>("field[@name='initial_players_data']");
        std::map<std::string, pcmn::Player> players;
        boost::transform(*playersRoot, std::inserter(players, players.end()), [this](const cmn::xml::Node& node){
            pcmn::Player player;
            player.Deserialize(node);
            return std::make_pair(player.Name(), player);
        });
            
        // get player sequence
        std::vector<std::string> sequence;
        const auto sequenceRoot = firstRound->Get<cmn::xml::Node::Ptr>("field[@name='players_sequence']");
        boost::transform(*sequenceRoot, proto::hlp::MakeProtoInserter(*result.mutable_info()->mutable_players()), [&](const cmn::xml::Node& node){
            net::Packet::Player result;

            const std::string name = node.Get<std::string>();
            sequence.push_back(name);

            result.set_name(name);
            result.set_stack(players[name].Stack());
            result.set_isactive(players[name].State() != pcmn::Player::State::Folded);
            for (const auto& card : players[name].Cards())
                result.add_cards(card.ToEvalFormat());

            return result;
        });

        // set up button
        const auto button = firstRound->Get<std::string>("item[@name='button']");
        const auto it = boost::find(sequence, button);
        result.mutable_info()->set_button(std::distance(sequence.begin(), it));

        // parse flop
        const auto lastRound = root.Get<cmn::xml::Node::Ptr>("//round[last()]");
        const auto flop = GetCards(*lastRound->Get<cmn::xml::Node::Ptr>("field[@name='flop']"));
        for (const auto& card : flop)
            result.mutable_info()->add_cards(card.ToEvalFormat());
       
        // parse actions
        boost::transform(root.Get<cmn::xml::Node::xpath>("//round"), proto::hlp::MakeProtoInserter(*result.mutable_phases()), [&](const cmn::xml::Node& round){
            net::Packet::Phase result;

            // get all actions for this round
            const auto actions = round.Get<cmn::xml::Node::Ptr>("field[@name='actions']");
            boost::transform(*actions, proto::hlp::MakeProtoInserter(*result.mutable_actions()), [&](const cmn::xml::Node& action){
                net::Packet::Action result;

                const std::string name = action.Get<std::string>("item[@name='name']");
                const auto it = boost::find(sequence, name);

                result.set_player(std::distance(sequence.begin(), it));
                result.set_id(action.Get<unsigned>("item[@name='action']"));
                result.set_amount(action.Get<unsigned>("item[@name='amount']"));

                return result;
            });

            return result;
        });

        return result;
    }

};

IXml2Proto::Ptr IXml2Proto::Instance()
{
    return boost::make_shared<Xml2ProtoConverter>();
}

} // namespace conv