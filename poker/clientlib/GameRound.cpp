﻿#include "GameRound.h"
#include "Player.h"
#include "common/Modules.h"
#include "xml/Node.h"
#include "serialization/Serialization.h"
#include "serialization/Helpers.h"
#include "TableDiff.h"

#include <map>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/scope_exit.hpp>
#include <boost/range/adaptors.hpp>

namespace clnt
{

template<typename T, typename Enable = void>
struct Comparer
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        CHECK(lhs == rhs, cmn::Exception("Members are not equal"));
        return true;
    }
};

template<typename T>
struct Comparer<boost::shared_ptr<T>, void>
{
    static bool Compare(const boost::shared_ptr<T>& lhs, const boost::shared_ptr<T>& rhs) 
    {
        return Comparer<T>::Compare(*lhs, *rhs);
    }
};

template<typename T>
struct Comparer<T, typename static boost::enable_if<cmn::meta::IsIterable<T>>::type>
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        return boost::equal(lhs, rhs, boost::bind(&Comparer<typename T::value_type>::Compare, _1, _2));
    }
};

template<typename T>
struct Comparer<T, typename static boost::enable_if<boost::is_same<cmn::srl::ISerializable, T>>::type>
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        return Comparer<Object>::Compare(dynamic_cast<const Object&>(lhs), dynamic_cast<const Object&>(rhs));
    }
};

template<typename T>
void CompareObjects(const T& lhs, const T& rhs)
{
    CHECK(Comparer<T>::Compare(lhs, rhs), cmn::Exception("Members are not equal"));
}

DECLARE_CURRENT_MODULE(Modules::Logic);

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

class GameRound : public IGameRound, public cmn::srl::Serializable
{
public:
    struct PlayerAction : public Action, public cmn::srl::Serializable
    {
        typedef boost::shared_ptr<PlayerAction> Ptr;
        typedef std::list<Ptr> List;
        PlayerAction(const std::string& name = "", pcmn::Action::Value v = pcmn::Action::Unknown, unsigned amount = 0, unsigned stackAfter = 0)
            : Action(name, v, amount, stackAfter)
            , SERIALIZABLE("action",
                FIELD(m_Name)
                FIELD(m_Action)
                FIELD(m_Amount)
                FIELD(m_StackAfter)
            )
        {
        }

        bool operator == (const PlayerAction& other) const
        {
            return m_Name == other.m_Name && m_Action == other.m_Action && m_Amount == other.m_Amount && m_StackAfter == other.m_StackAfter;
        }
    };

    typedef std::map<std::string, pcmn::Player> PlayerMap;

    GameRound(ILog& log, ICallback& cb)
        : SERIALIZABLE("round",
            FIELD(m_Button)
            FIELD(m_PlayersSequence)
            FIELD(m_Flop)
            FIELD(m_Actions)
            FIELD(m_InitialPlayersData)
            FIELD(m_Phase)
            FIELD(m_AllInPlayers)
        )
        , m_Log(log)
        , m_BigBlind()
        , m_Phase()
        , m_Callback(cb)
        , m_LastBet()
        , m_LastAction()
        , m_CurrentSnapshot(ITableSnapshot::Instance(m_Log))
        , m_IsStreetInitialized()
        , m_IsQueueInitialized()
    {
    }

private:

    virtual void Compare(const IGameRound& other) const override
    {
        const GameRound& that = dynamic_cast<const GameRound&>(other);

        CompareObjects(m_Button, that.m_Button);
        CompareObjects(m_PlayersSequence, that.m_PlayersSequence);
        CompareObjects(m_Flop, that.m_Flop);
        CompareObjects(m_Actions, that.m_Actions);
        CompareObjects(m_InitialPlayersData, that.m_InitialPlayersData);
    }


    virtual void SetPlayerCards(const std::string& name, const pcmn::Card::List& cards) override
    {
        m_InitialPlayersData[name].Cards(cards);
    }

    virtual void Initialize(ITableSnapshot& snap) override
    {
        if (m_IsStreetInitialized)
            return;

        // двухэтапная иницилизация, этот метод вызывается сразу после создания раунда, 
        // суть в том чтобы карты с флопа и улицу запоммнать сразу, а вот все остальное(очереь игроков и т.п.)
        // вычислять чем позже, тем лучше, т.к. чем позже момент иницализации тем более полные данные у нас есть
        // в то же время улицу надо запомнить сразу, т.к. некоторые действия(последнее дествие на улице)
        // приходит одновременно с новыми картами, и если инициалзировать улицу исходя из карт в момент последнего действия
        // то реалная улица будет впереди на 1(т.к. уже пришли карты со следующей улицы)

        m_CurrentSnapshot->Copy(snap);
        m_Phase = m_CurrentSnapshot->GetStreet();
        m_Flop.clear();
        boost::copy(m_CurrentSnapshot->GetFlop(), std::back_inserter(m_Flop));
        m_IsStreetInitialized = true;

        LOG_TRACE("Round initialized with street: '%s'") % pcmn::Table::Phase::ToString(m_Phase);
    }

    void CheckInitialized(ITableSnapshot& snap)
    {
        if (m_IsQueueInitialized)
            return;

        Initialize(snap);

        m_PlayersSequence.clear();

        const auto players = m_CurrentSnapshot->GetPlayers();
        boost::transform(players, std::back_inserter(m_PlayersSequence), [this](const pcmn::Player& p){
            m_InitialPlayersData[p.Name()] = p;
            return p.Name();
        });

        m_BigBlind = m_CurrentSnapshot->GetBigBlind();
        m_Button = m_CurrentSnapshot->GetButton();

        // transform player bets to stack sizes
        const auto betToStack = [](PlayerMap::value_type& pair){
            pair.second.Stack(pair.second.Stack() + pair.second.Bet());
            pair.second.Bet(0);
        };
        boost::for_each(m_InitialPlayersData, betToStack);

        Prepare();

        LOG_TRACE("Round initialization, queue: [%s], button: [%s], street: [%s], blind: [%s]") % m_Queue % m_Button % pcmn::Table::Phase::ToString(m_Phase) % m_BigBlind;

        // check for existing actions which must be played now
        const auto empty = ITableSnapshot::Instance(m_Log);
        m_Diff = snap.GetDiff(*empty);

        if (m_Diff->HasActions())
            PlayDiff(snap);

        m_IsQueueInitialized = true;
    }

    const std::string& GetNextPlayerName(const std::string& name) const
    {
        auto it = boost::find(m_PlayersSequence, name);
        CHECK(it != m_PlayersSequence.end(), cmn::Exception("Failed to find player in sequence"), name, m_PlayersSequence);

        for (;;)
        {
            ++it;
            if (it == m_PlayersSequence.end())
                it = m_PlayersSequence.begin();

            if (boost::find(m_Queue, *it) != m_Queue.end())
                return *it;
        }
        THROW(cmn::Exception("Failed to find nex player"), name, m_Queue, m_PlayersSequence);
    }

    void PostBlinds(const std::string& sb, const std::string& bb)
    {
        if (!ObtainBigBlind(bb))
            return;

        PlayerAction::List actions;
        const auto players = m_CurrentSnapshot->GetPlayers();
        {
            const auto player = boost::find(players, sb);
            CHECK(player != players.end(), cmn::Exception("Failed to find player in snapshot"), sb);

            const auto diff = player->Stack() > m_BigBlind / 2 ? m_BigBlind / 2 : m_BigBlind / 2 - player->Stack();
            const auto action = boost::make_shared<PlayerAction>(sb, pcmn::Action::SmallBlind, diff, player->Stack());
            const auto it = boost::find_if(m_Actions, boost::bind(&PlayerAction::operator==, _1, boost::ref(*action)));
            if (diff && it == m_Actions.end())
                actions.push_back(action);
        }
        {
            const auto player = boost::find(players, bb);
            CHECK(player != players.end(), cmn::Exception("Failed to find player in snapshot"), bb);

            const auto diff = player->Stack() > m_BigBlind ? m_BigBlind : m_BigBlind - player->Stack();
            const auto action = boost::make_shared<PlayerAction>(bb, pcmn::Action::BigBlind, diff, player->Stack());
            const auto it = boost::find_if(m_Actions, boost::bind(&PlayerAction::operator==, _1, boost::ref(*action)));
            if (diff && it == m_Actions.end())
                actions.push_back(action);
        }
        PlayAllActions(actions, *m_CurrentSnapshot);
    }

    bool CanPlayerPlay(const pcmn::Player& p)
    {
        return !m_AllInPlayers.count(p.Name()) && 
            (p.State() == pcmn::Player::State::Waiting || 
            m_Phase == pcmn::Table::Phase::Preflop && p.State() == pcmn::Player::State::Folded && p.Bet() && !m_IsQueueInitialized) && // folded player on preflop with bet and this is initial queue
            (p.Bet() + p.Stack());
    }

    void MakePlayerQueue() 
    {
        m_Queue.clear();
        const auto players = m_CurrentSnapshot->GetPlayers();

        const PlayerQueue& sequence = m_PlayersSequence;
        CHECK(boost::find(sequence, m_Button) != sequence.end(), cmn::Exception("Failed to find player in sequence"), m_Button, sequence);

        // add players to queue
        bool found = false;
        for (std::size_t i = 0; ; ++i)
        {
            if (i == sequence.size())
                i = 0;

            const auto player = boost::find(players, sequence[i]);
            CHECK(player != players.end(), cmn::Exception("Failed to find player in snapshot"), sequence[i]);
            const pcmn::Player& current = *player;

            if (!found && current.Name() == m_Button)
                found = true;
            else
            if (found && current.Name() == m_Button)
            {
                if (CanPlayerPlay(current))
                    m_Queue.push_back(current.Name());
                break;
            }
            else
            if (found && CanPlayerPlay(current))
            {
                m_Queue.push_back(current.Name());
            }
        }

        if (m_Phase == pcmn::Table::Phase::Preflop && !m_Queue.empty())
        {
            if (m_Queue.size() > 2)
            {
                // add players on blinds
                const std::string& smallBlind = GetNextPlayerName(m_Button);
                const std::string& bigBlind = GetNextPlayerName(smallBlind);

                m_Queue.push_back(smallBlind);
                m_Queue.push_back(bigBlind);

                PostBlinds(smallBlind, bigBlind);
            }
            else
            {
                m_Queue.push_back(m_Queue.front());
                m_Queue.pop_front();
                m_Queue.push_back(m_Button);
                const std::string& next = GetNextPlayerName(m_Button);
                m_Queue.push_back(next);

                PostBlinds(m_Queue[0], m_Queue[1]);
            }
        }
    }

    void Prepare()
    {
        CHECK(!m_Button.empty(), cmn::Exception("Empty button"));

        m_LastBet = (m_Phase == pcmn::Table::Phase::Preflop) ? m_BigBlind : 0;
        m_LastAction = (m_Phase == pcmn::Table::Phase::Preflop) ? pcmn::Action::BigBlind : pcmn::Action::Check;

        MakePlayerQueue();
    }

    bool ObtainBigBlind(const std::string& name)
    {
        if (m_BigBlind)
            return true;

        const auto it = boost::find_if
        (
            m_Actions, 
            boost::bind(&PlayerAction::m_Name, _1) == name 
            && 
            (
                boost::bind(&PlayerAction::m_Action, _1) == pcmn::Action::BigBlind 
                || 
                boost::bind(&PlayerAction::m_Action, _1) == pcmn::Action::Unknown
            )
        );

        if (it == m_Actions.end())
        {
            m_BigBlind = m_Callback.ObtainBet(name);
            return !!m_BigBlind;
        }

        m_BigBlind = (*it)->m_Amount;
        return true;
    }

    void ProcessUnknownAction(const pcmn::Player& currentPlayer, pcmn::Action::Value& currentAction, unsigned& currentAmount)
    {
        try 
        {
            if (currentPlayer.Afk())
            {
                currentAction = pcmn::Action::Fold;
                currentAmount = 0;
                return;
            }

	        // ok, it's a hard part, player action not exists(not parsed, missed or something...)
	        // so we need to determine what action he was made
            const unsigned bet = currentPlayer.Bet();
            if (!bet)
	        {
	            if (m_LastBet)
	            {
	                // if player doesn't have a bet it was fold
	                currentAction = pcmn::Action::Fold;
	            }
	            else
	            {
	                // there is not bet, that was check
	                currentAction = pcmn::Action::Check;
	            }
	        }
	        else
	        {
	            // player done something, it may be call, bet or raise, let's find out
	            if (m_LastAction == pcmn::Action::Check)
	            {
	                // it was bet
	                currentAction = pcmn::Action::Bet;
                    currentAmount = bet;
	            }
	            else 
                if (bet == m_LastBet)
	            {
	                // it was call
	                currentAction = pcmn::Action::Call;
	                currentAmount = m_LastBet;
	            }
	            else
                if (bet > m_LastBet)
	            {
	                // only raise possible
	                currentAction = pcmn::Action::Raise;
                    currentAmount = bet;
	            }
                else
                {
                    currentAction = pcmn::Action::Fold;
                    currentAmount = 0;
                }
	        }
	
            LOG_TRACE("Pushing unknown action: [%s]:[%s], amount: %s, stack after: %s, queue: %s") % currentPlayer.Name() % pcmn::Action::ToString(currentAction) % currentAmount % currentPlayer.Stack() % m_Queue;
        }
        CATCH_PASS(cmn::Exception("Failed to process unknown action"), currentPlayer.Bet(), m_Queue)
    }
    
    void ProcessExistingAction(const std::string& currentName, const PlayerAction& action, pcmn::Action::Value& currentAction, unsigned& currentAmount)
    {
        try 
        {
            currentAmount = action.m_Amount;
            currentAction = action.m_Action;

            LOG_TRACE("Pushing existing action: [%s]:[%s], amount: %s, stack after: %s, queue: %s") % currentName % pcmn::Action::ToString(currentAction) % currentAmount % action.m_StackAfter % m_Queue;

	        if (!action.m_StackAfter)
	            m_AllInPlayers.insert(currentName);
        }
        CATCH_PASS(cmn::Exception("Failed to process existing action"), action.m_Name, action.m_Action, action.m_Amount)
    }

    void ProcessPostAction(
        pcmn::Player& currentPlayer, 
        pcmn::Action::Value currentAction, 
        unsigned currentAmount, 
        unsigned stackAfter,
        const bool unknown,
        ITableSnapshot& snap)
    {
        if (currentAction == pcmn::Action::Fold)
            m_Callback.OnPlayerFolded(currentPlayer.Name());

        const int diff = currentAmount - currentPlayer.Bet();
        if (diff > 0)
            currentPlayer.Bet(currentPlayer.Bet() + diff);

        // correct initial state, player cannot be in 'Fold' state and make actions
        // actually players in 'Fold' state appears when round parsing starts at the middle
        if (m_InitialPlayersData[currentPlayer.Name()].State() == pcmn::Player::State::Folded)
            m_InitialPlayersData[currentPlayer.Name()].State(pcmn::Player::State::Waiting);

        m_Actions.push_back(boost::make_shared<PlayerAction>(currentPlayer.Name(), currentAction, currentPlayer.Bet(), stackAfter));

        if (currentAmount > m_LastBet)
            m_LastBet = currentPlayer.Bet();

        if (currentAction == pcmn::Action::SmallBlind || currentAction == pcmn::Action::BigBlind)
            return;

        // decrease player stack, but don't do it for blinds! because blinds are 
        // automatically made by server and player stack value equal to stack amount after blinds
        if (diff > 0)
        {
            assert(currentPlayer.Stack() >= static_cast<unsigned>(diff));
            currentPlayer.Stack(currentPlayer.Stack() - diff);
        }
        if (!unknown && currentPlayer.Stack() != stackAfter)
        {
            LOG_WARNING("Correcting player '%s' stack from %s to %s") % currentPlayer.Name() % currentPlayer.Stack() % stackAfter;
            currentPlayer.Stack(stackAfter);
        }

        currentPlayer.State(currentAction == pcmn::Action::Fold ? pcmn::Player::State::Folded : pcmn::Player::State::Called);
        if (currentAction > m_LastAction)
            m_LastAction = currentAction;

        if (currentAction == pcmn::Action::Bet || currentAction == pcmn::Action::Raise)
            ProcessRaise(currentPlayer.Name(), snap);
    }

    void ProcessRaise(const std::string& current, ITableSnapshot& snap)
    {
        bool found = false;
        CHECK(boost::find(m_PlayersSequence, current) != m_PlayersSequence.end(), cmn::Exception("Failed to find player in sequence"), current, m_PlayersSequence);
        const auto players = snap.GetPlayers();

        for (std::size_t i = 0; ; ++i)
        {
            if (i == m_PlayersSequence.size())
                i = 0;

            if (!found && m_PlayersSequence[i] == current)
                found = true;
            else
            if (found && m_PlayersSequence[i] == current)
                break;
            else
            if (found)
            {
                const auto player = boost::find(players, m_PlayersSequence[i]);
                CHECK(player != players.end(), cmn::Exception("Failed to find player by name"), m_PlayersSequence[i]);
                const pcmn::Player& currentPlayer = *player;

                const bool alreadyInQueue = boost::find(m_Queue, currentPlayer.Name()) != m_Queue.end();
                if (!alreadyInQueue && currentPlayer.State() != pcmn::Player::State::Folded && currentPlayer.Stack() && !m_AllInPlayers.count(currentPlayer.Name()))
                {
                    pcmn::Player copy(currentPlayer);
                    copy.State(pcmn::Player::State::Waiting);
                    snap.SetPlayer(copy);
                    m_Queue.push_back(currentPlayer.Name());
                }
            }
        }

        LOG_TRACE("Player '%s' raise processed, queue: %s") % current % m_Queue;
    }

    virtual pcmn::Table::Phase::Value GetStreet() const override
    {
        return m_Phase;
    }

    virtual const PlayerQueue& GetQueue(ITableSnapshot& snap) override
    {
        CheckInitialized(snap);
        return m_Queue;
    }

    virtual void Play(ITableSnapshot& snap)
    {
        const auto players = snap.GetPlayers();
        const auto range = players | boost::adaptors::filtered([](const pcmn::Player& p){ return !p.Cards().empty(); });
        boost::for_each(range, [this](const pcmn::Player& p){ m_InitialPlayersData[p.Name()].Cards(p.Cards()); });

        CheckInitialized(snap);
        if (m_Queue.empty())
            return; // nothing to process

        m_Diff = snap.GetDiff(*m_CurrentSnapshot);
        if (m_Diff->Empty())
            return; // nothing to play

        m_Flop.clear();
        boost::copy(snap.GetFlop(), std::back_inserter(m_Flop));

        PlayDiff(snap);
        m_CurrentSnapshot->Copy(snap);
    }

    void PlayDiff(ITableSnapshot& snap)
    {
        LOG_TRACE("Processing: %s") % m_Diff->GetLogInfo();

        PlayerAction::List actions;
        ExtractActions(actions);
        CheckIfPlayersFolded(actions);
        CheckIfBlindsMissing(actions);
        CheckIfBlindsDuplicated(actions);
        PlayAllActions(actions, snap, !m_Diff->GetNewButton().empty());
    }

    void CheckIfBlindsDuplicated(PlayerAction::List& actions)
    {
        for (auto it = actions.begin(), end = actions.end(); it != end;)
        {
            if ((*it)->m_Action == pcmn::Action::SmallBlind || (*it)->m_Action == pcmn::Action::BigBlind)
            {
                // workaround: some blinds may be duplicated by the server
                LOG_WARNING("Ignoring unknown blind action, player: [%s], action: [%s], queue: %s") % (*it)->m_Name % pcmn::Action::ToString((*it)->m_Action) % m_Queue;
                it = actions.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

    void ExtractActions(PlayerAction::List& actions)
    {
        std::string name;
        unsigned bet = 0;
        pcmn::Action::Value action = pcmn::Action::Unknown;
        unsigned stack = 0;
        while (m_Diff->GetAction(name, bet, action, stack))
        {
            const auto actionData = boost::make_shared<PlayerAction>(name, action, bet, stack);
            if (boost::find_if(m_Actions, boost::bind(&PlayerAction::operator==, _1, boost::ref(*actionData))) != m_Actions.end()) // do not process duplicated actions
            {
                LOG_TRACE("Ignoring duplicated action, player: [%s], action: [%s], queue: %s") % name % pcmn::Action::ToString(action) % m_Queue;
            }
            else
            {
                actions.push_back(actionData);
            }
        }
    }

    void CheckIfPlayersFolded(PlayerAction::List& actions)
    {
        // check for player state differences
        for (const auto& playerName : m_Queue)
        {
            const auto* player = m_Diff->GetPlayer(playerName);
            if (player && player->State() == pcmn::Player::State::Folded)
            {
                // we found player which folded in this diff, normally this player should 
                // have 'Fold' action, but in some cases it may absent, so add it if not exist
                const auto condition =
                    boost::bind(&PlayerAction::m_Action, _1) == pcmn::Action::Fold &&
                    boost::bind(&PlayerAction::m_Name, _1) == playerName;

                if (boost::find_if(actions, condition) != actions.end())
                    continue; // found fold for this player

                // make fold action
                actions.emplace_back(boost::make_shared<PlayerAction>(playerName, pcmn::Action::Fold, 0, player->Stack()));
            }
        }
    }

    void CheckIfBlindsMissing(const PlayerAction::List& actions)
    {
        if (m_Phase != pcmn::Table::Phase::Preflop)
            return;

        if (!m_Actions.empty())
            return; // if actions exists skip check

        if (m_BigBlind)
        {
            // just restore blinds with whole queue
            MakePlayerQueue();
        }

        if (actions.empty())
            return; // nothing to do here, can't correct situation without actions

        // ok, let's try to detect big blind amount from player bets
        std::set<unsigned> bets;
        boost::transform(actions | boost::adaptors::filtered(boost::bind(&PlayerAction::m_Amount, _1) != 0), std::inserter(bets, bets.end()), boost::bind(&PlayerAction::m_Amount, _1));

        if (bets.empty())
            return; // all actions without bets

        m_BigBlind = *bets.begin();

        LOG_WARNING("Big blind restored from actions: %s") % m_BigBlind;

        MakePlayerQueue();
    }

    void PlayAllActions(PlayerAction::List actions, ITableSnapshot& snap, bool gameFinished = false)
    {
        const auto players = snap.GetPlayers();
        for (; !m_Queue.empty() && (!actions.empty() || gameFinished); m_Queue.pop_front())
        {
            const auto player = boost::find(players, m_Queue.front());
            CHECK(player != players.end(), cmn::Exception("Failed to find player by name"), m_Queue.front());
            auto current = *player;
            BOOST_SCOPE_EXIT(&current, &snap){
                snap.SetPlayer(current);
            } BOOST_SCOPE_EXIT_END;

            pcmn::Action::Value currentAction = pcmn::Action::Unknown;
            unsigned currentAmount = 0;
            unsigned stackAfter = 0;

            // find action for this player
            const auto it = boost::find_if(actions, boost::bind(&PlayerAction::m_Name, _1) == current.Name());
            const bool isUnknown(it == actions.end() || (*it)->m_Action == pcmn::Action::Unknown);
            if (isUnknown)
            {
                if (it != actions.end())
                    actions.erase(it);

                // unable to find action for this player, process as unknown
                ProcessUnknownAction(current, currentAction, currentAmount);
                stackAfter = current.Stack();
            }
            else
            {
                BOOST_SCOPE_EXIT(&it, &actions){
                    actions.erase(it);
                } BOOST_SCOPE_EXIT_END;

                // process existing action
                const auto& action = **it;

                if (action.m_Action == pcmn::Action::Afk)
                {
                    current.Afk(true);
                    continue;
                }

                ProcessExistingAction(current.Name(), action, currentAction, currentAmount);
                stackAfter = action.m_StackAfter;
            }

            ProcessPostAction(current, currentAction, currentAmount, stackAfter, isUnknown, snap);
        }

        if (m_Queue.empty())
        {
            bool last = false;
            const auto players = snap.GetPlayers();

            const auto called = players | boost::adaptors::filtered([this](const pcmn::Player& player) {
                return player.State() == pcmn::Player::State::Called && player.Stack();
            });

            const auto active = boost::distance(called);
            LOG_TRACE("There are '%s' called players with stack, all in players: '%s'") % active % m_AllInPlayers; 
            if (active < 2 || m_Phase == pcmn::Table::Phase::River || gameFinished)
                last = true;

            LOG_TRACE("Street '%s' finished %s, actions remaining: '%s'") % pcmn::Table::Phase::ToString(m_Phase) % (last ? "that was last street" : "") % actions.size();
            Action::List remaining;
            boost::copy(actions, std::back_inserter(remaining));
            m_Callback.OnStreetFinished(last, remaining, !gameFinished);
        }
    }

private:
    ILog& m_Log;
    ICallback& m_Callback;

    PlayerQueue m_Queue;
    PlayerMap m_InitialPlayersData;
    PlayerQueue m_PlayersSequence;
    unsigned m_BigBlind;
    pcmn::srl::Card::List m_Flop;
    std::string m_Button;
    PlayerAction::List m_Actions;
    pcmn::Table::Phase::Value m_Phase;
    std::set<std::string> m_AllInPlayers;
    unsigned m_LastBet;
    pcmn::Action::Value m_LastAction;

    ITableSnapshot::Ptr m_CurrentSnapshot;
    IDiff::Ptr m_Diff;
    bool m_IsStreetInitialized;
    bool m_IsQueueInitialized;
};

#pragma warning(pop)

IGameRound::Ptr IGameRound::Instance(ILog& log, ICallback& cb)
{
    return boost::make_shared<GameRound>(log, cb);
}

} // namespace clnt
