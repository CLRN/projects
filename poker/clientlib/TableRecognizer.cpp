#include "TableRecognizer.h"
#include "recognition/RecognizableArea.h"
#include "Player.h"
#include "Table.h"
#include "exception/CheckHelpers.h"
#include "TableParser.h"
#include "GameRound.h"
#include "common/Modules.h"
#include "Game.h"
#include "CardsErrorInfo.h"
#include "serialization/Serialization.h"
#include "serialization/Helpers.h"
#include "ProtoSerializer.h"
#include "server.pb.h"
#include "ImageManager.h"
#include "RecognizeEvent.h"
#include "ITableControl.h"

#include <limits>
#include <mutex>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/operations.hpp>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/function.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/filesystem.hpp>
#include <boost/scope_exit.hpp>

#ifdef _DEBUG
#define VISUAL_DEBUG
#endif // _DEBUG

namespace cv
{
    template<typename _Tp> inline std::wostream& operator<<(std::wostream& out, const Rect_<_Tp>& rect)
    {
        out << L"[" << rect.width << L" x " << rect.height << L" from (" << rect.x << L", " << rect.y << L")]";
        return out;
    }
} // namespace cv

namespace clnt
{

DECLARE_CURRENT_MODULE(Modules::Client);

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

class TableRecognizer 
    : public ITableRecognizer
    , boost::noncopyable
    , public cmn::srl::Serializable
    , public boost::enable_shared_from_this<TableRecognizer>
    , public IGame::ICallback
{
public:
    typedef std::vector<cv::Point> Polygon;
    typedef std::vector<Polygon> PlayersAreas;

    static std::size_t InvalidIndex()
    {
        return std::numeric_limits<std::size_t>::max();
    }

    struct Search
    {
        enum Value
        {
            AreaOnly    = 0,
            Nearby      = 1
        };
    };

    struct State
    {
        enum Value
        {
            InProgress                  = 0,
            CollectingDataAfterFinish   = 1,
            NewRound                    = 2,
            ActionQueryInProgress       = 3
        };
    };

    struct PlayerAction
    {
        typedef std::vector<PlayerAction> List;

        bool operator == (const PlayerAction& other) const { return m_Player == other.m_Player && m_Action == other.m_Action && m_Bet == other.m_Bet && m_Stack == other.m_Stack;  }
        bool operator != (const PlayerAction& other) const { return !(*this == other); }

        unsigned m_Player;
        pcmn::Action::Value m_Action;
        unsigned m_Bet;
        unsigned m_Stack;
    };
    
    TableRecognizer(ILog& log, rpc::Endpoint& channel, ITableRecognizer::ICallback& callback)
        : SERIALIZABLE("recognizer", 
            FIELD(m_Players, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_Cards, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_Pot, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_FoldButton, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_CheckCallButton, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_BetRaiseButton, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
            FIELD(m_WindowWidth)
            FIELD(m_WindowHeight)
            FIELD(m_InGameSigns, boost::bind(&TableRecognizer::CreateArea, this, cv::Rect()))
        )
        , m_Log(log)
        , m_Channel(channel)
        , m_Callback(callback)
        , m_PotSize()
        , m_WindowWidth()
        , m_WindowHeight()
        , m_Game(IGame::Instance(m_Log, *this))
        , m_State(State::NewRound)
        , m_DelayedEvents(IEvents::Instance(m_Log, *this))
        , m_Control()
        , m_IsSomeoneBecameOnline()
    {
    }

private:

    void FlushData()
    {
        // flush cards here
        boost::for_each(m_FlopCards, boost::bind(&IGame::PushFlopCard, m_Game, _1));

        // flush actions here, because only at this moment player names resolved completely
        for (const auto& a : m_Actions)
        {
            m_PlayersData[a.m_Player].Stack(a.m_Stack);
            m_Game->SetPlayerParams(m_PlayersData[a.m_Player]);
            m_Game->PushAction(m_PlayersData[a.m_Player].Name(), a.m_Action, a.m_Bet, a.m_Stack);
        }
        m_Actions.clear();
    }

    void WaitForGameBegin()
    {
        if (m_IsSomeoneBecameOnline)
            return;

        LOG_INFO("Waiting for game begin");
        m_Callback.Wait4NewGame(boost::bind(&TableRecognizer::m_IsSomeoneBecameOnline, this));
        LOG_INFO("Game started");
    }

    virtual void Flush() override
    {
        std::call_once(m_InitFlag, boost::bind(&TableRecognizer::WaitForGameBegin, this));

        if (m_State == State::CollectingDataAfterFinish)
        {
            LOG_TRACE("Recognizer in state 'CollectingDataAfterFinish', ignoring flush");
            return;
        }

        if (m_State == State::ActionQueryInProgress)
            QueryAction();

        // check for any player with bets or actions and without name
        m_UndetectedPlayersCount = 0;
        for (unsigned index = 0; index < m_PlayersData.size(); ++index)
        {
            const auto& p = m_PlayersData[index];
            const auto action = boost::find_if(m_Actions, boost::bind(&PlayerAction::m_Player, _1) == index);
            if (p.Name() == pcmn::Player::Unknown().Name() && (p.Bet() || action != m_Actions.end()))
            {
                LOG_WARNING("Failed to detect player: %s") % p;
                ++m_UndetectedPlayersCount;
            }
        }

        LOG_TRACE("Flushing game, undetected players: %s") % m_UndetectedPlayersCount;
        if (m_UndetectedPlayersCount)
            return; // there is no point to flush this data, because it's not full

        FlushData();
        m_Game->Flush();
             
        if (m_State == State::InProgress)
        {
            // we should determine what event will be next and invoke callback
            const IImageManager::Event::List events = GetNextEvents();
            m_Callback.OnNextEventsNeeded(events);

            FlushData();
            m_Game->Flush();
        }
        else
        {
            m_State = State::InProgress;
        }
    }

    virtual void OnTableSize(int width, int height) override
    {
        m_WindowWidth = width;
        m_WindowHeight = height;

        LOG_TRACE("Table size: %s x %s") % width % height;
    }

    virtual void OnPlayerChanged(const cv::Rect& area, const std::string& name, unsigned stack) override
    {
        // ensure that player exists in the list of areas
        const auto player = GetArea(m_Players, area);
        const auto index = GetPlayerIndex(player);
        auto& data = m_PlayersData[index];

        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnPlayerChanged(area, data.Name(), stack);
            return;
        }

        int diff = data.Stack() - stack;
        data.Stack(stack);
        if (!data.HasName() || data.Name() == pcmn::Player::Unknown().Name())
        {
            LOG_TRACE("Player name changed: name: [%s], stack: [%s], diff: %s, area: %s, index: %s") % name % stack % diff % area % index;
            data.Name(name);

            assert(diff <= 0);
            SetPlayerList();
        }
        else
        {
            const auto& old = data.Name();
            const bool equal = old.find(name) != std::string::npos || name.find(old) != std::string::npos;
            if (!equal)
            {
                LOG_WARNING("Player name: [%s] is not equal to previous: [%s]") % name % old;
            }
        }

        if (diff)
        {
            if (diff > 0)
            {
                data.Bet(data.Bet() + diff);
                LOG_TRACE("Player bet changed: name: [%s], stack: [%s], diff: %s, bet: %s, area: %s, index: %s") % data.Name() % stack % diff % data.Bet() % area % index;
                if (!data.Stack()) // that was all in
                {
                    PlayerAction a{ index, pcmn::Action::Unknown, data.Bet(), 0 };
                    if (m_Actions.empty() || m_Actions.back() != a)
                        m_Actions.push_back(std::move(a));
                }
            }
            else
            {
                LOG_TRACE("Player params changed: name: [%s], stack: [%s], diff: %s, bet: %s, area: %s, index: %s") % data.Name() % stack % diff % data.Bet() % area % index;
            }
            m_Game->SetPlayerParams(data);
        }
    }

    virtual void OnPlayerAction(const cv::Rect& area, pcmn::Action::Value action, unsigned stack) override
    {
        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnPlayerAction(area, action, stack);
            return;
        }

        // ensure that player exists in the list of areas
        const auto player = GetArea(m_Players, area);
        const auto index = GetPlayerIndex(player);

        auto& current = m_PlayersData[index];
        const auto betBefore = current.Bet();
        const unsigned stackDiff = current.Stack() >= stack ? current.Stack() - stack : 0;
        const bool isActive = action != pcmn::Action::Fold && action != pcmn::Action::Check;
        if (isActive)
        {
            if (current.Stack() && current.Stack() < stack)
                current.Bet(0); // player won some money
            else
            if (stackDiff)
                current.Bet(current.Bet() + stackDiff);
        }

        LOG_TRACE("Player action: name: [%s], action: [%s], bet before: %s, bet after: %s, stack: %s, area: %s, index: %s") % current.Name() % pcmn::Action::ToString(action) % betBefore % current.Bet() % stack % area % index;

        current.Stack(stack);
        m_Game->SetPlayerParams(current);

        if (isActive && !stackDiff && !current.Bet())
        {
            LOG_WARNING("Can not process player '%s' active action '%s', because action amount is really unknown") % current.Name() % pcmn::Action::ToString(action);
            return;
        }

        if (!current.Bet() && (action == pcmn::Action::SmallBlind || action == pcmn::Action::BigBlind))
        {
            LOG_TRACE("Ignoring blind: name: [%s], action: [%s], bet: %s, stack: %s, area: %s, index: %s") % current.Name() % pcmn::Action::ToString(action) % current.Bet() % stack % area % index;
        }
        else
        {
            PlayerAction a{ index, action, isActive ? current.Bet() : 0, stack };
            if (m_Actions.empty() || m_Actions.back() != a)
                m_Actions.push_back(std::move(a));
        }
    }

    virtual void OnPlayerAfk(const cv::Rect& area, const std::string& name) override
    {
        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnPlayerAfk(area, name);
            return;
        }

        // ensure that player exists in the list of areas
        const auto player = GetArea(m_Players, area);
        const auto index = GetPlayerIndex(player);

        auto& current = m_PlayersData[index];

        LOG_TRACE("Player AFK: name: [%s], bet: %s, area: %s, index: %s") % current.Name() % current.Bet() % area % index;

        current.Afk(true);
        if (!current.Bet())
            current.State(pcmn::Player::State::Folded);
        m_Game->SetPlayerParams(current);

        const auto action = pcmn::Action::FromString(name);
        if (action != pcmn::Action::Unknown)
        {
            if (action == pcmn::Action::Fold)
            {
                PlayerAction a{ index, action, 0, current.Stack() };
                if (boost::find(m_Actions, a) == m_Actions.end()) // fold may be already in actions
                    m_Actions.push_back(a);
            }
        }
        else
        {
            LOG_TRACE("AFK player name changed: name: [%s], area: %s, index: %s") % name % area % index;
            m_PlayersData[index].Name(name);
            SetPlayerList();
        }
    }

    virtual void OnCardChanged(const cv::Rect& area, const pcmn::Card& card) override
    {
        try 
        {
            const auto cardArea = GetArea(m_Cards, area);
	
	        // check what kind of cards that is
            bool isFromFlop = false;
            const auto cardBottom = area.br();
            if (m_FlopCardsArea.width && m_FlopCardsArea.height)
            {
                // got complete area, simple check
	            isFromFlop = m_FlopCardsArea.contains(cardBottom);
            }
            else
            {
                // flop cards area not detected yet, compare by distance
                // find nearest player
                std::vector<double> distances;
                distances.reserve(m_Players.size());
                boost::transform(m_Players, std::back_inserter(distances), [&cardBottom](const rec::IRecognizableArea::Ptr& p){ 
                        return cv::norm(p->GetArea().tl() - cardBottom);
                    } 
                );
                boost::sort(distances);
                const auto toCenter = cv::norm(m_FlopCardsArea.tl() - cardBottom);
                isFromFlop = toCenter < distances.front();
            }

	        if (isFromFlop)
	        {
                LOG_TRACE("Flop card changed: [%s], area: %s") % card % area;

	            // this is a flop card
	            if (card.m_Value != pcmn::Card::Unknown && card.m_Suit != pcmn::Suit::Unknown)
	            {
                    if (boost::find(m_FlopCards, card) != m_FlopCards.end())
                        return; // cards duplicated

                    if (m_UndetectedPlayersCount)
	                    m_FlopCards.push_back(card);
                    else
	                    m_Game->PushFlopCard(card);
	            }
	        }
	        else
	        {
                const auto player = FindPlayerIndexByArea(cv::Rect(cardBottom, cardBottom), Search::Nearby);
	            if (player != InvalidIndex())
	            {
                    LOG_TRACE("Player [%s] card changed: [%s], area: %s") % m_PlayersData[player].Name() % card % area;

                    m_Game->PushPlayerCard(m_PlayersData[player].Name(), card);

                    auto cards = m_PlayersData[player].Cards();
	                cards.push_back(card);
	                m_PlayersData[player].Cards(cards);

                    if (cards.size() == 2 && pcmn::Player::ThisPlayer().Name() == m_PlayersData[player].Name())
                    {
                        LOG_TRACE("Bot '%s' is in game with cards: '%s'") % m_PlayersData[player].Name() % cards;
                        m_PlayersData[player].State(pcmn::Player::State::Waiting);
                        m_Game->SetPlayerParams(m_PlayersData[player]);
                    }
	            }
                else
                {
                    LOG_TRACE("Unknown player card changed: [%s], area: %s") % card % area;
                }
	        }
        }
        CATCH_PASS(cmn::Exception("Failed to handle card changes") << pcmn::CardErrorInfo(card))
    }

    virtual void OnPotChanged(const cv::Rect& area, unsigned amount) override
    {
        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnPotChanged(area, amount);
            return;
        }

        LOG_TRACE("Pot changed: amount: [%s], area: %s") % amount % area;

        if (!m_Pot)
            m_Pot = CreateArea(area);

        m_PotSize = amount;
    }

    virtual void OnBetChanged(const cv::Rect& area, unsigned amount) override
    {
        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnBetChanged(area, amount);
            return;
        }

        const cv::Point center(area.x + area.width / 2, area.y + area.height / 2);
        if (m_FlopCardsArea.contains(center))
            return;

        {
            // check if the bet is inside player name
            const auto it = boost::find_if(m_Players, boost::bind(&cv::Rect::contains, boost::bind(&rec::IRecognizableArea::GetArea, _1), center));
            if (it != m_Players.end())
                return;
        }

        const auto player = FindPlayerIndexByArea(area, Search::AreaOnly);
        if (player == InvalidIndex())
            return; // failed to determine which bet is it

        LOG_TRACE("Player bet changed: name: [%s], amount: [%s], area: %s, index: %s") % m_PlayersData[player].Name() % amount % area  % player;

        m_PlayersData[player].Bet(amount);
        m_Game->SetPlayerParams(m_PlayersData[player]);
    }

    virtual void OnPlayerState(const cv::Rect& area, bool isInGame) override
    {
        const auto player = FindPlayerIndexByArea(area, Search::AreaOnly);
        if (m_PlayersData[player].Name() == pcmn::Player::ThisPlayer().Name())
            isInGame = m_PlayersData[player].Cards().size() == 2;

        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnPlayerState(area, isInGame);
            return;
        }

        if (isInGame)
            m_IsSomeoneBecameOnline = true;

        if (!isInGame)
        {
            m_PlayersData[player].State(pcmn::Player::State::Folded);
        }
        else
        {
            if (m_State != State::InProgress)
                m_PlayersData[player].State(pcmn::Player::State::Waiting);
        }

        LOG_TRACE("Player '%s' %s") % m_PlayersData[player].Name() % (isInGame ? "in game" : "folded");

        m_Game->SetPlayerParams(m_PlayersData[player]);
    }

    virtual void OnDealerButtonChanged(const cv::Rect& area) override
    {
        try 
        {
            if (!m_DealerButton)
                m_DealerButton = CreateArea(area);

            m_DealerButton->SetArea(area);

            if (m_State == State::CollectingDataAfterFinish)
            {
                // this action from the next round, delay it
                m_DelayedEvents->OnDealerButtonChanged(area);
                return;
            }

            // find player on button
            const auto player = FindPlayerIndexByArea(area, Search::Nearby);
            CHECK(player != InvalidIndex(), cmn::Exception("Failed to find player on button"));
            m_Game->SetButton(m_PlayersData[player].Name());

            LOG_TRACE("Dealer changed: name: [%s], area: %s, index: %s") % m_PlayersData[player].Name() % area % player;
        }
        CATCH_PASS(cmn::Exception("Failed to process dealer button changes"))
    }

    virtual void OnActionButtonChanged(const cv::Rect& area, pcmn::Action::Value action, unsigned amount) override
    {
        if (m_State == State::ActionQueryInProgress)
            return; // already querying

        if (m_State == State::CollectingDataAfterFinish)
        {
            // this action from the next round, delay it
            m_DelayedEvents->OnActionButtonChanged(area, action, amount);
            return;
        }

        const auto center = cv::Point(area.x + area.width / 2, area.y + area.height / 2);

        // check for valid area
        if (m_FoldButton && !m_FoldButton->GetArea().contains(center) && 
            m_CheckCallButton && !m_CheckCallButton->GetArea().contains(center) && 
            m_BetRaiseButton && !m_FoldButton->GetArea().contains(center))
            return;

        // it means that action button appeared, so we need to process bot decision
        LOG_TRACE("Action button changed: action: [%s], amount: %s, area: %s") % pcmn::Action::ToString(action) % amount % area;
        m_State = State::ActionQueryInProgress;
    }

    void QueryAction()
    {
        FlushData();
        m_Game->Flush();

        cmn::xml::Node node;
        m_Game->Serialize(node);

        std::string data;
        node.Save(data);

        // save log
        if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace))
        {
            boost::filesystem::path path = m_Log.GetLogFolder(CURRENT_MODULE_ID);
            path /= "queries";
            boost::filesystem::create_directories(path);

            std::string time = boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time());
            boost::algorithm::replace_all(time, ":", ".");
            const auto file = (path / (time + ".xml"));
            node.Save2File(file.wstring());
        }

        // convert round xml data to protobuf format
        const auto packet = conv::IXml2Proto::Instance()->Convert(node);

        try
        {
            const net::Reply response = m_Channel.Channel<net::PokerService>().GetAction(packet);
            LOG_INFO("Server response: %s") % response.ShortDebugString();

            if (m_Control)
            {
                switch (response.action())
                {
                case pcmn::Action::Fold:
                    m_Control->Fold();
                    break;
                case pcmn::Action::Check:
                case pcmn::Action::Call:
                    m_Control->CheckCall();
                    break;
                case pcmn::Action::Bet:
                case pcmn::Action::Raise:
                    m_Control->BetRaise(response.amount());
                    break;
                default:
                    assert(false);
                }
            }
        }
        catch (const std::exception& e)
        {
            LOG_ERROR("Failed to get action, error: %s") % cmn::ExceptionInfo(e);
            if (m_Control)
                m_Control->Fold();
        }
    }

    void SetPlayerList()
    {
        // make list of players
        IGameRound::PlayerQueue players;

        // process only active players
        const auto range = m_Players | boost::adaptors::filtered([this](const rec::IRecognizableArea::Ptr& a){ 
            const auto index = GetPlayerIndex(a);
            return !m_EmptySits[index];
        });

        // set up player stacks
        boost::transform(range, std::back_inserter(players), [this](const rec::IRecognizableArea::Ptr& p){
            const pcmn::Player& player = m_PlayersData[GetPlayerIndex(p)];
            m_Game->SetPlayerParams(player);
            return player.Name();
        });

        // set up player list
        LOG_TRACE("Setting player list to logic: [%s]") % players;
        m_Game->SetPlayers(players);     
    }

    void SortPlayers()
    {
        // sort players by theirs location on the table(clockwise)
        if (m_Players.size() < 2)
            return;

        MergePlayers();

        assert(m_WindowWidth && m_WindowHeight);

        int minX = m_WindowWidth;
        int minY = m_WindowHeight;
        int maxX = 0;
        int maxY = 0;
        for (const rec::IRecognizableArea::Ptr& player : m_Players)
        {
            if (minX > player->GetArea().x)
                minX = player->GetArea().x;
            if (minY > player->GetArea().y)
                minY = player->GetArea().y;
            if (maxX < player->GetArea().br().x)
                maxX = player->GetArea().br().x;
            if (maxY < player->GetArea().br().y)
                maxY = player->GetArea().br().y;
        }

        const cv::Point center(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2);

        // order players clockwise
        const auto sorter = [&center](const rec::IRecognizableArea::Ptr& lhs, const rec::IRecognizableArea::Ptr& rhs)
        {
            // detect player table sides
            const bool isLeftOnTop = lhs->GetArea().y < center.y;
            const bool isRightOnTop = rhs->GetArea().y < center.y;

            // compare by x
            if (isLeftOnTop && isRightOnTop)
                return lhs->GetArea().x < rhs->GetArea().x;
            else
            if (!isLeftOnTop && !isRightOnTop)
                return lhs->GetArea().x > rhs->GetArea().x;
            else
            if (isLeftOnTop && !isRightOnTop)
                return true;
            return false;
        };

        std::map<rec::IRecognizableArea::Ptr, pcmn::Player> players;
        boost::transform(m_Players, std::inserter(players, players.end()), [&players, this](const rec::IRecognizableArea::Ptr& p) {
            const auto index = GetPlayerIndex(p);
            if (index != InvalidIndex() && m_PlayersData.size() > index)
                return std::make_pair(p, m_PlayersData[index]);
            else
                return std::make_pair(p, pcmn::Player::Unknown());
        });

        boost::sort(m_Players, sorter);

        m_PlayersData.clear();
        boost::transform(m_Players, std::back_inserter(m_PlayersData), [&players](const rec::IRecognizableArea::Ptr& p){
            return players[p];
        });

        m_EmptySits.resize(m_Players.size());
    }

    void CalculatePlayersAreas()
    {
        // players must be ordered!
        if (m_Players.size() < 2)
            return;

        struct PlayerPolygonEdges // player polygon edge points
        {
            typedef std::vector<PlayerPolygonEdges> List;
            PlayerPolygonEdges() : m_Previous(), m_Next() {}
            cv::Point m_Previous; // middle point on the line between current player and previous
            cv::Point m_Next;// middle point on the line between current player and next
        };

        const auto getCenterOfTable = [this]()
        {
            // recalculate real table center, because in m_WindowWidth and m_WindowHeight we have full screen
            int minX = m_WindowWidth;
            int minY = m_WindowHeight;
            int maxX = 0;
            int maxY = 0;
            for (const rec::IRecognizableArea::Ptr& player : m_Players)
            {
                if (minX > player->GetArea().x)
                    minX = player->GetArea().x;
                if (minY > player->GetArea().y)
                    minY = player->GetArea().y;
                if (maxX < player->GetArea().br().x)
                    maxX = player->GetArea().br().x;
                if (maxY < player->GetArea().br().y)
                    maxY = player->GetArea().br().y;
            }

            // validate
            if (minX > m_WindowWidth / 2 || maxX < m_WindowWidth / 2 || minY > m_WindowHeight / 2 || maxY < m_WindowHeight / 2)
            {
                minX = 0;
                minY = 0;
                maxX = m_WindowWidth;
                maxY = m_WindowHeight;
            }

            const cv::Point realCenter(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2);

            // find flop cards area
            m_FlopCardsArea = cv::Rect(realCenter.x, realCenter.y, 0, 0);

            // determine flop cards y
            int flopY = realCenter.y;
            for (const auto& card : m_Cards)
            {
                if (card->GetArea().x < minX || card->GetArea().x > maxX)
                    continue;

                unsigned counter = 0;
                for (const auto& onSameY : m_Cards)
                {
                    if (onSameY->GetArea().x < minX || onSameY->GetArea().x > maxX)
                        continue;

                    if (abs(card->GetArea().y - onSameY->GetArea().y) < 5)
                        ++counter;
                }

                if (counter >= 3)
                {
                    flopY = card->GetArea().y;
                    break;
                }
            }


            // filter flop cards, we need to remove player cards from input sequence
            const auto flopCards = m_Cards | boost::adaptors::filtered([&](const rec::IRecognizableArea::Ptr& card){
                const auto& area = card->GetArea();
                return area.x > minX && abs(card->GetArea().y - flopY) < 5 && area.x < maxX;
            });

            for (const rec::IRecognizableArea::Ptr& card : flopCards)
            {
                if (m_FlopCardsArea.x > card->GetArea().x)
                    m_FlopCardsArea.x = card->GetArea().x;

                if (m_FlopCardsArea.y > card->GetArea().y)
                    m_FlopCardsArea.y = card->GetArea().y;
            }

            // we have just calculated top left cards corner, reflect to center to get br point
            m_FlopCardsArea.width = (realCenter.x - m_FlopCardsArea.x) * 2;
            m_FlopCardsArea.height = (realCenter.y - m_FlopCardsArea.y) * 2;

            // return center minus flop cards area
            cv::Point result;
            result.x = m_FlopCardsArea.x + m_FlopCardsArea.width / 2;
            result.y = m_FlopCardsArea.y + m_FlopCardsArea.height / 2;

#ifdef VISUAL_DEBUG
            cv::Mat& drawing = m_DebugDrawing;
            cv::rectangle(drawing, cv::Point(minX, minY), cv::Point(maxX, maxY), cv::Scalar::all(255), 3);
            if (m_FlopCardsArea.width && m_FlopCardsArea.height)
                cv::rectangle(drawing, m_FlopCardsArea, cv::Scalar::all(255), 3);
            else
                cv::circle(drawing, cv::Point(m_FlopCardsArea.x, m_FlopCardsArea.y), 10, cv::Scalar::all(255), 3);
            cv::circle(drawing, result, 20, cv::Scalar::all(255), 3);
#endif // VISUAL_DEBUG

            return result;
        };

#ifdef VISUAL_DEBUG
        m_DebugDrawing = cv::Mat::zeros(m_WindowHeight, m_WindowWidth, CV_8UC3);
#endif // VISUAL_DEBUG

        // process polygon edges
        PlayerPolygonEdges::List polygonEdges(m_Players.size());
        for (std::size_t i = 0; i < m_Players.size(); ++i)
        {
            const std::size_t nextIndex = i != m_Players.size() - 1 ? i + 1 : 0;
            const cv::Rect& current = m_Players[i]->GetArea();
            const cv::Rect& next = m_Players[nextIndex]->GetArea();

            // find middle point between two players
            const int lineWidth = (next.x + next.width / 2) - (current.x + current.width / 2);
            const int lineHeight = (next.y + next.height / 2) - (current.y + current.height / 2);
            const cv::Point middle(current.x + current.width / 2 + lineWidth / 2, current.y + current.height / 2 + lineHeight / 2);

            // remember polygon edges for each player
            polygonEdges[i].m_Next = middle;
            polygonEdges[nextIndex].m_Previous = middle;

#ifdef VISUAL_DEBUG
            cv::rectangle(m_DebugDrawing, current, cv::Scalar(255, 0, 0), 3);
            cv::rectangle(m_DebugDrawing, next, cv::Scalar(255, 0, 0), 3);
            cv::circle(m_DebugDrawing, middle, 20, cv::Scalar(0, 0, 255), 5);
#endif // VISUAL_DEBUG
        }

        const auto center = getCenterOfTable();

        // build players polygons
        m_PlayerAreas.clear();
        m_PlayerAreas.resize(m_Players.size());
        for (std::size_t i = 0 ; i < m_Players.size(); ++i)
        {
            Polygon& current = m_PlayerAreas[i];

            // player name border
            const cv::Rect& playerArea = m_Players[i]->GetArea();
            const cv::Point playerCenter = cv::Point(
                playerArea.x > center.x ? playerArea.tl().x : playerArea.br().x,
                playerArea.y > center.y ? playerArea.tl().y : playerArea.br().y
            );

            const PlayerPolygonEdges& edges = polygonEdges[i];
            if (abs(edges.m_Previous.x - edges.m_Next.x) < 5 &&
                abs(edges.m_Previous.y - edges.m_Next.y) < 5)
            {
                // three points
                // next point is a center of the player area
                current.push_back(playerCenter);

                // next point is a table center according to flop cards correction
                current.push_back(center);

                // and final point is a next player edge
                current.push_back(edges.m_Next);
            }
            else
            {
                // four points
                // first point is a previous player edge
                current.push_back(edges.m_Previous);

                // next point is a center of the player area
                current.push_back(playerCenter);

                // next point is a next player edge
                current.push_back(edges.m_Next);

                // and final point is a table center according to flop cards correction
                current.push_back(center);
            }

#ifdef VISUAL_DEBUG
            cv::polylines(m_DebugDrawing, current, true, cv::Scalar(0, 255, 0), 3);
#endif // VISUAL_DEBUG
        }
    }

    std::size_t FindPlayerIndexByArea(const cv::Rect& area, Search::Value type)
    {
        if (type == Search::AreaOnly)
        {
            const auto finder = [&area](const Polygon& polygon){
                return 
                    !polygon.empty() && 
                    cv::pointPolygonTest(polygon, cv::Point(area.x + area.width / 2, area.y + area.height / 2), false) >= 0;
            };
            const auto it = boost::find_if(m_PlayerAreas, finder);
            return it == m_PlayerAreas.end() ? InvalidIndex() : std::distance(m_PlayerAreas.begin(), it);
        }
        else
        {
            assert(type == Search::Nearby);

            // find nearest player
            typedef std::pair<double, std::size_t> Pair;
            std::vector<Pair> distances;
            distances.reserve(m_Players.size());
            boost::transform(m_Players, std::back_inserter(distances), [&area, &distances, this](const rec::IRecognizableArea::Ptr& p){ 
                    const bool emptySit = m_EmptySits[GetPlayerIndex(p)];
                    double result = std::numeric_limits<double>::max();
                    if (!emptySit)
                    {
                        result = cv::norm(
                            cv::Point(p->GetArea().x + p->GetArea().width / 2, p->GetArea().y + p->GetArea().height / 2) 
                            - 
                            cv::Point(area.x + area.width / 2, area.y + area.height / 2) 
                        );
                    }

                    return std::make_pair(result, distances.size());
                } 
            );
            boost::sort(distances, boost::bind(&Pair::first, _1) < boost::bind(&Pair::first, _2));
            return distances.front().second;
        }
    }

    rec::IRecognizableArea::Ptr CreateArea(const cv::Rect& area)
    {
        return rec::IRecognizableArea::Instance(area);
    }

    rec::IRecognizableArea::Ptr GetArea(rec::IRecognizableArea::List& input, const cv::Rect& area)
    {
        const auto it = boost::find_if(input, boost::bind(&rec::IRecognizableArea::IsEqual, _1, area));
        if (it != input.end())
        {
            if ((*it)->GetArea() != area)
            {
                (*it)->SetArea(area);
                SortPlayers();
                CalculatePlayersAreas();
            }
            return *it;
        }

        const auto result = CreateArea(area);
        input.push_back(result);

        // areas changed, recalculate players areas
        m_PlayersData.resize(m_Players.size());
        m_EmptySits.resize(m_Players.size());
        SortPlayers();
        CalculatePlayersAreas();
        return result;
    }

    std::size_t GetPlayerIndex(const rec::IRecognizableArea::Ptr& player)
    {
        const auto it = boost::find(m_Players, player);
        CHECK(it != m_Players.end(), cmn::Exception("Failed to find player recognizable area"), player->GetArea());
        return std::distance(m_Players.begin(), it);
    }

    void SaveGameData()
    {
        try 
        {
            BOOST_SCOPE_EXIT(&m_Game)
            {
                m_Game->Clear();
            }
            BOOST_SCOPE_EXIT_END;

            if (m_Game->IsEmpty())
                return;

            cmn::xml::Node node;
            m_Game->Serialize(node);

            // save log
            if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace))
            {
                boost::filesystem::path path = m_Log.GetLogFolder(CURRENT_MODULE_ID);
                path /= "games";
                boost::filesystem::create_directories(path);

                std::string time = boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time());
                boost::algorithm::replace_all(time, ":", ".");
                const auto file = (path / (time + ".xml"));
                node.Save2File(file.wstring());
            }

            // send statistics request
            const auto packet = conv::IXml2Proto::Instance()->Convert(node);
            m_Channel.Channel<net::PokerService>().SaveGame(packet);
        }
        CATCH_PASS(cmn::Exception("Failed to log game"))
    }

    virtual void Deserialize(const cmn::xml::Node& node) override
    {
        cmn::srl::Serializable::Deserialize(node);
        SortPlayers();
        CalculatePlayersAreas();

        // set up player area
        m_PlayersArea = cv::Rect(m_WindowWidth / 2, m_WindowHeight / 2, 0, 0);
        const auto adjuster = [this](const rec::IRecognizableArea::Ptr& area)
        {
            const cv::Rect player = area->GetArea();
            if (player.br().x > m_PlayersArea.br().x)
                m_PlayersArea.width += (player.br().x - m_PlayersArea.br().x);
            if (player.y > m_PlayersArea.br().y)
                m_PlayersArea.height += (player.y - m_PlayersArea.br().y);
            if (player.x < m_PlayersArea.x)
                m_PlayersArea.x = player.x;
            if (player.y < m_PlayersArea.y)
                m_PlayersArea.y = player.y;
        };

        boost::for_each(m_Players, adjuster);
        LOG_TRACE("Calculated players area: %s") % m_PlayersArea;
    }

    void MergePlayers()
    {
        rec::IRecognizableArea::List toRemove;
        for (const auto& player : m_Players)
        {
            const auto finder = [player](const rec::IRecognizableArea::Ptr& area)
            {
                return player != area && player->IsEqual(area->GetArea());
            };
            const auto it = boost::find_if(m_Players, finder);
            if (it == m_Players.end())
                continue;

            // found player which will be merged
            if (player->GetArea().area() < (*it)->GetArea().area())
                toRemove.push_back(player);
        }

        // remove all duplicated
        for (const auto& player : toRemove)
        {
            const auto it = boost::find(m_Players, player);
            assert(it != m_Players.end());
            m_Players.erase(it);
        }
    }

    void RemovePlayer(const cv::Rect& area)
    {
        // try to find player first
        const cv::Point center(area.x + area.width / 2, area.y + area.height / 2);

        const auto it = boost::find_if(m_Players, boost::bind(&cv::Rect::contains, boost::bind(&rec::IRecognizableArea::GetArea, _1), center));
        if (it != m_Players.end())
        {
            const auto index = std::distance(m_Players.begin(), it);
            LOG_TRACE("Player has left the game: [%s], area: %s") % m_PlayersData[index].Name() % area; 
            OnPlayerFolded(m_PlayersData[index].Name());

            m_EmptySits[index] = true;

            SetPlayerList();
        }
    }

    std::pair<std::size_t, std::size_t> GetPlayerCards(const std::size_t player)
    {
        const auto& area = m_Players[player]->GetArea();

        // find nearest cards for this player
        typedef std::pair<double, std::size_t> Pair;
        std::vector<Pair> distances;
        distances.reserve(m_Cards.size());
        boost::transform(m_Cards, std::back_inserter(distances), [&area, &distances, this](const rec::IRecognizableArea::Ptr& p){ 
            return std::make_pair(cv::norm(p->GetArea().tl() - area.tl()), distances.size());
        });

        boost::sort(distances, boost::bind(&Pair::first, _1) < boost::bind(&Pair::first, _2));
        return std::make_pair((distances.begin() + 0)->second, (distances.begin() + 1)->second);
    }

    virtual void OnPlayerFolded(const std::string& name) override
    {
        const auto index = GetPlayerIndexByName(name);
        m_PlayersData[index].State(pcmn::Player::State::Folded);
    }

    virtual void OnNewStreet() override
    {
        LOG_TRACE("Street finished, flop: %s") % m_FlopCards;

        // reset player bets
        boost::for_each(m_PlayersData, boost::bind(&pcmn::Player::Bet, _1, 0));	

        // reset player afk flags
        boost::for_each(m_PlayersData, boost::bind(&pcmn::Player::Afk, _1, false));	

        // reset player actions
        m_Actions.clear();
    }

    virtual void OnNewGame(bool wait4Button) override
    {
        LOG_TRACE("Game finished");

        m_State = State::CollectingDataAfterFinish;

        // reset all delayed events
        m_DelayedEvents->Clear();

        // this callback should gather all info between rounds(player cards)
        // (showdown phase), expected that this call will block
        // until new rounds begin, and callee must gather all showdown phase
        // info and invoke appropriate methods from 'TableRegognizer'

        // if 'wait4Button' is set tot true, we already have new player on button
        if (wait4Button)
        {
            const auto playerOnButton = FindPlayerIndexByArea(m_DealerButton->GetArea(), Search::Nearby);
            m_Callback.Wait4NewGame([playerOnButton, this](){
                const auto currentPlayer = FindPlayerIndexByArea(m_DealerButton->GetArea(), Search::Nearby);
                return playerOnButton != currentPlayer;
            });
        }

        // save all game data, expected that we have full game data here
        // all showdown phase data must be collected during 'Wait4NewGame' call
        SaveGameData();

        // we must drop all fold states because they may break correct player states
        m_DelayedEvents->DropAllFolds();

        m_State = State::NewRound;

        m_FlopCards.clear();

        // reset player bets and cards
        boost::for_each(m_PlayersData, boost::bind(&pcmn::Player::Bet, _1, 0));
        boost::for_each(m_PlayersData, boost::bind(&pcmn::Player::Cards, _1, pcmn::Card::List()));

        // clear game because game may contain incomplete data
        // (big blinds are not known during this call)
        m_Game->Clear();

        // check for new round beginning, if actions exists skip wait
        if (!m_DelayedEvents->HasAction(pcmn::Action::SmallBlind) && !m_DelayedEvents->HasAction(pcmn::Action::BigBlind))
        {
            // wait until players became in game
            m_IsSomeoneBecameOnline = false;
            m_Callback.Wait4NewGame(boost::bind(&TableRecognizer::m_IsSomeoneBecameOnline, this));
        }

        // ok, all data saved/reseted, we are ready to process next game data
        m_Callback.OnNewGameReady();

        // remove all delayed events before blinds
        if (m_DelayedEvents->HasAction(pcmn::Action::SmallBlind) || m_DelayedEvents->HasAction(pcmn::Action::BigBlind))
            m_DelayedEvents->RemoveActionsBefore(boost::assign::list_of(pcmn::Action::SmallBlind)(pcmn::Action::BigBlind));
        else
            m_DelayedEvents->DropAllActions();

        // remove player info duplicates, we need only last info
        m_DelayedEvents->RemoveDuplicatedPlayers();

        // play all delayed events
        m_DelayedEvents->Play();

        // flush all new data
        Flush();
    }

    void ProcessUnknownPlayers()
    {
        // this method should be called when all objects recognized

        // process players with 'Unknown' name and empty stack
        const auto filter = [](const pcmn::Player& p){
            return p.Name() == pcmn::Player::Unknown().Name() && !p.Stack();
        };

        // copy sits data
        const auto previous = m_EmptySits;

        // make sits empty
        boost::transform(m_PlayersData, m_EmptySits.begin(), filter);

        // pass list without unknown players to the game logic
        SetPlayerList();

        // stop updating them
        for (unsigned i = 0 ; i < m_Players.size(); ++i)
        {
            if (m_EmptySits[i] && !previous[i]) // flag changed, cancel player update
                RemovePlayer(m_Players[i]->GetArea());
        }
    }

    IImageManager::Event::List GetNextEvents()
    {
        IImageManager::Event::List result;
        static const std::string empty;

        const std::string& name = m_DealerButton ? m_Game->GetLastPlayer() : empty;
        if (name.empty())
        {
            LOG_TRACE("Waiting for any player changes...");

            // game is not started yet, wait for any changes
            result.reserve(m_Players.size());
            boost::transform(m_Players, std::back_inserter(result), [this](const rec::IRecognizableArea::Ptr& p){
                return IImageManager::Event(p->GetArea(), false);
            });
            return result;
        }

        result.push_back(IImageManager::Event(m_DealerButton->GetArea(), false));
        result.push_back(IImageManager::Event(m_FoldButton->GetArea(), false));

        {
            LOG_TRACE("Waiting for player '%s' changes...") % name;
            const auto index = GetPlayerIndexByName(name);
            result.push_back(IImageManager::Event(m_Players[index]->GetArea(), true));
            result.push_back(IImageManager::Event(GetPlayerInGameSign(index), false));
        }

        if (m_Game->GetPhase() == pcmn::Table::Phase::Preflop)
        {
            const auto& sb = m_Game->GetSmallBlindPlayer();
            if (!sb.empty())
            {
                LOG_TRACE("Waiting for player on small blind '%s' changes...") % sb;
                const auto index = GetPlayerIndexByName(sb);
                result.push_back(IImageManager::Event(m_Players[index]->GetArea(), true));
                result.push_back(IImageManager::Event(GetPlayerInGameSign(index), false));
            }
        }

        return result;
    }

    cv::Rect GetPlayerInGameSign(const std::size_t player)
    {
        for (const auto& area : m_InGameSigns)
        {
            const auto index = FindPlayerIndexByArea(area->GetArea(), Search::Nearby);;
            if (index == player)
                return area->GetArea();
        }
        THROW(cmn::Exception("Failed ot find player in game sign"), player);
    }

    virtual IImageAdapter::Params GetAllAreas() const override
    {
        IImageAdapter::Params params;

        // all areas
        params.m_AllAreas.reserve(m_Players.size() + m_Cards.size() + 3);

        boost::transform(m_Players, std::back_inserter(params.m_AllAreas), boost::bind(&rec::IRecognizableArea::GetArea, _1));
        boost::transform(m_Cards, std::back_inserter(params.m_AllAreas), boost::bind(&rec::IRecognizableArea::GetArea, _1));

        if (m_FoldButton)
            params.m_AllAreas.push_back(m_FoldButton->GetArea());
        if (m_CheckCallButton)
            params.m_AllAreas.push_back(m_CheckCallButton->GetArea());
        if (m_BetRaiseButton)
            params.m_AllAreas.push_back(m_BetRaiseButton->GetArea());

        // in game signs
        params.m_InGameSigns.reserve(m_InGameSigns.size());
        boost::transform(m_InGameSigns, std::back_inserter(params.m_InGameSigns), boost::bind(&rec::IRecognizableArea::GetArea, _1));

        // players area
        params.m_PlayersArea = m_PlayersArea;

        // player areas
        params.m_PlayerAreas = m_PlayerAreas;

        // make flop cards area polygon
        params.m_CardsArea.push_back(cv::Point(m_FlopCardsArea.x - 10, m_FlopCardsArea.y - 10));
        params.m_CardsArea.push_back(cv::Point(m_FlopCardsArea.x + m_FlopCardsArea.width + 20, m_FlopCardsArea.y - 10));
        params.m_CardsArea.push_back(cv::Point(m_FlopCardsArea.x + m_FlopCardsArea.width + 20, m_FlopCardsArea.y + m_FlopCardsArea.height + 10));
        params.m_CardsArea.push_back(cv::Point(m_FlopCardsArea.x - 10, m_FlopCardsArea.y + m_FlopCardsArea.height + 10));

        return params;
    }

    virtual bool IsPlayerInGame(const std::string& name) override
    {
        if (m_State == State::NewRound)
            return true;

        const auto index = GetPlayerIndexByName(name);
        if (m_PlayersData[index].Afk())
            return false;

        return m_PlayersData[index].State() != pcmn::Player::State::Folded;
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        const auto index = GetPlayerIndexByName(name);
        return m_PlayersData[index].Bet();
    }

    std::size_t GetPlayerIndexByName(const std::string& name)
    {
        // get player index
        const auto it = boost::find_if(m_Players, [this, &name](const rec::IRecognizableArea::Ptr& area){
            const auto index = GetPlayerIndex(area);
            return m_PlayersData[index].Name() == name;
        });

        return std::distance(m_Players.begin(), it);
    }

    virtual void SetTableControl(ITableControl& ctrl)
    {
        m_Control = &ctrl;
    }

private:

    //! External references
    ILog& m_Log;
    rpc::Endpoint& m_Channel;
    ITableRecognizer::ICallback& m_Callback;
    ITableControl* m_Control;

    //! Table areas
    rec::IRecognizableArea::List m_Players;
    rec::IRecognizableArea::List m_Cards;
    rec::IRecognizableArea::Ptr m_Pot;
    rec::IRecognizableArea::Ptr m_DealerButton;
    rec::IRecognizableArea::Ptr m_FoldButton;
    rec::IRecognizableArea::Ptr m_CheckCallButton;
    rec::IRecognizableArea::Ptr m_BetRaiseButton;
    cv::Rect m_FlopCardsArea;
    rec::IRecognizableArea::List m_InGameSigns;

    //! Collected data
    pcmn::Player::List m_PlayersData;           //!< Players bets, cards, names
    PlayersAreas m_PlayerAreas;                 //!< List of players polygons, which describes area near player
    cv::Rect m_PlayersArea;                     //!< Area where players may be found
    unsigned m_PotSize;                         //!< Current pot size, actually not used now
    pcmn::Card::List m_FlopCards;               //!< Current flop cards, actually not used now
    IGame::Ptr m_Game;                          //!< Poker game logic
    std::vector<bool> m_EmptySits;              //!< Sits with players which has left the game(player index to flag mapping)
    State::Value m_State;                       //!< Recognizer state
    IEvents::Ptr m_DelayedEvents;               //!< Events which delayed by some reason
    unsigned m_UndetectedPlayersCount;          //!< Number of players with bets and without name
    PlayerAction::List m_Actions;               //!< List of player actions 
    bool m_IsSomeoneBecameOnline;               //!< Flag shows that someone on table just received cards
    std::once_flag m_InitFlag;                  //!< Flag for initial game waiting
    
    //! Table geometry
    int m_WindowWidth;
    int m_WindowHeight;

#ifdef _DEBUG
    cv::Mat m_DebugDrawing;
#endif // _DEBUG
};

#pragma warning(pop)

ITableRecognizer::Ptr ITableRecognizer::Instance(ILog& log, rpc::Endpoint& channel, ICallback& callback)
{
    return boost::make_shared<TableRecognizer>(log, channel, callback);
}

} // namespace clnt
