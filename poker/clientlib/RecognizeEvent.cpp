﻿#include "RecognizeEvent.h"
#include "common/Modules.h"
#include "xml/Node.h"
#include "common/GUID.h"
#include "exception/CheckHelpers.h"
#include "serialization/Helpers.h"
#include "serialization/Serialization.h"
#include "SerializableCards.h"

#include <map>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/filesystem.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/enum.hpp>

#include <opencv2/core/operations.hpp>

namespace cv
{
    template<typename _Tp> inline std::wostream& operator<<(std::wostream& out, const Rect_<_Tp>& rect)
    {
        out << L"[" << rect.width << L" x " << rect.height << L" from (" << rect.x << L", " << rect.y << L")]";
        return out;
    }
} // namespace cv

namespace clnt
{

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

DECLARE_CURRENT_MODULE(Modules::Events);

class SerializableRect : public cv::Rect, public cmn::srl::Serializable
{
public:
    SerializableRect(const cv::Rect& r = cv::Rect())
        : SERIALIZABLE("rectangle", 
        FIELD(x) 
        FIELD(y)
        FIELD(width)
        FIELD(height)
        )
        , cv::Rect(r)
    {
    }
};

class RecognizeEvents 
    : public IEvents
    , public cmn::srl::Serializable
    , boost::noncopyable
{
public:

    struct Event : public cmn::srl::Serializable
    {
        typedef std::list<Event> List;

        BOOST_ENUM(Type, 
            (Unknown)
            (TableSize)
            (Player)
            (Action)
            (Card)
            (Pot)
            (Bet)
            (Dealer)
            (ActionButton)
            (Afk)
            (State)
        )

        Event(Type::domain type = Type::Unknown)
            : SERIALIZABLE("event",
                FIELD(m_TypeString)
                FIELD(m_Type)
                FIELD(m_Player)
                FIELD(m_Rect)
                FIELD(m_Action)
                FIELD(m_Amount)
                FIELD(m_Stack)
                FIELD(m_Card)
            )
            , m_Type(type)
            , m_TypeString(Type(type).str())
        {

        }

        void Play(ITableRecognizer& table)
        {
            switch (m_Type)
            {
            case clnt::RecognizeEvents::Event::Type::Unknown:
                THROW(cmn::Exception("Event can't be unknown"));
                break;
            case clnt::RecognizeEvents::Event::Type::TableSize:
                table.OnTableSize(*m_Amount, *m_Stack);
                break;
            case clnt::RecognizeEvents::Event::Type::Player:
                table.OnPlayerChanged(m_Rect, m_Player, *m_Stack);
                break;
            case clnt::RecognizeEvents::Event::Type::Action:
                table.OnPlayerAction(m_Rect, *m_Action, *m_Stack);
                break;
            case clnt::RecognizeEvents::Event::Type::Card:
                table.OnCardChanged(m_Rect, *m_Card);
                break;
            case clnt::RecognizeEvents::Event::Type::Pot:
                table.OnPotChanged(m_Rect, *m_Amount);
                break;
            case clnt::RecognizeEvents::Event::Type::Bet:
                table.OnBetChanged(m_Rect, *m_Amount);
                break;
            case clnt::RecognizeEvents::Event::Type::Dealer:
                table.OnDealerButtonChanged(m_Rect);
                break;
            case clnt::RecognizeEvents::Event::Type::ActionButton:
                table.OnActionButtonChanged(m_Rect, *m_Action, *m_Amount);
                break;
            case clnt::RecognizeEvents::Event::Type::Afk:
                table.OnPlayerAfk(m_Rect, m_Player);
                break;
            case clnt::RecognizeEvents::Event::Type::State:
                table.OnPlayerState(m_Rect, *m_Amount != 0);
                break;
            default:
                THROW(cmn::Exception("Unknown action type"), m_Type);
            }
        }

        Event& Player(const std::string& name)
        {
            m_Player = name;
            return *this;
        }

        Event& Area(const cv::Rect& r)
        {
            m_Rect = r;
            return *this;
        }

        Event& Action(pcmn::Action::Value v)
        {
            Set(m_Action, v);
            return *this;
        }

        Event& Amount(unsigned v)
        {
            Set(m_Amount, v);
            return *this;
        }
        Event& Stack(unsigned v)
        {
            Set(m_Stack, v);
            return *this;
        }

        Event& Card(const pcmn::Card& v)
        {
            Set<pcmn::srl::Card>(m_Card, v);
            return *this;
        }

        template<typename T>
        Event& Set(boost::shared_ptr<T>& field, const T& value)
        {
            field = boost::make_shared<T>(value); 
            return *this;
        }

        bool operator == (const pcmn::Action::Value a) const
        {
            return m_Type == Type::Action && *m_Action == a;
        }

        Type::domain m_Type;
        std::string m_Player;
        boost::shared_ptr<pcmn::Action::Value> m_Action;
        boost::shared_ptr<unsigned> m_Amount;
        boost::shared_ptr<unsigned> m_Stack;
        boost::shared_ptr<pcmn::srl::Card> m_Card;
        SerializableRect m_Rect;
        std::string m_TypeString;
    };
 
    RecognizeEvents(ILog& log, ITableRecognizer& table)
        : SERIALIZABLE("events", FIELD(m_Events))
        , m_Log(log)
        , m_Table(table)
    {
    }

    virtual void Play()
    {
        boost::for_each(m_Events, boost::bind(&Event::Play, _1, boost::ref(m_Table)));
    }

    virtual void Load(const std::string& fileName)
    {
        cmn::xml::Node node(fileName);
        Deserialize(node);
    }

    virtual void OnTableSize(int width, int height)
    {
        LOG_TRACE("Table size: %s x %s") % width % height;
        m_Events.push_back(Event(Event::Type::TableSize).Amount(width).Stack(height));
    }

    virtual void OnPlayerChanged(const cv::Rect& area, const std::string& name, unsigned stack)
    {
        LOG_TRACE("Player name changed: name: [%s], stack: [%s], area: %s") % name % stack % area;
        m_Events.push_back(Event(Event::Type::Player).Area(area).Player(name).Stack(stack));
    }

    virtual void OnPlayerAction(const cv::Rect& area, pcmn::Action::Value action, unsigned stack)
    {
        LOG_TRACE("Player action: action: [%s], stack: %s, area: %s") % pcmn::Action::ToString(action) % stack % area;
        m_Events.push_back(Event(Event::Type::Action).Area(area).Action(action).Stack(stack));
    }
    virtual void OnPlayerAfk(const cv::Rect& area, const std::string& name)
    {
        LOG_TRACE("Player AFK: name: %s, area: %s") % name % area;
        m_Events.push_back(Event(Event::Type::Afk).Area(area).Player(name));
    }
    virtual void OnCardChanged(const cv::Rect& area, const pcmn::Card& card)
    {
        LOG_TRACE("Card changed: [%s], area: %s") % card % area;
        m_Events.push_back(Event(Event::Type::Card).Area(area).Card(card));
    }

    virtual void OnPotChanged(const cv::Rect& area, unsigned amount)
    {
        LOG_TRACE("Pot changed: amount: [%s], area: %s") % amount % area;
        m_Events.push_back(Event(Event::Type::Pot).Area(area).Amount(amount));
    }

    virtual void OnBetChanged(const cv::Rect& area, unsigned amount)
    {
        LOG_TRACE("Player bet changed: amount: [%s], area: %s") % amount % area;
        m_Events.push_back(Event(Event::Type::Bet).Area(area).Amount(amount));
    }

    virtual void OnDealerButtonChanged(const cv::Rect& area)
    {
        LOG_TRACE("Dealer changed: area: %s") % area;
        m_Events.push_back(Event(Event::Type::Dealer).Area(area));
    }

    virtual void OnActionButtonChanged(const cv::Rect& area, pcmn::Action::Value action, unsigned amount)
    {
        LOG_TRACE("Action button changed: action: [%s], amount: %s, area: %s") % pcmn::Action::ToString(action) % amount % area;
        m_Events.push_back(Event(Event::Type::ActionButton).Area(area).Action(action).Amount(amount));
    }

    virtual void OnPlayerState(const cv::Rect& area, bool isInGame)
    {
        LOG_TRACE("Player state: area: %s, in game: %s") % area % isInGame;
        m_Events.push_back(Event(Event::Type::State).Area(area).Amount(isInGame));
    }

    bool HasAction(pcmn::Action::Value action) const
    {
        return boost::find(m_Events, action) != m_Events.end();
    }

    virtual void Clear() override
    {
        m_Events.clear();
    }

    virtual IImageAdapter::Params GetAllAreas() const
    {
        return m_Table.GetAllAreas();
    }

    virtual void Flush() override
    {
        m_Table.Flush();
    }

    virtual void RemoveActionsBefore(const pcmn::Action::List& actions) override
    {
        for (auto it = m_Events.begin(), end = m_Events.end(); it != end; )
        {
            if (it->m_Type == Event::Type::Action)
            {
                if (boost::find(actions, *it->m_Action) != actions.end())
                    break;
                it = m_Events.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

    virtual void DropAllFolds() override
    {
        for (auto it = m_Events.begin(), end = m_Events.end(); it != end;)
        {
            if (it->m_Type == Event::Type::State && *it->m_Amount == 0)
                it = m_Events.erase(it);
            else
                ++it;
        }
    }

    virtual void RemoveDuplicatedPlayers() override
    {
        Event::List reversed;
        boost::copy(boost::reverse(m_Events), std::back_inserter(reversed));

        std::set<std::string> existing;
        for (auto it = reversed.begin(), end = reversed.end(); it != end;)
        {
            if (it->m_Type != Event::Type::Player)
            {
                ++it;
                continue;
            }
            if (existing.count(it->m_Player))
            {
                // start from beginning again
                it = reversed.erase(it);
            }
            else
            {
                existing.insert(it->m_Player);
                ++it;
            }
        }

        m_Events.clear();
        boost::copy(boost::reverse(reversed), std::back_inserter(m_Events));
    }

    virtual void DropAllActions() override
    {
        for (auto it = m_Events.begin(), end = m_Events.end(); it != end;)
        {
            if (it->m_Type == Event::Type::Action)
                it = m_Events.erase(it);
            else
                ++it;
        }
    }

    virtual void SetTableControl(ITableControl& ctrl) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:

    ILog& m_Log;
    ITableRecognizer& m_Table;

    Event::List m_Events;
};

#pragma warning(pop)

IEvents::Ptr IEvents::Instance(ILog& log, ITableRecognizer& table)
{
    return boost::make_shared<RecognizeEvents>(log, table);
}

} // namespace clnt
