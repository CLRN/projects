#ifndef TableParser_h__
#define TableParser_h__

#include "recognition/Recognition.h"
#include "common/ILog.h"
#include "ImageAdapter.h"

#include <boost/shared_ptr.hpp>

namespace cmn
{
namespace xml
{
    class Node;
}
}

namespace clnt
{

class ITableRecognizer;
class ITableParser : public ITableEvents
{
public:
    typedef boost::shared_ptr<ITableParser> Ptr;

    virtual ~ITableParser() {}

    virtual void SetPlayersArea(const cv::Rect& area) = 0;
    virtual unsigned ObtainBet(const rec::IRecognizer::Result::List& recognized, const cv::Point& center) const = 0;

    static Ptr Instance(ILog& log, rec::IRecognizer& recognizer, ITableRecognizer& table);
};

} // namespace clnt
#endif // TableParser_h__
