﻿#include "TableSnapshot.h"
#include "conversion/AnyCast.h"
#include "common/ILog.h"
#include "common/Modules.h"

#include "TableDiff.h"

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>

namespace clnt
{

DECLARE_CURRENT_MODULE(Modules::Client);

class TableSnapshot : public ITableSnapshot, boost::noncopyable
{
public:

    typedef pcmn::Player::List Players;

    TableSnapshot(ILog& log)
        : m_Log(log)
        , m_BigBlind()
        , m_Street()
    {
    }

    virtual void SetPlayer(const pcmn::Player& p) override
    {
        const auto it = boost::find(m_Players, p);
        if (it != m_Players.end())
            *it = p;
        else
            m_Players.push_back(p);
    }

    virtual void RemovePlayer(const pcmn::Player& p) override
    {
        const auto it = boost::find(m_Players, p);
        if (it != m_Players.end())
            m_Players.erase(it);
    }

    virtual void PushAction(const std::string& name, pcmn::Action::Value value, unsigned amount, unsigned stack) override
    {
        m_Actions.emplace_back(name, value, amount, stack);
    }

    void PushFlopCard(const pcmn::Card& card) override
    {
        if (boost::find(m_Flop, card) == m_Flop.end())
            m_Flop.push_back(card);
    }

    virtual DiffPtr GetDiff(const ITableSnapshot& old) const override
    {
        const TableSnapshot& snap = static_cast<const TableSnapshot&>(old);

        pcmn::Player::List players;

        // detect player differences
        for (const auto& p : m_Players)
        {
            const auto it = boost::find(snap.m_Players, p);
            if (it == snap.m_Players.end())
            {
                // new player
                players.push_back(p);
            }
            else
            {
                const int diff = static_cast<int>(it->Stack()) - static_cast<int>(p.Stack());
                if (diff > 0)
                {
                    // player bet
                    players.push_back(p);
                    players.back().Bet(diff);
                }
                else
                if (diff < 0)
                {
                    // player won some money
                    players.push_back(p);
                }
                else
                if (it->State() != pcmn::Player::State::Folded && p.State() == pcmn::Player::State::Folded)
                {
                    // player folded
                    players.push_back(p);
                }
            }
        }

        std::string newButton;
        if (m_Button.empty())
            const_cast<std::string&>(m_Button) = snap.m_Button;
        else
        if (!snap.m_Button.empty() && m_Button != snap.m_Button)
            newButton = m_Button;

        IDiff::PlayerAction::List actions;
        for (const auto& a : m_Actions)
        {
            const auto it = boost::find(snap.m_Actions, a);
            if (it == snap.m_Actions.end())
                actions.push_back(a);
        }

        return IDiff::Instance(m_Log, players, actions, newButton);
    }

    void Apply(const IDiff& diff) override
    {
        for (auto& p : m_Players)
        {
            const auto* player = diff.GetPlayer(p.Name());
            if (player)
                p = *player;
        }

        std::string name;
        unsigned bet = 0;
        pcmn::Action::Value action = pcmn::Action::Unknown;
        unsigned stack = 0;
        while (diff.GetAction(name, bet, action, stack))
            PushAction(name, action, bet, stack);
    }

    virtual std::string GetLogInfo() const override
    {
        std::ostringstream oss;
        oss << "snapshot: button: " << m_Button << ", bb: " << m_BigBlind << ", players: ";
        for (const auto& p : m_Players)
            oss << "player: " << p << " ";

        oss << ", actions: ";
        for (const auto& a : m_Actions)
            oss << "action: " << a.ToString() << " ";

        oss << ", flop: " << pcmn::Table::Phase::ToString(pcmn::Table::Phase::FromCardsCount(m_Flop.size()));
        return oss.str();
    }

    virtual void SetButton(const std::string& name) override
    {
        m_Button = name;
    }

    virtual void SetBigBlind(unsigned bb) override
    { 
        m_BigBlind = bb;
    }

    void SetSequence(const std::deque<std::string>& names) override
    {
        Players copy;
        copy.swap(m_Players);
        boost::transform(names, std::back_inserter(m_Players), [&copy](const std::string& name){
            const auto it = boost::find(copy, name);
            return it == copy.end() ? pcmn::Player(name) : *it;
        });
    }

    void SetStreet(pcmn::Table::Phase::Value street) override
    {
        m_Street = street;
    }

    virtual pcmn::Player::List GetPlayers() const override
    {
        pcmn::Player::List result;
        const auto range = m_Players | boost::adaptors::filtered(boost::bind(&pcmn::Player::Name, _1) != pcmn::Player::Unknown().Name());
        boost::copy(range, std::back_inserter(result));
        return result;
    }

    virtual const std::string& GetButton() const
    {
        return m_Button;
    }

    virtual pcmn::Table::Phase::Value GetStreet() const override
    {
        const auto fromCards = pcmn::Table::Phase::FromCardsCount(m_Flop.size());
        return std::max(fromCards, m_Street);
    }

    virtual unsigned GetBigBlind() const
    {
        return m_BigBlind;
    }

    void Clear()
    {
        Reset();
        m_Flop.clear();
        boost::for_each(m_Players, boost::bind(&pcmn::Player::Cards, _1, pcmn::Card::List()));
    }

    void Reset()
    {
        const auto reset = [](pcmn::Player& player){ 
            player.Bet(0); 
            if (player.State() == pcmn::Player::State::Called)
                player.State(pcmn::Player::State::Waiting);
        };
        boost::for_each(m_Players, reset);
        m_Actions.clear();
    }

    virtual const pcmn::Card::List& GetFlop() const override
    {
        return m_Flop;
    }

    virtual void PushPlayerCard(const std::string& name, const pcmn::Card& card) override
    {
        const auto it = boost::find(m_Players, name);
        CHECK(it != m_Players.end(), cmn::Exception("Failed to find player by name"), name);
        auto cards = it->Cards();
        CHECK(cards.size() < 2, cmn::Exception("Failed to append card to player"), cards, card, name);
        cards.push_back(card);
        it->Cards(cards);
    }

private:

    void Copy(const ITableSnapshot& other)
    {
        const TableSnapshot& snap = static_cast<const TableSnapshot&>(other);
        m_Players = snap.m_Players;
        m_Actions = snap.m_Actions;
        m_Flop = snap.m_Flop;
        m_BigBlind = snap.m_BigBlind;
        m_Button = snap.m_Button;
    }

    ILog& m_Log;
    Players m_Players;
    IDiff::PlayerAction::List m_Actions;
    std::string m_Button;
    unsigned m_BigBlind;
    pcmn::Card::List m_Flop; 
    pcmn::Table::Phase::Value m_Street;
};


ITableSnapshot::Ptr ITableSnapshot::Instance(ILog& log)
{
    return boost::make_shared<TableSnapshot>(log);
}

} // namespace clnt
