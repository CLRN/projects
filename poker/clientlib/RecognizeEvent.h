#ifndef RecognizeEvent_h__
#define RecognizeEvent_h__

#include "common/ILog.h"
#include "TableRecognizer.h"
#include "Actions.h"

#include <vector>

#include <boost/shared_ptr.hpp>

#include <opencv2/core/core.hpp>

namespace clnt
{

class IEvents : public ITableRecognizer
{
public:
    typedef boost::shared_ptr<IEvents> Ptr;

    virtual ~IEvents() {}

    virtual void Play() = 0;
    virtual void Load(const std::string& fileName) = 0;
    virtual void Clear() = 0;
    virtual bool HasAction(pcmn::Action::Value action) const = 0;
    virtual void RemoveActionsBefore(const pcmn::Action::List& actions) = 0;
    virtual void DropAllFolds() = 0;
    virtual void RemoveDuplicatedPlayers() = 0;
    virtual void DropAllActions() = 0;

    static Ptr Instance(ILog& log, ITableRecognizer& table);
};

} // namespace clnt
#endif // RecognizeEvent_h__
