#ifndef TableDiff_h__
#define TableDiff_h__

#include "common/ILog.h"

#include "Player.h"

#include <boost/shared_ptr.hpp>

namespace clnt
{

class IDiff
{
public:
    typedef boost::shared_ptr<IDiff> Ptr;

    struct PlayerAction
    {
        typedef std::list<PlayerAction> List;
        PlayerAction(const std::string& name = "", pcmn::Action::Value v = pcmn::Action::Unknown, unsigned amount = 0, unsigned stackAfter = 0);
        std::string ToString() const;
        bool operator == (const PlayerAction& other) const;
        std::string m_Name;
        pcmn::Action::Value m_Action;
        unsigned m_Amount;
        unsigned m_StackAfter;
    };

    virtual ~IDiff() {}

    virtual const pcmn::Player* GetPlayer(const std::string& player) const = 0;
    virtual bool GetAction(std::string& name, unsigned& amount, pcmn::Action::Value& action, unsigned& stack) const = 0;
    virtual std::string GetLogInfo() = 0;
    virtual bool Empty() const = 0;
    virtual bool HasActions() const = 0;
    virtual const std::string& GetNewButton() const = 0;

    static Ptr Instance(ILog& log, const pcmn::Player::List& players, const PlayerAction::List& actions, const std::string& newButton);
};

} // namespace clnt
#endif // TableDiff_h__
