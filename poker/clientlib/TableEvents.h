#ifndef TableEvents_h__
#define TableEvents_h__

#include "common/ILog.h"
#include "Actions.h"
#include "Cards.h"
#include "serialization/ISerializable.h"

#include <vector>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace clnt
{

class ITableEvents : public virtual cmn::srl::ISerializable
{
public:
    typedef boost::shared_ptr<ITableEvents> Ptr;

    virtual ~ITableEvents() {}

    virtual void OnTableSize(int width, int height) = 0;
    virtual void OnRegionRecognized(const cv::Rect& rect, const std::wstring& text) = 0;
    virtual void OnButtonFound(const cv::Rect& rect) = 0;
    virtual void OnPlayerState(const cv::Rect& rect, bool isInGame) = 0;
    virtual void OnPlayerBet(const cv::Rect& player, unsigned bet) = 0;
    virtual bool IsEmpty() const = 0;

    static Ptr Instance();
};

class ITableEventsReader : public ITableEvents
{
public:
    typedef boost::shared_ptr<ITableEventsReader> Ptr;
    typedef std::vector<cv::Rect> Areas;

    virtual void Play(ITableEvents& parser) = 0;
    virtual bool IsExists(const Areas& areas) = 0;

    static Ptr Instance();
};

} // namespace clnt

#endif // TableEvents_h__
