#include "ImageManager.h"
#include "TableRecognizer.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <boost/make_shared.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>

namespace clnt
{

class ImageManager : public IImageManager
{
public:
    ImageManager(ILog& log, rec::IRecognizer& recognizer) 
        : m_Log(log)
        , m_Recognizer(recognizer)
    {
    }

    virtual Diff::List GetChanges(const cv::Mat& image) override
    {
        assert(!image.empty());
        assert(!m_Areas.empty());

        Diff::List result;
        result.reserve(m_Areas.size());

        const auto cropper = [&image](const cv::Rect& r){ return image(r); };

        if (m_OldAreas.empty())
        {
            // this is first image, assuming that all regions has differences
            std::vector<cv::Mat> images(m_Areas.size());
            boost::transform(m_Areas, images.begin(), cropper);

            boost::transform(images, std::back_inserter(result), [this, &result](const cv::Mat& image){
                Diff diff;
                diff.m_Area = m_Areas[result.size()];
                diff.m_Image = image;
                return diff;
            });
            m_OldAreas = result;
        }
        else
        {
            // create all regions from captured image
            std::vector<cv::Mat> images(m_Areas.size());
            boost::transform(m_Areas, images.begin(), cropper);

            Diff::List newAreas;
            newAreas.reserve(m_Areas.size());
            boost::transform(images, std::back_inserter(newAreas), [this, &newAreas](const cv::Mat& image){
                Diff diff;
                diff.m_Area = m_Areas[newAreas.size()];
                diff.m_Image = image;
                return diff;
            });

            // compare images, make diff flags vector
            std::vector<bool> diffFlags(m_Areas.size());
            boost::transform(m_OldAreas, newAreas, diffFlags.begin(), std::not_equal_to<Diff>());

            // copy images that are changed
            unsigned i = 0;
            const auto range = newAreas | boost::adaptors::filtered([&i, &diffFlags](const Diff& image){
                return diffFlags[i++];
            });

            boost::copy(range, std::back_inserter(result));

            // remember new regions
            m_OldAreas.swap(newAreas);
        }
        
        return result;
    }

    virtual Snapshot Wait4Changes(const Event::List& areas, const GetImageFn& callback) override
    {
        assert(!areas.empty());
        assert(callback);

        Snapshot result;

        // get first image
        result.m_Before = callback();

        // get regions
        std::vector<cv::Mat> oldRegions(areas.size());
        boost::transform(areas, oldRegions.begin(), [&result](const Event& e){
            return result.m_Before(e.m_Region);
        });

        // recognize it
        std::vector<std::wstring> oldTexts;
        oldTexts.reserve(areas.size());
        boost::transform(areas, std::back_inserter(oldTexts), [this, &oldTexts, &oldRegions](const Event& e){
            return e.m_IsNeed2Recognize ? m_Recognizer.RecognizeText(oldRegions[oldTexts.size()]) : L"";
        });

        for (;;)
        {
            result.m_After = callback();

            for (unsigned i = 0; i < areas.size(); ++i)
            {
                const cv::Mat newRegion = result.m_After(areas[i].m_Region);

                if (!IsEqual(newRegion, oldRegions[i]))
                {
                    if (!areas[i].m_IsNeed2Recognize)
                        return result;

                    // recognize new region
                    const std::wstring newText = m_Recognizer.RecognizeText(newRegion);
                    if (newText != oldTexts[i])
                        return result;
                }
            }

            result.m_Before = std::move(result.m_After);
        }
    }

    virtual void SetRegions(const std::vector<cv::Rect>& regions)
    {
        m_Areas = regions;
    }

private:
    ILog& m_Log;
    rec::IRecognizer& m_Recognizer;
    std::vector<cv::Rect> m_Areas;
    Diff::List m_OldAreas;
};

bool IImageManager::Diff::operator == (const Diff& other) const
{
    assert(m_Area == other.m_Area);
    return ImageManager::IsEqual(m_Image, other.m_Image);
}
bool IImageManager::Diff::operator != (const Diff& other) const
{
    assert(m_Area == other.m_Area);
    return !ImageManager::IsEqual(m_Image, other.m_Image);
}


IImageManager::Ptr IImageManager::Instance(ILog& log, rec::IRecognizer& recognizer)
{
    return boost::make_shared<ImageManager>(log, recognizer);
}

bool IImageManager::IsEqual(const cv::Mat& lhs, const cv::Mat& rhs)
{
    cv::Mat diff;
    cv::absdiff(lhs, rhs, diff);
    cv::cvtColor(diff, diff, cv::COLOR_BGR2GRAY);
    const double diffPoints = cv::countNonZero(diff);
    //const bool result = (diffPoints / diff.size().area()) < 0.009f;
    return !diffPoints;
}

} // namespace clnt