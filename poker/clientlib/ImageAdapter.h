#ifndef ImageAdapter_h__
#define ImageAdapter_h__

#include "TableEvents.h"
#include "common/ILog.h"
#include "recognition/Recognition.h"

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace clnt
{

class ITableParser;

class IImageAdapter
{
public:
    typedef boost::shared_ptr<IImageAdapter> Ptr;
    typedef std::vector<cv::Rect> Areas;
    typedef std::vector<cv::Point> Polygon;
    typedef std::vector<Polygon> Polygons;

    struct Params
    {
        Areas m_InGameSigns;
        Areas m_AllAreas;
        cv::Rect m_PlayersArea;
        Polygons m_PlayerAreas;
        Polygon m_CardsArea;
    };

    virtual ~IImageAdapter() {}
    virtual void Initialize(const Params& params) = 0;
    virtual ITableEvents::Ptr Parse(const cv::Mat& image) = 0;

    static Ptr Instance(ILog& log, rec::IRecognizer& recognizer, ITableParser& parser, const cv::Mat& button, const cv::Mat& inGameSign);
};



} // namespace clnt

#endif // ImageAdapter_h__
