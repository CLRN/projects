#ifndef PokerClient_h__
#define PokerClient_h__

#include "common/ILog.h"
#include "TableEvents.h"
#include "rpc/Endpoint.h"

#include <boost/shared_ptr.hpp>

namespace rec
{
class IRecognizer;
}

namespace clnt
{

class ITableParser;
class ITableRecognizer;
class ITableControl;

class IClient
{
public:
    typedef boost::shared_ptr<IClient> Ptr;

    __declspec(novtable) struct ICallback
    {
        virtual ITableEventsReader::Ptr GetNextEvents() = 0;
        virtual void OnNewGame() = 0;
    };

    virtual void Run() = 0;
    virtual ITableParser& GetParser() = 0;
    virtual rec::IRecognizer& GetRecognizer() = 0;
    virtual ITableRecognizer& GetTable() = 0;

    static Ptr Instance(ILog& log, ICallback& cb, rpc::Endpoint& channel, ITableControl& ctrl);
};

} // namespace clnt

#endif // PokerClient_h__
