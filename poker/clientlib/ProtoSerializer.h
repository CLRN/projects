#ifndef ProtoSerializer_h__
#define ProtoSerializer_h__

#include "packet.pb.h"
#include "xml/Node.h"

#include <boost/shared_ptr.hpp>

namespace conv
{

class IXml2Proto
{
public:
    typedef boost::shared_ptr<IXml2Proto> Ptr;

    virtual ~IXml2Proto() {}
    virtual net::Packet Convert(const cmn::xml::Node& root) const = 0;

    static Ptr Instance();
};

} // namespace conv

#endif // ProtoSerializer_h__
