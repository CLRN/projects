﻿#include "../clientlib/TableRecognizer.h"
#include "../clientlib/TableParser.h"
#include "../clientlib/ImageManager.h"
#include "../TableEvents.h"
#include "common/Log.h"
#include "xml/Node.h"
#include "conversion/AnyCast.h"
#include "common/Modules.h"
#include "rpc/Endpoint.h"
#include "common/ServiceHolder.h"

#include <opencv2/highgui/highgui.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign/list_of.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <boost/asio/io_service.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Invoke;
using ::testing::Expectation;


class MockedTableCallback : public clnt::ITableRecognizer::ICallback
{
public:
    MOCK_METHOD1(Wait4NewGame, void(const ReadyFunc& func));
    MOCK_METHOD0(OnNewGameReady, void ());
    MOCK_METHOD1(OnNextEventsNeeded, void (const clnt::IImageManager::Event::List& area));
};

class TableCallbackMediator : public clnt::ITableRecognizer::ICallback
{
public:

    virtual void Wait4NewGame(const ReadyFunc& func)
    {
        m_Mocked->Wait4NewGame(func);
    }

    virtual void OnNewGameReady()
    {
        m_Mocked->OnNewGameReady();
    }

    virtual void OnNextEventsNeeded(const clnt::IImageManager::Event::List& evnt)
    {
        m_Mocked->OnNextEventsNeeded(evnt);
    }

    void SetMockedCallback(clnt::ITableRecognizer::ICallback* cb)
    {
        m_Mocked = cb;
    }

private:
    clnt::ITableRecognizer::ICallback* m_Mocked;
};

namespace gp = google::protobuf;
class EmptyChannel : public rpc::Endpoint
{
public:

    EmptyChannel(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}

    class EmptyChannelImpl : public rpc::details::IChannel
    {
    public:

        virtual rpc::IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, const gp::Message& request, const rpc::IStream& /*stream*/, const rpc::details::CallParams& /*params*/)
        {
            m_MethodName = method.name();
            m_Request.CopyFrom(request);

            net::Reply response;
            const auto size = response.ByteSize();
            const auto stream = boost::make_shared<std::stringstream>();
            stream->write(reinterpret_cast<const char*>(&size), sizeof(size));
            CHECK(response.SerializeToOstream(stream.get()), cmn::Exception("Failed to serialize response"));
            stream->seekg(0);

            const auto future = rpc::IFuture::Instance();
            cmn::Service::Instance().post(boost::bind(&rpc::IFuture::SetData, future, stream));
            return future;
        }

        const net::Packet& GetRequest() const
        {
            return m_Request;
        }

        const std::string& GetMethod() const
        {
            return m_MethodName;
        }

    private:
        net::Packet m_Request;
        std::string m_MethodName;
    };

    const net::Packet& GetRequest() const
    {
        return m_Channel.GetRequest();
    }

    const std::string& GetMethod() const
    {
        return m_Channel.GetMethod();
    }

    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id& id)
    {
        return m_Channel;
    }

private:
    EmptyChannelImpl m_Channel;
};

class MockedRecognizer : public clnt::ITableRecognizer
{
public:

    MOCK_METHOD2(OnTableSize, void (int width, int height));
    MOCK_METHOD3(OnPlayerChanged, void (const cv::Rect& area, const std::string& name, unsigned stack));
    MOCK_METHOD3(OnPlayerAction, void (const cv::Rect& area, pcmn::Action::Value action, unsigned stack));
    MOCK_METHOD2(OnPlayerAfk, void (const cv::Rect& area, const std::string& name));
    MOCK_METHOD2(OnCardChanged, void (const cv::Rect& area, const pcmn::Card& card));
    MOCK_METHOD2(OnPotChanged, void (const cv::Rect& area, unsigned amount));
    MOCK_METHOD2(OnBetChanged, void (const cv::Rect& area, unsigned amount));
    MOCK_METHOD1(OnDealerButtonChanged, void (const cv::Rect& area));
    MOCK_METHOD3(OnActionButtonChanged, void (const cv::Rect& area, pcmn::Action::Value action, unsigned amount));
    MOCK_METHOD2(OnPlayerState, void(const cv::Rect& area, bool isInGame));

    virtual void Serialize(cmn::xml::Node& /*node*/) const
    {
        throw std::exception("The method or operation is not implemented.");
    }

    virtual void Deserialize(const cmn::xml::Node& /*node*/)
    {
        throw std::exception("The method or operation is not implemented.");
    }

    virtual std::string GetNodeName() const
    {
        throw std::exception("The method or operation is not implemented.");
    }

    virtual void DrawObjects(cv::Mat& image)
    {
        throw std::exception("The method or operation is not implemented.");
    }

    virtual bool IsRecognized() const
    {
        throw std::exception("The method or operation is not implemented.");
    }

    virtual clnt::IImageAdapter::Params GetAllAreas() const
    {
        Log log;
        EmptyChannel channel(log, "tests");
        MockedTableCallback callback;
        const auto recognizer = rec::IRecognizer::Instance();
        const auto table = clnt::ITableRecognizer::Instance(log, channel, callback);
        const auto parser = clnt::ITableParser::Instance(log, *recognizer, *table);
        {
            // load configuration file
            cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
            table->Deserialize(node);
        }
        return table->GetAllAreas();
    }

    virtual void Flush() override
    {
    }

    virtual void SetTableControl(clnt::ITableControl& ctrl)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

};

class MockedTableLogic : public pcmn::ITableLogic
{
public:
    MOCK_METHOD3(PushAction, void (const std::string& name, pcmn::Action::Value action, unsigned amount));
    MOCK_METHOD2(SetPlayerStack, void (const std::string& name, unsigned stack));
    MOCK_METHOD2(SetPlayerCards, void (const std::string& name, const pcmn::Card::List& cards));
    MOCK_METHOD1(SetFlopCards, void (const pcmn::Card::List& cards));
    MOCK_METHOD1(Parse, void (const net::Packet& packet));
    MOCK_METHOD1(SetPhase, void (pcmn::Table::Phase::Value phase));
    MOCK_METHOD1(SendRequest, void (bool statistics));
    MOCK_METHOD1(RemovePlayer, void (const std::string& name));
    MOCK_METHOD1(SetNextRoundData, void (const pcmn::Player::List& players));
    MOCK_CONST_METHOD0(IsRoundCompleted,  bool ());
};

clnt::IImageManager::Event::List MakeEventsList(const cv::Rect& player, const cv::Rect& dealer, const cv::Rect& smallBlind = cv::Rect())
{
    clnt::IImageManager::Event::List result;
    result.push_back(clnt::IImageManager::Event(dealer, false));
    result.push_back(clnt::IImageManager::Event(cv::Rect(640, 820, 200, 80), false)); // fold
    result.push_back(clnt::IImageManager::Event(player, true));
    if (smallBlind != cv::Rect())
        result.push_back(clnt::IImageManager::Event(smallBlind, false));

    return result;
}

TEST(TableRecognize, ParseFull1)
{
    Log log;
    
    MockedRecognizer table;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));

    EXPECT_CALL(table, OnTableSize(_, _)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "kinley09", 2655)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "pelusito52", 3585)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "Noopygirl", 1520)).Times(Exactly(1));

    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Nine, pcmn::Suit::Spades))).Times(Exactly(1));
    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Nine, pcmn::Suit::Hearts))).Times(Exactly(1));
    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::King, pcmn::Suit::Clubs))).Times(Exactly(1));
    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Seven, pcmn::Suit::Diamonds))).Times(Exactly(1));
    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Hearts))).Times(Exactly(1));

    EXPECT_CALL(table, OnDealerButtonChanged(cv::Rect(987, 205, 41, 40))).Times(Exactly(1));

    EXPECT_CALL(table, OnPlayerAction(_, pcmn::Action::Bet, 3640)).Times(Exactly(1));

    EXPECT_CALL(table, OnPlayerState(_, true)).Times(Exactly(2));
    EXPECT_CALL(table, OnPlayerState(_, false)).Times(Exactly(7));

    EXPECT_CALL(table, OnBetChanged(_, 300)).Times(Exactly(1));

    cmn::xml::Node node("../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/1.xml");
    const auto events = clnt::ITableEventsReader::Instance();
    events->Deserialize(node);
    events->Play(*parser);
}

TEST(TableRecognize, ParseFull2)
{
    Log log; 

    MockedRecognizer table;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));

    EXPECT_CALL(table, OnTableSize(_, _)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "hornet19800", 1390)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "koky634", 0)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "redhawkpop", 1440)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "Shanhan", 930)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "Alex2835", 2270)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "claudigi", 3510)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "F250red", 3210)).Times(Exactly(1));

    EXPECT_CALL(table, OnDealerButtonChanged(_)).Times(Exactly(1));

    EXPECT_CALL(table, OnPlayerState(_, true)).Times(Exactly(3));
    EXPECT_CALL(table, OnPlayerState(_, false)).Times(Exactly(6));

    EXPECT_CALL(table, OnBetChanged(_, 20)).Times(Exactly(2));
    EXPECT_CALL(table, OnBetChanged(_, 350)).Times(Exactly(2));
    EXPECT_CALL(table, OnBetChanged(_, 10)).Times(Exactly(1));

    cmn::xml::Node node("../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/2.xml");
    const auto events = clnt::ITableEventsReader::Instance();
    events->Deserialize(node);
    events->Play(*parser);
}

TEST(TableRecognize, ParseFull4)
{
    Log log;

    MockedRecognizer table;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));

    EXPECT_CALL(table, OnTableSize(_, _)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "lvanna781000", 159)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "iscatel9", 293)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "osaaron", 210)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "dupontjac", 201)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "Tayson727", 531)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "GuSSinas", 196)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerChanged(_, "CLRN", 198)).Times(Exactly(1)); 
    EXPECT_CALL(table, OnPlayerChanged(_, "VasissualiyB", 198)).Times(Exactly(1));
    EXPECT_CALL(table, OnPlayerAfk(_, "italra")).Times(Exactly(1));

    EXPECT_CALL(table, OnDealerButtonChanged(_)).Times(Exactly(1));

    EXPECT_CALL(table, OnActionButtonChanged(_, pcmn::Action::Fold, 0)).Times(Exactly(1));
    EXPECT_CALL(table, OnActionButtonChanged(_, pcmn::Action::Check, 0)).Times(Exactly(1));
    EXPECT_CALL(table, OnActionButtonChanged(_, pcmn::Action::Raise, 4)).Times(Exactly(1));

    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Seven, pcmn::Suit::Diamonds))).Times(Exactly(1));
    EXPECT_CALL(table, OnCardChanged(_, pcmn::Card(pcmn::Card::Four, pcmn::Suit::Clubs))).Times(Exactly(1));

    EXPECT_CALL(table, OnPlayerState(_, true)).Times(Exactly(6));
    EXPECT_CALL(table, OnPlayerState(_, false)).Times(Exactly(3));
    EXPECT_CALL(table, OnBetChanged(_, _)).Times(Exactly(4));

    cmn::xml::Node node("../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/4.xml");
    const auto events = clnt::ITableEventsReader::Instance();
    events->Deserialize(node);
    events->Play(*parser);
}

TEST(TableRecognize, LogicFull1)
{
    return;
    Log log;

    pcmn::Player::ThisPlayer().Name("Noopygirl");
    EmptyChannel channel(log, "tests");
    MockedTableCallback callback;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto table = clnt::ITableRecognizer::Instance(log, channel, callback);
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, *table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));
    {
        // load configuration file
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        table->Deserialize(node);
    }

    const auto params = table->GetAllAreas();

    const pcmn::Card::List cards = boost::assign::list_of
        (pcmn::Card(pcmn::Card::Nine, pcmn::Suit::Spades))
        (pcmn::Card(pcmn::Card::Nine, pcmn::Suit::Hearts))
        (pcmn::Card(pcmn::Card::King, pcmn::Suit::Clubs))
        (pcmn::Card(pcmn::Card::Seven, pcmn::Suit::Diamonds))
        (pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Hearts));

    EXPECT_CALL(callback, OnNextEventsNeeded(MakeEventsList(
        cv::Rect(877, 590, 140, 70), 
        cv::Rect(987, 205, 41, 40),
        cv::Rect(911, 512, 37, 46)
        ))).Times(Exactly(1));

    table->OnPlayerChanged(cv::Rect(94, 415, 140, 70), "SomePlayer", 3940);

    {
        cmn::xml::Node node("../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/1.xml");
        const auto events = clnt::ITableEventsReader::Instance();
        events->Deserialize(node);
        events->Play(*parser);
        table->OnActionButtonChanged(cv::Rect(641, 821, 1, 1), pcmn::Action::Fold, 0);
        table->Flush();
        table->Flush();
    }

    const auto& method = channel.GetMethod();
    EXPECT_EQ(method, "GetAction");

    const net::Packet& packet = channel.GetRequest();
    EXPECT_EQ(packet.info().players(0).name(), "kinley09");
    EXPECT_EQ(packet.info().players(1).name(), "pelusito52");
    EXPECT_EQ(packet.info().players(2).name(), "Noopygirl");
    EXPECT_EQ(packet.info().players(3).name(), "SomePlayer");

    EXPECT_EQ(packet.info().button(), 1);

    EXPECT_EQ(packet.info().players(0).stack(), 2655);
    EXPECT_EQ(packet.info().players(1).stack(), 3585);
    EXPECT_EQ(packet.info().players(2).stack(), 1520);
    EXPECT_EQ(packet.info().players(3).stack(), 3940);

    pcmn::Card::List flop;
    for (const auto card : packet.info().cards())
        flop.push_back(pcmn::Card().FromEvalFormat(card));

    EXPECT_EQ(flop, cards);

    EXPECT_EQ(packet.info().phase(), pcmn::Table::Phase::River);
    EXPECT_EQ(packet.phases_size(), 1);

    EXPECT_EQ(packet.phases(0).actions(0).player(), 2);
    EXPECT_EQ(packet.phases(0).actions(0).id(), pcmn::Action::Check);
    EXPECT_EQ(packet.phases(0).actions(0).amount(), 0);

    EXPECT_EQ(packet.phases(0).actions(1).player(), 3);
    EXPECT_EQ(packet.phases(0).actions(1).id(), pcmn::Action::Bet);
    EXPECT_EQ(packet.phases(0).actions(1).amount(), 300);
}

TEST(TableRecognize, LogicFull2)
{
    return;
    Log log;

    pcmn::Player::ThisPlayer().Name("hornet19800");
    EmptyChannel channel(log, "tests");
    MockedTableCallback callback;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto table = clnt::ITableRecognizer::Instance(log, channel, callback);
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, *table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));
    {
        // load configuration file
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        table->Deserialize(node);
    }

    EXPECT_CALL(callback, OnNextEventsNeeded(MakeEventsList(
            cv::Rect(345, 65, 140, 70), 
            cv::Rect(397, 534, 41, 40),
            cv::Rect(428, 162, 37, 46)
    ))).Times(Exactly(1));

    {
        cmn::xml::Node node("../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/2.xml");
        const auto events = clnt::ITableEventsReader::Instance();

        events->Deserialize(node);
        events->Play(*parser);
    }

    table->OnPlayerAction(cv::Rect(345, 65, 140, 70), pcmn::Action::Call, 1390);
    table->OnPlayerAction(cv::Rect(1088, 224, 140, 70), pcmn::Action::Raise, 0);
    table->OnPlayerAction(cv::Rect(305, 590, 140, 70), pcmn::Action::Call, 2270);
    table->OnActionButtonChanged(cv::Rect(641, 821, 1, 1), pcmn::Action::Fold, 0);
    table->Flush();
    table->Flush();

    const auto& method = channel.GetMethod();
    EXPECT_EQ(method, "GetAction");

    const net::Packet& packet = channel.GetRequest();
    EXPECT_EQ(packet.info().players(0).name(), "redhawkpop");
    EXPECT_EQ(packet.info().players(1).name(), "hornet19800");
    EXPECT_EQ(packet.info().players(2).name(), "koky634");
    EXPECT_EQ(packet.info().players(3).name(), "F250red");
    EXPECT_EQ(packet.info().players(4).name(), "claudigi");
    EXPECT_EQ(packet.info().players(5).name(), "Alex2835");
    EXPECT_EQ(packet.info().players(6).name(), "Shanhan");

    EXPECT_EQ(packet.info().button(), 5);

    EXPECT_EQ(packet.info().players(0).stack(), 1460);
    EXPECT_EQ(packet.info().players(1).stack(), 1410);
    EXPECT_EQ(packet.info().players(2).stack(), 350);
    EXPECT_EQ(packet.info().players(3).stack(), 3210);
    EXPECT_EQ(packet.info().players(4).stack(), 3510);
    EXPECT_EQ(packet.info().players(5).stack(), 2620);
    EXPECT_EQ(packet.info().players(6).stack(), 940);

    pcmn::Card::List flop;
    for (const auto card : packet.info().cards())
        flop.push_back(pcmn::Card().FromEvalFormat(card));

    EXPECT_TRUE(flop.empty());

    EXPECT_EQ(packet.info().phase(), pcmn::Table::Phase::Preflop);
    EXPECT_EQ(packet.phases_size(), 1);

    EXPECT_EQ(packet.phases(0).actions(0).player(), 6);
    EXPECT_EQ(packet.phases(0).actions(0).id(), pcmn::Action::SmallBlind);
    EXPECT_EQ(packet.phases(0).actions(0).amount(), 10);

    EXPECT_EQ(packet.phases(0).actions(1).player(), 0);
    EXPECT_EQ(packet.phases(0).actions(1).id(), pcmn::Action::BigBlind);
    EXPECT_EQ(packet.phases(0).actions(1).amount(), 20);

    EXPECT_EQ(packet.phases(0).actions(2).player(), 1);
    EXPECT_EQ(packet.phases(0).actions(2).id(), pcmn::Action::Call);
    EXPECT_EQ(packet.phases(0).actions(2).amount(), 20);

    EXPECT_EQ(packet.phases(0).actions(3).player(), 2);
    EXPECT_EQ(packet.phases(0).actions(3).id(), pcmn::Action::Raise);
    EXPECT_EQ(packet.phases(0).actions(3).amount(), 350);

    EXPECT_EQ(packet.phases(0).actions(4).player(), 5);
    EXPECT_EQ(packet.phases(0).actions(4).id(), pcmn::Action::Call);
    EXPECT_EQ(packet.phases(0).actions(4).amount(), 350);
}

TEST(TableRecognize, Offline)
{
    Log log;

    log.Open("client.trace.cfg");
  
    pcmn::Player::ThisPlayer().Name("bot");
    EmptyChannel channel(log, "tests");
    TableCallbackMediator callback;
    const auto recognizer = rec::IRecognizer::Instance();
    const auto table = clnt::ITableRecognizer::Instance(log, channel, callback);
    const auto parser = clnt::ITableParser::Instance(log, *recognizer, *table);
    const auto manager = clnt::IImageManager::Instance(log, *recognizer);
    parser->SetPlayersArea(cv::Rect(94, 65, 1134, 545));

    const auto parseFile = [parser, &log, table](const std::string& path)
    {
        cmn::xml::Node node(path);
        const auto events = clnt::ITableEventsReader::Instance();

        events->Deserialize(node);
        events->Play(*parser);
        table->Flush();
    };

    {
        // load configuration file
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        table->Deserialize(node);

        const auto params = table->GetAllAreas();
        parser->SetPlayersArea(params.m_PlayersArea);
    }

    const auto params = table->GetAllAreas();

    // parse first image
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        //EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(params.m_AllAreas[1], cv::Rect(987, 205, 41, 40), cv::Rect(428, 162, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/1.xml");
    }

    // parse second
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, Wait4NewGame(_)).Times(Exactly(1)).WillOnce(Invoke([&](const clnt::ITableRecognizer::ICallback::ReadyFunc&){
            // emulate next image load to change button
            parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/3.xml");
        }));
        EXPECT_CALL(mocked, OnNewGameReady()).Times(Exactly(1));
        EXPECT_CALL(mocked, OnNextEventsNeeded(_)).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/2.xml");
    }

    // parse fourth
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(cv::Rect(1088, 224, 140, 70), cv::Rect(1059, 364, 41, 40), cv::Rect(1016, 254, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/4.xml");
    }

    // parse fifth
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(cv::Rect(1088, 224, 140, 70), cv::Rect(1059, 364, 41, 40), cv::Rect(1016, 254, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/5.xml");
    }

    // parse six
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(cv::Rect(94, 224, 140, 70), cv::Rect(1059, 364, 41, 40), cv::Rect(267, 253, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/6.xml");
    }

    // parse seven
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(cv::Rect(1088, 224, 140, 70), cv::Rect(1059, 364, 41, 40), cv::Rect(1016, 254, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/7.xml");
    }

    // parse eight
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, OnNextEventsNeeded(MakeEventsList(cv::Rect(877, 590, 140, 70), cv::Rect(1059, 364, 41, 40), cv::Rect(911, 512, 37, 46)))).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/8.xml");
    }

    // parse nine - last
    {
        MockedTableCallback mocked;
        callback.SetMockedCallback(&mocked);

        EXPECT_CALL(mocked, Wait4NewGame(_)).Times(Exactly(2));
        EXPECT_CALL(mocked, OnNextEventsNeeded(_)).Times(Exactly(1));
        EXPECT_CALL(mocked, OnNewGameReady()).Times(Exactly(1));
        parseFile("../../../poker/data/PokerStars/tests/client/logic/TableRecognize/9.xml");
    }
 
}