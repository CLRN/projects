﻿#include "../clientlib/GameRound.h"
#include "common/Log.h"
#include "xml/Node.h"
#include "Player.h"
#include "serialization/Serialization.h"
#include "serialization/Helpers.h"
#include "Actions.h"

#include <fstream>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Expectation;

pcmn::Player MakePlayer(const std::string& name = "test")
{
    pcmn::Player player;
    player.Name(name);
    player.Stack(1500);
    player.Cards(boost::assign::list_of(pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Diamonds))(pcmn::Card(pcmn::Card::Queen, pcmn::Suit::Spades)));
    return player;
}

TEST(Serialization, Player)
{
    pcmn::Player player;
    player.Name("test");
    player.Stack(1500);
    
    std::string data;
    {
        cmn::xml::Node node;
        player.Serialize(node);
        node.Save(data);
    }
    {
        const cmn::xml::Node node(data.c_str(), data.size());

        pcmn::Player another;
        another.Deserialize(node);

        EXPECT_EQ(player, another);
    }
}

TEST(Serialization, PlayerWithCards)
{
    const pcmn::Player player = MakePlayer();

    std::string data;
    {
        cmn::xml::Node node;
        player.Serialize(node);
        node.Save(data);
    }
    {
        const cmn::xml::Node node(data.c_str(), data.size());

        pcmn::Player another;
        another.Deserialize(node);

        EXPECT_EQ(player, another);
    }
}

class Holder : public cmn::srl::Serializable
{
public:
    Holder()
        : SERIALIZABLE("holder", FIELD(players))
    {

    }

    bool operator == (const Holder& a) const
    {
        return players == a.players;
    }

    std::map<std::string, pcmn::Player> players;
};

TEST(Serialization, PlayerMap)
{
    Holder h;
    h.players["1"] = MakePlayer("1");
    h.players["2"] = MakePlayer("2");
    h.players["3"] = MakePlayer("3");
    h.players["4"] = MakePlayer("4");
    h.players["5"] = MakePlayer("5");

    std::string data;
    {
        cmn::xml::Node node;
        h.Serialize(node);
        node.Save(data);
    }
    {
        const cmn::xml::Node node(data.c_str(), data.size());

        Holder another;
        another.Deserialize(node);

        EXPECT_EQ(h, another);
    }
}

TEST(Serialization, Actions)
{
    const std::string action = conv::cast<std::string>(pcmn::Action::Check);
    const pcmn::Action::Value back = conv::cast<pcmn::Action::Value>(action);
    EXPECT_EQ(action, "1");
    EXPECT_EQ(back, pcmn::Action::Check);
}