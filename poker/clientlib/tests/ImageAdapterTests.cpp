﻿#include "common/Log.h"
#include "xml/Node.h"
#include "../clientlib/ImageAdapter.h"
#include "../clientlib/TableRecognizer.h"
#include "../clientlib/TableParser.h"
#include "recognition/Recognition.h"
#include "rpc/Channel.h"

#include <memory>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/filesystem/operations.hpp>

using testing::Range;
using testing::Combine;
using testing::Values;

class EmptyEndpoint : public rpc::Endpoint
{
public:
    EmptyEndpoint(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}
    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id& id)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }
};


// image file name to valid events file name
typedef std::tuple<std::string, std::string> ParseAndValidateParams;

class AdapterTest 
    : public testing::TestWithParam<ParseAndValidateParams>
    , public clnt::ITableRecognizer::ICallback
{
public:

    void DoTest()
    {
        const auto events = m_Adapter->Parse(m_Image);

        if (!m_Valid)
        {
            cmn::xml::Node node;
            events->Serialize(node);
            node.Save2File(std::get<1>(GetParam()));
        }
        else
        {
            std::string valid;
            std::string test;

            m_Valid->Save(valid);

            cmn::xml::Node node;
            events->Serialize(node);
            node.Save(test);

            EXPECT_EQ(valid, test);
        }
    }

private:

    virtual void SetUp() override
    {
        m_Image = cv::imread(std::get<0>(GetParam()));
        const auto file = std::get<1>(GetParam());
        if (boost::filesystem::exists(file))
            m_Valid.reset(new cmn::xml::Node(file));

        m_Recognizer = rec::IRecognizer::Instance();
        m_DealerButton = cv::imread("../../../poker/data/PokerStars/objects/dealer.png");
        m_CardsBack = cv::imread("../../../poker/data/PokerStars/objects/cards_back.png");
        m_Table = clnt::ITableRecognizer::Instance(m_Log, EmptyEndpoint(m_Log, "test"), *this);
        m_Parser = clnt::ITableParser::Instance(m_Log, *m_Recognizer, *m_Table);
        m_Adapter = clnt::IImageAdapter::Instance(m_Log, *m_Recognizer, *m_Parser, m_DealerButton, m_CardsBack);

        // set up areas
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        m_Table->Deserialize(node);
        const auto params = m_Table->GetAllAreas();
        m_Adapter->Initialize(params);
    }

    virtual void TearDown() override
    {
        
    }

    virtual void Wait4NewGame(const ReadyFunc& func)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnNewGameReady()
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnNextEventsNeeded(const clnt::IImageManager::Event::List& evnt)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:
    Log m_Log;
    rec::IRecognizer::Ptr m_Recognizer;
    cv::Mat m_DealerButton;
    cv::Mat m_CardsBack;
    cv::Mat m_Image;
    std::unique_ptr<cmn::xml::Node> m_Valid;
    clnt::IImageAdapter::Ptr m_Adapter;
    clnt::ITableParser::Ptr m_Parser;
    clnt::ITableRecognizer::Ptr m_Table;
};

TEST_P(AdapterTest, Test)
{
    DoTest();
}


INSTANTIATE_TEST_CASE_P
(
    Prevalidated, 
    AdapterTest, 
    Values
    (
        ParseAndValidateParams("../../../poker/data/PokerStars/tests/client/recognition/wrong_flop/1.png", "../../../poker/data/PokerStars/tests/client/recognition/wrong_flop/1.xml"),
        ParseAndValidateParams("../../../data/tesseract/test_files/full_table.png", "../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/1.xml"),
        ParseAndValidateParams("../../../data/tesseract/test_files/full_table2.png", "../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/2.xml"),
        ParseAndValidateParams("../../../data/tesseract/test_files/full_table3.png", "../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/3.xml"),
        ParseAndValidateParams("../../../data/tesseract/test_files/full_table4.png", "../../../poker/data/PokerStars/tests/client/logic/AdapterTest/manual/4.xml")
    )
);