﻿#include "../clientlib/Game.h"
#include "TableLogic.h"
#include "common/Log.h"
#include "xml/Node.h"
#include "../clientlib/ProtoSerializer.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/filesystem.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Invoke;
using ::testing::Expectation;

class MockedTableLogicCallback : public pcmn::ITableLogicCallback
{
public:
    MOCK_METHOD2(SendRequest, void (const net::Packet& packet, bool statistics));
    MOCK_METHOD4(MakeDecision, void (const pcmn::Player& player, const pcmn::Player::Queue& activePlayers, const pcmn::TableContext& context, pcmn::Player::Position::Value position));
    MOCK_METHOD1(WriteStatistics, void (pcmn::TableContext::Data& data));
};

class MockedGameCallback : public clnt::IGame::ICallback
{
public:
    MOCK_METHOD1(OnPlayerFolded, void (const std::string& name));
    MOCK_METHOD0(OnNewStreet, void());
    MOCK_METHOD1(OnNewGame, void(bool));

    virtual bool IsPlayerInGame(const std::string& name)
    {
        return true;
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        if (name == "big") // this method will be called when flop test is in progress and we need to finish game and create new preflop round
            return 20;

        throw std::logic_error("The method or operation is not implemented.");
    }

};

class TestGame
{
public:

    typedef std::pair<std::string, unsigned> Player;

    template<typename T>
    TestGame(const T& p, const std::string& button, pcmn::Table::Phase::Value street)
        : m_Game(clnt::IGame::Instance(m_Log, m_Callback))
        , m_IsNeedDecision()
        , m_IsEndExpected()
        , m_InitialStreet(street)
    {
        for (unsigned i = 0; i < pcmn::Table::Phase::ToCardsCount(street); ++i)
            m_Cards.push_back(pcmn::Card(pcmn::Card::Value(i), pcmn::Suit::Spades));

        clnt::IGameRound::PlayerQueue players;
        boost::transform(p, std::back_inserter(players), [this](const Player& in){
            m_Players[in.first] = pcmn::Player(in.first).Stack(in.second);
            m_Game->SetPlayerParams(m_Players[in.first]);
            return in.first;
        });

        for (const auto& card : m_Cards)
            m_Game->PushFlopCard(card);

        m_Game->SetButton(button);
        m_Game->SetPlayers(players);
    }

    ~TestGame()
    {
        ValidateByServerLogic();
    }

    void Action(const std::string& name, pcmn::Action::Value action, unsigned amount)
    {
        if (action == pcmn::Action::Fold)
            EXPECT_CALL(m_Callback, OnPlayerFolded(name)).Times(Exactly(1));
        
        if (action != pcmn::Action::SmallBlind && action != pcmn::Action::BigBlind && action != pcmn::Action::Fold)
        {
            const unsigned diff = amount - m_Players[name].Bet();
            m_Players[name].Bet(m_Players[name].Bet() + diff);
            m_Players[name].Stack(m_Players[name].Stack() - diff);
        }
        else
        {
            m_Players[name].Bet(amount);
        }

        m_Game->PushAction(name, action, amount, m_Players[name].Stack());
    }

    void SerializeAndDeserialize()
    {
        std::string data;
        {
            cmn::xml::Node node;
            m_Game->Serialize(node);
            node.Save(data);
        }
        {
            const cmn::xml::Node node(data.c_str(), data.size());
            m_Game = clnt::IGame::Instance(m_Log, m_Callback);
            m_Game->Deserialize(node);
        }
    }

    void ValidateByServerLogic()
    {
        std::string data;
        if (!m_IsNeedDecision)
        {
            // that would be a statistics request, set invalid name to get assert if something fails in TableLogic
            pcmn::Player::ThisPlayer().Name("CLRN"); 

            EXPECT_CALL(m_Callback, OnNewGame(true)).Times(Exactly(1)).WillOnce(Invoke([this](bool){
                cmn::xml::Node node;
                m_Game->Serialize(node);
                node.Save(m_SerializedData);
            }));

            m_Game->Flush();
            data = m_SerializedData;
        }
        else
        {
            m_Game->Flush();

            SerializeAndDeserialize();

            cmn::xml::Node node;
            m_Game->Serialize(node);
            node.Save(data);
        }

        // make packet
        cmn::xml::Node node(data.c_str(), data.size());
        const auto packet = conv::IXml2Proto::Instance()->Convert(node);
        const auto debug = packet.DebugString();

        // validate by logic
        MockedTableLogicCallback callback;

        if (m_IsNeedDecision)
            EXPECT_CALL(callback, MakeDecision(pcmn::Player::ThisPlayer(), _, _, _)).Times(Exactly(1));
        else
            EXPECT_CALL(callback, WriteStatistics(_)).Times(Exactly(1));

        pcmn::TableLogic logic(m_Log, callback);
        logic.Parse(packet);
    }

    void NeedDecision()
    {
        m_IsNeedDecision = true;
    }

    void StreetMustBeFinished()
    {
        EXPECT_CALL(m_Callback, OnNewStreet()).Times(Exactly(1));

        m_Game->Flush(); // force new round creation

        // need to push cards from next street to emulate correct poker client
        const auto nextStreet = static_cast<pcmn::Table::Phase::Value>(m_InitialStreet + 1);

        // put flop cards
        for (unsigned i = 0; i < pcmn::Table::Phase::ToCardsCount(nextStreet); ++i)
            m_Game->PushFlopCard(pcmn::Card(pcmn::Card::Value(i + 5), pcmn::Suit::Clubs));
    }

private:

    MockedGameCallback m_Callback;
    Log m_Log;
    clnt::IGame::Ptr m_Game;
    std::map<std::string, pcmn::Player> m_Players;
    pcmn::Card::List m_Cards;
    bool m_IsNeedDecision;
    bool m_IsEndExpected;
    std::string m_SerializedData;
    const pcmn::Table::Phase::Value m_InitialStreet;
};

TEST(GameTest, Preflop)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("big", 1200))
        (TestGame::Player("last", 200))
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300)), 
        "button",
        pcmn::Table::Phase::Preflop
    );

    pcmn::Player::ThisPlayer().Name("button");
    test.NeedDecision();
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("last", pcmn::Action::Bet, 200);
}

TEST(GameTest, PreflopTwoPlayers)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("big", 1200))
        (TestGame::Player("small", 1300)), 
        "small",
        pcmn::Table::Phase::Preflop
    );

    pcmn::Player::ThisPlayer().Name("big");
    test.NeedDecision();
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("small", pcmn::Action::Call, 20);
}

TEST(GameTest, PreflopRaise)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop
    );

    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
}

TEST(GameTest, PreflopLongRound)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop
    );

    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Raise, 100);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("call", pcmn::Action::Call, 200);
}

TEST(GameTest, FlopRaise)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
}

TEST(GameTest, FlopLongRound)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::River
    );

    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Raise, 100);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("call", pcmn::Action::Call, 200);
}

TEST(GameTest, FlopLongRoundAndFold)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Turn
    );

    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Raise, 100);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("call", pcmn::Action::Fold, 0);
}

TEST(GameTest, PreflopAllIn)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 1300))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop
    );

    pcmn::Player::ThisPlayer().Name("call");
    test.StreetMustBeFinished();
    test.NeedDecision();
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 200);
    test.Action("button", pcmn::Action::Raise, 400);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 400);
}

TEST(GameTest, FlopAllIn)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 0))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
}

TEST(GameTest, Serialization)
{
    TestGame test(boost::assign::list_of
        (TestGame::Player("button", 1400))
        (TestGame::Player("small", 0))
        (TestGame::Player("big", 1200))
        (TestGame::Player("call", 2000))
        (TestGame::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
}