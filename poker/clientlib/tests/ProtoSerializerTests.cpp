﻿#include "../clientlib/ProtoSerializer.h"
#include "../clientlib/Game.h"
#include "../clientlib/GameRound.h"
#include "common/Log.h"
#include "xml/Node.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/filesystem.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Expectation;

class EmptyCallback : public clnt::IGame::ICallback
{
public:
    virtual void OnPlayerFolded(const std::string& name) override
    {
    }

    virtual void OnNewGame(bool) override
    {
        m_Game->Serialize(m_Node);
    }
    virtual void OnNewStreet() override
    {
    }
    virtual bool IsPlayerInGame(const std::string& name) override
    {
        return true;
    }

    const cmn::xml::Node& GetXml() const 
    {
        return m_Node;
    }

    void SetGame(const clnt::IGame::Ptr& game)
    {
        m_Game = game;
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:
    clnt::IGame::Ptr m_Game;
    cmn::xml::Node m_Node;
};

TEST(ProtoSerializer, Simple)
{
    Log log;

    // make some round data
    EmptyCallback cb;
    const auto game = clnt::IGame::Instance(log, cb);
    cb.SetGame(game);

    game->SetButton("third");

    game->SetPlayerParams(pcmn::Player("first").Stack(1500).Bet(0).State(pcmn::Player::State::Waiting));
    game->SetPlayerParams(pcmn::Player("second").Stack(1400).Bet(0).State(pcmn::Player::State::Waiting));
    game->SetPlayerParams(pcmn::Player("third").Stack(1600).Bet(0).State(pcmn::Player::State::Waiting));

    game->PushPlayerCard("first", pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Clubs));
    game->PushPlayerCard("first", pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Diamonds));

    game->PushFlopCard(pcmn::Card(pcmn::Card::King, pcmn::Suit::Clubs));
    game->PushFlopCard(pcmn::Card(pcmn::Card::King, pcmn::Suit::Diamonds));
    game->PushFlopCard(pcmn::Card(pcmn::Card::King, pcmn::Suit::Spades));
    game->SetPlayers(boost::assign::list_of("first")("second")("third"));

    game->PushAction("second", pcmn::Action::Fold, 0, 1400);
    game->PushAction("third", pcmn::Action::Fold, 0, 1600);
    game->PushAction("first", pcmn::Action::Raise, 100, 1400);
    game->Flush();

    const cmn::xml::Node& node = cb.GetXml();

    std::string test;
    node.Save(test);

    const auto data = conv::IXml2Proto::Instance()->Convert(node);

    const net::Packet::Table& info = data.info();
    EXPECT_EQ(info.players_size(), 3);

    EXPECT_EQ(info.players(0).name(), "first");
    EXPECT_EQ(info.players(1).name(), "second");
    EXPECT_EQ(info.players(2).name(), "third");

    EXPECT_EQ(info.players(0).stack(), 1500);
    EXPECT_EQ(info.players(1).stack(), 1400);
    EXPECT_EQ(info.players(2).stack(), 1600);

    EXPECT_EQ(info.players(0).cards(0), pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Clubs).ToEvalFormat());
    EXPECT_EQ(info.players(0).cards(1), pcmn::Card(pcmn::Card::Ace, pcmn::Suit::Diamonds).ToEvalFormat());
    EXPECT_EQ(info.players(1).cards_size(), 0);
    EXPECT_EQ(info.players(2).cards_size(), 0);

    EXPECT_EQ(info.button(), 2);
    EXPECT_EQ(info.cards(0), pcmn::Card(pcmn::Card::King, pcmn::Suit::Clubs).ToEvalFormat());
    EXPECT_EQ(info.cards(1), pcmn::Card(pcmn::Card::King, pcmn::Suit::Diamonds).ToEvalFormat());
    EXPECT_EQ(info.cards(2), pcmn::Card(pcmn::Card::King, pcmn::Suit::Spades).ToEvalFormat());

    EXPECT_EQ(data.phases_size(), 1);
    EXPECT_EQ(data.phases(0).actions_size(), 3);

    EXPECT_EQ(data.phases(0).actions(0).player(), 0);
    EXPECT_EQ(data.phases(0).actions(0).id(), pcmn::Action::Raise);
    EXPECT_EQ(data.phases(0).actions(0).amount(), 100);

    EXPECT_EQ(data.phases(0).actions(1).player(), 1);
    EXPECT_EQ(data.phases(0).actions(1).id(), pcmn::Action::Fold);
    EXPECT_EQ(data.phases(0).actions(1).amount(), 0);

    EXPECT_EQ(data.phases(0).actions(2).player(), 2);
    EXPECT_EQ(data.phases(0).actions(2).id(), pcmn::Action::Fold);
    EXPECT_EQ(data.phases(0).actions(2).amount(), 0);
}