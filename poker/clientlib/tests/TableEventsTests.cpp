#include "common/Log.h"
#include "xml/Node.h"
#include "../clientlib/TableEvents.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <opencv2/core/core.hpp>

const cv::Rect g_Button = cv::Rect(10, 10, 200, 100);
const cv::Rect g_Player = cv::Rect(100, 200, 10, 10);
const unsigned g_Bet = 100;
const bool g_State = true;
const cv::Rect g_Region = cv::Rect(200, 0, 20, 30);
const std::wstring g_Text = L"test text";
const int g_TableX = 1024;
const int g_TableY = 768;

class MockedReader : public clnt::ITableEvents
{
public:

    MOCK_METHOD2(OnTableSize, void(int width, int height));
    MOCK_METHOD2(OnRegionRecognized, void(const cv::Rect& rect, const std::wstring& text));
    MOCK_METHOD1(OnButtonFound, void(const cv::Rect& rect));
    MOCK_METHOD2(OnPlayerState, void(const cv::Rect& rect, bool isInGame));
    MOCK_METHOD2(OnPlayerBet, void(const cv::Rect& player, unsigned bet));

    virtual void Serialize(cmn::xml::Node& node) const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void Deserialize(const cmn::xml::Node& node)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual std::string GetNodeName() const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual bool IsEmpty() const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

};

TEST(Events, Serialize)
{
    Log log;
    std::string data;

    {
        cmn::xml::Node node;

        const auto writer = clnt::ITableEvents::Instance();

        writer->OnButtonFound(g_Button);
        writer->OnPlayerBet(g_Player, g_Bet);
        writer->OnPlayerState(g_Player, g_State);
        writer->OnRegionRecognized(g_Region, g_Text);
        writer->OnTableSize(g_TableX, g_TableY);

        writer->Serialize(node);
        node.Save(data);
    }

    {
        cmn::xml::Node node(data.c_str(), data.size());
        const auto reader = clnt::ITableEventsReader::Instance();
        
        MockedReader callback;
        reader->Deserialize(node);

        EXPECT_CALL(callback, OnButtonFound(g_Button));
        EXPECT_CALL(callback, OnPlayerBet(g_Player, g_Bet));
        EXPECT_CALL(callback, OnPlayerState(g_Player, g_State));
        EXPECT_CALL(callback, OnRegionRecognized(g_Region, g_Text));
        EXPECT_CALL(callback, OnTableSize(g_TableX, g_TableY));

        reader->Play(callback);
    }
}