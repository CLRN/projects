﻿#include "common/Log.h"
#include "conversion/AnyCast.h"
#include "common/Modules.h"
#include "rpc/Endpoint.h"
#include "common/FileSystem.h"
#include "xml/Node.h"
#include "common/ServiceHolder.h"

#include "Player.h"
#include "TableLogic.h"

#include "../clientlib/Client.h"
#include "../clientlib/ITableControl.h"

#include "../cachelib/ClientCache.h"

#include <opencv2/highgui/highgui.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <mutex>

#include <boost/assign/list_of.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/thread.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Invoke;
using ::testing::Expectation;

DECLARE_CURRENT_MODULE(Modules::Tools);

Log m_Log;

namespace gp = google::protobuf;

class EmptyClientTableControl : public clnt::ITableControl
{
public:
    virtual void Fold()
    {
        std::cout << "FOLD pressed" << std::endl;
    }

    virtual void CheckCall()
    {
        std::cout << "CHECK/CALL pressed" << std::endl;
    }

    virtual void BetRaise(unsigned amount)
    {
        std::cout << "BET/RAISE pressed, amount: " << amount << std::endl;
    }
};

class EmptyClientChannel : public rpc::details::IChannel
{
public:

    EmptyClientChannel()
    {
    }
    virtual rpc::IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, const gp::Message& request, const rpc::IStream&, const rpc::details::CallParams&) override
    {
        net::Reply response;
        const auto size = response.ByteSize();

        const auto stream = boost::make_shared<std::stringstream>();
        stream->write(reinterpret_cast<const char*>(&size), sizeof(size));
        CHECK(response.SerializeToOstream(stream.get()), cmn::Exception("Failed to serialize response"));
        stream->seekg(0);

        const auto future = rpc::IFuture::Instance();
        cmn::Service::Instance().post(boost::bind(&rpc::IFuture::SetData, future, stream));
        return future;
    }


private:
    net::Reply m_Reply;
};

class ClientCallback : public clnt::IClient::ICallback
{
public:
    template<typename T>
    ClientCallback(T&& files) : m_Files(files) {}

    virtual clnt::ITableEventsReader::Ptr GetNextEvents()
    {
        if (m_Files.empty())
            return clnt::ITableEventsReader::Ptr();

        cmn::xml::Node node(m_Files.front().wstring());

        LOG_TRACE("Processing events file: [%s]") % m_Files.front().wstring();

        m_Files.pop_front();
        const auto events = clnt::ITableEventsReader::Instance();
        events->Deserialize(node);
        return events;
    }

    virtual void OnNewGame()
    {

    }

private:
    std::list<boost::filesystem::path> m_Files;
};

class EmptyEndpoint : public rpc::Endpoint
{
public:
    EmptyEndpoint(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}
    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id&)
    {
        return m_Channel;
    }
private:
    EmptyClientChannel m_Channel;
};

TEST(TableRecognize, SavedGame1)
{
    //m_Log.Open("client.trace.cfg");

    pcmn::Player::ThisPlayer().Name("bot");

    const auto files = fs::GetFiles("../../../poker/data/PokerStars/tests/client/logic/SavedGame1", ".xml");
    ClientCallback cb(std::move(files));
    EmptyClientTableControl ctrl;
    EmptyEndpoint channel(m_Log, "tests");
    const auto client = clnt::IClient::Instance(m_Log, cb, channel, ctrl);
    client->Run();
}

TEST(TableRecognize, SavedGame2)
{
    //m_Log.Open("client.trace.cfg");

    pcmn::Player::ThisPlayer().Name("bot");

    const auto files = fs::GetFiles("../../../poker/data/PokerStars/tests/client/logic/SavedGame2", ".xml");
    ClientCallback cb(std::move(files));
    EmptyClientTableControl ctrl;
    EmptyEndpoint channel(m_Log, "tests");
    const auto client = clnt::IClient::Instance(m_Log, cb, channel, ctrl);
    client->Run();
}

TEST(TableRecognize, SavedGame3)
{
    //m_Log.Open("client.trace.cfg");

    pcmn::Player::ThisPlayer().Name("bot");

    const auto files = fs::GetFiles("../../../poker/data/PokerStars/tests/client/logic/SavedGame3", ".xml");
    ClientCallback cb(std::move(files));
    EmptyClientTableControl ctrl;
    EmptyEndpoint channel(m_Log, "tests");
    const auto client = clnt::IClient::Instance(m_Log, cb, channel, ctrl);
    client->Run();
}

TEST(TableRecognize, SavedGame4)
{
    //m_Log.Open("client.trace.cfg");

    pcmn::Player::ThisPlayer().Name("bot");

    const auto files = fs::GetFiles("../../../poker/data/PokerStars/tests/client/logic/SavedGame4", ".xml");
    ClientCallback cb(std::move(files));
    EmptyClientTableControl ctrl;
    EmptyEndpoint channel(m_Log, "tests");
    const auto client = clnt::IClient::Instance(m_Log, cb, channel, ctrl);
    client->Run();
}