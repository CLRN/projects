﻿#include "../clientlib/GameRound.h"
#include "../clientlib/Game.h"
#include "../clientlib/ProtoSerializer.h"
#include "../clientlib/TableSnapshot.h"
#include "common/Log.h"
#include "xml/Node.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>

using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Expectation;

class MockedRoundCallback : public clnt::IGameRound::ICallback
{
public:
    MOCK_METHOD1(OnPlayerFolded, void (const std::string& name));
    MOCK_METHOD3(OnStreetFinished, void(bool, const clnt::IGameRound::Action::List&, bool));

    virtual bool IsPlayerInGame(const std::string& name)
    {
        return true;
    }

    virtual unsigned ObtainBet(const std::string& name) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

};

class Round
{
public:

    typedef std::pair<std::string, unsigned> Player;

    template<typename T>
    Round(const T& p, const std::string& button, pcmn::Table::Phase::Value street, unsigned bb = 0)
        : m_Round(clnt::IGameRound::Instance(m_Log, m_Callback))
        , m_Before(clnt::ITableSnapshot::Instance(m_Log))
        , m_After(clnt::ITableSnapshot::Instance(m_Log))
        , m_InitialStreet(street)
    {
        pcmn::Player::List players;
        boost::transform(p, std::back_inserter(players), [this](const Player& in){
            pcmn::Player result;

            result.Name(in.first);
            result.Stack(in.second);
            m_Players[in.first].Stack(in.second);
            m_Players[in.first].Name(in.first);

            return result;
        });

        boost::for_each(players, boost::bind(&clnt::ITableSnapshot::SetPlayer, m_Before, _1));
        m_Before->SetButton(button);
        m_Before->SetBigBlind(bb);
        for (unsigned i = 0; i < pcmn::Table::Phase::ToCardsCount(street); ++i)
            m_Before->PushFlopCard(pcmn::Card(pcmn::Card::Value(i), pcmn::Suit::Value(0)));

        m_Round->Initialize(*m_Before);
        m_After->Copy(*m_Before);
    }

    ~Round()
    {
        try
        {
            SerializeAndDeserialize();

            const ::testing::TestInfo* const test_info = ::testing::UnitTest::GetInstance()->current_test_info();

            boost::filesystem::path path("../../../poker/data/PokerStars/tests/client/logic");
            path /= test_info->test_case_name();
            path /= test_info->name();
            path += ".xml";

#if 0
            cmn::xml::Node node;
            m_Round->Serialize(node);
            node.Save2File(path.wstring());
#else
            cmn::xml::Node node(path.wstring());
            const auto round = clnt::IGameRound::Instance(m_Log, m_Callback);
            round->Deserialize(node);
            m_Round->Compare(*round);
#endif
        }
        catch (const std::exception& e)
        {
            GTEST_MESSAGE_(conv::cast<std::string>(cmn::ExceptionInfo(e)).c_str(), ::testing::TestPartResult::kFatalFailure);
        }
    }

    void Action(const std::string& name, pcmn::Action::Value action, unsigned amount)
    {
        if (action != pcmn::Action::SmallBlind && action != pcmn::Action::BigBlind && action != pcmn::Action::Fold)
        {
            const unsigned diff = amount - m_Players[name].Bet();
            m_Players[name].Bet(m_Players[name].Bet() + diff);
            m_Players[name].Stack(m_Players[name].Stack() - diff);
        }
        else
        {
            m_Players[name].Bet(amount);
        }
        
        m_After->PushAction(name, action, amount, m_Players[name].Stack());
        m_After->SetPlayer(m_Players[name]);

        if (action == pcmn::Action::Fold)
            EXPECT_CALL(m_Callback, OnPlayerFolded(name)).Times(Exactly(1));
    }

    void ValidateQueue(const clnt::IGameRound::PlayerQueue& active)
    {
        m_Round->Play(*m_After);

        EXPECT_EQ(active, m_Round->GetQueue(*m_After));
    }

    void SerializeAndDeserialize()
    {
        std::string data;
        {
            cmn::xml::Node node;
            m_Round->Serialize(node);
            node.Save(data);
        }
        {
            const cmn::xml::Node node(data.c_str(), data.size());
            m_Round = clnt::IGameRound::Instance(m_Log, m_Callback);
            m_Round->Deserialize(node);
        }
    }

    void StreetMustBeFinished()
    {
        EXPECT_CALL(m_Callback, OnStreetFinished(false, clnt::IGameRound::Action::List(), true)).Times(Exactly(1));

        // need to push cards from next street to emulate correct poker client
        const auto nextStreet = static_cast<pcmn::Table::Phase::Value>(m_InitialStreet + 1);

        // put flop cards
        for (unsigned i = 0; i < pcmn::Table::Phase::ToCardsCount(nextStreet); ++i)
            m_Before->PushFlopCard(pcmn::Card(pcmn::Card::Value(i), pcmn::Suit::Value(i + 1)));
    }

    void GameMustBeFinished()
    {
        EXPECT_CALL(m_Callback, OnStreetFinished(true, clnt::IGameRound::Action::List(), true)).Times(Exactly(1));
    }

    void ValidateLast(const std::string& name)
    {
        m_Round->Play(*m_After);

        EXPECT_EQ(m_Round->GetQueue(*m_After).back(), name);
    }

private:

    MockedRoundCallback m_Callback;
    Log m_Log;
    clnt::IGameRound::Ptr m_Round;
    std::map<std::string, pcmn::Player> m_Players;
    const pcmn::Table::Phase::Value m_InitialStreet;

    clnt::ITableSnapshot::Ptr m_Before;
    clnt::ITableSnapshot::Ptr m_After;
};

TEST(GameRound, Preflop)
{
    Round test(boost::assign::list_of
        (Round::Player("big", 1200))
        (Round::Player("last", 200))
        (Round::Player("button", 1400))
        (Round::Player("small", 1300)), 
        "button",
        pcmn::Table::Phase::Preflop,
        20
    );

    pcmn::Player::ThisPlayer().Name("button");
    test.ValidateLast("big");
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("last", pcmn::Action::Bet, 200);
    test.ValidateLast("big");
    test.ValidateQueue(boost::assign::list_of("button")("small")("big"));
}

TEST(GameRound, PreflopTwoPlayers)
{
    Round test(boost::assign::list_of
        (Round::Player("big", 1200))
        (Round::Player("small", 1300)), 
        "small",
        pcmn::Table::Phase::Preflop,
        20
    );

    pcmn::Player::ThisPlayer().Name("big");
    test.ValidateLast("big");
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("small", pcmn::Action::Call, 20);
    test.ValidateQueue(boost::assign::list_of("big"));
}

TEST(GameRound, PreflopRaise)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop,
        20
    );

    test.GameMustBeFinished();
    test.ValidateLast("big");
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, PreflopLongRound)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop,
        20
    );

    test.GameMustBeFinished();
    test.ValidateLast("big");
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Raise, 100);
    test.ValidateLast("raise");
    test.Action("raise", pcmn::Action::Raise, 200);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, FlopRaise)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.GameMustBeFinished();
    test.ValidateLast("button");
    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, FlopLongRound)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::River
    );

    test.ValidateLast("button");
    test.GameMustBeFinished(); // because it's river
    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Raise, 100);
    test.ValidateLast("raise");
    test.Action("raise", pcmn::Action::Raise, 200);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, FlopLongRoundAndFold)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Turn
    );

    test.GameMustBeFinished();
    test.ValidateLast("button");
    test.Action("small", pcmn::Action::Check, 0);
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 40);
    test.Action("button", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Raise, 100);
    test.ValidateLast("raise");
    test.Action("raise", pcmn::Action::Raise, 200);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Fold, 0);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, PreflopAllIn)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 1300))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Preflop,
        20
    );

    pcmn::Player::ThisPlayer().Name("call");
    test.StreetMustBeFinished();
    test.ValidateLast("big");
    test.Action("small", pcmn::Action::SmallBlind, 10);
    test.Action("big", pcmn::Action::BigBlind, 20);
    test.Action("call", pcmn::Action::Call, 20);
    test.Action("raise", pcmn::Action::Bet, 200);
    test.Action("button", pcmn::Action::Raise, 400);
    test.Action("small", pcmn::Action::Fold, 0);
    test.Action("big", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("call", pcmn::Action::Call, 400);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, FlopAllIn)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 0))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.GameMustBeFinished();
    test.ValidateLast("button");
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}

TEST(GameRound, Serialization)
{
    Round test(boost::assign::list_of
        (Round::Player("button", 1400))
        (Round::Player("small", 0))
        (Round::Player("big", 1200))
        (Round::Player("call", 2000))
        (Round::Player("raise", 200)), 
        "button",
        pcmn::Table::Phase::Flop
    );

    test.GameMustBeFinished();
    test.ValidateLast("button");
    test.Action("big", pcmn::Action::Check, 0);
    test.Action("call", pcmn::Action::Bet, 20);
    test.Action("raise", pcmn::Action::Raise, 200);
    test.Action("button", pcmn::Action::Fold, 0);
    test.ValidateLast("call");
    test.Action("big", pcmn::Action::Fold, 0);
    test.Action("call", pcmn::Action::Call, 200);
    test.ValidateQueue(clnt::IGameRound::PlayerQueue());
}