#ifndef ImageManager_h__
#define ImageManager_h__

#include "recognition/Recognition.h"
#include "common/ILog.h"

#include <vector>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/function/function_fwd.hpp>

namespace clnt
{

class ITableRecognizer;
class IImageManager
{
public:
    struct Diff
    {
        typedef std::vector<Diff> List;
        cv::Mat m_Image;
        cv::Rect m_Area;
        bool operator == (const Diff& other) const;
        bool operator != (const Diff& other) const;
    };

    struct Snapshot
    {
        cv::Mat m_Before;
        cv::Mat m_After;
    };

    struct Event
    {
        typedef std::vector<Event> List;
        Event(const cv::Rect& r, bool isNeed2Recognize = false)
            : m_Region(r)
            , m_IsNeed2Recognize(isNeed2Recognize)
        {
        }
        bool operator == (const Event& other) const
        {
            return m_Region == other.m_Region && m_IsNeed2Recognize == other.m_IsNeed2Recognize;
        }
        cv::Rect m_Region;
        bool m_IsNeed2Recognize;
    };

    typedef boost::shared_ptr<IImageManager> Ptr;
    typedef boost::function<cv::Mat()> GetImageFn;

    virtual void SetRegions(const std::vector<cv::Rect>& regions) = 0;
    virtual Diff::List GetChanges(const cv::Mat& image) = 0; 
    virtual Snapshot Wait4Changes(const Event::List& areas, const GetImageFn& callback) = 0;

    static bool IsEqual(const cv::Mat& lhs, const cv::Mat& rhs);

    static Ptr Instance(ILog& log, rec::IRecognizer& recognizer);
};

} // namespace clnt

#endif // ImageManager_h__
