﻿#include "Client.h"
#include "common/Modules.h"
#include "xml/Node.h"
#include "exception/CheckHelpers.h"
#include "serialization/Helpers.h"
#include "recognition/Recognition.h"
#include "TableParser.h"
#include "TableRecognizer.h"

#include <map>

#include <opencv2/highgui.hpp>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/filesystem/operations.hpp>

namespace clnt
{

DECLARE_CURRENT_MODULE(Modules::Client);

class Client 
    : public IClient
    , boost::noncopyable
    , public clnt::ITableRecognizer::ICallback
{
public:
 
    Client(ILog& log, IClient::ICallback& cb, rpc::Endpoint& channel, ITableControl& ctrl)
        : m_Log(log)
        , m_Callback(cb)
        , m_Channel(channel)
        , m_Recognizer(rec::IRecognizer::Instance())
        , m_DealerButton(cv::imread("../../../poker/data/PokerStars/objects/dealer.png"))
        , m_CardsBack(cv::imread("../../../poker/data/PokerStars/objects/cards_back.png"))
        , m_Table(clnt::ITableRecognizer::Instance(m_Log, m_Channel, *this))
        , m_Parser(clnt::ITableParser::Instance(m_Log, *m_Recognizer, *m_Table))
    {
        m_Table->SetTableControl(ctrl);

        // load configuration file
        cmn::xml::Node node(std::string("../../../poker/data/PokerStars/config/config.xml"));
        m_Table->Deserialize(node);

        // set players area
        const auto areas = m_Table->GetAllAreas();
        m_Parser->SetPlayersArea(areas.m_PlayersArea);
    }

    virtual void Run() override
    {
        for (;;)
        {
            const auto events = m_Callback.GetNextEvents();
            if (!events)
                break;

            events->Play(*m_Parser);
            m_Table->Flush();
        }
    }

    virtual void Wait4NewGame(const ReadyFunc& func)
    {
        do
        {
            const auto events = m_Callback.GetNextEvents();
            if (!events)
                break;

            events->Play(*m_Parser);
        } 
        while (!func());
    }

    virtual void OnNewGameReady()
    {
        m_Callback.OnNewGame();
    }

    virtual void OnNextEventsNeeded(const IImageManager::Event::List& evnt)
    {
        ITableEventsReader::Areas areas;
        boost::transform(evnt, std::back_inserter(areas), boost::bind(&IImageManager::Event::m_Region, _1));

        for (;;)
        {
            const auto events = m_Callback.GetNextEvents();
            if (!events)
                break;

            events->Play(*m_Parser);
            if (events->IsExists(areas))
                break;
        }
    }

    virtual ITableParser& GetParser()
    {
        return *m_Parser;
    }

    virtual rec::IRecognizer& GetRecognizer()
    {
        return *m_Recognizer;
    }

    virtual ITableRecognizer& GetTable()
    {
        return *m_Table;
    }

private:
    ILog& m_Log;
    IClient::ICallback& m_Callback;
    rpc::Endpoint& m_Channel;

    rec::IRecognizer::Ptr m_Recognizer;
    cv::Mat m_DealerButton;
    cv::Mat m_CardsBack;
    cv::Mat m_Image;
    clnt::ITableRecognizer::Ptr m_Table;
    clnt::ITableParser::Ptr m_Parser;
};


IClient::Ptr IClient::Instance(ILog& log, ICallback& cb, rpc::Endpoint& channel, ITableControl& ctrl)
{
    return boost::make_shared<Client>(log, cb, channel, ctrl);
}


} // namespace clnt
