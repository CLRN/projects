﻿#include "TableParser.h"
#include "conversion/AnyCast.h"
#include "TableRecognizer.h"
#include "common/ILog.h"
#include "common/Modules.h"
#include "ImageManager.h"
#include "xml/Node.h"

#include <opencv2/highgui/highgui.hpp> 
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/scope_exit.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/variant.hpp>

namespace clnt
{

DECLARE_CURRENT_MODULE(Modules::Client);

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

class TableParser : public virtual ITableParser, boost::noncopyable
{
public:

    TableParser(ILog& log, rec::IRecognizer& recognizer, ITableRecognizer& table)
        : m_Log(log)
        , m_Recognizer(recognizer)
        , m_DealerButton(cv::imread("../../../poker/data/PokerStars/objects/dealer.png"))
        , m_CardsBack(cv::imread("../../../poker/data/PokerStars/objects/cards_back.png"))
        , m_Table(table)
        , m_PlayersArea()
    {
    }

    virtual void SetPlayersArea(const cv::Rect& area) override
    {
        m_PlayersArea = area;
    }

    unsigned NumberCast(std::wstring value) const
    {
        if (value == L"Allln")
            return 0;

        boost::algorithm::replace_all(value, L"I", L"1");
        boost::algorithm::replace_all(value, L"l", L"1");
        boost::algorithm::replace_all(value, L"o", L"0");
        boost::algorithm::replace_all(value, L"O", L"0");
        boost::algorithm::erase_all(value, L" ");
        boost::algorithm::erase_all(value, L",");
        boost::algorithm::erase_all(value, L".");
        boost::algorithm::erase_all(value, L"$");

        return conv::cast<unsigned>(value);
    }

    unsigned ObtainBet(const rec::IRecognizer::Result::List& recognized, const cv::Point& center) const override
    {
        auto result = recognized;
        boost::for_each(result, [](rec::IRecognizer::Result& r){
            boost::algorithm::erase_all(r.m_Text, L"★");
            boost::algorithm::erase_all(r.m_Text, L"♥");
            boost::algorithm::erase_all(r.m_Text, L"♣");
            boost::algorithm::erase_all(r.m_Text, L"♠");
            boost::algorithm::erase_all(r.m_Text, L"♦");
            boost::algorithm::trim(r.m_Text);
        });

        // we must sort results by distance to the center of table,
        // because bet sizes always closer to the the center than
        // chips. so closest result will be correct.
        static const boost::wregex moneyRegexp(std::wstring(L"([0-9oO]*,?[0-9oO]+)"));

        const auto bets = result | boost::adaptors::filtered([](const rec::IRecognizer::Result& res){
            return boost::regex_match(res.m_Text, moneyRegexp);
        });

        rec::IRecognizer::Result::List filtered;
        boost::copy(bets, std::back_inserter(filtered));

        // calculate distances between cards center and player bets
        typedef std::pair<double, rec::IRecognizer::Result> Pair;
        std::vector<Pair> distances;
        distances.reserve(filtered.size());

        boost::transform(filtered, std::back_inserter(distances), [&center](const rec::IRecognizer::Result& res){
            const auto result = cv::norm(
                cv::Point(res.m_Rect.x + res.m_Rect.width / 2, res.m_Rect.y + res.m_Rect.height / 2)
                -
                center
            );

            return std::make_pair(result, res);
        });
        boost::sort(distances, boost::bind(&Pair::first, _1) < boost::bind(&Pair::first, _2));
        return distances.empty() ? 0 : NumberCast(distances.front().second.m_Text);
    }

    virtual void OnRegionRecognized(const cv::Rect& rect, const std::wstring& input)
    {
        std::wstring text = input;
        boost::algorithm::erase_all(text, L"★");
        boost::algorithm::trim(text);

        if (text.empty())
            return;

        assert(m_PlayersArea.area());

        static const boost::wregex playerRegexp(std::wstring(L"(.+)\\n((\\$?\\d*\\s?,?\\d+)|(All\\s?ln)|(\\w*itting\\s?\\w*)|(Disconnected))"));
        static const boost::wregex cardRegexp(std::wstring(L"(J|Q|K|H|A|[2-9]|10|l0|I0)\\n(♠|♣|♥|♦)"));
        static const boost::wregex moneyRegexp(std::wstring(L"([0-9oO]*,?[0-9oO]+)"));
        static const boost::wregex potRegexp(std::wstring(L"Pot:\\s(\\d*,?\\d+)"));
        static const boost::wregex actionButtonRegexp(std::wstring(L"((Fold)|(Call)|(Check)|(Bet)|(Raise\\s?To))\\n?(\\d*,?\\d+)?"));

        bool isSomethingParsed = false;
        try
        {
            if (boost::regex_match(text, cardRegexp))
            {
                std::vector<std::wstring> values;
                boost::algorithm::split(values, text, boost::algorithm::is_any_of(L"\n"));
                if (values.size() != 2)
                    return;
                    
                const std::wstring& value = values.front();
                const std::wstring& suit = values.back();

                pcmn::Card card;
                card.m_Suit = pcmn::Suit::FromString(suit.front());

                if (value.size() == 1 && boost::algorithm::is_alpha()(value.front()))
                    card.m_Value = pcmn::Card::FromString(static_cast<char>(value.front()));
                else
                    card.m_Value = static_cast<pcmn::Card::Value>(NumberCast(value));
            
                m_Table.OnCardChanged(rect, card);

                isSomethingParsed = true;
            }
            else
            if (boost::regex_match(text, actionButtonRegexp) && rect.y > m_PlayersArea.br().y)
            {
                std::vector<std::wstring> values;
                boost::algorithm::split(values, text, boost::algorithm::is_any_of(L"\n"));
                if (values.size() != 1 && values.size() != 2)
                    return;

                boost::algorithm::erase_all(values.front(), L"To");
            
                unsigned amount = 0;
                if (values.size() == 2)
                    amount = NumberCast(values.back());

                const pcmn::Action::Value action = pcmn::Action::FromString(conv::cast<std::string>(values.front()));
                if (action != pcmn::Action::Fold && 
                    action != pcmn::Action::Check && 
                    action != pcmn::Action::Call && 
                    action != pcmn::Action::Raise && 
                    action != pcmn::Action::Bet)
                    return;

                // validate parsed action
                if ((action == pcmn::Action::Fold || action == pcmn::Action::Check) && amount ||
                    (action == pcmn::Action::Call || action == pcmn::Action::Bet || action == pcmn::Action::Raise) && !amount)
                {
                    isSomethingParsed = false;
                }
                else
                {
                    m_Table.OnActionButtonChanged(rect, action, amount);
                    isSomethingParsed = true;
                }
            }

            if (!isSomethingParsed && boost::regex_match(text, playerRegexp) && !m_AreaWithoutPlayers.contains(rect.tl()))
            {
                std::vector<std::wstring> values;
                boost::algorithm::split(values, text, boost::algorithm::is_any_of(L"\n"));
                if (values.size() < 2)
                    return;

                const std::string name = conv::cast<std::string>(*(&values.back() - 1));
                const std::wstring& stack = values.back();
                const auto action = pcmn::Action::FromString(name);

                if (pcmn::Action::FromString(conv::cast<std::string>(stack)) == pcmn::Action::Afk)
                {
                    m_Table.OnPlayerAfk(rect, name);
                }
                else
                {
                    // player text may contain action instead of name
                    // try to convert text to action first
                    if (action != pcmn::Action::Unknown && pcmn::Action::IsUseful(action))
                        m_Table.OnPlayerAction(rect, action, NumberCast(stack));
                    else 
                    if (action == pcmn::Action::Unknown)
                        m_Table.OnPlayerChanged(rect, name, NumberCast(stack));
                }

                isSomethingParsed = true;
            }
            else
            if (!isSomethingParsed && boost::regex_match(text, moneyRegexp))
            {
                m_Table.OnBetChanged(rect, NumberCast(text));
                isSomethingParsed = true;
            }
            else
            if (!isSomethingParsed && boost::regex_match(text, potRegexp))
            {
                m_Table.OnPotChanged(rect, NumberCast(boost::erase_all_copy(text, L"Pot: ")));
                isSomethingParsed = true;
            }       	
        }
        catch (const std::exception& e)
        {
        	LOG_ERROR("Failed to parse region, error: %s") % cmn::ExceptionInfo(e);
        }
    }

    virtual void OnTableSize(int width, int height)
    {
        m_Table.OnTableSize(width, height);
    }

    virtual void OnButtonFound(const cv::Rect& rect)
    {
        m_Table.OnDealerButtonChanged(rect);
    }

    virtual void OnPlayerState(const cv::Rect& rect, bool isInGame)
    {
        m_Table.OnPlayerState(rect, isInGame);
    }

    virtual void OnPlayerBet(const cv::Rect& player, unsigned bet)
    {
        m_Table.OnBetChanged(player, bet);
    }

    virtual void Serialize(cmn::xml::Node& node) const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void Deserialize(const cmn::xml::Node& node)
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual std::string GetNodeName() const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual bool IsEmpty() const
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:

    ILog& m_Log;
    rec::IRecognizer& m_Recognizer;
    ITableRecognizer& m_Table;
    cv::Mat m_CurrentImage;
    cv::Mat m_DealerButton;
    cv::Mat m_CardsBack;
    cv::Rect m_PlayersArea; // space(rectangle) between all players(rectangle where button will be searched)
    cv::Rect m_AreaWithoutPlayers; // table center area where players are impossible
    cv::Rect m_DealerButtonPosition;
};

#pragma warning(pop)

ITableParser::Ptr ITableParser::Instance(ILog& log, rec::IRecognizer& recognizer, ITableRecognizer& table)
{
    return boost::make_shared<TableParser>(log, recognizer, table);
}

} // namespace clnt
