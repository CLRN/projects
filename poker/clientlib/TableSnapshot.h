#ifndef TableSnapshot_h__
#define TableSnapshot_h__

#include "common/ILog.h"

#include "Player.h"
#include "Table.h"

#include <boost/shared_ptr.hpp>

namespace clnt
{

class IDiff;
typedef boost::shared_ptr<IDiff> DiffPtr;
class ITableSnapshot
{
public:
    typedef boost::shared_ptr<ITableSnapshot> Ptr;

    virtual ~ITableSnapshot() {}

    virtual void SetPlayer(const pcmn::Player& p) = 0;
    virtual void RemovePlayer(const pcmn::Player& p) = 0;
    virtual void PushAction(const std::string& name, pcmn::Action::Value value, unsigned amount, unsigned stack) = 0;
    virtual void PushFlopCard(const pcmn::Card& card) = 0;
    virtual void SetButton(const std::string& name) = 0;
    virtual void SetBigBlind(unsigned bb) = 0;
    virtual void Apply(const IDiff& diff) = 0;
    virtual void Copy(const ITableSnapshot& other) = 0;
    virtual void SetSequence(const std::deque<std::string>& names) = 0;
    virtual void SetStreet(pcmn::Table::Phase::Value street) = 0;

    //! Reset player states, bets and actions
    virtual void Reset() = 0;

    //! Clear flop and reset snapshot
    virtual void Clear() = 0;
    virtual void PushPlayerCard(const std::string& name, const pcmn::Card& card) = 0;

    virtual pcmn::Player::List GetPlayers() const = 0;
    virtual const std::string& GetButton() const = 0;
    virtual pcmn::Table::Phase::Value GetStreet() const = 0;
    virtual unsigned GetBigBlind() const = 0;
    virtual const pcmn::Card::List& GetFlop() const = 0;

    virtual DiffPtr GetDiff(const ITableSnapshot& old) const = 0;
    virtual std::string GetLogInfo() const = 0;

    static Ptr Instance(ILog& log);
};

} // namespace clnt
#endif // TableSnapshot_h__
