﻿#include "TableDiff.h"
#include "conversion/AnyCast.h"
#include "common/ILog.h"
#include "common/Modules.h"

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm.hpp>

namespace clnt
{

DECLARE_CURRENT_MODULE(Modules::Client);

class TableDiff : public IDiff, boost::noncopyable
{
public:
    typedef std::map<std::string, pcmn::Player> Players;

    TableDiff(ILog& log, const pcmn::Player::List& players, const PlayerAction::List& actions, const std::string& newButton)
        : m_Log(log)
        , m_Actions(actions)
        , m_NewButton(newButton)
    {
        boost::transform(players, std::inserter(m_Players, m_Players.end()), [](const pcmn::Player& p){
            return std::make_pair(p.Name(), p);
        });
    }

    const pcmn::Player* GetPlayer(const std::string& player) const override
    {
        const auto it = m_Players.find(player);
        return it != m_Players.end() ? &it->second : nullptr;
    }

    virtual bool GetAction(std::string& name, unsigned& amount, pcmn::Action::Value& action, unsigned& stack) const override
    {
        if (m_Actions.empty())
            return false;

        auto& front = m_Actions.front();

        name    .swap(front.m_Name);
        amount  = front.m_Amount;
        action  = front.m_Action;
        stack   = front.m_StackAfter;

        m_Actions.pop_front();
        return true;
    }

    virtual std::string GetLogInfo() override
    {
        std::ostringstream oss;
        oss << "diff: ";

        oss << ", actions: ";
        for (const auto& a : m_Actions)
            oss << "action: " << a.ToString() << " ";

        if (!m_NewButton.empty())
            oss << "button: " << m_NewButton << " ";

        for (const auto& p : m_Players)
            oss << "player: " << p.second << " ";

        return oss.str();
    }

    virtual bool Empty() const override
    {
        return m_Players.empty() && m_Actions.empty() && m_Flop.empty();
    }

    virtual bool HasActions() const override
    {
        return !m_Actions.empty();
    }

    const std::string& GetNewButton() const override
    {
        return m_NewButton;
    }

private:

    ILog& m_Log;

    Players m_Players;
    mutable PlayerAction::List m_Actions;
    const pcmn::Card::List m_Flop;
    const std::string m_NewButton;
};


IDiff::Ptr IDiff::Instance(ILog& log, const pcmn::Player::List& players, const PlayerAction::List& actions, const std::string& newButton)
{
    return boost::make_shared<TableDiff>(log, players, actions, newButton);
}


IDiff::PlayerAction::PlayerAction(const std::string& name /*= ""*/, pcmn::Action::Value v /*= pcmn::Action::Unknown*/, unsigned amount /*= 0*/, unsigned stackAfter /*= 0*/) : m_Name(name)
    , m_Action(v)
    , m_Amount(amount)
    , m_StackAfter(stackAfter)
{

}

std::string IDiff::PlayerAction::ToString() const
{
    std::ostringstream oss;
    oss << m_Name << ":" << m_Action << "(" << m_Amount << ")=" << m_StackAfter;
    return oss.str();
}

bool IDiff::PlayerAction::operator==(const PlayerAction& other) const
{
    return m_Name == other.m_Name && m_Action == other.m_Action && m_Amount == other.m_Amount && m_StackAfter == other.m_StackAfter;
}

} // namespace clnt
