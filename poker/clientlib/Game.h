#ifndef Game_h__
#define Game_h__

#include "common/ILog.h"
#include "GameRound.h"
#include "serialization/ISerializable.h"
#include "Actions.h"

#include <vector>

#include <boost/shared_ptr.hpp>

namespace clnt
{

class IGame : public virtual cmn::srl::ISerializable
{
public:
    typedef boost::shared_ptr<IGame> Ptr;
    typedef std::vector<IGameRound::Ptr> Rounds;

    __declspec(novtable) class ICallback
    {
    public:
        virtual void OnPlayerFolded(const std::string& name) = 0;
        virtual void OnNewStreet() = 0;
        virtual void OnNewGame(bool wait4Button) = 0;
        virtual bool IsPlayerInGame(const std::string& name) = 0;
        virtual unsigned ObtainBet(const std::string& name) = 0;
    };

    virtual ~IGame() {}

    virtual void Flush() = 0;
    virtual void Load(const std::string& fileName) = 0;
    virtual bool IsEmpty() const = 0;
    virtual const std::string& GetLastPlayer() = 0;
    virtual const std::string& GetSmallBlindPlayer() = 0;
    virtual pcmn::Table::Phase::Value GetPhase() const = 0;

    virtual void SetPlayers(const IGameRound::PlayerQueue& players) = 0;
    virtual void SetPlayerParams(const pcmn::Player& player) = 0;
    virtual void PushAction(const std::string& name, pcmn::Action::Value value, unsigned amount, unsigned stack) = 0;
    virtual void SetButton(const std::string& name) = 0;
    virtual void PushPlayerCard(const std::string& name, const pcmn::Card& card) = 0;
    virtual void PushFlopCard(const pcmn::Card& card) = 0;
    virtual void Clear() = 0;

    static Ptr Instance(ILog& log, ICallback& cb);
};

} // namespace clnt
#endif // Game_h__
