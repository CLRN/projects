#ifndef IReaction_h__
#define IReaction_h__

#include "Actions.h"
#include "BetSize.h"
#include "Hand.h"
#include "Player.h"

#include <boost/noncopyable.hpp>

namespace srv
{

//! Player reaction on different actions abstraction
class IReaction : boost::noncopyable
{
public:

    virtual ~IReaction() {}

    virtual float Get(pcmn::Action::Value action, pcmn::BetSize::Value bet) const = 0;

    virtual float Get(pcmn::Action::Value action, pcmn::BetSize::Value bet, pcmn::Player::Count::Value inPot) const = 0;
};

} // namespace srv


#endif // IReaction_h__
