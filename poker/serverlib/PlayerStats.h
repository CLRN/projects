#ifndef PlayerStats_h__
#define PlayerStats_h__

#include "Player.h"
#include "common/ILog.h"
#include "HandsDetector.h"
#include "IStatistics.h"
#include "IReaction.h"
#include "Cards.h"
#include "Table.h"

#include <memory>

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>

namespace srv
{

class PlayerStats : public pcmn::Player, boost::noncopyable
{
public:

    //! Pointer type
    typedef boost::shared_ptr<PlayerStats> Ptr;

    //! List type
    typedef std::list<Ptr> List;

    PlayerStats(const pcmn::Player& opponent, ILog& logger, const pcmn::Player& bot, const pcmn::Card::List& flop);
    PlayerStats(const PlayerStats& player);

    //! Fetch all statistics associated with this player
    void Fetch(IStatistics& stats, unsigned playerCount, pcmn::Table::Phase::Value street);

    //! Get player reaction
    const IReaction& GetReaction() const;

    //! Get win rate against this player
    float GetBotWinRate() const;

    //! Get player position
    Position::Value GetPosition() const;

    //! Set player position
    void SetPosition(Position::Value pos);

    //! Get all reactions
    const IStatistics::PlayerInfo::BetMap& GetReactionMap() const;

    //! Get possible hands
    const HandsDetector::Result& GetPossibleHands() const;

private:

    float GetWinRate(const pcmn::Card::List& botCards, const pcmn::Hand::Value hand) const;
    float GetWinRate(const pcmn::Card::List& botCards) const;

private:

    //! Position
    Position::Value m_Position;

    //! Possible hands
    HandsDetector::Result m_Hands;

    //! Reactions on bets
    IStatistics::PlayerInfo::BetMap m_ReactionsOnBets;

    //! Win rate against this player
    float m_BotWinRate;

    //! Player reaction
    std::unique_ptr<IReaction> m_Reaction;

    //! Bot reference
    const pcmn::Player& m_Bot;

    //! Flop cards
    const pcmn::Card::List& m_Flop;

    //! Logger
    ILog& m_Log;

    //! Fetch mutex
    boost::mutex m_FetchMutex;
};

} // namespace srv

#endif // PlayerStats_h__
