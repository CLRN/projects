#ifndef PokerServer_h__
#define PokerServer_h__

#include <memory>
#include <string>

namespace srv
{

class Server
{
public:
	Server(const std::string& ep);
	~Server();
	void Run();

private:
	class Impl;
	Impl* m_Impl;

};

} // namespace srv


#endif // PokerServer_h__
