#ifndef ReactionByHand_h__
#define ReactionByHand_h__

#include "IReaction.h"
#include "common/ILog.h"
#include "IStatistics.h"

#include <boost/thread/mutex.hpp>

namespace srv
{

class PlayerStats;

class ReactionByHand : public IReaction
{
public:

    ReactionByHand(ILog& logger, const PlayerStats& player);

    virtual float Get(pcmn::Action::Value action, pcmn::BetSize::Value bet) const override;
    virtual float Get(pcmn::Action::Value action, pcmn::BetSize::Value bet, pcmn::Player::Count::Value inPot) const override;

private:

    virtual void Recalculate();

protected:

    ILog& m_Logger;
    const PlayerStats& m_Player;
    IStatistics::PlayerInfo::BetMap m_Bets;
    bool m_Recalculated;
    mutable boost::mutex m_Mutex;
};

class PreflopReactionByHand : public ReactionByHand
{
public:
    PreflopReactionByHand(ILog& logger, const PlayerStats& player);
    virtual float Get(pcmn::Action::Value action, pcmn::BetSize::Value bet, pcmn::Player::Count::Value inPot) const override;
};

} // namespace srv


#endif // ReactionByHand_h__
