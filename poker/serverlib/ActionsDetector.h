#ifndef ActionsDetector_h__
#define ActionsDetector_h__

#include "common/ILog.h"
#include "Hand.h"
#include "BetSize.h"
#include "Player.h"
#include "IStatistics.h"
#include "Board.h"

#include <map>
#include <string>

namespace srv
{

class ActionsDetector
{
public:

    //! Input data type
    typedef std::map<pcmn::Hand::Value, float> InputHands;

    //! Output data type
    typedef std::map<pcmn::BetSize::Value, float> Chances;

    ActionsDetector(ILog& logger, const IStatistics& stats);

    void GetFoldChance
    (
        const std::string& name, 
        pcmn::Player::Position::Value pos, 
        const InputHands& hands, 
        unsigned street,
        unsigned playerCount,
        pcmn::Board::Value board,
        Chances& result
    );

private:

    ILog& m_Log;
    const IStatistics& m_Stats;
};

} // namespace srv

#endif // ActionsDetector_h__
