#include "Server.h"
#include "common/Log.h"
#include "packet.pb.h"
#include "exception/CheckHelpers.h"
#include "common/Modules.h"
#include "MongoStatistics.h"
#include "EVDecisionMaker.h"
#include "Evaluator.h"
#include "../neuro/DatabaseReader.h"
#include "../statslib/StatisticsCache.h"
#include "../cachelib/ClientCache.h"
#include "TableLogic.h"
#include "common/ServiceHolder.h"
#include "net/PipeHost.h"
#include "rpc/Server.h"
#include "server.pb.h"

#include <iostream>
#include <atomic>

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>

namespace srv
{

DECLARE_CURRENT_MODULE(Modules::Server);

class Server::Impl : public net::PokerService
{
    //! Statistics to thread id map
    typedef std::map<boost::thread::id, boost::shared_ptr<IStatistics>> StatisticMap;

public:
	Impl(const std::string& ep) 
		: m_Host(boost::make_shared<net::pipes::Transport>(m_Log))
        , m_Statistics()
		, m_RequestsCount(0)
        , m_Server(rpc::IServer::Instance(m_Log, *m_Host, ep, "poker server"))
	{
        m_Log.Open("server.trace.cfg");

        pcmn::Player::ThisPlayer().Name("CLRN");

        stats::Cache::Create(m_Log);
        ClientCache::Create(m_Log);

        m_Server->ProvideService(*this);
        m_Server->SetOnClientConnection(boost::bind(&Impl::OnConnection, this, _1, _2));
        m_Server->Start();

        LOG_INFO("Listening on: %s") % ep;
	}

    void Run()
    {
        cmn::Service::Holder holder(30);
        cmn::Service::Instance().run();
        m_Server->Stop();
    }

private:

    void OnConnection(const boost::exception_ptr& e, const rpc::InstanceId& instance)
    {
        LOG_INFO("Client %s") % (e ? "disconnected" : "connected");
    }

    virtual void SaveGame(const rpc::Request<::net::Packet>::Ptr& request, const rpc::Response<::net::EmptyResponse>::Ptr& /*response*/) override
    {
        SCOPED_LOG(m_Log);

        LOG_DEBUG("Request: [%s]") % request->DebugString();

        {
            const auto decisionMaker = boost::make_shared<EVDecisionMaker>(m_Log, GetStatistics(), net::Reply());
            pcmn::TableLogic logic(m_Log, *decisionMaker);
            logic.Parse(*request);
        }

        LOG_INFO("Requests processed: [%s]") % ++m_RequestsCount;
    }

    virtual void GetAction(const rpc::Request<::net::Packet>::Ptr& request, const rpc::Response<::net::Reply>::Ptr& response) override
    {
        SCOPED_LOG(m_Log);

        LOG_DEBUG("Request: [%s]") % request->DebugString();

        {
            const auto decisionMaker = boost::make_shared<EVDecisionMaker>(m_Log, GetStatistics(), *response);
            pcmn::TableLogic logic(m_Log, *decisionMaker);
            logic.Parse(*request);
        }

        LOG_INFO("Requests processed: [%s]") % ++m_RequestsCount;
    }

    IStatistics& GetStatistics()
    {
        boost::mutex::scoped_lock lock(m_StatsMutex);
        StatisticMap::iterator it = m_Statistics.find(boost::this_thread::get_id());
        if (it == m_Statistics.end())
            it = m_Statistics.insert(std::make_pair(boost::this_thread::get_id(), boost::shared_ptr<IStatistics>(new MongoStatistics(m_Log)))).first;
        return *it->second.get();
    }

private:

	//! Logger
	Log									m_Log;

	//! Client
	net::IHost::Ptr			            m_Host;

    //! Server side rpc instance
    rpc::IServer::Ptr                   m_Server;

	//! Server statistics
	StatisticMap						m_Statistics;

    //! Statistics mutex
    boost::mutex                        m_StatsMutex;

	//! Requests count
	std::atomic<std::size_t>			m_RequestsCount;
};

void Server::Run()
{
	m_Impl->Run();
}

Server::Server(const std::string& ep) : m_Impl(new Impl(ep))
{

}

Server::~Server()
{
	delete m_Impl;
}

}