#ifndef EVDecisionMaker_h__
#define EVDecisionMaker_h__

#include "TableLogic.h"
#include "common/ILog.h"
#include "WinRate.h"
#include "Player.h"
#include "IStatistics.h"
#include "HandsDetector.h"
#include "PlayerStats.h"
#include "Table.h"
#include "packet.pb.h"

#include <map>

#include <boost/noncopyable.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/ptime.hpp>

namespace pcmn
{
	class Evaluator;
}

namespace srv
{

//! Expected value based decision maker
class EVDecisionMaker : public pcmn::ITableLogicCallback, boost::noncopyable, public boost::enable_shared_from_this<EVDecisionMaker>
{
    struct TableDesc
    {
        //! Player hands map
        typedef std::map<std::string, pcmn::Hand::Value> Hands;

        //! Table list
        typedef std::list<TableDesc> List;

        TableDesc(const pcmn::TableContext& context);

        //! Get possible actions for current player 
        pcmn::Action::List GetPossibleActions(const pcmn::Player& player, unsigned street) const;

        //! Get bet size from future player action
        pcmn::BetSize::Value BetFromFutureAction(pcmn::Action::Value action, const pcmn::Player& player, unsigned BB) const;

        //! Get current player position
        pcmn::Player::Position::Value GetCurrentPosition() const;

        //! Process current player decision
        void ProcessDecision(pcmn::Action::Value id, pcmn::BetSize::Value amount, unsigned bet, unsigned botStack);

        //! Calculate table EV
        void CalculateEV(const EVDecisionMaker& maker);

        //! Is bot in pot
        bool IsPlayerInPot(const std::string& name) const;

        //! Set recursion level
        void SetRecursionLevel(unsigned level);

        unsigned m_Pot;                         //!< Current pot size
        unsigned m_Bet;                         //!< Current max bet
        float m_EV;                             //!< Expected value from this table
        float m_WinRate;                        //!< Bot win rate
        PlayerStats::List m_InPot;              //!< Players already in pot
        PlayerStats::List m_Queue;              //!< Players without bets, first player - current
        double m_Percent;                       //!< Total chance of this flop
        Hands m_Hands;                          //!< Players hands
        pcmn::Action::Value m_LastAggression;   //!< Last most aggressive action
        pcmn::BetSize::Value m_LastRaise;       //!< Last aggression bet amount
        unsigned m_BotStack;                    //!< Size of the bot stack at the final
        pcmn::Action::Value m_BotAction;        //!< Bot first action
        pcmn::BetSize::Value m_BotBetSize;      //!< First bot action bet size
#ifdef _DEBUG
        std::vector<std::string> m_Events;
#endif // _DEBUG
        unsigned m_RaiseCount;                  //!< Number of raises
        unsigned m_RecursionCount;              //!< Number of recursion calls on this table
    };
public:
    struct ActionAndBet
    {
        pcmn::Action::Value m_Action;
        pcmn::BetSize::Value m_Bet;
        bool operator < (const ActionAndBet& other) const;
    };

    struct Result
    {
        Result() : m_EV(), m_Summ(), m_AverageEV() {}
        double m_EV;
        EVDecisionMaker::TableDesc::List m_Tables;

        double m_Summ;
        double m_AverageEV;
    };

public:
	EVDecisionMaker
	(
		ILog& logger, 
		IStatistics& stats, 
        net::Reply& response
	);

private:

	virtual void MakeDecision(const pcmn::Player& player, 
		const pcmn::Player::Queue& activePlayers,
		const pcmn::TableContext& context,
		pcmn::Player::Position::Value position) override;

    virtual void SendRequest(const net::Packet& packet, bool statistics) override;
    virtual void WriteStatistics(pcmn::TableContext::Data& data) override;

private:

    //! Get all bet sizes by context and action
    template<typename T>
    std::vector<pcmn::BetSize::Value> GetBetSizes(pcmn::Action::Value action, const T& ctx);

    //! Prepare opponents
    void PrepareOpponents(const pcmn::Player::List& activePlayers);

    //! Get EV for stack size
    float GetEVForStackSize(unsigned stack) const;

    //! Process table
    void ProcessTable(const TableDesc& table);

    //! Process occurred player event
    void ProcessPast(TableDesc& table);

    //! Process possible player event
    void ProcessFuture(const TableDesc& table);

    //! Make decision by bot
    void ProcessBot(const TableDesc& table);

    //! Process player
    void ProcessPlayer(const TableDesc& table, const PlayerStats::Ptr& player);

    //! Get average win rate
    float GetAverageWinRate(unsigned opponentsCount) const;

    //! Process deferred tables asynchronously
    void ProcessDeferredTables();

private:

	//! Logger
	ILog& m_Log;

	//! Statistics
	srv::IStatistics& m_Stat;

    //! Bot
    pcmn::Player m_Bot;

    //! Flop cards
    pcmn::Card::List m_Flop;

    //! Big blind amount
    unsigned m_BigBlind;

    //! Opponents
    PlayerStats::List m_Opponents;

    //! Max bet before player
    unsigned m_MaxBet;

    //! Current street
    pcmn::Table::Phase::Value m_Street;

    //! Sum of the all stacks
    unsigned m_StacksSum;

    //! Bot total win rates for different number of opponents
    mutable std::vector<float> m_BotWinRates;

    //! Result tables
    TableDesc::List m_Tables;

    //! Deferred tables which will be processed asynchronously
    TableDesc::List m_DeferredTables;

    //! Decision response
    net::Reply& m_Response;

    //! Tables mutex
    boost::mutex m_TablesMutex;

    //! Request timeout(maximum time of processing)
    boost::posix_time::ptime m_TimeoutTime;
};

} // namespace srv
#endif // EVDecisionMaker_h__