#include "PlayerStats.h"
#include "IStatistics.h"
#include "ReactionByHand.h"
#include "Evaluator.h"
#include "common/Modules.h"
#include "../statslib/StatisticsCache.h"
#include "../statslib/PlayerStatistics.h"

#include <boost/unordered_map.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/scope_exit.hpp>
#include <boost/assign/list_of.hpp>

namespace srv
{

DECLARE_CURRENT_MODULE(Modules::Server);

PlayerStats::PlayerStats(const pcmn::Player& opponent, ILog& logger, const pcmn::Player& bot, const pcmn::Card::List& flop) 
    : pcmn::Player(opponent)
    , m_Log(logger)
    , m_Position()
    , m_Bot(bot)
    , m_Flop(flop)
    , m_BotWinRate()
{

}

PlayerStats::PlayerStats(const PlayerStats& player)
    : pcmn::Player(player)
    , m_Log(player.m_Log)
    , m_Position()
    , m_Bot(player.m_Bot)
    , m_Flop(player.m_Flop)
    , m_BotWinRate()
{

}

void PlayerStats::Fetch(IStatistics& stats, unsigned playerCount, pcmn::Table::Phase::Value street)
{
    SCOPED_LOG(m_Log);

    boost::unique_lock<boost::mutex> lock(m_FetchMutex);

    if (!m_Hands.empty() && !m_ReactionsOnBets.empty() && m_Reaction)
        return; // already got

    // set reaction at exit
    PlayerStats& instance = *this;
    BOOST_SCOPE_EXIT(&m_Reaction, &m_Log, &instance, &street)
    {
        // determine player reaction
        // for now assuming that player is playing by hand
        if (street == pcmn::Table::Phase::Preflop)
            m_Reaction.reset(new PreflopReactionByHand(m_Log, instance));
        else
            m_Reaction.reset(new ReactionByHand(m_Log, instance));
    }
    BOOST_SCOPE_EXIT_END;

    // get cached player data
    const stats::IPlayer::Ptr thisPlayer = stats::Cache::Instance().GetPlayerStatistics(Name());

    // get possible hands
    stats::IPlayer::Id id;
    id.m_Actions = GetActions();
    id.m_Board = m_Flop;

    m_Hands = thisPlayer->GetHands(id);
    if (m_Hands.size() == 1 && m_Hands.count(pcmn::Hand::Unknown))
        m_Hands.clear();

    // get reactions
    m_ReactionsOnBets = thisPlayer->GetReaction(street, pcmn::Board(m_Flop).Parse().GetValue(), m_Position, pcmn::Player::Count::FromValue(playerCount));
}

const IReaction& PlayerStats::GetReaction() const
{
    return *m_Reaction;
}

float PlayerStats::GetBotWinRate() const
{
    // calculate win rate
    if (m_BotWinRate)
        return m_BotWinRate;
   
    // get win rate from possible hands
    float result = 0;

    if (m_Hands.empty())
    {
        result = GetWinRate(m_Bot.Cards());
    }
    else
    {
        // for each expected hand
        for (const HandsDetector::Result::value_type& handAndPercent : m_Hands)
        {
            // calculate bot win percentage first
            const float winRate = GetWinRate(m_Bot.Cards(), handAndPercent.first);

            // total rate
            result += winRate * handAndPercent.second;
        }
    }
    const_cast<float&>(m_BotWinRate) = result;  
    return m_BotWinRate;
}

pcmn::Player::Position::Value PlayerStats::GetPosition() const
{
    return m_Position;
}

float PlayerStats::GetWinRate(const pcmn::Card::List& botCards) const
{
    SCOPED_LOG(m_Log);

    std::vector<int> flop;

    for (const pcmn::Card& card : m_Flop)
        flop.push_back(card.ToEvalFormat());

    static const std::vector<short> ranges(1, cfg::CARD_DECK_SIZE);
    return pcmn::Evaluator::Instance().GetEquity(m_Bot.Cards().front().ToEvalFormat(), m_Bot.Cards().back().ToEvalFormat(), flop, ranges) / 100.0f;
}

float PlayerStats::GetWinRate(const pcmn::Card::List& botCards, const pcmn::Hand::Value hand) const
{
    SCOPED_LOG(m_Log);

    return pcmn::Evaluator::Instance().GetEquity(m_Bot.Cards().front().ToEvalFormat(), m_Bot.Cards().back().ToEvalFormat(), m_Flop, boost::assign::list_of(hand)) / 100.0f;
}

void PlayerStats::SetPosition(Position::Value pos)
{
    m_Position = pos;
}

const IStatistics::PlayerInfo::BetMap& PlayerStats::GetReactionMap() const
{
    return m_ReactionsOnBets;
}

const HandsDetector::Result& PlayerStats::GetPossibleHands() const
{
    return m_Hands;
}

} // namespace srv