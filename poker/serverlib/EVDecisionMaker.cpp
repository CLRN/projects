﻿#include "EVDecisionMaker.h"
#include "common/Modules.h"
#include "../statslib/StatisticsCache.h"
#include "common/ServiceHolder.h"
#include "../cachelib/ClientCache.h"

#include <boost/scope_exit.hpp>
#include <boost/make_shared.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/range/algorithm.hpp>


// полное мат. ожидание - сумма мат ожиданий по каждому игроку
// мат ожидание по игроку - сумма всех исходов 
// для каждой возможной ставки считаем подное мат. ожидание, берем максимальное значение

// мат. ожидание для bet:
// для каждого игрока мат. ожидание это сумма мат. ожиданий для всех его предполагаемых рук умноженное на вероятность руки и поделеное на кол-во игроков:
// M[player] = ∑ P(hand) * M[hand] / count

// мат. ожидание руки считаетя как:
// M[hand] = M[call] + M[fold] + M[raise]
// M[call] = ((банк + ставка бота + колл оппонента) * вероятность выигрыша бота + (- ставка бота) * вероятность проигрыша бота) * вероятность колла
// M[fold] = банк * вероятность фолда
// M[raise] = (расчитывается мат. ожидание колла бота для рейза High) * вероятность рейза

// мат. ожидание для call
// для каждой предполагаемой руки оппонента:
// M[player] = ∑ P(hand) * M[hand]
// M[hand] = (размер банка + размер ставки оппонента) * вероятность выигрыша бота + (- размер ставки оппонента) * вероятность проигрыша бота


// полный просчет всех возможных рук и действий оппонентов:
// передача данных в стеке обработки осуществляется с помощью стркутуры:
// struct Table
// {
//     unsigned m_Pot;              //!< Current pot size
//     unsigned m_Bet;              //!< Current max bet
//     pcmn::Player::List m_InPot;  //!< Players already in pot
//     pcmn::Player::List m_Queue;  //!< Players without bets, first player - current
//     double m_Percent;            //!< Total chance of this flop
// };
// 
// Итак, представим ситуацию, за столом 3 игрока, player1, bot, player2, player1 делает бет 20 и пот становится равным 100:
// на вход поступает стол:
// m_Pot = 100
// m_Bet = 20
// { }
// { player1, bot, player2 }
// 1.0f
// прогоняем этот стол по всем игрокам, начиная с первого:
// предположим что у обоих игроков возможны 3 руки, с вероятностями 30, 30 и 40%
// тогда для каждой руки расчитаем новый стол созданный из поданного на вход,
// для первой руки:
// помещаем руку в m_InPot, а вероятность этого стола умножаем на вероятность предполагаемой руки:
// m_InPot.push_back(hand1);    = { hand1 }
// m_Percent *= hand1Chance;    = { 0.3f }
// m_Queue.pop_front();         = { bot, player2 }
// подаем на вход следующему игроку этот стол, т.к. это бот, то он начинает просчитывать решение, 
// предполагается что рука первого игрока ему известна, т.к. она есть в данных стола
// далее он начинает просчитывать свои действия и следующих за ним оппонентов, 
// бот начинает просчитывать рейз 100:
// m_Pot += 100 = 200
// m_Bet = 100
// m_Queue.pop_front(); 
// тут в случае рейза помимо извлечения из очереди нужно добавить в конец очереди первого игрока m_Queue = { player2, player1 }
// вероятность не трогаем т.к. в действии бота мы уверенны, т.е. оно считается детерминированным
// подаем на вход следующему игроку и для каждого возможного действия:
// предположим что текущее действие call(50%), помещаем это действие в структуру игрока и вытаскиваем статистику с учетом этого действия
// m_Percent *= callChance = { 0.15f } // учитываем шанс call в текущем столе
// далее определяем руку как будто игрок совершил call, далее для каждой руки делаем копию стола:
// m_Pot += 100 = 300
// m_InPot.push_back(hand1)                      // поместили предполагаемую руку второго игрока
// m_Percent *= hand1Chance;    = { 0.045f }      // умножили на шанс руки второго игрока
// m_Queue.pop_front();         = { player1 }
// передаем стол дальше, к следующему игроку в очереди:
// смотрим возможные реакции игрока на произошедшие события, предположим что получили 50/50 call/fold
// рассмотрим call:
// m_Percent *= callChance = { 0.0225f } // учитываем шанс call в текущем столе
// m_Pot += 100 = 400
// помещаем это действие в структуру игрока и вытаскиваем статистику с учетом этого действия
// далее определяем руку как будто игрок совершил call, далее для каждой руки делаем копию стола:
// предположим что тут рука оппонента точно известна, заменяем руку игрока 2 в структуре m_InPot новой рукой
//
// все ставки уравнены, можно расчитывать EV действия:
// имеем пот 400 и 2 оппонента с руками определенными со второго раза, т.е. более точными
// считаем шансы бота на победу против этих двух рук и получаем EV, сохраняем стол вместе с действием, ставкой бота и EV в список столов, 
// обрабатываем следующее возможное действие player1 и т.д.


// в итоге:
// получаем список столов, сумма вероятностей каждого должна равняться 1.0f! у каждого стола есть действие, ставка бота и EV
// группируем столы по действию и ставке 
// для каждой группы получаем полное EV = ∑ tableTotalChance * tableEV, т.е. по всем столам суммируем произведения вероятности стола на его EV
// получаем несколько действий со ставками и полное EV, сортируем по EV и выбираем наиболее оптимальное действие и ставку бота

// важное:
// 1. тянем статистику по рукам только если действие оппонента уже совершено до бота или предполпагается
// 2. из-за большого кол-ва комбинаций действий статистика может загнуться, поэтому нужен кэш


namespace srv
{

DECLARE_CURRENT_MODULE(Modules::Server);

enum { RECURSION_COUNT_LIMIT = 1 };
enum { MAX_TABLE_COUNT = 1000 * 1000 };
#ifdef _DEBUG
enum { TIMEOUT_SECONDS = 90 };
#else
enum { TIMEOUT_SECONDS = 10 };
#endif // _DEBUG

class TimedOut {};
class TooMuchTables {};

EVDecisionMaker::EVDecisionMaker(ILog& logger, IStatistics& stats, net::Reply& response)
    : m_Log(logger)
    , m_Stat(stats)
    , m_BigBlind()
    , m_MaxBet()
    , m_Street()
    , m_StacksSum()
    , m_Response(response)
    , m_TimeoutTime(boost::posix_time::second_clock::local_time() + boost::posix_time::seconds(TIMEOUT_SECONDS))
{

}

void EVDecisionMaker::MakeDecision(const pcmn::Player& player, const pcmn::Player::Queue& activePlayers, const pcmn::TableContext& context, const pcmn::Player::Position::Value position)
{
    SCOPED_LOG(m_Log);

    m_MaxBet = context.m_MaxBet;
    m_Street = static_cast<pcmn::Table::Phase::Value>(context.m_Street);
    assert(m_Street <= 3);
    m_StacksSum = context.m_TotalMoney;
    assert(m_StacksSum);

    m_Bot = player;
    CHECK(m_Bot.Cards().size() == 2, cmn::Exception("Can't make decision without info about bot cards"), m_Bot.Cards().size());

    m_BigBlind = context.m_BigBlind;
    CHECK(m_BigBlind, cmn::Exception("Can't make decision without info about big blind size"));

    m_Flop.clear();
    for (const int card : context.m_Data.m_Flop)
        m_Flop.push_back(pcmn::Card().FromEvalFormat(card));

    PrepareOpponents(context.m_Data.m_PlayersData);

    TableDesc table(context);

    std::deque<std::string> queue = context.m_QueueOnLastStreet;
    if (queue.size() > 2 && m_Street == pcmn::Table::Phase::Preflop)
    {
        // remove blinds
        if (queue.front() == queue[queue.size() - 2])
            queue.pop_front();
        if (queue.front() == queue.back())
            queue.pop_front();
    }

    for (const std::string& name : queue)
    {
        for (const pcmn::Player& player : activePlayers)
        {
            if (player.Name() == name)
            {
                table.m_Queue.push_back(boost::make_shared<PlayerStats>(player, m_Log, m_Bot, m_Flop));
                break;
            }
        }
    }

    // make sure that all players who make actions got correct state
    auto it = table.m_Queue.rbegin();
    const auto end = table.m_Queue.rend();
    for (; it != end; ++it)
    {
        if ((*it)->State() == pcmn::Player::State::Called)
        {
            // set this state to all previous players in the queue
            for ( ; it != end; ++it)
                (*it)->State(pcmn::Player::State::Called);
            break;
        }
    }

    try
    {
        ProcessTable(table);
    }
    catch (const TimedOut&)
    {
        LOG_ERROR("Unable to process initial tables, too much time spent on preparation of the table");
    }

    LOG_INFO("Processing [%s] tables asynchronously") % m_DeferredTables.size();

    ProcessDeferredTables();

    LOG_INFO("Processing EV for [%s] tables") % m_Tables.size();

    unsigned counter = 0;

    // find out best EV
    ActionAndBet best = {};
    {
        std::map<ActionAndBet, Result> evs;

        TableDesc perfectTable(context);

        // group tables by first bot action
        for (TableDesc& table : m_Tables)
        {
            table.CalculateEV(*this);

            const ActionAndBet key = { table.m_BotAction, table.m_BotBetSize };
            auto& data = evs[std::move(key)];
            data.m_EV += table.m_Percent * table.m_EV;
            data.m_Summ += table.m_Percent;
            data.m_Tables.push_back(table);

            if (table.m_EV > perfectTable.m_EV)
                perfectTable = table;
        }

        LOG_INFO("Perfect table, EV: %s, percent: %s, action: %s, amount: %s, pot: %s, hands: %s, rate: %s") 
            % perfectTable.m_EV 
            % perfectTable.m_Percent 
            % perfectTable.m_BotAction 
            % perfectTable.m_BotBetSize 
            % perfectTable.m_Pot 
            % perfectTable.m_Hands
            % perfectTable.m_WinRate;
        LOG_INFO("Calculated EVs: %s") % evs;

        double bestEV = 0.0f;
        for (const auto& pair : evs)
        {
            if (pair.second.m_EV > bestEV)
            {
                bestEV = pair.second.m_EV;
                best = pair.first;
            }
        }

        for (auto& pair: evs)
        {
            pair.second.m_Tables.sort([](const TableDesc& lhs, const TableDesc& rhs)
                {
                    return rhs.m_Percent * rhs.m_EV < lhs.m_Percent * lhs.m_EV;
                }
            );

            double summ = 0;
            for (const auto& table : pair.second.m_Tables)
                summ += table.m_EV;

            pair.second.m_AverageEV = summ / pair.second.m_Tables.size();
        }
    }

    // set response
    {
        unsigned bet = best.m_Action == pcmn::Action::Call ? context.m_MaxBet : pcmn::BetSize::FromValue(best.m_Bet, context.m_Pot, m_Bot.Stack(), m_Bot.Bet(), m_BigBlind);
        if (bet > 10)
            bet = (bet / 10) * 10;

        m_Response.set_action(best.m_Action);
        m_Response.set_amount(bet);

        LOG_INFO("Decision: [%s], bet: [%s], cards: [%s], flop: [%s]") % best.m_Action % bet % m_Bot.Cards() % m_Flop;
    }
}

void EVDecisionMaker::SendRequest(const net::Packet& /*packet*/, bool /*statistics*/)
{
    throw std::exception("The method or operation is not implemented.");
}

void EVDecisionMaker::WriteStatistics(pcmn::TableContext::Data& data)
{
    m_Stat.Write(data);
}

template<typename T>
std::vector<pcmn::BetSize::Value> EVDecisionMaker::GetBetSizes(pcmn::Action::Value action, const T& ctx)
{
    std::vector<pcmn::BetSize::Value> result;

    switch (action)
    {
    case pcmn::Action::Fold:
    case pcmn::Action::Check:
        result.push_back(pcmn::BetSize::NoBet);
        break;
    case pcmn::Action::Call:
        result.push_back(pcmn::BetSize::FromDecision(m_Bot.Bet(), ctx.m_Bet, ctx.m_Pot, m_Bot.Stack(), m_BigBlind));
        break;
    case pcmn::Action::Bet:
        result.push_back(pcmn::BetSize::Low);
    case pcmn::Action::Raise:
        if (ctx.m_RaiseCount >= 2) 
        {
            result.push_back(pcmn::BetSize::Huge);
            break;
        }
        result.push_back(pcmn::BetSize::Normal);
        result.push_back(pcmn::BetSize::High);
        result.push_back(pcmn::BetSize::Huge);
    	break;
    default: assert(false);
    }

    return result;
}

void EVDecisionMaker::PrepareOpponents(const pcmn::Player::List& activePlayers)
{
    m_Opponents.clear();
    
    for (const pcmn::Player& player : activePlayers)
        m_Opponents.push_back(boost::make_shared<PlayerStats>(player, m_Log, m_Bot, m_Flop));

    // prefetch players data
    typedef boost::unique_future<void> Future;
    typedef boost::promise<void> Promise;
    typedef std::map<std::string, Promise> Promises;
    typedef std::vector<Future> Futures;

    Promises promises;

    Futures futures;

    for (const pcmn::Player& player : activePlayers)
    {
        const std::string& name = player.Name();
        promises.insert(std::make_pair(name, Promise()));

        const auto functor = [&promises, name]()
        {
            stats::Cache::Instance().GetPlayerStatistics(name);
            promises[name].set_value();
        };
        cmn::Service::Instance().post(functor);
    }

    // obtain futures
    for (auto& pair : promises)
        futures.push_back(pair.second.get_future());

    const auto isReady = [&futures]()
    {
        for (const auto& future : futures)
        {
            if (!future.is_ready())
                return false;
        }
        return true;
    };

    while (!isReady())
        cmn::Service::Instance().poll_one();

    // calculate positions
    unsigned i = 0;
    for (PlayerStats::Ptr& player : m_Opponents)
    {
        unsigned leftInQueue = m_Opponents.size() - i;

        pcmn::Player::Position::Value result = pcmn::Player::Position::Middle;

        float step = static_cast<float>(m_Opponents.size()) / 3;
        if (step < 1)
            step = 1;

        if (leftInQueue <= step)
            result = pcmn::Player::Position::Later;
        else
        if (leftInQueue >= step * 2.5f)
            result = pcmn::Player::Position::Early;

        player->SetPosition(result);
        ++i;
    }
}

float EVDecisionMaker::GetEVForStackSize(unsigned stack) const
{
    // wins: 5$, 3$, 2$, hard coded now
    const float firstPlace = static_cast<float>(stack) / m_StacksSum;
    float secondPlace = 0;

    for (const PlayerStats::Ptr& p : m_Opponents)
        secondPlace += (static_cast<float>(p->Stack()) / m_StacksSum) * (static_cast<float>(m_Bot.Stack()) / (m_StacksSum - p->Stack()));

    const float diff = 1.0f - (firstPlace + secondPlace);

    const float thirdPlace = m_Opponents.size() <= 3 ? diff : diff / m_Opponents.size();

    return firstPlace * 5.0f + secondPlace * 3.0f + thirdPlace * 2.0f;
}

void EVDecisionMaker::ProcessTable(const TableDesc& table)
{
    // if there are no players this branch is finished
    if (
        table.m_Queue.size() < 2 && table.m_InPot.empty() || 
        table.m_Queue.empty() || 
        !table.IsPlayerInPot(m_Bot.Name())
        )
    {
        boost::unique_lock<boost::mutex> lock(m_TablesMutex);

        if (!(m_Tables.size() % 100000))
            LOG_INFO("Processed [%s] tables") % m_Tables.size();

        // street finished, save this table
        m_Tables.push_back(table);

        if (m_Tables.size() > MAX_TABLE_COUNT)
            throw TooMuchTables();
        return;
    }

    if (table.m_RecursionCount == RECURSION_COUNT_LIMIT)
    {
        m_DeferredTables.push_back(table);
        return;
    }

    if (boost::posix_time::second_clock::local_time() > m_TimeoutTime)
        throw TimedOut();

    // copy table
    TableDesc nestedTable(table);
    ++nestedTable.m_RecursionCount;

    // get current player
    const PlayerStats::Ptr current = nestedTable.m_Queue.front();

    // determine player status
    if (current->Name() == m_Bot.Name())
    {
        // process bot decision
        ProcessBot(nestedTable);
    }
    else
    if (current->State() == pcmn::Player::State::Called)
    {
        // this player done some actions, we can determine which hands he may have and process each of them
        ProcessPast(nestedTable);
    }
    else
    {
        // this player is waiting for his turn to play, we can determine all possible player moves and process each of them
        ProcessFuture(nestedTable);
    }
}

void EVDecisionMaker::ProcessPast(TableDesc& table)
{
    // get current player
    const PlayerStats::Ptr current = table.m_Queue.front();

    // get last player action
    CHECK(current->GetActions().size() > m_Street, cmn::Exception("Missing player actions for street"), current->GetActions().size(), m_Street, current->Name());
    CHECK(!current->GetActions()[m_Street].empty(), cmn::Exception("Empty player actions on street"), m_Street, current->Name());

    const pcmn::Player::ActionDesc& action = current->GetActions()[m_Street].back();
#ifdef _DEBUG
    table.m_Events.push_back(std::string("past:") + current->Name() + std::string(":") + pcmn::Action::ToString(action.m_Id));
#endif // _DEBUG

    // process player action
    table.ProcessDecision(action.m_Id, action.m_Amount, current->Bet(), m_Bot.Stack());

    // this player done some actions, we can determine which hands he may have and process each of them
    if (action.m_Id == pcmn::Action::Fold)
        ProcessTable(table);
    else
        ProcessPlayer(table, current);
}

void EVDecisionMaker::ProcessFuture(const TableDesc& table)
{
    // total players still in game
    const unsigned stillInGame = table.m_Queue.size() + table.m_InPot.size();

    // players in pot 
    const pcmn::Player::Count::Value inPot = pcmn::Player::Count::FromValue(table.m_InPot.size());

    // get current player
    const PlayerStats::Ptr current = table.m_Queue.front();

    // process each possible player action
    current->SetPosition(table.GetCurrentPosition());

    current->Fetch(m_Stat, stillInGame, m_Street);

    // for each possible action
    const pcmn::Action::List actions = table.GetPossibleActions(*current, m_Street);
    for (const pcmn::Action::Value action : actions)
    {
        // get correct last raise value
        auto lastRaise = table.m_LastRaise;
        if (lastRaise == pcmn::BetSize::High && current->Stack() > table.m_Bet * 20)
            lastRaise = pcmn::BetSize::Normal;
        else
        if (lastRaise == pcmn::BetSize::Huge && current->Stack() > table.m_Bet * 5)
            lastRaise = pcmn::BetSize::High;

        // get this action chance
        const float possibleActionChance = current->GetReaction().Get(action, lastRaise, inPot);
        if (!possibleActionChance)
            continue; // this action is impossible

        // copy table
        TableDesc tableWithAction(table);

        // get possible bet size for this action
        pcmn::BetSize::Value bet = table.BetFromFutureAction(action, *current, m_BigBlind);

        // bet amount
        unsigned betSize = action == pcmn::Action::Call ? table.m_Bet : pcmn::BetSize::FromValue(bet, table.m_Pot, current->Stack(), current->Bet(), m_BigBlind);
        if (betSize > current->Stack())
            betSize = current->Stack();

        // if this is call action bet size is always determined, so obtain bet description from bet size
        if (action == pcmn::Action::Call) 
            bet = pcmn::BetSize::FromAction(betSize, tableWithAction.m_Pot, current->Stack(), m_BigBlind);

        // push this action to player structure
        const PlayerStats::Ptr playerWithPossibleAction = boost::make_shared<PlayerStats>(*current, m_Log, m_Bot, m_Flop);
        playerWithPossibleAction->PushAction
        (
            m_Street, 
            action,
            bet,
            tableWithAction.GetCurrentPosition(),
            table.m_LastAggression,
            table.m_LastRaise,
            pcmn::Player::Count::FromValue(table.m_InPot.size() + table.m_Queue.size())
        );
        playerWithPossibleAction->Stack(playerWithPossibleAction->Stack() - betSize);
        playerWithPossibleAction->Bet(betSize);

        // replace last player
        tableWithAction.m_Queue.front() = playerWithPossibleAction;

#ifdef _DEBUG
        tableWithAction.m_Events.push_back(current->Name() + std::string(":") + conv::cast<std::string>(possibleActionChance) + std::string(":") + pcmn::Action::ToString(action));
#endif // _DEBUG

        // process player action by table
        tableWithAction.ProcessDecision(action, bet, betSize, m_Bot.Stack());

        // multiply table chance by this action chance
        tableWithAction.m_Percent *= possibleActionChance;

        // process player
        if (action == pcmn::Action::Fold)
            ProcessTable(tableWithAction);
        else
            ProcessPlayer(tableWithAction, playerWithPossibleAction);
    }
}

void EVDecisionMaker::ProcessBot(const TableDesc& table)
{
    // total players still in game
    const unsigned stillInGame = table.m_Queue.size() + table.m_InPot.size();

    // get current player
    const PlayerStats::Ptr current = table.m_Queue.front();

    // for each possible action
    const pcmn::Action::List actions = table.GetPossibleActions(*current, m_Street);
    for (const pcmn::Action::Value action : actions)
    {
        // get all possible bet sizes
        const std::vector<pcmn::BetSize::Value> bets = GetBetSizes(action, table);
        for (const pcmn::BetSize::Value bet : bets)
        {
            // copy table
            TableDesc tableWithAction(table);
            if (tableWithAction.m_BotAction == pcmn::Action::Unknown)
            {
                tableWithAction.m_BotAction = action;
                tableWithAction.m_BotBetSize = bet;
            }

            // bet amount
            unsigned betSize = action == pcmn::Action::Call ? tableWithAction.m_Bet : pcmn::BetSize::FromValue(bet, tableWithAction.m_Pot, current->Stack(), current->Bet(), m_BigBlind);
            if (betSize > current->Stack())
                betSize = current->Stack();

            // push this action to player structure
            const PlayerStats::Ptr botWithAction = boost::make_shared<PlayerStats>(*current, m_Log, m_Bot, m_Flop);
            botWithAction->PushAction
            (
                m_Street, 
                action,
                bet,
                tableWithAction.GetCurrentPosition(),
                tableWithAction.m_LastAggression,
                tableWithAction.m_LastRaise,
                pcmn::Player::Count::FromValue(tableWithAction.m_InPot.size() + tableWithAction.m_Queue.size())
            );
            botWithAction->Stack(botWithAction->Stack() - betSize);
            botWithAction->Bet(betSize);
            tableWithAction.m_BotStack = botWithAction->Stack();

            // replace last player
            tableWithAction.m_Queue.front() = botWithAction;
    #ifdef _DEBUG
            tableWithAction.m_Events.push_back(current->Name() + std::string(":") + pcmn::Action::ToString(action));
    #endif // _DEBUG

            // process player action by table
            tableWithAction.ProcessDecision(action, bet, betSize, m_Bot.Stack());

            // process table
            ProcessTable(tableWithAction);
        }
    }
}
 
void EVDecisionMaker::ProcessPlayer(const TableDesc& table, const PlayerStats::Ptr& player)
{
    // total players still in game
    const unsigned stillInGame = table.m_Queue.size() + table.m_InPot.size(); 

    player->SetPosition(table.GetCurrentPosition());
    player->Fetch(m_Stat, stillInGame, m_Street);

    // for each player hand sorted by chance
    using Pair = std::pair<pcmn::Hand::Value, float>;
    std::vector<Pair> sorted;
    boost::copy(player->GetPossibleHands(), std::back_inserter(sorted));
    boost::sort(sorted, boost::bind(&Pair::second, _1) < boost::bind(&Pair::second, _2));

    for (const HandsDetector::Result::value_type& handAndPercent : boost::reverse(sorted))
    {
        TableDesc copy(table);

        // multiply this table chance by hand chance
        copy.m_Percent *= handAndPercent.second;

        // save this hand in table info
        copy.m_Hands[player->Name()] = handAndPercent.first;

        // process nested table
        ProcessTable(copy);
    }
}

float EVDecisionMaker::GetAverageWinRate(unsigned opponentsCount) const
{
    if (!opponentsCount)
        return 100.0f;

    if (opponentsCount < 2)
        opponentsCount = 2;

    if (m_BotWinRates.size() > opponentsCount && m_BotWinRates[opponentsCount])
        return m_BotWinRates[opponentsCount];

    const std::vector<short> ranges(opponentsCount, cfg::CARD_DECK_SIZE);
    std::vector<int> flop;
    for (const auto& c : m_Flop)
        flop.push_back(c.ToEvalFormat());

    m_BotWinRates.resize(opponentsCount + 1);
    m_BotWinRates[opponentsCount] = pcmn::Evaluator::Instance().GetEquity
    (
        m_Bot.Cards().front().ToEvalFormat(), 
        m_Bot.Cards().back().ToEvalFormat(), 
        flop, 
        ranges
    );

    return m_BotWinRates[opponentsCount];
}

void EVDecisionMaker::ProcessDeferredTables()
{
    // set up recursion level to ignore recursion limit condition
    boost::for_each(m_DeferredTables, boost::bind(&TableDesc::SetRecursionLevel, _1, RECURSION_COUNT_LIMIT + 1));

    // set up promises
    typedef boost::promise<void> Promise;
    typedef boost::shared_ptr<Promise> PromisePtr;
    typedef boost::unique_future<void> Future;

    const auto instance(shared_from_this());

    std::vector<PromisePtr> promises;
    boost::transform(m_DeferredTables, std::back_inserter(promises), [instance, this](const TableDesc& t){
        const auto promise = boost::make_shared<Promise>();

        const auto work = [instance, promise, this, t](){
            try
            {
                ProcessTable(t);
            }
            catch (const TimedOut&)
            {
                LOG_WARNING("Processing of table timed out");
            }
            catch (const TooMuchTables&)
            {
                LOG_WARNING("Stopping tables processing, tables count: %s") % m_Tables.size();
            }
            
            promise->set_value();
        };
        cmn::Service::Instance().post(work);
        return promise;
    });

    std::vector<Future> futures;
    boost::transform(promises, std::back_inserter(futures), boost::bind(&Promise::get_future, _1));
    const auto isReady = [&futures](){
        return boost::find_if(futures, !boost::bind(&Future::is_ready, _1)) == futures.end();
    };
    while (!isReady())
        cmn::Service::Instance().poll();
}

EVDecisionMaker::TableDesc::TableDesc(const pcmn::TableContext& context)
    : m_Pot(context.m_Pot)
    , m_Bet(context.m_MaxBet)
    , m_InPot()
    , m_Queue()
    , m_Percent(1)
    , m_Hands()
    , m_LastAggression(context.m_Street ? pcmn::Action::Check : pcmn::Action::BigBlind)
    , m_LastRaise(context.m_Street ? pcmn::BetSize::NoBet : pcmn::BetSize::Low)
    , m_EV()
    , m_WinRate()
    , m_BotStack()
    , m_BotAction(pcmn::Action::Unknown)
    , m_BotBetSize()
    , m_RaiseCount()
    , m_RecursionCount()
{
}

std::vector<pcmn::Action::Value> EVDecisionMaker::TableDesc::GetPossibleActions(const pcmn::Player& player, unsigned street) const
{
    pcmn::Action::List result;
    pcmn::Action::Value action = pcmn::Action::Fold;
    for (; action <= pcmn::Action::Raise; action = static_cast<pcmn::Action::Value>(action + 1))
    {
        // can't check if somebody raised
        if (m_RaiseCount && action == pcmn::Action::Check)
            continue;
        // skip impossible actions
        if (m_Bet && m_Bet != player.Bet() && (action == pcmn::Action::Check || action == pcmn::Action::Bet))
            continue;
        if (!m_Bet && (action == pcmn::Action::Fold || action == pcmn::Action::Raise))
            continue;
        if (player.Bet() == m_Bet && action == pcmn::Action::Call)
            continue; // can't call, already bet max bet
        if (action == pcmn::Action::Fold && player.Bet() == m_Bet)
            continue; // can't fold, already bet max bet
        if (!street && action == pcmn::Action::Bet)
            continue; // can't bet on preflop
        if (player.Stack() <= m_Bet && (action == pcmn::Action::Raise || action == pcmn::Action::Bet))
            continue; // can't raise or bet without stack
        if (street && m_RaiseCount && m_InPot.size() > 2 && action != pcmn::Action::Fold)
            continue; // somebody called raise
        if (m_RaiseCount > 1 && action == pcmn::Action::Raise)
            continue; // raise and reraise, you can't raise it again, because second raise will be huge(all stack)
        if (m_RaiseCount > 1 && m_InPot.size() > 2 && action != pcmn::Action::Fold)
            continue; // raise and reraise then somebody called this, it's a good chance to fold

        result.push_back(action);
    }

    return result;
}

pcmn::BetSize::Value EVDecisionMaker::TableDesc::BetFromFutureAction(pcmn::Action::Value action, const pcmn::Player& player, unsigned BB) const
{
    switch (action)
    {
    case pcmn::Action::Fold:
    case pcmn::Action::Check:
        return pcmn::BetSize::NoBet;
    case pcmn::Action::Call:
        return pcmn::BetSize::FromDecision(player.Bet(), m_Bet, m_Pot, player.Stack(), BB);
    case pcmn::Action::Bet:
    case pcmn::Action::Raise:
        return m_RaiseCount ? pcmn::BetSize::Huge : pcmn::BetSize::High;
    default: assert(false); return pcmn::BetSize::NoBet;
    }
}

pcmn::Player::Position::Value EVDecisionMaker::TableDesc::GetCurrentPosition() const
{
    unsigned leftInQueue = m_Queue.size();
    pcmn::Player::Position::Value result = pcmn::Player::Position::Middle;

    float step = static_cast<float>(m_Queue.size() + m_InPot.size()) / 3;
    if (step < 1)
        step = 1;

    if (leftInQueue <= step)
        result = pcmn::Player::Position::Later;
    else
    if (leftInQueue >= step * 2.5f)
        result = pcmn::Player::Position::Early;
    return result;
}

void EVDecisionMaker::TableDesc::ProcessDecision(pcmn::Action::Value id, pcmn::BetSize::Value amount, unsigned bet, unsigned botStack)
{
    // process queues
    switch (id)
    {
    case pcmn::Action::Fold:
        {
#ifdef _DEBUG
            if (m_Hands.count(m_Queue.front()->Name()))
                m_Events.back() += (std::string(":") + pcmn::Hand::ToString(m_Hands[m_Queue.front()->Name()]));
#endif // _DEBUG
            m_Hands.erase(m_Queue.front()->Name());

            const auto it = boost::find(m_InPot, m_Queue.front());
            if (it != m_InPot.end())
                m_InPot.erase(it);
            m_Queue.pop_front();
        }
        break;
    case pcmn::Action::Check:
    case pcmn::Action::Call:
        m_InPot.push_back(m_Queue.front());
        m_Queue.pop_front();
        break;
    case pcmn::Action::Bet:
    case pcmn::Action::Raise:
        {
            const std::string currentPlayer = m_Queue.front()->Name();
            for (const PlayerStats::Ptr& player : m_InPot)
            {
                if (player->Name() == currentPlayer)
                    continue; // skip current player because he made a move just right now

                m_Queue.push_back(boost::make_shared<PlayerStats>(*player));
                m_Queue.back()->State(pcmn::Player::State::Waiting);
            }

            m_InPot.clear();
            m_InPot.push_back(m_Queue.front());
            m_Queue.pop_front();

            // check for aggression
            ++m_RaiseCount;
            if (id > m_LastAggression || amount > m_LastRaise)
            {
                m_LastAggression = id;
                m_LastRaise = amount;
            }
        }
        break;
    default: assert(false);
    }

    // calculated bet size must be less than bot stack
    if (bet > botStack)
        bet = botStack;

    // increment pot size
    m_Pot += bet;
    if (m_Bet < bet)
        m_Bet = bet;
}

void EVDecisionMaker::TableDesc::CalculateEV(const EVDecisionMaker& maker)
{
    const auto botTotalBet = maker.m_Bot.Stack() - m_BotStack;

    // find bot in pot
    if (!IsPlayerInPot(maker.m_Bot.Name()))
    {
        // bot folded
        m_EV = static_cast<float>(botTotalBet * -1.0);// maker.GetEVForStackSize(m_BotStack);
        return;
    }

    // prepare opponents hands
    pcmn::Hand::List hands;
    bool isUnknownHandsExists = false;
    for (const auto& pair : m_Hands)
    {
        hands.push_back(pair.second);
        if (!isUnknownHandsExists && pair.second == pcmn::Hand::Unknown)
            isUnknownHandsExists = true;
    }

    // determine which way to use
    const bool isNeed2UseAverage = false;// (maker.m_Street == pcmn::Table::Phase::Preflop && hands.size() > 1);

    // average rate
    const float averageRate = maker.m_Bot.Equities().back();

    // calculate bot win rate
    if (hands.empty())
    {
        assert(m_InPot.size() == 1);
        m_WinRate = static_cast<float>(m_Percent * 100.0); // all players folded
    }
    else
    if (isNeed2UseAverage)
    {
        m_WinRate = averageRate;
    }
    else 
    {
        m_WinRate = srv::ClientCache::GetEquity
        (
            maker.m_Bot.Cards(),
            maker.m_Flop,
            hands
        );
    }

    // track unknown hands, if exists use average win ratio
//     if (isUnknownHandsExists && winRate > averageRate)
//         winRate = averageRate;

    if (m_WinRate == cfg::INVALID_PERCENT)
        m_WinRate = averageRate;

    m_WinRate /= 100.0f;

    // ev for this pot win
    //const float winEV = maker.GetEVForStackSize(m_BotStack + m_Pot);

    // ev for this pot loose
    //const float looseEV = maker.GetEVForStackSize(m_BotStack);

    // calculate EV
    //m_EV = winEV * winRate + looseEV * (1.0f - winRate);
    const auto potentialProfit = m_Pot - botTotalBet;
    const auto potentialLoose = botTotalBet * -1.0;

    m_EV = static_cast<float>(m_WinRate * potentialProfit + potentialLoose * (1.0 - m_WinRate));

#ifdef _DEBUG
    m_Events.push_back((boost::format("percent: %s, pot/stack: %s/%s") % m_WinRate % (m_BotStack + m_Pot) % m_BotStack).str());
#endif // _DEBUG
}

bool EVDecisionMaker::TableDesc::IsPlayerInPot(const std::string& name) const
{
    const auto queue = std::find_if(m_Queue.begin(), m_Queue.end(), [&](const PlayerStats::Ptr& p) { return p->Name() == name; });
    if (queue != m_Queue.end())
        return true; // player in queue, it's mean that he is in pot

    const auto player = std::find_if(m_InPot.begin(), m_InPot.end(), [&](const PlayerStats::Ptr& p) { return p->Name() == name; });
    return (player != m_InPot.end());
}

void EVDecisionMaker::TableDesc::SetRecursionLevel(unsigned level)
{
    m_RecursionCount = level;
}


bool EVDecisionMaker::ActionAndBet::operator<(const ActionAndBet& other) const
{
    if (m_Action == other.m_Action)
        return m_Bet < other.m_Bet;
    return m_Action < other.m_Action;
}

std::ostream& operator << (std::ostream& os, const EVDecisionMaker::ActionAndBet& val)
{
    os << "[" << pcmn::Action::ToString(val.m_Action) << "][" << pcmn::BetSize::ToString(val.m_Bet) << "]";
    return os;
}

std::ostream& operator << (std::ostream& os, const EVDecisionMaker::Result& r)
{
    os << "[" << r.m_EV << "], tables: " << r.m_Tables.size() << ", summ: " << r.m_Summ << ", avg: " << r.m_AverageEV;
    return os;
}


} // namespace srv