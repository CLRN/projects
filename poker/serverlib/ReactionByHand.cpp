#include "ReactionByHand.h"
#include "PlayerStats.h"

#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>

namespace srv
{

ReactionByHand::ReactionByHand(ILog& logger, const PlayerStats& player) 
    : m_Logger(logger)
    , m_Player(player)
    , m_Bets(m_Player.GetReactionMap())
    , m_Recalculated()
{
}

float ReactionByHand::Get(pcmn::Action::Value action, pcmn::BetSize::Value bet) const 
{
    boost::unique_lock<boost::mutex> lock(m_Mutex);
    if (!m_Recalculated)
        const_cast<ReactionByHand&>(*this).Recalculate();
    lock.unlock();

    // optimize reaction on bot check
    if (bet == pcmn::BetSize::NoBet)
    {
        if (action == pcmn::Action::Call)
            return 1.0f;
        else
        if (action == pcmn::Action::Fold)
            return 0.0f;
        else
        if (action == pcmn::Action::Raise) // opponent can't raise if there is no bet
            action = pcmn::Action::Bet;
    }

    const IStatistics::PlayerInfo::BetMap::const_iterator itBet = m_Bets.find(bet);
    if (m_Bets.end() == itBet)
        return action == pcmn::Action::Fold ? 1.0f : 0.0f;

    const IStatistics::PlayerInfo::ActionMap::const_iterator itAction = itBet->second.find(action);
    if (itBet->second.end() == itAction)
        return 0.0f;

    if (action == pcmn::Action::Fold)
    {
        float readyHandChance = 0.0f;
        for (const auto& pair : m_Player.GetPossibleHands())
        {
            const pcmn::Hand::Value readyHand = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::FLOP_HAND_MASK));
            if (readyHand >= pcmn::Hand::Pair)
                readyHandChance += pair.second;
        }

        if (readyHandChance > 0.5f && !m_Player.GetActions().empty())
        {
            //for (const auto& actions : m_Player.GetActions())
            const auto& actions = m_Player.GetActions().back();
            {
                const auto it = boost::find_if(actions, boost::bind(&pcmn::Player::ActionDesc::m_Amount, _1) >= pcmn::BetSize::High);
                if (it != actions.end())
                    return 0.0f;
            }
        }
    }

    return itAction->second;
}

float ReactionByHand::Get(pcmn::Action::Value action, pcmn::BetSize::Value bet, pcmn::Player::Count::Value inPot) const 
{
    if (inPot > pcmn::Player::Count::One && bet >= pcmn::BetSize::High)
    {
        bool gotReadyHand = false;
        for (const auto& pair : m_Player.GetPossibleHands())
        {
            const pcmn::Hand::Value readyHand = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::FLOP_HAND_MASK));
            const pcmn::Hand::Value power = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::POWER_MASK));

            if (readyHand == pcmn::Hand::Pair && power >= pcmn::Hand::Middle || readyHand > pcmn::Hand::Pair)
            {
                gotReadyHand = true;
                break;
            }
        }
        if (!gotReadyHand)
            return (action == pcmn::Action::Fold || action == pcmn::Action::Check) ? 1.0f : 0.0f;
    }

    if (m_Player.GetPosition() != pcmn::Player::Position::Later && 
        m_Player.GetPossibleHands().empty()
        )
    {
        if (bet == pcmn::BetSize::NoBet && (action == pcmn::Action::Bet || action == pcmn::Action::Raise))
            return 0.0f; // bet without hand is impossible
        if (bet == pcmn::BetSize::NoBet && action == pcmn::Action::Check)
            return 1.0f;
    }

    return Get(action, bet);
}

void ReactionByHand::Recalculate()
{
    // check for good hands
    for (auto& betAndReaction : m_Bets)
    {
        if (betAndReaction.first == pcmn::BetSize::NoBet)
            continue; // no need to recalculate, fold or call is impossible

        float callChance = 0;
        for (const auto& pair : m_Player.GetPossibleHands())
        {
            const pcmn::Hand::Value readyHand = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::FLOP_HAND_MASK));
            const pcmn::Hand::Value power = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::POWER_MASK));
            const pcmn::Hand::Value draws = static_cast<pcmn::Hand::Value>(pair.first & (pcmn::Hand::DRAWS_MASK));

            switch (betAndReaction.first)
            {
            case pcmn::BetSize::Low:
                if (readyHand >= pcmn::Hand::HighCard || draws >= pcmn::Hand::GutShot)
                    callChance += pair.second;
                break;
            case pcmn::BetSize::Normal:
                if (readyHand >= pcmn::Hand::Pair || draws >= pcmn::Hand::StraightDraw)
                    callChance += pair.second;
                break;
            case pcmn::BetSize::High:
                if (readyHand == pcmn::Hand::Pair && power >= pcmn::Hand::Middle || readyHand > pcmn::Hand::Pair || draws >= pcmn::Hand::StraightDraw)
                    callChance += pair.second;
                break;
            case pcmn::BetSize::Huge:
                if (readyHand == pcmn::Hand::Pair && power == pcmn::Hand::Top || draws >= pcmn::Hand::GoodDraw)
                    callChance += pair.second;
                break;
            }
        }

        // checking for chances correction
        IStatistics::PlayerInfo::BetMap reactions = m_Player.GetReactionMap();
        float& fold = betAndReaction.second[pcmn::Action::Fold];
        if (callChance && fold)
        {
            const float diff = callChance - betAndReaction.second[pcmn::Action::Call];
            if (diff > 0)
            {
                if (fold > diff)
                {
                    fold -= diff;
                    betAndReaction.second[pcmn::Action::Call] += diff;
                }
                else
                {
                    betAndReaction.second[pcmn::Action::Call] += fold;
                    betAndReaction.second[pcmn::Action::Fold] = 0.0f;
                }         

                assert(fold < 1.0f);
                assert(betAndReaction.second[pcmn::Action::Call] <= 1.0f);
            }
        }
    }

    m_Recalculated = true;
}


PreflopReactionByHand::PreflopReactionByHand(ILog& logger, const PlayerStats& player)
    : ReactionByHand(logger, player)
{

}

float PreflopReactionByHand::Get(pcmn::Action::Value action, pcmn::BetSize::Value bet, pcmn::Player::Count::Value inPot) const
{
    if (inPot >= pcmn::Player::Count::Two)
    {
        float readyHandChance = 0.0f;
        for (const auto& pair : m_Player.GetPossibleHands())
        {
            if (pair.first & pcmn::Hand::PoketPair || pair.first & pcmn::Hand::BothHigh || pair.first & pcmn::Hand::Ace)
                readyHandChance += pair.second;
        }
        if (!readyHandChance)
        {
            if (bet > pcmn::BetSize::Normal && (action == pcmn::Action::Bet || action == pcmn::Action::Raise))
                return 0.0f;
        }
        else
        {
            if (action == pcmn::Action::Fold && readyHandChance > 0.5)
                return 0.0;
        }
    }
    return ReactionByHand::Get(action, bet, inPot);
}

} // namespace srv