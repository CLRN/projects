#include "MongoStatistics.h"
#include "exception/CheckHelpers.h"
#include "Config.h"
#include "common/Modules.h"
#include "conversion/AnyCast.h"
#include "BetSize.h"
#include "Actions.h"

#pragma warning(push)
#pragma warning(disable : 4005) // 'WIN32_LEAN_AND_MEAN' : macro redefinition
#include <mongo/client/dbclient.h>
#include <mongo/bson/bsonelement.h>
#pragma warning(pop) 

#include <numeric>

#include <boost/format.hpp>

namespace srv
{
DECLARE_CURRENT_MODULE(Modules::DataBase);

class MongoStatistics::Impl
{
public:

Impl(ILog& logger) : m_Log(logger)
{
	try 
	{
        m_Connection.connect("localhost");
	}
	CATCH_PASS(cmn::Exception("Failed to init statistics"))
}

void Write(pcmn::TableContext::Data& data)
{
	try 
	{
		SCOPED_LOG(m_Log);

        // generate board description
        std::vector<bson::bo> board;
        for (const pcmn::Board::Value current : data.m_Board)
        {
            std::vector<unsigned> street = conv::cast<std::vector<unsigned>>(static_cast<unsigned>(current));
            if (street.empty())
                street.push_back(0); // unknown by default
            board.push_back(bson::bob().append("street", street).obj());
        }

        std::vector<bson::bo> players;
        for (const pcmn::Player& player : data.m_PlayersData)
            players.push_back(BuildPlayer(player));

        m_Connection.insert(cfg::STAT_COLLECTION_NAME, BSON( "players" << players << 
                                                        "flop" << data.m_Flop << 
                                                        "board" << board));
	}
	CATCH_PASS(cmn::Exception("Failed to write data to database"))
}

unsigned GetRanges(PlayerInfo::List& players)
{
	try
	{
        // get all games with specified actions on preflop
        SCOPED_LOG(m_Log);

        unsigned count = 0;
        for (PlayerInfo& info : players)
        {
            info.m_CardRange = GetRange(info.m_Actions, info.m_Name);
            if (!info.m_CardRange)
                info.m_CardRange = GetRange(info.m_Actions);
        }
   
        return count;
	}
	CATCH_PASS(cmn::Exception("Failed to get ranges"))
}

pcmn::Player::Style::Value GetAverageStyle(const std::string& target, const std::string& opponent)
{
	try
	{
        SCOPED_LOG(m_Log);

        LOG_TRACE("Fetching last actions: [%s], opponent: [%s]") % target % opponent;

        mongo::Query query =             
        BSON("players" << BSON("$all" 
                                    << BSON_ARRAY(
                                            BSON("$elemMatch" 
                                                << BSON("name" << target))
                                            << BSON("$elemMatch" 
                                                << BSON("name" << opponent))
                                        )));
        query.sort("_id", 0);

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            10
        );

        int checkFolds = 0;
        int calls = 0;
        int raises = 0;
        unsigned count = 0;

        while (cursor->more()) 
        {
            unsigned passiveActions = 0;
            unsigned normalActions = 0;
            unsigned aggressiveActions = 0;

            const bson::bo data = cursor->next();
            LOG_TRACE("Fetched last actions: [%s]") % data.toString();

            const std::vector<bson::be> players = data["players"].Array();
            for (const bson::be& elem : players)
            {
                const bson::bo player = elem.Obj();
                if (player["name"].String() != target)
                    continue;

                // found target player, enum all actions
                const std::vector<bson::be> streets = player["streets"].Array();
                for (const bson::be& street : streets)
                {
                    const std::vector<bson::be> actions = street["actions"].Array();
                    for (const bson::be& action : actions)
                    {
                        const pcmn::Action::Value id = static_cast<pcmn::Action::Value>(action["id"].Int());

                        switch (id)
                        {
                        case pcmn::Action::Check:
                        case pcmn::Action::Fold:
                            ++passiveActions;
                            break;
                        case pcmn::Action::Call:
                            ++normalActions;
                            break;
                        case pcmn::Action::Bet:
                        case pcmn::Action::Raise:
                            ++aggressiveActions;
                            break;
                        }
                    }
                }
                break;
            }

            if (aggressiveActions)
                ++raises;
            else
            if (normalActions)
                ++calls;
            else
                ++checkFolds;

            ++count;

            if (count == 4 && raises > 1)
                return pcmn::Player::Style::Aggressive;
        }

        const int summ = checkFolds + calls + raises;
        if (!summ)
            return pcmn::Player::Style::Normal;

        if (raises >= summ / 2)
            return pcmn::Player::Style::Aggressive;

	    if (raises > 1)
		    return pcmn::Player::Style::Normal;

	    return pcmn::Player::Style::Passive;
	}
	CATCH_PASS(cmn::Exception("Failed to get last actions"))
}

void GetEquities(PlayerInfo::List& players, unsigned street)
{
    try
    {
        SCOPED_LOG(m_Log);

        for (PlayerInfo& info : players)
        {
            info.m_WinRate = GetEquity(info.m_Actions, street, info.m_Name);

            if (!info.m_WinRate)
            {
                LOG_TRACE("Failed to find equity by player name: [%s], getting from all statistics") % info.m_Name;
                info.m_WinRate = GetEquity(info.m_Actions, street);
            }
        }
    }
    CATCH_PASS(cmn::Exception("Failed to get equities"))
}


void GetHands(PlayerInfo& player, unsigned street, unsigned playerCount)
{
    try
    {
        SCOPED_LOG(m_Log);

        const pcmn::Player::Count::Value count = pcmn::Player::Count::FromValue(playerCount);
        if (!GetHand(player, street, player.m_Name, count))
        {
            LOG_TRACE("Failed to find hand by player name: [%s], getting from all statistics") % player.m_Name;
            GetHand(player, street, count);
        }
    }
    CATCH_PASS(cmn::Exception("Failed to get hands"))
}

void GetActions(PlayerInfo& player, pcmn::Board::Value board, unsigned streetId, unsigned playerCount) 
{
    try
    {
        SCOPED_LOG(m_Log);
        if (!GetActionsByName(player, board, streetId, playerCount))
        {
            LOG_TRACE("Failed to find actions by player name: [%s], getting from all statistics") % player.m_Name;
            GetActionsFromAll(player, board, streetId, playerCount);
        }
    }
    CATCH_PASS(cmn::Exception("Failed to get actions"), player.m_Name, player.m_Position, board, streetId, playerCount)
}

void GetActionsFromAll(PlayerInfo& player, pcmn::Board::Value board, unsigned streetId, unsigned count) 
{
    try
    {
        SCOPED_LOG(m_Log);

        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();
         
        // prepare list of actions
        std::vector<bson::bo> actions;
        actions.push_back(BSON("$elemMatch" << BSON("position" << player.m_Position << "count" << pcmn::Player::Count::FromValue(count))));

        const std::string boardAndStreet = std::string("board.") + conv::cast<std::string>(streetId) + std::string(".street");
        const std::string streetsAndActions = std::string("streets.") + conv::cast<std::string>(streetId) + std::string(".actions");

        std::vector<unsigned> boardArray = conv::cast<std::vector<unsigned>>(static_cast<unsigned>(board));
        if (boardArray.empty())
            boardArray.push_back(0); // unknown by default

        mongo::Query query =
        BSON(boardAndStreet << BSON("$all" << boardArray) <<
             "players" << BSON("$elemMatch" 
                                << BSON(streetsAndActions << BSON("$all" << actions))));

        query.sort("_id", 0);       

        LOG_TRACE("Fetching all players actions: [%s] by board: [%s]") % query.toString() % board;

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            200,
            0, 
            &returnProjection
        );

        // multiply player stat by coefficient
        for (auto& betAndActions : player.m_Bets)
            for (auto& pair : betAndActions.second)
                pair.second *= 2;

        unsigned totalRows = 0;
        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_DEBUG("Fetched players actions, data: [%s]") % player.m_Name % data.toString();

            // found target player, enum all actions
            const std::vector<bson::be> streets = data.getFieldDotted("players.0.streets").Array();
            CHECK(streets.size() > streetId, cmn::Exception("Bad statistics, returned less streets than expected"), streetId, data.toString());

            // get actions from this street
            const std::vector<bson::be> actions = streets[streetId]["actions"].Array();
            for (const bson::be& action : actions)
            {
                const pcmn::Player::Position::Value position = static_cast<pcmn::Player::Position::Value>(action["position"].Int());
                if (position != player.m_Position)
                    continue;

                const pcmn::Action::Value actionId = static_cast<pcmn::Action::Value>(action["id"].Int());
                const pcmn::BetSize::Value betSize = static_cast<pcmn::BetSize::Value>(action["bet"].Int());
                LOG_TRACE("Player: [%s], board: [%s], action: [%s], bet size: [%s]") % data.getFieldDotted("players.0.name").String()  % board % actionId % betSize;

                ++player.m_Bets[betSize][actionId];
            }
            ++totalRows;
        }

        if (!totalRows && player.m_Position == pcmn::Player::Position::Early)
        {
            // try to match with middle position
            player.m_Position = pcmn::Player::Position::Middle;
            GetActionsFromAll(player, board, streetId, count);
            return;
        }

        assert(totalRows);

        // convert counters to percents
        for (auto& betAndActions : player.m_Bets)
        {
            float total = 0;
            for (const auto& action : betAndActions.second)
                total += action.second;

            for (auto& action : betAndActions.second)
                action.second = action.second / total;
        }
    }
    CATCH_PASS(cmn::Exception("Failed to get actions"), player.m_Name, player.m_Position, board, streetId, count)
}

bool GetActionsByName(PlayerInfo& player, pcmn::Board::Value board, unsigned streetId, unsigned count) 
{
    try
    {
        SCOPED_LOG(m_Log);

        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();
         
        // prepare list of actions
        std::vector<bson::bo> actions;
        actions.push_back(BSON("$elemMatch" << BSON("position" << player.m_Position << "count" << pcmn::Player::Count::FromValue(count))));

        const std::string boardAndStreet = std::string("board.") + conv::cast<std::string>(streetId) + std::string(".street");
        const std::string streetsAndActions = std::string("streets.") + conv::cast<std::string>(streetId) + std::string(".actions");

        std::vector<unsigned> boardArray = conv::cast<std::vector<unsigned>>(static_cast<unsigned>(board));
        if (boardArray.empty())
            boardArray.push_back(0); // unknown by default

        mongo::Query query =
        BSON(boardAndStreet << BSON("$all" << boardArray) <<
             "players" << BSON("$elemMatch" 
                                << BSON("name" << player.m_Name <<
                                        streetsAndActions << BSON("$all" << actions))));

        query.sort("_id", 0);       

        LOG_TRACE("Fetching player: [%s], actions: [%s] by board: [%s]") % player.m_Name % query.toString() % board;

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            200,
            0, 
            &returnProjection
        );

        unsigned total = 0;
        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_DEBUG("Fetched player actions: [%s], data: [%s]") % player.m_Name % data.toString();

            // found target player, enum all actions
            const std::vector<bson::be> streets = data.getFieldDotted("players.0.streets").Array();
            CHECK(streets.size() > streetId, cmn::Exception("Bad statistics, returned less streets than expected"), streetId, data.toString());

            // get actions from this street
            const std::vector<bson::be> actions = streets[streetId]["actions"].Array();
            for (const bson::be& action : actions)
            {
                const pcmn::Player::Position::Value position = static_cast<pcmn::Player::Position::Value>(action["position"].Int());
                if (position != player.m_Position)
                    continue;

                const pcmn::Action::Value actionId = static_cast<pcmn::Action::Value>(action["id"].Int());
                const pcmn::BetSize::Value betSize = static_cast<pcmn::BetSize::Value>(action["bet"].Int());
                LOG_TRACE("Player: [%s], board: [%s], action: [%s], bet size: [%s]") % player.m_Name % board % actionId % betSize;

                ++player.m_Bets[betSize][actionId];
            }
            ++total;
        }

        if (player.m_Bets.size() <= 3)
            return false;

        for (const auto& betAndActions : player.m_Bets)
        {
            if (betAndActions.second.size() == 1)
                return false; // actions on this bet is undefined
        }

        // convert counters to percents
        for (auto& betAndActions : player.m_Bets)
        {
            float total = 0;
            for (const auto& action : betAndActions.second)
                total += action.second;

            for (auto& action : betAndActions.second)
                action.second = action.second / total;
        }

        return true;
    }
    CATCH_PASS(cmn::Exception("Failed to GetActionsByName"), player.m_Name, player.m_Position, board, streetId, count)
}

private:

bool GetHand(PlayerInfo& info, unsigned streetId, const std::string& name, const pcmn::Player::Count::Value count)
{
    SCOPED_LOG(m_Log);

    try
    {
        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();

        std::map<pcmn::Hand::Value, unsigned> counters;
         
        // prepare list of actions
        const std::vector<bson::bo> actions = GetActionsFilter(info.m_Actions, true);

        const std::string streetsAndActions = std::string("streets.") + conv::cast<std::string>(streetId) + std::string(".actions");
        mongo::Query query =
        BSON("players" << BSON("$elemMatch" 
                            << BSON("name" << name
                            << "cards.0" << BSON("$exists" << 1)
                            << streetsAndActions << BSON("$all" << actions))));

        query.sort("_id", 0);       

        LOG_TRACE("Fetching player hands: [%s]") % query.toString();

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            100,
            0, 
            &returnProjection
        );

        unsigned total = 0;
        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_DEBUG("Fetched player hands: [%s], data: [%s]") % name % data.toString();

            // found target player, enum all actions
            const std::vector<bson::be> streets = data.getFieldDotted("players.0.streets").Array();
            CHECK(streets.size() > streetId, cmn::Exception("Bad statistics, returned less streets than expected"), streetId, data.toString());

            pcmn::Hand::Value hand = GetHandFromStreet(streets[streetId]);
            if (streetId)
                hand = static_cast<pcmn::Hand::Value>(hand & (pcmn::Hand::FLOP_HAND_MASK | pcmn::Hand::POWER_MASK));
            if (streetId == 3) // remove draws from river
                hand = static_cast<pcmn::Hand::Value>(hand & ~pcmn::Hand::DRAWS_MASK);

            if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace))
            {
                pcmn::Card::List cards;
                const std::vector<bson::be> cardsArray = data.getFieldDotted("players.0.cards").Array();
                for (const bson::be& elem : cardsArray)
                    cards.push_back(pcmn::Card().FromEvalFormat(elem.Int()));

                LOG_TRACE("Player: [%s], hand: [%s], with cards: [%s]") % name % hand % cards;
            }
            ++counters[hand];
            ++total;
        }

        for (const auto& pair : counters)
        {
            // calculate average chance of this hand
            const float avg = static_cast<float>(pair.second) / total;
            info.m_Hands[pair.first] = avg;
        }

        return total > 2 && counters.size() > 2;       
    }
    CATCH_PASS(cmn::Exception("Failed to GetHand"), name)
}

void GetHand(PlayerInfo& info, unsigned streetId, const pcmn::Player::Count::Value count)
{
    try
    {
        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();

        std::vector<double> equities;

        // query all games with equity info about this player

        // prepare list of actions
        const std::vector<bson::bo> actions = GetActionsFilter(info.m_Actions, true);

        // prepare query
        const std::string streetsAndActions = std::string("streets.") + conv::cast<std::string>(streetId) + std::string(".actions");
        mongo::Query query =
        BSON("players" << BSON("$elemMatch" 
                            << BSON("cards.0" << BSON("$exists" << 1)
                                    << streetsAndActions << BSON("$all"
                                                            << actions))));
        query.sort("_id", 0);

        LOG_TRACE("Fetching player hands: [%s]") % query.toString();

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            100,
            0, 
            &returnProjection
        );

        std::map<pcmn::Hand::Value, unsigned> counters;
        unsigned total = 0;
        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_DEBUG("Fetched all stats equities, data: [%s]") % data.toString();

            const std::vector<bson::be> streets = data.getFieldDotted("players.0.streets").Array();
            CHECK(streets.size() > streetId, cmn::Exception("Bad, statistics returned less streets than expected"), streetId, data.toString());

            pcmn::Hand::Value hand = GetHandFromStreet(streets[streetId]);
            if (streetId)
                hand = static_cast<pcmn::Hand::Value>(hand & (pcmn::Hand::FLOP_HAND_MASK | pcmn::Hand::POWER_MASK));
            if (streetId == 3) // remove draws from river
                hand = static_cast<pcmn::Hand::Value>(hand & ~pcmn::Hand::DRAWS_MASK);

            LOG_TRACE("Player: [%s], hand: [%s]") % data.getFieldDotted("players.0.name").String() % hand;
            ++counters[hand];
            ++total;
        }

        for (const auto& pair : info.m_Hands)
        {
            // detect average count of the detected hand
            const unsigned count = static_cast<unsigned>(pair.second * total);
            if (count > counters[pair.first])
            {
                total += count - counters[pair.first];
                counters[pair.first] = count;
            }
        }

        for (const auto& pair : counters)
        {
            // calculate average chance of this hand
            const float avg = static_cast<float>(pair.second) / total;
            info.m_Hands[pair.first] = avg;
        }
    }
    CATCH_PASS(cmn::Exception("Failed to GetHand from all stats"))
}

unsigned GetRange(const pcmn::Player::ActionDesc::List& actionDescs, const std::string& name = std::string())
{
	try
	{
        // get all games with specified actions on preflop
        SCOPED_LOG(m_Log);

        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();

        std::vector<int> cards;

        // query all games with cards info about this player or for all stats

        // prepare list of actions
        const std::vector<bson::bo> actions = GetActionsFilter(actionDescs);

        mongo::Query query = 
            name.empty() ? 
        BSON("players" << BSON("$elemMatch" 
                                << BSON("cards" << BSON("$size" << 2)
                                        << "streets.0.actions" << BSON("$all"
                                            << actions))))
            :
        BSON("players" << BSON("$elemMatch" 
                        << BSON("name" << name 
                                << "cards" << BSON("$size" << 2)
                                << "streets.0.actions" << BSON("$all"
                                    << actions))))
            ;

        query.sort("_id", 0);

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            100, 
            0, 
            &returnProjection
        );

        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_TRACE("Fetched player range: [%s], data: [%s]") % name % data.toString();

            const std::vector<bson::be> cardsArray = data.getFieldDotted("players.0.cards").Array();
            for (const bson::be& elem : cardsArray)
                cards.push_back(elem.Int());
        }

        if (cards.size() < 4)
            return 0;

        // find average value
        const unsigned summ = std::accumulate(cards.begin(), cards.end(), 0);
        LOG_TRACE("Player: [%s], cards: [%s], summ: [%s], result: [%s]") % name % cards % summ % (summ / cards.size());
        return summ / cards.size();
	}
	CATCH_PASS(cmn::Exception("Failed to GetRanges"))
}

bool IsActionsMatch(const bson::bo& street, const PlayerInfo::Actions& actionsDescs)
{
    // get all actions from street and compare with all actions from outside
    const std::vector<bson::be> actions = street["actions"].Array();

    PlayerInfo::Actions fetched;

    for (const bson::be& action : actions)
    {
        const pcmn::Action::Value id = static_cast<pcmn::Action::Value>(action["id"].Int());
        const pcmn::BetSize::Value amount = static_cast<pcmn::BetSize::Value>(action["amount"].Int());

        pcmn::Player::ActionDesc desc;
        desc.m_Id = id;
        desc.m_Amount = amount;
        if (actionsDescs.size() == 1 && actionsDescs.front() == desc)
            return true;

        fetched.push_back(desc);
    }

    if (actions.size() == 1)
        return false; // not matched

    for (const PlayerInfo::Actions::value_type& actionDesc : actionsDescs)
    {
        if (std::find(fetched.begin(), fetched.end(), actionDesc) == fetched.end())
            return false;
    }

    return true;
}

float GetEquity(const PlayerInfo::Actions& actionDescs, unsigned streetId, const std::string& name)
{
    try
    {
        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();

        std::vector<double> equities;

        // query all games with equity info about this player

        // prepare list of actions
        const std::vector<bson::bo> actions = GetActionsFilter(actionDescs);

        // prepare query
        mongo::Query query = 
        BSON("players" << BSON("$elemMatch" 
                                << BSON("name" << name
                                        << "equities.0" << BSON("$exists" << 1)
                                        << "streets" << BSON("$elemMatch" 
                                                        << BSON("actions" << BSON("$all"
                                                                << actions))))));

        query.sort("_id", 0);
        

        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            10, // ten last games
            0, 
            &returnProjection
        );

        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_TRACE("Fetched player equities: [%s], data: [%s]") % name % data.toString();

            // all equities
            const std::vector<bson::be> equitiesArray = data.getFieldDotted("players.0.equities").Array();
            assert(!equitiesArray.empty());

            // found target player, enum all actions
            const std::vector<bson::be> streets = data.getFieldDotted("players.0.streets").Array();

            if (!streetId) // preflop
            {
                if (IsActionsMatch(streets.front().Obj(), actionDescs))
                    equities.push_back(equitiesArray.front().Double());
            }
            else
            {
                for (unsigned i = 0; i < streets.size(); ++i)
                {
                    if (equitiesArray.size() <= i)
                        break;

                    if (IsActionsMatch(streets[i].Obj(), actionDescs))
                        equities.push_back(equitiesArray[i].Double());
                }
            }
        }

        if (equities.size() == 1)
        {
            const float avg = GetEquity(actionDescs, streetId);
            return static_cast<float>((avg + equities.front()) / 2);
        }

        if (equities.empty())
            return 0;

        // find average value
        const double summ = std::accumulate(equities.begin(), equities.end(), double());
        LOG_TRACE("Player: [%s], equities: [%s], summ: [%s], result: [%s]") % name % equities % summ % static_cast<float>(summ / equities.size());
        return static_cast<float>(summ / equities.size());
    }
    CATCH_PASS(cmn::Exception("Failed to GetEquity"), name)
}

float GetEquity(const PlayerInfo::Actions& actionDescs, unsigned street)
{
    try
    {
        // return projection
        static const bson::bo returnProjection = bson::bob().append("players.$", 1).append("_id", 0).obj();

        std::vector<double> equities;

        // query all games with equity info about this player

        // prepare list of actions
        const std::vector<bson::bo> actions = GetActionsFilter(actionDescs);

        // prepare query
        const std::string streetsAndActions = std::string("streets.") + conv::cast<std::string>(street) + std::string(".actions");
        mongo::Query query =
        BSON("players" << BSON("$elemMatch" 
                << BSON("equities.0" << BSON("$exists" << 1)
                        << streetsAndActions << BSON("$all"
                                                << actions))));
        query.sort("_id", 0);


        // fetch 
        const std::auto_ptr<mongo::DBClientCursor> cursor = m_Connection.query
        (
            cfg::STAT_COLLECTION_NAME, 
            query,
            100,
            0, 
            &returnProjection
        );

        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
            LOG_TRACE("Fetched all stats equities, data: [%s]") % data.toString();

            const std::vector<bson::be> equitiesArray = data.getFieldDotted("players.0.equities").Array();
            if (equitiesArray.size() > street)
                equities.push_back(equitiesArray[street].Double());
        }

        if (equities.empty())
            return 0;

        // find average value
        const double summ = std::accumulate(equities.begin(), equities.end(), double());
        LOG_TRACE("All stats equities: [%s], summ: [%s], result: [%s]") % equities % summ % static_cast<float>(summ / equities.size());
        return static_cast<float>(summ / equities.size());
    }
    CATCH_PASS(cmn::Exception("Failed to GetEquity from all stats"))
}

bson::bo BuildPlayer(const pcmn::Player& player) const
{
    bson::bob builder;

    // player hand description on different streets
    const pcmn::Player::Hands& hands = player.GetHands();

    std::vector<bson::bo> streets;
    for (const pcmn::Player::ActionDesc::List& street : player.GetActions())
    {
        std::vector<bson::bo> actions;
        for (const pcmn::Player::ActionDesc& action : street)
        {
            actions.push_back
            (
                bson::bob()
                    .append("id", action.m_Id)
                    .append("amount", action.m_Amount)
                    .append("position", action.m_Position)
                    .append("reason", action.m_ReasonId)
                    .append("bet", action.m_ReasonAmount)
                    .append("count", action.m_Count).obj()
            );
        }

        std::vector<unsigned> hand;
        if (hands.size() > streets.size()) 
            hand = conv::cast<std::vector<unsigned>>(static_cast<unsigned>(hands[streets.size()]));

        streets.push_back(bson::bob().append("actions", actions).append("hand", hand).obj());
    }

    std::vector<int> cards;
    for (const pcmn::Card& card : player.Cards())
        cards.push_back(card.ToEvalFormat());

    builder
        .append("name", player.Name())
        .append("stack", player.Stack())
        .append("streets", streets)
        .append("cards", cards)
        .append("equities", player.Equities());

    return builder.obj();
}

pcmn::Hand::Value GetHandFromStreet(const bson::be& street)
{
    const std::vector<bson::be> hands = street.Obj()["hand"].Array();
    std::vector<unsigned> bits;
    for (const bson::be& element : hands)
        bits.push_back(element.Int());

    return static_cast<pcmn::Hand::Value>(conv::cast<unsigned>(bits));   
}

std::vector<bson::bo> GetActionsFilter(const pcmn::Player::ActionDesc::List& actionsDscs, const bool reason = false)
{
    std::vector<bson::bo> actions;
    for (const pcmn::Player::ActionDesc& actionDsc : actionsDscs)
    {
        bson::bo object;
        if (reason)
        {
            object = BSON(   "id" << actionDsc.m_Id << 
                                            "amount" << actionDsc.m_Amount << 
                                            "position" << actionDsc.m_Position <<
                                            "reason" << actionDsc.m_ReasonId << 
                                            "bet" << actionDsc.m_ReasonAmount << 
                                            "count" << actionDsc.m_Count);
        }
        else
        {
            object = BSON(   "id" << actionDsc.m_Id << 
                                            "amount" << actionDsc.m_Amount << 
                                            "position" << actionDsc.m_Position);
        }
        const bson::bo match = BSON("$elemMatch" << object);
        actions.push_back(match);
    }
    return actions;
}

private:
	ILog& m_Log;
	mongo::DBClientConnection m_Connection;
};

MongoStatistics::MongoStatistics(ILog& logger) : m_Impl(new Impl(logger))
{

}

MongoStatistics::~MongoStatistics()
{
	delete m_Impl;
}

void MongoStatistics::Write(pcmn::TableContext::Data& data)
{
	m_Impl->Write(data);
}

unsigned MongoStatistics::GetRanges(PlayerInfo::List& players) const
{
	return m_Impl->GetRanges(players);
}

void MongoStatistics::GetEquities(PlayerInfo::List& players, unsigned street) const
{
	m_Impl->GetEquities(players, street);
}

pcmn::Player::Style::Value srv::MongoStatistics::GetAverageStyle(const std::string& target, const std::string& opponent) const 
{
    return m_Impl->GetAverageStyle(target, opponent);
}

void MongoStatistics::GetHands(PlayerInfo& player, unsigned street, unsigned count) const 
{
    m_Impl->GetHands(player, street, count);
}

void MongoStatistics::GetActions(IStatistics::PlayerInfo& player, pcmn::Board::Value board, unsigned street, unsigned count) const 
{
    m_Impl->GetActions(player, board, street, count);
}

}

