#include "NeuroNetwork.h"
#include "exception/CheckHelpers.h"
#include "Config.h"

#include <fann/floatfann.h>

#include <boost/scoped_array.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

namespace neuro
{
	Network::Network(const std::string& file)
	{
		CHECK(m_Net = fann_create_from_file(file.c_str()), cmn::Exception("Failed to open network"), file);
	}

	Network::~Network()
	{
		fann_destroy(m_Net);
	}

	void Network::Process(const std::vector<float>& input, std::vector<float>& output)
	{
		CHECK(m_Net, cmn::Exception("Network not initialized"));
		CHECK(input.size() == cfg::INPUT_COUNT, cmn::Exception("Wrong input count"), input);
		output.clear();

		fann_reset_MSE(m_Net);
		const float* results = fann_run(m_Net, const_cast<float*>(&input.front()));
		std::copy(results, results + cfg::OUTPUT_COUNT, std::back_inserter(output));
	}

} // namespace neuro


