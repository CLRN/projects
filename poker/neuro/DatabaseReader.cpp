#include "DatabaseReader.h"
#include "exception/CheckHelpers.h"
#include "Config.h"
#include "db/SQLiteDB.h"
#include "Params.h"

#include <boost/format.hpp>
#include <boost/thread/mutex.hpp>

namespace neuro
{
	DatabaseReader::DatabaseReader(ILog& logger, const std::string& file) : m_Db(new sql::SQLiteDataBase(logger))
	{
        m_Db->Open(file);
	}

	DatabaseReader::~DatabaseReader()
	{
		delete m_Db;
	}

	void DatabaseReader::Process(const std::vector<float>& input, std::vector<float>& output)
	{
        static boost::mutex mx;
        boost::mutex::scoped_lock lock(mx);

		CHECK(input.size() == cfg::INPUT_COUNT, cmn::Exception("Wrong input size"), input, cfg::INPUT_COUNT);
        output.clear();

        Params params;
        params.FromNeuroFormat(input, output);

        const std::string where
        (
            (
                boost::format
                (
                    " win = %s "
                    "AND position = %s "
                    "AND bet = %s "
                    "AND players = %s "
                    "AND danger = %s "
                    "AND bot_avg_style = %s "
                    "AND bot_style = %s "
                    "AND bot_stack = %s "
                ) 
                % int(params.m_WinRate)
                % int(params.m_Position)
                % int(params.m_BetSize)
                % int(params.m_ActivePlayers)
                % int(params.m_Danger)
                % int(params.m_BotAverageStyle)
                % int(params.m_BotStyle)
                % int(params.m_BotStackSize)
            ).str()
        );

        Params::List result;
        Params::ReadAll(result, *m_Db, where);

        CHECK(result.size() == 1, cmn::Exception("Invalid result size for query"), result.size(), where);

        output.push_back(static_cast<float>(result.front().m_CheckFold));
        output.push_back(static_cast<float>(result.front().m_CheckCall));
        output.push_back(static_cast<float>(result.front().m_BetRaise));
	}

} // namespace neuro


