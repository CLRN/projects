#include "common/Log.h"

#include <gtest/gtest.h>

#include "../statslib/StatisticsCache.h"

#include <boost/assign/list_of.hpp>
#include <boost/chrono.hpp>

TEST(Stats, Hands)
{
    Log log;
    stats::Cache::Create(log);

    if (1)
    {
        const stats::IPlayer::Ptr stat = stats::Cache::Instance().GetPlayerStatistics("Meigo84");

        using namespace pcmn;
        const Card::List board = boost::assign::list_of(Card(Card::Two, Suit::Diamonds))(Card(Card::Nine, Suit::Spades))(Card(Card::Three, Suit::Diamonds));
        stat->Prefetch(board);

        pcmn::Player testPlayer("Meigo84", 0);
        //testPlayer.PushAction(0, Action::Call, BetSize::Low, Player::Position::Later, Action::BigBlind, BetSize::Low, pcmn::Player::Count::ThreeOrMore);
        //testPlayer.PushAction(1, Action::Raise, BetSize::High, Player::Position::Later, Action::Raise, BetSize::High, pcmn::Player::Count::ThreeOrMore);

        //testPlayer.PushAction(1, Action::Bet, BetSize::Low, Player::Position::Middle, Action::Check, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);
        //testPlayer.PushAction(1, Action::Call, BetSize::High, Player::Position::Later, Action::Raise, BetSize::High, pcmn::Player::Count::ThreeOrMore);
        //testPlayer.PushAction(1, Action::Call, BetSize::High, Player::Position::Later, Action::Raise, BetSize::Normal, pcmn::Player::Count::Two);
        // testPlayer.PushAction(0, Action::Raise, BetSize::Huge, Player::Position::Later, Action::Raise, BetSize::Huge, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(0, Action::Call, BetSize::Low, Player::Position::Middle, Action::BigBlind, BetSize::Low, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(0, Action::Call, BetSize::High, Player::Position::Later, Action::Raise, BetSize::Huge, pcmn::Player::Count::ThreeOrMore);
        //testPlayer.PushAction(1, Action::Call, BetSize::High, Player::Position::Early, Action::Bet, BetSize::High, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(1, Action::Call, BetSize::High, Player::Position::Middle, Action::Bet, BetSize::High, pcmn::Player::Count::ThreeOrMore);
        // testPlayer.PushAction(1, Action::Raise, BetSize::Huge, Player::Position::Early, Action::Bet, BetSize::Normal, pcmn::Player::Count::ThreeOrMore);

        const stats::IPlayer::Id id = { testPlayer.GetActions(), board };
        const stats::IPlayer::Hands hands = stat->GetHands(id);
    }

    {
        using namespace pcmn;

        const std::vector<int> boardCards = boost::assign::list_of(5)(29)(26)(17);
        Card::List board;
        for (const int c : boardCards)
            board.push_back(Card().FromEvalFormat(c));

        const stats::IPlayer::Ptr stat = stats::Cache::Instance().GetPlayerStatistics("CLRN");
        stat->Prefetch(board);

        pcmn::Player testPlayer("CLRN", 0);
        testPlayer.PushAction(0, Action::Call, BetSize::High, Player::Position::Later, Action::Raise, BetSize::High, pcmn::Player::Count::ThreeOrMore);
        //testPlayer.PushAction(1, Action::Bet, BetSize::High, Player::Position::Middle, Action::Check, BetSize::NoBet, pcmn::Player::Count::Two);
        testPlayer.PushAction(1, Action::Raise, BetSize::High, Player::Position::Later, Action::Raise, BetSize::High, pcmn::Player::Count::ThreeOrMore);

        const stats::IPlayer::Id id = { testPlayer.GetActions(), board };
        const stats::IPlayer::Hands hands = stat->GetHands(id);
    }
    /*
    {
        srv::HandsDetector::Result result;

        const std::vector<int> boardCards = boost::assign::list_of(5)(29)(26)(17);
        Card::List board;
        for (const int c : boardCards)
            board.push_back(Card().FromEvalFormat(c));

        pcmn::Player testPlayer("CLRN", 0);
        testPlayer.PushAction(0, Action::Raise, BetSize::High, Player::Position::Later, Action::BigBlind, BetSize::Low, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(1, Action::Bet, BetSize::High, Player::Position::Middle, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(2, Action::Bet, BetSize::High, Player::Position::Later, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);

        detector.DetectHand(board, testPlayer, result, 9);
    }

    {
        srv::HandsDetector::Result result;

        const std::vector<int> boardCards = boost::assign::list_of(21)(28)(36)(13)(8);
        Card::List board;
        for (const int c : boardCards)
            board.push_back(Card().FromEvalFormat(c));

        const std::vector<int> playerCards = boost::assign::list_of(9)(5);
        Card::List player;
        for (const int c : playerCards)
            player.push_back(Card().FromEvalFormat(c));

        pcmn::Player testPlayer("CLRN", 0);
        testPlayer.PushAction(0, Action::Call, BetSize::Low, Player::Position::Middle, Action::BigBlind, BetSize::Low, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(1, Action::Check, BetSize::NoBet, Player::Position::Early, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(2, Action::Check, BetSize::NoBet, Player::Position::Early, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(3, Action::Bet, BetSize::High, Player::Position::Middle, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);

        detector.DetectHand(board, testPlayer, result, 9);
    }
    {
        srv::HandsDetector::Result result;

        const std::vector<int> boardCards = boost::assign::list_of(5)(29)(26)(17);
        Card::List board;
        for (const int c : boardCards)
            board.push_back(Card().FromEvalFormat(c));

        pcmn::Player testPlayer("CLRN", 0);
        testPlayer.PushAction(0, Action::Call, BetSize::High, Player::Position::Later, Action::Raise, BetSize::High, pcmn::Player::Count::ThreeOrMore);
        testPlayer.PushAction(1, Action::Bet, BetSize::High, Player::Position::Middle, Action::Unknown, BetSize::NoBet, pcmn::Player::Count::ThreeOrMore);

        detector.DetectHand(board, testPlayer, result, 9);
    }*/
}