#ifndef PlayerStatistics_h__
#define PlayerStatistics_h__

#include "Actions.h"
#include "Player.h"
#include "Table.h"
#include "Board.h"
#include "common/ILog.h"
#include "Cards.h"

#include <map>

#include <boost/shared_ptr.hpp>

namespace stats
{

//! Statistics for concrete player
class IPlayer
{
public:

    //! Pointer type
    typedef boost::shared_ptr<IPlayer> Ptr;

    //! List of actions
    typedef pcmn::Player::ActionDesc::List Actions;

    //! Hands percentages
    typedef std::map<pcmn::Hand::Value, float> Hands;

    //! Actions percentages
    typedef std::map<pcmn::Action::Value, float> ActionMap;

    //! Player reaction map
    typedef std::map<pcmn::BetSize::Value, ActionMap> ReactionMap;

    //! Data identifier type
    struct Id
    {
        pcmn::Player::Actions m_Actions;
        pcmn::Card::List m_Board;
    };

    //! Dtor
    virtual ~IPlayer() {}

    //! Prepare cache data
    virtual void Prefetch(const pcmn::Card::List& flop) = 0;

    //! Fetch all player data
    virtual void Fetch() = 0;

    //! Get all possible hands
    virtual Hands GetHands(const Id& id) = 0;

    //! Get all possible remaining actions
    virtual Actions GetRemainingActions(const pcmn::Player::Actions& actions) const = 0;

    //! Get all possible reactions
    virtual const ReactionMap& GetReaction(pcmn::Table::Phase::Value street, pcmn::Board::Value board, pcmn::Player::Position::Value pos, pcmn::Player::Count::Value count) const = 0;

    //! Create instance by player name
    static Ptr Instance(ILog& log, const std::string& name);

    //! Create instance by unknown player
    static Ptr Instance(ILog& log);

};

} // namespace stats

#endif // PlayerStatistics_h__
