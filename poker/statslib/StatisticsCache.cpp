#include "StatisticsCache.h"
#include "common/CachingProxy.h"

#include <boost/thread/mutex.hpp>
#include <boost/make_shared.hpp>

namespace stats
{

class StatisticsCacheImpl : public Cache
{
    typedef cmn::cache::Proxy<IPlayer::Ptr> Proxy;
    typedef std::map<std::string, Proxy::Ptr> PlayerMap;
public:

    StatisticsCacheImpl(ILog& log) : m_Log(log)
    {

    }

    virtual IPlayer::Ptr GetAverageStatistics() override
    {
        static IPlayer::Ptr stats;
        static boost::mutex mutex;
        boost::unique_lock<boost::mutex> lock(mutex);
        if (!stats)
        {
            stats = IPlayer::Instance(m_Log);
            stats->Fetch();
        }
        return stats;
    }

    //! Get concrete player statistics
    virtual IPlayer::Ptr GetPlayerStatistics(const std::string& name) override
    {
        Proxy::Ptr result;

        {
            boost::unique_lock<boost::mutex> lock(m_Mutex);
            auto it = m_Players.find(name);
            if (it == m_Players.end())
            {
                const auto proxy = cmn::cache::Factory<IPlayer::Ptr>::Instance
                (
                    [this, name]()
                    {
                        const auto player = IPlayer::Instance(m_Log, name);
                        player->Fetch();
                        return player;
                    },
                    [this, name]()
                    {
                        boost::unique_lock<boost::mutex> lock(m_Mutex);
                        m_Players.erase(name);
                    },
                    boost::posix_time::minutes(1),
                    boost::posix_time::minutes(10)
                );

                it = m_Players.insert(std::make_pair(name, proxy)).first;
            }
            result = it->second;
        }
        return *result;
    }

private:
    ILog& m_Log;
    boost::mutex m_Mutex;
    PlayerMap m_Players; 
};

Cache& Cache::Instance()
{
    return *s_Instance;
}

void Cache::Create(ILog& log)
{
    s_Instance.reset(new StatisticsCacheImpl(log));
}

std::unique_ptr<Cache> Cache::s_Instance;

} // namespace stats