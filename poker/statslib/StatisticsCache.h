#ifndef StatisticsCache_h__
#define StatisticsCache_h__

#include "common/ILog.h"

#include <string>
#include <memory>

#include "PlayerStatistics.h"

namespace stats
{

class Cache
{
public:

    //! Create statistics
    static void Create(ILog& log);

    //! Single instance
    static Cache& Instance();

    //! Get all players statistics
    virtual IPlayer::Ptr GetAverageStatistics() = 0;

    //! Get concrete player statistics
    virtual IPlayer::Ptr GetPlayerStatistics(const std::string& name) = 0;

private:

    static std::unique_ptr<Cache> s_Instance;
};


} // namespace stats


#endif // StatisticsCache_h__
