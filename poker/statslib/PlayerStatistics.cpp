#include "PlayerStatistics.h"
#include "Hand.h"
#include "Config.h"
#include "common/Modules.h"
#include "StatisticsCache.h"
#include "CardsErrorInfo.h"

#include <map>

#pragma warning(push)
#pragma warning(disable : 4005) // 'WIN32_LEAN_AND_MEAN' : macro redefinition
#include <mongo/client/dbclient.h>
#include <mongo/bson/bsonelement.h>
#pragma warning(pop) 

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/sequenced_index.hpp>

namespace stats
{

namespace mi = boost::multi_index;
DECLARE_CURRENT_MODULE(Modules::DataBase);
enum { AVERAGE_TO_CONCRETE_HANDS_RATIO = 10 }; 

//! Connection pool
class ConnectionPool
{
    typedef mongo::DBClientConnection Connection;
    typedef boost::shared_ptr<Connection> Ptr;
    typedef std::map<boost::thread::id, Ptr> Map;

    ConnectionPool() {}
public:

    static ConnectionPool& Instance()
    {
        static ConnectionPool instance;
        return instance;
    }

    Connection& Get() 
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        auto it = m_Map.find(boost::this_thread::get_id());
        if (it == m_Map.end())
        {
            it = m_Map.insert(std::make_pair(boost::this_thread::get_id(), boost::make_shared<Connection>())).first;
            it->second->connect(cfg::DB_SERVER_NAME);
        }
        return *it->second;
    }

private:
    boost::mutex m_Mutex;
    Map m_Map;
};

template<typename T>
T GetBits(const bson::bo& player, const std::string& field)
{
    const std::vector<bson::be> values = player[field].Array();
    std::vector<unsigned> bits;
    for (const bson::be& element : values)
        bits.push_back(element.Int());

    if (bits.size() == 1 && bits.front() == 0)
    {
        bits.clear(); // workaround for 'Unknown' board, see MongoStatistics.cpp:48
        // TODO: update database and then remove this workarounds 
    }

    return static_cast<T>(conv::cast<unsigned>(bits));   
}

//! Implementation of the all players statistics or average statistics
class AverageStatistics : public IPlayer
{
public:

    //! Tags
    struct ActionFull {};       //!< find by all action fields
    struct StreetFull {};       //!< find by all action fields
    struct ActionSequenced {};  //!< sequenced actions

    //! Hand description
    struct HandDesc
    {
        typedef std::list<HandDesc> List;
        HandDesc() {}
        HandDesc(const pcmn::Card::List* cards, float w) : m_Cards(cards), m_Weight(w) {}
        const pcmn::Card::List* m_Cards;
        float m_Weight;
    };

    //! Street data for player
    struct StreetData
    {
        typedef std::list<StreetData> List;
        typedef std::map<pcmn::Hand::Value, unsigned> HandCounters;
    
        typedef mi::multi_index_container
        <
            pcmn::Player::ActionDesc,
            mi::indexed_by
            <
                mi::sequenced
                <
                    mi::tag<ActionSequenced>
                >,
                mi::ordered_unique
                <
                    mi::tag<ActionFull>, 
                    mi::composite_key
                    <
                        pcmn::Player::ActionDesc,
                        mi::member<pcmn::Player::ActionDesc, pcmn::Action::Value, &pcmn::Player::ActionDesc::m_Id>,
                        mi::member<pcmn::Player::ActionDesc, pcmn::BetSize::Value, &pcmn::Player::ActionDesc::m_Amount>,
                        mi::member<pcmn::Player::ActionDesc, pcmn::Player::Position::Value, &pcmn::Player::ActionDesc::m_Position>,
                        mi::member<pcmn::Player::ActionDesc, pcmn::Action::Value, &pcmn::Player::ActionDesc::m_ReasonId>,
                        mi::member<pcmn::Player::ActionDesc, pcmn::BetSize::Value, &pcmn::Player::ActionDesc::m_ReasonAmount>,
                        mi::member<pcmn::Player::ActionDesc, pcmn::Player::Count::Value, &pcmn::Player::ActionDesc::m_Count>
                    >
                >
            >
        > ActionsSet;

        //! Find by list of actions
        struct FullSorter
        {
            bool operator () (const StreetData& lhs, const StreetData& rhs) const
            {
                return lhs.m_Actions.get<ActionFull>() < rhs.m_Actions.get<ActionFull>();
            }
        };

        typedef mi::multi_index_container
        <
            StreetData,
            mi::indexed_by
            <
                mi::ordered_unique
                <
                    mi::tag<StreetFull>, 
                    mi::identity<StreetData>,
                    FullSorter
                >
            >
        > Set;

        StreetData(const bson::bo& player, pcmn::Table::Phase::Value streetId)
        {
            // get all actions
            const std::vector<bson::be> actions = player["actions"].Array();
            for (const bson::be& action : actions)
            {
                Actions::value_type dsc;
                dsc.m_Id = static_cast<pcmn::Action::Value>(action["id"].Int());
                dsc.m_Amount = static_cast<pcmn::BetSize::Value>(action["amount"].Int());
                dsc.m_Position = static_cast<pcmn::Player::Position::Value>(action["position"].Int());
                dsc.m_ReasonId = static_cast<pcmn::Action::Value>(action["reason"].Int());
                dsc.m_ReasonAmount = static_cast<pcmn::BetSize::Value>(action["bet"].Int());
                dsc.m_Count = static_cast<pcmn::Player::Count::Value>(action["count"].Int());
                m_Actions.insert(m_Actions.end(), std::move(dsc));
            }
        }

        bool IsEqualAction(pcmn::Action::Value lhs, pcmn::Action::Value rhs) const
        {
            if ((lhs == pcmn::Action::Raise || lhs == pcmn::Action::Bet) && (rhs == pcmn::Action::Raise || rhs == pcmn::Action::Bet))
                return true;
            return lhs == rhs;
        }


        //! Needed to make search by incomplete list, during request
        bool IsEqual (const pcmn::Player::ActionDesc::List& actions, const bool fullSearch = true) const
        {
            for (const auto& outerAction : actions)
            {
                bool found = false;
                for (const auto& thisAction : m_Actions)
                {
                    if (thisAction.m_Amount == outerAction.m_Amount &&
                        thisAction.m_Position == outerAction.m_Position &&
                        thisAction.m_ReasonId == outerAction.m_ReasonId &&
                        thisAction.m_ReasonAmount == outerAction.m_ReasonAmount &&
                            (fullSearch ?
                                (
                                    thisAction.m_Count == outerAction.m_Count &&
                                    IsEqualAction(thisAction.m_Id, outerAction.m_Id)
                                ) : 
                                (
                                    true
                                )
                            )
                        )
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }

        ActionsSet m_Actions;                       //!< all player actions on this street
        mutable HandCounters m_Hands;               //!< player hands counters
        mutable HandDesc::List m_HandsDescription;  //!< precalculated hand weights
    };

    //! Hands percentage calculation helper
    struct HandsHolder
    {
        StreetData::HandCounters m_Hands;
        unsigned m_MatchedCount;

        HandsHolder() : m_MatchedCount() {}

        void Merge(const StreetData::HandCounters& counters)
        {
            ++m_MatchedCount;
            for (const auto& pair : counters)
                m_Hands[pair.first] += pair.second;   
        }

        Hands Calculate() const
        {
            Hands result;
            float total = 0;
            for (const auto& pair : m_Hands)
                total += pair.second;

            for (auto& pair : m_Hands)
                result[pair.first] = pair.second / total;    

            return result;
        }

        bool IsWellMatched() const 
        {
            return m_MatchedCount >= 3 && m_Hands.size() > 3;
        }
    };

    //! Games on different streets
    typedef std::vector<StreetData::List> GamesList;

    //! All actions on different streets
    typedef std::vector<StreetData::Set> RawGamesList;

    //! Reactions on different boards
    typedef std::map<pcmn::Board::Value, ReactionMap> Board2Reactions;

    //! Reactions on different positions
    typedef std::map<pcmn::Player::Position::Value, Board2Reactions> Position2Reactions;

    //! Different reaction on different players count
    typedef std::map<pcmn::Player::Count::Value, Position2Reactions> PlayerCount2Position2Reactions;

    //! Reactions on streets
    typedef std::vector<PlayerCount2Position2Reactions> ReactionMaps;

    //! Position to average reaction
    typedef std::map<pcmn::Player::Position::Value, ReactionMap> AveragePosition2Reactions;

    //! Average player count to average reaction
    typedef std::map<pcmn::Player::Count::Value, AveragePosition2Reactions> AverageCount2Position2Reactions;

    //! Average reactions on streets
    typedef std::vector<AverageCount2Position2Reactions> AverageReactionMaps;

    //! Preprocessed hands
    typedef std::unordered_map<const pcmn::Card::List*, pcmn::Hand::Value> PreprocessedHands;

    //! Preprocessed hands by flop
    typedef std::map<pcmn::Card::List, PreprocessedHands> PreprocessedFlop;

    AverageStatistics(ILog& log) 
        : m_Log(log)
        , m_Reaction(pcmn::Table::Phases().size())
        , m_AverageReactions(pcmn::Table::Phases().size())
    {      
        Prefetch(pcmn::Card::List()); // prefetch preflop
    }

    const RawGamesList& GetDBData() const
    {
        return m_DatabaseData;
    }

    RawGamesList& GetDBData()
    {
        return m_DatabaseData;
    }

    const ReactionMaps& GetReactions() const
    {
        return m_Reaction;
    }

    const AverageReactionMaps& GetAverageReactions() const
    {
        return m_AverageReactions;
    }

protected:

    virtual std::auto_ptr<mongo::DBClientCursor> MakeCursor()
    {
        // query all games
        mongo::Query query;
        //query.sort("_id", 0);

        // fetch 
        return ConnectionPool::Instance().Get().query
        (
            cfg::STAT_COLLECTION_NAME, 
            query
        );
    }

    virtual bool IsPlayerDataNeeded(const bson::bo& player) 
    {
        return true;
    }

    void FetchData()
    {
        m_DatabaseData.resize(pcmn::Table::Phases().size());

        const std::auto_ptr<mongo::DBClientCursor> cursor = MakeCursor();
        while (cursor->more()) 
        {
            const bson::bo data = cursor->next();
#ifdef _DEBUG
            const auto text = data.toString();
            LOG_TRACE("Fetched all stats, data: [%s]") % text;
#endif // _DEBUG

            // board description
            const std::vector<bson::be> boardsArray = data.getField("board").Array();

            // get board description for all streets
            std::vector<pcmn::Board::Value> boards;
            boards.reserve(pcmn::Table::Phases().size());
            for (const bson::be& boardObj : boardsArray)
                boards.push_back(GetBits<pcmn::Board::Value>(boardObj.Obj(), "street"));

            // for each player
            const std::vector<bson::be> players = data.getField("players").Array();
            for (const bson::be& playerObj : players)
            {
                const bson::bo player = playerObj.Obj();
                if (!IsPlayerDataNeeded(player))
                    continue;

                // get cards
                const auto cardsArray = player.getField("cards").Array();
#ifdef _DEBUG
                pcmn::Card::List cards;
                {
                    for (const auto& card : cardsArray)
                        cards.push_back(pcmn::Card().FromEvalFormat(card.Int()));
                }
#endif // _DEBUG

                // for each street
                const std::vector<bson::be> streets = player.getField("streets").Array();
                for (const auto streetId : pcmn::Table::Phases())
                {
                    if (streetId == streets.size())
                        break;

                    const bson::bo street = streets[streetId].Obj();
                    const pcmn::Board::Value board = boards.size() > streetId ? boards[streetId] : boards.back();

                    StreetData data(street, streetId);

                    // process reaction
                    for (const auto& desc : data.m_Actions)
                    {
                        ++m_Reaction[streetId][desc.m_Count][desc.m_Position][board][desc.m_ReasonAmount][desc.m_Id];
                        ++m_AverageReactions[streetId][desc.m_Count][desc.m_Position][desc.m_ReasonAmount][desc.m_Id];
                    }

                    // process hands
                    if (!cardsArray.empty())
                    {
#ifdef _DEBUG
                        pcmn::Player::ActionDesc::List actions;
                        boost::copy(data.m_Actions, std::back_inserter(actions));
#endif // _DEBUG

                        pcmn::Hand::Value hand = GetBits<pcmn::Hand::Value>(street, "hand");
                        if (streetId)
                            hand = static_cast<pcmn::Hand::Value>(hand & (pcmn::Hand::FLOP_HAND_MASK | pcmn::Hand::POWER_MASK));
                        if (streetId == pcmn::Table::Phase::River) // remove draws from river
                            hand = static_cast<pcmn::Hand::Value>(hand & ~pcmn::Hand::DRAWS_MASK);

                        auto& currentData = m_DatabaseData[streetId];
                        const auto it = currentData.insert(std::move(data)).first;
                        ++it->m_Hands[hand];
                    }
                }
            }
        }
    }

    void ProcessHands()
    {
        if (m_DatabaseData.empty())
            return;

        // get all possible cards
        const pcmn::Board::HandList& allPocketCards = pcmn::Board().GetAllPocketCards();

        // for each actions
        for (const StreetData& current : m_DatabaseData.front())
        {
            // calculate hand percentages first
            HandsHolder holder;
            holder.Merge(current.m_Hands);

            const Hands percentages = holder.Calculate();
            for (const auto& currentCards : allPocketCards)
            {
                const static pcmn::Card::List emptyBoard;
                const pcmn::Hand::Value parsed = pcmn::Hand().Parse(currentCards, emptyBoard).GetValue();
                const auto hand = percentages.find(parsed);
                if (hand != percentages.end())
                    current.m_HandsDescription.push_back(HandDesc(&currentCards, hand->second));
            }
        }
    }

    void ProcessReactions()
    {
        // calculate percentages instead of counters
        for (auto& streetData : m_Reaction)
        {
            for (auto& positionAndReactionMap : streetData)
            {
                for (auto& boardAndCountMap : positionAndReactionMap.second)
                {
                    for (auto& boardAndReactionMap : boardAndCountMap.second)
                    {
                        for (auto& betSizeAndActionMap : boardAndReactionMap.second)
                        {
                            float counter = 0;
                            for (auto& actionAndCounter : betSizeAndActionMap.second)
                                counter += actionAndCounter.second;

                            for (auto& actionAndCounter : betSizeAndActionMap.second)
                                actionAndCounter.second /= counter;
                        }
                    }
                }
            }
        }

        for (auto& streetData : m_AverageReactions)
        {
            for (auto& countMap : streetData)
            {
                for (auto& positionAndReactionMap : countMap.second)
                {
                    for (auto& reactionMap : positionAndReactionMap.second)
                    {
                        float counter = 0;
                        for (auto& actionAndCounter : reactionMap.second)
                            counter += actionAndCounter.second;

                        for (auto& actionAndCounter : reactionMap.second)
                            actionAndCounter.second /= counter;
                    }
                }
            }
        }
    }

    virtual void Fetch() override
    {
        FetchData();
        ProcessHands();
        ProcessReactions();
    }

    const Hands& GetDefaultHands() const
    {
        static Hands result;
        static const pcmn::Card::List empty;

        if (!result.empty())
            return result;

        HandDesc::List hands;
        const pcmn::Board::HandList& allPocketCards = pcmn::Board().GetAllPocketCards();
        for (const auto& currentCards : allPocketCards)
            hands.push_back(HandDesc(&currentCards, 1.0f));

        GetAllPossibleHands(hands, result, pcmn::Table::Phase::Preflop, empty);
        return result;
    }

    //! Get all possible hands
    virtual Hands GetHands(const Id& id) override
    {
        if (id.m_Actions.empty())
            return GetDefaultHands();

        // filter cards by each street
        HandDesc::List hands;
        pcmn::Card::List board;
        pcmn::Table::Phase::Value street = static_cast<pcmn::Table::Phase::Value>(id.m_Actions.size() - 1);
        for (; street < id.m_Actions.size(); street = static_cast<pcmn::Table::Phase::Value>(street + 1))
        {
            CHECK(m_DatabaseData.size() > street, cmn::Exception("Database data for this street doesn't exists"), street, m_DatabaseData.size());

            // prepare board
            board = id.m_Board;
            const auto needBoardSize = pcmn::Table::Phase::ToCardsCount(street);
            CHECK(board.size() >= needBoardSize, cmn::Exception("Expected more cards on flop"), needBoardSize, board);
            board.resize(needBoardSize);

            // find this actions in the database data
            const Actions& actions = id.m_Actions[street];

            HandsHolder holder;
            for (const StreetData& data : m_DatabaseData[street])
            {
                if (data.IsEqual(actions))
                    holder.Merge(data.m_Hands);
            }

            const Ptr average = Cache::Instance().GetAverageStatistics();
            const AverageStatistics& avg = static_cast<const AverageStatistics&>(*average);

            // we got some statistics for player, but hand history is not enough, merge it with average
            if (!holder.IsWellMatched())
            {
                // get from average statistics
                for (const StreetData& data : avg.GetDBData()[street])
                {
                    if (data.IsEqual(actions))
                        holder.Merge(data.m_Hands);
                }
            }

            if (!holder.IsWellMatched())
            {
                // get from average statistics with less strict search
                for (const StreetData& data : avg.GetDBData()[street])
                {
                    if (data.IsEqual(actions, false))
                        holder.Merge(data.m_Hands);
                }
            }

//             if (holder.m_Hands.empty() && actions.size() > 1)
//             {
//                 // use most aggressive actions only
//                 Id newId = id;
//                 boost::sort(newId.m_Actions[street]);
//                 newId.m_Actions[street].erase(newId.m_Actions[street].begin());
//                 return GetHands(newId);
//             }

            if (!holder.IsWellMatched())
                return Hands(); // failed to detect hands, it's mean that this actions is impossible

            const Hands currentHands = holder.Calculate();
            //if (street == pcmn::Table::Phase::Preflop && id.m_Actions.size() == 1)
                return currentHands; // no need to calculate anything else, we got percentages from statistics

            // ensure that all hands are parsed
            Prefetch(board);
            boost::shared_lock<boost::shared_mutex> lock(s_Mutex);
            const PreprocessedHands& preprocessed = s_PreprocessedFlop[board];
            lock.unlock();

            // need to initialize hands list
            if (hands.empty())
            {
                const pcmn::Board::HandList& allPocketCards = pcmn::Board().GetAllPocketCards();
                for (const auto& currentCards : allPocketCards)
                {
                    const auto parsed = preprocessed.find(&currentCards);
                    if (parsed == preprocessed.end())
                        continue;

                    const auto hand = currentHands.find(parsed->second);
                    if (hand != currentHands.end())
                        hands.emplace_back(&currentCards, hand->second);
                }
                continue;
            }

            // apply weights from this street
            auto it = hands.begin();
            const auto end = hands.end();
            for (; it != end; )
            {
                const auto parsed = preprocessed.find(it->m_Cards);

                const auto hand = parsed != preprocessed.end() ? currentHands.find(parsed->second) : currentHands.end();
                if (hand != currentHands.end())
                {
                    it->m_Weight *= (hand->second * street);
                    ++it;
                }
                else
                {
                    it = hands.erase(it);
                }
            }
        }

        Hands result;
        GetAllPossibleHands(hands, result, static_cast<pcmn::Table::Phase::Value>(id.m_Actions.size() - 1), board);
        return result;
    }

    //! Get all possible reactions
    virtual const ReactionMap& GetReaction(pcmn::Table::Phase::Value street, pcmn::Board::Value board, pcmn::Player::Position::Value pos, pcmn::Player::Count::Value count) const override
    {
        CHECK(m_Reaction.size() > street, cmn::Exception("Invalid street id"), street, m_Reaction.size());

        const auto countIt = m_Reaction[street].find(count);
        CHECK(countIt != m_Reaction[street].end(), cmn::Exception("Reactions data for this player count is absent"), board, street, pos, count);

        const auto position = countIt->second.find(pos);
        CHECK(position != countIt->second.end(), cmn::Exception("Failed to find reaction by position"), board, street, pos, count);

        const auto boardIt = position->second.find(board);
        if (boardIt == position->second.end() && !position->second.empty())
        {
            // ok, we don't have statistics for this table description, we can made it by partial matching board description
            std::vector<ReactionMap> mergedReactions;
            for (const auto& pair : position->second)
            {
                if (pair.first & board)
                {
                    // this table partially matched
                    mergedReactions.push_back(pair.second);
                }
            }

            // ok, now merge all this matched maps into one
            ReactionMap result;
            for (const auto& map : mergedReactions)
            {
                for (const auto& betAndReactions : map)
                {
                    for (const auto& actionAndPercent : betAndReactions.second)
                        result[betAndReactions.first][actionAndPercent.first] += actionAndPercent.second;
                }
            }

            // now recalculate percentages to reach 100% at summ
            for (auto& betAndReactionMap : result)
            {
                float summ = 0.0f;
                for (const auto& actionAndPercent : betAndReactionMap.second)
                    summ += actionAndPercent.second;

                for (auto& actionAndPercent : betAndReactionMap.second)
                    actionAndPercent.second = actionAndPercent.second / summ;
            }

            return const_cast<Board2Reactions&>(position->second).insert(std::make_pair(board, result)).first->second;
        }

        CHECK(boardIt != position->second.end(), cmn::Exception("Failed to find reaction by board"), board, street, pos, count);
        return boardIt->second;
    }

    template<typename T>
    void ParseHands(const pcmn::Card::List& flop, Hands& result, float &totalWeight, T& possibleHands) const
    {
        boost::shared_lock<boost::shared_mutex> lock(s_Mutex);
        const PreprocessedHands& preprocessed = s_PreprocessedFlop[flop];
        lock.unlock();

        // parse all cards and save with weights
        for (HandDesc& currentHand : possibleHands)
        {
            const auto parsed = preprocessed.find(currentHand.m_Cards);
            if (parsed == preprocessed.end())
                continue;

            result[parsed->second] += currentHand.m_Weight;
            totalWeight += currentHand.m_Weight;
        }
    }

    void GetAllPossibleHands(HandDesc::List& possibleHands, Hands& result, pcmn::Table::Phase::Value street, const pcmn::Card::List& flop) const
    {
        float totalWeight = 0.0f;
//         if (street && possibleHands.size() > 100)
//         {
//             const auto summ = boost::accumulate(possibleHands, float(), [](float val, const HandDesc& desc){ return val + desc.m_Weight; });
//             const auto average = summ / possibleHands.size();
//             const auto min = average * 9 / 10;
// 
//             const auto range = possibleHands | boost::adaptors::filtered(boost::bind(&HandDesc::m_Weight, _1) > min);
//             ParseHands(flop, result, totalWeight, range);
//         }
//         else
        {
            ParseHands(flop, result, totalWeight, possibleHands);
        }


        // recalculate weights to percents
        for (Hands::value_type& pair : result)
            pair.second = pair.second / totalWeight;

        // check results
        if (result.empty())
            result.insert(std::make_pair(pcmn::Hand::Unknown, 1.0f)); // failed to detect opponent hand

        if (result.size() == 1 && result.count(pcmn::Hand::Unknown))
           result.clear();
    }

    virtual void Prefetch(const pcmn::Card::List& flop)
    {
        boost::unique_lock<boost::shared_mutex> lock(s_Mutex);
        const auto it = s_PreprocessedFlop.insert(std::make_pair(flop, PreprocessedHands()));
        if (!it.second)
            return; // nothing to process

        // preprocessed hands
        PreprocessedHands& hands = it.first->second;

        // detect street
        const pcmn::Table::Phase::Value street = pcmn::Table::Phase::FromCardsCount(flop.size());

        const pcmn::Board::HandList& allPocketCards = pcmn::Board().GetAllPocketCards();
        for (const auto& cards : allPocketCards)
        {
            if (boost::find(flop, cards.front()) != flop.end() || boost::find(flop, cards.back()) != flop.end())
                continue;// duplicated cards

            pcmn::Hand::Value parsed = pcmn::Hand().Parse(cards, flop).GetValue();
            if (street != pcmn::Table::Phase::Preflop)
                parsed = static_cast<pcmn::Hand::Value>(parsed & (pcmn::Hand::FLOP_HAND_MASK | pcmn::Hand::POWER_MASK));
            if (street == pcmn::Table::Phase::River) // remove draws from river
                parsed = static_cast<pcmn::Hand::Value>(parsed & ~pcmn::Hand::DRAWS_MASK);

            CHECK(hands.insert(std::make_pair(&cards, parsed)).second, cmn::Exception("Cards duplicated") << pcmn::CardsErrorInfo(cards));
        }
    }

    virtual Actions GetRemainingActions(const pcmn::Player::Actions& actions) const override
    {
        Actions result;



        return result;
    }

private:

    ReactionMaps m_Reaction;
    AverageReactionMaps m_AverageReactions;
    RawGamesList m_DatabaseData;
    ILog& m_Log;
    static PreprocessedFlop s_PreprocessedFlop;
    static boost::shared_mutex s_Mutex;
};

IPlayer::Ptr IPlayer::Instance(ILog& log)
{
    return boost::make_shared<AverageStatistics>(log);
}

AverageStatistics::PreprocessedFlop AverageStatistics::s_PreprocessedFlop;
boost::shared_mutex AverageStatistics::s_Mutex;


class ConcretePlayerStatistics final : public AverageStatistics
{
public:
    ConcretePlayerStatistics(ILog& log, const std::string& name) 
        : AverageStatistics(log)
        , m_Name(name)
    {
    }

    void Merge()
    {
        // get average statistics
        const Ptr average = Cache::Instance().GetAverageStatistics();
        const AverageStatistics& avg = static_cast<const AverageStatistics&>(*average);

        // for each street
        for (const auto street : pcmn::Table::Phases())
        {
            const auto& otherData = avg.GetDBData()[street];
            auto& thisData = this->GetDBData()[street];

            // detect data size difference
            //const std::size_t averageSize = otherData.size();
            const std::size_t thisSize = thisData.size();

            if (!thisSize)
            {
                // copy average, because player doesn't have stats
                thisData = otherData;
            }

            // ���������� ��������� �������������,
            // ������� ���������� ��������� �������� ��� �������� ������ ���������������
            // ������� ����������, ����� �������� ���������� ����� �����������
            // ������ �����: ��� ������ �� ������ �� ������ ��� ������� ��� ������ ��������� ������� ����������
            // �� �� ����� ��������� ���� ������, �.�. ����� ���� �������� ��������� ��������� ��������
//             const float ratio = static_cast<float>(averageSize * 2) / (thisSize);
// 
//             // calculate player counters with ratio
//             for (auto& game : thisData)
//             {
//                 for (auto& pair : game.m_Hands)
//                     pair.second = static_cast<unsigned>(ratio * pair.second);
//             }
// 
//             // merge data
//             for (const auto& game : otherData)
//             {
//                 const auto it = thisData.insert(game).first;
//                 auto& hands = it->m_Hands;
// 
//                 for (const auto& pair : game.m_Hands)
//                     hands[pair.first] += pair.second;
//             }
        }
    }

    virtual void Fetch() override
    {
        FetchData();
        //Merge(); // do not merge, all needed data will be extracted at runtime
        ProcessHands();
        ProcessReactions();
    }

    virtual std::auto_ptr<mongo::DBClientCursor> MakeCursor() override
    {
        mongo::Query query = BSON("players" << BSON("$elemMatch" << BSON("name" << m_Name)));

        //query.sort("_id", 0);    

        // fetch 
        return ConnectionPool::Instance().Get().query
        (
            cfg::STAT_COLLECTION_NAME, 
            query
        );
    }

    virtual bool IsPlayerDataNeeded(const bson::bo& player) override
    {
        return player["name"].String() == m_Name;
    }

    virtual const ReactionMap& GetReaction(pcmn::Table::Phase::Value street, pcmn::Board::Value board, pcmn::Player::Position::Value pos, pcmn::Player::Count::Value count) const override
    {
        const auto& maps = GetReactions();
        if (maps.size() <= street)
            return GetFromAverage(street, board, pos, count);

        const auto& data = maps[street];
        const auto countIt = data.find(count);
        if (countIt == data.end())
            return GetFromAverage(street, board, pos, count);

        const auto position = countIt->second.find(pos);
        if (position == countIt->second.end())
            return GetFromAverage(street, board, pos, count);

        const auto boardData = position->second.find(board);
        if (boardData == position->second.end())
            return GetFromAverage(street, board, pos, count);

        if (street != pcmn::Table::Phase::Preflop && boardData->second.size() < 3)
            return GetFromAverage(street, board, pos, count);

        for (const auto& pair : boardData->second)
        {
            if (pair.second.size() < 2)
                return GetFromAverage(street, board, pos, count);
        }

        return boardData->second;
    }

    const ReactionMap& GetFromAverage(pcmn::Table::Phase::Value street, pcmn::Board::Value board, pcmn::Player::Position::Value pos, pcmn::Player::Count::Value count) const
    {
        const Ptr average = Cache::Instance().GetAverageStatistics();

        const auto& counts = GetAverageReactions()[street];
        const auto it = counts.find(count);
        if (it != counts.end())
        {
            const auto itPosition = it->second.find(pos);
            if (itPosition != it->second.end())
            {
                if (street != pcmn::Table::Phase::Preflop && it->second.size() < 3)
                    return average->GetReaction(street, board, pos, count);

                for (const auto& pair : it->second)
                {
                    if (pair.second.size() < 2)
                        return average->GetReaction(street, board, pos, count);
                }

                return itPosition->second;
            }
        }

        return average->GetReaction(street, board, pos, count);
    }

private:
    const std::string m_Name;
};


IPlayer::Ptr IPlayer::Instance(ILog& log, const std::string& name)
{
    return boost::make_shared<ConcretePlayerStatistics>(log, name);
}

} // namespace stats