#include "exception/CheckHelpers.h"
#include "../serverlib/Server.h"

#include <iostream>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char* argv[])
{
	try 
	{
        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("ep", po::value<std::string>()->default_value("poker", "poker service endpoint"), "server endpoint name");

        po::positional_options_description p;

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help")) 
        {
            std::cout << desc << std::endl;
            return 1;
        }

        CHECK(vm.count("ep"), cmn::Exception("Server endpoint must be specified"));

		srv::Server srv(vm["ep"].as<std::string>());
		srv.Run();
	}
	catch (const std::exception& e)
	{
		std::wcout << cmn::ExceptionInfo(e) << std::endl;
	}

	return 0;
}

