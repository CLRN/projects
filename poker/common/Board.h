#ifndef Board_h__
#define Board_h__

#include "Cards.h"
#include "Hand.h"

namespace pcmn
{

//! Table board with cards implementation
class Board
{
public:

    //! Board description
    enum Value
    {
        Unknown         = 0,
        Low             = 1 << 0,   //!< board contains only low cards
        High            = 1 << 1,   //!< board contains more than half high cards
        Ace             = 1 << 2,   //!< board contains ace
        StraightDraw    = 1 << 3,   //!< straight without two cards
        Straight        = 1 << 4,   //!< straight without one card or full straight
        FlushDraw       = 1 << 5,   //!< flush without two cards
        Flush           = 1 << 6,   //!< flush without one card or full Flush
        Pair            = 1 << 7    //!< board contains pair
    };

    //! Possible hand description
    typedef std::vector<Card::List> HandList;

public:

    Board(const Card::List& cards = Card::List());

    //! Get all possible cards combinations for concrete hand
    HandList GetCardsByHand(const Hand::Value hand);

    //! Get all possible hand cards
    const HandList& GetAllPocketCards() const;

    //! Parse board
    Board& Parse();

    //! Get board value
    Value GetValue() const { return m_Value; }

    //! To string
    static std::string ToString(const Value v);

    //! Get all possible board cards
    const HandList& GetRemainingCards() const;

    //! Get all possible boards
    HandList GetAllBoards(Value desc);

private:

    //! Generate possible hands
    void GeneratePossibleHands(HandList& result, const Hand::Value hand) const;

    //! Filter possible hands by board cards and hand description
    HandList FilterCards(const HandList& src, const Hand::Value hand);

private:

    //! Cards on the board
    Card::List m_Board;

    //! Board value
    Value m_Value;
};

//! Stream operator
std::ostream& operator << (std::ostream& s, pcmn::Board::Value b);

}

#endif // Board_h__
