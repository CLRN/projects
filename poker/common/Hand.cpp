#include "Hand.h"
#include "exception/CheckHelpers.h"
#include "CardsErrorInfo.h"

#include <math.h>
#include <algorithm>
#include <map>

#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>

namespace pcmn
{
    
static const Suit::List g_Suites = boost::assign::list_of(Suit::Hearts)(Suit::Clubs)(Suit::Spades)(Suit::Diamonds);

Hand::Value DiffFromTopToStrength(std::size_t diff, bool suited)
{
    if (suited)
    {
        if (!diff)
            return Hand::Top;
        else
        if (diff < 5)
            return Hand::Middle;
        else
            return Hand::Low;
    }
    else
    {
        switch (diff)
        {
        case 0: return Hand::Top;
        case 1: return Hand::Middle;
        default: return Hand::Low;
        }
    }
}

Hand::Value KickerFromCard(Card::Value hand, Card::Value kicker)
{
    if (kicker == Card::Ace || hand == Card::Ace && kicker == Card::King)
        return Hand::TopKicker;
    else
    if (kicker >= Card::Jack)
        return Hand::GoodKicker;
    else
        return Hand::LowKicker;
}

bool IsConnectors(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value != second.m_Value && first.m_Value != Card::Ace && second.m_Value != Card::Ace && abs(second.m_Value - first.m_Value) <= 2;
}

bool IsSuited(const Card::List& cards)
{
    return cards.front().m_Suit == cards.back().m_Suit;
}

bool IsOneLow(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value >= Card::Six && second.m_Value < Card::Six || second.m_Value >= Card::Six && first.m_Value < Card::Six;
}

bool IsBothLow(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value <= Card::Five && second.m_Value <= Card::Five;
}

bool IsOneHigh(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value >= Card::Jack && second.m_Value < Card::Jack || second.m_Value >= Card::Jack && first.m_Value < Card::Jack;
}

bool IsBothHigh(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value >= Card::Jack && second.m_Value >= Card::Jack;
}

bool IsAce(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value == Card::Ace || second.m_Value == Card::Ace;
}

bool IsPocketPair(const Card::List& cards)
{
    const Card& first = cards.front();
    const Card& second = cards.back();
    return first.m_Value == second.m_Value;
}

void ExtractOrdered(const Card::List& input, std::vector<Card::List>& result)
{
    Card::List cards;
    cards.reserve(input.size());
    result.reserve(input.size());

    std::unique_copy(input.begin(), input.end(), std::back_inserter(cards), [](const Card& lhs, const Card& rhs) { return lhs.m_Value == rhs.m_Value; });

    result.resize(result.size() + 1);
    result.back().reserve(input.size());

    const bool aceExists = cards.back().m_Value == Card::Ace;
    unsigned cardsBetween = 0;

    Card* current;
    Card* next;
    for (unsigned i = 0 ; i < (aceExists ? cards.size() : cards.size() - 1); ++i)
    {
        const bool compareWithAce = (!i && aceExists);

        current = aceExists ? (i ? &cards[i - 1] : &cards.back()) : &cards[i];
        next = aceExists ? (i ? &cards[i] : &cards.front()) : &cards[i + 1];

        if (compareWithAce && next->m_Value == Card::Two || abs(current->m_Value - next->m_Value) == 1)
        {
            if (result.back().empty())
                result.back().push_back(*current);
            result.back().push_back(*next);
            cardsBetween = 0;
        }
        else
        {
            if (!cardsBetween)
            {
                result.resize(result.size() + 1);
                result.back().reserve(input.size());
                result.back().push_back(*next);
            }
            else
                result.back().clear();

            ++cardsBetween;
        }
    }
}

bool IsStraight(const Card::List& cards, Hand::Value& strength, const Card::List& player, bool& draw, const std::vector<Card::List>& sequences)
{
    draw = false;
    for (unsigned i = 0 ; i < sequences.size(); ++i)
    {
        const Card::List& sequence = sequences[i];
        if (sequence.size() < 4)
            continue;

        if (sequence.size() == 4)
        {
            if (sequence.front().m_Value != Card::Ace && sequence.back().m_Value != Card::Ace)
                draw = true;
            else
                continue;
        }

        const Card::List::const_iterator itFirst = std::find(sequence.begin(), sequence.end(), player.front());
        const Card::List::const_iterator itSecond = std::find(sequence.begin(), sequence.end(), player.back());
        if (itFirst == sequence.end() && itSecond == sequence.end())
            continue; // draw on the board
            
        const Card::List::const_iterator highCardInSequence = sequence.end() - 1;
        const Card::List::const_iterator lowCardInSequence = sequence.begin();

        if (highCardInSequence == itFirst || highCardInSequence == itSecond)
            strength = draw ? Hand::TopDraw : Hand::Top;
        else
        if (lowCardInSequence == itFirst || lowCardInSequence == itSecond)
            strength = draw ? Hand::Unknown : Hand::Low;
        else
        if (!draw)
            strength = Hand::Middle;
            
        return true;      
    }

    return false;
}

bool IsGutShot(const Card::List& cards, const Card::List& player, const std::vector<Card::List>& sequences)
{
    for (unsigned i = 0 ; i < sequences.size() - 1; ++i)
    {
        const Card::List& current = sequences[i];
        const Card::List& next = sequences[i + 1];

        if 
        (
            std::find(current.begin(), current.end(), player.front()) == current.end() 
            && 
            std::find(current.begin(), current.end(), player.back()) == current.end()
            &&
            std::find(next.begin(), next.end(), player.front()) == next.end() 
            && 
            std::find(next.begin(), next.end(), player.back()) == next.end()
         )
            continue; // god shot on board

        if (current.size() + next.size() >= 4 && !current.empty() && !next.empty() && next.front().m_Value - current.back().m_Value == 2)
            return true; // god shot in the middle

        if (current.size() == 4 && (current.back().m_Value == Card::Ace || current.front().m_Value == Card::Ace))
            return true; // god shot from the beginning or the end

        if (next.size() == 4 && (next.back().m_Value == Card::Ace || next.front().m_Value == Card::Ace))
            return true; // god shot from the beginning or the end
    }

    return false;
}

void CountSuites(const Card::List& cards, unsigned* counters, Card* highCards)
{
    for (const Card& c : cards)
    {
        ++counters[c.m_Suit];
        if (highCards[c.m_Suit].m_Value < c.m_Value)
            highCards[c.m_Suit] = c;
    }
}

void CountCards(const Card::List& cards, unsigned* counters)
{
    for (const Card& c : cards)
        ++counters[c.m_Value];
}

bool IsFlush(const Card::List& cards, const Card::List& player, Hand::Value& strength, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    for (const Suit::Value suit : g_Suites)
    {
        if (counters[suit] < 5)
            continue;

        // get all cards of this suit
        Card::List suited;
        suited.reserve(7);
        std::copy_if(cards.begin(), cards.end(), std::back_inserter(suited), [suit](const Card& c) { return c.m_Suit == suit; });

        // sort by card rank
        std::sort(suited.begin(), suited.end(), [](const Card& lhs, const Card& rhs) { return lhs.m_Value > rhs.m_Value; });

        // remove low elements
        suited.resize(5);

        // find out player cards
        const Card::List::const_iterator firstCard = std::find(suited.begin(), suited.end(), first);
        const Card::List::const_iterator secondCard = std::find(suited.begin(), suited.end(), second);

        if (firstCard == suited.end() && secondCard == suited.end())
            continue; // flush on the board

        Card::Value best = Card::Unknown;
        if (firstCard != suited.end() && secondCard != suited.end())
            best = std::max(firstCard->m_Value, secondCard->m_Value);
        else
        if (firstCard != suited.end())
            best = firstCard->m_Value;
        else
            best = secondCard->m_Value;

        // flush strength
        const std::size_t diff = Card::Ace - best;
        strength = DiffFromTopToStrength(diff, true);

        return true;
    }
    return false;
}

bool IsFlushDraw(const Card::List& cards, const Card::List& player, Hand::Value& strength, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    for (const Suit::Value suit : g_Suites)
    {
        if (counters[suit] != 4)
            continue;

        // get all cards of this suit
        Card::List suited;
        suited.reserve(7);
        std::copy_if(cards.begin(), cards.end(), std::back_inserter(suited), [suit](const Card& c) { return c.m_Suit == suit; });

        // sort by card rank
        std::sort(suited.begin(), suited.end(), [](const Card& lhs, const Card& rhs) { return lhs.m_Value > rhs.m_Value; });

        // remove low elements
        suited.resize(4);

        // find out player cards
        const Card::List::const_iterator firstCard = std::find(suited.begin(), suited.end(), first);
        const Card::List::const_iterator secondCard = std::find(suited.begin(), suited.end(), second);

        if (firstCard == suited.end() && secondCard == suited.end())
            continue; // flush on the board

        // straight strength
        Card::Value best = Card::Unknown;
        if (firstCard != suited.end() && secondCard != suited.end())
            best = std::max(firstCard->m_Value, secondCard->m_Value);
        else
        if (firstCard != suited.end())
            best = firstCard->m_Value;
        else
            best = secondCard->m_Value;

        // flush strength
        const std::size_t diff = Card::Ace - best;
        strength = DiffFromTopToStrength(diff, true);
        switch (strength)
        {
        case Hand::Top: strength = Hand::TopDraw; break;
        case Hand::Middle: strength = Hand::GoodDraw; break;
        default: strength = Hand::Unknown; break;
        }

        return true;
    }
    return false;
}

bool IsStraightFlush(const Card::List& cards, const Card::List& player, const std::vector<Card::List>& sequences)
{
    const Card& first = player.front();
    const Card& second = player.back();

    for (unsigned i = 0 ; i < sequences.size(); ++i)
    {
        if (sequences[i].size() < 5)
            continue;

        const Card::List::const_iterator itFirst = std::find(sequences[i].begin(), sequences[i].end(), player.front());
        const Card::List::const_iterator itSecond = std::find(sequences[i].begin(), sequences[i].end(), player.back());
        if (itFirst == sequences[i].end() && itSecond == sequences[i].end())
            continue; // player hand doesn't play

        Card::List sequence = sequences[i];

        // copy player cards to sequence
        if (std::find_if(sequence.begin(), sequence.end(), [&](const Card& c) { return c.m_Value == first.m_Value; }) != sequence.end())
            sequence.push_back(first);

        if (std::find_if(sequence.begin(), sequence.end(), [&](const Card& c) { return c.m_Value == second.m_Value; }) != sequence.end())
            sequence.push_back(second);

        std::sort(sequence.begin(), sequence.end(), [](const Card& lhs, const Card& rhs) { return lhs.m_Value < rhs.m_Value; });

        // erase duplicates
        sequence.erase(std::unique(sequence.begin(), sequence.end(), [](const Card& lhs, const Card& rhs) { return lhs == rhs; }), sequence.end());

        unsigned counters[Suit::MaxValue + 1] = {};
        Card highCards[Suit::MaxValue + 1] = {};
        CountSuites(sequence, counters, highCards);

        for (const Suit::Value suit : g_Suites)
        {
            if (counters[suit] >= 5)
                return true;
        }
    }

    return false;
}

bool IsXOfKind(const Card::List& cards, const Card::List& player, unsigned count, Hand::Value& value, Hand::Value& kicker, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    for (Card::Value card = Card::MaxValue; card >= Card::Two; card = static_cast<Card::Value>(card - 1))
    {
        if (counters[card] != count || (player.front().m_Value != card && player.back().m_Value != card))
            continue;

        Card::List board;
        std::copy_if(cards.begin(), cards.end(), std::back_inserter(board), [&](const Card& c){ return !(c == first || c == second); });

        // detect strength
        const Card::List::const_reverse_iterator it = std::find_if(board.rbegin(), board.rend(), [card](const Card& c) { return c.m_Value == card; });
        assert(it != board.rend());

        // difference from top card
        const std::size_t diff = std::distance<Card::List::const_reverse_iterator>(board.rbegin(), it);
        value = DiffFromTopToStrength(diff, false);

        // kicker
        Card::Value otherCard = Card::Unknown;
        if (first.m_Value != card)
            otherCard = first.m_Value;
        else
        if (second.m_Value != card)
            otherCard = second.m_Value;
        else
            return true; // this is pocket pair, no kicker

        kicker = KickerFromCard(card, otherCard);
        return true;
    }

    return false;
}

bool IsFullHouse(const Card::List& cards, const Card::List& player, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    Card::Value pair = Card::Unknown;
    Card::Value trips = Card::Unknown;

    for (Card::Value card = Card::MaxValue; card >= Card::Two; card = static_cast<Card::Value>(card - 1))
    {
        if (pair == Card::Unknown && counters[card] == 2)
        {
            pair = card;
            if (trips != Card::Unknown)
                break;
        }

        if (trips == Card::Unknown && counters[card] == 3)
        {
            trips = card;
            if (pair != Card::Unknown)
                break;
        }
    }

    return pair != Card::Unknown && trips != Card::Unknown && 
        (first.m_Value == pair || first.m_Value == trips || second.m_Value == pair || second.m_Value == trips);
}

bool IsTwoPairs(const Card::List& cards, const Card::List& player, Hand::Value& value, Hand::Value& kicker, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    Card::Value pair1 = Card::Unknown;
    Card::Value pair2 = Card::Unknown;

    for (Card::Value card = Card::MaxValue; card >= Card::Two; card = static_cast<Card::Value>(card - 1))
    {
        if (pair1 == Card::Unknown && counters[card] == 2)
        {
            pair1 = card;
        }
        else
        if (pair2 == Card::Unknown && counters[card] == 2)
        {
            pair2 = card;
            break;
        }
    }

    const bool isTwoPairs = pair1 != Card::Unknown && pair2 != Card::Unknown && 
        (first.m_Value == pair1 || first.m_Value == pair2 || second.m_Value == pair1 || second.m_Value == pair2);

    if (isTwoPairs)
    {
        // detect strength of the best pair
        std::vector<Card::Value> playerCards;
        if (pair1 == first.m_Value || pair1 == second.m_Value)
            playerCards.push_back(pair1);

        if (pair2 == first.m_Value || pair2 == second.m_Value)
            playerCards.push_back(pair2);

        assert(!playerCards.empty());
        std::sort(playerCards.begin(), playerCards.end());

        const Card::Value bestpair = playerCards.back();

        Card::List unique;
        std::unique_copy(cards.begin(), cards.end(), std::back_inserter(unique), [](const Card& lhs, const Card& rhs) { return lhs.m_Value == rhs.m_Value; });

        const Card::List::const_reverse_iterator it = std::find_if(unique.rbegin(), unique.rend(), [&](const Card& c) { return c.m_Value == bestpair; });
        assert(it != unique.rend());

        // difference from top card
        const std::size_t diff = std::distance<Card::List::const_reverse_iterator>(unique.rbegin(), it);
        value = DiffFromTopToStrength(diff, false);

        // kicker
        Card kickerCard;
        if (first.m_Value != second.m_Value)
        {
            if ((first.m_Value == pair1 || first.m_Value == pair2) && second.m_Value != pair1 && second.m_Value != pair2)
                kickerCard = second;
            else
            if ((second.m_Value == pair1 || second.m_Value == pair2) && first.m_Value != pair1 && first.m_Value != pair2)
                kickerCard = first;
        }

        kicker = KickerFromCard(pair1, kickerCard.m_Value);
    }

    return isTwoPairs;
}

bool IsPair(const Card::List& cards, const Card::List& player, Hand::Value& result, Hand::Value& kicker, const unsigned* counters)
{
    const Card& first = player.front();
    const Card& second = player.back();

    for (Card::Value card = Card::MaxValue; card >= Card::Two; card = static_cast<Card::Value>(card - 1))
    {
        if (counters[card] == 2 && (card == first.m_Value || card == second.m_Value))
        {
            Card::List board;
            if (card != first.m_Value || card != second.m_Value)
            {
                // this is not a pocket pair, it should be compared with board only
                std::copy_if(cards.begin(), cards.end(), std::back_inserter(board), [&](const Card& c) { return !(c == first || c == second); });
            }
            else
            {
                std::unique_copy(cards.begin(), cards.end(), std::back_inserter(board), [](const Card& lhs, const Card& rhs) { return lhs.m_Value == rhs.m_Value; });
            }

            // found pair, calculate pair value
            const Card::List::const_reverse_iterator it = std::find_if(board.rbegin(), board.rend(), [card](const Card& c) { return c.m_Value == card; });
            assert(it != board.rend());

            // difference from top card
            const std::size_t diff = std::distance<Card::List::const_reverse_iterator>(board.rbegin(), it);
            result = DiffFromTopToStrength(diff, false);

            // find out kicker
            Card::Value kickerCard = Card::Unknown;
            if (first.m_Value != card)
                kickerCard = first.m_Value;
            else
            if (second.m_Value != card)
                kickerCard = second.m_Value;

            kicker = KickerFromCard(card, kickerCard);
            return true;
        }
    }

    return false;
}

Hand::Hand() : m_Value(Unknown)
{

}

Hand::Hand(Value v) : m_Value(v)
{

}

Hand& Hand::Parse(const Card::List& player, const Card::List& boardInput)
{
    CHECK(player.size() == 2, cmn::Exception("Invalid hand! Must be two cards") << CardsErrorInfo(player));
    
    m_Value = Unknown;

    // parse player hand first
    if (IsOneLow(player))
        Add(OneLow);

    if (IsBothLow(player))
        Add(BothLow);

    if (IsSuited(player))
        Add(Suited);

    if (IsConnectors(player))
        Add(Connectors);
    
    if (IsOneHigh(player))
        Add(OneHigh);

    if (IsBothHigh(player))
        Add(BothHigh);

    if (IsAce(player))
        Add(Ace);

    if (IsPocketPair(player))
        Add(PoketPair);

    if (boardInput.empty())
        return *this;

    CHECK(boardInput.size() >= 3 && boardInput.size() <= 5, cmn::Exception("Invalid board must be 3, 4 or 5 cards") << CardsErrorInfo(boardInput));

    // now parse hands with board
    Card::List hand;
    hand.reserve(player.size() + boardInput.size());
    std::copy(player.begin(), player.end(), std::back_inserter(hand));
    std::copy(boardInput.begin(), boardInput.end(), std::back_inserter(hand));

    // sort by value
    std::sort(hand.begin(), hand.end(), [](const Card& lhs, const Card& rhs) { return lhs.m_Value < rhs.m_Value; });

    hand.erase(std::unique(hand.begin(), hand.end(), [](const Card& lhs, const Card& rhs) { return lhs == rhs; }), hand.end());
    CHECK(hand.size() == boardInput.size() + player.size(), cmn::Exception("Cards are duplicated"), player, boardInput);

    // check hand
    Value kicker = Unknown;
    Value strength = Unknown;
    Value straightStrength = Unknown;

    std::vector<Card::List> sequences;
    unsigned suitCounters[Suit::MaxValue + 1] = {};
    Card highCards[Suit::MaxValue + 1] = {};
    unsigned cardCounters[Card::MaxValue + 1] = {};

    ExtractOrdered(hand, sequences);
    CountSuites(hand, suitCounters, highCards);
    CountCards(hand, cardCounters);

    if (IsStraightFlush(hand, player, sequences))
    {
        Add(StraightFlush);
        return *this;
    }
    if (IsXOfKind(hand, player, 4, strength, kicker, cardCounters))
    {
        Add(FourOfKind);
        return *this;
    }
    if (IsFullHouse(hand, player, cardCounters))
    {
        Add(FullHouse);
        return *this;
    }

    bool straightDraw = false;
    const bool isFlush = IsFlush(hand, player, strength, suitCounters);
    const bool isStraight = IsStraight(hand, straightStrength, player, straightDraw, sequences);

    if (isFlush)
    {
        Add(Flush);
        Add(strength);
        return *this;
    }

    if (isStraight && !straightDraw)
    {
        Add(Straight);
        Add(straightStrength);
        return *this;
    }
    if (IsGutShot(hand, player, sequences))
    {
        Add(GutShot);
    }

    if (IsXOfKind(hand, player, 3, strength, kicker, cardCounters))
    {
        Add(ThreeOfKind);
        Add(strength);
        Add(kicker);
    }
    else
    if (IsTwoPairs(hand, player, strength, kicker, cardCounters))
    {
        Add(TwoPairs);
        Add(strength);
        Add(kicker);
    }
    else
    if (IsPair(hand, player, strength, kicker, cardCounters))
    {
        Add(Pair);
        Add(strength);
        Add(kicker);
    }
    else
    {
        const Card::Value maxPlayerCard = std::max(player.front().m_Value, player.back().m_Value);
        const Card::List::const_iterator maxCard = std::max_element(boardInput.begin(), boardInput.end(), [](const Card& lhs, const Card& rhs) { return lhs.m_Value < rhs.m_Value; });
        assert(maxCard != boardInput.end());

        if (maxPlayerCard > maxCard->m_Value)
            Add(HighCard);
    }

    if (!isFlush && IsFlushDraw(hand, player, strength, suitCounters))
    {
        Add(FlushDraw);
        Add(strength);
    }

    if (!isFlush && isStraight && straightDraw)
    {
        Add(StraightDraw);
        Add(straightStrength);
    }
    return *this;
}

void Hand::Add(Value prop)
{
    m_Value = static_cast<Hand::Value>(int(m_Value) | prop);
}

void Hand::AddKicker(const Card::Value card)
{
    if (card == Card::Ace)
        Add(TopKicker);
    else
    if (card >= Card::Jack)
        Add(GoodKicker);
    else
        Add(LowKicker);
}

std::string Hand::ToString(Value v)
{
    std::string result;

    if (v == Unknown)
        result = "Unknown";

#define CASE(x) if (v & x) if (result.empty()) result += #x; else result += " | ", result += #x;
    CASE(OneLow);
    CASE(BothLow);
    CASE(Suited);
    CASE(Connectors);
    CASE(OneHigh);
    CASE(BothHigh);
    CASE(Ace);
    CASE(PoketPair);
    CASE(StraightDraw);
    CASE(GutShot);
    CASE(FlushDraw);
    CASE(GoodDraw);
    CASE(TopDraw);
    CASE(HighCard);
    CASE(Pair);
    CASE(TwoPairs);
    CASE(ThreeOfKind);
    CASE(Straight);
    CASE(Flush);
    CASE(FullHouse);
    CASE(FourOfKind);
    CASE(StraightFlush);
    CASE(Low);
    CASE(Middle);
    CASE(Top);
    CASE(TopKicker);
    CASE(GoodKicker);
    CASE(LowKicker);
#undef CASE
    assert(!result.empty());
    return result;
}

Hand::Value Hand::FromEval(int rank)
{
    if (rank < 1278) // high card 1277 - 49
        return Hand::HighCard;

    if (rank < 4138) // pair 4137 - 1296
        return Hand::Pair;

    if (rank < 4996) // two pairs 5855 - 4996
        return Hand::TwoPairs;

    if (rank < 5854) // set 5853 - 5004
        return Hand::ThreeOfKind;

    if (rank < 5864) // straight 5863 - 5857
        return Hand::Straight;

    if (rank < 7141) // flush 7140 - 5865
        return Hand::Flush;

    if (rank < 7297) // full 7296 - 7141
        return Hand::FullHouse;

    if (rank < 7453) // quad 7452 - 7299
        return Hand::FourOfKind;

    if (rank == 7462) // royal 7462
        return Hand::StraightFlush;

    assert(false);
    return Hand::Unknown;
}

bool Hand::operator < (const Hand::Value other) const
{
    const int l = m_Value & Hand::FLOP_HAND_MASK;
    const int r = other & Hand::FLOP_HAND_MASK;
    if (l == r)
        return (m_Value & Hand::POWER_MASK) < (other & Hand::POWER_MASK);
    return l < r;
}

bool Hand::operator>(const Hand::Value other) const
{
    const int l = m_Value & Hand::FLOP_HAND_MASK;
    const int r = other & Hand::FLOP_HAND_MASK;
    if (l == r)
        return (m_Value & Hand::POWER_MASK) > (other & Hand::POWER_MASK);
    return l > r;
}

bool Hand::operator == (const Hand::Value other) const
{
    const int l = m_Value & Hand::FLOP_HAND_MASK;
    const int r = other & Hand::FLOP_HAND_MASK;
    if (l == r)
        return (m_Value & Hand::POWER_MASK) == (other & Hand::POWER_MASK);
    return false;
}

const unsigned Hand::POWER_MASK = Hand::Low | Hand::Middle | Hand::Top;
const unsigned Hand::DRAWS_MASK = Hand::StraightDraw | Hand::GutShot | Hand::FlushDraw | Hand::GoodDraw | Hand::TopDraw;
const unsigned Hand::KICKERS_MASK = Hand::LowKicker | Hand::GoodKicker | Hand::TopKicker;
const unsigned Hand::FLOP_HAND_MASK = Hand::HighCard | Hand::Pair | Hand::TwoPairs | Hand::ThreeOfKind | Hand::Straight | Hand::Flush | Hand::FullHouse | Hand::FourOfKind | Hand::StraightFlush;
const unsigned Hand::POCKET_HAND_MASK = Hand::OneLow | Hand::BothLow | Hand::Suited | Hand::Connectors | Hand::OneHigh | Hand::BothHigh | Hand::Ace | Hand::PoketPair;

std::ostream& operator<<(std::ostream& s, const Hand::Value h)
{
    s << Hand::ToString(h);
    return s;
}

}