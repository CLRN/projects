#include "Actions.h"

#include <boost/algorithm/string.hpp>

namespace pcmn
{
	std::string Action::ToString(const Action::Value value)
	{
#define CASE(x) case x: return #x;
		switch (value)
		{
			CASE(Fold)
			CASE(Check)
			CASE(Call)
			CASE(Bet)
			CASE(Raise)
			CASE(ShowCards)
			CASE(SmallBlind)
			CASE(BigBlind)
			CASE(Ante)
			CASE(Win)
			CASE(Loose)
			CASE(MoneyReturn)
			CASE(SecondsLeft)
			CASE(Rank)
			CASE(WinCards)
            CASE(Afk)
		default: return "Unknown";
		}
#undef CASE
	}

	bool Action::IsUseful(Value value)
	{
		switch (value)
		{
            case Fold:
            case Check:
			case Call:
			case Bet:
			case Raise:
			case SmallBlind:
			case BigBlind:
				return true;
		}
		return false;
	}

	Action::Value Action::FromString(const std::string& text)
	{
#define CASE(x) if (boost::algorithm::iequals(text, #x)) return x;
		{
			CASE(Fold)
			CASE(Check)
			CASE(Call)
			CASE(Bet)
			CASE(Raise)
			CASE(ShowCards)
			CASE(SmallBlind)
			CASE(BigBlind)
			CASE(Ante)
			CASE(Win)
			CASE(Loose)
			CASE(MoneyReturn)
			CASE(SecondsLeft)
			CASE(Rank)
			CASE(WinCards)
            if (boost::algorithm::iequals(text, "PostBB"))
                return BigBlind;
            if (boost::algorithm::iequals(text, "PostSB"))
                return SmallBlind;
            if (boost::algorithm::iequals(text, "SittingOut"))
                return Afk;
            if (boost::algorithm::iequals(text, "Disconnected"))
                return Afk;
            if (boost::algorithm::iequals(text, "Muck"))
                return Loose;
        }
#undef CASE
		return Unknown;
	}

    bool Action::IsActive(Value value)
    {
        switch (value)
        {
        case Fold:
        case Check:
        case Call:
        case Bet:
        case Raise:
            return true;
        }
        return false;
    }

    std::ostream& operator<<(std::ostream& s, const pcmn::Action::Value a)
    {
        s << pcmn::Action::ToString(a);
        return s;
    }

}
