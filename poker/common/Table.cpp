#include "Table.h"

#include <sstream>

#include <boost/assign/list_of.hpp>

namespace pcmn
{


const Table::Phase::List& Table::Phases()
{
    static const Phase::List list = boost::assign::list_of(Phase::Preflop)(Phase::Flop)(Phase::Turn)(Phase::River);
    return list;
}

unsigned Table::Phase::ToCardsCount(Phase::Value v)
{
    switch (v)
    {
    case pcmn::Table::Phase::Preflop: return 0;
    case pcmn::Table::Phase::Flop: return 3;
    case pcmn::Table::Phase::Turn: return 4;
    case pcmn::Table::Phase::River: return 5;
    default: assert(false); return 0;
    }
}

Table::Phase::Value Table::Phase::FromCardsCount(unsigned c)
{
    switch (c)
    {
    case 0: return pcmn::Table::Phase::Preflop;
    case 3: return pcmn::Table::Phase::Flop;
    case 4: return pcmn::Table::Phase::Turn;
    case 5: return pcmn::Table::Phase::River;
    default: assert(false); return pcmn::Table::Phase::Preflop;
    }
}

std::string Table::Phase::ToString(Phase::Value v)
{
    std::stringstream ss;
    ss << v;
    return ss.str();
}

std::ostream& operator<<(std::ostream& s, Table::Phase::Value v)
{
    switch (v)
    {
    case pcmn::Table::Phase::Preflop:
        s << "Preflop";
        break;
    case pcmn::Table::Phase::Flop:
        s << "Flop";
        break;
    case pcmn::Table::Phase::Turn:
        s << "Turn";
        break;
    case pcmn::Table::Phase::River:
        s << "River";
        break;
    default:
        assert(false);
    }
    return s;
}

} // namespace pcmn