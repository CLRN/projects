#include "Evaluator.h"
#include "exception/CheckHelpers.h"
#include "../evaluator/SevenEval.h"
#include "../evaluator/HandEval.h"
#include "Board.h"
#include "common/ServiceHolder.h"
#include "CardsErrorInfo.h"

#include <cassert>
#include <algorithm>

#include <boost/unordered_map.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/scope_exit.hpp>
#include <boost/thread/future.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/optional.hpp>

namespace pcmn
{

Evaluator::Evaluator(unsigned repititions) 
    : m_RanksEvaluator(new SevenEval())
    , m_HandsEvaluator(new HandEval())
    , m_Repititions(repititions)
{
	srand((unsigned int) time(0));
}

Evaluator::~Evaluator()
{
	delete m_RanksEvaluator;
	delete m_HandsEvaluator;
}

short Evaluator::GetRank(const Card::List& cards) const
{
	CHECK(cards.size() == 7, cmn::Exception("Invalid cards size") << pcmn::CardsErrorInfo(cards));

	int evals[7];
	for (std::size_t i = 0 ; i < cards.size(); ++i)
		evals[i] = cards[i].ToEvalFormat();

	return m_RanksEvaluator->getRankOfSeven(evals[0], evals[1], evals[2], evals[3], evals[4], evals[5], evals[6]);
}

short Evaluator::GetRandomCard(bool* dead, const short minValue) const
{
	for (;;)
	{
		const short card = rand() % minValue;
		if (!dead[card])
		{
			dead[card] = true;
			return card;
		}
	}
}

boost::optional<std::pair<short, short>> GetRandomCards(bool* dead, const Board::HandList& hands)
{
    for (unsigned tryCount = 0; tryCount < hands.size() * 1.5; ++tryCount)
    {
        const short index = rand() % hands.size();
        const Card::List& cards = hands[index];

        assert(cards.size() == 2);

        const short first = cards.front().ToEvalFormat();
        const short second = cards.back().ToEvalFormat();

        if (!dead[first] && !dead[second])
        {
            dead[first] = true;
            dead[second] = true;
            return std::make_pair(first, second);
        }
    }

    // failed to get random cards, because all cards from 'hands' marked as dead
    return boost::optional<std::pair<short, short>>();
}

template<typename T>
Evaluator::RunResult Evaluator::RunTask(const T& functor) const
{
    RunResult result(0);

    // calculate how much threads we can run
    const std::size_t threads = boost::thread::hardware_concurrency();

    // iterations per thread
    const std::size_t iterationsPerThread = m_Repititions / threads;

    std::vector<boost::unique_future<RunResult> > futures;

    typedef boost::packaged_task<RunResult> Task;

    // start tasks for each thread
    for (std::size_t i = 0 ; i < threads; ++i)
    {
        const boost::shared_ptr<Task> task = boost::make_shared<Task>(boost::bind(functor, iterationsPerThread));
        futures.push_back(task->get_future());
        cmn::Service::Instance().post(boost::bind(&Task::operator(), task));
    }

    // ok, all tasks queued, need to wait, but we can't just wait, so run io_service and check if futures ready
    const auto isReady = [](const std::vector<boost::unique_future<RunResult> >& futures) 
    { 
        for (const auto& future : futures)
        {
            if (!future.is_ready())
                return false;
        }
        return true;
    };

    while (!isReady(futures))
        cmn::Service::Instance().poll_one();

    assert(!futures.empty() && isReady(futures));

    for (auto& future : futures)
    {
        const RunResult& runResult = future.get();
        result.m_Total += runResult.m_Total;
        result.m_Loose += runResult.m_Loose;
    }

    return result;
}

float Evaluator::GetEquity(const short player1, const short player2, const std::vector<int>& flop, const std::vector<short>& playerRanges) const
{
    const RunResult result = RunTask(boost::bind(&Evaluator::RunEquityCalcRange, this, player1, player2, boost::ref(flop), boost::ref(playerRanges), _1));
    return result.m_Total ? static_cast<float>(100.0f - (static_cast<double>(result.m_Loose * 100) / result.m_Total)) : 1.0f;
}

float Evaluator::GetEquity(short player1, short player2, const Card::List& flop, const Hand::List& opponentsInput) const
{
    const RunResult runResult = RunTask(boost::bind(&Evaluator::RunEquityCalcHands, this, player1, player2, boost::ref(flop), opponentsInput, _1));
    return runResult.m_Total ? static_cast<float>(100.0f - (static_cast<double>(runResult.m_Loose * 100) / runResult.m_Total)) : 1.0f;
}

float Evaluator::GetEquity(const Card::List& bot, const Card::List& flop, const Hand::List& opponents) const
{
    if (flop.size() == 5)
    {
        const Hand::Value botHand = Hand().Parse(bot, flop).GetValue();

        // all cards are known, make simple comparison
        unsigned equalCnt = 0;
        for (const Hand current : opponents)
        {
            if (current > botHand)
            {
                // there is player with hand much bigger than bot have, so bot has no chances
                return 0.0f;
            }
            else
            if (current == botHand)
                ++equalCnt;
        }
        if (!equalCnt)
        {
            // there are no players with hand much bigger than bot have, so bot wins
            return 100.0f;
        }

        return 50.0f;
    }

    return GetEquity(bot.front().ToEvalFormat(), bot.back().ToEvalFormat(), flop, opponents);
}

float Evaluator::GetEquity(const Card::List& bot, const Card::List& flop, const std::vector<Card::List>& opponents) const
{
    // prepare hands
    const short player1 = bot.front().ToEvalFormat();
    const short player2 = bot.back().ToEvalFormat();

    std::vector<std::vector<short>> opponentCards;
    for (const auto& hand : opponents)
        opponentCards.push_back(boost::assign::list_of(hand.front().ToEvalFormat())(hand.back().ToEvalFormat()));

    // calculate dead cards
    bool deadCards[cfg::CARD_DECK_SIZE] = {};

    deadCards[player1] = true;
    deadCards[player2] = true;

    for (const auto& c : opponentCards)
    {
        deadCards[c.front()] = true;
        deadCards[c.back()] = true;
    }

    for (const auto& c : flop)
        deadCards[c.ToEvalFormat()] = true;

    // board description
    Board board(flop);

    // get all remaining cards
    const Board::HandList& allRemaining = board.GetRemainingCards();

    short flopCards[5] = {};

    for (unsigned i = 0 ; i < flop.size(); ++i)
        flopCards[i] = flop[i].ToEvalFormat();

    if (allRemaining.empty())
    {
        // evaluate
        const short botRank = m_RanksEvaluator->getRankOfSeven(player1, player2, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);

        // evaluate all players
        bool draw = false;
        for (const auto& playerHand : opponentCards)
        {
            const short opponentRank = m_RanksEvaluator->getRankOfSeven(playerHand.front(), playerHand.back(), flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);

            if (opponentRank > botRank)
                return 0.0f;
            else
            if (opponentRank == botRank)
                draw = true;
        }
        return draw ? 50.0f : 100.0f;
    }

    // enumerate all
    RunResult result(0);
    for (const Card::List& currentFlopCards : allRemaining)
    {
        // copy to flop array
        bool skip = false;
        for (unsigned i = flop.size(); i < 5; ++i)
        {
            const short card = currentFlopCards[i - flop.size()].ToEvalFormat();
            if (deadCards[card])
            {
                skip = true;
                break;
            }

            flopCards[i] = card;
        }
        if (skip)
            continue;

        // evaluate
        const short botRank = m_RanksEvaluator->getRankOfSeven(player1, player2, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);

        // evaluate all players
        bool loose = false;
        for (const auto& playerHand : opponentCards)
        {
            const short opponentRank = m_RanksEvaluator->getRankOfSeven(playerHand.front(), playerHand.back(), flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);

            if (opponentRank > botRank)
            {
                loose = true;
                break;
            }
        }

        if (loose)
            ++result.m_Loose;

        ++result.m_Total;
    }

    return result.m_Total ? static_cast<float>(100.0f - (static_cast<double>(result.m_Loose * 100) / result.m_Total)) : 1.0f;
}

const Evaluator& Evaluator::Instance()
{
    const static Evaluator instance;
    return instance;
}

Evaluator::RunResult Evaluator::RunEquityCalcRange(short player1, short player2, const std::vector<int>& flop, const std::vector<short>& playerRanges, unsigned iterations) const
{
    RunResult result(iterations);

    bool deadCards[cfg::CARD_DECK_SIZE] = {false};

    assert(std::find(flop.begin(), flop.end(), player1) == flop.end());
    assert(std::find(flop.begin(), flop.end(), player2) == flop.end());

    deadCards[player1] = true;
    deadCards[player2] = true;

    // get flop cards
    int flopCards[5];
    std::size_t flopCardsCount = 0;
    for ( ; flopCardsCount < flop.size(); ++flopCardsCount)
    {
        const int temp = flop[flopCardsCount];
        flopCards[flopCardsCount] = temp;
        deadCards[temp] = true;
    }

    std::vector<int> outs;
    for (std::size_t iteration = 0 ; iteration < iterations; ++iteration)
    {
        outs.clear();

        // get random cards for flop
        for (std::size_t i = flopCardsCount; i < 5; ++i)
        {
            const int temp = GetRandomCard(deadCards);
            assert(temp != player1);
            assert(temp != player2);
            flopCards[i] = temp;
            deadCards[temp] = true;
            outs.push_back(temp);
        }

        // get bot rank
        const short botRank = m_RanksEvaluator->getRankOfSeven(player1, player2, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);

        // get random cards for each player
        for (std::size_t i = 0; i < playerRanges.size(); ++i)
        {
            const int first = GetRandomCard(deadCards, playerRanges[i]);
            const int second = GetRandomCard(deadCards, playerRanges[i]);

            outs.push_back(first);
            outs.push_back(second);

            // get player rank
            const short playerRank = m_RanksEvaluator->getRankOfSeven(first, second, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);
            if (playerRank >= botRank)
            {
                ++result.m_Loose;
                break;
            }
        }

        // clean up dead cards
        for (const int card : outs)
            deadCards[card] = false;
    }

    return result;
}

Evaluator::RunResult Evaluator::RunEquityCalcHands(short player1, short player2, const Card::List& flop, const Hand::List& playerHands, unsigned iterations) const
{
    typedef std::vector<Board::HandList> Players;

    RunResult result(iterations / 10);
    bool deadCards[cfg::CARD_DECK_SIZE] = {false};

    assert(std::find(flop.begin(), flop.end(), Card().FromEvalFormat(player1)) == flop.end());
    assert(std::find(flop.begin(), flop.end(), Card().FromEvalFormat(player2)) == flop.end());

    deadCards[player1] = true;
    deadCards[player2] = true;

    // get flop cards
    int flopCards[5];
    std::size_t flopCardsCount = 0;
    for ( ; flopCardsCount < flop.size(); ++flopCardsCount)
    {
        const int temp = flop[flopCardsCount].ToEvalFormat();
        flopCards[flopCardsCount] = temp;
        deadCards[temp] = true;
    }

    // get all possible cards for opponents hand description
    Players allHands;
    Board board(flop);
    for (const Hand::Value hand : playerHands)
    {
        allHands.push_back(board.GetCardsByHand(hand));

        // filter hands by dead cards
        allHands.back().erase
        (
            std::remove_if
            (
                allHands.back().begin(), 
                allHands.back().end(), 
                [&](const Card::List& hand)
                {
                    return deadCards[hand.front().ToEvalFormat()] || deadCards[hand.back().ToEvalFormat()];
                }
            ),
            allHands.back().end()
        );
    }

    // sort hands by size
    std::sort(allHands.begin(), allHands.end(), [](const Board::HandList& lhs, const Board::HandList& rhs) { return lhs.size() < rhs.size(); });

    std::vector<int> outs;
    for (std::size_t iteration = 0 ; iteration < iterations / 10; ++iteration)
    {
        outs.clear();

        // get random cards for each player
        std::vector<std::pair<short, short>> playersCards;
        for (std::size_t i = 0; i < allHands.size(); ++i)
        {
            const boost::optional<std::pair<short, short>> cards = GetRandomCards(deadCards, allHands[i]);
            if (!cards)
                break; // failed to get random cards

            outs.push_back(cards.get().first);
            outs.push_back(cards.get().second);

            playersCards.push_back(cards.get());
        }

        BOOST_SCOPE_EXIT(&outs, &deadCards)
        {
            // clean up dead cards
            for (const int card : outs)
                deadCards[card] = false;
        }
        BOOST_SCOPE_EXIT_END

        if (playersCards.size() != allHands.size())
        {
            // failed to get random cards, ignore this iteration
            --result.m_Total;
            continue;
        }

        // get random cards for flop
        for (std::size_t i = flopCardsCount; i < 5; ++i)
        {
            const int temp = GetRandomCard(deadCards);
            assert(temp != player1);
            assert(temp != player2);
            flopCards[i] = temp;
            deadCards[temp] = true;
            outs.push_back(temp);
        }

        // get bot rank
        const short botRank = m_RanksEvaluator->getRankOfSeven(player1, player2, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);
        for (const std::pair<short, short>& cards : playersCards)
        {
            // get player rank
            const short playerRank = m_RanksEvaluator->getRankOfSeven(cards.first, cards.second, flopCards[0], flopCards[1], flopCards[2], flopCards[3], flopCards[4]);
            if (playerRank >= botRank)
            {
                ++result.m_Loose;
                break;
            }
        }
    }

    return result;
}

}
