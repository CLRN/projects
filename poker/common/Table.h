#ifndef Table_h__
#define Table_h__

#include <vector>

namespace pcmn
{

class Table
{
public:

    //! Table phase
    struct Phase
    {
        enum Value : unsigned
        {
            Preflop	    = 0,
            Flop        = 1,
            Turn        = 2,
            River       = 3,
            Max         = River
        };

        typedef std::vector<Value> List;

        static unsigned ToCardsCount(Phase::Value v);
        static Phase::Value FromCardsCount(unsigned c);
        static std::string ToString(Phase::Value v);
    };

    static const Phase::List& Phases();
};

//! Stream operator
std::ostream& operator << (std::ostream& s, pcmn::Table::Phase::Value v);

} // namespace pcmn

#endif // Table_h__
