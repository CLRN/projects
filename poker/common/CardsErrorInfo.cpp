#include "CardsErrorInfo.h"
#include "conversion/AnyCast.h"

#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>

namespace pcmn
{

std::wostream& Write(std::wostream& stream, const pcmn::Card& card)
{
    stream << L"(" << conv::cast<std::wstring>(pcmn::Card::ToString(card.m_Value)) << L" of " << conv::cast<std::wstring>(pcmn::Suit::ToString(card.m_Suit)) << L")";
    return stream;
}

std::wostream& operator << (std::wostream& stream, const CardErrorInfo& e)
{
    return Write(stream, e.value());
}

std::wostream& operator<< (std::wostream& stream, const CardsErrorInfo& e)
{
    boost::for_each(e.value(), boost::bind(&Write, boost::ref(stream), _1));
    return stream;
}

} // namespace pcmn
