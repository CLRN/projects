#ifndef Evaluator_h__
#define Evaluator_h__

#include "Cards.h"
#include "Config.h"
#include "Hand.h"
#include "Board.h"

#include <string>

#include <boost/scoped_ptr.hpp>

class SevenEval;
class HandEval;

namespace pcmn
{

class Evaluator
{
public:
	Evaluator(unsigned repititions = cfg::NUMBER_OF_REPITITIONS);
	~Evaluator();
	short GetRank(const Card::List& cards) const;
    short GetRandomCard(bool* dead, const short minValue = cfg::CARD_DECK_SIZE) const; 
	float GetEquity(short player1, short player2, const std::vector<int>& flop, const std::vector<short>& playerRanges) const;
    float GetEquity(short player1, short player2, const Card::List& flop, const Hand::List& opponents) const;
    float GetEquity(const Card::List& bot, const Card::List& flop, const Hand::List& opponents) const;
    float GetEquity(const Card::List& bot, const Card::List& flop, const std::vector<Card::List>& opponents) const;

    static const Evaluator& Instance();
private:
    struct RunResult
    {
        RunResult(unsigned total) : m_Loose(), m_Total(total) {}
        unsigned m_Loose;
        unsigned m_Total;
    };
    RunResult RunEquityCalcRange(short player1, short player2, const std::vector<int>& flop, const std::vector<short>& playerRanges, unsigned iterations) const;
    RunResult RunEquityCalcHands(short player1, short player2, const Card::List& flop, const Hand::List& opponents, unsigned iterations) const;

    template<typename T>
    RunResult RunTask(const T& functor) const;

private:
	SevenEval* m_RanksEvaluator;
	HandEval* m_HandsEvaluator;
    unsigned m_Repititions;
};
}
#endif // Evaluator_h__
