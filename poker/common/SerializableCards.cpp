﻿#include "SerializableCards.h"
#include "serialization/Helpers.h"

pcmn::srl::Card::Card(Value value, Suit::Value suit) 
    : SERIALIZABLE("card",
        FIELD(m_Value)
        FIELD(m_Suit)
    )
    , pcmn::Card(value, suit)
{

}

pcmn::srl::Card::Card(const pcmn::Card& card)
    : SERIALIZABLE("card",
        FIELD(m_Value)
        FIELD(m_Suit)
    )
    , pcmn::Card(card)
{

}
