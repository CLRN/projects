#ifndef Player_h__
#define Player_h__

#include "SerializableCards.h"
#include "Actions.h"
#include "BetSize.h"
#include "Hand.h"
#include "serialization/Serialization.h"

#include <string>
#include <vector>
#include <cassert>
#include <exception>
#include <deque>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

namespace pcmn
{
struct TableContext;

class Player : public cmn::srl::Serializable
{
public:

	//! Player game styles
	struct Style
	{
		enum Value
		{
			Passive		= 0,		//!< check or fold
			Normal		= 1,		//!< call
			Aggressive	= 2,		//!< raise/bet/reraise
			Max			= Aggressive
		};

		static std::string ToString(Value value);
	};

	//! Player state
	struct State
	{
		enum Value
		{
			Called	= 0,    // player called all bets
			Folded	= 1,    // player folded
			Waiting	= 2     // player waiting for his turn
		};

        static std::string ToString(Value value);
	};
	
	//! Player positions
	struct Position
	{
		enum Value
		{
			Early	= 0,
			Middle	= 1,
			Later	= 2,
			Max = Later
		};
		static std::string ToString(Value value);
	};

	//! Players count
	struct Count
	{
		enum Value
		{
			One			= 0,
			Two			= 1,
			ThreeOrMore	= 2,
			Max			= ThreeOrMore
		};
		static std::string ToString(Value value);
		static Value FromValue(unsigned value);
	};

	//! Player action descriptor
	struct ActionDesc
	{
        typedef std::vector<ActionDesc> List;
		Action::Value m_Id;             //!< action id
		BetSize::Value m_Amount;        //!< actions amount
        Position::Value m_Position;     //!< player position at action moment
        Action::Value m_ReasonId;       //!< previous another player action
        BetSize::Value m_ReasonAmount;  //!< previous bet size
        Count::Value m_Count;           //!< players in queue at action moment

        ActionDesc();
        bool operator == (const ActionDesc& other) const;
        bool operator < (const ActionDesc& other) const;
	};

	typedef std::vector<Player> List;
	typedef std::vector<Style::Value> Styles;
	typedef std::vector<ActionDesc::List> Actions;
    typedef std::deque<Player> Queue;
    typedef std::vector<Hand::Value> Hands;

    virtual ~Player() {}
	Player(const std::string& name = "", const std::size_t stack = 0);

    bool HasName() const                    { return !m_Name.empty(); }
	const std::string& Name() const			{ assert(!m_Name.empty()); return m_Name; }
    Player& Name(const std::string& val)    { assert(!val.empty()); m_Name = val; return *this; }
	std::size_t Stack() const				{ return m_Stack; }
    Player& Stack(std::size_t val)          { m_Stack = val; return *this; }
	std::size_t Bet() const					{ return m_Bet; }
    Player& Bet(std::size_t val)            { m_Bet = val; return *this; }
	Card::List Cards() const;
    Player& Cards(const Card::List& val);
	State::Value State() const				{ return m_State; }
    Player& State(State::Value val)			{ m_State = val; return *this; }
	const Actions& GetActions() const		{ return m_Actions; }
    std::size_t TotalBet() const            { return m_TotalBet; }
    Player& TotalBet(std::size_t val)       { m_TotalBet = val; return *this; }
    bool Afk() const                        { return m_Afk; }
    Player& Afk(bool val)                   { m_Afk = val; return *this; }
    const Hands& GetHands() const           { return m_Hands; }
    Player& PushHand(Hand::Value val)       { m_Hands.push_back(val); return *this; }

    const std::vector<float>& Equities() const { return m_Equities; }

    Player& Reset();

    Player& SetStyle(std::size_t phase, Style::Value style)
	{
		if (style > m_Styles[phase])
			m_Styles[phase] = style;
        return *this;
	}

	Style::Value GetStyle(std::size_t phase)
	{
		return m_Styles[phase];
	}

	static Player& ThisPlayer();
    const static Player& Unknown();
    
	bool operator == (const Player& other) const;

	//! Add player action
    Player& PushAction
    (
        unsigned street, 
        Action::Value action, 
        BetSize::Value value, 
        Position::Value pos,
        Action::Value reasonAction,
        BetSize::Value reasonAmount,
        Count::Value count
    );

    //! Add player equity
    void PushEquity(float val) { m_Equities.push_back(val); }

protected:
	std::string m_Name;		
	std::size_t m_Stack;
	std::size_t m_Bet;				//!< player bet on this street
	srl::Card::List m_Cards;        //!< player cards
	State::Value m_State;			//!< player state
    std::size_t m_TotalBet;         //!< player total bet

    Actions m_Actions;				//!< player actions
    Hands m_Hands;                  //!< player hands description
	Styles m_Styles;				//!< player styles during this game
    std::vector<float> m_Equities;  //!< player equities on streets
    bool m_Afk;                     //!< is player away from keyboard
};

//! Stream operator
std::ostream& operator << (std::ostream& s, const pcmn::Player& p);

//! Stream operator
std::ostream& operator << (std::ostream& s, pcmn::Player::Position::Value p);

} // namespace pcmn

namespace boost
{
    std::size_t hash_value(const pcmn::Player::ActionDesc& action); // id, amount, position
    std::size_t hash_value(const pcmn::Player& p); // name and actions
}

#endif // Player_h__
