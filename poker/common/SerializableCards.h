#ifndef SerializableCards_h__
#define SerializableCards_h__

#include "Cards.h"
#include "serialization/Serialization.h"

#include <string>
#include <vector>

#include <boost/functional/hash/hash.hpp>

namespace pcmn
{
namespace srl
{

struct Card : public pcmn::Card, public cmn::srl::Serializable
{
	typedef std::vector<Card> List;

    Card(const pcmn::Card& card);
	Card(Value value = Unknown, Suit::Value suit = Suit::Unknown);
};

} // namespace srl
} // namespace pcmn
#endif // SerializableCards_h__
