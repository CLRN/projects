#ifndef Cards_h__
#define Cards_h__

#include <string>
#include <vector>

#include <boost/functional/hash/hash.hpp>

namespace pcmn
{
	struct Suit
	{
		enum Value : char
		{
			Unknown		= 0,
			Hearts		= 'h',
			Clubs		= 'c',
			Spades		= 's',
			Diamonds	= 'd',
            MaxValue    = Spades
		};

        typedef std::vector<Suit::Value> List;

		static std::string ToString(Value value);
        static Value FromString(wchar_t symbol);
	};

	struct Card
	{
		typedef std::vector<Card> List;
		enum Value : char
		{
			Unknown	= 0,
			Two		= 2,
			Three	= 3,
			Four	= 4,
			Five	= 5,
			Six		= 6,
			Seven	= 7,
			Eight	= 8,
			Nine	= 9,
			Ten		= 10,
			Jack	= 11,
			Queen	= 12,
			King	= 13,
			Ace		= 14,
            MaxValue = Ace
		};

		Card(Value value = Unknown, Suit::Value suit = Suit::Unknown);

		static std::string ToString(Value value);
		static Value FromString(const char value);
		static Card FromString(const std::string& value, const std::string& suit);
		short ToEvalFormat() const;
		const Card& FromEvalFormat(short value); 
		bool IsValid() const;
        bool operator == (const Card& other) const;
        bool operator < (const Card& other) const
        {
            if (m_Value == other.m_Value)
                return m_Suit < other.m_Suit;
            return m_Value < other.m_Value;
        }

		Value m_Value;
		Suit::Value m_Suit;
	};

    //! Cards list holder/appender
    class CardListHolder
    {
    public:
        CardListHolder(const Card& c);
        CardListHolder& operator () (const Card& c);
        operator const Card::List& () const;
    private:
        Card::List m_List;
    };


    //! Stream operator
    std::ostream& operator << (std::ostream& s, const pcmn::Card& c);
    std::wostream& operator << (std::wostream& s, const pcmn::Card& c);

    //! Stream operator
    std::ostream& operator << (std::ostream& s, const pcmn::Card::List& list);
} // namespace pcmn

namespace boost
{
    template<>
    struct hash<pcmn::Card>
    {
        std::size_t operator () (const pcmn::Card& card)
        {
            return boost::hash<short>()(card.ToEvalFormat());
        }
    };
}

#endif // Cards_h__
