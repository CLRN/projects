#ifndef ITableLogic_h__
#define ITableLogic_h__

#include "Player.h"
#include "Actions.h"
#include "ITable.h"
#include "Table.h"
#include "packet.pb.h"

namespace pcmn
{

//! Table logic implementation
class ITableLogic
{
public:

    virtual ~ITableLogic() {}

    //! Push action
    virtual void PushAction(const std::string& name, Action::Value action, unsigned amount) = 0;

    //! Set player info
    virtual void SetPlayerStack(const std::string& name, unsigned stack) = 0;

    //! Set player cards
    virtual void SetPlayerCards(const std::string& name, const Card::List& cards) = 0;

    //! Set flop cards
    virtual void SetFlopCards(const Card::List& cards) = 0;

    //! Parse packet
    virtual void Parse(const net::Packet& packet) = 0;

    //! Go to another phase
    virtual void SetPhase(Table::Phase::Value phase) = 0;

    //! Pack request and send
    virtual void SendRequest(bool statistics) = 0;

    //! Remove player
    virtual void RemovePlayer(const std::string& name) = 0;

    //! Set data from next round
    virtual void SetNextRoundData(const pcmn::Player::List& players) = 0;

    //! Is round finished
    virtual bool IsRoundCompleted() const = 0;
};

}


#endif // TableLogic_h__
