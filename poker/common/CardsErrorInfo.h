#ifndef CardsErrorInfo_h__
#define CardsErrorInfo_h__

#include "Cards.h"

#include <ostream>
#include <boost/exception/info.hpp>

namespace pcmn
{
    typedef boost::error_info<struct card_error_info, pcmn::Card> CardErrorInfo;
    typedef boost::error_info<struct cards_error_info, pcmn::Card::List> CardsErrorInfo;

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const CardErrorInfo& e);

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const CardsErrorInfo& e);

} // namespace pcmn
#endif // CardsErrorInfo_h__
