set(PROJECT_NAME client)

file(GLOB SOURCES "*.cpp")
file(GLOB HEADERS "*.h" "*.rc" "*.def")

add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "poker/client")
target_link_libraries(${PROJECT_NAME}
	lib_client
)

set(LOGGER_CONFIG 
	general "${POKER_DATA_PATH}/client.trace.cfg"
)

include(symlinks)
symlink_to_output_dirs(${LOGGER_CONFIG})

include(CreateLaunchers)
create_target_launcher(
    ${PROJECT_NAME}
    ARGS "--window-class=PokerStarsTableFrameClass --config=../../../poker/data/PokerStars/config/config.xml"
    WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_CFG_INTDIR}
)
