#include "exception/CheckHelpers.h"
#include "common/Log.h"
#include "common/Modules.h"
#include "common/Window.h"
#include "common/GUID.h"
#include "common/AsioHelpers.h"
#include "recognition/Recognition.h"
#include "xml/Node.h"
#include "net/PipeHost.h"
#include "rpc/Client.h"

#include <conio.h>

#include "TableLogic.h"
#include "../clientlib/TableRecognizer.h"
#include "../clientlib/TableParser.h"
#include "../clientlib/ImageManager.h"
#include "../clientlib/ImageAdapter.h"
#include "../clientlib/Client.h"
#include "../clientlib/PokerStarsTableControl.h"
#include "../clientlib/PokerStarsConfig.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/program_options.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/assign.hpp>

namespace po = boost::program_options;
namespace gp = google::protobuf;

class EmptyChannel : public rpc::details::IChannel
{
public:
    virtual rpc::IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, const gp::Message& request, const rpc::IStream&, const rpc::details::CallParams&) override
    {
        std::cout << method.name() << " sent to server" << std::endl;
        return rpc::IFuture::Ptr();
    }

};

class EmptyClient : public rpc::Endpoint
{
public:
    EmptyClient(ILog& log, const rpc::InstanceId& id) : rpc::Endpoint(log, id) {}
    virtual rpc::details::IChannel& GetChannel(const rpc::IService::Id&)
    {
        return m_Channel;
    }
private:
    EmptyChannel m_Channel;
};

DECLARE_CURRENT_MODULE(Modules::Client);
Log m_Log;

class ClientCallback : public clnt::IClient::ICallback
{
public:
    ClientCallback(const unsigned long window)
        : m_Window(window)
        , m_Recognizer()
        , m_Adapter()
        , m_Counter()
    {
        MakeLogFolder();
    }

    void Init(rec::IRecognizer& recognizer, clnt::IImageAdapter& adapter)
    {
        m_Recognizer = &recognizer;
        m_Adapter = &adapter;
    }

    void MakeLogFolder()
    {
        m_LogFolder = m_Log.GetLogFolder(Modules::Client);
        m_LogFolder /= "events";
        std::string time = boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time());
        boost::algorithm::replace_all(time, ":", ".");
        m_LogFolder /= time;
        boost::filesystem::create_directories(m_LogFolder);
    }

    virtual clnt::ITableEventsReader::Ptr GetNextEvents()
    {
        clnt::ITableEvents::Ptr events;
        cv::Mat image;
        while (!events || events->IsEmpty())
        {
            image = m_Recognizer->CaptureWindow(m_Window);
            events = m_Adapter->Parse(image);
        }

        cmn::xml::Node node;
        events->Serialize(node);

        if (m_Log.IsEnabled(Modules::Client, ILog::Level::Trace))
        {
            {
                boost::filesystem::path path = m_LogFolder / conv::cast<std::string>(m_Counter);
                path += ".xml";

                node.Save2File(path.string());
            }
            {
                boost::filesystem::path path = m_LogFolder / conv::cast<std::string>(m_Counter);
                path += ".png";

                cv::imwrite(path.string(), image);
            }
            ++m_Counter;
        }

        const auto result = clnt::ITableEventsReader::Instance();
        result->Deserialize(node);
        return result;
    }

    virtual void OnNewGame()
    {
        MakeLogFolder();
    }

private:
    unsigned m_Counter;
    unsigned long m_Window;
    rec::IRecognizer* m_Recognizer;
    clnt::IImageAdapter* m_Adapter;
    boost::filesystem::path m_LogFolder;
};

void GetImage(const po::variables_map& vm, cv::Mat& image, unsigned& window)
{
    window = 0;
    CHECK(vm.count("window-class"), cmn::Exception("Unable to get window class parameter"));

    const std::string windowClass = vm["window-class"].as<std::string>();

    for (;;)
    {
        // find window
        const auto windows = cmn::Window::GetAll();
        const auto it = boost::find_if(windows, boost::bind(&cmn::Window::ClassName, _1) == windowClass);
        if (it != windows.end())
        {
            LOG_INFO("Table found: %s") % it->Title();
            window = it->GetId();
            image = rec::IRecognizer::Instance()->CaptureWindow(it->GetId());
            break;
        }

        boost::this_thread::interruptible_wait(1000);
    }
}

boost::shared_ptr<rpc::Endpoint> GetChannel(const po::variables_map& vm)
{
    if (vm.count("server"))
    {
        // server connection
        static const auto host = boost::make_shared<net::pipes::Transport>(m_Log);

        // client rpc instance
        const auto client = rpc::IClient::Instance(m_Log, *host, vm["server"].as<std::string>(), "poker client");
        client->Connect();
        cmn::Service::Instance().poll();
        return client;
    }
    else
    {
        return boost::make_shared<EmptyClient>(m_Log, "");
    }
}

int main(int argc, char* argv[])
{
    try 
    {
        pcmn::Player::ThisPlayer().Name("CLRN");

        m_Log.Open("client.trace.cfg");

        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("window-class", po::value<std::string>(), "find window by class name")
            ("config", po::value<std::string>(), "input config file name")
            ("server", po::value<std::string>(), "server endpoint name");

        po::positional_options_description p;

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help")) 
        {
            std::cout << desc << std::endl;
            return 1;
        }

        CHECK(vm.count("config"), cmn::Exception("Config file name parameter missing"));

        // obtain rpc server channel
        const auto channel = GetChannel(vm);

        unsigned window = 0;
        cv::Mat image;
        GetImage(vm, image, window);

        const auto recognizer = rec::IRecognizer::Instance();
        const auto dealerButton = cv::imread("../../../poker/data/PokerStars/objects/dealer.png");
        const auto cardsBack = cv::imread("../../../poker/data/PokerStars/objects/cards_back.png");

        ClientCallback cb(window);
        clnt::ps::Config cfg(m_Log);
        clnt::ps::TableControl control(m_Log, cfg, reinterpret_cast<HWND>(window));
        const auto client = clnt::IClient::Instance(m_Log, cb, *channel, control);
        const auto adapter = clnt::IImageAdapter::Instance(m_Log, client->GetRecognizer(), client->GetParser(), dealerButton, cardsBack);

        const auto params = client->GetTable().GetAllAreas();
        adapter->Initialize(params);

        cb.Init(client->GetRecognizer(), *adapter);

        client->Run();

        m_Log.Close();

        return 0;
    }
    catch (const std::exception& e)
    {
        LOG_ERROR("%s") % cmn::ExceptionInfo(e);
        m_Log.Close();
        return 1;
    }
}


