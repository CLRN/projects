if(__PROTOBUF_INCLUDED)
  return()
endif()
set(__PROTOBUF_INCLUDED TRUE)

include(CMakeParseArguments)

set(PROTO_FULL_PATH "${PROJECTS_ROOT_PATH}/\;${THIRD_PARTY}/inc/\;${PROJECTS_ROOT_PATH}/common/protocols/")
set(PROTOBUF_COMPILER "${THIRD_PARTY}/bin/protobuf/protoc.exe")
set(PROTOBUF_COMPILER_BINARIES 
    general ${PROTOBUF_COMPILER}
)
include(symlinks)
symlink_to_output_dirs(${PROTOBUF_COMPILER_BINARIES})

# получить имя файла без расширения
function(get_filename_we _sourceFile _fileNameWE)
    # cmake built-in function get_filename_component returns extension from first occurrence of . in file name
    # this function computes the extension from last occurrence of . in file name
    string (FIND "${_sourceFile}" "." _index REVERSE)
    if (_index GREATER -1)
        string (SUBSTRING "${_sourceFile}" 0 ${_index} _sourceExt)
    else()
        set (_sourceExt "${_sourceFile}")
    endif()
    
    set (${_fileNameWE} "${_sourceExt}" PARENT_SCOPE)
endfunction()

function(protobuf_generate_cpp GENERATED_HDR GENERATED_SRC)
    set(options "")
    set(oneValueArgs INCLUDE_PATH TARGET FOLDER)
    set(multiValueArgs PROTOFILES)

    cmake_parse_arguments(PROTOBUF_FUNC "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    
    foreach(_protoFile ${PROTOBUF_FUNC_PROTOFILES})
        get_filename_component(_absoluteFilePath ${_protoFile} ABSOLUTE)
        get_filename_component(_protoFilePath ${_absoluteFilePath} PATH)
        get_filename_component(_protoNameTmp ${_absoluteFilePath} NAME)
        get_filename_we(${_protoNameTmp} _protoName)
                
        set(_generatedHdr "${PROTOBUF_FUNC_FOLDER}/${_protoName}.pb.h")
        set(_generatedSrc "${PROTOBUF_FUNC_FOLDER}/${_protoName}.pb.cc")
        list(APPEND _generatedHeaders ${_generatedHdr})
        list(APPEND _generatedSources ${_generatedSrc})    
    
        ADD_CUSTOM_COMMAND(
            PRE_BUILD
            OUTPUT ${_generatedSrc} ${_generatedHdr}
            COMMAND "${PROTOBUF_COMPILER}"
            ARGS ${_protoFile} --cpp_out="${PROJECTS_ROOT_PATH}/" --python_out="${PROJECTS_ROOT_PATH}/py/" --clrn_out="${PROJECTS_ROOT_PATH}/" --proto_path="${PROTO_FULL_PATH}" --error_format=msvs
            WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_CFG_INTDIR} 
            DEPENDS ${_protoFile}
            COMMENT "Executing protocompiler: ${_protoFile} --cpp_out=${PROJECTS_ROOT_PATH} --python_out=${PROJECTS_ROOT_PATH}/py/ --clrn_out=${PROJECTS_ROOT_PATH} --proto_path=${PROTO_FULL_PATH} --error_format=msvs"
        )
    endforeach()
    
    set(${GENERATED_HDR} ${_generatedHeaders} PARENT_SCOPE)
    set(${GENERATED_SRC} ${_generatedSources} PARENT_SCOPE)
    
endfunction()