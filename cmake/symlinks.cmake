if(__SYMLINKS_INCLUDED)
  return()
endif()
set(__SYMLINKS_INCLUDED TRUE)

include(parse_file_list)
include(get_config_list)

function(create_symlink link target)
    if(NOT EXISTS ${target})
        message(FATAL_ERROR "Could not create symbolic link for: ${target} --> ${output}, target not exists")
    endif()
    if(EXISTS ${target} AND NOT EXISTS ${link})
        file(TO_NATIVE_PATH "${link}" link_path)
        file(TO_NATIVE_PATH "${target}" target_path)
        
        if(WIN32)
            if(IS_DIRECTORY ${target_path})
                # using junctions because python 3.1.3 have a bug with symlinks for directories
                set(command cmd /c mklink /J "${link_path}" "${target_path}")
            else()
                set(command cmd /c mklink "${link_path}" "${target_path}")
            endif()
        else()
            set(command ln -s ${target_path} ${link_path})
        endif()
        
        execute_process(COMMAND ${command}
                        OUTPUT_QUIET
                        RESULT_VARIABLE result
                        ERROR_VARIABLE output)
                        
        if (NOT ${result} EQUAL 0)
            message(FATAL_ERROR "Could not create symbolic link for: ${target} --> ${output}")
        endif()
    endif()
endfunction()

function(symlink_to_output_dirs)
    parse_debug_optimized_list(_debugList _optimizedList _generalList ${ARGN})
    get_configuration_list(_debugConfigs _releaseConfigs)
    
    # create output dirs
    foreach(item ${CMAKE_CONFIGURATION_TYPES})
        set(dir_name "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${item}")
        file(MAKE_DIRECTORY ${dir_name})
    endforeach()

    # из _debugList пихаем в дебажные конфигурации
    foreach(item ${_debugList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${_debugConfigs})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Linking ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
                create_symlink("${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" "${item}")
			endif()
        endforeach()
    endforeach()
    
    # из _optimizedList пихаем в релизные конфигурации
    foreach(item ${_optimizedList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${_releaseConfigs})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Linking ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				create_symlink("${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" "${item}")
			endif()
        endforeach()
    endforeach()
    
    # из _generalList пихаем во все конфигурации
    foreach(item ${_generalList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${CMAKE_CONFIGURATION_TYPES})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Linking ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				create_symlink("${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" "${item}")
			endif()
        endforeach()
    endforeach()
endfunction()