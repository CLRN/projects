include(ExternalProject)

function(build_opencv url)
        
    set(COMPONENT opencv)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
	
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
	
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        GIT_TAG "master"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_COMMAND ""
        CMAKE_ARGS 
                -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
                -DBUILD_WITH_STATIC_CRT=OFF
                -DBUILD_SHARED_LIBS=OFF
                -DBUILD_TESTS=OFF
                -DBUILD_PERF_TESTS=OFF
                -DBUILD_EXAMPLES=OFF
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${BINARY_DIR}/${COMPONENT}.sln /Build Debug
        COMMAND devenv ${BINARY_DIR}/${COMPONENT}.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/install/include/opencv" "${THIRD_PARTY}/inc/opencv"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/install/include/opencv2" "${THIRD_PARTY}/inc/opencv2"
        
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/lib/Debug" "${THIRD_PARTY}/lib/Debug"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/lib/Release" "${THIRD_PARTY}/lib/Release"
        
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/3rdparty/lib/Debug" "${THIRD_PARTY}/lib/Debug"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BINARY_DIR}/3rdparty/lib/Release" "${THIRD_PARTY}/lib/Release"
        COMMAND ${CMAKE_COMMAND} -E copy "${BINARY_DIR}/install/x86/vc12/staticlib/ippicvmt.lib" "${THIRD_PARTY}/lib/ippicvmt.lib"
        
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  	
endfunction()