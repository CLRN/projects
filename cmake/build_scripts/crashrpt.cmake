include(ExternalProject)

function(build_crashrpt url)
        
    set(COMPONENT crashrpt)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
        
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        SVN_REPOSITORY "${url}"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_IN_SOURCE 1
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CMAKE_ARGS -DCMAKE_CXX_FLAGS=${LOG4CPLUS_CXX_FLAGS}                
                -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${COMPONENT_ROOT}/${COMPONENT}.sln /Build Debug
        COMMAND devenv ${COMPONENT_ROOT}/${COMPONENT}.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/${COMPONENT}_lang.ini" "${THIRD_PARTY}/bin/${COMPONENT}/${COMPONENT}_lang.ini"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/${COMPONENT}1403.dll" "${THIRD_PARTY}/bin/${COMPONENT}/${COMPONENT}1403.dll"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/${COMPONENT}1403d.dll" "${THIRD_PARTY}/bin/${COMPONENT}/${COMPONENT}1403d.dll"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/dbghelp.dll" "${THIRD_PARTY}/bin/${COMPONENT}/dbghelp.dll"

        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/CrashSender1403.exe" "${THIRD_PARTY}/bin/${COMPONENT}/CrashSender1403.exe"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/CrashSender1403d.exe" "${THIRD_PARTY}/bin/${COMPONENT}/CrashSender1403d.exe"

        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/CrashRpt1403d.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/CrashRpt1403.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"
        
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/include" "${THIRD_PARTY}/inc/${COMPONENT}"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  
endfunction()