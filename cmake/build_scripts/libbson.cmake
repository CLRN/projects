include(ExternalProject)

function(build_libbson url)
        
    set(COMPONENT libbson)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_COMMAND "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_COMMAND "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_COMMAND "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_COMMAND "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_COMMAND "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_COMMAND "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
                        
    set(BUILD_COMMAND 
        COMMAND call msbuild.exe ALL_BUILD.vcxproj
    )
    set(TARGET_NAME ${COMPONENT}_build)
    set(INSTALL_CMD
        COMMAND msbuild.exe INSTALL.vcxproj
    )
    
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        GIT_TAG "1.0.0"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_COMMAND "${BUILD_COMMAND}"
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND "${INSTALL_CMD}"
        CMAKE_ARGS  -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} 
                    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
                    -DCMAKE_INSTALL_PREFIX=${COMPONENT_ROOT}
    ) 
    

    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${COMPONENT_ROOT}/${COMPONENT}.sln /Build Debug
        COMMAND devenv ${COMPONENT_ROOT}/${COMPONENT}.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  

    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${PROJECTS_ROOT_PATH}/third_party/patches/libbson" "${COMPONENT_ROOT}/include/${COMPONENT}-1.0"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/include/${COMPONENT}-1.0" "${THIRD_PARTY}/inc/${COMPONENT}"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Debug/bson-1.0.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Release/bson-1.0.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"

        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Debug/libbson-1.0.dll" "${THIRD_PARTY}/bin/${COMPONENT}/debug/libbson-1.0.dll"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Release/libbson-1.0.dll" "${THIRD_PARTY}/bin/${COMPONENT}/release/libbson-1.0.dll"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )   
endfunction()