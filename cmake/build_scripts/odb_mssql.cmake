include(ExternalProject)

function(build_odb_mssql url)
        
    set(COMPONENT odb-mssql)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()  
    
    set(PRECONFIGURE_CMD 
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${THIRD_PARTY}/inc/odb" "${COMPONENT_ROOT}/odb"
        COMMAND ${CMAKE_COMMAND} -E copy "${THIRD_PARTY}/lib/release/odb.lib" "${COMPONENT_ROOT}/odb/mssql/odb.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${THIRD_PARTY}/lib/debug/odb.lib" "${COMPONENT_ROOT}/odb/mssql/odb-d.lib"
    )
    set(PATCH_CMD 
        COMMAND ${CMAKE_COMMAND} -E copy "${PROJECTS_ROOT_PATH}/third_party/patches/odb/libodb-mssql-vc12.vcxproj" "${COMPONENT_ROOT}/odb/mssql/libodb-mssql-vc12_patched.vcxproj"
        COMMAND ${CMAKE_COMMAND} -E copy "${PROJECTS_ROOT_PATH}/third_party/patches/odb/libodb-mssql-vc12.sln" "${COMPONENT_ROOT}/libodb-mssql-vc12.sln"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${PROJECTS_ROOT_PATH}/third_party/patches/odb/mssql" "${COMPONENT_ROOT}/odb/mssql/details"
    )    
    
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        GIT_TAG "2.3"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        PATCH_COMMAND "${PATCH_CMD}"
        CONFIGURE_COMMAND "${PRECONFIGURE_CMD}"
        BUILD_IN_SOURCE 1
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
    )  
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${COMPONENT_ROOT}/libodb-mssql-vc12.sln /Build Debug
        COMMAND devenv ${COMPONENT_ROOT}/libodb-mssql-vc12.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/bin" "${THIRD_PARTY}/bin/odb"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/odb" "${THIRD_PARTY}/inc/odb"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/${COMPONENT}.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/${COMPONENT}-d.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"        
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )     

        
    add_dependencies(${TARGET_NAME}
        odb_build
        odb-boost_build
    )
    
endfunction()