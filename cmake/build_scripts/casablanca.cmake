include(ExternalProject)

function(build_casablanca url)
        
    set(COMPONENT rest)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CONFIGURE_COMMAND ""
        BUILD_IN_SOURCE 1
    ) 
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND nuget restore casablanca120.desktop.sln
        COMMAND devenv ${COMPONENT_ROOT}/casablanca120.desktop.sln /Build Debug /Project casablanca120
        COMMAND devenv ${COMPONENT_ROOT}/casablanca120.desktop.sln /Build Release /Project casablanca120
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Debug/cpprest120d_2_2.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Release/cpprest120_2_2.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"

        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Debug/cpprest120d_2_2.dll" "${THIRD_PARTY}/bin/${COMPONENT}/debug/cpprest120d_2_2.dll"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Release/cpprest120_2_2.dll" "${THIRD_PARTY}/bin/${COMPONENT}/release/cpprest120_2_2.dll"

        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Debug/cpprest120d_2_2.pdb" "${THIRD_PARTY}/bin/${COMPONENT}/debug/cpprest120d_2_2.pdb"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/Binaries/Win32/Release/cpprest120_2_2.pdb" "${THIRD_PARTY}/bin/${COMPONENT}/release/cpprest120_2_2.pdb"

        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/Release/include" "${THIRD_PARTY}/inc/${COMPONENT}"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  
    add_dependencies(${TARGET_NAME}
        boost_build
    )    
endfunction()