include(ExternalProject)

function(build_log4cplus url)
        
    set(LOG4CPLUS_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/log4cplus")
    set(TARGET_NAME log4cplus_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
   
    ExternalProject_Add(${TARGET_NAME}
        PREFIX log4cplus
        GIT_REPOSITORY "${url}"
        GIT_TAG "a743b76770bd1c33328f01f4c9de3da8a8749735"
        DOWNLOAD_DIR "${LOG4CPLUS_ROOT}"
        SOURCE_DIR "${LOG4CPLUS_ROOT}"
        INSTALL_COMMAND ""
        BUILD_COMMAND ""
        CMAKE_ARGS 
                -DUNICODE=OFF
                -DLOG4CPLUS_BUILD_TESTING=OFF
                -DBUILD_SHARED_LIBS=OFF
                -DCMAKE_CXX_FLAGS=${LOG4CPLUS_CXX_FLAGS}                
                -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${BINARY_DIR}/log4cplus.sln /Build Debug
        COMMAND devenv ${BINARY_DIR}/log4cplus.sln /Build Release
        WORKING_DIRECTORY ${LOG4CPLUS_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy "${BINARY_DIR}/src/debug/log4cplusSD.lib" "${THIRD_PARTY}/lib/debug/log4cplus.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${BINARY_DIR}/src/release/log4cplusS.lib" "${THIRD_PARTY}/lib/release/log4cplus.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${LOG4CPLUS_ROOT}/include/log4cplus" "${THIRD_PARTY}/inc/log4cplus"
        WORKING_DIRECTORY ${LOG4CPLUS_ROOT}
        DEPENDEES build
    )  
    
endfunction()