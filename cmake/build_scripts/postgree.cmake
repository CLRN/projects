include(ExternalProject)

function(build_postgree url)
        
    set(COMPONENT postgree)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        URL "${url}"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        CONFIGURE_COMMAND ""
        BUILD_IN_SOURCE 1
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CMAKE_ARGS  -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
    )  
    
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/include" "${THIRD_PARTY}/inc/postgree"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/libpq.lib" "${THIRD_PARTY}/lib/libpq.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/libpq.dll" "${THIRD_PARTY}/bin/postgree/libpq.dll"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/bin/libintl.dll" "${THIRD_PARTY}/bin/postgree/libintl.dll"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )    
    
endfunction()