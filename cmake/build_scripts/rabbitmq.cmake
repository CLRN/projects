include(ExternalProject)

function(build_rabbitmq url)
        
    set(COMPONENT librabbitmq)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
	
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
	
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        GIT_TAG "master"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_COMMAND ""
        CMAKE_ARGS 
               -DBUILD_SHARED_LIBS:BOOL=On
               -DBUILD_STATIC_LIBS:BOOL=Off
               -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} 
               -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
               -DENABLE_SSL_SUPPORT=OFF
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${BINARY_DIR}/rabbitmq-c.sln /Build Debug
        COMMAND devenv ${BINARY_DIR}/rabbitmq-c.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/${COMPONENT}" "${THIRD_PARTY}/inc/${COMPONENT}"
        
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/${COMPONENT}/Debug/rabbitmq.1.dll" "${THIRD_PARTY}/bin/${COMPONENT}/debug/rabbitmq.1.dll"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/${COMPONENT}/Debug/rabbitmq.1.lib" "${THIRD_PARTY}/lib/debug/rabbitmq.1.lib"

        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/${COMPONENT}/Release/rabbitmq.1.dll" "${THIRD_PARTY}/bin/${COMPONENT}/release/rabbitmq.1.dll"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/${COMPONENT}/Release/rabbitmq.1.lib" "${THIRD_PARTY}/lib/release/rabbitmq.1.lib"
               
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  	
endfunction()