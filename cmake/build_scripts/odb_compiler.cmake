include(ExternalProject)

function(build_odb_compiler url)
        
    set(COMPONENT odb-compiler)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        URL "${url}"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        CONFIGURE_COMMAND ""
        BUILD_IN_SOURCE 1
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CMAKE_ARGS  -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
    )  
    
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/etc" "${THIRD_PARTY}/bin/odb/etc"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/bin" "${THIRD_PARTY}/bin/odb/bin"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/mingw" "${THIRD_PARTY}/bin/odb/mingw"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )      
endfunction()