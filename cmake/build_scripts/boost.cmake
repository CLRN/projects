include(ExternalProject)
include(zlib)
include(bzip2)

function(build_boost url)
        
    build_zlib(${ZLIB_SOURCE_PATH})
    build_bzip2(${BZIP2_SOURCE_PATH})
    set(BOOST_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/boost")
    
    if(WIN32)
        set(CONFIGURE_COMMAND "bootstrap.bat")
        set(BUILD_COMMAND bjam)
                
        if(MSVC11)
            set(toolset "msvc-11.0")
        elseif(MSVC12)
            set(toolset "msvc-12.0")
        else()
            message(FATAL_ERROR "Toolset for this compiler not resolved")
        endif()
    else()
        set(CONFIGURE_COMMAND ./bootstrap.sh)
        set(BUILD_COMMAND ./b2)
                
        if (CMAKE_COMPILER_IS_GNUCC)
            execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion
                            OUTPUT_VARIABLE GCC_VERSION)
            string(REGEX MATCHALL "[0-9]+" GCC_VERSION_COMPONENTS ${GCC_VERSION})
            list(GET GCC_VERSION_COMPONENTS 0 GCC_MAJOR)
            list(GET GCC_VERSION_COMPONENTS 1 GCC_MINOR)
                
            set(toolset "gcc-${GCC_MAJOR}.${GCC_MINOR}")
        else()
            message(FATAL_ERROR "Toolset for this compiler not resolved")
        endif()
    endif()
            
    set(BUILD_COMMAND ${BUILD_COMMAND} 
        toolset=${toolset} 
        link=static 
        --with-system 
        --with-thread 
        --with-serialization 
        --with-date_time 
        --with-program_options 
        --with-regex 
        --with-filesystem 
        --with-locale 
        --with-random 
        --with-timer 
        --with-python
        --with-iostreams 
        -sBZIP2_SOURCE="${THIRD_PARTY_DOWNLOAD_DIR}/bzip2"
        -sZLIB_SOURCE="${THIRD_PARTY_DOWNLOAD_DIR}/zlib"
        stage
    )
    
    set(TARGET_NAME boost_build)
    set(PATCH_CMD 
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${PROJECTS_ROOT_PATH}/third_party/patches/boost" "${BOOST_ROOT}/boost"
    )
    set(INSTALL_CMD
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BOOST_ROOT}/boost" "${THIRD_PARTY}/inc/boost"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${BOOST_ROOT}/stage/lib" "${THIRD_PARTY}/lib"
    )
    
    ExternalProject_Add(${TARGET_NAME}
        PREFIX boost
        URL "${BOOST_SOURCE_PATH}"
        DOWNLOAD_DIR "${BOOST_ROOT}"
        SOURCE_DIR "${BOOST_ROOT}"
        PATCH_COMMAND "${PATCH_CMD}"
        CONFIGURE_COMMAND "${CONFIGURE_COMMAND}"
        BUILD_COMMAND "${BUILD_COMMAND}"
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND "${INSTALL_CMD}"
        CMAKE_ARGS -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    )
    add_dependencies(${TARGET_NAME}
        zlib_build
        bzip2_build
    )
#    ExternalProject_Add_Step(${TARGET_NAME} cleanup
#        COMMAND ${CMAKE_COMMAND} -E remove_directory "${BOOST_ROOT}"
#        WORKING_DIRECTORY ${BOOST_ROOT}
#        DEPENDEES install
#    )   
endfunction()