include(ExternalProject)

function(build_odb url)
        
    set(COMPONENT odb)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
    
    set(PATCH_CMD 
        COMMAND ${CMAKE_COMMAND} -E copy "${PROJECTS_ROOT_PATH}/third_party/patches/odb/libodb-vc12.vcxproj" "${COMPONENT_ROOT}/odb/libodb-vc12.vcxproj"
        COMMAND ${CMAKE_COMMAND} -E copy "${PROJECTS_ROOT_PATH}/third_party/patches/odb/libodb-vc12.sln" "${COMPONENT_ROOT}/libodb-vc12.sln"
    )
    
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        GIT_REPOSITORY "${url}"
        GIT_TAG "2.3"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        PATCH_COMMAND "${PATCH_CMD}"
        CONFIGURE_COMMAND ""
        BUILD_IN_SOURCE 1
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
    ) 
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${COMPONENT_ROOT}/libodb-vc12.sln /Build Debug
        COMMAND devenv ${COMPONENT_ROOT}/libodb-vc12.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/${COMPONENT}" "${THIRD_PARTY}/inc/${COMPONENT}"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/bin" "${THIRD_PARTY}/bin/${COMPONENT}"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/${COMPONENT}.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${COMPONENT_ROOT}/lib/${COMPONENT}-d.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  

endfunction()