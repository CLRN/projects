include(ExternalProject)

function(build_gmock url)
        
    set(COMPONENT gmock)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
   
    set(__cxx_flags "")
    if(MSVC11)
        # VC11 contains std::tuple with variadic templates emulation macro.
        # _VARIADIC_MAX defaulted to 5, but gtest requires 10.
        set(__cxx_flags "/D_VARIADIC_MAX=10")
    endif()
        
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        SVN_REPOSITORY "${url}"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CMAKE_ARGS  -DCMAKE_CXX_FLAGS=${__cxx_flags}
                    -Dgtest_force_shared_crt=ON
                    -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${BINARY_DIR}/${COMPONENT}.sln /Build Debug
        COMMAND devenv ${BINARY_DIR}/${COMPONENT}.sln /Build Release
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/debug/${COMPONENT}_main.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}_main.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/debug/${COMPONENT}.lib" "${THIRD_PARTY}/lib/debug/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/release/${COMPONENT}_main.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}_main.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${BINARY_DIR}/release/${COMPONENT}.lib" "${THIRD_PARTY}/lib/release/${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/include/${COMPONENT}" "${THIRD_PARTY}/inc/${COMPONENT}"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  
endfunction()