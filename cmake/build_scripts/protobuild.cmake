include(ExternalProject)

function(build_protobuf url)
        
    set(COMPONENT protobuf)
    set(COMPONENT_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/${COMPONENT}")
    set(TARGET_NAME ${COMPONENT}_build)
            
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_CMD "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_CMD "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_CMD "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_CMD "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_CMD "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_CMD "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
        
    ExternalProject_Add(${TARGET_NAME}
        PREFIX ${COMPONENT}
        SVN_REPOSITORY "${url}"
        SVN_REVISION -r "525"
        DOWNLOAD_DIR "${COMPONENT_ROOT}"
        SOURCE_DIR "${COMPONENT_ROOT}"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        CMAKE_ARGS  -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    ) 
    
    ExternalProject_Get_Property(${TARGET_NAME} BINARY_DIR)
    
    ExternalProject_Add_Step(${TARGET_NAME} build_both
        COMMAND ${CONFIGURE_CMD}
        COMMAND devenv ${COMPONENT_ROOT}/vsprojects/${COMPONENT}.sln /upgrade
        COMMAND devenv ${COMPONENT_ROOT}/vsprojects/${COMPONENT}.sln /Build Debug /Project "${COMPONENT_ROOT}/vsprojects/lib${COMPONENT}.vcxproj"
        COMMAND devenv ${COMPONENT_ROOT}/vsprojects/${COMPONENT}.sln /Build Release /Project "${COMPONENT_ROOT}/vsprojects/lib${COMPONENT}.vcxproj"
        COMMAND devenv ${COMPONENT_ROOT}/vsprojects/${COMPONENT}.sln /Build Debug /Project "${COMPONENT_ROOT}/vsprojects/protoc.vcxproj"
        COMMAND devenv ${COMPONENT_ROOT}/vsprojects/${COMPONENT}.sln /Build Release /Project "${COMPONENT_ROOT}/vsprojects/protoc.vcxproj"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDERS build
        DEPENDEES configure
    )  
      
    ExternalProject_Add_Step(${TARGET_NAME} install_out
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${COMPONENT_ROOT}/vsprojects/debug/lib${COMPONENT}.lib" "${THIRD_PARTY}/lib/debug/lib${COMPONENT}.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${COMPONENT_ROOT}/vsprojects/release/lib${COMPONENT}.lib" "${THIRD_PARTY}/lib/release/lib${COMPONENT}.lib"
        
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${COMPONENT_ROOT}/vsprojects/debug/libprotoc.lib" "${THIRD_PARTY}/lib/debug/libprotoc.lib"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${COMPONENT_ROOT}/vsprojects/release/libprotoc.lib" "${THIRD_PARTY}/lib/release/libprotoc.lib"
        
        COMMAND ${CMAKE_COMMAND} -E copy_if_different "${COMPONENT_ROOT}/vsprojects/release/protoc.exe" "${THIRD_PARTY}/bin/${COMPONENT}/protoc.exe"
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${COMPONENT_ROOT}/src/google/${COMPONENT}" "${THIRD_PARTY}/inc/google/${COMPONENT}"
        WORKING_DIRECTORY ${COMPONENT_ROOT}
        DEPENDEES build
    )  
endfunction()