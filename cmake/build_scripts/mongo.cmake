include(ExternalProject)

function(build_mongo url)
        
    set(MONGO_ROOT "${THIRD_PARTY_DOWNLOAD_DIR}/mongo")
    
    if(WIN32)
        if(MSVC12)
            set(CONFIGURE_COMMAND "$ENV{VS120COMNTOOLS}/vsvars32.bat")
        elseif(MSVC11)
            set(CONFIGURE_COMMAND "$ENV{VS110COMNTOOLS}/vsvars32.bat")
        elseif(MSVC10)
            set(CONFIGURE_COMMAND "$ENV{VS100COMNTOOLS}/vsvars32.bat")
        elseif(MSVC90)
            set(CONFIGURE_COMMAND "$ENV{VS90COMNTOOLS}/vsvars32.bat")
        elseif(MSVC80)
            set(CONFIGURE_COMMAND "$ENV{VS80COMNTOOLS}/vsvars32.bat")
        elseif(MSVC71)
            set(CONFIGURE_COMMAND "$ENV{VS71COMNTOOLS}/vsvars32.bat")
        else()
            message(FATAL_ERROR "Unknown/unsupported MSVC version " MSVC_VERSION)
        endif()
    else()
        message(FATAL_ERROR "Toolset for this compiler not resolved")
    endif()
                        
    set(BUILD_COMMAND 
        COMMAND call scons --use-system-boost --extrapath=${THIRD_PARTY} --dynamic-windows --dbg=on --32 --propagate-shell-environment --prefix=${THIRD_PARTY} mongoclient --full --variant-dir=debug
        COMMAND call scons --use-system-boost --extrapath=${THIRD_PARTY} --dynamic-windows --32 --propagate-shell-environment --prefix=${THIRD_PARTY} mongoclient --full --variant-dir=release
    )
    set(TARGET_NAME mongo_build)
    set(PATCH_CMD "")
    set(INSTALL_CMD
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${MONGO_ROOT}/src/mongo" "${THIRD_PARTY}/inc/mongo"
        COMMAND ${CMAKE_COMMAND} -E copy "${MONGO_ROOT}/build/debug/libmongoclient.lib" "${THIRD_PARTY}/lib/debug/libmongoclient.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${MONGO_ROOT}/build/release/libmongoclient.lib" "${THIRD_PARTY}/lib/release/libmongoclient.lib"
        COMMAND ${CMAKE_COMMAND} -E copy "${MONGO_ROOT}/build/release/mongo/base/error_codes.h" "${THIRD_PARTY}/inc/mongo/base/error_codes.h"
    )
    
    ExternalProject_Add(${TARGET_NAME}
        PREFIX mongo
        GIT_REPOSITORY "${url}"
        GIT_TAG "26compat"
        DOWNLOAD_DIR "${MONGO_ROOT}"
        SOURCE_DIR "${MONGO_ROOT}"
        PATCH_COMMAND "${PATCH_CMD}"
        CONFIGURE_COMMAND "${CONFIGURE_COMMAND}"
        BUILD_COMMAND "${BUILD_COMMAND}"
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND "${INSTALL_CMD}"
        CMAKE_ARGS -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
    ) 
    
    ExternalProject_Add_Step(${TARGET_NAME} prepare_boost
        COMMAND ${CMAKE_COMMAND} -E copy_directory "${THIRD_PARTY}/inc/boost" "${THIRD_PARTY}/include/boost"
        WORKING_DIRECTORY ${MONGO_ROOT}
        DEPENDERS build
    )     
    
    ExternalProject_Add_Step(${TARGET_NAME} clean
        COMMAND ${CMAKE_COMMAND} -E remove_directory "${THIRD_PARTY}/include"
        WORKING_DIRECTORY ${MONGO_ROOT}
        DEPENDEES install
    )     
    
    add_dependencies(${TARGET_NAME}
        boost_build
    )
endfunction()