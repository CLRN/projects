��������� �������� ��������� ��������� ������ � ������� ���������� ��� �������� ������.

����� ������: https://github.com/rpavlik/cmake-modules
�������� � ����������� ��� ��� ����� (��� ������ ������ - �� �����).

�������������:

# _�����_ add_library / add_executable

include(CreateLaunchers)
create_target_launcher(
    ${PROJECT_NAME}
    ARGS "--mode=true --filename=test.ptp --script=trace.config --package=SQLiteDBDriver.dll"
    WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)
