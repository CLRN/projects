include(parse_file_list)
include(get_config_list)

function(copy_to_output_dirs)
    parse_debug_optimized_list(_debugList _optimizedList _generalList ${ARGN})
    get_configuration_list(_debugConfigs _releaseConfigs)

    # �� _debugList ������ � �������� ������������
    foreach(item ${_debugList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${_debugConfigs})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Copying ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				configure_file("${item}" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" COPYONLY)
			endif()
        endforeach()
    endforeach()
    
    # �� _optimizedList ������ � �������� ������������
    foreach(item ${_optimizedList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${_releaseConfigs})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Copying ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				configure_file("${item}" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" COPYONLY)
			endif()
        endforeach()
    endforeach()
    
    # �� _generalList ������ �� ��� ������������
    foreach(item ${_generalList})
        get_filename_component(FILENAME "${item}" NAME)
        foreach(conf ${CMAKE_CONFIGURATION_TYPES})
			if (NOT EXISTS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				message(STATUS "Copying ${item} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}")
				configure_file("${item}" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${conf}/${FILENAME}" COPYONLY)
			endif()
        endforeach()
    endforeach()
endfunction()