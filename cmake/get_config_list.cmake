function(get_configuration_list _debugConfigs _releaseConfigs)
      
	set(debugConfigs "")
	foreach(conf ${CMAKE_CONFIGURATION_TYPES})
		if (conf STREQUAL "Debug")
			LIST(APPEND debugConfigs ${conf})
		endif()
	endforeach() 

	set(releaseConfigs "")
	foreach(conf ${CMAKE_CONFIGURATION_TYPES})
		if (conf STREQUAL "Release" OR conf STREQUAL "MinSizeRel" OR conf STREQUAL "RelWithDebInfo")
			LIST(APPEND releaseConfigs ${conf})
		endif()
	endforeach()
    
    set(${_debugConfigs} ${debugConfigs} PARENT_SCOPE)
    set(${_releaseConfigs} ${releaseConfigs} PARENT_SCOPE)
  
endfunction()