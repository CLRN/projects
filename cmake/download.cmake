
########################################
# Download file from remote FILE_URL location and extract it to ${CMAKE_BINARY_DIR}/<COMPONENT> folder
# Defines the following variables:
#
#   <COMPONENT>_DownloadPath - extracted <COMPONENT> directory(PARENT_SCOPE)
#
function(download_and_decompress)

    include(CMakeParseArguments)
    set(options "")
    set(oneValueArgs COMPONENT PATH FILE_URL)
    set(multiValueArgs "")        
    cmake_parse_arguments(_ARGS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(ExtractCompressedFileRegexp "(([^\\/]+)$)")
    string(REGEX MATCH ${ExtractCompressedFileRegexp} CompressedFileName ${_ARGS_FILE_URL})
    if(NOT CompressedFileName)
	    message(FATAL_ERROR "Can't obtain filename from URL: ${_ARGS_FILE_URL}")
    endif()

    set(DownloadedFile "${CMAKE_BINARY_DIR}/${CompressedFileName}")
    if(EXISTS "${DownloadedFile}")
        message("[${DownloadedFile}] already has been downloaded... skip")
    else()
        message("Downloading [${CompressedFileName}]")
        file(DOWNLOAD ${_ARGS_FILE_URL} ${DownloadedFile}
	        LOG FileDownloadLog
	        STATUS DownloadStatus
	        SHOW_PROGRESS)

        list(GET DownloadStatus 0 error_code )
        if(error_code)
            list(GET DownloadStatus 1 error_message)
            message(FATAL_ERROR "Failed to download file: ${error_message}"
       	        log: ${FileDownloadLog})
        endif()
        message(STATUS "downloading done")
    endif()
		
    set(OutDir "${_ARGS_PATH}")
    IF(EXISTS ${OutDir} AND IS_DIRECTORY ${OutDir})
        message("${OutDir} already has been created... skip")
    else()
        message("Create ${OutDir} directory")
        execute_process(
	        COMMAND ${CMAKE_COMMAND} -E make_directory ${OutDir}
        )
        message("UnZipping...")
        execute_process(
	        COMMAND ${CMAKE_COMMAND} -E tar xzf ${DownloadedFile}
	        WORKING_DIRECTORY ${OutDir}
	        RESULT_VARIABLE DecompressResult
        )
		
        if(${DecompressResult})
	        message(FATAL_ERROR "Failed with code: ${DecompressResult}")
        else()
	        message("Success!")
        endif()
    endif()

    set(${_ARGS_COMPONENT}_DownloadPath ${OutDir} PARENT_SCOPE)

endfunction()