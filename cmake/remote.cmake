function(download url fileName)
    if(EXISTS "${fileName}")
        message("[${fileName}] already has been downloaded... skip")
    else()
        message("Downloading [${url}] to [${fileName}]")
        file(DOWNLOAD ${url} ${fileName}
	        LOG FileDownloadLog
	        STATUS DownloadStatus
	        SHOW_PROGRESS)

        list(GET DownloadStatus 0 error_code)
        if(error_code)
            list(GET DownloadStatus 1 error_message)
            message(FATAL_ERROR "Failed to download file: ${error_message}"
       	        log: ${FileDownloadLog})
        endif()
        message(STATUS "Download completed")
    endif()
endfunction()

function(decompress fileName dir)
    IF(EXISTS ${dir} AND IS_DIRECTORY ${dir})
        message("${dir} already has been created... skip")
    else()
        message("Create ${dir} directory")
        execute_process(
	        COMMAND ${CMAKE_COMMAND} -E make_directory ${dir}
        )
        message("UnZipping [${fileName}] to [${dir}]")
        execute_process(
	        COMMAND ${CMAKE_COMMAND} -E tar xzf ${fileName}
	        WORKING_DIRECTORY ${dir}
	        RESULT_VARIABLE DecompressResult
        )
		
        if(${DecompressResult})
	        message(FATAL_ERROR "Failed with code: ${DecompressResult}")
        else()
	        message("Success!")
        endif()
    endif()
endfunction()    