#ifndef ServiceHolder_h__
#define ServiceHolder_h__

namespace boost
{
    namespace asio
    {
        class io_service;
    }
}

namespace cmn
{

class Service
{
public:

    class Holder
    {
    public:
        Holder(unsigned threads);
        ~Holder();
    };

    static boost::asio::io_service& Instance();
    static void AddWorkers(unsigned count);
    static void Wait();
};

} // namespace cmn

#endif // ServiceHolder_h__
