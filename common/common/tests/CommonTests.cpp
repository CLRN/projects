#include "common/Processes.h"
#include "common/CachingProxy.h"
#include "common/Log.h"
#include "common/Modules.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <boost/assign.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

using testing::Exactly;
using testing::AtLeast;
using testing::Return;
using testing::Expectation;

TEST(Process, Simple)
{
    const cmn::Process current = cmn::Process::Current();

    bool found = false;
    for (const auto& process : cmn::Process::GetAll())
    {
        if (process.GetId() == current.GetId() && process.GetName() == current.GetName())
        {
            found = true;
            break;
        }
    }

    EXPECT_TRUE(found);
}

class MockedCallback
{
public:
    MOCK_METHOD0(GetValue, int(void));
    MOCK_METHOD0(ValueOutdated, void());
};

TEST(Caching, Simple)
{
    MockedCallback callback;

    static int counter = 0;
    volatile bool outdated = false;

    const auto proxy = cmn::cache::Factory<int>::Instance(
        boost::bind(&MockedCallback::GetValue, &callback),
        boost::bind(&MockedCallback::ValueOutdated, &callback), 
        boost::posix_time::milliseconds(100), 
        boost::posix_time::milliseconds(200)
    );

    EXPECT_CALL(callback, GetValue()).Times(AtLeast(2)).WillOnce(Return(1)).WillRepeatedly(Return(100));
    EXPECT_CALL(callback, ValueOutdated()).Times(Exactly(1));

    // expect that cached value will be returned 
    for (unsigned i = 0 ; i < 100; ++i)
        EXPECT_EQ(int(*proxy), 1);

    // wait for out date interval
    cmn::Service::Instance().poll();
    boost::this_thread::interruptible_wait(250);
    cmn::Service::Instance().poll();

    EXPECT_EQ(int(*proxy), 100);
    cmn::Service::Instance().poll();
}

TEST(Logging, FormattedString)
{
    Log log;
    log.Open(ILog::Level::Trace);

    LERROR(log, Modules::Tools, "test%1%2");
}