#ifndef FileSystem_h__
#define FileSystem_h__

#include <string>
#include <list>

#include <boost/filesystem/path.hpp>

namespace fs
{
    std::list<boost::filesystem::path> GetFiles(const boost::filesystem::path& folder, const boost::filesystem::path& extension);

    //! Scoped file
    class ScopedFile
    {
    public:
        ScopedFile(const char* ext = nullptr);
        ScopedFile(const std::string& name);
        ScopedFile(const std::wstring& name);
        operator const std::wstring& () const { return m_Path; }
        ~ScopedFile();
    private:
        std::wstring m_Path;
    };


} // namespace fs


#endif // FileSystem_h__