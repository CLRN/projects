#include "FileSystem.h"
#include "GUID.h"
#include "conversion/AnyCast.h"

#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

namespace bfs = boost::filesystem;

namespace fs
{

ScopedFile::ScopedFile(const std::string& name)
{
    m_Path = bfs::system_complete(name).wstring();
}

ScopedFile::ScopedFile(const std::wstring& name)
{
    m_Path = bfs::system_complete(name).wstring();
}

ScopedFile::ScopedFile(const char* ext /*= nullptr*/)
     : m_Path((boost::filesystem::temp_directory_path() / cmn::GUID::Generate()).wstring())
{
    if (ext)
        m_Path += conv::cast<std::wstring>(ext);
}

ScopedFile::~ScopedFile()
{
    bfs::remove(m_Path);
}

std::list<boost::filesystem::path> GetFiles(const boost::filesystem::path& folder, const boost::filesystem::path& extension)
{
    std::list<boost::filesystem::path> result;
    if (!boost::filesystem::is_directory(folder) && boost::filesystem::path(folder).extension() == extension)
    {
        result.push_back(folder);
        return result;
    }

    boost::filesystem::directory_iterator it(folder);
    const boost::filesystem::directory_iterator end;

    for (; it != end; ++it)
    {
        if (it->path().extension() == extension)
            result.push_back(it->path());
    }
    result.sort([](const boost::filesystem::path& lhs, const boost::filesystem::path& rhs){
        try
        {
            return conv::cast<unsigned>(lhs.filename().replace_extension().string()) < conv::cast<unsigned>(rhs.filename().replace_extension().string());
        }
        catch (const conv::CastException&)
        {
            return false;
        }
    });

    if (result.empty())
    {
        boost::filesystem::directory_iterator it(folder);
        for (; it != end; ++it)
        {
            if (boost::filesystem::is_directory(it->path()))
            {
                const auto temp = GetFiles(it->path(), extension);
                boost::copy(temp, std::back_inserter(result));
            }
        }
    }

    return result;
}

} // namespace fs

