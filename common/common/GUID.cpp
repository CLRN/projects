#include "GUID.h"

#include "conversion/AnyCast.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/uuid/string_generator.hpp>

namespace cmn
{
GUID::GUID()
{
}

GUID::~GUID()
{
}

std::string GUID::Generate()
{
    const boost::uuids::uuid u = boost::uuids::random_generator()();
    return conv::cast<std::string>(u);
}

bool GUID::IsValid(const std::string& guid)
{
    boost::uuids::string_generator gen;
    try
    {
        return gen(guid) != boost::uuids::uuid();
    }
    catch (const std::runtime_error&)
    {
        return false;
    }
}

} // namespace cmn
