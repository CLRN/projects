#ifndef Process_h__
#define Process_h__

#include <string>
#include <memory>

#include <boost/iterator/iterator_facade.hpp>
#include <boost/function/function_fwd.hpp>

namespace cmn
{

class Process
{
public:

    struct Data;
    typedef unsigned long Id;
    typedef void* Handle;
    typedef boost::function<void()> WaitCallback;

    Process();
    Process(Id id);
    Process(const Data& data);
    
    class Iterator : public boost::iterator_facade<Iterator, const Process&, boost::forward_traversal_tag>
    {
        friend class boost::iterator_core_access;
    public:
        Iterator(Handle h);
        Iterator(const Iterator& other);
        ~Iterator();

        Iterator& operator = (const Iterator& other);

        void Swap(Iterator& other);
        void increment();
        bool equal(const Iterator& other ) const;
        const Process& dereference() const;
    private:
        Handle m_Snapshot;
        Data* m_Entry;
    };

    class Snapshot
    {
    public:
        typedef Iterator const_iterator;
        Snapshot(const Handle h);
        ~Snapshot();

        Iterator begin() const;
        Iterator end() const;
    private:
        Handle m_Snapshot;
    };

    cmn::Process::Id GetId() const { return m_Id; }
    const std::wstring& GetName() const { return m_Name; }
    void Wait(const WaitCallback& cb);

    static Snapshot GetAll();
    static Process Current();

private:

    Id m_Id;
    std::wstring m_Name;
};


} // namespace cmn

#endif // Process_h__
