#ifndef ILog_h__
#define ILog_h__

#include "conversion/AnyCast.h"

#include <string>
#include <vector>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>

//! Scoped log macro
#define SCOPED_LOG(log) const ILog::ScopedLogPtr __scopedLogPtr##__LINE__ = log.MakeScopedLog(CURRENT_MODULE_ID, __FUNCTION__);

//! Logging macro
#define LOG_MACRO(logger, text, level, module)                                                      \
    logging::MessageFormatter(logger, module, level, text, __FILE__, __LINE__, __FUNCTION__) 

#define LINFO(logger, module, message)		if (logger.IsEnabled(module, ILog::Level::Info))		\
    LOG_MACRO(logger, message, ILog::Level::Info, module)
#define LERROR(logger, module, message)		if (logger.IsEnabled(module, ILog::Level::Error))		\
    LOG_MACRO(logger, message, ILog::Level::Error, module)
#define LWARNING(logger, module, message)	if (logger.IsEnabled(module, ILog::Level::Warning))	    \
    LOG_MACRO(logger, message, ILog::Level::Warning, module)
#define LTRACE(logger, module, message)		if (logger.IsEnabled(module, ILog::Level::Trace))		\
    LOG_MACRO(logger, message, ILog::Level::Trace, module)
#define LDEBUG(logger, module, message)		if (logger.IsEnabled(module, ILog::Level::Debug))		\
    LOG_MACRO(logger, message, ILog::Level::Debug, module)

#define LOG_INFO(message) \
    LINFO(m_Log, CURRENT_MODULE_ID, message)
#define LOG_ERROR(message) \
	LERROR(m_Log, CURRENT_MODULE_ID, message)
#define LOG_WARNING(message) \
	LWARNING(m_Log, CURRENT_MODULE_ID, message)
#define LOG_TRACE(message) \
	LTRACE(m_Log, CURRENT_MODULE_ID, message)
#define LOG_DEBUG(message) \
	LDEBUG(m_Log, CURRENT_MODULE_ID, message)

//! Forward declarations
class ScopedLog;
class LogAnyHolder;

//! Logger interface
//!
//! \class ILog
//!
class ILog
{
public:

	//! Scoped log ptr type
	typedef boost::shared_ptr<ScopedLog> ScopedLogPtr;

	//! Log levels
	struct Level
	{
		enum Value
		{
			Info	= 0,
			Error	= 1,
			Warning	= 2,
			Trace	= 3,
			Debug	= 4
		};
	};

	//! Open log configuration file
	virtual void		Open(const std::string& src) = 0;

    //! Open log with default configuration
    virtual void		Open(Level::Value lvl) = 0;

    //! Enable module
    virtual void        Enable(unsigned int module) = 0;

    //! Disable module
    virtual void        Disable(unsigned int module) = 0;

    //! Enable all modules
    virtual void        EnableAll() = 0;

    //! Disable all modules
    virtual void        DisableAll() = 0;

	//! Close log
	virtual void		Close() = 0;

	//! Is logging enabled
	virtual bool		IsEnabled(unsigned int module, const Level::Value level) const = 0;

    //! Get module log folder
    virtual std::wstring GetLogFolder(unsigned int module) const = 0;

	//! Write text
	virtual ILog&		Write(unsigned int module, ILog::Level::Value level, const std::string& text, const char* file, const unsigned line, const char* function)  = 0;

	//! Make scoped log
	virtual ScopedLogPtr MakeScopedLog(unsigned int module, const std::string& func)	= 0;

	//! Destructor
	virtual ~ILog() {}
};

namespace logging
{
    class MessageFormatter
    {
    public:
        MessageFormatter(ILog& log, unsigned module, ILog::Level::Value level, const std::string& text, const char* file, unsigned line, const char* function) 
            : m_Log(log) 
            , m_Module(module)
            , m_Level(level)
            , m_Text(text)
            , m_Format(text)
            , m_File(file)
            , m_Line(line)
            , m_Function(function)
        {}
        ~MessageFormatter()
        {
            m_Log.Write(m_Module, m_Level, m_Format.fed_args() ? m_Format.str() : m_Text, m_File, m_Line, m_Function);
        }
        template<typename T>
        MessageFormatter& operator % (const T& value)
        {
            m_Format % value;
            return *this;
        }
        MessageFormatter& operator % (const std::wstring& value)
        {
            m_Format % conv::cast<std::string>(value);
            return *this;
        }
        template<typename T>
        MessageFormatter& operator % (const std::vector<T>& value)
        {
            WriteSequence(value);
            return *this;
        }
        template<typename T>
        MessageFormatter& operator % (const std::deque<T>& value)
        {
            WriteSequence(value);
            return *this;
        }
        template<typename T>
        MessageFormatter& operator % (const std::set<T>& value)
        {
            WriteSequence(value);
            return *this;
        }
        template<typename K, typename V, typename A>
        MessageFormatter& operator % (const std::map<K, V, A>& arg)
        {
            std::ostringstream oss;
            auto it = arg.begin();
            auto end = arg.end();
            for (; it != end; ++it)
            {
                if (it != arg.begin())
                    oss << ",";
                oss << it->first << ":" << it->second;
            }

            m_Format % oss.str();
            return *this;
        }
        template<typename T>
        void WriteSequence(const T& arg)
        {
            std::ostringstream oss;
            auto it = arg.begin();
            auto end = arg.end();
            for (; it != end; ++it)
            {
                if (it != arg.begin())
                    oss << ",";
                oss << *it;
            }

            m_Format % oss.str();
        }
    private:
        ILog& m_Log;
        const unsigned m_Module;
        const ILog::Level::Value m_Level;
        const char* const m_File;
        const unsigned m_Line;
        const char* const m_Function;
        boost::format m_Format;
        const std::string& m_Text;
    };
}

#endif // ILog_h__