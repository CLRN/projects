#include "Log.h"
#include "Modules.h"

#include <fstream>

#include <log4cplus/logger.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/configurator.h>
#include <log4cplus/fileappender.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/hierarchy.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/thread/thread.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/range/algorithm.hpp>

namespace fs = boost::filesystem;

//! Class for scoped logging
class ScopedLog : boost::noncopyable
{
public:
	ScopedLog(ILog& Logger, const std::string& functionName, unsigned int module) 
		: m_Log(Logger)
		, m_FunctionName(functionName)
		, m_ThreadId(boost::this_thread::get_id())
		, m_Module(module)
	{
		std::wstring indent;
		{
			boost::mutex::scoped_lock Lock(s_mxMap);
			indent.assign(++s_mapThreads[m_ThreadId], L'-');
		}

        logging::MessageFormatter(
            m_Log, 
            m_Module, 
            ILog::Level::Trace, 
            " -%s> [%s]", 
            __FILE__, 
            __LINE__, 
            ""
        ) % indent % m_FunctionName;
	}

	~ScopedLog()
	{
		std::wstring indent;
		{
			boost::mutex::scoped_lock Lock(s_mxMap);
			indent.assign(s_mapThreads[m_ThreadId]--, L'-');
		}

        logging::MessageFormatter(
            m_Log, 
            m_Module, 
            ILog::Level::Trace,
            " <-%s [%s]", 
            __FILE__, 
            __LINE__, 
            ""
        ) % indent % m_FunctionName;
	}
private:

	typedef std::map<boost::thread::id, std::size_t> ThreadMap;
	ILog&					m_Log;
	const std::string		m_FunctionName;
	boost::thread::id   	m_ThreadId;
	static ThreadMap		s_mapThreads;
	static boost::mutex		s_mxMap;
	unsigned int			m_Module;
};

ScopedLog::ThreadMap 	ScopedLog::s_mapThreads;
boost::mutex		 	ScopedLog::s_mxMap;

log4cplus::LogLevel GetLevel(ILog::Level::Value v)
{
    switch (v)
    {
        case ILog::Level::Info:     return log4cplus::INFO_LOG_LEVEL;
        case ILog::Level::Error:    return log4cplus::ERROR_LOG_LEVEL;
        case ILog::Level::Warning:  return log4cplus::WARN_LOG_LEVEL;
        case ILog::Level::Trace:    return log4cplus::TRACE_LOG_LEVEL;
        case ILog::Level::Debug:    return log4cplus::DEBUG_LOG_LEVEL;
        default: assert(!"wrong log level"); return 0;
    }
}

std::string GetModuleName(const unsigned id)
{
    return Modules(static_cast<Modules::domain>(id)).str();
}

#pragma warning (push)
#pragma warning (disable : 4822) // warning C4822: local class member function does not have a body

// ������� ��� ����� �������� ��� ����� �� ���������
class FileNameGetter : public log4cplus::FileAppender
{
    FileNameGetter();
public:
    const log4cplus::tstring GetFileName() const { return filename; }
};
#pragma warning (pop)

Log::Log() : m_IsOpened(), m_Counter()
{
}

Log::~Log()
{
	Close();
}

void Log::Open(const std::string& src)
{
    namespace fs = boost::filesystem;

    const fs::path config_file = fs::current_path() / src;

    if (fs::is_regular_file(config_file))
    {
        log4cplus::PropertyConfigurator cfg(conv::cast<log4cplus::tstring>(config_file.wstring()));
        cfg.configure();
        
        // create log folders
        std::set<fs::path> folders;

        {
            log4cplus::LoggerList loggers = log4cplus::Logger::getCurrentLoggers();
            for (auto& logger : loggers)
            {
                for (const auto& appender : logger.getAllAppenders())
                {
                    const log4cplus::FileAppender* fileAppender = dynamic_cast<const log4cplus::FileAppender*>(appender.get());
                    if (fileAppender)
                    {
                        const FileNameGetter& getter = static_cast<const FileNameGetter&>(*fileAppender);
                        const auto path = fs::system_complete(getter.GetFileName()).branch_path();
                        if (!path.empty())
                            folders.insert(path);
                    }
                }
            }
        }

        boost::for_each(folders, boost::bind(fs::create_directories, _1));

        // reconfigure
        cfg.configure();

        m_IsOpened = true;
    }
}

void Log::Open(Level::Value lvl)
{
    log4cplus::BasicConfigurator config;
    config.configure();

    // initialize all loggers
    for (const auto& module : Modules())
        log4cplus::Logger::getInstance(conv::cast<log4cplus::tstring>(module.str()));

    log4cplus::LoggerList loggers = log4cplus::Logger::getCurrentLoggers();
    for (auto& logger : loggers)
        logger.getRoot().setLogLevel(GetLevel(lvl));

    m_IsOpened = true;
}

std::wstring Log::GetLogFolder(unsigned int module) const
{
    log4cplus::LoggerList loggers = log4cplus::Logger::getCurrentLoggers();
    for (auto& logger : loggers)
    {
        for (const auto& appender : logger.getAllAppenders())
        {
            const log4cplus::FileAppender* fileAppender = dynamic_cast<const log4cplus::FileAppender*>(appender.get());
            if (fileAppender)
            {
                const FileNameGetter& getter = static_cast<const FileNameGetter&>(*fileAppender);
                return fs::system_complete(getter.GetFileName()).branch_path().wstring();
            }
        }
    }
    THROW(cmn::Exception("Unable to get log folder"));
}

void Log::Close()
{
    m_IsOpened = false;
}

bool Log::IsEnabled(unsigned int module, const Level::Value level) const
{
    if (!m_IsOpened)
        return false;

    const auto logger = log4cplus::detail::macros_get_logger(GetModuleName(module)); 
    return logger.isEnabledFor(GetLevel(level));
}

ILog& Log::Write(unsigned int module, ILog::Level::Value level, const std::string& text, const char* file, const unsigned line, const char* function)
{
    const auto logger = log4cplus::detail::macros_get_logger(GetModuleName(module)); 
    const auto logLevel = GetLevel(level);
    if (!logger.isEnabledFor(logLevel))
        return *this;

    ++m_Counter;
    log4cplus::detail::macro_forced_log(logger, logLevel, text, file, line, function);
    return *this;
}

ILog::ScopedLogPtr Log::MakeScopedLog(unsigned int module, const std::string& func)
{
	if (!m_IsOpened || !IsEnabled(module, Level::Debug))
		return ScopedLogPtr();

	return ScopedLogPtr(new ScopedLog(*this, func, module));
}

void Log::Enable(unsigned int module)
{
    const auto logger = log4cplus::detail::macros_get_logger(GetModuleName(module)); 
    logger.getHierarchy().enableAll();
}

void Log::Disable(unsigned int module)
{
    const auto logger = log4cplus::detail::macros_get_logger(GetModuleName(module)); 
    logger.getHierarchy().disableAll();
}

void Log::EnableAll()
{
    log4cplus::getDefaultHierarchy().enableAll();
}

void Log::DisableAll()
{
    log4cplus::getDefaultHierarchy().disableAll();
}
