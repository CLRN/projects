#ifndef Window_h__
#define Window_h__

#include <vector>

namespace cmn
{

class Window
{
public:
    typedef unsigned Id;
    typedef std::vector<Window> List;
    
    static List GetAll();

    Window(Id id = 0);

    std::string ClassName() const;
    Id GetId() const { return m_Id; }
    std::wstring Title() const;

private:

    Id m_Id;
};

} // namespace cmn

#endif // Window_h__
