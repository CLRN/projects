#include "Processes.h"
#include "exception/CheckHelpers.h"
#include "common/ServiceHolder.h"

#include <windows.h>
#include <tlhelp32.h>

#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/asio/windows/object_handle.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>

namespace cmn
{

Process::Snapshot::Snapshot(const Handle h) : m_Snapshot(h)
{

}

Process::Snapshot::~Snapshot()
{
    assert(CloseHandle(m_Snapshot));
}

Process::Iterator Process::Snapshot::begin() const
{
    return Iterator(m_Snapshot);
}

Process::Iterator Process::Snapshot::end() const
{
    return Iterator(NULL);
}

struct Process::Data
{
    Data()
    {
        memset(&m_Entry, 0, sizeof(PROCESSENTRY32W));
        m_Entry.dwSize = sizeof(PROCESSENTRY32W);
    }
    PROCESSENTRY32W m_Entry;
    Process m_Process;
};

Process::Iterator::Iterator(Handle h) : m_Snapshot(h), m_Entry(new Data())
{
    if (m_Snapshot)
        CHECK_LE(Process32FirstW(m_Snapshot, &m_Entry->m_Entry), cmn::Exception("Failed to process entry"));
}

Process::Iterator::Iterator(const Iterator& other) : m_Snapshot(other.m_Snapshot), m_Entry(new Data(*other.m_Entry)) 
{

}

Process::Iterator::~Iterator()
{
    delete m_Entry;
}

void Process::Iterator::increment()
{
    if (!Process32NextW(m_Snapshot, &m_Entry->m_Entry))
    {
        m_Snapshot = NULL;
        m_Entry->m_Process = Process();
    }
    else
    {
        m_Entry->m_Process = Process(*m_Entry);
    }
}

bool Process::Iterator::equal(const Iterator& other) const
{
    return !m_Snapshot && !other.m_Snapshot;
}

const Process& Process::Iterator::dereference() const
{
    return m_Entry->m_Process;
}

Process::Iterator& Process::Iterator::operator = (const Iterator& other)
{
    Iterator copy(other);    
    copy.Swap(*this);
    return *this;
}

void Process::Iterator::Swap(Iterator& other)
{
    std::swap(m_Snapshot, other.m_Snapshot);
    std::swap(m_Entry, other.m_Entry);
}

Process::Snapshot Process::GetAll()
{
    const HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    CHECK_LE(snapshot, cmn::Exception("Failed to create snapshot"));

    return Snapshot(snapshot);
}

Process::Process(const Data& data)
    : m_Id(data.m_Entry.th32ProcessID)
    , m_Name(data.m_Entry.szExeFile)
{
   
}

Process::Process() : m_Id(), m_Name()
{

}

Process::Process(Id id) : m_Id(id), m_Name()
{

}

cmn::Process Process::Current()
{
    const auto range = GetAll();
    const auto current = boost::find_if(range, boost::bind(&Process::GetId, _1) == GetCurrentProcessId());
    assert(current != boost::end(range));

    return *current;
}

void Process::Wait(const WaitCallback& cb)
{
    assert(m_Id);

    const HANDLE process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_Id);
    CHECK_LE(process, cmn::Exception("Failed to open process"), m_Id);

    const auto waiter = boost::make_shared<boost::asio::windows::object_handle>(cmn::Service::Instance(), process);
    waiter->async_wait([waiter, cb](const boost::system::error_code&){
        cb();
    });
}

} // namespace cmn