#ifndef StreamHelpers_h__
#define StreamHelpers_h__

#include <iosfwd>

#include <boost/iostreams/categories.hpp>
#include <boost/cstdint.hpp>

namespace cmn
{

void CopyStream(std::istream& is, std::ostream& os);
void Compress(std::istream& src, std::ostream& dst);
void Decompress(std::istream& src, std::ostream& dst);
boost::uint32_t StreamSize(std::istream& is);


class SizedStream
{
public:
    typedef char char_type;
    typedef boost::iostreams::source_tag category;

    SizedStream(std::istream& s, boost::uint32_t size);

    std::streamsize read(char_type* s, std::streamsize n);

private:
    std::istream& m_Stream;
    const boost::uint32_t m_Size;
    boost::uint32_t m_Read;
};

} // namespace cmn

#endif // StreamHelpers_h__
