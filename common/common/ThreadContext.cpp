#include "ThreadContext.h"

#include <boost/thread.hpp>

namespace cmn
{
namespace ctx
{

class Storage::Impl
{
public:

    boost::any& Current()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        return m_Context[boost::this_thread::get_id()];
    }

    void Reset()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_Context.erase(boost::this_thread::get_id());
    }

private:
    boost::mutex m_Mutex;
    std::map<boost::thread::id, boost::any> m_Context;
};

boost::any& Storage::Current()
{
    static boost::mutex mutex;
    boost::unique_lock<boost::mutex> lock(mutex);

    if (!s_Instance)
        s_Instance.reset(new Impl());
    return s_Instance->Current();
}

void Storage::Reset()
{
    Current();
    s_Instance->Reset();
}

std::unique_ptr<Storage::Impl> Storage::s_Instance;


Scoped::Scoped()
{
    Storage::Reset();
}

Scoped::~Scoped()
{
    Storage::Reset();
}

} // namespace ctx
} // namespace cmn