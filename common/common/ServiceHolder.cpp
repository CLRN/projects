#include "ServiceHolder.h"
#include "exception/CheckHelpers.h"

#include <boost/asio/signal_set.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

namespace cmn
{

class Pool
{
    //! Thread shared pointer
    typedef boost::shared_ptr<boost::thread>    ThreadPtr;

    //! Pointer of the thread pointer
    typedef boost::shared_ptr<ThreadPtr>        PtrOfTheThreadPtr;

    Pool()
    {

    }

public:

    void AddWorkers(unsigned count)
    {
        for (std::size_t i = m_Pool.size(); i < count; ++i)
        {
            const PtrOfTheThreadPtr threadPtr(new ThreadPtr());
            threadPtr->reset(m_Pool.create_thread(boost::bind(&Pool::ThreadFunc, this, threadPtr)));	
        }
    }

    void Wait()
    {
        while (m_Pool.size())
            boost::this_thread::interruptible_wait(100);
    }

    static Pool& Instance()
    {
        static Pool instance;
        return instance;
    }

private:

    void ThreadFunc(const PtrOfTheThreadPtr& ptr)
    {
        boost::this_thread::at_thread_exit(boost::bind(&Pool::RemoveFromPool, this, *ptr));
        boost::system::error_code e;

        try
        {
            Service::Instance().run(e);
        }
        catch (const std::exception& e)
        {
            const std::wstring message = cmn::ExceptionInfo(e);
            assert(false);
        }
    }

    void RemoveFromPool(const ThreadPtr& threadPtr)
    {
        m_Pool.remove_thread(threadPtr.get());
    }

private:
    boost::thread_group m_Pool;
};

class ServiceInstance
{
public:

    ServiceInstance()
        : m_Service()
        , m_Signals(m_Service)
        , m_Work(m_Service)
    {
        m_Signals.add(SIGINT);
        m_Signals.add(SIGTERM);

#if defined(SIGQUIT)
        m_Signals.add(SIGQUIT);
#endif // defined(SIGQUIT)

        m_Signals.async_wait(boost::bind(&boost::asio::io_service::stop, &m_Service));
    }

    boost::asio::io_service& GetService()
    {
        return m_Service;
    }

private:
    boost::asio::io_service m_Service;
    boost::asio::signal_set m_Signals;
    boost::asio::io_service::work m_Work;
};

boost::asio::io_service& Service::Instance()
{
    static ServiceInstance srvc;
    return srvc.GetService();
}

void Service::AddWorkers(unsigned count)
{
    Pool::Instance().AddWorkers(count);
}

void Service::Wait()
{
    Pool::Instance().Wait();
}


Service::Holder::Holder(unsigned threads)
{
    Service::Instance().reset();
    Service::AddWorkers(threads);
}

Service::Holder::~Holder()
{
    Service::Instance().stop();
    Service::Wait();
    Service::Instance().reset();
}

} // namespace cmn