#ifndef Modules_h__
#define Modules_h__

#include <boost/enum.hpp>

BOOST_ENUM(Modules,
    (Events)
    (Table)
    (Client)
    (Network)
    (Rpc)
    (Tools)
    (DataBase)
    (Server)
    (NeuroNet)
    (Teacher)
    (Logic)
    (Cache)
    (Common)
)

#define DECLARE_CURRENT_MODULE(name) const unsigned CURRENT_MODULE_ID = name;

#endif // Modules_h__
