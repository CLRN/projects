set(PROJECT_NAME lib_common)

file(GLOB SOURCES "*.cpp" "*.h")

add_library(${PROJECT_NAME} STATIC ${SOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "common")
target_link_libraries(${PROJECT_NAME}
	lib_exception
	log4cplus
    
    ws2_32 # log4cplus requires this
)

add_subdirectory(tests)
