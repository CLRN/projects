#include "StreamHelpers.h"

#include <iostream>


#pragma warning(push)
#pragma warning(disable:4244) // warning C4244: '=' : conversion from 'std::streamsize' to 'size_t', possible loss of data
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/copy.hpp>
#pragma warning(pop)

namespace cmn
{

void CopyStream(std::istream& is, std::ostream& os)
{
    // write stream data if available
    char buffer[4096];
    std::size_t sent = 0;
    for (;;)
    {
        is.read(buffer, _countof(buffer));

        const std::size_t read = static_cast<std::size_t>(is.gcount());
        os.write(buffer, read);
        sent += read;
        if (read != _countof(buffer))
            break;
    }
}

void Compress(std::istream& src, std::ostream& dst)
{
    boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
    in.push(boost::iostreams::bzip2_compressor());
    in.push(src);
    boost::iostreams::copy(in, dst);
}

void Decompress(std::istream& src, std::ostream& dst)
{
    boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
    in.push(boost::iostreams::bzip2_decompressor());
    in.push(src);
    boost::iostreams::copy(in, dst);
}


SizedStream::SizedStream(std::istream& s, boost::uint32_t size) 
    : m_Stream(s)
    , m_Size(size)
    , m_Read()
{

}

std::streamsize SizedStream::read(char_type* s, std::streamsize n)
{
    if (m_Read + n >= m_Size)
    {
        // this is last block data
        n = m_Size - m_Read;
        m_Stream.read(s, n);
    }
    else
    {
        m_Stream.read(s, n);
    }

    m_Read += static_cast<boost::uint32_t>(n);
    return n;
}

boost::uint32_t StreamSize(std::istream& is)
{
    const auto pos = is.tellg();
    is.seekg(0, std::ios::end);

    const boost::uint32_t result = static_cast<boost::uint32_t>(is.tellg() - pos);

    is.seekg(pos);
    return result;
}

} // namespace cmn


