#include "common/Window.h"
#include "exception/CheckHelpers.h"

#include <windows.h>

namespace cmn
{

Window::Window(Id id) : m_Id(id)
{

}

Window::List Window::GetAll()
{
    List windows;

    auto callback = [](HWND hwnd, LPARAM lParam) -> BOOL
    {
        reinterpret_cast<List*>(lParam)->push_back(Window(reinterpret_cast<Id>(hwnd)));
        return TRUE;
    };

    CHECK_LE(EnumWindows(callback, reinterpret_cast<LPARAM>(&windows)), cmn::Exception("Failed to enumerate windows"));

    return windows;
}

std::string Window::ClassName() const
{
    CHECK(m_Id, cmn::Exception("Window is not initialized"));
    char name[512] = {0};
    CHECK_LE(GetClassNameA(reinterpret_cast<HWND>(m_Id), name, _countof(name)), cmn::Exception("Failed to get window class"));
    return name;
}

std::wstring Window::Title() const
{
    wchar_t name[512] = {0};
    CHECK_LE(GetWindowTextW(reinterpret_cast<HWND>(m_Id), name, _countof(name)), cmn::Exception("Failed to get window title"));
    return name;
}


} // namespace cmn