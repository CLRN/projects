#ifndef ThreadContext_h__
#define ThreadContext_h__

#include <memory>

#include <boost/any.hpp>

namespace cmn
{
namespace ctx
{

class Storage
{
public:
    static boost::any& Current();
    static void Reset();
private:
    class Impl;
    static std::unique_ptr<Impl> s_Instance;
};

class Scoped
{
public:
    Scoped();
    ~Scoped();
};

} // namespace ctx
} // namespace cmn

#endif // ThreadContext_h__
