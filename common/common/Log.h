#ifndef Log_h__
#define Log_h__

#include "common/ILog.h"

#include <ostream>
#include <vector>

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

//! Logger interface
//!
//! \class Log
//!
class Log : public ILog
{

public:
	Log();
	~Log();

    virtual void Open(const std::string& src) override;
    virtual void Open(Level::Value lvl) override;
    virtual void Enable(unsigned int module) override;
    virtual void Disable(unsigned int module) override;
    virtual void EnableAll() override;
    virtual void DisableAll() override;
	virtual void Close() override;
	virtual bool IsEnabled(unsigned int module, const Level::Value level) const override;
    virtual std::wstring GetLogFolder(unsigned int module) const override;
    virtual ILog& Write(unsigned int module, ILog::Level::Value level, const std::string& text, const char* file, const unsigned line, const char* function) override;
	ScopedLogPtr MakeScopedLog(unsigned int module, const std::string& szFunction) override;

private:
    bool m_IsOpened;
    unsigned m_Counter;
};

#endif // Log_h__
