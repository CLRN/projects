#pragma once

#include "exception/CheckHelpers.h"
#include "common/ServiceHolder.h"
#include "common/ILog.h"
#include "common/Modules.h"

#include <atomic>

#include <boost/asio/io_service.hpp>
#include <boost/chrono/time_point.hpp>
#include <boost/thread/future.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/recursive_mutex.hpp>

//! Poll wrapper with logging and error handling
template<typename Condition>
void PollUntil(ILog& m_Log, const Condition& condition, const unsigned timeoutMS = 0)
{
    boost::chrono::system_clock::time_point start;
    if (timeoutMS)
        start = boost::chrono::system_clock::now();

    while (!condition())
    {
        if (timeoutMS)
        {
            CHECK(boost::chrono::duration_cast<boost::chrono::milliseconds>(
                    boost::chrono::system_clock::now() - start
                ).count() <= timeoutMS,
                cmn::Exception("operation timed out"), 
                timeoutMS
            );
        }

        try
        {
            const std::size_t polled = cmn::Service::Instance().poll_one();
            if (!polled)
                boost::this_thread::interruptible_wait(1);
        }
        catch (const std::exception& e)
        {
            DECLARE_CURRENT_MODULE(Modules::Common);
            LOG_ERROR("failed to poll request: %s") % cmn::ExceptionInfo(e);
        }
    }
}

//! Poll wrapper without logging and error handling
template<typename Condition>
void PollUntil(const Condition& condition, const unsigned timeoutMS = 0)
{
    boost::chrono::system_clock::time_point start;
    if (timeoutMS)
        start = boost::chrono::system_clock::now();

    while (!condition())
    {
        if (timeoutMS)
        {
            CHECK(boost::chrono::duration_cast<boost::chrono::milliseconds>(
                boost::chrono::system_clock::now() - start
                ).count() <= timeoutMS,
                cmn::Exception("operation timed out"), 
                timeoutMS
            );
        }

        try
        {
            const std::size_t polled = cmn::Service::Instance().poll_one();
            if (!polled)
                boost::this_thread::interruptible_wait(1);
        }
        CATCH_PASS(cmn::Exception("failed to poll request"));
    }
}

//! Wait for future completion
template<typename T>
void PollUntil(ILog& log, boost::unique_future<T>& future, const unsigned timeoutMS = 0)
{
    PollUntil(log, boost::bind(&boost::unique_future<T>::is_ready, &future), timeoutMS);
}

//! Wait for future completion
template<typename T>
void PollUntil(boost::unique_future<T>& future, const unsigned timeoutMS = 0)
{
    PollUntil(boost::bind(&boost::unique_future<T>::is_ready, &future), timeoutMS);
}

//! Callback guard 
class CallbackGuard : boost::noncopyable, public boost::enable_shared_from_this<CallbackGuard>
{
public:
    typedef std::atomic<std::size_t> Counter;
    typedef boost::shared_ptr<CallbackGuard> Ptr;

    CallbackGuard(ILog& log) : m_Log(log) {}

    template<typename T>
    class Handler
    {
        Handler();
        class CounterHolder : boost::noncopyable // implements auto decrement on out of scope
        {
        public:
            CounterHolder(const CallbackGuard::Ptr& guard) : m_Guard(guard) { m_Guard->OnHandlerStarted(); }
            ~CounterHolder() { m_Guard->OnHandlerCompleted(); }
        private:
            const CallbackGuard::Ptr m_Guard;
        };
    public:
        Handler(const T& h, const CallbackGuard::Ptr& guard) : m_Handler(h), m_Guard(guard)
        {
            ++m_Guard->m_Counter;
        }

        void operator()()
        {
            CounterHolder holder(m_Guard);
            if (!m_Guard->m_IsStopped)
                m_Handler();
        }

        template <typename Arg1>
        void operator()(Arg1 arg1)
        {
            CounterHolder holder(m_Guard);
            if (!m_Guard->m_IsStopped)
                m_Handler(arg1);
        }

        template <typename Arg1, typename Arg2>
        void operator()(Arg1 arg1, Arg2 arg2)
        {
            CounterHolder holder(m_Guard);
            if (!m_Guard->m_IsStopped)
                m_Handler(arg1, arg2);
        }

    private:
        T m_Handler;
        CallbackGuard::Ptr m_Guard;
    };

    template<typename T>
    Handler<T> Wrap(const T& handler)
    {
        return Handler<T>(handler, shared_from_this());
    }

    void Shutdown()
    {
        m_IsStopped = true;
        const auto isCompleted = [this]()
        {
            boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
            return m_Counter == 0 || 
                m_Counter == 1 && m_Threads.size() == 1 && m_Threads.count(boost::this_thread::get_id());
        };
        PollUntil(m_Log, isCompleted);
    }

    std::size_t GetCount() const 
    { 
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        return (m_Threads.count(boost::this_thread::get_id()) ? m_Counter - 1 : m_Counter); 
    }

private:

    void OnHandlerStarted()
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_Threads.insert(boost::this_thread::get_id());
    }

    void OnHandlerCompleted()
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        --m_Counter; 
        m_Threads.erase(boost::this_thread::get_id());
    }

private:
    ILog& m_Log;
    Counter m_Counter;
    std::atomic<bool> m_IsStopped;
    mutable boost::recursive_mutex m_Mutex;
    std::set<boost::thread::id> m_Threads;
};

//----------------------------------------------------------------------
