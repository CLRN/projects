#ifndef proto_helpers_h__
#define proto_helpers_h__

#include <iterator>

#include <boost/type_traits/remove_pointer.hpp>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>

namespace proto
{
namespace hlp
{

//! Wraps google::protobuf::RepeatedField with push_back method, 
//! this allows usage of repeated fields in stl algorithms
template<typename T, typename Enable = void>
class PushBackWrapper;

//! This specialization enabled for pod types
template<typename T>
class PushBackWrapper
<
    T, 
    typename boost::enable_if
    <
        boost::is_pod<T>
    >::type 
> : public google::protobuf::RepeatedField<T>
{
    typedef google::protobuf::RepeatedField<T> Field;
    PushBackWrapper(Field& container);
public:
    typedef T value_type;
    typedef const value_type const_reference;

    void push_back(const value_type& val)
    {
        Field::Add(val);
    }

    void push_back(value_type&& val)
    {
        Field::Add(val);
    }
};

template<typename T>
class PushBackWrapper
<
    T, 
    typename boost::disable_if
    <
        boost::is_pod<T>
    >::type 
> : public google::protobuf::RepeatedPtrField<T>
{
    typedef google::protobuf::RepeatedPtrField<T> Field;
    PushBackWrapper(Field& container);
public:
    typedef T value_type;
    typedef const value_type const_reference;

    void push_back(const value_type& val)
    {
        *Field::Add() = val;
    }

    void push_back(value_type&& val)
    {
        Field::Add()->Swap(&val);
    }
};


template<typename T>
std::back_insert_iterator<PushBackWrapper<typename T::value_type> > MakeProtoInserter(T& container)
{
    PushBackWrapper<typename T::value_type>& wrapper = static_cast<PushBackWrapper<typename T::value_type>&>(container);
    return std::back_inserter(wrapper);
}

template<typename Enum, typename T>
std::vector<typename boost::remove_pointer<typename T::TypeTraits::MutableType>::type> GetExtensions(const T& id)
{
    std::vector<typename boost::remove_pointer<typename T::TypeTraits::MutableType>::type> result;
    const auto* desc = google::protobuf::GetEnumDescriptor<Enum>();

    result.reserve(desc->value_count());
    for (int i = 0; i < desc->value_count(); ++i)
        result.push_back(desc->value(i)->options().GetExtension(id));
    return result;
}

template<typename T, typename M>
boost::shared_ptr<T> MakeShared(const M& m)
{
    return boost::make_shared<T>(m);
}

namespace details
{

template<typename T, typename M>
void Serialize(const boost::shared_ptr<T>& o, M& m)
{
    o->Serialize(m);
}

template<typename T, typename M>
void Serialize(const boost::weak_ptr<T>& o, M& m)
{
    const auto ptr = o.lock();
    ptr->Serialize(m);
}

template<typename T, typename M>
void Serialize(const T& o, M& m)
{
    o.Serialize(m);
}

} // namespace details

template<typename T, typename O>
T MakeProto(const O& object)
{
    T proto;
    details::Serialize(object, proto);
    return proto;
}

} // namespace hlp
} // namespace proto


#endif // proto_helpers_h__
