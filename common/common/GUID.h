#ifndef GUID_h__
#define GUID_h__

#include <string>

namespace cmn
{
//! Guid helper
//!
//! \class CGUID
//!
class GUID
{
public:
	GUID();
	~GUID();

	//! Generate guid
	static std::string Generate();

    //! Validate
    static bool IsValid(const std::string& guid);
};

} // namespace cmn

#endif // GUID_h__
