#ifndef CachingProxy_h__
#define CachingProxy_h__

#include "ServiceHolder.h"

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/function.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>

namespace cmn
{
namespace cache
{

template<typename>
class Factory;

template<typename T>
class Proxy : public boost::enable_shared_from_this<Proxy<T>>, boost::noncopyable
{
public:

    //! Pointer type
    typedef boost::shared_ptr<Proxy<T>> Ptr;

    //! On value outdated callback
    typedef boost::function<void ()> OutdatedCallback;

    //! Fetch value functor
    typedef boost::function<T ()> FetchFunctor;

    friend class Factory<T>;

    operator T ()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        if (!m_IsObtained)
        {
            m_Value = m_FetchFunctor();
            m_IsObtained = true;
        }
        return m_Value;
    }

    T Get() 
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        if (!m_IsObtained)
        {
            m_Value = m_FetchFunctor();
            m_IsObtained = true;
        }
        return m_Value;
    }

private:

    Proxy
    (
        const FetchFunctor& func, 
        const OutdatedCallback& cb, 
        const boost::posix_time::time_duration& updateInterval, 
        const boost::posix_time::time_duration& outdateInterval
    ) 
        : m_OnOutdated(cb)
        , m_FetchFunctor(func)
        , m_Value()
        , m_UpdateInterval(updateInterval)
        , m_OutdateInterval(outdateInterval)
        , m_UpdateTimer(cmn::Service::Instance())
        , m_OutdateTimer(cmn::Service::Instance())
        , m_IsObtained(false)
    {
    }

    void Init()
    {
        StartOutdateTimer();
        StartUpdateTimer();
    }

    void StartOutdateTimer()
    {
        m_OutdateTimer.expires_from_now(m_OutdateInterval);
        m_OutdateTimer.async_wait(boost::bind(&Proxy::OnOutdated, shared_from_this(), boost::asio::placeholders::error));
    }

    void StartUpdateTimer()
    {
        m_UpdateTimer.expires_from_now(m_UpdateInterval);
        m_UpdateTimer.async_wait(boost::bind(&Proxy::OnUpdate, shared_from_this(), boost::asio::placeholders::error));
    }

    void OnOutdated(const boost::system::error_code& e)
    {
        if (e == boost::asio::error::operation_aborted)
            return;

        m_UpdateTimer.cancel();
        m_OutdateTimer.cancel();
        m_OnOutdated();
    }

    void OnUpdate(const boost::system::error_code& e)
    {
        if (e == boost::asio::error::operation_aborted)
            return;

        m_Value = m_FetchFunctor();
        {
            boost::unique_lock<boost::mutex> lock(m_Mutex);
            m_IsObtained = true;
        }
        StartOutdateTimer();
        StartUpdateTimer();
    }

private:

    T m_Value;
    FetchFunctor m_FetchFunctor;
    OutdatedCallback m_OnOutdated;
    boost::posix_time::time_duration m_UpdateInterval;
    boost::posix_time::time_duration m_OutdateInterval;
    boost::asio::deadline_timer m_UpdateTimer;
    boost::asio::deadline_timer m_OutdateTimer;
    boost::mutex m_Mutex;
    bool m_IsObtained;
};

template<typename T>
class Factory
{
public:
    static typename Proxy<T>::Ptr Instance
    (
        const typename Proxy<T>::FetchFunctor& fetch, 
        const typename Proxy<T>::OutdatedCallback& onOutdated, 
        const boost::posix_time::time_duration& updateInterval, 
        const boost::posix_time::time_duration& outdateInterval
    )
    {
        const typename Proxy<T>::Ptr instance(new Proxy<T>(fetch, onOutdated, updateInterval, outdateInterval));
        instance->Init();
        return instance;
    }
};

} // namespace cache
} // namespace cmn

#endif // CachingProxy_h__
