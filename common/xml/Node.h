#ifndef Node_h__
#define Node_h__

//! Common headers
#include "details/XmlCommon.h"

//! STL headers
#include <vector>
#include <string>

//! LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"
#include "libxml/xpathInternals.h"

namespace cmn
{
namespace xml
{

//! Forward declarations
class NodeIterator;

template<typename NodeType>
class XPathIterator;

template<typename NodeType>
class XPath;

//! Node implementation
class Node
{
	friend class NodeIterator;
	template<typename T>
	friend class XPathIterator;

public:
	typedef boost::shared_ptr<xmlDoc> DocPtr;
	typedef boost::shared_ptr<Node> Ptr;

	//! Iterator typedefs
	typedef NodeIterator iterator;
	typedef NodeIterator const_iterator;
	typedef XPathIterator<Node> xpath_iterator;
	typedef XPath<Node> xpath;
    typedef const Node& const_reference;
    typedef Node value_type;

private:
	typedef std::pair<std::string, std::string> StringPair;
	typedef std::vector<StringPair> PairsList;

	template<typename Object, typename Path>
	struct GetHelper
	{
		Object operator () (const Path& path, const Node& node) const;
	};

	template<typename Path>
	struct GetHelper<XPathIterator<Node>, Path>
	{
		XPathIterator<Node> operator () (const Path& path, const Node& node) const;
	};

	template<typename Path>
	struct GetHelper<XPath<Node>, Path>
	{
		XPath<Node> operator () (const Path& path, const Node& node) const;
	};

	template<typename Path>
	struct GetHelper<Ptr, Path>
	{
		Ptr operator () (const Path& path, const Node& node) const;
	};

public:


	//! Constructors
	//!
	//!

	//! Create new document
	Node();

	//! Construct from memory
	template<typename Char>
	Node(const Char* data, const std::size_t size);

	//! Construct from raw node
	Node(xmlNodePtr node, const Node& parent);

	//! Construct from file path
	template<typename Path>
	Node(const Path& path);

	//! Construct from memory and validate
	template<typename Char, typename Path>
	Node(const Char* data, const std::size_t size, const Path& schema);

	//! Destructor
	virtual ~Node();

	Node& operator = (const Node& other);

	NodeIterator begin() const;
	NodeIterator end() const;

	//! Get name
	template<typename T>
	T Name() const;

	//!
	//!
	//! Constructors end

	//! Getters
	//!
	//!

	//! Get value of the current node
	template<typename Object>
	Object Get() const;

	//! Getter
	template<typename Object, typename Path>
	Object Get(const Path& path) const;

	//! Getter
	template<typename Object, typename Path, std::size_t N>
	Object Get(const Path (&path)[N]) const;

	//! Getter with default value
	template<typename Object, typename Path>
	Object Get(const Path& path, const Object& defaultValue) const;

	//! Getter with default value
	template<typename Object, typename Path, std::size_t N>
	Object Get(const Path (&path)[N], const Object& defaultValue) const;


	//!
	//!
	//! Getters end

	//! Setters
	//!
	//!

	//! Setter
	template<typename Value>
    Node& Set(const Value& value);

	//! Setter
	template<typename Path, typename Value>
    Node& Set(const Path& path, const Value& value);

	//! Setter
	template<typename Path, typename Value, std::size_t N>
    Node& Set(const Path(&path)[N], const Value& value);

	//! Set name
	template<typename T>
	Node& Name(const T& name);

	//! Set namespace
	template<typename Prefix, typename Url>
	void SetNamespace(const Prefix& prefix, const Url& url);

	//!
	//!
	//! Setters end

	//! Create new node
	template<typename T>
	Ptr Add(const T& name);

	//! Add existing node
    Node& Add(const Node& node);
    Node& push_back(const Node& node);

	//! Save node to string
	template<typename String>
	void Save(String& result, const std::string encoding = xml::DEFAULT_ENCODING) const;

	//! Save node to file
	template<typename Path>
	void Save2File(const Path& path, const std::string encoding = xml::DEFAULT_ENCODING) const;

	//! Erase child node
	NodeIterator Erase(const NodeIterator& it);

	//! Erase child node
	void Erase(const Node& node);

	//! Erase child node
	template<typename Path>
	std::size_t Erase(const Path& path);

	//! Erase all child nodes
	void Clear();

	//! Is node contain any child
	bool Empty() const;

	//! Children count
	std::size_t	Size() const;

private:
	//! Document pointer
	DocPtr m_Document;

	//! Current node
	xmlNode* m_Node;

	//! Document namespaces
	PairsList m_Namespaces;
};

//! Stream operator
template<typename T>
T& operator << (T& s, const cmn::xml::Node& node)
{
    std::basic_string<T::char_type> data;
    node.Save(data);
    s.write(data.c_str(), data.size());
    return s;
}

} // namespace xml
} // namespace cmn

#include "details/NodeImpl.h"

#endif // Node_h__