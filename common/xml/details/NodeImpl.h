#ifndef NodeImpl_h__
#define NodeImpl_h__

#include "XmlErrorInfo.h"

#include <libxml/xmlschemas.h>

#include "../Node.h"
#include "conversion/AnyCast.h"

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

namespace cmn
{
namespace xml
{

//! Xml node iterator
class NodeIterator : public boost::iterator_facade<NodeIterator, Node, boost::bidirectional_traversal_tag>
{
public:
	NodeIterator();
	NodeIterator(const Node& node, xmlNodePtr raw = 0);

private:
	friend class boost::iterator_core_access;

	void increment();
	void decrement();
	bool equal(const NodeIterator& other) const;
	Node& dereference() const;

private:
	Node::Ptr m_Current;
};

//! Xml node xpath iterator
template<typename NodeType>
class XPathIterator : public boost::iterator_facade<XPathIterator<NodeType>, NodeType, boost::forward_traversal_tag>
{
	typedef boost::shared_ptr<xmlXPathContext> ContextPtr;
	typedef boost::shared_ptr<xmlXPathObject> ObjectPtr;
public:

	XPathIterator::XPathIterator() : m_Index(0)
	{

	}

	template<typename T>
	XPathIterator(const Node& node, const T& expr)
		: m_Context(xmlXPathNewContext(node.m_Document.get()), xmlXPathFreeContext)
		, m_Index(0)
	{
		CHECK(m_Context, xml::Exception("Failed to create xpath context"));
		m_Context->node = node.m_Node;

		for (const Node::StringPair& pair : node.m_Namespaces)
		{
			CHECK
			(
				!xmlXPathRegisterNs(m_Context.get(), reinterpret_cast<const xmlChar*>(pair.first.c_str()), reinterpret_cast<const xmlChar*>(pair.second.c_str())), 
				Exception("Failed to register namespace"),
				pair.first,
				pair.second
			);
		}

		m_Object.reset(xmlXPathEvalExpression(reinterpret_cast<const xmlChar*>(conv::cast<std::string>(expr).c_str()), m_Context.get()), xmlXPathFreeObject);
		CHECK(m_Object, xml::Exception("Failed to evaluate xpath"), expr);

		if (m_Object->nodesetval && m_Object->nodesetval->nodeNr)
		{
			m_Current.reset(new Node(node));
			m_Current->m_Node = m_Object->nodesetval->nodeTab[m_Index];
		}
	}

private:
	friend class boost::iterator_core_access;

	void XPathIterator::increment()
	{
		CHECK(m_Current, xml::Exception("Empty node"));
		CHECK(m_Current->m_Node, xml::Exception("Empty node"));

		++m_Index;
		if (m_Index < m_Object->nodesetval->nodeNr)
			m_Current->m_Node = m_Object->nodesetval->nodeTab[m_Index];
		else
			m_Current.reset();
	}

	bool XPathIterator::equal(const XPathIterator& other) const
	{
		if (!m_Current && !other.m_Current)
			return true;

		if (!m_Current || !other.m_Current)
			return false;

		return m_Current->m_Node == other.m_Current->m_Node;
	}

	NodeType& XPathIterator::dereference() const
	{
		CHECK(m_Current, xml::Exception("Empty node"));
		CHECK(m_Current->m_Node, xml::Exception("Empty node"));
		return static_cast<NodeType&>(*m_Current);
	}

private:
	typename NodeType::Ptr m_Current;
	ContextPtr m_Context;
	ObjectPtr m_Object;
	int m_Index;
};

xmlDocPtr ParseWrapper(const char* src, std::size_t length);
xmlDocPtr ParseWrapper(const wchar_t* src, std::size_t length);
XmlErrorInfo GetLastXmlError();

template<typename Char>
Node::Node(const Char* data, const std::size_t size)
{
	try
	{
		m_Document.reset(ParseWrapper(data, size), xmlFreeDoc);
		CHECK(m_Document, xml::Exception("Failed to read document from memory"));
		m_Node = xmlDocGetRootElement(m_Document.get());
		CHECK(m_Node, xml::Exception("Failed to get root node"));
	}
	CATCH_PASS(xml::Exception("Failed to construct xml node from memory") << GetLastXmlError(), size)
}

template<typename Path>
Node::Node(const Path& path) : m_Node(0)
{
	try
	{
		m_Document.reset(xmlReadFile(conv::cast<std::string>(path).c_str(), NULL, 0), xmlFreeDoc);
		CHECK(m_Document, xml::Exception("Failed to read document from memory"));
		m_Node = xmlDocGetRootElement(m_Document.get());
		CHECK(m_Node, xml::Exception("Failed to get root node"));
	}
	CATCH_PASS(xml::Exception("Failed to construct xml node from path") << GetLastXmlError(), path)
}

template<typename Char, typename Path>
Node::Node(const Char* data, const std::size_t size, const Path& schema)
{
	try
	{
		m_Document.reset(ParseWrapper(data, size), xmlFreeDoc);
		CHECK(m_Document, xml::Exception("Failed to read document from memory"));
		m_Node = xmlDocGetRootElement(m_Document.get());
		CHECK(m_Node, xml::Exception("Failed to get root node"));

		const std::string schemaPath = conv::cast<std::string>(schema);

		const DocPtr schemaDoc(xmlReadFile(schemaPath.c_str(), NULL, XML_PARSE_NONET), xmlFreeDoc);
		CHECK(schemaDoc, xml::Exception("Failed to read schema document from path"), schemaPath);

		const boost::shared_ptr<xmlSchemaParserCtxt> parserCtxt(xmlSchemaNewDocParserCtxt(schemaDoc.get()), xmlSchemaFreeParserCtxt);
		CHECK(parserCtxt, xml::Exception("Failed to create parser context"));

		const boost::shared_ptr<xmlSchema> schemaPtr(xmlSchemaParse(parserCtxt.get()), xmlSchemaFree);
		CHECK(schemaPtr, xml::Exception("Failed to create schema object"));

		const boost::shared_ptr<xmlSchemaValidCtxt> validationContext(xmlSchemaNewValidCtxt(schemaPtr.get()), xmlSchemaFreeValidCtxt);
		CHECK(validationContext, xml::Exception("Failed to create schema validation context"));

		CHECK(!xmlSchemaValidateDoc(validationContext.get(), m_Document.get()), xml::Exception("Validation failed"));
	}
	CATCH_PASS(xml::Exception("Failed to construct and validate xml node from memory") << GetLastXmlError(), size)	
}

template<typename T>
T Node::Name() const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	return conv::cast<T>(reinterpret_cast<const char*>(m_Node->name));
}

template<typename T>
Node& Node::Name(const T& name)
{
	CHECK(m_Node || m_Document, xml::Exception("Empty node"));
	const std::string textString = conv::cast<std::string>(name);

	if (!m_Node)
	{
		m_Node = xmlNewDocNode(m_Document.get(), NULL, reinterpret_cast<const xmlChar*>(textString.c_str()), NULL);
		xmlDocSetRootElement(m_Document.get(), m_Node);
	}
	else
	{
		xmlNodeSetName(m_Node, reinterpret_cast<const xmlChar*>(textString.c_str()));
	}
    return *this;
}

template<typename Prefix, typename Url>
void Node::SetNamespace(const Prefix& prefix, const Url& url)
{
	m_Namespaces.push_back(std::make_pair(conv::cast<std::string>(prefix), conv::cast<std::string>(url)));
}

template<typename Object, typename Path>
Object Node::Get(const Path& path, const Object& defaultValue) const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		return GetHelper<Object, Path>()(path, *this);
	}
	catch (const conv::CastException&)
	{
		return defaultValue;
	}
	CATCH_PASS(xml::Exception("Failed to get object") << GetLastXmlError(), path)
}

template<typename Object, typename Path>
Object Node::Get(const Path& path) const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		return GetHelper<Object, Path>()(path, *this);
	}
	CATCH_PASS(xml::Exception("Failed to get object") << GetLastXmlError(), path)
}

template<typename Object, typename Path, std::size_t N>
Object Node::Get(const Path (&path)[N], const Object& defaultValue) const
{
	typedef std::basic_string<Path> PathString;
	return Get<Object, PathString>(PathString(path, N), defaultValue);
}

template<typename Object, typename Path, std::size_t N>
Object Node::Get(const Path (&path)[N]) const
{
	typedef std::basic_string<Path> PathString;
	return Get<Object, PathString>(PathString(path, N));
}

template<typename Object>
Object Node::Get() const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		if (m_Node->children && m_Node->children->type == XML_TEXT_NODE && m_Node->children->content) 
			return conv::cast<Object>(std::string(reinterpret_cast<const char*>(m_Node->children->content)));
		else 
			return Object();
	}
	CATCH_PASS(xml::Exception("Failed to get object") << GetLastXmlError())
}


template<typename Object, typename Path>
Object Node::GetHelper<Object, Path>::operator()(const Path& path, const Node& node) const
{
	if (path.empty())
	{
		if (node.m_Node->children && node.m_Node->children->type == XML_TEXT_NODE && node.m_Node->children->content) 
			return conv::cast<Object>(std::string(reinterpret_cast<const char*>(node.m_Node->children->content)));
		else 
			return Object();
	}

	if (path[0] == Path::value_type('@'))
	{
		const Path attributeName(path.begin() + 1, path.end());
		const boost::shared_ptr<xmlChar> value(xmlGetProp(node.m_Node, reinterpret_cast<const xmlChar*>(conv::cast<std::string>(attributeName).c_str())), xmlFree);
		CHECK(value, xml::Exception("Failed to find attribute by name"), attributeName);

		return conv::cast<Object>(std::string(reinterpret_cast<const char*>(value.get())));
	}

	const XPathIterator<Node> it(node, path);
	CHECK(it != XPathIterator<Node>(), xml::Exception("Failed to find node by xpath"), path);
	return it->Get<Object>();
}

template<typename Path>
XPathIterator<Node> Node::GetHelper<XPathIterator<Node>, Path>::operator() (const Path& path, const Node& node) const
{
	return XPathIterator<Node>(node, path);
}

template<typename Path>
Node::Ptr Node::GetHelper<Node::Ptr, Path>::operator()(const Path& path, const Node& node) const
{
	const XPathIterator<Node> it(node, path);
	CHECK(it != XPathIterator<Node>(), xml::Exception("Failed to find node by xpath"), path);
	return Node::Ptr(new Node(*it));
}

template<typename Value>
Node& Node::Set(const Value& value)
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		const std::string textString = conv::cast<std::string>(value);
		xmlNodeSetContent(m_Node, reinterpret_cast<const xmlChar*>(textString.c_str()));
        return *this;
	}
	CATCH_PASS(xml::Exception("Failed to set value") << GetLastXmlError(), value)
}

template<typename Path, typename Value, std::size_t N>
Node& Node::Set(const Path(&path)[N], const Value& value)
{
	CHECK(m_Node, xml::Exception("Empty node"));
	typedef std::basic_string<Path> PathString;
	Set<PathString, Value>(PathString(path, N - 1), value);
    return *this;
}

template<typename Path, typename Value>
Node& Node::Set(const Path& path, const Value& value)
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		if (path.empty())
		{
			Set(value);
			return *this;
		}

		if (path[0] == Path::value_type('@'))
		{
			const std::string textString = conv::cast<std::string>(value);
			const Path attributeName(path.begin() + 1, path.end());
			xmlSetProp(m_Node, reinterpret_cast<const xmlChar*>(conv::cast<std::string>(attributeName).c_str()), reinterpret_cast<const xmlChar*>(textString.c_str()));
            return *this;
		}

        // try to find node by xpath and set value
		XPathIterator<Node> it(*this, path);
         
        if (it == XPathIterator<Node>())
        {
            // failed to find node, need to create it
            std::vector<Path> nodes;
            boost::algorithm::split(nodes, path, boost::algorithm::is_any_of(Path(1, Path::value_type('/'))));

            Path currentPath;
            Ptr created(this, [](Node*) {}); // null deleter
            for (Path& node : nodes)
            {
                if (node.empty())
                    continue; 

                // check for node attributes
                Path attributeName;
                Path attributeValue;
                const typename Path::size_type open = node.find(Path::value_type('['));
                if (open != Path::npos)
                {
                    const typename Path::size_type close = node.find(Path::value_type(']'), open);
                    if (close != Path::npos)
                    {
                        const Path attribute(node.substr(open + 1, close - open - 1));
                        const typename Path::size_type equal = attribute.find(Path::value_type('='));
                        if (equal != Path::npos && equal && attribute.front() == Path::value_type('@'))
                        {
                            node.assign(node.substr(0, open));
                            attributeName.assign(attribute.substr(0, equal));
                            attributeValue.assign(attribute.substr(equal + 2, attribute.size() - equal - 3));
                        }
                    }
                }

                // build full xpath to current node
                currentPath.append(Path(1, Path::value_type('/')) + node);

                // check this node existence
                const XPathIterator<Node> current(*this, currentPath);
                if (current == XPathIterator<Node>())
                {
                    if (node.front() == '@')
                        created->Set(node, "");
                    else
                        created = created->Add(node); // create node

                    // create attribute
                    if (!attributeName.empty())
                        created->Set(attributeName, attributeValue);
                }
            }

            // find node again
            it = XPathIterator<Node>(*this, path);
        }

        // validate iterator
		CHECK(it != XPathIterator<Node>(), xml::Exception("Failed to find node by xpath"), path);

        // set value to node
		return it->Set<Value>(value);
	}
	CATCH_PASS(xml::Exception("Failed to set value") << GetLastXmlError(), value, path)
}

template<typename Path>
std::size_t Node::Erase(const Path& path)
{
	std::size_t count = 0;
	XPathIterator<Node> it(*this, path);
	const XPathIterator<Node> itEnd;
	for (; it != itEnd; ++it, ++count)
		Erase(*it);
	return count;
}

template<typename T>
Node::Ptr Node::Add(const T& name)
{
	CHECK(m_Node || m_Document, xml::Exception("Empty node"));
	try
	{
		const std::string textString = conv::cast<std::string>(name);
		xmlNodePtr newNode;
		if (m_Node)
        {
    	    newNode = xmlNewChild(m_Node, NULL, reinterpret_cast<const xmlChar*>(textString.c_str()), NULL);
        }
		else
		{
			newNode = xmlNewDocNode(m_Document.get(), NULL, reinterpret_cast<const xmlChar*>(textString.c_str()), NULL);
			xmlDocSetRootElement(m_Document.get(), newNode);
		}

		CHECK(newNode, xml::Exception("Failed to create node"));
		return Node::Ptr(new Node(newNode, *this));
	}
	CATCH_PASS(xml::Exception("Failed to add node") << GetLastXmlError(), name)
}

template<typename Path>
void Node::Save2File(const Path& path, const std::string encoding) const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		const std::string textString = conv::cast<std::string>(path);
		CHECK(xmlSaveFormatFileEnc(textString.c_str(), m_Document.get(), encoding.c_str(), true) != -1, xml::Exception("Failed to save doc to file"), path);
	}
	CATCH_PASS(xml::Exception("Failed to save to file") << GetLastXmlError(), encoding, path)
}

template<typename String>
void Node::Save(String& result, const std::string encoding) const
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		xmlChar* outBuf = NULL;
		int outSize = 0;
		xmlDocDumpFormatMemoryEnc(m_Document.get(), &outBuf, &outSize, encoding.c_str(), true);
		result = conv::cast<String>(reinterpret_cast<const char*>(outBuf));
	}
	CATCH_PASS(xml::Exception("Failed to save to string") << GetLastXmlError(), encoding)
}

//! Xpath result holder
template<typename T>
class XPath
{
public:
	typedef XPathIterator<T> iterator;
	typedef XPathIterator<T> const_iterator;

	XPath()
	{

	}
	XPath(const iterator& it)
		: m_It(it) 
	{

	}
	iterator begin() const
	{
		return m_It;
	}
	iterator end() const
	{
		return XPath::iterator();
	}
private:
	iterator m_It;
};


template<typename Path>
XPath<Node> Node::GetHelper<XPath<Node>, Path>::operator()(const Path& path, const Node& node) const
{
	return XPath<Node>(XPathIterator<Node>(node, path));
}

} // namespace Xml
} // namespace PT

#endif // NodeImpl_h__