#include "NodeImpl.h"

namespace cmn
{
namespace xml
{

DECLARE_EXCEPTION_INFO_VISITOR(&Visitor);

NodeIterator::NodeIterator()
{

}

NodeIterator::NodeIterator(const Node& node, xmlNodePtr raw /*= 0*/) : m_Current(new Node())
{
	m_Current->m_Document = node.m_Document;
	m_Current->m_Node = raw ? raw : node.m_Node->children;
	if (m_Current->m_Node && m_Current->m_Node->type != XML_ELEMENT_NODE)
		increment();
}

void NodeIterator::increment()
{
	CHECK(m_Current, xml::Exception("Empty node"));
	CHECK(m_Current->m_Node, xml::Exception("Empty node"));

	do 
	{
		m_Current->m_Node = m_Current->m_Node->next;
	} 
	while (m_Current->m_Node && m_Current->m_Node->type != XML_ELEMENT_NODE);

	if (!m_Current->m_Node)
		m_Current.reset();
}

void NodeIterator::decrement()
{
	CHECK(m_Current, xml::Exception("Empty node"));
	CHECK(m_Current->m_Node, xml::Exception("Empty node"));

	do 
	{
		m_Current->m_Node = m_Current->m_Node->prev;
	} 
	while (m_Current->m_Node && m_Current->m_Node->type != XML_ELEMENT_NODE);
}

bool NodeIterator::equal(const NodeIterator& other) const
{
	if ((!m_Current || !m_Current->m_Node) && (!other.m_Current || !other.m_Current->m_Node))
		return true;

	if (!m_Current || !other.m_Current)
		return false;

	return m_Current->m_Node == other.m_Current->m_Node;
}

Node& NodeIterator::dereference() const
{
	CHECK(m_Current, xml::Exception("Empty node"));
	CHECK(m_Current->m_Node, xml::Exception("Empty node"));

	return *m_Current;
}

cmn::xml::NodeIterator Node::begin() const
{
	return NodeIterator(*this);
}

cmn::xml::NodeIterator Node::end() const
{
	return NodeIterator();
}

Node::Node()
	: m_Document(xmlNewDoc(reinterpret_cast<const xmlChar*>(XML_DEFAULT_VERSION)), xmlFreeDoc)
	, m_Node(0)
{

}

Node::Node(xmlNodePtr node, const Node& parent)
	: m_Node(node)
	, m_Document(parent.m_Document)
	, m_Namespaces(parent.m_Namespaces)
{

}

Node& Node::operator = (const Node& other)
{
	m_Document = other.m_Document;
	m_Namespaces = other.m_Namespaces;
	m_Node = other.m_Node;
	return *this;
}

Node::~Node()
{
	m_Document.reset();
	m_Node = 0;
}

Node& Node::Add(const Node& node)
{
	CHECK(m_Node, xml::Exception("Empty source node"));
	CHECK(node.m_Node, xml::Exception("Empty input node"));
	xmlAddChild(m_Node, xmlCopyNode(node.m_Node, 1));
    return *this;
}

Node& Node::push_back(const Node& node)
{
    Add(node);
    return *this;
}

cmn::xml::NodeIterator Node::Erase(const NodeIterator& it)
{
	CHECK(m_Node, xml::Exception("Empty node"));
	try
	{
		xmlNodePtr next = it->m_Node->next;
		xmlUnlinkNode(it->m_Node);
		return next ? NodeIterator(*this, next) : NodeIterator();
	}
	CATCH_PASS(xml::Exception("Failed to erase iterator") << GetLastXmlError())
}

void Node::Erase(const Node& node)
{
	CHECK(m_Node, xml::Exception("Empty node"));
	xmlUnlinkNode(node.m_Node);
}

void Node::Clear()
{
	CHECK(m_Node, xml::Exception("Empty node"));
	m_Node->children = 0;
}

bool Node::Empty() const
{
	return !(m_Node && (xmlChildElementCount(m_Node) || m_Node->properties));
}

std::size_t Node::Size() const
{
	return m_Node ? xmlChildElementCount(m_Node) : 0;
}

XmlErrorInfo GetLastXmlError()
{
	const xmlErrorPtr error = xmlGetLastError();
    static const xmlError empty = {};
    return XmlErrorInfo(error ? *error : empty); 
}

xmlDocPtr ParseWrapper(const char* src, std::size_t length)
{
	return xmlReadMemory(src, length, NULL, NULL, 0);
}

xmlDocPtr ParseWrapper(const wchar_t* src, std::size_t length)
{
	const std::wstring buffer(src, length);
	const std::string narrow = conv::cast<std::string>(buffer);
	return xmlReadMemory(narrow.c_str(), narrow.size(), NULL, NULL, 0);
}

} // namespace Xml
} // namespace PT