#ifndef XmlCommon_h__
#define XmlCommon_h__

#include "exception/CheckHelpers.h"

#include <string>

namespace cmn
{
namespace xml
{

DECLARE_SIMPLE_EXCEPTION_CLASS(Exception, cmn::Exception);

const std::string DEFAULT_ENCODING = "UTF-8";

} // namespace xml
} // namespace cmn


#endif // XmlCommon_h__