#ifndef XmlErrorInfo_h__
#define XmlErrorInfo_h__

#include <libxml/xmlerror.h>

#include <ostream>

#include <boost/exception/info.hpp>

namespace cmn
{
namespace xml
{
    typedef boost::error_info<struct xml_error_info, xmlError> XmlErrorInfo;
    void Visitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
} // namespace xml
} // namespace cmn


#endif // XmlErrorInfo_h__
