#include "XmlErrorInfo.h"
#include "conversion/AnyCast.h"

namespace cmn
{
namespace xml
{

void Print(std::wostream& os, const xmlError& info, unsigned indentLevel = 0)
{
    std::string errorMessage(info.message ? info.message : "unknown");
    if (errorMessage.size() > 2 && errorMessage[errorMessage.size() - 1] == '\n')
        errorMessage.resize(errorMessage.size() - 1);
    cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel) << "[Xml error] = " << conv::cast<std::wstring>(errorMessage);

    if (info.file)
        cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel + 1) << "[File] = " << conv::cast<std::wstring>(info.file);

    cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel + 1) << "[Line] = " << conv::cast<std::wstring>(info.line);
    cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel + 1) << "[Column] = " << conv::cast<std::wstring>(info.int2);
}

void Visitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
{
    if (const xmlError* info = boost::get_error_info<XmlErrorInfo>(be))
        Print(os, *info, indentLevel);
}

} // namespace xml
} // namespace cmn
