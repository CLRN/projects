// Google test library headers
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "xml/Node.h"
#include "common/FileSystem.h"

#include <boost/filesystem.hpp>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>

void PopulateTag(cmn::xml::Node& node)
{
    static const std::vector<char> binary = boost::assign::list_of(1)(2)(3)(4)(5);

	node.Set("@unicode", L"������");
	node.Set("@ansi", L"ansi");
    node.Set("@int", 54321);
    node.Set("@float", 1234.56702f);
	node.Set("@binary", binary);
	node.Name("some_tag");
    node.Set("Data/nested_tag[@attr='attr_value']/@value", 1);
}

void ValidateTag(const cmn::xml::Node& node)
{
	EXPECT_EQ(node.Get<std::wstring>("@unicode"), L"������");
	EXPECT_EQ(node.Get<std::string>("@ansi"), "ansi");
	EXPECT_EQ(node.Get<int>("@int"), 54321);
	EXPECT_EQ(node.Get<float>("@float"), 1234.56702f);
	EXPECT_EQ(node.Get<std::vector<char>>("@binary"), boost::assign::list_of(1)(2)(3)(4)(5));
	EXPECT_EQ(node.Name<std::wstring>(), L"some_tag");
    EXPECT_EQ(node.Get<int>("Data/nested_tag[@attr='attr_value']/@value"), 1);
}

std::string GetPopulatedDocument()
{
	using namespace cmn::xml;

	Node document;
	const Node::Ptr root = document.Add(L"root");
	root->Set(L"@version", 1234);

	const Node::Ptr tag = root->Add("name");
	PopulateTag(*tag);

	const Node::Ptr anotherTag = tag->Add(L"some_tag");
    PopulateTag(*anotherTag);
	root->Add(*anotherTag);
	tag->Clear();
    root->Erase(*tag);

    root->Add("some_tag");

	std::string serialized;
	root->Save(serialized);
	return serialized;
}

TEST(Xml, LoadSave)
{
	using namespace cmn::xml;

	std::string serialized;
    fs::ScopedFile tempFile;
    const std::wstring& name = tempFile;

	// populate and serialize
	{
		Node document;
		const Node::Ptr root = document.Add(L"root");
		root->Set(L"@version", 1234);
		root->Add("tag");

		root->Save(serialized);
		root->Save2File(name);
	}

	// deserialize and validate
	{
		Node document(serialized.c_str(), serialized.size());
		
		EXPECT_EQ(document.Size(), 1);
		EXPECT_EQ(document.Get<int>("@version"), 1234);
	}

	// deserialize and validate
	{
		Node document(name);

		EXPECT_EQ(document.Size(), 1);
		EXPECT_EQ(document.Get<int>("@version"), 1234);
	}
}

TEST(Xml, Populate)
{
	using namespace cmn::xml;

	const std::string serialized = GetPopulatedDocument();
	
	Node document(serialized.c_str(), serialized.size());

	EXPECT_EQ(document.Size(), 2);
	EXPECT_EQ(document.Get<int>("@version"), 1234);

	const Node::Ptr tag = document.Get<Node::Ptr>("some_tag");
	ASSERT_TRUE(tag);
	ValidateTag(*tag);
}

TEST(Xml, Travers)
{
	using namespace cmn::xml;
	const std::string serialized = GetPopulatedDocument();

	Node document(serialized.c_str(), serialized.size());

	Node::iterator it = document.begin();
	const Node::iterator itEnd = document.end();

	ValidateTag(*it);
	++it;
    EXPECT_TRUE(it->Empty());
    ++it;
	EXPECT_EQ(it, itEnd);
}

TEST(Xml, Xpath)
{
	using namespace cmn::xml;
	const std::string serialized = GetPopulatedDocument();

	Node document(serialized.c_str(), serialized.size());

	std::size_t count = 0;
	BOOST_FOREACH(const Node& node, document.Get<Node::xpath>("some_tag"))
	{
		if (!count++)
			ValidateTag(node);
		else
			EXPECT_TRUE(node.Empty());
	}

	EXPECT_EQ(count, 2);

	Node::xpath_iterator itXpath = document.Get<Node::xpath_iterator>("some_tag");
	const Node::xpath_iterator itXpathEnd;

	ValidateTag(*itXpath);
	++itXpath;
	EXPECT_TRUE(itXpath->Empty());
	++itXpath;
	EXPECT_EQ(itXpath, itXpathEnd);

	itXpath = document.Get<Node::xpath_iterator>("//some_tag[1]");
	ValidateTag(*itXpath);
	++itXpath;
	EXPECT_EQ(itXpath, itXpathEnd);

	itXpath = document.Get<Node::xpath_iterator>("//some_tag[2]");
	EXPECT_TRUE(itXpath->Empty());
	++itXpath;
	EXPECT_EQ(itXpath, itXpathEnd);
}

TEST(Xml, Namespaces)
{
	using namespace cmn::xml;

	static const std::wstring xmlData = 
		L"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		L"<Package xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
			L"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
			L"xsi:schemaLocation=\"http://www.ptsecurity.ru/import https://support.ptsecurity.ru/xsd/mp8/24/maxpatrol80-import.xsd\" "
			L"xmlns=\"http://www.ptsecurity.ru/import\">"
			L"<profiles>"
				L"<profile name=\"Default\" id=\"1\">"
					L"<parameters/>"
				L"</profile>"
			L"</profiles>"
		L"</Package>";

	Node document(xmlData.c_str(), xmlData.size());

	document.SetNamespace(L"im", "http://www.ptsecurity.ru/import");

	Node::xpath_iterator itXpath = document.Get<Node::xpath_iterator>("//im:profile");
	const Node::xpath_iterator itXpathEnd;

	ASSERT_NE(itXpath, itXpathEnd);
	EXPECT_EQ(itXpath->Get<int>("@id"), 1);
	EXPECT_EQ(itXpath->Get<std::string>("@name"), "Default");
	++itXpath;
	EXPECT_EQ(itXpath, itXpathEnd);

}

TEST(Xml, Erase)
{
	using namespace cmn::xml;

	const std::string serialized = GetPopulatedDocument();

	{
		Node document(serialized.c_str(), serialized.size());

		EXPECT_EQ(document.Erase("some_tag"), 2);
		EXPECT_FALSE(document.Size());
	}

	{
		Node document(serialized.c_str(), serialized.size());

		Node::iterator it = document.begin();
		const Node::iterator itEnd = document.end();

		for (; it != itEnd; )
			it = document.Erase(it);

		EXPECT_FALSE(document.Size());
	}
}

TEST(Xml, Error)
{
    using namespace cmn::xml;

    std::string serialized = GetPopulatedDocument();

    serialized.resize(serialized.size() / 2);

    try
    {
        Node document(serialized.c_str(), serialized.size());
    }
    catch (const std::exception& e)
    {
        const std::wstring error = cmn::ExceptionInfo(e);
        EXPECT_NE(error.find(L"[Xml error] = "), std::wstring::npos);
    }
}