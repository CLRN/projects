#include "Router.h"
#include "Exceptions.h"
#include "exception/CheckHelpers.h"
#include "common/ProtoHelpers.h"

#include <deque>

#include <boost/make_shared.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/thread/mutex.hpp>

namespace rpc
{
namespace
{

class Router : public IRouter
{
public:
    typedef std::map<IService::Id, InstanceId> ClosestMap;
    typedef std::deque<const Instances::value_type> InstanceQueue;

    Router(ILog& log, const rpc::InstanceId& id) : m_Log(log)
    {
        m_LocalInstance.set_id(id);
    }

    virtual const rpc::InstanceId& GetLocalId() const override
    {
        return m_LocalInstance.id();
    }

    void AddNeighbor(const Instances::value_type& instance) override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        const auto it = boost::find_if(m_LocalInstance.instances(), boost::bind(&proto::RegistrationInfo::Instance::id, _1) == instance.id());
        if (it != m_LocalInstance.instances().end())
            return; // already have this neighbor

        auto& newInstance = *m_LocalInstance.add_instances();
        newInstance.CopyFrom(instance);
        newInstance.clear_instances();
    }

    virtual void UpdateInfo(Instances& info) override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_RemoteInstances.reserve(m_RemoteInstances.size() + info.size());
        for (auto& instance : info)
        {
            const auto it = boost::find_if(m_RemoteInstances, boost::bind(&proto::RegistrationInfo::Instance::id, _1) == instance.id());
            if (it == m_RemoteInstances.end() && instance.id() != m_LocalInstance.id())
            {
                m_RemoteInstances.emplace_back();
                m_RemoteInstances.back().Swap(&instance);
            }
        }
    }

    virtual void AddLocalService(IService::Id id) override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_LocalInstance.add_providedservices(id);
    }

    virtual void RemoveInstance(const InstanceId& id) override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        m_ClosestMap.clear(); // reset cache

        {
            const auto it = boost::find_if(m_RemoteInstances, boost::bind(&proto::RegistrationInfo::Instance::id, _1) == id);
            if (it != m_RemoteInstances.end())
                m_RemoteInstances.erase(it);
        }

        if (m_LocalInstance.instances_size())
        {
            auto old = m_LocalInstance.instances();
            m_LocalInstance.clear_instances();
            const auto range = old | boost::adaptors::filtered(boost::bind(&proto::RegistrationInfo::Instance::id, _1) != id);
            boost::copy(range, proto::hlp::MakeProtoInserter(*m_LocalInstance.mutable_instances()));
        }
    }

    virtual Instances GetInstances() const override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        Instances result;
        result.push_back(m_LocalInstance);
        boost::copy(m_RemoteInstances, std::back_inserter(result));
        return result;
    }

    rpc::InstanceId GetNextNode(const details::CallParams& params, IService::Id serviceId) override
    {
        switch (params.m_Type)
        {
        case choose::Unloaded:
        case choose::Closest:
        case choose::Any:
        {
            boost::unique_lock<boost::mutex> lock(m_Mutex);

            const auto it = m_ClosestMap.find(serviceId);
            if (it != m_ClosestMap.end())
                return it->second;
        }
        {

            // using breadth search
            InstanceQueue queue;
            {
                boost::unique_lock<boost::mutex> lock(m_Mutex);
                for (const auto& instance : m_LocalInstance.instances())
                    queue.push_back(instance);
            }

            const auto id = FindServiceInChain(queue, serviceId);
            CHECK(!id.empty(), Exception("Failed to find instance by id"), serviceId, (Visualize(GetInstances()), "net"));

            boost::unique_lock<boost::mutex> lock(m_Mutex);
            m_ClosestMap.insert(std::make_pair(serviceId, id)); // save in cache
            return id;
        }
        default:
            THROW(Exception("Can't process this dispatch type by router"), serviceId);
        }
    }

    InstanceId FindServiceInChain(InstanceQueue& queue, IService::Id id)
    {
        while (!queue.empty())
        {
            const auto& current = queue.front();

            const auto it = boost::find(current.providedservices(), id);
            if (it != current.providedservices().end())
                return current.id();

            // iterate through all neighbors
            for (const auto& neighbor : current.instances())
                queue.push_back(neighbor);

            queue.pop_front();
        }
        return rpc::InstanceId();
    }


    virtual bool IsExists(const InstanceId& instance, IService::Id service) override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        // find closest instance by id
        const auto it = boost::find_if(m_LocalInstance.instances(), boost::bind(&proto::RegistrationInfo::Instance::id, _1) == instance);
        if (it == m_LocalInstance.instances().end())
            return false;

        // find instance with specified id
        InstanceQueue queue;
        queue.push_back(*it);
        const auto id = FindServiceInChain(queue, service);
        return !id.empty();
    }

private:

    ILog& m_Log;
    proto::RegistrationInfo::Instance m_LocalInstance;
    Instances m_RemoteInstances;
    ClosestMap m_ClosestMap;
    mutable boost::mutex m_Mutex;
};


} // anonymous namespace


IRouter::Ptr IRouter::Instance(ILog& log, const rpc::InstanceId& id)
{
    return boost::make_shared<Router>(log, id);
}

std::string Visualize(const IRouter::Instances& graph)
{
    std::ostringstream oss;

    oss << "graph net {" << std::endl;

    for (const auto& instance : graph)
        oss << "\t\"" << instance.id() << "\"[label=\"" << boost::algorithm::erase_all_copy(instance.ShortDebugString(), "\"") << "\"];" << std::endl;

    oss << std::endl;

    for (const auto& instance : graph)
    {
        for (const auto& remote : instance.instances())
            oss << "\t\"" << instance.id() << "\" -- \"" << remote.id() << "\";" << std::endl;
    }


    oss << "}" << std::endl;


    return oss.str();
}

} // namespace rpc
