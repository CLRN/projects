#ifndef rpc_exceptions_h__
#define rpc_exceptions_h__

#include "exception/Exception.h"

namespace rpc
{

//! Rpc exception
DECLARE_SIMPLE_EXCEPTION_CLASS(Exception, cmn::Exception);

boost::exception_ptr MakeException(const std::string& text);
std::string GetExceptionText(const boost::exception_ptr& e);

} // namespace rpc

#endif // rpc_exceptions_h__