#include "Balancer.h"

#include <boost/make_shared.hpp>

namespace rpc
{
namespace
{

class Balancer : public IBalancer
{
public:
    Balancer(ILog& log) : m_Log(log)
    {

    }

private:

    ILog& m_Log;
};


} // anonymous namespace


IBalancer::Ptr IBalancer::Instance(ILog& log)
{
    return boost::make_shared<Balancer>(log);
}

} // namespace rpc
