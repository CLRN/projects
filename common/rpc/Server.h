#ifndef RpcServer_h__
#define RpcServer_h__

#include "Base.h"
#include "net/IHost.h"
#include "Endpoint.h"

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

class ILog;

namespace rpc
{

class IServer : public Endpoint
{
public:
    typedef boost::shared_ptr<IServer> Ptr;
    typedef boost::function<void(const boost::exception_ptr& e, const InstanceId& id)> ConnectCallback;

    IServer(ILog& log, const InstanceId& id) : Endpoint(log, id) {}
    virtual ~IServer() {}

    virtual void Start() = 0;
    virtual void SetOnClientConnection(const ConnectCallback& cb) = 0;

    //! Instance
    static Ptr Instance(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id);
};

} // namespace rpc


#endif // RpcServer_h__
