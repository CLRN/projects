#include "Server.h"
#include "Exceptions.h"
#include "exception/CheckHelpers.h"
#include "common/ILog.h"
#include "common/Modules.h"
#include "Stream.h"

#include <boost/make_shared.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/thread/mutex.hpp>

namespace rpc
{
namespace
{

DECLARE_CURRENT_MODULE(Modules::Rpc);

class IChannelCallback
{
public:
    struct ChannelDesc
    {
        ISequencedChannel::Ptr m_Channel;
        InstanceId m_Id;
    };
    typedef std::vector<ChannelDesc> Channels;
    virtual ISequencedChannel::Ptr GetChannelByInstanceId(const InstanceId& id) const = 0;
    virtual Channels GetAllChannels() const = 0;
    virtual bool CanWeHandleThis(rpc::IService::Id service) const = 0;
};


//! Fake channel, not mapped to any connection, in each call through this channel it must determine 
//! client instance according to call params and make a call
//! This channel used to only for outgoing server calls and for convenient local services registering.
//! 1. Server code wants to execute some remote RPC: invoke Stub(), than ServerChannel::CallMethod(), 
//! find appropriate client channel(or multiple channels) and forward call to ClientChannel::CallMethod()
//! 2. Server code provides RPC service through ServerChannel::ProvideService, than new client channel
//! uses all provided services to handle requests to server through calling ServerChannel::CopyServices.
class ServerChannel : public IChannel
{
    typedef std::vector<rpc::IService*> ServiceHandlers;
public:

    ServerChannel(ILog& log, IRouter& router, IChannelCallback& cb) : m_Log(log), m_Router(router), m_Callback(cb) {}

    rpc::IService& Ptr2Ref(rpc::IService* s) const
    {
        return *s;
    }

    void CopyServices(rpc::ISequencedChannel& other) const
    {
        boost::for_each(m_LocalServices, boost::bind(&IChannel::ProvideService, &other, boost::bind(&ServerChannel::Ptr2Ref, this, _1)));
    }

private:
    virtual void SetConnection(const net::IConnection::Ptr& connection) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void ProvideService(rpc::IService& service) override
    {
        m_LocalServices.push_back(&service);
    }

    virtual void SetTimeout(const boost::posix_time::time_duration& timeout) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void AddHandler(details::IRequestHandler& handler) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual void OnIncomingData(const IStream& stream) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual Services GetLocalServices() const override
    {
        Services result;
        boost::transform(m_LocalServices, std::inserter(result, result.end()), boost::bind(&IService::GetId, _1));
        return result;
    }

    virtual void Close() override
    {
    }

    virtual IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, const gp::Message& request, const IStream& stream, const details::CallParams& params) override
    {
        if (params.m_Type == choose::All)
        {
            const auto all = m_Callback.GetAllChannels();
            for (const auto& c : all)
            {
                if (!m_Router.IsExists(c.m_Id, method.service()->index()))
                    continue;

                try
                {
                    c.m_Channel->CallMethod(method, request, stream, params);
                }
                catch (const std::exception& e)
                {
                    LOG_ERROR("Failed to deliver rpc call to all, packet: [%s][%s]: %s") % method.full_name() % request.DebugString() % cmn::ExceptionInfo(e);
                }
            }

            return IFuture::Ptr();
        }
        else
        {
            const IService::Id service = method.service()->options().GetExtension(proto::service_id);
            const auto instanceId = m_Router.GetNextNode(params, service);

            const auto channel = m_Callback.GetChannelByInstanceId(instanceId);
            return channel->CallMethod(method, request, stream, params);
        }
    }

    virtual void ForwardCall(const gp::Message& base, const IStream& stream, const net::IConnection::Ptr& connection) override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

    virtual net::IConnection::Ptr GetConnection() const override
    {
        throw std::logic_error("The method or operation is not implemented.");
    }

private:
    ILog& m_Log;
    ServiceHandlers m_LocalServices;
    IRouter& m_Router;
    IChannelCallback& m_Callback;
};

class RequestRedirector : public rpc::details::IRequestHandler
{
public:
    RequestRedirector(ILog& log, IRouter& router, IChannelCallback& cb) : m_Log(log), m_Router(router), m_Callback(cb) {}

    virtual bool Handle(const gp::Message& baseInput, const IStream& stream, const rpc::ISequencedChannel::Ptr& callerChannel) override
    {
        const proto::BasePacket& base = static_cast<const proto::BasePacket&>(baseInput);
        if (base.dispatch() == proto::BasePacket::All)
        {
            // do not forward call back to the caller
            const auto all = m_Callback.GetAllChannels();
            const auto range = all | boost::adaptors::filtered([&callerChannel, this, &base](const IChannelCallback::ChannelDesc& c){ 
                return c.m_Channel != callerChannel && m_Router.IsExists(c.m_Id, base.serviceid());
            });

            LOG_DEBUG("Firing event: [%s:%s], all: %s, range: %s") % base.serviceid() % base.method() % boost::distance(all) % boost::distance(range);

            boost::for_each(range, [&](const IChannelCallback::ChannelDesc& channel){

                const auto position = stream->tellg();
                try
                {
                    channel.m_Channel->ForwardCall(base, stream, callerChannel->GetConnection());
                }
                catch (const std::exception& e)
                {
                    LOG_ERROR("Failed to forward call [%s]: %s") % base.DebugString() % cmn::ExceptionInfo(e);
                }
                
                stream->clear();
                stream->seekg(position);
            });

            // return false because 'all' means all remotes and local instance too
            // so return false to be able to handle request by local handler
            return false;
        }
        
        if (base.has_instance() && !base.instance().empty() && base.instance() != m_Router.GetLocalId() || !m_Callback.CanWeHandleThis(base.serviceid()))
        {
            details::CallParams params;
            params.m_Type = choose::Closest;

            const auto instanceId = m_Router.GetNextNode(params, base.serviceid());
            const auto channel = m_Callback.GetChannelByInstanceId(instanceId);
            channel->ForwardCall(baseInput, stream, callerChannel->GetConnection());

            return true;
        }

        return false;
    }

private:
    ILog& m_Log;
    IRouter& m_Router;
    IChannelCallback& m_Callback;
};

class Server 
    : public IServer
    , public boost::enable_shared_from_this<Server>
    , public IChannelCallback
{
public:

    struct Client
    {
        typedef std::vector<Client> List;

        net::IConnection::Ptr m_Connection;
        rpc::ISequencedChannel::Ptr m_Channel;
        InstanceId m_Id;
    };

    Server(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id)
        : IServer(log, id)
        , m_Log(log)
        , m_Host(host)
        , m_Ep(ep)
        , m_Redirector(m_Log, *m_Router, *this)
    {
        // replace local channel with server behavior
        m_LocalChannel = boost::make_shared<ServerChannel>(m_Log, *m_Router, *this);
        m_LocalChannel->ProvideService(*m_RegistrationService);
    }
private:

    virtual void Start() override
    {
        m_Host.Receive(m_Ep, boost::bind(&Server::OnClientConnection, shared_from_this(), _1, _2));
    }

    virtual void SetOnClientConnection(const ConnectCallback& cb) override
    {
        m_Callback = cb;
    }

    virtual void Stop() override
    {
        m_Host.Close();

        Endpoint::Stop();
    }

    void OnClientConnection(const net::IConnection::Ptr& connection, const boost::system::error_code& e)
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        if (e)
        {
            const auto it = boost::find_if(m_Clients, boost::bind(&Client::m_Connection, _1) == connection);
            if (it == m_Clients.end())
                return; // unknown client
            
            const auto id = it->m_Id;
            m_Router->RemoveInstance(id);

            it->m_Channel->Close();
            it->m_Connection->Close();

            m_Clients.erase(it);
            try
            {
                THROW(Exception("Client disconnected"), id, e.message());
            }
            catch (const std::exception&)
            {
                m_Callback(boost::current_exception(), id);
            }
        }
        else
        {
            Client client;

            // create new RPC channel and bind to connection
            client.m_Connection = connection;
            client.m_Channel = rpc::IChannel::Instance(m_Log, m_Router);
            client.m_Channel->SetConnection(connection);
            client.m_Connection->Receive(boost::bind(&IChannel::OnIncomingData, client.m_Channel, _1));

            // copy provided services
            static_cast<const ServerChannel&>(*m_LocalChannel).CopyServices(*client.m_Channel);

            // add redirect handler
            client.m_Channel->AddHandler(m_Redirector);

            // request client id
            const auto instance(shared_from_this());
            try
            {
                proto::RegistrationService::Stub(*client.m_Channel).GetInfo(proto::Empty()).Async([instance, client, this](const rpc::Future<proto::RegistrationInfo>& future){
                    try
                    {
                        const auto id = future.Response().instances(0).id();

                        LOG_TRACE("Received client id response: %s") % future.Response().ShortDebugString();

                        Client copy(client);
                        copy.m_Id = id;

                        {
                            boost::unique_lock<boost::mutex> lock(m_Mutex);
                            m_Clients.push_back(std::move(copy));
                        }
                        m_Router->AddNeighbor(future.Response().instances(0));

                        if (m_Callback)
                            m_Callback(boost::exception_ptr(), id);
                    }
                    catch (const std::exception& e)
                    {
                        LOG_ERROR("Failed to query client Id: %s") % cmn::ExceptionInfo(e);
                    }
                });
            }
            catch (const std::exception& e)
            {
                LOG_ERROR("Failed to call GetInfo: %s") % cmn::ExceptionInfo(e);
            }
        }
    }

    virtual ISequencedChannel::Ptr GetChannelByInstanceId(const InstanceId& id) const override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        const auto it = boost::find_if(m_Clients, boost::bind(&Client::m_Id, _1) == id);
        CHECK(it != m_Clients.end(), Exception("Failed to find client by id"), id);
        return it->m_Channel;
    }

    virtual Channels GetAllChannels() const override
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);

        Channels result(m_Clients.size());
        boost::transform(m_Clients, result.begin(), [](const Client& c){
            IChannelCallback::ChannelDesc desc = { c.m_Channel, c.m_Id };
            return desc;
        });
        return result;
    }

    virtual bool CanWeHandleThis(rpc::IService::Id service) const override
    {
        return !!m_LocalChannel->GetLocalServices().count(service);
    }

private:

    ILog& m_Log;
    net::IHost& m_Host;
    const net::IHost::Endpoint m_Ep;
    ConnectCallback m_Callback;
    Client::List m_Clients;
    RequestRedirector m_Redirector;
    mutable boost::mutex m_Mutex;
};


} // anonymous namespace


IServer::Ptr IServer::Instance(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id)
{
    return boost::make_shared<Server>(log, host, ep, id);
}

} // namespace rpc
