#ifndef Endpoint_h__
#define Endpoint_h__

#include "Base.h"
#include "protocols/rpc_base.pb.h"
#include "Router.h"
#include "Channel.h"

#include <vector>

#include <boost/shared_ptr.hpp>

class ILog;

namespace rpc
{

//! RPC endpoint abstraction, base class for Client and Service endpoints
class Endpoint
{
public:
    Endpoint(ILog& log, const InstanceId& id);

    //! Provide RPC service
    void ProvideService(rpc::IService& svc);

    //! Stop endpoint
    virtual void Stop();

    //! Get RPC channel
    template<typename Service>
    typename Service::Stub Channel(const details::CallParams& params = details::CallParams());

    //! Get network graph
    IRouter::Instances GetNetwork() const;

    //! Get local instance id
    const InstanceId& GetId() const;

    virtual details::IChannel& GetChannel(const IService::Id& id);

protected:

    IRouter::Ptr m_Router;
    IChannel::Ptr m_LocalChannel;
    std::unique_ptr<IService> m_RegistrationService;
};

template<typename Service>
typename Service::Stub Endpoint::Channel(const details::CallParams& params)
{
    const auto id = Service::descriptor().options().GetExtension(proto::service_id);
    return Service::Stub(GetChannel(id), params);
}


} // namespace rpc
#endif // Endpoint_h__
