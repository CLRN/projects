#include "Endpoint.h"
#include "common/ProtoHelpers.h"

#include <boost/range/algorithm.hpp>

namespace rpc
{

namespace
{

class RegistrationService : public proto::RegistrationService
{
public:
    RegistrationService(IRouter& router) : m_Router(router)
    {

    }
    virtual void Register(const rpc::Request<::proto::RegistrationInfo>::Ptr& request, const rpc::Response<::proto::Empty>::Ptr& response) override
    {
        // update remote instances
        IRouter::Instances list;
        boost::copy(request->instances(), std::back_inserter(list));
        m_Router.UpdateInfo(list);
    }
    virtual void Unregister(const rpc::Request<::proto::RegistrationInfo>::Ptr& request, const rpc::Response<::proto::Empty>::Ptr&) override
    {
        for (const auto& instance : request->instances())
            m_Router.RemoveInstance(instance.id());
    }
    virtual void GetInfo(const rpc::Request<::proto::Empty>::Ptr& request, const rpc::Response<::proto::RegistrationInfo>::Ptr& response) override
    {
        // send our info
        boost::copy(m_Router.GetInstances(), proto::hlp::MakeProtoInserter(*response->mutable_instances()));
    }
private:

    IRouter& m_Router;
};

} // anonymous namespace

Endpoint::Endpoint(ILog& log, const InstanceId& id)
    : m_Router(IRouter::Instance(log, id))
    , m_LocalChannel(IChannel::Instance(log, m_Router))
    , m_RegistrationService(new RegistrationService(*m_Router))
{
    m_LocalChannel->ProvideService(*m_RegistrationService);
    m_Router->AddLocalService(m_RegistrationService->GetId());
}


void Endpoint::ProvideService(rpc::IService& svc)
{
    m_LocalChannel->ProvideService(svc);
    m_Router->AddLocalService(svc.GetId());
}

void Endpoint::Stop()
{
    if (m_LocalChannel)
        m_LocalChannel->Close();
}

details::IChannel& Endpoint::GetChannel(const IService::Id& /*id*/)
{
    return *m_LocalChannel; // by default return local channel, this behavior is default for client endpoint
}

IRouter::Instances Endpoint::GetNetwork() const
{
    return m_Router->GetInstances();
}

const InstanceId& Endpoint::GetId() const
{
    return m_Router->GetLocalId();
}

} // namespace rpc
