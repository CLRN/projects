#ifndef Stream_h__
#define Stream_h__

#include "net/IConnection.h"
#include "rpc/Exceptions.h"

#include "rpc/Base.h"

#include <google/protobuf/message.h>

#include <boost/cstdint.hpp>
#include <boost/scope_exit.hpp>

namespace rpc
{
namespace details
{

class ReadStream
{
public:
    static boost::uint32_t Read(std::istream& s, gp::Message& message)
    {
        boost::uint32_t size = 0;
        s.read(reinterpret_cast<char*>(&size), sizeof(boost::uint32_t));
        if (!size)
            return size;

        if (size <= 4096)
        {
            char buffer[4096];
            s.read(buffer, size);
            CHECK(message.ParseFromArray(buffer, size), Exception("Failed to parse incoming packet"));
        }
        else
        {
            std::unique_ptr<char[]> buffer(new char[size]);
            s.read(buffer.get(), size);
            CHECK(message.ParseFromArray(buffer.get(), size), Exception("Failed to parse incoming packet"));
        }
        return size;
    }
};

class WriteStream
{
public:
    WriteStream(const net::IConnection::Ptr& stream)
        : m_Stream(stream)
    {
    }

    void Write(const gp::Message& base, const gp::Message* request = nullptr, const rpc::IStream& stream = rpc::IStream())
    {
        const boost::uint32_t baseSize = base.ByteSize();
        const boost::uint32_t requestSize = request ? request->ByteSize() : 0;
        const auto totalSize = baseSize + requestSize + sizeof(baseSize) + (request ? sizeof(requestSize) : 0);

        char staticBuffer[4096];
        bool allocated = false;
        char* buffer = nullptr;
        BOOST_SCOPE_EXIT(&buffer, &allocated) 
        {
            if (allocated)
                delete[] buffer;
        }
        BOOST_SCOPE_EXIT_END;
        if (totalSize > _countof(staticBuffer))
        {
            buffer = new char[totalSize];
            allocated = true;
        }
        else
        {
            buffer = staticBuffer;
        }

        *reinterpret_cast<boost::uint32_t*>(buffer) = baseSize;
        CHECK(base.SerializeToArray(buffer + sizeof(baseSize), baseSize), Exception("Failed to serialize base packet"));

        if (request)
        {
            *reinterpret_cast<boost::uint32_t*>(buffer + baseSize + sizeof(baseSize)) = requestSize;
            CHECK(request->SerializeToArray(buffer + baseSize + sizeof(baseSize) + sizeof(requestSize), requestSize), Exception("Failed to serialize base packet"));
        }
        m_Stream->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer, totalSize)));

        if (stream)
            SendStream(stream); // TODO: make this call async, because big streams may block rpc call for a long time
    }

private:

    void SendStream(const rpc::IStream& stream)
    {
        // write stream data if available
        char buffer[4096];
        std::size_t sent = 0;
        for (;;)
        {
            stream->read(buffer, _countof(buffer));

            const std::size_t read = static_cast<std::size_t>(stream->gcount());
            m_Stream->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer, read)));
            sent += read;
            if (read != _countof(buffer))
                break;
        }
    }

private:
    net::IConnection::Ptr m_Stream;
};

} // namespace details
} // namespace rpc

#endif // Stream_h__
