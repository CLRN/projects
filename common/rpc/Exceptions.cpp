#include "Exceptions.h"
#include "exception/CheckHelpers.h"
#include "conversion/AnyCast.h"

namespace rpc
{

boost::exception_ptr MakeException(const std::string& text)
{
    try
    {
        THROW(Exception(text.c_str()));
    }
    catch (const std::exception&)
    {
        return boost::current_exception();
    }
}

std::string GetExceptionText(const boost::exception_ptr& e)
{
    try
    {
        boost::rethrow_exception(e);
    }
    catch (const std::exception& e)
    {
        return conv::cast<std::string>(cmn::ExceptionInfo(e));
    }
}

} // namespace rpc
