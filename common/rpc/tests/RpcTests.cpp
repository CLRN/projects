#include "common/Log.h"
#include "common/Modules.h"
#include "net/IConnection.h"
#include "common/ServiceHolder.h"
#include "net/PipeHost.h"
#include "protocols/test_service.pb.h"
#include "common/AsioHelpers.h"
#include "net/FileHost.h"
#include "rpc/Server.h"
#include "rpc/Client.h"
#include "rpc/Exceptions.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <google/protobuf/descriptor.h>

#include <iostream>

#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/thread/future.hpp>
#include <boost/thread/detail/thread_group.hpp>
#include <boost/make_shared.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/range/algorithm.hpp>

using testing::Range;
using testing::Combine;
using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Invoke;
using ::testing::Expectation;

class TestService : public proto::test::TestService
{
public:
    virtual void TestMethod(const rpc::Request<::proto::test::Request>::Ptr& request,
        const rpc::Response<::proto::test::Response>::Ptr& response) override
    {
        response->set_data(request->data() + 1);

        if (request->Stream())
        {
            const auto stream = boost::make_shared<std::stringstream>();
            std::copy(std::istream_iterator<char>(*request->Stream()), std::istream_iterator<char>(), std::ostream_iterator<char>(*stream));
            response->Stream(stream);
        }

        ++m_Calls;
    }

    std::atomic<unsigned> m_Calls;
};

class MockedConnectCallback
{
public:
    MOCK_METHOD2(OnConnection, void(const boost::exception_ptr& e, const rpc::InstanceId& id));
};

const std::string SERVER_ID = "server";
const std::string FIRST_CLIENT_ID = "client 1";
const std::string SECOND_CLIENT_ID = "client 2";
const std::string THIRD_CLIENT_ID = "client 3";
const std::string FOURTH_CLIENT_ID = "client 4";
Log g_Log;

TEST(Rpc, SuccessfullConnect)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);
    
    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));
    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client->Connect();
    cmn::Service::Instance().poll(); // server: accept client and query client id
    cmn::Service::Instance().poll(); // client: handle server request, return client id 
}

TEST(Rpc, UnsuccessfullConnect)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    EXPECT_THROW(client->Connect(), rpc::Exception);
}

TEST(Rpc, AsynCalls)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));

    TestService service;
    server->ProvideService(service);
    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client->Connect();
    cmn::Service::Instance().poll(); // server: accept client and query client id
    cmn::Service::Instance().poll(); // client: handle server request, return client id 

    for (int i = 0; i < 10; ++i)
    {
        proto::test::Request request;
        request.set_data(10);
        client->Channel<proto::test::TestService>().TestMethod(request).Async([](const rpc::Future<proto::test::Response>& response){
            EXPECT_EQ(response.Response().data(), 11);
        });
    }

    cmn::Service::Instance().poll();
}

TEST(Rpc, SynCalls)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));

    TestService service;
    server->ProvideService(service);
    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client->Connect();
    cmn::Service::Instance().poll(); // server: accept client and query client id
    cmn::Service::Instance().poll(); // client: handle server request, return client id 

    for (int i = 0; i < 10; ++i)
    {
        proto::test::Request request;
        request.set_data(10);
        const auto response = client->Channel<proto::test::TestService>().TestMethod(request);

        EXPECT_EQ(response.Response().data(), 11);
    }

    cmn::Service::Instance().poll();
}

TEST(Rpc, AsynCallsStream)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));

    TestService service;
    server->ProvideService(service);
    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client->Connect();
    cmn::Service::Instance().poll(); // server: accept client and query client id
    cmn::Service::Instance().poll(); // client: handle server request, return client id 
    cmn::Service::Holder holder(1);

    std::string bigData(4096, 'a');

    for (int i = 0; i < 10; ++i)
    {
        proto::test::Request request;
        request.set_data(10);
        client->Channel<proto::test::TestService>().TestMethod(request, boost::make_shared<std::stringstream>(bigData)).Async([bigData](const rpc::Future<proto::test::Response>& response){
            EXPECT_EQ(response.Response().data(), 11);

            std::string text;
            std::copy(std::istream_iterator<char>(*response.Stream()), std::istream_iterator<char>(), std::back_inserter(text));

            EXPECT_EQ(text, bigData);
        });
    }

    cmn::Service::Instance().poll();
}

TEST(Rpc, SynCallsStream)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));

    TestService service;
    server->ProvideService(service);
    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client->Connect();
    cmn::Service::Instance().poll(); // server: accept client and query client id
    cmn::Service::Instance().poll(); // client: handle server request, return client id 
    cmn::Service::Holder holder(1);

    std::string bigData(4096, 'a');
    for (int i = 0; i < 10; ++i)
    {
        proto::test::Request request;
        request.set_data(10);
        const auto response = client->Channel<proto::test::TestService>().TestMethod(request, boost::make_shared<std::stringstream>(bigData));

        EXPECT_EQ(response.Response().data(), 11);

        std::string text;
        std::copy(std::istream_iterator<char>(*response.Stream()), std::istream_iterator<char>(), std::back_inserter(text));

        EXPECT_EQ(text, bigData);
    }

    cmn::Service::Instance().poll();
}


TEST(Rpc, ForwardCalls)
{
    const auto host = boost::make_shared<net::pipes::Transport>(g_Log);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();
    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);
    const auto client1 = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FIRST_CLIENT_ID);
    const auto client2 = rpc::IClient::Instance(g_Log, *host, testInfo->name(), SECOND_CLIENT_ID);
    const auto client3 = rpc::IClient::Instance(g_Log, *host, testInfo->name(), THIRD_CLIENT_ID); 
    const auto client4 = rpc::IClient::Instance(g_Log, *host, testInfo->name(), FOURTH_CLIENT_ID);

    TestService service;
    client1->ProvideService(service);

    MockedConnectCallback callback;
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FIRST_CLIENT_ID)).Times(Exactly(1));
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), SECOND_CLIENT_ID)).Times(Exactly(1));
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), THIRD_CLIENT_ID)).Times(Exactly(1));
    EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), FOURTH_CLIENT_ID)).Times(Exactly(1));

    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    client1->Connect();
    client2->Connect();
    client3->Connect();
    client4->Connect();
    cmn::Service::Instance().poll();

    cmn::Service::Holder reader(1);

    static const std::size_t size = 1024 * 1024 * 20; // 20 mb
    const auto stream = boost::make_shared<std::stringstream>();
    std::string buffer(1024, 'a');
    for (int i = 0; i < size / 1024; ++i)
        stream->write(buffer.c_str(), buffer.size());

    proto::test::Request request;
    request.set_data(10);
    client2->Channel<proto::test::TestService>().TestMethod(request, stream).Async([](const rpc::Future<proto::test::Response>& response){
        EXPECT_EQ(response.Response().data(), 11);

        const auto pos = response.Stream()->tellg();
        response.Stream()->seekg(0, std::ios::end);
        const std::size_t streamSize = static_cast<std::size_t>(response.Stream()->tellg() - pos);

        EXPECT_EQ(streamSize, size);

        cmn::Service::Instance().stop();
    });

    cmn::Service::Instance().run();
}

TEST(Rpc, ManyClients)
{
    static const std::size_t clientsCount = 10;
    cmn::Service::Holder holder(3);

    const auto host = boost::make_shared<net::pipes::Transport>(g_Log, clientsCount);

    const ::testing::TestInfo* const testInfo = ::testing::UnitTest::GetInstance()->current_test_info();

    const auto server = rpc::IServer::Instance(g_Log, *host, testInfo->name(), SERVER_ID);

    std::vector<rpc::IClient::Ptr> clients;
    MockedConnectCallback callback;
    for (int i = 0; i < clientsCount; ++i)
    {
        const std::string id = std::string("client ") + conv::cast<std::string>(i);
        clients.push_back(rpc::IClient::Instance(g_Log, *host, testInfo->name(), id));
        EXPECT_CALL(callback, OnConnection(boost::exception_ptr(), id)).Times(Exactly(1));
    }

    server->SetOnClientConnection(boost::bind(&MockedConnectCallback::OnConnection, &callback, _1, _2));
    server->Start();

    boost::for_each(clients, boost::bind(&rpc::IClient::Connect, _1, rpc::IClient::ConnectCallback()));
    cmn::Service::Instance().poll();

    const auto net = rpc::Visualize(clients.front()->GetNetwork());
}
