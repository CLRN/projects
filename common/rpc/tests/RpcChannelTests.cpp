#include "common/Log.h"
#include "common/Modules.h"
#include "common/ServiceHolder.h"
#include "rpc/Channel.h"
#include "protocols/test_service.pb.h"
#include "common/AsioHelpers.h"
#include "rpc/Router.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <google/protobuf/descriptor.h>

#include <iostream>
#include <sstream>

#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/thread/future.hpp>
#include <boost/range/algorithm.hpp>

using testing::Range;
using testing::Combine;
using ::testing::_;
using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::Invoke;
using ::testing::Expectation;

void TestContent(std::istream& s)
{
    // copy data
    std::vector<char> testData;
    std::copy(std::istream_iterator<char>(s), std::istream_iterator<char>(), std::back_inserter(testData));
}

class Service : public proto::test::TestService
{
public:

    virtual void TestMethod(const rpc::Request<::proto::test::Request>::Ptr& request, const rpc::Response<::proto::test::Response>::Ptr& response) override
    {
        response->set_data(request->data() + 1);

        // copy stream back
        if (request->Stream())
        {
            const auto out = boost::make_shared<std::stringstream>();
            std::copy(std::istream_iterator<char>(*request->Stream()), std::istream_iterator<char>(), std::ostream_iterator<char>(*out));
            response->Stream(out);
        }
    }

};

class SimpleLocalConnection : public net::IConnection
{
public:

    SimpleLocalConnection() : m_Stream(boost::make_shared<std::stringstream>()) {}

    virtual void Send(const Buffers& data)
    {
        for (const auto& buffer : data)
            m_Stream->write(boost::asio::buffer_cast<const char*>(buffer), static_cast<std::streamsize>(boost::asio::buffer_size(buffer)));
    }
    virtual void Receive(const Callback& callback)
    {
        throw std::exception("The method or operation is not implemented.");
    }
    virtual void Close()
    {
        throw std::exception("The method or operation is not implemented.");
    }

    template<typename T>
    void WriteToChannel(T& channel)
    {
        channel.OnIncomingData(m_Stream);
    }
private:
    boost::shared_ptr<std::stringstream> m_Stream;
};

class SequencedLocalConnection : public net::IConnection
{
public:

    virtual void Send(const Buffers& data)
    {
        const auto stream = boost::make_shared<std::stringstream>();
        for (const auto& buffer : data)
            stream->write(boost::asio::buffer_cast<const char*>(buffer), static_cast<std::streamsize>(boost::asio::buffer_size(buffer)));
        m_Streams.push_back(stream);
    }
    virtual void Receive(const Callback& callback)
    {
        throw std::exception("The method or operation is not implemented.");
    }
    virtual void Close()
    {
        throw std::exception("The method or operation is not implemented.");
    }

    template<typename T>
    void WriteToChannel(T& channel)
    {
        boost::for_each(m_Streams, boost::bind(&T::OnIncomingData, &channel, _1));
    }
private:
    std::vector<rpc::IStream> m_Streams;
};

template<typename T>
struct ConnectionGetter;

template<>
struct ConnectionGetter<rpc::ISequencedChannel>
{
    typedef SimpleLocalConnection Type;
};

template<>
struct ConnectionGetter<rpc::IChannel>
{
    typedef SequencedLocalConnection Type;
};

Log g_Logger;

template<typename T>
void SynchronousWithoutStreamTest()
{
    const auto router = rpc::IRouter::Instance(g_Logger, rpc::InstanceId());

    // initialize client
    const auto clientConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto client = T::Instance(g_Logger, router);
    client->SetConnection(clientConnection);

    // send request
    proto::test::Request request;
    request.set_data(99);
    const auto future = proto::test::TestService::Stub(*client).TestMethod(request);

    // initialize server
    const auto serverConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto server = T::Instance(g_Logger, router);
    server->SetConnection(serverConnection);

    // parse client output stream by server channel
    Service service;
    server->ProvideService(service);
    clientConnection->WriteToChannel(*server);

    // process request by service
    cmn::Service::Instance().poll();

    // parse server output stream by client channel
    serverConnection->WriteToChannel(*client);

    // obtain and validate result from future
    EXPECT_EQ(future.Response().data(), 100);
}

template<typename T>
void SynchronousWithStreamTest()
{
    const auto router = rpc::IRouter::Instance(g_Logger, rpc::InstanceId());

    // initialize client
    const auto clientConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto client = T::Instance(g_Logger, router);
    client->SetConnection(clientConnection);

    // send request
    proto::test::Request request;
    request.set_data(1);
    const auto streamData = boost::make_shared<std::stringstream>("sometext");
    const auto future = proto::test::TestService::Stub(*client).TestMethod(request, streamData);

    // initialize server
    const auto serverConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto server = T::Instance(g_Logger, router);
    server->SetConnection(serverConnection);

    // parse client output stream by server channel
    Service service;
    server->ProvideService(service);
    clientConnection->WriteToChannel(*server);

    // process request by service
    cmn::Service::Instance().poll();

    // parse server output stream by client channel
    serverConnection->WriteToChannel(*client);

    // obtain and validate result from future
    EXPECT_EQ(future.Response().data(), 2);

    std::string out;
    *future.Stream() >> out;

    EXPECT_EQ(out, "sometext");
}


template<typename T>
void AsynchronousWithoutStream()
{
    const auto router = rpc::IRouter::Instance(g_Logger, rpc::InstanceId());

    // initialize client
    const auto clientConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto client = T::Instance(g_Logger, router);
    client->SetConnection(clientConnection);

    // set up future callback
    const auto callback = [](const rpc::Future<proto::test::Response>& future){

        // obtain and validate result from future
        EXPECT_EQ(future.Response().data(), 100);
    };

    // send request
    proto::test::Request request;
    request.set_data(99);
    proto::test::TestService::Stub(*client).TestMethod(request).Async(callback);

    // initialize server
    const auto serverConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto server = T::Instance(g_Logger, router);
    server->SetConnection(serverConnection);

    // parse client output stream by server channel
    Service service;
    server->ProvideService(service);
    clientConnection->WriteToChannel(*server);

    // process request by service
    cmn::Service::Instance().poll();

    // parse server output stream by client channel
    serverConnection->WriteToChannel(*client);
}


template<typename T>
void AsynchronousWithStreamTest()
{
    const auto router = rpc::IRouter::Instance(g_Logger, rpc::InstanceId());

    // initialize client
    const auto clientConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto client = T::Instance(g_Logger, router);
    client->SetConnection(clientConnection);

    // set up future callback
    const auto callback = [](const rpc::Future<proto::test::Response>& future){

        // obtain and validate result from future
        EXPECT_EQ(future.Response().data(), 2);

        std::string out;
        *future.Stream() >> out;

        EXPECT_EQ(out, "sometext");
    };

    // send request
    proto::test::Request request;
    request.set_data(1);
    const auto streamData = boost::make_shared<std::stringstream>("sometext");
    proto::test::TestService::Stub(*client).TestMethod(request, streamData).Async(callback);

    // initialize server
    const auto serverConnection = boost::make_shared<ConnectionGetter<T>::Type>();
    const auto server = T::Instance(g_Logger, router);
    server->SetConnection(serverConnection);

    // parse client output stream by server channel
    Service service;
    server->ProvideService(service);
    clientConnection->WriteToChannel(*server);

    // process request by service
    cmn::Service::Instance().poll();

    // parse server output stream by client channel
    serverConnection->WriteToChannel(*client);
}

TEST(SequencedRpcChannel, SynchronousWithoutStream)
{
    SynchronousWithoutStreamTest<rpc::ISequencedChannel>();
}

TEST(SequencedRpcChannel, SynchronousWithStream)
{
    SynchronousWithStreamTest<rpc::ISequencedChannel>();
}

TEST(SequencedRpcChannel, AsynchronousWithoutStream)
{
    AsynchronousWithoutStream<rpc::ISequencedChannel>();
}

TEST(SequencedRpcChannel, AsynchronousWithStream)
{
    AsynchronousWithStreamTest<rpc::ISequencedChannel>();
}

TEST(RpcChannel, SynchronousWithoutStream)
{
    SynchronousWithoutStreamTest<rpc::IChannel>();
}

TEST(RpcChannel, SynchronousWithStream)
{
    SynchronousWithStreamTest<rpc::IChannel>();
}

TEST(RpcChannel, AsynchronousWithoutStream)
{
    AsynchronousWithoutStream<rpc::IChannel>();
}

TEST(RpcChannel, AsynchronousWithStream)
{
    AsynchronousWithStreamTest<rpc::IChannel>();
}
