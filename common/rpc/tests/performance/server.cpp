#include "exception/CheckHelpers.h"
#include "common/Log.h"
#include "common/ServiceHolder.h"

#include "net/PipeHost.h"
#include "net/TCPHost.h"
#include "rpc/Server.h"

#include "protocols/test_service.pb.h"

#include <fstream>

#include <boost/assign/list_of.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>

/*

void TestRaw()
{
    Log log;
    const auto host = net::tcp::ITransport::Instance(log);
    cmn::Service::Holder holder(boost::thread::hardware_concurrency() - 1);

    unsigned counter = 0;

    host->Receive("127.0.0.1:6000", [&counter](const net::IConnection::Ptr& connection, const boost::system::error_code& e)
    {
        if (!e)
        {
            connection->Receive([connection, &counter](const net::IConnection::StreamPtr& stream){
                if (stream)
                {
                    const auto work = [connection, stream]()
                    {
                        char buffer[4096];
                        stream->read(buffer, _countof(buffer));
                        connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer, static_cast<std::size_t>(stream->gcount()))));
                    };

                    //work();
                    //cmn::Service::Instance().post(work);

                    ++counter;
                    //std::cout << counter << std::endl;
                }
            });
        }
    });

    cmn::Service::Instance().run();
}
*/
void TestRpc(const std::string& ep)
{
    Log log;
    log.Open(ILog::Level::Error);
    const auto host = net::tcp::ITransport::Instance(log);
    cmn::Service::Holder holder(boost::thread::hardware_concurrency() - 1);

    class Handler : public proto::test::TestService
    {
    public:
        Handler() : m_Counter() {}

        virtual void TestMethod(const rpc::Request<::proto::test::Request>::Ptr& request, const rpc::Response<::proto::test::Response>::Ptr& response) override
        {
            //std::cout << "request " << request->DebugString() << std::endl;
            response->set_data(0);
            if (request->Stream())
            {
                *request->Stream() >> std::noskipws;
                std::vector<char> data;
                std::copy(std::istream_iterator<char>(*request->Stream()), std::istream_iterator<char>(), std::back_inserter(data));

                response->set_data(data.size());
            }

            ++m_Counter;
        }
        unsigned m_Counter;
    };

    Handler h;
    const auto instance = rpc::IServer::Instance(log, *host, ep, "server");
    instance->ProvideService(h);
    instance->SetOnClientConnection([](const boost::exception_ptr& e, const rpc::InstanceId& id){
        std::cout << "client with id: '" << id << (e ? "' disconnected" : "' connected") << std::endl;
    });
    instance->Start();


    cmn::Service::Instance().run();
}

int main(int argc, char* argv[])
{
    try
    {
        //TestRaw();
        if (argc < 2)
        {
            std::cout << "endpoint expected, format 'host:port'" << std::endl;
            return 1;
        }
        TestRpc(argv[1]);
        return 0;
    }
    catch (const std::exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e) << std::endl;
        return 1;
    }
}

