#include "exception/CheckHelpers.h"
#include "common/Log.h"
#include "common/ServiceHolder.h"

#include "net/PipeHost.h"
#include "net/TCPHost.h"
#include "rpc/Client.h"

#include "protocols/test_service.pb.h"

#include <boost/assign/list_of.hpp>
#include <boost/thread.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/timer/timer.hpp>
#include <boost/chrono.hpp>
#include <boost/make_shared.hpp>

void TestRaw(const unsigned total, boost::timer::cpu_timer& timer)
{
    /*
    Log log;
    const auto host = net::tcp::ITransport::Instance(log);
    cmn::Service::Holder holder(1);

    unsigned counter = 0;

    const auto connection = host->Connect("127.0.0.1:6000");
    connection->Receive([&counter](const net::IConnection::StreamPtr& stream){
        if (stream)
            ++counter;
    });

    timer.start();
    char buffer[22];
    for (unsigned i = 0; i < total; ++i)
        connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer, _countof(buffer))));
    */
//     while (counter != total)
//         cmn::Service::Instance().poll();
}

class FakeHost : public net::IHost
{
public:

    class Connection : public net::IConnection
    {
    public:
        virtual void Send(const Buffers& /*data*/) override {}
        virtual void Receive(const Callback& /*callback*/) override {}
        virtual void Close() override {}
    };

    virtual net::IConnection::Ptr Connect(const std::string& /*endpoint*/) override
    {
        return boost::make_shared<Connection>();
    }

    virtual void Receive(const std::string& /*endpoint*/, const Callback& callback) override
    {
        callback(boost::make_shared<Connection>(), boost::system::error_code());
    }

    virtual void Close() override
    {
    }

};

void TestRpc(const unsigned total, boost::timer::cpu_timer& timer, const std::string& ep)
{
    Log log;
    const auto host = net::tcp::ITransport::Instance(log);
    cmn::Service::Holder holder(1);

    const auto instance = rpc::IClient::Instance(log, *host, ep, "client");

    boost::promise<void> connected;
    instance->Connect([&connected](const boost::exception_ptr&)
    { 
        connected.set_value(); 
    });
    auto future = connected.get_future();
    future.get();

    proto::test::Request request;
    request.set_data(1);

    const auto stream = boost::make_shared<std::stringstream>("test stream data");

    timer.start();
    unsigned counter = 0;
    for (unsigned i = 0; i < total; ++i)
    {
        instance->Channel<proto::test::TestService>().TestMethod(request, stream).Async([&counter](const rpc::Future<proto::test::Response>& r){
            ++counter;
        });
    }

     while (counter != total)
         cmn::Service::Instance().poll();
}

int main(int argc, char* argv[])
{
    try
    {
        boost::timer::cpu_timer timer;

        const unsigned total = 1000;

        if (argc < 2)
        {
            std::cout << "endpoint expected, format 'host:port'" << std::endl;
            return 1;
        }
        
        //TestRaw(total, timer);
        TestRpc(total, timer, argv[1]);
        timer.stop();

        const boost::chrono::nanoseconds nanosec(timer.elapsed().wall);
        const boost::chrono::nanoseconds system(timer.elapsed().system);
        const boost::chrono::nanoseconds user(timer.elapsed().user);
        std::cout
            << boost::chrono::duration_cast<boost::chrono::milliseconds>(nanosec) << " total, " 
            << boost::chrono::duration_cast<boost::chrono::milliseconds>(system) << " system, "
            << boost::chrono::duration_cast<boost::chrono::milliseconds>(user) << " user, "
            << boost::chrono::duration_cast<boost::chrono::microseconds>(nanosec).count() / total << " microseconds per call" << std::endl;
        return 0;
    }
    catch (const std::exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e) << std::endl;
        return 1;
    }
}

