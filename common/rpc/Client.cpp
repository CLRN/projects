#include "Client.h"
#include "Exceptions.h"
#include "exception/CheckHelpers.h"
#include "common/ILog.h"
#include "common/Modules.h"
#include "common/ProtoHelpers.h"
#include "common/ServiceHolder.h"

#include <boost/make_shared.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/deadline_timer.hpp>

namespace rpc
{
namespace
{

DECLARE_CURRENT_MODULE(Modules::Rpc);

class Client : public IClient, public boost::enable_shared_from_this<Client>
{
public:
    Client(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id)
        : IClient(log, id)
        , m_Log(log)
        , m_Host(host)
        , m_ServerEp(ep)
    {
    }

private:

    void DoConnect()
    {
        try
        {
            LOG_INFO("Connecting to: %s") % m_ServerEp;

            // connect and setup callback
            m_Connection = m_Host.Connect(m_ServerEp);
            m_LocalChannel->SetConnection(m_Connection);
            m_Connection->Receive(boost::bind(&Client::OnIncomingStream, shared_from_this(), _1));

            // do registration
            DoRegistration();
            if (m_Callback)
                m_Callback(boost::exception_ptr());
        }
        catch (const std::exception& e)
        {
            LOG_ERROR("Failed to connect to: %s, error: %s") % m_ServerEp % cmn::ExceptionInfo(e);

            if (!m_Callback)
                THROW(rpc::Exception("Failed to connect to server") << NESTED_CURRENT_EXCEPTION());

            const auto timer = boost::make_shared<boost::asio::deadline_timer>(cmn::Service::Instance());
            timer->expires_from_now(boost::posix_time::seconds(5));

            const auto instance(shared_from_this());
            timer->async_wait([instance, timer](const boost::system::error_code& e){
                if (!e)
                    instance->StartConnection();
            });

            if (m_Callback)
                m_Callback(boost::current_exception());
        }
    }

    void StartConnection()
    {
        cmn::Service::Instance().post(boost::bind(&Client::DoConnect, shared_from_this()));
    }

    virtual void Connect(const ConnectCallback& cb) override
    {
        m_Callback = cb;
        if (!m_Callback)
            DoConnect();
        else
            StartConnection();
    }

    virtual void Stop() override
    {
        LOG_INFO("Stopping client");

        m_Host.Close();
        if (m_Connection)
            m_Connection->Close();

        Endpoint::Stop();
    }

    void OnIncomingStream(const IStream& data)
    {
        if (!data)
        {
            LOG_ERROR("Server channel closed");
            StartConnection();
        }
        else
        {
            m_LocalChannel->OnIncomingData(data);
        }
    }

    void DoRegistration()
    {
        const auto instance(shared_from_this());
        Channel<proto::RegistrationService>().GetInfo(proto::Empty()).Async([instance, this](const rpc::Future<proto::RegistrationInfo>& data){

            LOG_INFO("Server registration info: %s") % data.Response().DebugString();

            IRouter::Instances list;
            boost::copy(data.Response().instances(), std::back_inserter(list));
            m_Router->UpdateInfo(list);
            m_Router->AddNeighbor(data.Response().instances(0)); // first element is always neighbor

            proto::RegistrationInfo local;
            boost::copy(m_Router->GetInstances(), proto::hlp::MakeProtoInserter(*local.mutable_instances()));

            LOG_DEBUG("Do registration with: %s") % local.ShortDebugString();

            // do registration on all known instances
            Channel<proto::RegistrationService>(rpc::choose::All).Register(local); // ignore response
        });
    }

private:

    ILog& m_Log;
    net::IHost& m_Host;
    const net::IHost::Endpoint m_ServerEp;
    net::IConnection::Ptr m_Connection;
    ConnectCallback m_Callback;
};


} // anonymous namespace


IClient::Ptr IClient::Instance(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id)
{
    return boost::make_shared<Client>(log, host, ep, id);
}

} // namespace rpc
