#include "Future.h"
#include "exception/CheckHelpers.h"
#include "rpc/Exceptions.h"
#include "common/AsioHelpers.h"
#include "common/ServiceHolder.h"
#include "Stream.h"

#include <google/protobuf/message.h>

#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <boost/optional.hpp>

namespace rpc
{

void details::ParseMessage(google::protobuf::Message& message, std::istream& s)
{
    details::ReadStream::Read(s, message);
}

namespace
{

class FutureImpl : public IFuture, public boost::enable_shared_from_this<FutureImpl>
{
public:

    virtual StreamPtr GetData() const override
    {
        boost::promise<StreamPtr> promise;

        {
            boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
            if (m_Exception)
                boost::rethrow_exception(m_Exception);

            if (m_Stream)
                return *m_Stream;

            const auto callback = [&promise](const IFuture::Ptr& f)
            {
                if (f->GetException())
                    promise.set_exception(f->GetException());
                else
                    promise.set_value(f->GetData());
            };

            GetData(callback);
        }

        auto future = promise.get_future();
        PollUntil(future);
        return future.get();
    }

    virtual void GetData(const Callback& c) const override
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_Callback = c;
    }

    virtual void SetData(const StreamPtr& stream) override
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_Stream = stream;
        InvokeCallback();
    }

    virtual void SetException(const boost::exception_ptr& e) override
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_Exception = e;
        InvokeCallback();
    }

    virtual boost::exception_ptr GetException() const override
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        return m_Exception;
    }

    void InvokeCallback()
    {
        Callback cb;
        {
            boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
            m_Callback.swap(cb);
        }

        if (cb)
            cb(shared_from_this());
    }

private:
    boost::optional<StreamPtr> m_Stream;
    mutable Callback m_Callback;
    boost::exception_ptr m_Exception;

    mutable boost::recursive_mutex m_Mutex;
};

} // anonymous namespace


IFuture::Ptr IFuture::Instance()
{
    return boost::make_shared<FutureImpl>();
}

} // namespace rpc