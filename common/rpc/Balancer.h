#ifndef Balancer_h__
#define Balancer_h__

#include "protocols/rpc_base.pb.h"

#include "Base.h"

#include <vector>

#include <boost/shared_ptr.hpp>

class ILog;

namespace rpc
{

class IBalancer
{
public:
    typedef boost::shared_ptr<IBalancer> Ptr;
    typedef std::vector<proto::RegistrationInfo::Instance> Instances;

    virtual ~IBalancer() {}
    
    //! Instance
    static Ptr Instance(ILog& log);
};

} // namespace rpc
#endif // Balancer_h__
