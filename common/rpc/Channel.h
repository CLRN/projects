#ifndef Channel_h__
#define Channel_h__

#include "Base.h"
#include "net/IConnection.h"
#include "Router.h"

#include <iosfwd>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>

class ILog;

namespace rpc
{

namespace details
{
struct IRequestHandler;
} // namespace details
    
class ISequencedChannel : public rpc::details::IChannel
{
public:
    typedef boost::shared_ptr<ISequencedChannel> Ptr;

    typedef std::set<unsigned> Services;

    virtual ~ISequencedChannel() {}
    virtual void ForwardCall(const gp::Message& base, const IStream& stream, const net::IConnection::Ptr& connection) = 0;
    virtual void SetConnection(const net::IConnection::Ptr& connection) = 0;
    virtual net::IConnection::Ptr GetConnection() const = 0;
    virtual void ProvideService(rpc::IService& service) = 0;
    virtual void SetTimeout(const boost::posix_time::time_duration& timeout) = 0;
    virtual void AddHandler(details::IRequestHandler& handler) = 0;
    virtual void OnIncomingData(const IStream& stream) = 0;
    virtual Services GetLocalServices() const = 0;
    virtual void Close() = 0;

    static Ptr Instance(ILog& log, const IRouter::Ptr& router);
};

class IChannel : virtual public ISequencedChannel
{
public:
    typedef boost::shared_ptr<IChannel> Ptr;

    static Ptr Instance(ILog& log, const IRouter::Ptr& router);
};

namespace details
{

//! RPC request handler, may be set as additional request handler for rpc channel
struct IRequestHandler
{
    virtual ~IRequestHandler() {}

    //! Handle request
    //!\return true if request handled, false otherwise
    virtual bool Handle(const gp::Message& base, const IStream& stream, const rpc::ISequencedChannel::Ptr& channel) = 0;
};

} // namespace details
} // namespace rpc

#endif // Channel_h__
