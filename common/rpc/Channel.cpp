#include "Channel.h"
#include "exception/CheckHelpers.h"
#include "rpc/Exceptions.h"
#include "common/ServiceHolder.h"
#include "conversion/AnyCast.h"
#include "Stream.h"
#include "net/DataSequence.h"
#include "net/ConnectionStream.h"
#include "Router.h"
#include "common/Modules.h"
#include "common/ILog.h"
#include "Router.h"

#include "protocols/rpc_base.pb.h"

#include <vector>
#include <atomic>

#include <google/protobuf/descriptor.h>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/function.hpp>


namespace rpc
{

namespace
{

DECLARE_CURRENT_MODULE(Modules::Rpc);

class SequencedChannel
    : virtual public rpc::ISequencedChannel
    , boost::noncopyable
    , public boost::enable_shared_from_this < SequencedChannel >
{
public:

    class LocalServiceHandler : public details::IRequestHandler
    {
    public:

        LocalServiceHandler(SequencedChannel& channel) : m_Parent(channel) {}

        bool WriteEventResponse(const proto::BasePacket& baseMessage, const rpc::ISequencedChannel::Ptr& channel)
        {
            const auto connection = channel->GetConnection();
            details::WriteStream writer(connection);

            try
            {
                proto::BasePacket base(baseMessage);
                base.set_direction(proto::BasePacket::Response);

                writer.Write(base, nullptr, rpc::IStream());
            }
            catch (const std::exception& e)
            {
                LERROR(m_Parent.m_Log, CURRENT_MODULE_ID, "Failed to write response: %s") % cmn::ExceptionInfo(e);
            }
            return true;
        }

        virtual bool Handle(const gp::Message& baseMessage, const IStream& stream, const rpc::ISequencedChannel::Ptr& channel) override
        {
            const auto& currentBase = static_cast<const proto::BasePacket&>(baseMessage);
            const auto it = boost::find_if(m_Parent.m_LocalServices, boost::bind(&IService::GetId, _1) == currentBase.serviceid());
            if (currentBase.dispatch() == proto::BasePacket::All && it == m_Parent.m_LocalServices.end())
                return WriteEventResponse(currentBase, channel); // simply ignore event

            CHECK(it != m_Parent.m_LocalServices.end(), Exception("Unable to handle request, service is not supported"), currentBase.ShortDebugString());

            IService& service = **it;

            // get method description from service
            const google::protobuf::MethodDescriptor* methodDesc = service.GetDescriptor().method(currentBase.method());
            assert(methodDesc);

            // prepare request and response
            std::unique_ptr<gp::Message> rawRequest(service.CreateRequest(*methodDesc));
            std::unique_ptr<gp::Message> rawResponse(service.CreateResponse(*methodDesc));

            // parse request from stream
            details::ReadStream::Read(*stream, *rawRequest);

            // assign stream if more data exist
            if (stream)
            {
                const auto pos = stream->tellg();
                stream->seekg(0, std::ios::end);
                if (pos != stream->tellg())
                {
                    stream->clear();
                    stream->seekg(pos);
                    dynamic_cast<details::StreamHolder<IStream>&>(*rawRequest).Stream(stream);
                }
            }

            struct LocalHolder : public details::StreamHolder < IStream >
            {
                void SetChannel(details::IChannel* c) { m_Channel = c; }
            };

            // get output stream
            auto holder = dynamic_cast<details::StreamHolder<IStream>*>(rawResponse.get());
            assert(holder && "Can't extract stream holder from response, generated service is invalid");

            // initialize request and response with current channel
            static_cast<LocalHolder&>(*holder).SetChannel(&m_Parent);
            static_cast<LocalHolder&>(dynamic_cast<details::StreamHolder<IStream>&>(*rawRequest)).SetChannel(&m_Parent);

            const auto base = boost::make_shared<proto::BasePacket>(currentBase);
            base->set_direction(proto::BasePacket::Response);
            if (m_Parent.m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace) || currentBase.debug_size())
                base->add_debug((boost::format("[%s]: Handling request [%s] by local handler") % m_Parent.m_Router->GetLocalId() % methodDesc->full_name()).str());

            const auto instance(m_Parent.shared_from_this());

            const MessagePtr request(rawRequest.release());
            const MessagePtr response(rawResponse.release(), [holder, base, instance, this, channel](gp::Message* m){

                // ensure that message will be deleted
                std::unique_ptr<gp::Message> guard(m);

                // this method will be invoked when response is out of user code scope
                // and there is no more references. so we are ready to send it.
                if (!base->has_error())
                {
                    try
                    {
                        if (holder->GetException())
                            boost::rethrow_exception(holder->GetException());
                        CHECK(m->IsInitialized(), Exception("Failed to send response, not initialized"));
                    }
                    catch (const std::exception& e)
                    {
                        m = nullptr;
                        base->set_error(conv::cast<std::string>(cmn::ExceptionInfo(e)));
                    }
                }
                else
                {
                    m = nullptr;
                }

                // log error if exists
                if (base->has_error())
                {
                    LERROR(m_Parent.m_Log, CURRENT_MODULE_ID, "Sending error response: %s") % base->error();
                }
                else
                {
                    LTRACE(m_Parent.m_Log, CURRENT_MODULE_ID, "[%s]: Sending response packet: %s") % m_Parent.m_Router->GetLocalId() % base->DebugString();
                }


                // serialize response
                const auto connection = channel->GetConnection();
                details::WriteStream writer(connection);

                try
                {
                    writer.Write(*base, m, holder->Stream());
                }
                catch (const std::exception& e)
                {
                    connection->Close();
                    LERROR(m_Parent.m_Log, CURRENT_MODULE_ID, "Failed to write response: %s") % cmn::ExceptionInfo(e);
                }
            });

            const auto handler = [&service, methodDesc, request, response, base]()
            {
                try
                {
                    try
                    {
                        service.CallMethod(*methodDesc, request, response);
                    }
                    CATCH_PASS(Exception("Service handler failed"), (service.GetDescriptor().full_name(), "service"), (methodDesc->full_name(), "method"));
                }
                catch (const std::exception&)
                {
                    base->set_error(GetExceptionText(boost::current_exception()));
                }
            };

            cmn::Service::Instance().post(handler);
            return true;
        }
    private:
        SequencedChannel& m_Parent;
    };


    typedef std::set<rpc::IService*> LocalServiceList;
    typedef boost::shared_ptr<boost::asio::deadline_timer> TimerPtr;
    typedef boost::function<void(const IStream&)> StreamCallback;
    typedef std::vector<details::IRequestHandler*> Handlers;

    struct FutureResponse
    {
        FutureResponse() : m_Future(IFuture::Instance()) {}
        TimerPtr m_Timer;
        IFuture::Ptr m_Future;
    };

    typedef std::map<std::size_t, FutureResponse> ResponseMap;

    SequencedChannel(ILog& log, const IRouter::Ptr& router)
        : m_Log(log)
        , m_Router(router)
        , m_Connection()
        , m_Timeout()
        , m_LocalHandler(*this)
    {
        AddHandler(m_LocalHandler);
    }

protected:
    virtual void OnIncomingData(const IStream& stream)
    {
        // execute this asynchronously, because we must release current context as soon as possible
        if (!stream)
            Close();
        else
            cmn::Service::Instance().post(boost::bind(&SequencedChannel::HandleBasePacket, shared_from_this(), stream));
    }

    virtual void SetConnection(const net::IConnection::Ptr& connection) override
    {
        m_Connection = connection;
    }

    proto::BasePacket::DispatchType MapDispatchType2Proto(const rpc::details::CallParams& params)
    {
        if (!params.m_Id.empty())
        {
            assert(params.m_Type == choose::Any);
            return proto::BasePacket::Id;
        }

        switch (params.m_Type)
        {
        case rpc::choose::Any:          return proto::BasePacket::Any;
        case rpc::choose::All:          return proto::BasePacket::All;
        case rpc::choose::Unloaded:     return proto::BasePacket::Load;
        case rpc::choose::Closest:      return proto::BasePacket::Closest;
        default:
            assert(!"unknown type");
            return proto::BasePacket::Any;
        }
    }

    IFuture::Ptr CallMethodImpl(const proto::BasePacket& base, const gp::Message* request, const IStream& stream)
    {
        LOG_TRACE("[%s]: Calling method, base: %s") % m_Router->GetLocalId() % base.DebugString();
        CHECK(!request || request->IsInitialized(), Exception("Can't call method because request is not initialized"), base.ShortDebugString(), request->ShortDebugString());

        {
            const auto connection = GetConnection();
            details::WriteStream writer(connection);
            writer.Write(base, request, stream);
        }

        if (base.dispatch() == proto::BasePacket::All)
            return IFuture::Ptr(); // this is an event, no need to wait for response

        FutureResponse response;
        const auto instance(shared_from_this());
        const auto packetId = base.packetid();

        if (m_Timeout.total_milliseconds())
        {
            response.m_Timer = boost::make_shared<boost::asio::deadline_timer>(cmn::Service::Instance());
            response.m_Timer->expires_from_now(m_Timeout);
            response.m_Timer->async_wait([instance, this, packetId](const boost::system::error_code& e){
                if (e == boost::asio::error::operation_aborted)
                    return;

                boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
                const auto it = m_Responses.find(packetId);
                if (it == m_Responses.end())
                    return;

                it->second.m_Future->SetException(rpc::MakeException("Response timed out"));
                m_Responses.erase(it);
            });
        }

        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        CHECK(m_Connection.lock(), Exception("Failed to call method, channel is closed"));
        CHECK(m_Responses.insert(std::make_pair(packetId, response)).second, Exception("Duplicated packet id"), packetId);
        return response.m_Future;
    }


    virtual void ForwardCall(const gp::Message& base, const IStream& stream, const net::IConnection::Ptr& connection) override
    {
        proto::BasePacket baseCopy(static_cast<const proto::BasePacket&>(base));
        const auto initialPacketId = baseCopy.packetid();

        if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace))
            baseCopy.add_debug((boost::format("[%s]: Forwarding method [%s:%s], dispatch: [%s] to instance: [%s]") % m_Router->GetLocalId() % baseCopy.serviceid() % baseCopy.method() % baseCopy.dispatch() % baseCopy.instance()).str());

        // generate new packet id
        baseCopy.set_packetid(GetNextPacketId());

        // routine to forward some RPC call to this channel
        // we must make a RPC call as usual, but call response must be sent to specified connection
        const auto future = CallMethodImpl(baseCopy, nullptr, stream);
        if (!future)
            return; // there is no future ...

        // set packet id back
        baseCopy.set_packetid(initialPacketId);

        // wait for response
        baseCopy.set_direction(proto::BasePacket::Response);
        auto instance(shared_from_this());
        future->GetData([connection, baseCopy, instance, this](const IFuture::Ptr& future){

            // write response back to connection
            details::WriteStream writer(connection);

            try
            {
                try
                {
                    const auto stream = future->GetData();
                    writer.Write(baseCopy, nullptr, stream);
                }
                catch (const std::exception& e)
                {
                    proto::BasePacket base(baseCopy);
                    base.set_error(conv::cast<std::string>(cmn::ExceptionInfo(e)));
                    writer.Write(base);
                    throw;
                }
            }
            catch (const std::exception& e)
            {
                LOG_ERROR("Failed to resend forward response: %s") % cmn::ExceptionInfo(e);
            }
        });
    }

    virtual IFuture::Ptr CallMethod(
        const gp::MethodDescriptor& method,
        const gp::Message& request,
        const IStream& stream,
        const details::CallParams& params
        ) override
    {
        try
        {
            // make base request packet
            proto::BasePacket base;

            gp::uint32 streamSize = 0;
            {
                const IService::Id id = method.service()->options().GetExtension(proto::service_id);

                base.set_method(method.index());
                base.set_serviceid(id);
                base.set_packetid(GetNextPacketId());
                base.set_direction(proto::BasePacket::Request);
                base.set_dispatch(MapDispatchType2Proto(params.m_Type));
                if (!params.m_Id.empty())
                    base.set_instance(params.m_Id);
            }

            if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace))
                base.add_debug((boost::format("[%s]: Calling method [%s], dispatch: [%s], instance: [%s]") % m_Router->GetLocalId() % method.full_name() % base.dispatch() % base.instance()).str());

            return CallMethodImpl(base, &request, stream);
        }
        CATCH_PASS(rpc::Exception("Failed to call rpc method"), request.DebugString(), method.full_name())
    }

    virtual net::IConnection::Ptr GetConnection() const override
    {
        const auto result = m_Connection.lock();
        CHECK(result, Exception("Failed to obtain connection, maybe channel was closed"));
        return result;
    }

    unsigned GetNextPacketId()
    {
        static unsigned counter = 0;
        static boost::mutex mutex;
        boost::unique_lock<boost::mutex> lock(mutex);
        return counter++;
    }

private:

    virtual void ProvideService(rpc::IService& service)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_LocalServices.insert(&service);
    }

    virtual void SetTimeout(const boost::posix_time::time_duration& timeout)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        m_Timeout = timeout;
    }

    void HandleRequest(proto::BasePacket& basePacket, const IStream& stream)
    {
        try
        {
            if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace) || basePacket.debug_size())
                basePacket.add_debug((boost::format("[%s]: Handling request [%s:%s], dispatch: [%s] to instance: [%s]") % m_Router->GetLocalId() % basePacket.serviceid() % basePacket.method() % basePacket.dispatch() % basePacket.instance()).str());

            HandleRawRequestData(basePacket, stream);
        }
        catch (const std::exception& e)
        {
            LOG_ERROR("Failed to process request: %s") % cmn::ExceptionInfo(e);

        	// send error response
            basePacket.set_direction(proto::BasePacket::Response);
            basePacket.set_error(conv::cast<std::string>(cmn::ExceptionInfo(e)));

            const auto connection = GetConnection();
            details::WriteStream wrapper(connection);
            wrapper.Write(basePacket);
        }
    }

    void HandleResponse(proto::BasePacket& basePacket, const IStream& stream)
    {
        try
        {
            if (m_Log.IsEnabled(CURRENT_MODULE_ID, ILog::Level::Trace) || basePacket.debug_size())
                basePacket.add_debug((boost::format("[%s]: Handling response [%s:%s], dispatch: [%s] to instance: [%s]") % m_Router->GetLocalId() % basePacket.serviceid() % basePacket.method() % basePacket.dispatch() % basePacket.instance()).str());

            HandleRawResponseData(basePacket, stream);
        }
        catch (const std::exception&)
        {
            const auto it = m_Responses.find(basePacket.packetid());
            if (it == m_Responses.end())
                return;

            if (it->second.m_Timer)
                it->second.m_Timer->cancel();
            it->second.m_Future->SetException(boost::current_exception());
            m_Responses.erase(it);
        }
    }

    void HandleBasePacket(const IStream& stream)
    {
        try
        {
            proto::BasePacket basePacket;
            details::ReadStream::Read(*stream, basePacket);

            LOG_TRACE("[%s]: Handling base packet: %s") % m_Router->GetLocalId() % basePacket.DebugString();

            if (basePacket.direction() == proto::BasePacket::Request)
                HandleRequest(basePacket, stream);
            else
            if (basePacket.direction() == proto::BasePacket::Response)
                HandleResponse(basePacket, stream);
            else
                THROW(Exception("Wrong packet direction"), basePacket.ShortDebugString());
        }
        catch (const std::exception& e)
        {
            LOG_ERROR("Failed to handle base packet: %s") % cmn::ExceptionInfo(e);
        }
    }

    void HandleRawResponseData(proto::BasePacket& basePacket, const IStream& stream)
    {
        FutureResponse future;

        {
            boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
            const auto it = m_Responses.find(basePacket.packetid());
            if (it == m_Responses.end())
            {
                // response outdated or duplicated, duplicated response may be received
                // when packet dispatch type is 'All'
                return;
            }

            future = it->second;
            m_Responses.erase(it);
        }

        if (future.m_Timer)
            future.m_Timer->cancel();

        if (basePacket.has_error())
        {
            try
            {
                THROW(Exception(basePacket.error().c_str()));
            }
            catch (const std::exception&)
            {
                future.m_Future->SetException(boost::current_exception());
            }
        }
        else
        {
            future.m_Future->SetData(stream);
        }

        
    }

    void HandleRawRequestData(proto::BasePacket& basePacket, const IStream& stream)
    {
        for (auto* handler : m_RequestHandlers)
        {
            if (handler->Handle(basePacket, stream, shared_from_this()))
                return;
        }
        assert(!"this should never happen, because default handler matches everything");
    }

    virtual Services GetLocalServices() const override
    {
        Services result;

        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        boost::transform(m_LocalServices, std::inserter(result, result.end()), boost::bind(&IService::GetId, _1));
        return result;
    }

    void AddHandler(details::IRequestHandler& handler) override
    {
        m_RequestHandlers.push_back(&handler);

        const auto it = boost::find(m_RequestHandlers, &m_LocalHandler);
        assert(it != m_RequestHandlers.end());
        std::swap(*it, m_RequestHandlers.back()); // move local handler to the end
    }

    virtual void Close() override
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_Mutex);
        const auto exception = rpc::MakeException("Channel closed");

        boost::for_each(m_Responses, [&exception](const ResponseMap::value_type& pair){
            try
            {
                pair.second.m_Future->SetException(exception);
            }
            catch (const std::exception&)
            {
            }
        });
        m_Responses.clear();
        m_Connection.reset();
    }


protected:
    boost::weak_ptr<net::IConnection> m_Connection;
    ILog& m_Log;
    const IRouter::Ptr m_Router;
private:
    LocalServiceList m_LocalServices;
    Handlers m_RequestHandlers;
    LocalServiceHandler m_LocalHandler;

    mutable boost::recursive_mutex m_Mutex;
    boost::posix_time::time_duration m_Timeout;
    ResponseMap m_Responses;
};

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: 'rpc::`anonymous-namespace'::Channel' : inherits 'rpc::`anonymous-namespace'::SequencedChannel::rpc::`anonymous-namespace'::SequencedChannel::CallMethod' via dominance

class Channel 
    : public SequencedChannel
    , public IChannel
    , public net::ISequenceCollector::ICallback
{
public:
    Channel(ILog& log, const IRouter::Ptr& router)
        : SequencedChannel(log, router)
        , m_Collector(net::ISequenceCollector::Instance(*this))
    {}

    virtual void OnIncomingData(const IStream& stream) override
    {
        // parse streams, collect all sequence data
        m_Collector->OnNewStream(stream);
    }

    virtual void OnFullStreamCollected(const net::ReadSequence::StreamPtr& stream) override
    {
        SequencedChannel::OnIncomingData(stream);
    }

    virtual net::IConnection::Ptr GetConnection() const override
    {
        return boost::make_shared<net::SequencedConnection>(SequencedChannel::GetConnection());
    }

private:
    net::ISequenceCollector::Ptr m_Collector;
};

#pragma warning(pop)

} // anonymous namespace

ISequencedChannel::Ptr ISequencedChannel::Instance(ILog& log, const IRouter::Ptr& router)
{
    return boost::make_shared<SequencedChannel>(log, router);
}

IChannel::Ptr IChannel::Instance(ILog& log, const IRouter::Ptr& router)
{
    return boost::make_shared<Channel>(log, router);
}


} // namespace rpc