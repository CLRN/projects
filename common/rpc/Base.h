#ifndef RpcBase_h__
#define RpcBase_h__

#include "Future.h"

#include <iosfwd>
#include <string>

#include <boost/noncopyable.hpp>

namespace google
{
namespace protobuf
{
    class MethodDescriptor;
    class Message;
    class ServiceDescriptor;
} // namespace protobuf
} // namespace google

namespace rpc
{

namespace gp = google::protobuf;
typedef boost::shared_ptr<std::istream> IStream;
typedef boost::shared_ptr<std::ostream> OStream;
typedef boost::shared_ptr<gp::Message> MessagePtr;
typedef std::string InstanceId;

namespace choose
{

enum CallType
{
    Any         = 0,
    All         = 1,
    Unloaded    = 2,
    Closest     = 3
};

typedef InstanceId ById;

}// namespace choose

namespace details
{

class IChannel;

template<typename T>
class StreamHolder
{
public:
    StreamHolder() : m_Channel() {}
    virtual ~StreamHolder() {}

    const T& Stream() const { return m_Stream; }
    T& Stream() { return m_Stream; }
    void Stream(const T& stream) { m_Stream = stream; }
    const boost::exception_ptr& GetException() const { return m_Exception; }
    void SetException(const boost::exception_ptr& e) { m_Exception = e; }
    IChannel& GetChannel() { return *m_Channel; }
protected:
    boost::exception_ptr m_Exception;
    IChannel* m_Channel;
private:
    T m_Stream;
};


struct CallParams
{
    CallParams() : m_Type(), m_Id() {}
    CallParams(const InstanceId& id) : m_Type(), m_Id(id) {}
    CallParams(choose::CallType type) : m_Type(type), m_Id() {}
    choose::CallType m_Type;
    choose::ById m_Id;
};

} // namespace details

template<typename T>
class Request : public T, public details::StreamHolder<IStream>
{
public:
    typedef boost::shared_ptr<Request<T> > Ptr;
};

template<typename T>
class Response : public T, public details::StreamHolder<IStream>
{
public:
    typedef boost::shared_ptr<Response<T> > Ptr;
};

namespace details
{

class IChannel
{
public:
    virtual ~IChannel() {}
    virtual IFuture::Ptr CallMethod(const gp::MethodDescriptor& method, 
                                    const gp::Message& request,
                                    const IStream& stream,
                                    const CallParams& params) = 0;
};

} // namespace details

class IService : boost::noncopyable
{
public:
    virtual ~IService() {}

    typedef unsigned Id;

    //! Auto generated implementation
    virtual void CallMethod(const gp::MethodDescriptor& method,
                            const MessagePtr& request,
                            const MessagePtr& response) = 0;

    virtual const gp::ServiceDescriptor& GetDescriptor() = 0;
    virtual Id GetId() const = 0;
    virtual gp::Message* CreateRequest(const gp::MethodDescriptor& method) const = 0;
    virtual gp::Message* CreateResponse(const gp::MethodDescriptor& method) const = 0;
};

} // namespace rpc

#endif // RpcBase_h__
