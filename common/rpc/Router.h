#ifndef Router_h__
#define Router_h__

#include "protocols/rpc_base.pb.h"

#include "Base.h"

#include <vector>

#include <boost/shared_ptr.hpp>

class ILog;

namespace rpc
{

class IRouter
{
public:
    typedef boost::shared_ptr<IRouter> Ptr;
    typedef std::vector<proto::RegistrationInfo::Instance> Instances;

    virtual ~IRouter() {}

    //! Get local instance Id
    //!
    //! \return const rpc::InstanceId&
    //!
    virtual const rpc::InstanceId& GetLocalId() const = 0;

    //! Register local service
    //!
    //! \return void
    //!
    virtual void AddLocalService(IService::Id id) = 0;
    
    //! Updates remote services info
    //!
    //! \param[in] info - remote instances info
    //! \return void
    //!
    virtual void UpdateInfo(Instances& info) = 0;

    //! Add neighbor instance(with direct connection)
    //!
    //! \param[in] info     - neighbor instance
    //! \return void
    //!
    virtual void AddNeighbor(const Instances::value_type& info) = 0;

    //! RemoveInstance
    //!
    //! \param[in] id - remote instance identifier
    //! \return void
    //!
    virtual void RemoveInstance(const InstanceId& id) = 0;

    //! Computes next node in the path
    //!
    //! \param[in] params           - route parameters
    //! \return rpc::InstanceId     - next node id in the deliver chain
    //!
    virtual InstanceId GetNextNode(const details::CallParams& params, IService::Id id) = 0;

    //! Get list of all discovered instances 
    //!
    //! \return rpc::IRouter::Instances - list of instances, includes local instance info
    //!
    virtual Instances GetInstances() const = 0;

    //! Tests instance chain for specefied service existance
    //!
    //! \param[in] instance - instance id
    //! \param[in] service  - service id
    //! \return bool
    //!
    virtual bool IsExists(const InstanceId& instance, IService::Id service) = 0;

    //! Create instance
    //!
    //! \param[in] log  - logger 
    //! \param[in] id   - local endpoint id
    //! \return rpc::IRouter::Ptr
    //!
    static Ptr Instance(ILog& log, const rpc::InstanceId& id);
};

//! Returns network graph in DOT format( http://ru.wikipedia.org/wiki/DOT_(%D1%8F%D0%B7%D1%8B%D0%BA) )
std::string Visualize(const IRouter::Instances& graph);

} // namespace rpc

#endif // Router_h__
