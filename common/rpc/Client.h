#ifndef Client_h__
#define Client_h__

#include "Base.h"
#include "net/IHost.h"
#include "Endpoint.h"

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

class ILog;

namespace rpc
{

class IClient : public Endpoint
{
public:
    typedef boost::shared_ptr<IClient> Ptr;
    typedef boost::function<void(const boost::exception_ptr&)> ConnectCallback;

    IClient(ILog& log, const InstanceId& id) : Endpoint(log, id) {}
    virtual ~IClient() {}

    virtual void Connect(const ConnectCallback& cb = ConnectCallback()) = 0;

    //! Instance
    static Ptr Instance(ILog& log, net::IHost& host, const net::IHost::Endpoint& ep, const InstanceId& id);
};

} // namespace rpc

#endif // Client_h__
