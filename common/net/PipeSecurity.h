#ifndef pipe_security_h__
#define pipe_security_h__

#include <windows.h>

namespace net
{
namespace pipes
{
namespace details
{

class PipeSecurity
{
public:
    PipeSecurity();
    ~PipeSecurity();
    void BuildAttributes(SECURITY_ATTRIBUTES& sa);
private:
    BOOL GetUserSid(PSID*  ppSidUser);
private:

    BOOL                    m_IsAllocated;
    PSECURITY_DESCRIPTOR    m_SecurityDescriptor;
    PACL                    m_ACL;
    PTOKEN_USER             m_TokenUser;
};

} // namespace details
} // namespace pipes
} // namespace net


#endif // pipe_security_h__