#ifndef FileHost_h__
#define FileHost_h__

#include "IHost.h"
#include "common/ILog.h"

#include <string>

#include <boost/noncopyable.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

namespace net
{

//! File transport
class FileTransport : public net::IHost
{
public:

	FileTransport(ILog& log);

	//! Dtor
	virtual ~FileTransport();

    virtual IConnection::Ptr Connect(const std::string& endpoint) override;
    virtual void Receive(const std::string& endpoint, const Callback& callback) override;
    virtual void Close() override;

private:

	//! Implementation
	class Impl;
	boost::shared_ptr<Impl> m_Impl;

};

} // namespace net

#endif // FileHost_h__
