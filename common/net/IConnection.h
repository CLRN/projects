#ifndef IConnection_h__
#define IConnection_h__

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/asio/buffer.hpp>

namespace net
{

//! Remote host connection abstraction
class IConnection : boost::noncopyable
{
public:

	virtual ~IConnection(){}

	//! Pointer type
	typedef boost::shared_ptr<IConnection> Ptr;

    //! Data pointer type
    typedef boost::shared_ptr<std::istream> StreamPtr;

    //! Buffer type
    typedef boost::asio::const_buffer ConstBuffer;

    typedef std::vector<ConstBuffer> Buffers;

	//! Callback function type
	typedef boost::function<void (const StreamPtr& stream)> Callback;

	//! Send to remote host
	virtual void Send(const Buffers& data) = 0;

	//! Receive callback
	virtual void Receive(const Callback& callback) = 0;

    //! Close connection
    virtual void Close() = 0;
};


} // namespace net


#endif // IConnection_h__
