#include "UDPHost.h"
#include "common/Modules.h"
#include "exception/CheckHelpers.h"
#include "SocketConnection.h"
#include "conversion/AnyCast.h"
#include "common/ServiceHolder.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

namespace net
{

const std::size_t DEFAULT_BUFFER_SIZE = 4096 * 2;

class UDPHost::Impl
{
	//! Buffer type
	typedef std::vector<char>									Buffer;

	//! Buffer pointer type
	typedef boost::shared_ptr<Buffer>							BufferPtr;

	//! Service type
	typedef boost::asio::io_service								Service;

	//! Server receive endpoint
	typedef boost::asio::ip::udp::endpoint						Endpoint;

	//! Endpoint ptr
	typedef  boost::shared_ptr<Endpoint>						EndpointPtr;

	//! Socket type
	typedef boost::asio::ip::udp::socket						Socket;

	//! Socket ptr type
	typedef boost::shared_ptr<Socket>							SocketPtr;

public:
	Impl(ILog& logger)
		: m_Log(logger)
        , m_Service(cmn::Service::Instance())
		, m_BufferSize(DEFAULT_BUFFER_SIZE)
	{
	}

	~Impl()
	{
		SCOPED_LOG(m_Log);
	}

	void SetBufferSize(const int size)
	{
		SCOPED_LOG(m_Log);

		LOG_TRACE("Old size: [%s], new size: [%s]") % m_BufferSize % size;

		CHECK(size, cmn::Exception("Empty buffer size"), size, m_BufferSize);
		m_BufferSize = size;
	}

	void Receive(const std::string& ep, const IHost::Callback& callback)
	{
		SCOPED_LOG(m_Log);

        std::vector<std::string> args;
        boost::algorithm::split(args, ep, boost::algorithm::is_any_of(":"));
        CHECK(args.size() == 2, cmn::Exception("Invalid endpoint format, must be host:port"), ep);

        m_Callback = callback;

		const SocketPtr socket(new Socket(m_Service, Endpoint(boost::asio::ip::udp::v4(), conv::cast<unsigned short>(args.back()))));
		const IConnection::Ptr connection(new Connection<Socket, Endpoint>(m_Log, socket, m_BufferSize));

		for (std::size_t i = 0 ; i < boost::thread::hardware_concurrency() * 2; ++i)
			callback(connection, boost::system::error_code());
	}

	IConnection::Ptr Connect(const std::string& host)
	{
		SCOPED_LOG(m_Log);

        std::vector<std::string> args;
        boost::algorithm::split(args, host, boost::algorithm::is_any_of(":"));
        CHECK(args.size() == 2, cmn::Exception("Invalid endpoint format, must be host:port"), host);

		boost::asio::ip::udp::resolver resolver(m_Service);
		const boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), args.front(), args.back());
		const Endpoint ep = *resolver.resolve(query);

		const SocketPtr socket(new Socket(m_Service, boost::asio::ip::udp::v4()));
		const IConnection::Ptr connection(new Connection<Socket, Endpoint>(m_Log, socket, m_BufferSize, ep));
		return connection;
	}

private:

	//! Logger
	ILog&					m_Log;

	//! Service
	Service&                m_Service;

	//! Buffer size
	std::size_t				m_BufferSize;

	//! Callback
	IHost::Callback			m_Callback;
};

UDPHost::UDPHost(ILog& logger) : m_Impl(new Impl(logger))
{

}

UDPHost::~UDPHost()
{
	delete m_Impl;
}

IConnection::Ptr UDPHost::Connect(const std::string& ep)
{
	return m_Impl->Connect(ep);
}

void UDPHost::Receive(const std::string& endpoint, const IHost::Callback& callback)
{
	m_Impl->Receive(endpoint, callback);
}

void UDPHost::Close()
{
    
}

} // namespace net

