#ifndef IHost_h__
#define IHost_h__

#include "IConnection.h"

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/system/error_code.hpp>

namespace net
{

//! Remote host abstraction
class IHost
{
public:
    typedef std::string Endpoint;
    virtual ~IHost(){}

    //! Pointer type
    typedef boost::shared_ptr<IHost> Ptr;

    //! Callback function type
    typedef boost::function<void (const IConnection::Ptr& connection, const boost::system::error_code& e = boost::system::error_code())> Callback;

    //! Connect to remote host
    virtual IConnection::Ptr Connect(const Endpoint& endpoint) = 0;

    //! Receive callback
    virtual void Receive(const Endpoint& endpoint, const Callback& callback) = 0;

    //! Stop all activity
    virtual void                Close() = 0;
};


} // namespace net
#endif // IHost_h__
