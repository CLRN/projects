#include "DataSequence.h"
#include "exception/CheckHelpers.h"
#include "common/FileSystem.h"
#include "conversion/AnyCast.h"
#include "common/GUID.h"
#include "common/StreamHelpers.h"

#include <map>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/static_assert.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/scoped_array.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

namespace net
{

namespace seq
{

#pragma pack(push, 1)

struct Header
{
    typedef boost::uint16_t Id;

    enum Value : char
    {
        END_SEQUENCE_MARKER   = 0,
        BLOCK_MARKER          = 1
    };

    Id m_Id;
    Value m_Marker;
};

#pragma pack(pop)

BOOST_STATIC_ASSERT(sizeof(Header) == 3);

} // namespace seq


seq::Header::Id g_Counter = 0;
boost::mutex g_WriteLock;

SequencedConnection::SequencedConnection(const IConnection::Ptr& connection) 
    : m_Connection(connection)
    , m_Id()
    , m_IsEmpty(true)
{
    boost::unique_lock<boost::mutex> lock(g_WriteLock);
    ++g_Counter;
    m_Id = g_Counter;
}

SequencedConnection::~SequencedConnection()
{
    if (m_IsEmpty)
        return;

    const seq::Header header = {m_Id, seq::Header::END_SEQUENCE_MARKER };

    // write EOF marker(means that sequence is ended)
    try
    {
        if (m_Connection)
            m_Connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(&header, sizeof(header))));
    }
    catch (const std::exception&)
    {
        if (!std::uncaught_exception())
            throw;
    }
}

void SequencedConnection::Send(const Buffers& data)
{
    m_IsEmpty = false;

    const seq::Header header = {m_Id, seq::Header::BLOCK_MARKER };

    Buffers buffers;
    buffers.reserve(data.size() + 1);

    // write block marker(means that sequence is in progress)
    buffers.push_back(boost::asio::buffer(&header, sizeof(header)));

    // write data
    boost::copy(data, std::back_inserter(buffers));
    if (m_Connection)
        m_Connection->Send(buffers);
}

void SequencedConnection::Receive(const Callback& callback)
{
    m_Connection->Receive(callback);
}

void SequencedConnection::Close()
{
    m_IsEmpty = true;
    m_Connection->Close();
}

ReadSequence::ReadSequence(Streams& streams) : m_Read()
{
    m_Streams.swap(streams);
}

std::streamsize ReadSequence::read(char_type* s, const std::streamsize n)
{
    std::streamsize total = 0;

    auto it = m_Streams.begin();
    const auto end = m_Streams.end();
    for (; it != end; m_ReadStreams.push_back(*it), it = m_Streams.erase(it))
    {
        const auto toRead = n - total;
        (*it)->read(s + total, toRead);
        const std::streamsize bytes = (*it)->gcount();
        total += bytes;
        m_Read += bytes;

        if (total == n)
            return n; // read full buffer

        // read completely, use next stream
        assert(bytes < toRead);
    }

    return total;
}

std::streamsize ReadSequence::write(const char_type* /*s*/, std::streamsize /*n*/)
{
    assert(!"not implemented");
    return 0;
}

std::streampos ReadSequence::seek(std::streamoff offset, std::ios::seekdir dir)
{
    if (dir == std::ios::cur)
    {
        assert(!offset);
        return m_Read;
    }

    if (dir == std::ios::end)
        offset = std::numeric_limits<std::streamoff>::max();

    for (const auto& file : m_Streams)
    {
        // copy streams and check
        if (boost::find(m_ReadStreams, file) == m_ReadStreams.end())
            m_ReadStreams.push_back(file);
    }

    m_ReadStreams.swap(m_Streams);
    boost::for_each(m_Streams, boost::bind(&std::istream::clear, _1, std::ios::goodbit, false));

    m_Read = offset;
    m_ReadStreams.clear();
    for (const auto& stream : m_Streams)
    {
        if (offset)
        {
            // there is offset remaining, obtain current stream size
            stream->seekg(0, std::ios::end);
            const auto size = static_cast<std::size_t>(stream->tellg()) - sizeof(seq::Header);
            if (size > offset)
            {
                stream->seekg(offset + sizeof(seq::Header));
                offset = 0;
            }
            else
            {
                offset -= size;
            }
        }
        else
        {
            // there is no offset value remaining, reset current stream to the beginning
            stream->seekg(sizeof(seq::Header), std::ios::beg);
        }
    }

    if (dir == std::ios::end)
    {
        // calculate streams size
        const auto size = std::numeric_limits<std::streamoff>::max() - offset;
        m_Read = size;
        return size;
    }

    return offset;
}

namespace 
{

class StreamCollector : public ISequenceCollector, boost::noncopyable
{
public:
    enum { MAX_STREAM_SIZE_IN_MEMORY = 1024 * 1024 * 10 }; // 10 mb

    struct StreamDesc
    {
        StreamDesc() : m_Size(), m_IsMemory(true) {}
        ReadSequence::Streams m_Streams;
        std::size_t m_Size;
        bool m_IsMemory;
    };

    typedef std::map<seq::Header::Id, StreamDesc> StreamMap;

    StreamCollector(ICallback& c) : m_Callback(c) 
    {
        boost::filesystem::create_directories(boost::filesystem::temp_directory_path() / "streams");
    }

    virtual void OnNewStream(const ReadSequence::StreamPtr& stream) override
    {
        if (!stream)
        {
            m_Callback.OnFullStreamCollected(stream);
            boost::unique_lock<boost::mutex> lock(m_Mutex);
            m_Streams.clear();
            return;
        }

        // calculate size
        stream->seekg(0, std::ios::end);
        const std::size_t size = static_cast<std::size_t>(stream->tellg());
        stream->seekg(0, std::ios::beg);

        // read sequence id and block marker
        seq::Header header;
        stream->read(reinterpret_cast<char*>(&header), sizeof(seq::Header));

        if (header.m_Marker == seq::Header::BLOCK_MARKER)
        {
            // collect stream
            boost::unique_lock<boost::mutex> lock(m_Mutex);
            auto& desc = m_Streams[header.m_Id];

            if (desc.m_IsMemory)
            {
                if (desc.m_Size > MAX_STREAM_SIZE_IN_MEMORY)
                {
                    const auto file = CreateStream();
                    for (const auto& src : desc.m_Streams)
                        cmn::CopyStream(*src, *file);

                    desc.m_Streams.clear();
                    desc.m_Streams.push_back(file);
                    desc.m_IsMemory = false;
                }
                else
                {
                    desc.m_Streams.push_back(stream);
                }
            }
            
            if (!desc.m_IsMemory)
            {
                assert(desc.m_Streams.size() == 1);
                auto& file = dynamic_cast<std::ostream&>(*desc.m_Streams.front());
                cmn::CopyStream(*stream, file);
            }

            desc.m_Size += size;
        }
        else
        if (header.m_Marker == seq::Header::END_SEQUENCE_MARKER)
        {
            // stream is over, invoke callback
            ReadSequence::Streams streams;
            bool isInFile = false;
            {
                boost::unique_lock<boost::mutex> lock(m_Mutex);
                const auto it = m_Streams.find(header.m_Id);
                if (it == m_Streams.end())
                    return;

                isInFile = !it->second.m_IsMemory;
                streams.swap(it->second.m_Streams);
                m_Streams.erase(it);
            }

            if (streams.empty())
                return;

            if (isInFile)
            {
                CHECK(streams.size() == 1, cmn::Exception("Wrong sequence count, one stream expected"), streams.size());
                streams.front()->seekg(0, std::ios::beg);
                m_Callback.OnFullStreamCollected(streams.front());
            }
            else
            {
                // make read sequence
                const auto sequence = boost::make_shared<ReadSequence::Stream>(streams);
                m_Callback.OnFullStreamCollected(sequence);
            }
        }
        else
        {
            THROW(cmn::Exception("Wrong header marker"), header.m_Marker, header.m_Id);
        }
    }

    boost::shared_ptr<std::iostream> CreateStream()
    {
        const auto holder = boost::make_shared<fs::ScopedFile>((boost::filesystem::temp_directory_path() / "streams" / cmn::GUID::Generate()).wstring());
        const auto name = conv::cast<std::string>(holder->operator const std::wstring &());

        {
            std::ofstream test(name.c_str());
            assert(test.is_open());
        }

        std::unique_ptr<boost::filesystem::fstream> stream(
            new boost::filesystem::fstream(name, std::ios::binary | std::ios::in | std::ios::out)
        );
        CHECK(stream->is_open(), cmn::Exception("Failed to open stream"));

        // hold scoped file until stream is deleted
        boost::shared_ptr<boost::filesystem::fstream> result(stream.release(), [holder](boost::filesystem::fstream* s){
            delete s;
        });
        return result;
    }

private:
    ICallback& m_Callback;
    StreamMap m_Streams;
    boost::mutex m_Mutex;
};


} // anonymous namespace 


ISequenceCollector::Ptr ISequenceCollector::Instance(ICallback& callback)
{
    return boost::make_shared<StreamCollector>(callback);
}

} // namespace net