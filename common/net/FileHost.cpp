#include "FileHost.h"
#include "Pipe.h"
#include "exception/CheckHelpers.h"
#include "Connection.h"
#include "common/ServiceHolder.h"
#include "conversion/AnyCast.h"
#include "common/Modules.h"

#include <boost/filesystem.hpp>

namespace net
{

DECLARE_CURRENT_MODULE(Modules::Network);

class FileTransport::Impl : public boost::enable_shared_from_this<Impl>
{
    typedef boost::shared_ptr<boost::asio::windows::random_access_handle> FilePtr;

    class Connection : public net::details::Channel<FilePtr>
    {
    public:

        Connection(Impl& owner, const std::wstring& file) 
            : net::details::Channel<FilePtr>(boost::make_shared<boost::asio::windows::random_access_handle>(cmn::Service::Instance()))
            , m_Offset()
            , m_Owner(owner)
            , m_File(file)
            , m_IsNewFile(!boost::filesystem::exists(file))
        {
            const HANDLE handle = CreateFileW
            (
                file.c_str(), 
                GENERIC_WRITE | GENERIC_READ, 
                FILE_SHARE_READ | FILE_SHARE_WRITE, 
                NULL, 
                (m_IsNewFile ? CREATE_ALWAYS : OPEN_EXISTING), 
                FILE_ATTRIBUTE_HIDDEN | FILE_FLAG_OVERLAPPED, 
                NULL
            );
            CHECK(handle != INVALID_HANDLE_VALUE, cmn::Exception("Failed to create file"), file);
            m_IoObject->assign(handle);
        }

        virtual void BeginReadHeader() override
        {
            if (m_IsNewFile)
                return; // ignore read from new file

            boost::mutex::scoped_lock lock(m_HandleMutex);
            if (m_IoObject)
            {
                boost::asio::async_read_at
                (
                    *m_IoObject, 
                    m_Offset, 
                    boost::asio::buffer(&m_Size, sizeof(m_Size)), 
                    m_Strand.wrap(boost::bind(&Channel::ReadHeaderCallback, this, _1, _2))
                );   
            }
        }

        virtual void BeginReadBody() override
        {
            if (m_IsNewFile)
                return; // ignore read from new file

            boost::mutex::scoped_lock lock(m_HandleMutex);
            if (m_IoObject)
            {
                boost::asio::async_read_at
                (
                    *m_IoObject, 
                    m_Offset, 
                    boost::asio::buffer(&m_ReadBuffer[m_Read], m_Size - m_Read),
                    m_Strand.wrap(boost::bind(&Channel::ReadMessageCallback, this, _1, _2))
                );     
            }
        }

        virtual void Write(const std::vector<boost::asio::const_buffer>& buffers, boost::system::error_code& e) override
        {
            if (m_IsNewFile)
                boost::asio::write_at(*m_IoObject, boost::filesystem::file_size(m_File), buffers, e);
        }

        virtual void ConnectionClosed(const boost::system::error_code& e, std::size_t bytes) override
        {
            if (m_Owner.m_ClientConnectedCallback)
                m_Owner.m_ClientConnectedCallback(shared_from_this(), e);
        }

        //! Read callback
        virtual void OnBytesRead(const std::size_t bytes) override
        {
            m_Offset += bytes;
        }

    private:
        unsigned __int64 m_Offset;
        Impl& m_Owner;
        std::wstring m_File;
        bool m_IsNewFile;
    };

public:
	Impl(ILog& log)
		: m_Log(log)
	{
	}

	~Impl()
	{

	}

    void Receive(const std::string& endpoint, const Callback& callback)
    {
        m_ClientConnectedCallback = callback;
        const auto connection = boost::make_shared<Connection>(*this, conv::cast<std::wstring>(endpoint));
        callback(connection, boost::system::error_code());
    }

    IConnection::Ptr Connect(const std::string& endpoint)
    {
        return boost::make_shared<Connection>(*this, conv::cast<std::wstring>(endpoint));
    }

    void Close()
    {
    }

private:
    ILog& m_Log;
    Callback m_ClientConnectedCallback;
};

FileTransport::FileTransport(ILog& log)
    : m_Impl(boost::make_shared<Impl>(log))
{

}

void FileTransport::Receive(const std::string& endpoint, const Callback& callback)
{
    m_Impl->Receive(endpoint, callback);
}

IConnection::Ptr FileTransport::Connect(const std::string& endpoint)
{
    return m_Impl->Connect(endpoint);
}

void FileTransport::Close()
{
    m_Impl->Close();
}

FileTransport::~FileTransport()
{
    m_Impl->Close();
    m_Impl.reset();
}

} // namespace net
