#ifndef TCPHost_h__
#define TCPHost_h__

#include "IHost.h"
#include "common/ILog.h"

#include <string>

#include <boost/noncopyable.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

namespace net
{
namespace tcp
{

//! TCP socket transport
class ITransport : public net::IHost
{
public:

	enum { DEFAULT_BUFFER_SIZE = 1024 * 4 };
    static Ptr Instance(ILog& log, std::size_t bufferSize = DEFAULT_BUFFER_SIZE);

};

} // namespace tcp
} // namespace net

#endif // TCPHost_h__
