#ifndef pipe_host_h__
#define pipe_host_h__

#include "IHost.h"
#include "common/ILog.h"

#include <string>

#include <boost/noncopyable.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

namespace net
{
namespace pipes
{

//! Named pipe transport
class Transport : public net::IHost
{
public:

	enum { DEFAULT_BUFFER_SIZE = 1024 * 64 };

	Transport(ILog& log, std::size_t startupInstances = 5, std::size_t bufferSize = DEFAULT_BUFFER_SIZE);

	//! Dtor
	virtual ~Transport();

    virtual IConnection::Ptr Connect(const std::string& endpoint) override;
    virtual void Receive(const std::string& endpoint, const Callback& callback) override;
    virtual void Close() override;

private:

	//! Implementation
	class Impl;
	boost::shared_ptr<Impl> m_Impl;

};

} // namespace pipes
} // namespace net

#endif // pipe_host_h__
