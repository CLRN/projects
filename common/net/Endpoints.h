#ifndef endpoints_h__
#define endpoints_h__

#include <string>

namespace net
{
namespace cfg
{

typedef std::string Endpoint;
Endpoint GetServiceEndpoint();

} // namespace cfg
} // namespace net


#endif // endpoints_h__
