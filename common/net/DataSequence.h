#ifndef DataSequence_h__
#define DataSequence_h__

#include "IConnection.h"

#include <atomic>
#include <list>

#include <boost/iostreams/stream.hpp>
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

namespace net
{

// �����-like ��������� ��� ����������� ������� ������
// � ������ ������:
// ������� ������������� ������������������
// � ������ ������������ ����� ���� ����� ��������� ���������������
// ���������� ����� ���������� ��������� ������������������
class SequencedConnection : public IConnection
{
public:
    SequencedConnection(const IConnection::Ptr& connection);
    ~SequencedConnection();
    virtual void Send(const Buffers& data) override;
    virtual void Receive(const Callback& callback) override;
    virtual void Close() override;

private:
    IConnection::Ptr m_Connection;
    boost::uint16_t m_Id;
    boost::mutex m_Mutex;
    bool m_IsEmpty;
};

// � ������ ������:
// �������� ��������� �� �������� ����� � �������, �������� ������������� ������������������
// ���������� �������� ������ �� ��������������
// �������� ����� ���������� ������������������, �������� �������� ������� � input ������� ���������� ��� ������ ������������������
class ReadSequence
{
public:
    typedef char char_type;
    typedef boost::iostreams::seekable_device_tag category;
    typedef boost::iostreams::stream<ReadSequence> Stream;
    typedef boost::shared_ptr<std::istream> StreamPtr;
    typedef std::list<StreamPtr> Streams;

    ReadSequence(Streams& streams);

    std::streamsize read(char_type* s, std::streamsize n);
    std::streamsize write(const char_type* s, std::streamsize n);
    std::streampos seek(std::streamoff offset, std::ios::seekdir dir);

private:
    Streams m_Streams;
    Streams m_ReadStreams;
    std::istream::streampos m_Read;
};

class ISequenceCollector
{
public:

    class __declspec(novtable) ICallback
    {
    public:
        virtual void OnFullStreamCollected(const ReadSequence::StreamPtr& stream) = 0;
    };

    typedef boost::shared_ptr<ISequenceCollector> Ptr;

    virtual ~ISequenceCollector() {}

    virtual void OnNewStream(const ReadSequence::StreamPtr& stream) = 0;

    static Ptr Instance(ICallback& callback);
};


} // namespace net


#endif // DataSequence_h__
