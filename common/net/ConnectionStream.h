#ifndef ConnectionStream_h__
#define ConnectionStream_h__

#include "net/IConnection.h"

#include <boost/iostreams/stream.hpp>

namespace net
{

class ConnectionStream
{
public:

    typedef char char_type;
    typedef boost::iostreams::sink_tag category;
    typedef boost::iostreams::stream<ConnectionStream> Type;

    ConnectionStream(IConnection& connection) : m_Connection(connection) {}

    std::streamsize write(const char_type* s, std::streamsize n)
    {
        m_Connection.Send(net::IConnection::Buffers(1, boost::asio::buffer(s, static_cast<std::size_t>(n))));
        return n;
    }

private:
    IConnection& m_Connection;
};

} // namespace net

#endif // ConnectionStream_h__
