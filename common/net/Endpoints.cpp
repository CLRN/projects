#include "endpoints.h"

namespace net
{
namespace cfg
{

Endpoint GetServiceEndpoint()
{
    return "clrn_service";
}

} // namespace cfg
} // namespace net