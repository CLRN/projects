#ifndef pipe_connection_h__
#define pipe_connection_h__

#include "Pipe.h"
#include "IConnection.h"
#include "exception/CheckHelpers.h"
#include "exception/ErrorCodeInfo.h"
#include "StreambufHolder.h"

#include <atomic>
#include <vector>

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/foreach.hpp>
#include <boost/system/windows_error.hpp>
#include <boost/asio.hpp>
#include <boost/scope_exit.hpp>
#include <boost/cstdint.hpp>
#include <boost/static_assert.hpp>
#include <boost/range/algorithm.hpp>

namespace net
{
namespace details
{

//! Channel interface
class IChannel : public net::IConnection
{
public:

    virtual ~IChannel() {}

    //! Prepare for header reading
    virtual void BeginReadHeader() = 0;

    //! Start to read, implementation depends on io object type
    virtual void BeginReadBody() = 0;

    //! Write all buffers to io object, implementation depends on io object type
    virtual void Write(const std::vector<boost::asio::const_buffer>& buffers, boost::system::error_code& e) = 0;

    //! On connection closed
    virtual void ConnectionClosed(const boost::system::error_code& e, std::size_t bytes) = 0;
};

//! Pipe/file channel abstraction
template<typename Handle>
class Channel 
    : public boost::enable_shared_from_this<Channel<Handle> >
    , public IChannel
{
public:

    //! Client pointer type
    typedef boost::shared_ptr<Channel> Ptr;
    typedef std::vector<char> Buffer;

    Channel(const Handle& pipe) 
        : m_IoObject(pipe)
        , m_Size()
        , m_Strand(cmn::Service::Instance())
        , m_Read()
        , m_HeaderSize()
    {
    }

    virtual ~Channel()
    {
        Close();
    }

    virtual void Close() override
    {
        boost::mutex::scoped_lock lock(m_HandleMutex);
        m_IoObject.reset();
    }

    virtual void Send(const Buffers& data) override
    {
        const boost::uint32_t size = static_cast<boost::uint32_t>(boost::asio::buffer_size(data));
        BOOST_STATIC_ASSERT(sizeof(size) == 4);

        Buffers buffers;
        buffers.reserve(data.size() + 1);
        buffers.push_back(boost::asio::buffer(&size, sizeof(size)));
        boost::copy(data, std::back_inserter(buffers));

        boost::system::error_code e;

        {
            boost::mutex::scoped_lock lock(m_HandleMutex);
            CHECK(m_IoObject, cmn::Exception("Unable to send data, connection is closed by local side"));
            Write(buffers, e);
        }
        CHECK(!e, cmn::Exception("Failed to write data") << cmn::ErrorCodeInfo(e));
    }

    virtual void Receive(const Callback& callback) override
    {
        m_Callback = callback;
        m_HeaderSize = 0;
        m_Size = 0;
        BeginReadHeader();
    }

    //! Read message size callback
    void ReadHeaderCallback(boost::system::error_code e, const std::size_t bytes)
    {
        if (!e && m_HeaderSize + bytes != sizeof(m_Size))
        {
            m_HeaderSize += bytes;
            BeginReadHeader();
        }
        else
        if (e || m_Size > 1024 * 1024 * 50) // check size limit(50 mb)
        {
            // failed to read, probably pipe is broken or client disconnected
            ConnectionClosed(e ? e : boost::system::error_code(boost::system::errc::protocol_error, boost::system::system_category()), bytes);
        }
        else
        {
            OnBytesRead(bytes);
            m_Read = 0;
            m_ReadBuffer.resize(m_Size);
            BeginReadBody();
        }
    }

    //! Read callback
    void ReadMessageCallback(boost::system::error_code e, const std::size_t bytes)
    {
        if (e)
        {
            // failed to read, probably pipe is broken or client disconnected
            ConnectionClosed(e, bytes);
            return;
        }

        // increment write offset
        OnBytesRead(bytes);

        // commit received bytes to the input sequence
        m_Read += bytes;

        if (m_Size > m_Read)
        {
            // read partially
            BeginReadBody();
            return;
        }

        assert(m_Size == m_Read); // assert that we read expected size exactly 

        const auto stream = boost::make_shared<boost::iostreams::stream<details::SeekableStreamBuf>>(&m_ReadBuffer);
        m_HeaderSize = 0;
        m_Size = 0;
        BeginReadHeader();

        // invoke callback
        m_Callback(stream);
    }

    //! Increment offset, method is virtual because some random access io objects must have access to bytes read
    virtual void OnBytesRead(const std::size_t /*bytes*/)
    {
    }

protected:
    Handle m_IoObject;
    Buffer m_ReadBuffer;
    boost::uint32_t m_Size;
    boost::uint32_t m_Read;
    boost::uint32_t m_HeaderSize;
    boost::mutex m_HandleMutex;
    Callback m_Callback;
    boost::asio::io_service::strand m_Strand;
};	

} // namespace details
} // namespace net
#endif // pipe_connection_h__
