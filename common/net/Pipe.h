#ifndef Pipe_h__
#define Pipe_h__

#include <string>

#include <boost/asio/io_service.hpp>
#include <boost/asio/windows/stream_handle.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

namespace net
{
namespace pipes
{

namespace details
{
	class ClientConnectOperation : public boost::asio::detail::operation
	{
	public:
		typedef boost::shared_ptr<boost::asio::windows::stream_handle> PipePtr;
		typedef boost::function<void (PipePtr, const boost::system::error_code& e)> CallbackFn;

		ClientConnectOperation(const HANDLE pipe, boost::asio::io_service& service, const CallbackFn& callback);
		void Do();
		void Completed(const boost::system::error_code& e);

	private:
		PipePtr m_Pipe;
		CallbackFn m_Callback;
	};
} // namespace details

//! Pipe acceptor/connector
class Pipe : boost::noncopyable
{
public:

	//! Pointer to the pipe handle
	typedef details::ClientConnectOperation::PipePtr Ptr;

	//! Pipe message type
	struct Type
	{
		enum Value
		{
			Stream		= 0,
			Message		= 1,
			Default		= Stream
		};
	};

	Pipe(const std::string& pipeName, boost::asio::io_service& service, const DWORD bufferSize, const Type::Value type = Type::Default);

	template<typename T>
	void AsyncAccept(const T& handler)
	{
		boost::system::error_code e;
		const HANDLE pipe = CreatePipe(e);
		boost::asio::detail::throw_error(e, "create");

		std::auto_ptr<details::ClientConnectOperation> operation(new details::ClientConnectOperation(pipe, m_Service, handler));
		operation->Do();
		operation.release();
	}

	void Accept(Ptr& client);
	void Accept(Ptr& client, boost::system::error_code& error);
	void Connect(Ptr& server);
	void Connect(Ptr& server, boost::system::error_code& error);

private:

	HANDLE CreatePipe(boost::system::error_code& e);
	HANDLE ConnectToPipe(boost::system::error_code& e);
	
private:
	const std::string m_PipeName;
	boost::asio::io_service& m_Service;
	const DWORD m_BufferSize;
	const Type::Value m_Type;
};

} // namespace pipes
} // namespace net


#endif // Pipe_h__