#include "PipeSecurity.h"
#include "exception/CheckHelpers.h"

#include <boost/scope_exit.hpp>

namespace net
{
namespace pipes
{
namespace details
{

PipeSecurity::PipeSecurity()
    : m_IsAllocated()
    , m_SecurityDescriptor()
    , m_ACL()
    , m_TokenUser()
{
}

void PipeSecurity::BuildAttributes(SECURITY_ATTRIBUTES& sa)
{
    DWORD dwAclSize;
    PSID pSidAnonymous = NULL;
    PSID pSidOwner = NULL;

    BOOST_SCOPE_EXIT(&pSidAnonymous, &pSidOwner, &m_IsAllocated, &m_SecurityDescriptor, &m_ACL)
    {
        if (pSidAnonymous)
            FreeSid(pSidAnonymous);
        if (pSidOwner)
            FreeSid(pSidOwner);

        if (!m_IsAllocated)
        {
            if (m_SecurityDescriptor)
                HeapFree(GetProcessHeap(), 0, m_SecurityDescriptor);
            if (m_ACL)
                HeapFree(GetProcessHeap(), 0, m_ACL);
        }
    }
    BOOST_SCOPE_EXIT_END;

    CHECK(!m_IsAllocated, cmn::Exception("already allocated"));

    SID_IDENTIFIER_AUTHORITY siaAnonymous = SECURITY_NT_AUTHORITY;

    m_SecurityDescriptor = (PSECURITY_DESCRIPTOR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, SECURITY_DESCRIPTOR_MIN_LENGTH);
    CHECK_LE(m_SecurityDescriptor, cmn::Exception("failed to allocate sec secriptor"));
    CHECK_LE(InitializeSecurityDescriptor(m_SecurityDescriptor, SECURITY_DESCRIPTOR_REVISION), cmn::Exception("failed to initialize sec descriptor"));

    // Build SID for authenticated user(may be some another sids needed here)
    CHECK_LE(AllocateAndInitializeSid(&siaAnonymous, 1, SECURITY_AUTHENTICATED_USER_RID, 0, 0, 0, 0, 0, 0, 0, &pSidAnonymous), cmn::Exception("failed to build anonymous sid"));

    CHECK(GetUserSid(&pSidOwner), cmn::Exception("faield to get owner id"));

    // Compute size of ACL
    dwAclSize = sizeof(ACL) +
        2 * (sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD)) +
        GetLengthSid(pSidAnonymous) +
        GetLengthSid(pSidOwner);

    m_ACL = (PACL)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwAclSize);
    CHECK_LE(m_ACL, cmn::Exception("failed to allocate ACL"));
    CHECK_LE(InitializeAcl(m_ACL, dwAclSize, ACL_REVISION), cmn::Exception("failed to initialize ACL"));

    CHECK_LE(AddAccessAllowedAce(m_ACL, ACL_REVISION, GENERIC_ALL, pSidOwner), cmn::Exception("failed to add access allowed ace to owner"));

    CHECK_LE(AddAccessAllowedAce(m_ACL, ACL_REVISION, GENERIC_ALL, pSidAnonymous), cmn::Exception("failed to add access allowed ace to anonymous"));

    CHECK_LE(SetSecurityDescriptorDacl(m_SecurityDescriptor, TRUE, m_ACL, FALSE), cmn::Exception("failed to set sec descruiptor DACL"));

    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = m_SecurityDescriptor;

    m_IsAllocated = TRUE;
}


BOOL PipeSecurity::GetUserSid(PSID* ppSidUser)
{
    HANDLE hToken;
    DWORD dwLength;

    if (!OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE, &hToken))
    {
        if (GetLastError() == ERROR_NO_TOKEN)
        {
            if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }


    if (!GetTokenInformation(hToken,       // handle of the access token
        TokenUser,    // type of information to retrieve
        m_TokenUser,   // address of retrieved information 
        0,            // size of the information buffer
        &dwLength     // address of required buffer size
        ))
    {
        if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
        {
            m_TokenUser = (PTOKEN_USER)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwLength);
            if (m_TokenUser == NULL)
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    if (!GetTokenInformation(hToken,     // handle of the access token
        TokenUser,  // type of information to retrieve
        m_TokenUser, // address of retrieved information 
        dwLength,   // size of the information buffer
        &dwLength   // address of required buffer size
        ))
    {
        HeapFree(GetProcessHeap(), 0, m_TokenUser);
        m_TokenUser = NULL;

        return FALSE;
    }

    *ppSidUser = m_TokenUser->User.Sid;
    return TRUE;
}

PipeSecurity::~PipeSecurity()
{
    if (m_IsAllocated)
    {
        if (m_SecurityDescriptor)
            HeapFree(GetProcessHeap(), 0, m_SecurityDescriptor);
        if (m_ACL)
            HeapFree(GetProcessHeap(), 0, m_ACL);
        if (m_TokenUser)
            HeapFree(GetProcessHeap(), 0, m_TokenUser);
        m_IsAllocated = FALSE;
    }
}

} // namespace details
} // namespace pipes
} // namespace net
