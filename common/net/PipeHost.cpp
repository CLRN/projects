#include "PipeHost.h"
#include "Pipe.h"
#include "exception/CheckHelpers.h"
#include "Connection.h"
#include "conversion/AnyCast.h"
#include "common/Modules.h"
#include "common/ServiceHolder.h"

namespace net
{
namespace pipes
{

DECLARE_CURRENT_MODULE(Modules::Network);

class Transport::Impl : public boost::enable_shared_from_this<Impl>
{
    friend class net::details::Channel<Pipe::Ptr>;

    //! Channel type
    class Channel : public net::details::Channel<Pipe::Ptr>
    {
    public:
        Channel(Impl& owner, const Pipe::Ptr& pipe)
            : net::details::Channel<Pipe::Ptr>(pipe)
            , m_Owner(owner)
        {
        }

        virtual void BeginReadHeader() override
        {
            const auto object = m_IoObject;
            if (object)
            {
                object->async_read_some(
                    boost::asio::buffer(&m_Size, sizeof(m_Size)), 
                    m_Strand.wrap(boost::bind(&Channel::ReadHeaderCallback, shared_from_this(), _1, _2))
                );   
            }
        }

        //! Begin asynchronous read to buffer
        virtual void BeginReadBody() override
        {
            const auto object = m_IoObject;
            if (object)
            {
                object->async_read_some(
                    boost::asio::buffer(&m_ReadBuffer[m_Read], m_Size - m_Read),
                    m_Strand.wrap(boost::bind(&Channel::ReadMessageCallback, shared_from_this(), _1, _2))
                );   
            }
        }

        virtual void Write(const std::vector<boost::asio::const_buffer>& buffers, boost::system::error_code& e) override
        {
            boost::asio::write(*m_IoObject, buffers, e); // using scattered write, sending all buffers(complete packet) or nothing	
        }

        virtual void ConnectionClosed(const boost::system::error_code& e, std::size_t bytes) override
        {
            if (m_Owner.m_ClientConnectedCallback)
                m_Owner.m_ClientConnectedCallback(shared_from_this(), e);
            m_Callback(IConnection::StreamPtr());
        }

    private:
        Impl& m_Owner; 
    };

public:
    Impl(ILog& log, const std::size_t startupInstances, std::size_t bufferSize)
		: m_Log(log)
        , m_BufferSize(bufferSize)
        , m_StartupInstances(startupInstances)
	{
	}

	~Impl()
	{
        Close();
	}

    void Receive(const std::string& endpoint, const Callback& callback)
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_ClientConnectedCallback = callback;
        m_Pipe.reset(new Pipe(MakeValidName(endpoint), cmn::Service::Instance(), m_BufferSize));
        for (std::size_t i = 0; i < m_StartupInstances; ++i)
            m_Pipe->AsyncAccept(boost::bind(&Impl::ClientAccepted, shared_from_this(), _1, _2));
    }

    IConnection::Ptr Connect(const std::string& endpoint)
    {
        Pipe pipe(MakeValidName(endpoint), cmn::Service::Instance(), m_BufferSize);

        Pipe::Ptr handle;
        pipe.Connect(handle);
        return boost::make_shared<Channel>(*this, handle);
    }

    void Close()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        m_Pipe.reset();
    }

private:

    std::string MakeValidName(const std::string& name)
    {
        static const std::string prefix = "\\\\.\\pipe\\";
        if (name.size() > prefix.size() && name.substr(0, prefix.size()) == prefix)
            return name;

        return prefix + name;
    }

	//! Accept client handler
	void ClientAccepted(const Pipe::Ptr& client, boost::system::error_code e)
	{
		// prepare to accept new client
        {
            boost::unique_lock<boost::mutex> lock(m_Mutex);
            if (m_Pipe)
                m_Pipe->AsyncAccept(boost::bind(&Impl::ClientAccepted, shared_from_this(), _1, _2));
        }

		if (e)
		{
            LOG_ERROR("Failed to accept new client: %s") % e.message();
			return; // failed to accept client
		}

		LOG_INFO("New client accepted");

		// construct new pipe instance
        const Channel::Ptr instance = boost::make_shared<Channel>(*this, client);
		
		// invoke callback
		m_ClientConnectedCallback(instance, e);
	}

private:
    ILog& m_Log;
	std::unique_ptr<Pipe> m_Pipe;
	const std::size_t m_BufferSize;
    const std::size_t m_StartupInstances;
    Callback m_ClientConnectedCallback;
    boost::mutex m_Mutex;
};

Transport::Transport(ILog& log, std::size_t startupInstances/* = 5*/, std::size_t bufferSize /*= DEFAULT_BUFFER_SIZE*/)
    : m_Impl(boost::make_shared<Impl>(log, startupInstances, bufferSize))
{

}

void Transport::Receive(const std::string& endpoint, const Callback& callback)
{
    m_Impl->Receive(endpoint, callback);
}

IConnection::Ptr Transport::Connect(const std::string& endpoint)
{
    return m_Impl->Connect(endpoint);
}

void Transport::Close()
{
    m_Impl->Close();
}

Transport::~Transport()
{
    m_Impl->Close();
    m_Impl.reset();
}


} // namespace pipes
} // namespace net
