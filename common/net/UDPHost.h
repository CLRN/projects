#ifndef UDPHost_h__
#define UDPHost_h__

#include "IHost.h"
#include "common/ILog.h"

namespace net
{
class UDPHost : public IHost
{
public:
	UDPHost(ILog& logger);
	~UDPHost();
	virtual IConnection::Ptr	Connect(const std::string& ep) override;
	virtual void				Receive(const std::string& endpoint, const Callback& callback) override;
    virtual void                Close() override;
private:

	class Impl;
	Impl* m_Impl;
};
}

#endif // UDPHost_h__
