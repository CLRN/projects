#include "Pipe.h"
#include "common/Modules.h"

namespace net
{
namespace pipes
{
namespace details
{

void ClientConnectedStub(boost::asio::detail::win_iocp_io_service* service, boost::asio::detail::operation* operation, const boost::system::error_code& e, std::size_t size);

ClientConnectOperation::ClientConnectOperation(const HANDLE pipe, boost::asio::io_service& service, const CallbackFn& callback) 
	: boost::asio::detail::operation(&ClientConnectedStub)
	, m_Pipe(new boost::asio::windows::stream_handle(service, pipe))
	, m_Callback(callback)
{
}

void ClientConnectOperation::Do()
{
	boost::asio::detail::win_iocp_io_service& iocpService = boost::asio::use_service<boost::asio::detail::win_iocp_io_service>(m_Pipe->get_io_service());
	iocpService.work_started();

	const BOOL result = ConnectNamedPipe(m_Pipe->native_handle(), this);
	const DWORD lastError = GetLastError();

	if (!result && lastError != ERROR_IO_PENDING)
		iocpService.on_completion(this, lastError);
	else
		iocpService.on_pending(this);
}

void ClientConnectOperation::Completed(const boost::system::error_code& e)
{
	m_Callback(m_Pipe, e);
}

void ClientConnectedStub(boost::asio::detail::win_iocp_io_service* service, boost::asio::detail::operation* operation, const boost::system::error_code& e, std::size_t /*size*/)
{
    std::auto_ptr<ClientConnectOperation> pipeOperation(static_cast<ClientConnectOperation*>(operation));
	if (service)
		pipeOperation->Completed(e);
}

} // namespace details

Pipe::Pipe(const std::string& pipeName, boost::asio::io_service& service, const DWORD bufferSize, const Type::Value type /*= Type::Default*/) : m_PipeName(pipeName)
	, m_Service(service)
	, m_Type(type)
	, m_BufferSize(bufferSize)
{
}

HANDLE Pipe::ConnectToPipe(boost::system::error_code& e)
{
	const HANDLE pipe = CreateFileA(m_PipeName.c_str(),                 
		GENERIC_READ | GENERIC_WRITE,            
		NULL,                                          
		NULL,                                       
		OPEN_EXISTING,                              
		FILE_FLAG_OVERLAPPED,                     
		NULL
	);

    const DWORD lastError = GetLastError();

	if (pipe == INVALID_HANDLE_VALUE)
		e = boost::system::error_code(lastError, boost::asio::error::get_system_category());
	else
		e = boost::system::error_code();
	return pipe;
}

HANDLE Pipe::CreatePipe(boost::system::error_code& e)
{
	const DWORD flags = m_Type == Type::Message 
		? PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT
		: PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT;

	const HANDLE pipe = CreateNamedPipeA( 
		m_PipeName.c_str(),
		PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | WRITE_DAC,
		flags,                
		PIPE_UNLIMITED_INSTANCES,
		m_BufferSize,
		m_BufferSize,
		NULL,
		NULL
	);

    const DWORD lastError = GetLastError();

	if (pipe == INVALID_HANDLE_VALUE)
	{
		e = boost::system::error_code(lastError, boost::asio::error::get_system_category());
		return NULL;
	}
	e = boost::system::error_code();
	return pipe;
}

void Pipe::Connect(Ptr& server, boost::system::error_code& error)
{
	assert(!server);
	const HANDLE pipe = ConnectToPipe(error);
	if (error)
		return;

	server.reset(new boost::asio::windows::stream_handle(m_Service, pipe));
}

void Pipe::Connect(Ptr& server)
{
	boost::system::error_code e;
	Connect(server, e);
	boost::asio::detail::throw_error(e, "connect");
}

void Pipe::Accept(Ptr& client, boost::system::error_code& error)
{
	assert(!client);
	const HANDLE pipe = CreatePipe(error);
	if (error)
		return;

	if (!ConnectNamedPipe(pipe, NULL))
		error = boost::system::error_code(GetLastError(), boost::asio::error::get_system_category());
	else
		client.reset(new boost::asio::windows::stream_handle(m_Service, pipe));
}

void Pipe::Accept(Ptr& client)
{
	boost::system::error_code e;
	Accept(client, e);
	boost::asio::detail::throw_error(e, "accept");
}

} // namespace pipes
} // namespace net