#include "common/Log.h"
#include "common/Modules.h"
#include "net/UDPHost.h"
#include "net/IConnection.h"
#include "common/ServiceHolder.h"
#include "net/ConnectionStream.h"

#include "gtest/gtest.h"

#include <iostream>

#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>

using testing::Range;
using testing::Combine;

Log logger;

net::UDPHost srv(logger);
net::UDPHost clnt(logger);

std::size_t g_Counter = 0;

void ReceiveFromClientCallback(const net::IConnection::StreamPtr& stream, const net::IConnection::Ptr& connection)
{
    net::ConnectionStream::Type out(*connection);
    std::copy(std::istream_iterator<char>(*stream), std::istream_iterator<char>(), std::ostream_iterator<char>(out));

	++g_Counter;
	if (g_Counter > 100)
	{
		cmn::Service::Instance().stop();

		std::cout << g_Counter << " messages transfered" << std::endl;
	}
}

void ClientConnectedCallback(const net::IConnection::Ptr& connection)
{
    connection->Receive(boost::bind(&ReceiveFromClientCallback, _1, connection));
}

void ReceiveFromServerCallback(const net::IConnection::StreamPtr& stream, const net::IConnection::Ptr& connection)
{
    net::ConnectionStream::Type out(*connection);
    std::copy(std::istream_iterator<char>(*stream), std::istream_iterator<char>(), std::ostream_iterator<char>(out));
	++g_Counter;
}

void TestFunc()
{
	srv.Receive("127.0.0.1:5000", boost::bind(&ClientConnectedCallback, _1));
	const net::IConnection::Ptr connection = clnt.Connect("127.0.0.1:5000");

    std::string buffer = "test data";
	connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer)));
	connection->Receive(boost::bind(&ReceiveFromServerCallback, _1, connection));

	cmn::Service::Instance().run();
}


TEST(Udp, Simple)
{
	TestFunc();	
}

