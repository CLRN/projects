#include "common/Log.h"
#include "common/Modules.h"
#include "common/ServiceHolder.h"
#include "common/FileSystem.h"
#include "net/FileHost.h"

#include <gtest/gtest.h>

#include <iostream>

#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/thread.hpp>
#include <boost/range/algorithm.hpp>


TEST(NetConnection, ReadWrite)
{
    Log log;
	fs::ScopedFile file;
    const std::wstring path = file;

    const std::vector<std::string> data = boost::assign::list_of("data 1 text, \n \n ok")("data 2 text")("data 3 text bla bla bla");

    {
        net::FileTransport host(log);
        const auto connection = host.Connect(conv::cast<std::string>(path));
        for (const auto& buffer : data)
            connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(buffer)));

    }
    {
        std::vector<std::string> received;
        const auto callback = [&received](const net::IConnection::StreamPtr& stream)
        {
            *stream >> std::noskipws;
            received.push_back(std::string());
            std::copy(std::istream_iterator<char>(*stream), std::istream_iterator<char>(), std::back_inserter(received.back()));
        };

        net::FileTransport host(log);
        const auto connection = host.Connect(conv::cast<std::string>(path));
        connection->Receive(callback);

        cmn::Service::Instance().poll();

        EXPECT_EQ(data, received);
    }
}

TEST(NetConnection, ReadWriteMultithread)
{
    Log log;
    fs::ScopedFile file;
    const std::wstring path = file;

    unsigned threads = 100;

    std::vector<std::string> etalon;
    for (unsigned i = 0; i < threads; ++i)
        etalon.push_back(std::string((i + 1) * 10, static_cast<char>(i)));
    
    {
        net::FileTransport host(log);
        const auto connection = host.Connect(conv::cast<std::string>(path));

        const auto func = [&](unsigned i)
        {
            const std::string& data = etalon[i];
            connection->Send(net::IConnection::Buffers(1, boost::asio::buffer(data)));
        };

        boost::thread_group pool;
        for (unsigned i = 0; i < threads; ++i)
            pool.create_thread(boost::bind<void>(func, i));

        pool.join_all();
    }
    {
        std::vector<std::string> received;
        const auto callback = [&received](const net::IConnection::StreamPtr& stream)
        {
            *stream >> std::noskipws;
            received.push_back(std::string());
            std::copy(std::istream_iterator<char>(*stream), std::istream_iterator<char>(), std::back_inserter(received.back()));
        };

        net::FileTransport host(log);
        const auto connection = host.Connect(conv::cast<std::string>(path));
        connection->Receive(callback);

        cmn::Service::Instance().poll();

        boost::sort(etalon);
        boost::sort(received);
        EXPECT_EQ(etalon, received);
    }
}