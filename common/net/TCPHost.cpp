#include "TCPHost.h"
#include "exception/CheckHelpers.h"
#include "Connection.h"
#include "conversion/AnyCast.h"
#include "common/Modules.h"
#include "common/ServiceHolder.h"

#include <boost/asio.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

namespace net
{
namespace
{

DECLARE_CURRENT_MODULE(Modules::Network);

class Impl : public boost::enable_shared_from_this<Impl>, public net::tcp::ITransport
{
    typedef boost::asio::ip::tcp::socket Socket;
    typedef boost::shared_ptr<Socket> SocketPtr;

    //! Channel type
    class Channel : public net::details::Channel<SocketPtr>
    {
    public:
        Channel(Impl& owner, const SocketPtr& pipe)
            : net::details::Channel<SocketPtr>(pipe)
            , m_Owner(owner)
        {
        }

        virtual void BeginReadHeader() override
        {
            LTRACE(m_Owner.m_Log, CURRENT_MODULE_ID, "Reading header: %s") % m_HeaderSize;

            const auto object = m_IoObject;
            if (object)
            {
                object->async_read_some(
                    boost::asio::buffer(reinterpret_cast<char*>(&m_Size) + m_HeaderSize, sizeof(m_Size) - m_HeaderSize),
                    m_Strand.wrap(boost::bind(&Channel::ReadHeaderCallback, shared_from_this(), _1, _2))
                );   
            }
        }

        //! Begin asynchronous read to buffer
        virtual void BeginReadBody() override
        {
            LTRACE(m_Owner.m_Log, CURRENT_MODULE_ID, "Reading body, read: %s, size: %s") % m_Read % m_Size;

            const auto object = m_IoObject;
            if (object)
            {
                object->async_read_some(
                    boost::asio::buffer(&m_ReadBuffer[m_Read], m_Size - m_Read),
                    m_Strand.wrap(boost::bind(&Channel::ReadMessageCallback, shared_from_this(), _1, _2))
                );   
            }
        }

        virtual void OnBytesRead(const std::size_t bytes) override
        {
            LTRACE(m_Owner.m_Log, CURRENT_MODULE_ID, "Read bytes: %s, size: %s") % bytes % m_Size;
        }

        virtual void Write(const std::vector<boost::asio::const_buffer>& buffers, boost::system::error_code& e) override
        {
            boost::asio::write(*m_IoObject, buffers, e); // using scattered write, sending all buffers(complete packet) or nothing	
        }

        virtual void ConnectionClosed(const boost::system::error_code& e, std::size_t bytes) override
        {
            LINFO(m_Owner.m_Log, CURRENT_MODULE_ID, "Connection closed, size: %s, bytes: %s, error: (%s) %s") % m_Size % bytes % e.value() % conv::cast<std::wstring>(e.message());

            if (m_Owner.m_ClientConnectedCallback)
                m_Owner.m_ClientConnectedCallback(shared_from_this(), e);
            m_Callback(IConnection::StreamPtr());
        }

    private:
        Impl& m_Owner; 
    };

public:
	Impl(ILog& log, std::size_t bufferSize)
		: m_Log(log)
        , m_BufferSize(bufferSize)
        , m_Acceptor(cmn::Service::Instance())
	{
	}

	~Impl()
	{
        Close();
	}

    virtual void Receive(const std::string& endpoint, const Callback& callback) override
    {
        m_ClientConnectedCallback = callback;

        const boost::asio::ip::tcp::endpoint ep(ParseEP(endpoint));
        m_Acceptor.open(ep.protocol());
        m_Acceptor.bind(ep);
        m_Acceptor.listen();

        Accept();       
        Accept();
    }

    void Accept()
    {
        const auto socket = boost::make_shared<Socket>(cmn::Service::Instance());
        m_Acceptor.async_accept(*socket, boost::bind(&Impl::ClientAccepted, shared_from_this(), socket, boost::asio::placeholders::error));
    }

    virtual IConnection::Ptr Connect(const std::string& endpoint) override
    {
        const SocketPtr socket = boost::make_shared<Socket>(cmn::Service::Instance());
        socket->connect(ParseEP(endpoint));
        return boost::make_shared<Channel>(*this, socket);
    }

    virtual void Close() override
    {
        m_Acceptor.close();
    }

private:

    boost::asio::ip::tcp::endpoint ParseEP(const std::string& endpoint)
    {
        std::vector<std::string> parts;
        boost::algorithm::split(parts, endpoint, boost::algorithm::is_any_of(":"));
        CHECK(parts.size() == 2, cmn::Exception("Wrong endpoint"), endpoint);

        return boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(parts.front()), conv::cast<short>(parts.back()));
    }

	//! Accept client handler
	void ClientAccepted(const SocketPtr& client, boost::system::error_code e)
	{
		// prepare to accept new client
            Accept();

	    if (e)
	    {
                LOG_ERROR("Failed to accept new client: %s") % e.message();
		    return; // failed to accept client
	     }

            LOG_INFO("New client accepted: %s") % client->remote_endpoint();

	     // construct new pipe instance
             const Channel::Ptr instance = boost::make_shared<Channel>(*this, client);
		
	     // invoke callback
	     m_ClientConnectedCallback(instance, e);
	}

private:
    ILog& m_Log;
	boost::asio::ip::tcp::acceptor m_Acceptor;
	const std::size_t m_BufferSize;
    Callback m_ClientConnectedCallback;
};

} // anonymous namespace

net::IHost::Ptr net::tcp::ITransport::Instance(ILog& log, std::size_t bufferSize /*= DEFAULT_BUFFER_SIZE*/)
{
    return boost::make_shared<Impl>(log, bufferSize);
}

} // namespace net
