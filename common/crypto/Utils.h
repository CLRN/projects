﻿#pragma once

// Windows types
#include <WTypes.h>

// STL
#include <string>

// Boost
#include <boost/asio/detail/socket_types.hpp>
#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <boost/tuple/tuple.hpp>

namespace boost {
namespace asio {
namespace ssl {
    class verify_context;
}
}
}

namespace crypto
{
    typedef boost::optional<boost::system::error_code&> error_code_ot;
    struct key_format
    {
        enum Enum
        {
            asn1 = 0,
            pem_with_header,
            pem,
            pem_rsa,    // PEM RSAPubicKey
            asn1_rsa    // DER RSAPubicKey
        };
    };
}

namespace crypto
{
    boost::optional<std::string> load_file_as_binary(const boost::filesystem::path& path);
    void save_file_as_binary(const boost::filesystem::path& path, const std::string& content);

    // TODO boost::system::error_code

    boost::tuple<LONG, DWORD, std::wstring> check_digital_sign(const boost::filesystem::path& path);
    boost::optional<std::wstring> get_file_version(const boost::filesystem::path& path);

    void verify_certificate(boost::asio::ssl::verify_context& ctx);

    void verify(std::istream& data, const std::vector<char>& signed_data, const std::vector<char>& key, key_format::Enum format);
    void sign(std::istream& data, const std::vector<char>& key, key_format::Enum format, std::vector<char>& signed_data);
    void digest(std::istream& data, std::vector<char>& digest);
    void md5(std::istream& data, std::vector<char>& digest);
}

namespace crypto
{
    namespace n_secure_errc
    {

        inline const boost::system::error_category& error_category();

        /// error codes
        struct errc_t
        {
            enum Enum
            {
                pem_write_bio_err = 0,
                bio_read_err,
                bio_alloc_mem_buf_err,
                b64_decode_err,
                bio_read_key_err,
                digest_calc_err,
                digest_calc_empty_err,
                get_ref_rsa_err,
                rsa_pub_decrypt_err,
                rsa_priv_encrypt_err
            };
        };

        inline boost::system::error_code make_error_code(errc_t::Enum e)
        {
            return boost::system::error_code(static_cast<int>(e), error_category());
        }

        inline boost::system::error_condition make_error_condition(errc_t::Enum e)
        {
            return boost::system::error_condition(static_cast<int>(e), error_category());
        }
    }
}

namespace boost
{
    namespace system
    {
        template <>
        struct is_error_code_enum<crypto::n_secure_errc::errc_t::Enum>{ static const bool value = true; };
    };
};//
