﻿// this
#include "utils.h"

// STL
#include <sstream>

// Boost
#include "boost\filesystem\fstream.hpp"

// Windows
#include <Softpub.h>    // для проверки цифровой подписи
#include <wincrypt.h>   // для проверки цифровой подписи
#include <wintrust.h>   // для проверки цифровой подписи

namespace crypto
{
//----------------------------------------------------------------------------------------------------
boost::optional<std::string> load_file_as_binary(const boost::filesystem::path& path)
{
    boost::filesystem::ifstream file(path, std::ios::binary);
    if (file.is_open())
    {
        std::ostringstream buf;
        buf << file.rdbuf();
        return buf.str();
    }
    return boost::optional<std::string>();
}
//----------------------------------------------------------------------------------------------------
void save_file_as_binary(const boost::filesystem::path& path, const std::string& content)
{
    boost::filesystem::ofstream file(path, std::ios::binary);
    if (file.is_open())
    {
        file << content;
    }
}
//----------------------------------------------------------------------------------------------------
boost::tuple<LONG, DWORD, std::wstring> check_digital_sign(const boost::filesystem::path& path)
{
    // пример из MSDN

    WINTRUST_FILE_INFO wtfi;
    {
        ::memset(&wtfi, 0, sizeof(wtfi));

        wtfi.cbStruct = sizeof(WINTRUST_FILE_INFO);
        wtfi.pcwszFilePath = path.wstring().c_str();
        wtfi.hFile = NULL;
        wtfi.pgKnownSubject = NULL;
    }

    /*
    wv_policy_guid specifies the policy to apply on the file
    WINTRUST_ACTION_GENERIC_VERIFY_V2 policy checks:
    
    1) The certificate used to sign the file chains up to a root 
    certificate located in the trusted root certificate store. This 
    implies that the identity of the publisher has been verified by 
    a certification authority.
    
    2) In cases where user interface is displayed (which this example
    does not do), WinVerifyTrust will check for whether the  
    end entity certificate is stored in the trusted publisher store,  
    implying that the user trusts content from this publisher.
    
    3) The end entity certificate has sufficient permission to sign 
    code, as indicated by the presence of a code signing EKU or no 
    EKU.
    */

    GUID wv_policy_guid = WINTRUST_ACTION_GENERIC_VERIFY_V2;

    WINTRUST_DATA wtd;
    {
        memset(&wtd, 0, sizeof(wtd));

        wtd.cbStruct = sizeof(wtd);

        // Use default code signing EKU.
        wtd.pPolicyCallbackData = NULL;

        // No data to pass to SIP.
        wtd.pSIPClientData = NULL;

        // Disable WVT UI.
        wtd.dwUIChoice = WTD_UI_NONE;

        // No revocation checking.
        wtd.fdwRevocationChecks = WTD_REVOKE_NONE; 

        // Verify an embedded signature on a file.
        wtd.dwUnionChoice = WTD_CHOICE_FILE;

        // Default verification.
        wtd.dwStateAction = 0;

        // Not applicable for default verification of embedded signature.
        wtd.hWVTStateData = NULL;

        // Not used.
        wtd.pwszURLReference = NULL;

        // This is not applicable if there is no UI because it changes 
        // the UI to accommodate running applications instead of 
        // installing applications.
        wtd.dwUIContext = 0;

        // Set pFile.
        wtd.pFile = &wtfi;
    }

    // WinVerifyTrust verifies signatures as specified by the GUID and Wintrust_Data.
    const LONG status = ::WinVerifyTrust(NULL, &wv_policy_guid, &wtd);
    DWORD last_err = S_OK;
    std::wstring msg;

    switch (status)
    {
        case ERROR_SUCCESS:
            /*
            Signed file:
                - Hash that represents the subject is trusted.

                - Trusted publisher without any verification errors.

                - UI was disabled in dwUIChoice. No publisher or time stamp chain errors.

                - UI was enabled in dwUIChoice and the user clicked "Yes" when asked to install and run the signed subject.
            */
            msg = L"The file is signed and the signature was verified.";
            break;

        case TRUST_E_NOSIGNATURE:
            // The file was not signed or had a signature that was not valid.

            // Get the reason for no signature.
            last_err = ::GetLastError();
            if (TRUST_E_NOSIGNATURE == last_err || TRUST_E_SUBJECT_FORM_UNKNOWN == last_err || TRUST_E_PROVIDER_UNKNOWN == last_err)
            {
                // The file was not signed.
                msg = L"The file is not signed.";
            } 
            else 
            {
                // The signature was not valid or there was an error opening the file.
                msg = L"An unknown error occurred trying to verify the signature of the file.";
            }

            break;

        case TRUST_E_EXPLICIT_DISTRUST:
            // The hash that represents the subject or the publisher is not allowed by the admin or user.
            msg = L"The signature is present, but specifically disallowed.";
            break;

        case TRUST_E_SUBJECT_NOT_TRUSTED:
            // The user clicked "No" when asked to install and run.
            msg = L"The signature is present, but not trusted.";
            break;

        case CRYPT_E_SECURITY_SETTINGS:
            /*
            The hash that represents the subject or the publisher 
            was not explicitly trusted by the admin and the 
            admin policy has disabled user trust. No signature, 
            publisher or time stamp errors.
            */
            msg = L"CRYPT_E_SECURITY_SETTINGS - The hash "
                  L"representing the subject or the publisher wasn't "
                  L"explicitly trusted by the admin and admin policy "
                  L"has disabled user trust. No signature, publisher "
                  L"or timestamp errors.";
            break;

        default:
            // The UI was disabled in dwUIChoice or the admin policy 
            // has disabled user trust. status contains the 
            // publisher or time stamp chain error.
            msg = L"check status value";
            break;
    }

    return boost::make_tuple(status, last_err, msg);
}
//----------------------------------------------------------------------------------------------------
boost::optional<std::wstring> get_file_version(const boost::filesystem::path& path)
{
    boost::optional<std::wstring> version_o;
    {
        DWORD scratch = 0;
        const DWORD inf_size = ::GetFileVersionInfoSizeW(path.wstring().c_str(), &scratch);
        if (inf_size)
        {
            std::vector<unsigned char> inf_buff(inf_size, 0);

            if (::GetFileVersionInfoW(path.wstring().c_str(), 0, inf_size, &inf_buff[0]))
            {
                DWORD* lang_char_ptr;
                UINT size;
                if (::VerQueryValueW(&inf_buff[0], L"\\VarFileInfo\\Translation", (void**)(&lang_char_ptr), &size))
                {
                    wchar_t resource_str[80];
                    if (::VerLanguageNameW(LOWORD(*lang_char_ptr), resource_str, sizeof(resource_str)))
                    {
//                        LOG_MYPATROL_INFO(L"language: " << resource_str);
                    }

                    wsprintfW(resource_str, L"\\StringFileInfo\\%04X%04X\\FileVersion", LOWORD(*lang_char_ptr), HIWORD(*lang_char_ptr));
                    wchar_t version_str[32];
                    wchar_t* version_ptr = version_str;
                    if (::VerQueryValueW(&inf_buff[0], resource_str, (void**)(&version_ptr), &size))
                    {
                        version_o = std::wstring(version_ptr);
                    }
                }
            }
            else
            {
                // version_o будет неинициализированна
            }
        }
        else
        {
            // version_o будет неинициализированна
        }
    }
    return version_o;
}
//----------------------------------------------------------------------------------------------------
} //namespace crypto
