﻿// this
#include "utils.h"
#include "exception/ErrorCodeInfo.h"

// Boost
#pragma warning (disable : 4512)
#include <boost/scope_exit.hpp>
#include <boost/asio/ssl/verify_context.hpp>
#include <boost/bind.hpp>
#include <boost/scoped_array.hpp>

#include "exception/CheckHelpers.h"

#include <openssl/evp.h>
#include <openssl/md5.h>

// Windows
#include <Wincrypt.h>

namespace crypto
{
///---------------------------------------------------------------------------------------------------------
void add_cert_to_store(HCERTSTORE store, X509* cert, PCCERT_CONTEXT* cert_ctx, DWORD dwAddDisposition)
{
    boost::shared_ptr<BIO> b(BIO_new(BIO_s_mem()), boost::bind(&BIO_free, _1));

    CHECK(PEM_write_bio_X509(b.get(), cert), cmn::Exception("PEM_write_bio_X509") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::pem_write_bio_err)));

    std::vector<char> buff_b64;
    {
        char tmp_buff[256];
        int tmp_buff_size = sizeof(tmp_buff);

        while(1)
        {
            int len = BIO_read(b.get(), tmp_buff, tmp_buff_size);

            if (len > 0)
            {
                buff_b64.insert(buff_b64.end(), tmp_buff, tmp_buff + len);

                if (len < tmp_buff_size)
                    break;
            }
            else if (len == 0)
                break;
            else
            {
                THROW(cmn::Exception("bio_read") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::bio_read_err)));
                return;
            }
        }
    }

    buff_b64.push_back(0);

    std::vector<char> buff_DER;
    {
        DWORD req_len = 0;
        CHECK_LE(CryptStringToBinaryA(&buff_b64[0], 0, CRYPT_STRING_BASE64HEADER, NULL, &req_len, NULL, NULL) || req_len == 0, cmn::Exception("CryptStringToBinaryA"));

        buff_DER.resize(req_len);
        CHECK_LE(CryptStringToBinaryA(&buff_b64[0], 0, CRYPT_STRING_BASE64HEADER, reinterpret_cast<BYTE*>(&buff_DER[0]), &req_len, NULL, NULL), cmn::Exception("CryptStringToBinaryA"));
    }

    CHECK_LE(CertAddEncodedCertificateToStore(store, X509_ASN_ENCODING, reinterpret_cast<const BYTE*>(&buff_DER[0]), buff_DER.size(), dwAddDisposition, cert_ctx), cmn::Exception("CertAddEncodedCertificateToStore"));

    return;
}

///---------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------
void verify_certificate(boost::asio::ssl::verify_context& ctx) 
{
    HCERTSTORE tmp_store = NULL;
    PCCERT_CONTEXT cert_ctx = NULL;
    PCCERT_CHAIN_CONTEXT chain_ctx = NULL;

    BOOST_SCOPE_EXIT((&tmp_store)(&cert_ctx)(&chain_ctx))
    {
        if (chain_ctx)
            CertFreeCertificateChain(chain_ctx);

        if (cert_ctx)
            CertFreeCertificateContext(cert_ctx);

        if (tmp_store)
            CertCloseStore(tmp_store, CERT_CLOSE_STORE_FORCE_FLAG);
    }
    BOOST_SCOPE_EXIT_END;

    tmp_store = CertOpenStore(CERT_STORE_PROV_MEMORY, X509_ASN_ENCODING, NULL, CERT_STORE_CREATE_NEW_FLAG, NULL);

    CHECK(tmp_store, cmn::Exception("OpenStore") << cmn::ErrorCodeInfo(boost::system::error_code(::GetLastError(), boost::system::system_category())));

    X509* encoded_cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());

    add_cert_to_store(tmp_store, encoded_cert, &cert_ctx, CERT_STORE_ADD_REPLACE_EXISTING);

    CERT_CHAIN_PARA chain_para = {0};
    chain_para.cbSize = sizeof(chain_para);

    CHECK_LE(CertGetCertificateChain(HCCE_LOCAL_MACHINE, cert_ctx, NULL, NULL, &chain_para, 0, NULL, &chain_ctx), cmn::Exception("CertGetCertificateChain"));

    CERT_CHAIN_POLICY_PARA policy_para;
    memset(&policy_para, 0x0, sizeof(policy_para));
    policy_para.cbSize = sizeof(policy_para);
    policy_para.dwFlags = CERT_CHAIN_POLICY_IGNORE_ALL_REV_UNKNOWN_FLAGS;
    policy_para.pvExtraPolicyPara = NULL;

    CERT_CHAIN_POLICY_STATUS policy_status;
    policy_status.cbSize = sizeof(policy_status);

    CHECK_LE(CertVerifyCertificateChainPolicy(CERT_CHAIN_POLICY_SSL, chain_ctx, &policy_para, &policy_status), cmn::Exception("CertVerifyCertificateChainPolicy"));
    CHECK(!policy_status.dwError, cmn::Exception("Policy error") << cmn::ErrorCodeInfo(boost::system::error_code(policy_status.dwError, boost::system::system_category())));
}
///---------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------
EVP_PKEY* load_key(const std::vector<char>& key_data, key_format::Enum format, bool pub)
{
    CHECK(key_data.empty(), cmn::Exception("Empty data"));

    boost::shared_ptr<BIO> key_bio(BIO_new_mem_buf(const_cast<char*>(&key_data[0]), key_data.size()), boost::bind(&BIO_free, _1));
    CHECK(key_bio, cmn::Exception("BIO_new_mem_buf") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::bio_alloc_mem_buf_err)));

    EVP_PKEY *pkey = NULL;
    switch (format)
    {
    case key_format::asn1:
        pkey = pub ? d2i_PUBKEY_bio(key_bio.get(), NULL)
            : d2i_PrivateKey_bio(key_bio.get(), NULL);
        break;

    case key_format::pem_with_header:
        pkey = pub ? PEM_read_bio_PUBKEY(key_bio.get(), NULL, NULL, NULL)
            : PEM_read_bio_PrivateKey(key_bio.get(), NULL, NULL, NULL);
        break;

    case key_format::pem:
        {
            std::vector<char> b64decoded(key_data.size()*2);
            {
                EVP_ENCODE_CTX ctx;
                EVP_DecodeInit(&ctx);
                int decoded = 0, tmp = 0;

                int i = EVP_DecodeUpdate(&ctx,
                    reinterpret_cast<unsigned char*>(&b64decoded[0]), &tmp,
                    reinterpret_cast<const unsigned char*>(&key_data[0]), key_data.size());

                if (i < 0)
                {
                    THROW(cmn::Exception("b64_decode_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::b64_decode_err)));
                    return NULL;
                }

                decoded += tmp;

                i = EVP_DecodeFinal(&ctx, reinterpret_cast<unsigned char*>(&b64decoded[decoded]), &tmp);
                if (i < 0)
                {
                    THROW(cmn::Exception("b64_decode_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::b64_decode_err)));
                    return NULL;
                }

                decoded += tmp;
                b64decoded.resize(decoded);

                pkey = load_key(b64decoded, key_format::asn1, pub);
            }
        }break;

    case key_format::pem_rsa:
    case key_format::asn1_rsa:
        {
            RSA* rsa
                = (format == key_format::pem_rsa)

                ? (pub ? PEM_read_bio_RSAPublicKey(key_bio.get(), NULL, NULL, NULL)
                : PEM_read_bio_RSAPrivateKey(key_bio.get(), NULL, NULL, NULL))

                : (pub ? d2i_RSAPublicKey_bio(key_bio.get(), NULL)
                : d2i_RSAPrivateKey_bio(key_bio.get(), NULL));

            if (rsa)
            {
                pkey = EVP_PKEY_new();

                if (pkey && !EVP_PKEY_set1_RSA(pkey, rsa))
                    EVP_PKEY_free(pkey), pkey = NULL;

                RSA_free(rsa);
            }
        }break;
    }

    if (!pkey)
    {
        THROW(cmn::Exception("bio_read_key_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::bio_read_key_err)));
        return NULL;
    }

    return pkey;
}
///---------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------
void digest(std::istream& data, std::vector<char>& digest)
{
    bool err = false;
    SHA256_CTX digest_ctx;

    err = !SHA256_Init(&digest_ctx);

    char buff[1024];
    const auto buff_size = sizeof(buff);

    std::streamoff pos = data.tellg();

    while(!err)
    {
        data.read(buff, buff_size);
        const std::size_t read = static_cast<std::size_t>(data.gcount());

        if (read && !SHA256_Update(&digest_ctx, buff, read))
        {
            err = true;
            break;
        }

        if (read < buff_size && data.eof())
        {
            digest.resize(SHA256_DIGEST_LENGTH);
            err = !SHA256_Final(reinterpret_cast<unsigned char*>(&digest[0]), &digest_ctx);
            break;
        }

        err = data.fail();
    }

    data.clear();
    data.seekg(pos);

    if (err)
        THROW(cmn::Exception("digest_calc_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::digest_calc_err)));
}
///---------------------------------------------------------------------------------------------------------

void md5(std::istream& data, std::vector<char>& digest)
{
    MD5_CTX digest_ctx;

    CHECK(MD5_Init(&digest_ctx), cmn::Exception("MD5_Init"));

    char buff[1024];
    const auto buff_size = sizeof(buff);

    std::streamoff pos = data.tellg();

    for (;;)
    {
        data.read(buff, buff_size);
        const std::size_t read = static_cast<std::size_t>(data.gcount());

        if (read)
            CHECK(MD5_Update(&digest_ctx, buff, read), cmn::Exception("MD5_Update"), read, pos);

        if (read < buff_size)
        {
            digest.resize(MD5_DIGEST_LENGTH);
            CHECK(MD5_Final(reinterpret_cast<unsigned char*>(&digest[0]), &digest_ctx), cmn::Exception("MD5_Final"), read, pos);
            break;
        }
    }

    data.clear();
    data.seekg(pos);
}

///---------------------------------------------------------------------------------------------------------
void verify(std::istream& data, const std::vector<char>& signed_data, const std::vector<char>& key, key_format::Enum format)
{
    EVP_PKEY* pubkey = load_key(key, format, true);

    boost::shared_ptr<RSA> rsa(EVP_PKEY_get1_RSA(pubkey), boost::bind(&RSA_free, _1));
    EVP_PKEY_free(pubkey);

    if (!rsa)
        THROW(cmn::Exception("get_ref_rsa_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::get_ref_rsa_err)));

    int keysize = RSA_size(rsa.get());

    boost::scoped_array<unsigned char> rsa_out(new unsigned char[keysize]);

    int sig_len  = RSA_public_decrypt(signed_data.size(), reinterpret_cast<const unsigned char*>(&signed_data[0]), rsa_out.get(), rsa.get(), RSA_PKCS1_PADDING);
    if (sig_len == -1)
        THROW(cmn::Exception("rsa_pub_decrypt_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::rsa_pub_decrypt_err)));

    std::vector<char> hash;
    digest(data, hash);

    if (hash.empty())
        THROW(cmn::Exception("digest_calc_empty_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::digest_calc_empty_err)));

    CHECK(!memcmp(rsa_out.get(), &hash[0], hash.size()), cmn::Exception("Verify"));
}
///---------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------
void sign(std::istream& data, const std::vector<char>& key, key_format::Enum format, std::vector<char>& signed_data)
{
    EVP_PKEY* privkey = load_key(key, format, false);

    boost::shared_ptr<RSA> rsa(EVP_PKEY_get1_RSA(privkey), boost::bind(&RSA_free, _1));
    EVP_PKEY_free(privkey);

    std::vector<char> hash;
    digest(data, hash);

    if (hash.empty())
        THROW(cmn::Exception("digest_calc_empty_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::digest_calc_empty_err)));

    int keysize = RSA_size(rsa.get());
    signed_data.resize(keysize);

    int sign_res = RSA_private_encrypt(hash.size(),
        reinterpret_cast<const unsigned char*>(&hash[0]),
        reinterpret_cast<unsigned char*>(&signed_data[0]), rsa.get(), RSA_PKCS1_PADDING);

    if (sign_res == -1)
        THROW(cmn::Exception("rsa_priv_encrypt_err") << cmn::ErrorCodeInfo(n_secure_errc::make_error_code(n_secure_errc::errc_t::rsa_priv_encrypt_err)));
}
///---------------------------------------------------------------------------------------------------------
} //namespace crypto

namespace crypto
{
    namespace n_secure_errc
    {
        class error_category_impl : public boost::system::error_category
        {
            virtual const char* name() const
            {
                return "secure utils";
            }

            virtual std::string message(int e) const
            {
                switch (e)
                {
                    case errc_t::pem_write_bio_err:
                        return "Failed to write pem data to BIO";
                    case errc_t::bio_read_err:
                        return "Failed to read from BIO";
                    case errc_t::bio_alloc_mem_buf_err:
                        return "Failed to create BIO on supplied buffer";
                    case errc_t::b64_decode_err:
                        return "Failed to decode from b64";
                    case errc_t::bio_read_key_err:
                        return "Failed to read key from BIO";
                    case errc_t::digest_calc_err:
                        return "Failed to calculate message digest";
                    case errc_t::digest_calc_empty_err:
                        return "Message digest is empty";
                    case errc_t::get_ref_rsa_err:
                        return "Failed to get the referenced RSA key";
                    case errc_t::rsa_pub_decrypt_err:
                        return "RSA decryption failed";
                    case errc_t::rsa_priv_encrypt_err:
                        return "RSA encryption failed";
                default:
                    return "Unknown secure error";
                }
            }
        };

        const boost::system::error_category& error_category()
        {
            static error_category_impl instance;
            return instance;
        }
    }
}

#pragma warning (default : 4512)
