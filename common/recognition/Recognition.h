#ifndef Recognition_h__
#define Recognition_h__

#include <string>
#include <list>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace rec
{

class IRecognizer
{
public:

    struct Result
    {
        typedef std::list<Result> List;
        cv::Rect m_Rect;
        std::wstring m_Text;
        bool operator == (const Result& another) const;
    };

    typedef boost::shared_ptr<IRecognizer> Ptr;

    virtual ~IRecognizer() {}
    virtual Result::List Recognize(const cv::Mat& image) = 0;
    virtual cv::Mat CaptureWindow(const unsigned window, const cv::Rect& region = cv::Rect()) = 0;
    virtual cv::Rect FindObject(const cv::Mat& src, const cv::Mat& object) = 0;
    virtual std::wstring RecognizeText(const cv::Mat& image) = 0;
    virtual void Draw(const unsigned window, const cv::Point& point, const std::string& text) = 0;

    static Ptr Instance();
};

} // namespace rec


#endif // Recognition_h__
