#ifndef RecognizableArea_h__
#define RecognizableArea_h__

#include "common/ILog.h"
#include "Recognition.h"
#include "serialization/ISerializable.h"

#include <vector>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace rec
{
    
class IRecognizableArea : public virtual cmn::srl::ISerializable
{
public:
    typedef boost::shared_ptr<IRecognizableArea> Ptr;
    typedef std::vector<Ptr> List;

    virtual ~IRecognizableArea() {}
    virtual void SetArea(const cv::Rect& rect) = 0;
    virtual const cv::Rect& GetArea() const = 0;
    virtual bool IsEqual(const cv::Rect& area) const = 0;

    static Ptr Instance(const cv::Rect& rect = cv::Rect());
};

} // namespace rec

#endif // RecognizableArea_h__
