﻿#include "recognition/Recognition.h"
#include "exception/CheckHelpers.h"
#include "conversion/AnyCast.h"
#include "recognition/OCR.h"

#include <windows.h>

#include <gtest/gtest.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

rec::IRecognizer::Ptr g_Recognizer;

TEST(Train, Simple)
{
    const auto ocr = rec::IOCR::Instance("../../../data/ocr/storage/");
    ocr->Assemble();
//     ocr->Train();
// // 
//     const std::wstring text = ocr->Recognize(cv::imread("../../../data/ocr/input/i.tif"));
}

rec::IRecognizer::Result::List RecognizeFile(const char* file)
{
    if (!g_Recognizer)
        g_Recognizer = rec::IRecognizer::Instance();
    const cv::Mat image = cv::imread(file);
    return g_Recognizer->Recognize(image);
}

void ExpectFound(const rec::IRecognizer::Result::List& result, const std::wstring& text)
{
    for (const auto& res : result)
    {
        if (boost::algorithm::iequals(res.m_Text, text))
            return;
    }
    FAIL() << "text not found in results: " << text;
}

// L"♠♣♦♥"
const wchar_t POKERSTARS_TABLE_WINDOW_CLASS[] = L"PokerStarsTableFrameClass";
std::wstring GetWindowClass(HWND handle)
{
    wchar_t name[512] = {0};
    CHECK_LE(GetClassNameW(handle, name, _countof(name)), cmn::Exception("Failed to get window class"));
    return name;
}

void Check4Inversion(cv::Mat& src)
{
    cv::Mat temp;
    cv::threshold(src, temp, 255 / 2, 255, cv::THRESH_BINARY);

    const int nonZeroElements = cv::countNonZero(temp);
    const int totalElements = temp.size().height * temp.size().width;

    // if non zero pixels more then zero pixels(white space more than black space)
    // we should not invert image
    if (nonZeroElements <= (totalElements - nonZeroElements))
        cv::bitwise_not(src, src);
}

std::wstring ParseBoxFile(const std::string& name)
{
    std::ifstream ifs(name.c_str(), std::ios::binary);
    CHECK(ifs.is_open(), cmn::Exception("Failed to open box file"), name);

    std::string result;
    std::string buffer;
    while (std::getline(ifs, buffer))
        result += buffer.substr(0, 1);
    return conv::cast<std::wstring>(result);
}
/*
TEST(Recognition, Generate)
{
    unsigned counter = 0;

    boost::filesystem::directory_iterator it("../../../data/tesseract/tesseract_training/tesseract_input/cropped");
    boost::filesystem::directory_iterator end;
    for (; it != end; ++it)
    {
        if (it->path().extension().wstring() != L".tif")
            continue;

        std::cout << it->path().string() << ", counter: " << counter++ << std::endl;

        const cv::Mat image = cv::imread(it->path().string());
        const auto result = RecognizeFile(it->path().string().c_str());
        for (const auto& res : result)
        {
            // need to make box file
            // %bin_path%\tesseract %CD%\%image% %arg% batch.nochop makebox
            std::string prefix = it->path().filename().string();
            boost::algorithm::erase_all(prefix, ".tif");
            const char* exeFile = "d:\\git\\data\\tesseract\\tesseract_training\\tesseract_bin\\tesseract.exe";
            const std::string cmdLine = (boost::format("%s %s %s batch.nochop makebox") 
                % exeFile 
                % boost::filesystem::system_complete(it->path()).string() 
                % prefix).str();

            CHECK(!system(cmdLine.c_str()));
            std::wstring data = ParseBoxFile(prefix + ".box");

            boost::algorithm::erase_all(data, L"\n");
            boost::algorithm::erase_all(data, L" ");

            std::wstring recognized = res.m_Text;

            boost::algorithm::erase_all(recognized, L"\n");
            boost::algorithm::erase_all(recognized, L" ");

            if (recognized != data)
                std::wcout << L"data mismatch, old: " << recognized << L", box: " << data << std::endl;

            boost::filesystem::rename(prefix + ".box", it->path().branch_path() / (prefix + ".box"));
        }
    }
}

TEST(Recognition, Recognize)
{
    static const boost::wregex playerRegexp(std::wstring(L"[\\s\\w]+\\n(\\d?,?\\d{2,})"));
    static const boost::wregex cardRegexp(std::wstring(L"(J|Q|K|A|\\d|10)\\n(♠|♣|♥|♦)"));
    static const boost::wregex moneyRegexp(std::wstring(L"(\\d?,?\\d{2,})"));
    static const boost::wregex textRegexp(std::wstring(L"(\\w{3,})"));

    unsigned counter = 0;

    boost::filesystem::directory_iterator it("../../../data/tesseract/tesseract_training/tesseract_input");
    boost::filesystem::directory_iterator end;
    for (; it != end; ++it)
    {
        if (it->path().extension().wstring() != L".tif")
            continue;

        std::cout << it->path().string() << ", counter: " << counter << std::endl;

        const cv::Mat image = cv::imread(it->path().string());
        const auto result = RecognizeFile(it->path().string().c_str());
        for (const auto& res : result)
        {
            if (boost::regex_match(res.m_Text, playerRegexp) || 
                boost::regex_match(res.m_Text, cardRegexp) || 
                boost::regex_match(res.m_Text, textRegexp) || 
                boost::regex_match(res.m_Text, moneyRegexp))
            {
                cv::Mat region = image(res.m_Rect);
                const std::string file = (boost::format("../../../data/tesseract/tesseract_training/tesseract_input/cropped/eng.arial.exp%s.tif") % counter++).str();

                cv::cvtColor(region, region, CV_BGR2GRAY);
                Check4Inversion(region);
                cv::threshold(region, region, 80, 255, CV_THRESH_BINARY);
                cv::imwrite(file, region);
            }
        }
    }
}
*/
TEST(Recognition, Capture)
{
    auto callback = [](HWND hwnd, LPARAM lParam) -> BOOL
    {
        static int counter = 0;
        if (GetWindowClass(hwnd) == POKERSTARS_TABLE_WINDOW_CLASS)
        {
            const cv::Mat result = reinterpret_cast<rec::IRecognizer*>(lParam)->CaptureWindow(
                reinterpret_cast<unsigned>(hwnd)
            );
            cv::imwrite((boost::format("../../../data/tesseract/tesseract_training/tesseract_input/eng.arial.exp%s.tif") % counter).str(), result);
            ++counter;
        }

        return TRUE;
    };

    if (!g_Recognizer)
        g_Recognizer = rec::IRecognizer::Instance();

    EnumWindows(callback, reinterpret_cast<LPARAM>(g_Recognizer.get()));
}

/*
TEST(Recognition, Simple)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/simple.png");
    ExpectFound(result, L"very simple test");
}*/
TEST(Recognition, Afk)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/afk.png");
    ExpectFound(result, L"Fold\nSittingOut");
}
TEST(Recognition, 90)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/90.png");
    for (const auto& r : result)
    {
        if (r.m_Text.find(L"90") != std::wstring::npos)
            return;
    }
    FAIL();
}
TEST(Recognition, 90_2)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/90-2.png");
    ExpectFound(result, L"90");
}
TEST(Recognition, 10bet)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/10bet.png");
    ExpectFound(result, L"10");
}

TEST(Recognition, 20)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/20.png");
    ExpectFound(result, L"20");
}

TEST(Recognition, 350)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/350.png");
    ExpectFound(result, L"350");
}

TEST(Recognition, Check)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/check.png");
    ExpectFound(result, L"Check");
}

TEST(Recognition, Bet350)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/bet350.png");
    ExpectFound(result, L"350");
}

TEST(Recognition, RaiseTo)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/raise.png");
    ExpectFound(result, L"RaiseTo\n4");
}

TEST(Recognition, Name5)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/name5.png");
    ExpectFound(result, L"dupontjac\n201");
}

TEST(Recognition, PSSimpleTest)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/ps_simple.png");
    ExpectFound(result, L"cfcwesty1981\n1,500");
}

TEST(Recognition, Name)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/name.png");
    ExpectFound(result, L"Holzwurm4\n1,110");
}

TEST(Recognition, PSCards5Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards6.png");
    ExpectFound(result, L"Q\n♦");
}

TEST(Recognition, Name3)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/name3.png");
    ExpectFound(result, L"R3DxXx\n1,470");
}

TEST(Recognition, PSPot3)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/pot3.png");
    ExpectFound(result, L"Potll1,094");
}

TEST(Recognition, Name4)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/name4.png");
    ExpectFound(result, L"Spreizhoaxn\n380");
}

TEST(Recognition, Name2)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/name2.png");
    ExpectFound(result, L"podolyak1986\n500");
}

TEST(Recognition, Ten)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/10.png");
    ExpectFound(result, L"10\n♣");
}

TEST(Recognition, WrongName)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/chris.png");
    ExpectFound(result, L"chris810\n1,480");
}

TEST(Recognition, 200)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/200.png");
    ExpectFound(result, L"200");
}

TEST(Recognition, PSCards4Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards5.png");
    ExpectFound(result, L"Q\n♦");
    ExpectFound(result, L"A\n♥");
    ExpectFound(result, L"J\n♥");
}

TEST(Recognition, Nine)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/9.png");
    ExpectFound(result, L"9\n♠");
}

TEST(Recognition, Jack)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/jack.png");
    ExpectFound(result, L"5\n♣");
    ExpectFound(result, L"J\n♣");
    ExpectFound(result, L"6\n♥");
}

TEST(Recognition, PSCards1Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards2.png");
    ExpectFound(result, L"9\n♠");
    ExpectFound(result, L"9\n♥");
    ExpectFound(result, L"K\n♣");
    ExpectFound(result, L"7\n♦");
    ExpectFound(result, L"A\n♥");
}
TEST(Recognition, PSCards2Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards3.png");
    ExpectFound(result, L"K\n♣");
    ExpectFound(result, L"8\n♣");
}
TEST(Recognition, PSCards3Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards4.png");
    ExpectFound(result, L"Q\n♣");
    ExpectFound(result, L"8\n♦");
}

TEST(Recognition, PSPlayersTest)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/players4.png");
    ExpectFound(result, L"Commackcid\n1,490");
}

TEST(Recognition, PSPlayers2Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/players5.png");
    ExpectFound(result, L"situ19\n1,500");
}

TEST(Recognition, PSCards7)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/cards7.png");
    ExpectFound(result, L"7\n♦");
    ExpectFound(result, L"4\n♣");
}

TEST(Recognition, PSPlayers3Test)
{
    const auto result = RecognizeFile("../../../data/tesseract/test_files/players6.png");
    ExpectFound(result, L"CLRN\n198");
}

TEST(Recognition, PSButton)
{
    if (!g_Recognizer)
        g_Recognizer = rec::IRecognizer::Instance();
    const cv::Mat image = cv::imread("../../../data/tesseract/test_files/full_table2.png");
    const cv::Mat object = cv::imread("../../../data/tesseract/test_files/dealer.png");

    const auto result = g_Recognizer->FindObject(image, object);
    static const auto etalon = cv::Rect(396, 535, 43, 40);
    EXPECT_EQ(result, etalon);
}