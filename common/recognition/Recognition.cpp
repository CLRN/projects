﻿#include "Recognition.h"
#include "exception/CheckHelpers.h"
#include "conversion/AnyCast.h"
#include "OCR.h"
#include "Contours.h"

#include <opencv2/core/core.hpp> // cv::Mat
#include <opencv2/highgui/highgui.hpp> // cv::imread()
#include <opencv2/imgproc/imgproc.hpp> // cv::Canny()
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <boost/make_shared.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/regex.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/noncopyable.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/scope_exit.hpp>
#include <boost/thread.hpp>

#ifdef _DEBUG
//#define VISUAL_DEBUG
#endif // _DEBUG

namespace rec
{

class RectangleRecognizer : boost::noncopyable, public IRecognizer
{
public:

    typedef IContours::List RectList;

    RectangleRecognizer()
    {
    }

    virtual cv::Mat CaptureWindow(const unsigned window, const cv::Rect& region) override
    {
        const HWND hwnd = reinterpret_cast<HWND>(window);

        int height, width, srcHeight, srcWidth;
        HBITMAP hbwindow;

        const HDC hwindowDC = GetDC(hwnd);
        const HDC hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);

        BOOST_SCOPE_EXIT(&hbwindow, &hwindowCompatibleDC, &hwnd, &hwindowDC)
        {
            DeleteObject(hbwindow); 
            DeleteDC(hwindowCompatibleDC); 
            ReleaseDC(hwnd, hwindowDC);
        }
        BOOST_SCOPE_EXIT_END;

        CHECK_LE(hwindowDC, cmn::Exception("Failed to get window dc"));
        CHECK(hwindowCompatibleDC, cmn::Exception("Failed to get window compatible dc"));

        SetStretchBltMode(hwindowCompatibleDC, COLORONCOLOR);  

        int sourceX = 0;
        int sourceY = 0;
        if (!region.height || !region.width)
        {
            RECT windowsize;    // get the height and width of the screen
            CHECK(GetWindowRect(hwnd, &windowsize), cmn::Exception("Failed to get window rect"));

            srcHeight = windowsize.bottom - windowsize.top;
            srcWidth = windowsize.right - windowsize.left;
        }
        else
        {
            srcHeight = region.height;
            srcWidth = region.width;
            sourceX = region.x;
            sourceY = region.y;
        }

        height = srcHeight;  //change this to whatever size you want to resize to
        width = srcWidth;


        // create a bitmap
        BITMAPINFOHEADER  bi = {};
        hbwindow = CreateCompatibleBitmap(hwindowDC, width, height);
        CHECK_LE(hbwindow, cmn::Exception("Failed to CreateCompatibleBitmap"));
        bi.biSize = sizeof(BITMAPINFOHEADER); 
        bi.biWidth = width;    
        bi.biHeight = -height;  //this is the line that makes it draw upside down or not
        bi.biPlanes = 1;    
        bi.biBitCount = 32;    
        bi.biCompression = BI_RGB;    

        // use the previously created device context with the bitmap
        SelectObject(hwindowCompatibleDC, hbwindow);
        // copy from the window device context to the bitmap device context
        CHECK_LE(StretchBlt(hwindowCompatibleDC, 0, 0, width, height, hwindowDC, sourceX, sourceY, srcWidth, srcHeight, SRCCOPY), cmn::Exception("Failed to StretchBlt")); //change SRCCOPY to NOTSRCCOPY for wacky colors !

        cv::Mat result;
        result.create(height, width, CV_8UC4);
        GetDIBits(hwindowCompatibleDC, hbwindow, 0, height, result.data, (BITMAPINFO *)&bi, DIB_RGB_COLORS);  //copy from hwindowCompatibleDC to hbwindow

        return result;
    }

    virtual Result::List Recognize(const cv::Mat& image) override
    {
        m_Source = image;

#ifdef VISUAL_DEBUG
        cv::RNG rng(12345);
        cv::Mat display = m_Source.clone();
#endif // VISUAL_DEBUG

        const auto contours = FindAllContours();
        Result::List result;

        const auto processText = [&](const std::wstring& text, const cv::Rect& rectangle)
        {
            if (text.empty())
                return;

            Result data;
            data.m_Rect = rectangle;
            data.m_Text = text;

            if (boost::find(result, data) == result.end())
            {
                result.push_back(data);
#ifdef VISUAL_DEBUG
                cv::Scalar color = cv::Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
                cv::rectangle(display, rectangle, color, 1);
                cv::putText(display, conv::cast<std::string>(text), rectangle.tl(), CV_FONT_HERSHEY_PLAIN, 1, CV_RGB(0, 0, 250), 1);
#endif // VISUAL_DEBUG
            }
        };

        for (const auto& rectangle : contours)
        {
            // recognize text
            const std::wstring text = RecognizeText(m_Source(rectangle));
            processText(text, rectangle);       
        }
#ifdef VISUAL_DEBUG
        cv::namedWindow("output");
        cv::imshow("output", display);
        cv::waitKey(100);
#endif // VISUAL_DEBUG
        return result;
    }

    virtual std::wstring RecognizeText(const cv::Mat& image) override
    {
        return GetApi().Recognize(image);
    }

private:

    void Check4Inversion(cv::Mat& src)
    {
        cv::Mat temp;
        cv::threshold(src, temp, 255 / 2, 255, cv::THRESH_BINARY);

        const int nonZeroElements = cv::countNonZero(temp);
        const int totalElements = temp.size().height * temp.size().width;

        // if non zero pixels more then zero pixels(white space more than black space)
        // we should not invert image
        if (nonZeroElements <= (totalElements - nonZeroElements))
            cv::bitwise_not(src, src);
    }

    RectList FindAllContours()
    {
        const auto processor = IContours::Instance(m_Source);
        RectList result = processor->Find();
        processor->Group(result, IContours::GroupingType::Words);

        boost::for_each(result, boost::bind(&RectangleRecognizer::AdjustRectangle, this, _1));

        // append all image region
        if (result.empty())
            result.push_back(cv::Rect(0, 0, m_Source.size().width, m_Source.size().height));

        boost::reverse(result);
        return result;
    }

    void AdjustRectangle(cv::Rect& rect)
    {
        rect.x -= 1;
        rect.width += 2;
        rect.y -= 2;
        rect.height += 5;
        if (rect.x < 0)
            rect.x = 0;
        if (rect.y < 0)
            rect.y = 0;

        {
            const int diff = rect.br().x - (m_Source.size().width - 1);
            if (diff > 0)
                rect.width -= diff;
        }
        {
            const int diff = rect.br().y - (m_Source.size().height - 1);
            if (diff > 0)
                rect.height -= diff;
        }
    }

    virtual cv::Rect FindObject(const cv::Mat& img, const cv::Mat& templ) override
    {
        assert(img.size().height && img.size().width);
        assert(templ.size().height && templ.size().width);

        /// Create the result matrix
        int result_cols =  img.cols - templ.cols + 1;
        int result_rows = img.rows - templ.rows + 1;

        cv::Mat result(result_cols, result_rows, CV_32FC1);

        cv::Mat source = img;
        if (source.channels() == 4)
            cv::cvtColor(source, source, cv::COLOR_BGRA2BGR);

        /// Do the Matching and Normalize
        cv::matchTemplate(source, templ, result, cv::TM_CCOEFF);
        cv::normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

        cv::Point maxLoc;
        cv::minMaxLoc(result, NULL, NULL, NULL, &maxLoc);

        const cv::Rect rect(maxLoc.x, maxLoc.y, templ.cols, templ.rows);

        const cv::Mat found = source(rect);
        cv::Mat diff = found - templ;
        cv::cvtColor(diff, diff, cv::COLOR_BGR2GRAY);
        cv::threshold(diff, diff, 80, 255, cv::THRESH_OTSU);
        const int nonZero = cv::countNonZero(diff);
        const int totalElements = diff.size().height * diff.size().width;
        const double error = double(nonZero) / totalElements;
        if (error > 0.18)
            return cv::Rect();

#ifdef VISUAL_DEBUG
        cv::Mat drawing = img.clone();
        cv::rectangle(drawing, rect, cv::Scalar(0, 255, 0), 2);
#endif // VISUAL_DEBUG
        return rect;
    }

    IOCR& GetApi()
    {
        boost::unique_lock<boost::mutex> lock(m_Mutex);
        auto it = m_Api.find(boost::this_thread::get_id());
        if (it == m_Api.end())
        {
            const auto api = IOCR::Instance("../../../data/ocr/storage/");
            api->Train();
            it = m_Api.insert(std::make_pair(boost::this_thread::get_id(), api)).first;
            lock.unlock();
        }
        return *it->second;
    }

    virtual void Draw(const unsigned window, const cv::Point& point, const std::string& text)
    {
        HWND hDesktopWindow = reinterpret_cast<HWND>(window);
        HDC hdcDesktop = GetDC(hDesktopWindow);
        TextOutA(hdcDesktop, point.x, point.y, text.c_str(), 12);
        ReleaseDC(hDesktopWindow, hdcDesktop);
    }

private:
    typedef std::map<boost::thread::id, IOCR::Ptr> ApiPool;

    cv::Mat m_Source;
    ApiPool m_Api;
    boost::mutex m_Mutex;
};

IRecognizer::Ptr IRecognizer::Instance()
{
    return boost::make_shared<RectangleRecognizer>();
}


bool IRecognizer::Result::operator == (const Result& another) const
{
    const auto isContains = [this](const Result& another)
    {
        if (!m_Rect.contains(another.m_Rect.tl()) || !m_Rect.contains(another.m_Rect.tl()))
            return false;

        // ok, outer rectangle included in this rectangle, compare line by line
        std::vector<std::wstring> thisText;
        std::vector<std::wstring> otherText;
        {
            boost::algorithm::split(thisText, m_Text, boost::algorithm::is_any_of(L"\n"));
            boost::algorithm::split(otherText, another.m_Text, boost::algorithm::is_any_of(L"\n"));
        }

        if (thisText.size() != otherText.size())
            return false;

        for (std::size_t i = 0 ; i < thisText.size(); ++i)
        {
            // if this rectangle line not contains another rectangle line
            // this not contains other
            if (thisText[i].find(otherText[i]) == std::wstring::npos)
                return false;
        }
        return true;
    };

    return 
        abs(m_Rect.x - another.m_Rect.x) < 5 &&
        abs(m_Rect.y - another.m_Rect.y) < 5 &&
        abs(m_Rect.width - another.m_Rect.width) < 5 &&
        abs(m_Rect.height - another.m_Rect.height) < 5 &&
        m_Text == another.m_Text && 
        isContains(another);
}

} // namespace rec

