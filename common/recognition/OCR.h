#ifndef OCR_h__
#define OCR_h__

#include <string>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace rec
{

class IOCR
{
public:
    typedef boost::shared_ptr<IOCR> Ptr;

    virtual ~IOCR() {}
    virtual void Disassemble(const cv::Mat& image) = 0;
    virtual void Assemble() = 0;
    virtual void Train() = 0;
    virtual std::wstring RecognizeSign(const cv::Mat& image) = 0;
    virtual std::wstring Recognize(const cv::Mat& image) = 0;

    static Ptr Instance(const std::string& storage);
};

} // namespace rec


#endif // OCR_h__
