#include "RecognizableArea.h"
#include "serialization/Serialization.h"
#include "serialization/Helpers.h"
#include "common/ServiceHolder.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

namespace rec
{

class SerializableRect : public cv::Rect, public cmn::srl::Serializable
{
public:
    SerializableRect(const cv::Rect& r = cv::Rect())
        : SERIALIZABLE("rectangle", 
            FIELD(x) 
            FIELD(y)
            FIELD(width)
            FIELD(height)
        )
        , cv::Rect(r)
    {
    }
};

#pragma warning(push)
#pragma warning(disable:4250) // warning C4250: inherits via dominance

class RecognizableArea final
    : public IRecognizableArea
    , boost::noncopyable
    , public boost::enable_shared_from_this<RecognizableArea>
    , public cmn::srl::Serializable
{
public:

    RecognizableArea(const cv::Rect& rect) 
        : SERIALIZABLE("area",
            FIELD(m_Rect)
        )
        , m_Rect(rect)
    {
    }

   
    virtual const cv::Rect& GetArea() const override
    {
        return m_Rect;
    }

    virtual void SetArea(const cv::Rect& rect) override
    {
        m_Rect = rect;
    }

    bool IsEqual(const cv::Rect& area) const
    {
        return 
            abs(m_Rect.x - area.x) < std::min(area.width, m_Rect.width) &&
            abs(m_Rect.y - area.y) < std::min(area.height, m_Rect.height) / 2 &&
            abs(m_Rect.width - area.width) < (area.width + m_Rect.width) / 2 &&
            abs(m_Rect.height - area.height) < std::min(area.height, m_Rect.height) &&
            (m_Rect.contains(cv::Point(area.x + area.width / 2, area.y + area.height / 2)) ||
            area.contains(cv::Point(m_Rect.x + m_Rect.width / 2, m_Rect.y + m_Rect.height / 2)));
    }

private:
    SerializableRect m_Rect;
};

#pragma warning(pop)

IRecognizableArea::Ptr IRecognizableArea::Instance(const cv::Rect& rect)
{
    return boost::make_shared<RecognizableArea>(rect);
}

} // namespace rec
