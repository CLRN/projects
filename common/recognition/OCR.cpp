#include "OCR.h"
#include "exception/CheckHelpers.h"
#include "conversion/AnyCast.h"
#include "common/FileSystem.h"
#include "recognition/Contours.h"
#include "serialization/Serialization.h"
#include "serialization/Helpers.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml.hpp>

#include <boost/make_shared.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/function.hpp>

#ifdef _DEBUG
#define VISUAL_DEBUG
#endif // _DEBUG

namespace rec
{

class OpenCVOCR : public IOCR, public cmn::srl::Serializable
{
public:
    typedef std::list<cv::Rect> RectList;
    typedef boost::function<void(cv::Mat&, cv::Mat&, const boost::filesystem::path&, unsigned)> DirectoryFunctor;

    OpenCVOCR(const std::string& storage)
        : SERIALIZABLE("ocr", FIELD(m_Symbols))
        , m_Storage(boost::filesystem::system_complete(storage))
        , m_Knn(cv::ml::KNearest::create())
    {
        boost::filesystem::create_directories(m_Storage);
    }

    unsigned CountFiles(const boost::filesystem::path& dir)
    {
        assert(boost::filesystem::is_directory(dir));
        boost::filesystem::directory_iterator it(dir);
        const boost::filesystem::directory_iterator end;

        return std::count_if(it, end, 
            boost::bind(&boost::filesystem::path::extension, boost::bind(&boost::filesystem::directory_entry::path, _1)) == L".png");
    }

    virtual void Disassemble(const cv::Mat& image) override
    {
        cv::Mat thr, gray, con;
        cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);
        cv::threshold(gray, thr, 200, 255, cv::THRESH_BINARY_INV); //Threshold to recognize

        IContours::List contours;
        {
            const auto processor = IContours::Instance(image);
            contours = processor->Find();
            processor->Group(contours, IContours::GroupingType::Symbols);
        }

        for (const cv::Rect& r : contours)
        {
#ifdef VISUAL_DEBUG
            cv::Mat display = image.clone();
            cv::rectangle(display, r, cv::Scalar(0, 255, 0));
#endif // VISUAL_DEBUG

            const cv::Mat input = image(cv::Rect(r.x - 1, r.y - 1, r.width + 2, r.height + 2));
            const std::wstring text = Recognize(input);

            // save image to storage
            boost::filesystem::path path;
            if (text.empty())
            {
                path = m_Storage / "unrecognized";
                boost::filesystem::create_directories(path);
            }
            else
            {
                const unsigned symbolId = static_cast<unsigned>(text.front());
                path = GetSymbolPath(symbolId, text);
            }
            
            const cv::Mat roi = thr(r); //Crop the image
            SaveSymbol(roi, path);
        }
    }

    boost::filesystem::path GetSymbolPath(const unsigned symbolId, const std::wstring& text)
    {
        std::wostringstream woss;
        woss << symbolId << L" (" << text << L")";
        boost::filesystem::path path = m_Storage / woss.str();
        boost::system::error_code e;
        boost::filesystem::create_directories(path, e);

        if (e)
        {
            path = m_Storage / conv::cast<std::string>(symbolId);
            boost::filesystem::create_directories(path);
        } 
        return path;
    }

    void SaveSymbol(const cv::Mat& roi, boost::filesystem::path path)
    {
        cv::Mat tmp1;
        cv::resize(roi, tmp1, cv::Size(10, 10), 0, 0, cv::INTER_LINEAR); //resize to 10X10

        const auto count = CountFiles(path);
        path /= (conv::cast<std::string>(count + 1) + ".png");

        fs::ScopedFile file(".png");
        const std::wstring name = file;

        const bool result = cv::imwrite(conv::cast<std::string>(name), tmp1);
        assert(result);

        boost::filesystem::rename(name, path);
    }

    cv::Mat ReadImageFromUnicodePath(const boost::filesystem::path& path)
    {
        fs::ScopedFile file(".png");
        const std::wstring tempFile = file;
        boost::filesystem::copy(path, tempFile);

        cv::Mat result = cv::imread(conv::cast<std::string>(tempFile), cv::IMREAD_GRAYSCALE);
        if (result.size().height != 10 || result.size().width != 10)
            cv::resize(result, result, cv::Size(10, 10), 0, 0, cv::INTER_LINEAR); //resize to 10X10

        return result;
    }

    void TrainOnFile(cv::Mat& sample, cv::Mat& response, const boost::filesystem::path& path, unsigned id)
    {
        const cv::Mat src = ReadImageFromUnicodePath(path);
        cv::Mat tmp;
        src.convertTo(tmp, CV_32FC1); //convert to float
        sample.push_back(tmp.reshape(1, 1)); // Store  sample data
        response.push_back(int(id)); // Store label to a mat
    }

    void ProcessDirectory(cv::Mat& sample, cv::Mat& response, const boost::filesystem::path& path, const DirectoryFunctor& func)
    {
        std::wstring name = path.filename().wstring();
        std::wstring text;
        const auto index = name.find(' ');
        if (index != std::wstring::npos)
        {
            text.assign(name.substr(index + 2, name.size() - index - 3));
            name.resize(index);
        }

        const unsigned id = conv::cast<unsigned>(name);

        if (text.size() > 1)
            m_Symbols[id] = text;

        boost::filesystem::directory_iterator it(path);
        const boost::filesystem::directory_iterator end;

        std::vector<boost::filesystem::path> files;
        for (; it != end; ++it)
        {
            if (it->path().extension() == L".png")
                files.push_back(it->path());
        }

        boost::for_each(files, boost::bind(func, boost::ref(sample), boost::ref(response), _1, id));
    }

    virtual void Assemble() override
    {
        boost::filesystem::directory_iterator it(m_Storage);
        const boost::filesystem::directory_iterator end;
        std::vector<boost::filesystem::directory_entry> entries;
        boost::copy(boost::make_iterator_range(it, end), std::back_inserter(entries));

        const auto range = entries | boost::adaptors::filtered([](const boost::filesystem::directory_entry& e){
            return boost::filesystem::is_directory(e.path()) && 
                boost::algorithm::is_digit()(e.path().filename().string().front());
        });

        cv::Mat sample;
        cv::Mat responseArray;  
        const DirectoryFunctor func = boost::bind(&OpenCVOCR::TrainOnFile, this, _1, _2, _3, _4);
        boost::for_each(range, boost::bind(&OpenCVOCR::ProcessDirectory, this, boost::ref(sample), boost::ref(responseArray), _1, func));

        // Store the data to file
        cv::Mat tmp;
        cv::Mat response;
        tmp = responseArray.reshape(1, 1); // make continuous
        tmp.convertTo(response, CV_32FC1); // Convert  to float

        {
            const boost::filesystem::path trainData = m_Storage / "train_data.yml";
            cv::FileStorage data(trainData.string(), cv::FileStorage::WRITE); // Store the sample data in a file
            data << "data" << sample;
            data << "label" << response;

            std::string xmlMapping;
            cmn::xml::Node node;
            Serialize(node);
            node.Save(xmlMapping);

            data << "mapping" << xmlMapping;
        }
    }

    virtual void Train() override
    {
        cv::Mat sample;
        cv::Mat response;  
        std::string xmlMapping;

        // Read stored sample and label for training
        {
            const boost::filesystem::path trainData = m_Storage / "train_data.yml";
            cv::FileStorage data(trainData.string(), cv::FileStorage::READ); 
            data["data"] >> sample;
            data["label"] >> response;
            data["mapping"] >> xmlMapping;
        }

        m_Knn->train(sample, cv::ml::ROW_SAMPLE, response); // Train with sample and responses

        cmn::xml::Node node(xmlMapping.c_str(), xmlMapping.size());
        Deserialize(node); // deserialize mapping data

#ifdef _DEBUG
        Validate();
#endif // _DEBUG
    }

    virtual std::wstring RecognizeSign(const cv::Mat& image) override
    {
        cv::Mat tmp1, tmp2;
        assert(image.size().area());
        cv::resize(image, tmp1, cv::Size(10, 10), 0, 0, cv::INTER_LINEAR); //resize to 10X10
        tmp1.convertTo(tmp2, CV_32FC1); //convert to float

        cv::Mat results;
        cv::Mat neighbors;
        cv::Mat responses;
        const float p = m_Knn->findNearest(tmp2.reshape(1, 1), 1, results, neighbors, responses);
        const unsigned code = static_cast<unsigned>(p);

        const auto it = m_Symbols.find(code);
        return it != m_Symbols.end() ? it->second : std::wstring(1, static_cast<wchar_t>(code));
    }

    virtual std::wstring Recognize(const cv::Mat& image) override
    {
        cv::Mat thr, gray, con;
        cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

        IContours::List contours;
        IContours::List lines;
        {
            const auto processor = IContours::Instance(image);
            contours = processor->Find();
            processor->Group(contours, IContours::GroupingType::Symbols);

            lines = contours;
            processor->Group(lines, IContours::GroupingType::Lines);

            cv::threshold(gray, thr, 200, 255, cv::THRESH_BINARY); //Threshold to recognize
            processor->Check4Inversion(thr);
        }

        boost::sort(contours, boost::bind(&cv::Rect::x, _1) < boost::bind(&cv::Rect::x, _2));
        boost::sort(lines, boost::bind(&cv::Rect::y, _1) < boost::bind(&cv::Rect::y, _2));

        std::vector<std::wstring> linesData(lines.size());

        for (const cv::Rect& r : contours)
        {
            if (r.width > r.height * 4)
                continue; // this is not a symbol

            // find out line
            const auto it = boost::find_if(lines, boost::bind(&cv::Rect::contains, _1, cv::Point(r.x + r.width / 2, r.y + r.height / 2)));
            if (it == lines.end())
                continue; // skipping rectangle, because it's not in any line

            std::wstring& lineData = linesData[std::distance(lines.begin(), it)];

#ifdef VISUAL_DEBUG
            cv::Mat display = image.clone();
            cv::rectangle(display, r, cv::Scalar(0, 255, 0));
#endif // VISUAL_DEBUG

            const cv::Mat roi = thr(r); //Crop the image
            const std::wstring recognized = cv::countNonZero(roi) && roi.size().area() < 50 * 50 ? RecognizeSign(roi) : std::wstring();
            lineData.append(recognized);

            // save symbol
            bool need2Save = false;
            if (need2Save) 
            {
                std::wstring tesseractText;
                const unsigned symbolId = static_cast<unsigned>(tesseractText.front());
                const auto path = GetSymbolPath(symbolId, tesseractText);
                SaveSymbol(roi, path);
            }
        }

        // make result
        std::wstring result;
        for (std::size_t i = 0; i < linesData.size(); ++i)
        {
            if (linesData[i].empty())
                continue;

            boost::copy(linesData[i], std::back_inserter(result));
            if (i != linesData.size() - 1)
                result.push_back(L'\n');
        }
        return result;
    }

    void ValidateFile(cv::Mat&, cv::Mat&, const boost::filesystem::path& path, unsigned id)
    {
        const cv::Mat src = ReadImageFromUnicodePath(path);
        const std::wstring text = RecognizeSign(src);
        if (text.size() == 1)
        {
            CHECK(static_cast<unsigned>(text.front()) == id, cmn::Exception("Recognition validation failed"), text, id);
        }
        else
        {
            CHECK(text == m_Symbols[id], cmn::Exception("Recognition validation failed"), text, id);
        }
    }

    void Validate()
    {
        boost::filesystem::directory_iterator it(m_Storage);
        const boost::filesystem::directory_iterator end;

        std::vector<boost::filesystem::directory_entry> entries;
        boost::copy(boost::make_iterator_range(it, end), std::back_inserter(entries));

        const auto range = entries | boost::adaptors::filtered([](const boost::filesystem::directory_entry& e){
            return boost::filesystem::is_directory(e.path()) && 
                boost::algorithm::is_digit()(e.path().filename().string().front());
        });

        cv::Mat sample;
        cv::Mat response;  
        const DirectoryFunctor func = boost::bind(&OpenCVOCR::ValidateFile, this, _1, _2, _3, _4);
        boost::for_each(range, boost::bind(&OpenCVOCR::ProcessDirectory, this, boost::ref(sample), boost::ref(response), _1, func));
    }

private:
    boost::filesystem::path m_Storage;
    cv::Ptr<cv::ml::KNearest> m_Knn;
    std::map<unsigned, std::wstring> m_Symbols;
};


IOCR::Ptr IOCR::Instance(const std::string& storage)
{
    return boost::make_shared<OpenCVOCR>(storage);
}

} // namespace rec