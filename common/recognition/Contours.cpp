#include "Contours.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>

#ifdef _DEBUG
#define VISUAL_DEBUG
#endif // _DEBUG

namespace rec
{

class ContoursProcessor : public IContours
{
public:

    ContoursProcessor(const cv::Mat& source) 
        : m_Source(source)
#ifdef VISUAL_DEBUG
        , m_IsDebugging()
#endif // _DEBUG
    {

    }

    static bool IsContains(const cv::Rect& rect, const cv::Point& p)
    {
        return p.x >= rect.x && p.x <= rect.br().x && p.y >= rect.y && p.y <= rect.br().y;
    }

    static bool IsEqual(const cv::Rect& lhs, const cv::Rect& rhs)
    {
        return 
            abs(lhs.x - rhs.x) < std::min(rhs.width, lhs.width) / 2 &&
            abs(lhs.y - rhs.y) < std::min(rhs.height, lhs.height) / 2 &&
            abs(lhs.width - rhs.width) < (rhs.width + lhs.width) / 2 &&
            abs(lhs.height - rhs.height) < std::min(rhs.height, lhs.height);
    }

    virtual void Check4Inversion(cv::Mat& src) override
    {
        cv::Mat temp;
        cv::threshold(src, temp, 255 / 2, 255, cv::THRESH_BINARY);

        const int nonZeroElements = cv::countNonZero(temp);
        const int totalElements = temp.size().height * temp.size().width;

        // if non zero pixels more then zero pixels(white space more than black space)
        // we should not invert image
        if (nonZeroElements > (totalElements - nonZeroElements))
            cv::bitwise_not(src, src);
    }

    void RemoveInternalContours(List& src)
    {
        auto it = src.begin();
        auto end = src.end();
        for (; it != end; )
        {
            // find all child rectangles(child - internal rectangle)
            const auto childrens = src | boost::adaptors::filtered([&it](const cv::Rect& child){
                return *it != child && IsContains(*it, child.tl()) && IsContains(*it, child.br());
            });

            DebugDraw(childrens);

            const auto count = boost::distance(childrens);
            if (count && count < 3)
            {
                // it's mean that one rectangle contains another smaller one
                // this is typical situations for symbols like '6', '9', '0', etc...
                // so we need to remove internal rectangles
                DebugDraw(src);

                // copy all childrens to temp vector, because each erase will invalidate iterators
                List temp;
                temp.reserve(count);
                boost::copy(childrens, std::back_inserter(temp));
                
                boost::for_each(temp, [&src](const cv::Rect& r){
                    src.erase(boost::find(src, r));
                });
                it = src.begin();
                end = src.end();

                DebugDraw(src);
            }
            else
            {
                ++it;
            }
        }
    }

    virtual List Find() override
    {
        cv::Mat con;
        if (m_Source.channels() > 1)
            cv::cvtColor(m_Source, con, cv::COLOR_BGR2GRAY);
        else
            con = m_Source;

        Check4Inversion(con);
        cv::threshold(con, con, 160, 255, cv::THRESH_BINARY); //Threshold to find contour
        cv::threshold(con, con, 200, 255, cv::THRESH_OTSU); //Threshold to find contour

        // Create sample and label data
        std::vector< std::vector <cv::Point> > contours; // Vector for storing contour
        std::vector< cv::Vec4i > hierarchy;

        cv::findContours(con, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE); //Find contour

        // make rectangles
        List temp;
        {
            temp.reserve(contours.size());
            boost::transform(contours, std::back_inserter(temp), boost::bind(&cv::boundingRect, _1));
            DebugDraw(temp);
        }

        // make unique
        List result;
        {
            const auto range = boost::unique(temp, boost::bind(&ContoursProcessor::IsEqual, _1, _2));
            result.reserve(contours.size());
            boost::copy(range, std::back_inserter(result));
            DebugDraw(result);
        }

        RemoveInternalContours(result);
        DebugDraw(result);

        // if we have more than rectangle remove rectangle which coordinates equal to image size
        if (result.size() > 1)
        {
            const cv::Rect full(0, 0, m_Source.size().width, m_Source.size().height);
            const auto it = boost::remove_if(result, 
                boost::bind(&ContoursProcessor::IsEqual, _1, full) &&       // similar proportions
                boost::bind(&cv::Rect::area, _1) > full.area() * 2 / 3      // smaller size
            );
            result.erase(it, result.end());
        }

        return result;
    }

    bool VerticalSymbolPartsGrouper(const cv::Rect& current, const cv::Rect& r)
    {
        if (current == r)
            return false;

#ifdef VISUAL_DEBUG
        if (m_IsDebugging)
        {
            cv::Mat display = m_Source.clone();
            cv::rectangle(display, current, cv::Scalar(0, 0, 255), 1);
            cv::rectangle(display, r, cv::Scalar(0, 255, 0), 1);
        }
#endif // VISUAL_DEBUG

        const cv::Point centerLeft(current.x + current.width / 2, current.y + current.height / 2);
        const cv::Point centerRight(r.x + r.width / 2, r.y + r.height / 2);

        // compare centers and heights
        bool centersEqualByX = centerLeft.x == centerRight.x;
        if (!centersEqualByX)
        {
            if (r.area() > current.area())
                centersEqualByX = r.height > current.height && IsContains(r, cv::Point(current.x, r.y)) && IsContains(r, cv::Point(current.br().x, r.y)); 
            else
                centersEqualByX = current.height > r.height && IsContains(current, cv::Point(r.x, current.y)) && IsContains(current, cv::Point(r.br().x, current.y));
        }

        bool isHigher = false;
        bool centersEqualByY = false;
        const double koeff = 1.0;
        if (centerLeft.y > centerRight.y)
        {
            isHigher = current.y >= r.br().y;
            const auto diff = abs(r.br().y - current.y);
            centersEqualByY = diff < 10 && diff <= koeff * std::min(current.height, r.height);
        }
        else
        {
            isHigher = current.br().y <= r.y;
            const auto diff = abs(current.br().y - r.y);
            centersEqualByY = diff < 10 && diff <= koeff * std::min(current.height, r.height);
        }

        return centersEqualByX && centersEqualByY && isHigher;
    };

    bool AreasHorisontalGrouper(const cv::Rect& current, const cv::Rect& r)
    {
        if (current == r)
            return false;

#ifdef VISUAL_DEBUG
        if (m_IsDebugging)
        {
            cv::Mat display = m_Source.clone();
            cv::rectangle(display, current, cv::Scalar(0, 0, 255), 1);
            cv::rectangle(display, r, cv::Scalar(0, 255, 0), 1);
        }
#endif // VISUAL_DEBUG

        // calculate left and right points for current rectangle
        const cv::Point centerLeft(current.tl().x + current.width / 2, current.y + current.height / 2);
        const cv::Point centerRight(r.x + r.width / 2, r.y + r.height / 2);

        const double heightDiff = abs(double(r.height) / current.height - 1.0);
        const bool centersEqualByY = abs(centerLeft.y - centerRight.y) <= double(0.29) * std::min(current.height, r.height);

        const bool isBottom = abs(current.y - r.br().y) <= current.height;
        const bool isComma = current.area() > 20 && current.area() <= 40 && current.height > current.width;
        const bool isDot = current.area() < 20;
        const bool isPunctuation = isBottom && current.y > r.y + r.height / 2 && (isComma || isDot);
        const bool isUnderline = isBottom && current.width / current.height > 2 && current.height <= 5;

        const double distanceByX = abs(centerLeft.x > centerRight.x ? current.x - r.br().x : r.x - current.br().x);
        const bool isNear = distanceByX < (isPunctuation ? double(2) : double(0.84)) * std::min(current.width, r.width);
        const bool isCurrentBefore = current.br().x <= r.x;

        return 
            (centersEqualByY && heightDiff < 0.42 || isCurrentBefore && (isPunctuation || isUnderline)) &&    // objects on the same line or it is punctuation
            (isNear || r.contains(centerLeft));                                             // object is near or contains part of each other
    };

    bool AreasVerticalGrouper(const cv::Rect& current, const cv::Rect& r)
    {
        if (current == r)
            return false;

        const cv::Point centerLeft(current.x + current.width / 2, current.y + current.height / 2);
        const cv::Point centerRight(r.x + r.width / 2, r.y + r.height / 2);

        const cv::Point overlappedTop(centerRight.x, current.y);
        const cv::Point overlappedBottom(centerRight.x, current.br().y);

        // compare centers and heights
        const bool centersEqualByX = abs(centerLeft.x - centerRight.x) < double(0.15) * std::min(current.width, r.width);
        const bool centersEqualByY = abs(centerLeft.y - centerRight.y) < double(2.5) * std::min(current.height, r.height);
        const double heightDiff = abs(double(r.height) / current.height - 1.0);
        return heightDiff < 5 && centersEqualByX && (centersEqualByY || (r.contains(overlappedTop) || r.contains(overlappedBottom)));
    };

    template<typename T>
    void DebugDraw(const T& data)
    {
#ifdef VISUAL_DEBUG
        m_Display = m_Source.clone();
        for (const auto& rect : data)
        {
            cv::Scalar color = cv::Scalar(255, 0, 0);
            cv::rectangle(m_Display, rect, color, 1);
        }
#endif // VISUAL_DEBUG    
    }

    bool LinesHorisontalGrouper(const cv::Rect& current, const cv::Rect& r)
    {
        if (current == r)
            return false;

#ifdef VISUAL_DEBUG
        if (m_IsDebugging)
        {
            cv::Mat display = m_Source.clone();
            cv::rectangle(display, current, cv::Scalar(0, 0, 255), 1);
            cv::rectangle(display, r, cv::Scalar(0, 255, 0), 1);
        }
#endif // VISUAL_DEBUG

        // calculate left and right points for current rectangle
        const cv::Point centerLeft(current.tl().x + current.width / 2, current.y + current.height / 2);
        const cv::Point centerRight(r.x + r.width / 2, r.y + r.height / 2);
        const bool centersEqualByY = abs(centerLeft.y - centerRight.y) <= double(0.3) * std::min(current.height, r.height);
        const bool isPartOfEachOther = current.contains(r.tl()) || r.contains(current.tl());
        return centersEqualByY || isPartOfEachOther;
    };

    List GroupRectangles(const List& input, const boost::function<bool (const cv::Rect& current, const cv::Rect& other)>& functor, bool eraseNonGrouped = false)
    {
#ifdef VISUAL_DEBUG
        cv::Mat display = m_Source.clone();
#endif // VISUAL_DEBUG

        class RectWithWeight : public cv::Rect
        {
        public:
            RectWithWeight(const cv::Rect& r) : cv::Rect(r), m_Counter() {}
            RectWithWeight() : cv::Rect(), m_Counter() {}
            unsigned m_Counter;
        };

        std::list<RectWithWeight> result;
        boost::copy(input, std::back_inserter(result));
        result.sort(boost::bind(&RectWithWeight::x, _1) > boost::bind(&RectWithWeight::x, _2));

        auto it = result.begin();
        const auto end = result.end();
        for (; it != end; )
        {
            // assuming that distance between rectangles equal to half width of the rectangle
            RectWithWeight& current = *it;

#ifdef VISUAL_DEBUG
            cv::rectangle(display, current, cv::Scalar::all(0));
#endif // VISUAL_DEBUG

            // find rectangles which contains one of this points
            std::list<RectWithWeight> existingList;
            const auto filtered = result | boost::adaptors::filtered(boost::bind(functor, boost::ref(current), _1));
            boost::copy(filtered, std::back_inserter(existingList));

            // sort existing by weight
            existingList.sort(boost::bind(&RectWithWeight::m_Counter, _1) > boost::bind(&RectWithWeight::m_Counter, _2));
            if (!existingList.empty())
            {
                const auto existing = boost::find(result, existingList.front());
#ifdef VISUAL_DEBUG
                cv::rectangle(display, *existing, cv::Scalar(0, 255, 0));
#endif // VISUAL_DEBUG

                // found, extend existing group to new one
                ++existing->m_Counter;
                if (existing->x > current.x)
                {
                    existing->width += existing->x - current.x;
                    existing->x = current.x;
                }
                if (existing->y > current.y)
                {
                    existing->height += existing->y - current.y;
                    existing->y = current.y;
                }
                if (existing->br().x < current.br().x)
                    existing->width = current.br().x - existing->x;
                if (existing->br().y < current.br().y)
                    existing->height = current.br().y - existing->y;

#ifdef VISUAL_DEBUG
                cv::rectangle(display, *existing, cv::Scalar(255, 0, 0));
#endif // VISUAL_DEBUG

                it = result.erase(it);
            }
            else
            {
                ++it;
            }
        }

        if (eraseNonGrouped)
        {
            result.remove_if([](const RectWithWeight& r){
                return !r.m_Counter && r.area() < 40; // remove small non grouped objects
            });
        }

        List temp;
        boost::copy(result, std::back_inserter(temp));
        return temp;
    }

    virtual void Group(List& data, GroupingType::Value type)
    {
        DebugDraw(data);

        switch (type)
        {
        case rec::IContours::GroupingType::Words:
            data = GroupRectangles(data, boost::bind(&ContoursProcessor::AreasHorisontalGrouper, this, _1, _2));
            DebugDraw(data);
            data = GroupRectangles(data, boost::bind(&ContoursProcessor::AreasVerticalGrouper, this, _1, _2));
            break;
        case rec::IContours::GroupingType::Symbols:
            if (data.size() > 2)
                data = GroupRectangles(data, boost::bind(&ContoursProcessor::VerticalSymbolPartsGrouper, this, _1, _2));
            break;
        case rec::IContours::GroupingType::Lines:
            data = GroupRectangles(data, boost::bind(&ContoursProcessor::LinesHorisontalGrouper, this, _1, _2), true);
            break;
        default:
            break;
        }

        DebugDraw(data);
    }

private:
    cv::Mat m_Source;
#ifdef VISUAL_DEBUG
    cv::Mat m_Display;
    bool m_IsDebugging;
#endif // VISUAL_DEBUG
};



IContours::Ptr IContours::Instance(const cv::Mat& source)
{
    return boost::make_shared<ContoursProcessor>(source);
}

} // namespace rec