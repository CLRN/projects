set(PROJECT_NAME lib_recognition)

add_subdirectory(tests)

file(GLOB SOURCES "*.cpp" "*.h")
file(GLOB DEBUG_LIBS 
    "${THIRD_PARTY}/lib/Debug/opencv_*.lib" 
    "${THIRD_PARTY}/lib/Debug/libjpegd.lib" 
    "${THIRD_PARTY}/lib/Debug/libpngd.lib" 
    "${THIRD_PARTY}/lib/Debug/libtiffd.lib"
    "${THIRD_PARTY}/lib/Debug/zlibd.lib"
    "${THIRD_PARTY}/lib/Debug/libjasperd.lib"
    "${THIRD_PARTY}/lib/Debug/IlmImfd.lib"
    "${THIRD_PARTY}/lib/Debug/libwebpd.lib"
    "${THIRD_PARTY}/lib/ippicvmt.lib"
)
file(GLOB RELEASE_LIBS 
    "${THIRD_PARTY}/lib/Release/opencv_*.lib" 
    "${THIRD_PARTY}/lib/Release/libjpeg.lib" 
    "${THIRD_PARTY}/lib/Release/libpng.lib" 
    "${THIRD_PARTY}/lib/Release/libtiff.lib"
    "${THIRD_PARTY}/lib/Release/zlib.lib"
    "${THIRD_PARTY}/lib/Release/libjasper.lib"
    "${THIRD_PARTY}/lib/Release/IlmImf.lib"
    "${THIRD_PARTY}/lib/Release/libwebp.lib"
    "${THIRD_PARTY}/lib/ippicvmt.lib"
)

foreach(fileName ${DEBUG_LIBS})
    list(APPEND OPENCV_LIBRARIES debug "${fileName}")
endforeach()

foreach(fileName ${RELEASE_LIBS})
    list(APPEND OPENCV_LIBRARIES optimized "${fileName}")
endforeach()

add_library(${PROJECT_NAME} STATIC ${SOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "common")

target_link_libraries(${PROJECT_NAME} 
    lib_serialization
    
    # CreateToolbarEx
    Comctl32
    
    ${OPENCV_LIBRARIES}
)
