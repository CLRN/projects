#ifndef Contours_h__
#define Contours_h__

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>

#include <boost/shared_ptr.hpp>

namespace rec
{

class IContours
{
public:
    typedef boost::shared_ptr<IContours> Ptr;
    typedef std::vector<cv::Rect> List;

    struct GroupingType
    {
        enum Value
        {
            Words   = 0,        //!< group words in text areas
            Symbols = 1,        //!< group symbol parts in complete signs
            Lines   = 2         //!< group symbols to lines
        };
    };

    virtual ~IContours() {}

    virtual List Find() = 0;
    virtual void Group(List& data, GroupingType::Value type) = 0;
    virtual void Check4Inversion(cv::Mat& src) = 0;

    static Ptr Instance(const cv::Mat& source);
};

} // namespace rec
#endif // Contours_h__
