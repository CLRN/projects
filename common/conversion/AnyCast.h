#ifndef Conversion_h__
#define Conversion_h__

#include "exception/CheckHelpers.h"

#include <string>
#include <utility>

#include <conversion/stlencoders/base64.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/type_traits/is_enum.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/remove_cv.hpp>
#include <boost/mpl/or.hpp>
#pragma warning(push)
#pragma warning(disable:4244) // 'argument' : conversion from 'boost::locale::utf::code_point' to 'const wchar_t', possible loss of data
#include <boost/locale/encoding.hpp>
#pragma warning(pop)

namespace conv
{
	DECLARE_SIMPLE_EXCEPTION_CLASS(CastException, cmn::Exception);

    struct Ansi {};
    struct Base64 {};
    struct Hex {};

	namespace details
	{
		//! Customized stream type for boolean
		struct Boolean 
		{
			bool m_Data;
			Boolean() {}
			Boolean(bool data) : m_Data(data) {}
			operator bool() const { return m_Data; }

			template<typename T>
			friend std::basic_ostream<T>& operator << (std::basic_ostream<T>& out, const Boolean& b) 
			{
				out << std::boolalpha << b.m_Data;
				return out;
			}

			template<typename T>
			friend std::basic_istream<T> & operator >> (std::basic_istream<T>& in, Boolean& b) 
			{
				in >> std::boolalpha >> b.m_Data;
				if (in.fail())
				{
					in.unsetf(std::ios_base::boolalpha);
					in.clear();
					in >> b.m_Data;
				}
				return in;
			}
		};

		//! Type holder
		template<typename Target, typename Source>
		struct TypeHolder
		{
			typedef Target ResultType;
			typedef Source SourceType;
		};

		template<typename Target, typename Source>
		typename boost::disable_if
		<
			boost::mpl::or_<boost::is_enum<Target>, boost::is_enum<Source> >,
			Target
		>::type CastImpl(const Source& src)
		{
			try 
			{
				return boost::lexical_cast<typename boost::remove_cv<Target>::type>(src);
			}
			CATCH_PASS(CastException("Conversion failed"), src, (typeid(Target).name(), "targetType"), (typeid(Source).name(), "sourceType"))
		}

		template<typename Target, typename Source>
		typename boost::enable_if
		<
			boost::is_enum<Target>,
			Target
		>::type CastImpl(const Source& src)
		{
			try 
			{
				const int value = boost::lexical_cast<int>(src);
				return static_cast<typename boost::remove_cv<Target>::type>(value);
			}
			CATCH_PASS(CastException("Conversion failed"), src, (typeid(Target).name(), "targetType"), (typeid(Source).name(), "sourceType"))
		}

        template<typename Target, typename Source>
		typename boost::enable_if
		<
			 boost::is_enum<Source>,
			 Target
		>::type CastImpl(const Source& src)
		{
			try 
			{
				return boost::lexical_cast<typename boost::remove_cv<Target>::type>(static_cast<int>(src));
			}
			CATCH_PASS(CastException("Conversion failed"), src, (typeid(Target).name(), "targetType"), (typeid(Source).name(), "sourceType"))
		}

		//! Help template struct
		template<typename Target, typename Source>
		struct Caster : public TypeHolder<Target, Source>
		{
			ResultType operator () (const SourceType& src)
			{
				return CastImpl<ResultType, SourceType>(src);
			}
		};

		//! Specialized bool help struct
		template<typename Source>
		struct Caster<bool, Source> : public TypeHolder<bool, Source>
		{
			ResultType operator () (const SourceType& src)
			{
				return CastImpl<Boolean, SourceType>(src);
			}
		};

		//! Specialized bool help struct
		template<typename Target>
		struct Caster<Target, bool> : public TypeHolder<Target, bool>
		{
			ResultType operator () (const SourceType& src)
			{
				return CastImpl<ResultType, Boolean>(src);
			}
		};

        //! Specialized byte help struct
        template<typename Source>
        struct Caster<unsigned char, Source> : public TypeHolder<unsigned char, Source>
        {
            ResultType operator () (const SourceType& src)
            {
                return static_cast<unsigned char>(CastImpl<unsigned, SourceType>(src));
            }
        };

        //! Specialized byte help struct
        template<typename Target>
        struct Caster<Target, unsigned char> : public TypeHolder<Target, unsigned char>
        {
            ResultType operator () (const SourceType& src)
            {
                return CastImpl<ResultType, unsigned>(src);
            }
        };

		//! Specialized unicode to utf8 struct
		template<>
		struct Caster<std::string, std::wstring> : public TypeHolder<std::string, std::wstring>
		{
			ResultType operator () (const SourceType& src)
			{
				return boost::locale::conv::from_utf<wchar_t>(src, "utf8");
			}
		};

		//! Specialized utf8 to unicode struct
		template<>
		struct Caster<std::wstring, std::string> : public TypeHolder<std::wstring, std::string>
		{
			ResultType operator () (const SourceType& src)
			{
				return boost::locale::conv::utf_to_utf<wchar_t, char>(src);
			}
		};

		//! Specialized unicode to utf8 help struct
		template<>
		struct Caster<std::string, const wchar_t*> : public TypeHolder<std::string, const wchar_t*>
		{
			ResultType operator () (SourceType src)
			{
				return src ? boost::locale::conv::from_utf<wchar_t>(src, "utf8") : std::string();
			}
		};

		//! Specialized utf8 to unicode help struct
		template<>
		struct Caster<std::wstring, const char*> : public TypeHolder<std::wstring, const char*>
		{
			ResultType operator () (SourceType src)
			{
				return src ? boost::locale::conv::utf_to_utf<wchar_t, char>(src) : std::wstring();
			}
		};

		//! Specialized ansi to utf8 help struct
		template<>
		struct Caster<std::string, Ansi> : public TypeHolder<std::string, std::string>
		{
			ResultType operator () (const SourceType& src)
			{
				return boost::locale::conv::to_utf<char>(src, "cp1251");
			}
		};

		//! Specialized ansi to unicode help struct
		template<>
		struct Caster<std::wstring, Ansi> : public TypeHolder<std::wstring, std::string>
		{
			ResultType operator () (const SourceType& src)
			{
				return boost::locale::conv::to_utf<wchar_t>(src, "cp1251");
			}
		};

		//! Specialized unicode to ansi help struct
		template<>
		struct Caster<Ansi, std::wstring> : public TypeHolder<std::string, std::wstring>
		{
			ResultType operator () (const SourceType& src)
			{
				return boost::locale::conv::from_utf(src, "cp1251");
			}
		};

        //! Specialized help struct - conversion bits from integer to vector
        template<>
        struct Caster<std::vector<unsigned>, unsigned> : public TypeHolder<std::vector<unsigned>, unsigned>
        {
            ResultType operator () (const SourceType& src)
            {
                ResultType result;

                if (!src)
                    return result;

                unsigned mask = 1;
                unsigned counter = 0;
                for (; mask; mask <<= 1, ++counter)
                {
                    if (src & mask)
                        result.push_back(counter);
                }

                return result;
            }
        };

        //! Specialized help struct - conversion vector of values to bits
        template<>
        struct Caster<unsigned, std::vector<unsigned>> : public TypeHolder<unsigned, std::vector<unsigned>>
        {
            ResultType operator () (const SourceType& src)
            {
                ResultType result = 0;

                if (src.empty())
                    return result;

                for (const unsigned bit : src)
                    result |= 1 << bit;

                return result;
            }
        };

        //! Specialized help struct - conversion posix time to 
        template<>
        struct Caster<unsigned __int64, boost::posix_time::ptime> : public TypeHolder<unsigned __int64, boost::posix_time::ptime>
        {
            ResultType operator () (const SourceType& src)
            {
                using namespace boost::posix_time;
                static ptime epoch(boost::gregorian::date(2012, 1, 1));
                time_duration diff(src - epoch);
                return diff.total_milliseconds();
            }
        };

        //! Specialized help struct - conversion int64 to posix time
        template<>
        struct Caster<boost::posix_time::ptime, unsigned __int64> : public TypeHolder<boost::posix_time::ptime, unsigned __int64>
        {
            ResultType operator () (const SourceType& src)
            {
                using namespace boost::posix_time;
                static ptime epoch(boost::gregorian::date(2012, 1, 1));
                epoch + boost::posix_time::milliseconds(src);
                return epoch + boost::posix_time::milliseconds(src);
            }
        };

        //! Specialized help struct - conversion posix time to string
        template<>
        struct Caster<std::string, boost::posix_time::ptime> : public TypeHolder <std::string, boost::posix_time::ptime>
        {
            ResultType operator () (const SourceType& src)
            {
                return boost::posix_time::to_iso_extended_string(src);
            }
        };

        //! Specialized help struct - conversion string to posix time
        template<>
        struct Caster<boost::posix_time::ptime, std::string> : public TypeHolder <boost::posix_time::ptime, std::string>
        {
            ResultType operator () (const SourceType& src)
            {
                boost::posix_time::ptime pt;
                std::istringstream is(src);
                is.imbue(std::locale(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%dT%H:%M:%S%f")));
                is >> pt;
                return pt;
            }
        };

        template<typename T>
        struct CharTraits
        {
            typedef T type;
        };

        template<>
        struct CharTraits<char>
        {
            typedef char type;
        };
        template<>
        struct CharTraits<unsigned char>
        {
            typedef char type;
        };

        //! Bin to base64 help struct
        template<typename Source>
        struct Caster<Base64, Source> : public TypeHolder<std::string, Source>
        {
            ResultType operator () (const SourceType& src)
            {
                if (src.empty())
                    return ResultType();

                ResultType result;
                stlencoders::base64<CharTraits<SourceType::value_type>::type>::encode(src.begin(), src.end(), std::back_inserter(result));
                return result;
            }
        };

        template<typename Target>
        struct Caster<Target, Base64> : public TypeHolder<Target, Base64>
        {
            template<typename T>
            ResultType operator () (const T& src)
            {
                if (src.empty())
                    return ResultType();

                ResultType result;
                stlencoders::base64<CharTraits<T::value_type>::type>::decode(src.begin(), src.end(), std::back_inserter(result));
                return result;
            }
        };

        //! Base64 to binary help struct
        template<>
        struct Caster<std::vector<char>, std::string> : public TypeHolder<std::vector<char>, std::string>
        {
            ResultType operator () (const SourceType& src)
            {
                return Caster<ResultType, Base64>()(src);
            }
        };

        //! Binary to base64 help struct
        template<>
        struct Caster<std::string, std::vector<char> > : public TypeHolder<std::string, std::vector<char> >
        {
            ResultType operator () (const SourceType& src)
            {
                return Caster<Base64, std::vector<char> >()(src);
            }
        };

        //! Base64 to binary help struct
        template<>
        struct Caster<std::vector<unsigned char>, std::string> : public TypeHolder<std::vector<unsigned char>, std::string>
        {
            ResultType operator () (const SourceType& src)
            {
                return Caster<ResultType, Base64>()(src);
            }
        };

        //! Binary to base64 help struct
        template<>
        struct Caster<std::string, std::vector<unsigned char> > : public TypeHolder<std::string, std::vector<unsigned char> >
        {
            ResultType operator () (const SourceType& src)
            {
                return Caster<Base64, std::vector<unsigned char> >()(src);
            }
        };

        //! Bin to hex help struct
        template<>
        struct Caster<Hex, std::vector<char> > : public TypeHolder<std::string, std::vector<char> >
        {
            ResultType operator () (const SourceType& src)
            {
                if (src.empty())
                    return std::string();

                ResultType result;
                result.reserve(src.size() * 2);
                std::string temp;
                std::ostringstream oss;
                for (const auto byte : src)
                {
                    oss.str("");
                    oss << std::hex << std::setw(2) << std::setfill('0') << static_cast<unsigned>(byte);
                    temp = oss.str();
                    result.append(temp.substr(temp.size() - 2, 2));
                }

                return result;
            }
        };


        //! Hex to binary help struct
        template<>
        struct Caster<std::vector<char>, Hex> : public TypeHolder<std::vector<char>, std::string>
        {
            ResultType operator () (const SourceType& src)
            {
                ResultType data;
                data.reserve(src.size() / 2);

                unsigned int tmp;
                for (unsigned i = 0; i < src.size() / 2; ++i)
                {                   
                    std::istringstream iss(src.substr(i * 2, 2));
                    iss >> std::hex >> tmp;
                    data.push_back(static_cast<ResultType::value_type>(tmp));
                }
                return data;
            }
        };

	} // namespace details

	//! Cast function
	template<typename Target, typename Source>
	inline typename details::Caster<Target, Source>::ResultType cast(const Source& value)
	{
		return details::Caster<Target, Source>()(value);
	}

	//! Cast function
	template<typename Target, typename From, typename Source>
	inline typename details::Caster<Target, Source>::ResultType cast(const Source& value)
	{
		return details::Caster<Target, From>()(value);
	}
    
	//! Cast function for const strings
	template<typename Target, typename Source, size_t N>
	inline typename details::Caster<Target, Source>::ResultType cast(const Source (&value)[N])
	{
		typedef std::basic_string<Source> SourceString;
		return details::Caster<Target, SourceString>()(SourceString(value, N - 1));
	}
}


#endif // Conversion_h__
