file(GLOB SOURCES "*.cpp" "*.h")

set(target_name lib_conversion)

add_library(${target_name} STATIC ${SOURCES})
target_link_libraries(${target_name}
    lib_exception
)
set_target_properties(${target_name} PROPERTIES FOLDER "common")

add_subdirectory(tests)
