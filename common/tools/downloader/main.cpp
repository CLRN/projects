#include "exception/Exception.h"
#include "common/Log.h"
#include "common/Modules.h"
#include "conversion/AnyCast.h"
#include "inet/RemoteFile.h"

#include <wininet.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/scope_exit.hpp>

namespace po = boost::program_options;


DECLARE_CURRENT_MODULE(Modules::Tools);


void DownloadRaw(ILog& log, const std::string& url, const std::string& out)
{
    std::wstring result;
    {
        const auto file = net::IRemoteFile::Instance(log, L"temp", conv::cast<std::wstring>(url), [](unsigned size, unsigned total){
            std::cout << "progress: " << size << "(" << total << ") bytes" << std::endl;
        });
        result = file->Download();
    }

    boost::filesystem::rename(result, out);
    boost::filesystem::remove("temp");
}

void DownloadWin(ILog& log, const std::string& url, const std::string& out)
{
    const ::HINTERNET session = ::InternetOpenW(0, INTERNET_OPEN_TYPE_DIRECT, 0, 0, 0);
    CHECK_LE(session, cmn::Exception("InternetOpen"));

    BOOST_SCOPE_EXIT(&session)
    {
        InternetCloseHandle(session);
    }
    BOOST_SCOPE_EXIT_END;

    const ::HINTERNET stream = ::InternetOpenUrlA(session, url.c_str(), 0, 0, 0, 0);
    CHECK_LE(stream, cmn::Exception("InternetOpenUrl"));

    BOOST_SCOPE_EXIT(&stream)
    {
        InternetCloseHandle(stream);
    }
    BOOST_SCOPE_EXIT_END;

    std::ofstream ofs(out.c_str(), std::ios::binary);
    CHECK(ofs.is_open(), cmn::Exception("Failed to open output file for write"), out);

    static const DWORD SIZE = 1024;
    DWORD error = ERROR_SUCCESS;
    BYTE data[SIZE];
    DWORD size = 0;
    do 
    {
        BOOL result = ::InternetReadFile(stream, data, SIZE, &size);
        CHECK_LE(result, cmn::Exception("InternetReadFile"));

        ofs.write((const char*)data, size);
    } while ((error == ERROR_SUCCESS) && (size > 0));
}

int main(int ac, char* av[])
{
	Log logger;
    logger.Open(ILog::Level::Trace);

	try 
	{
		// Declare the supported options.
		po::options_description desc("allowed options");
		desc.add_options()
			("help", "produce help message")
			("url,U", po::value<std::string>(), "remote file url")
			("method,M", po::value<std::string>()->default_value("raw"), "download method(raw/wininet)")
			("out,O", po::value<std::string>(), "local file path");
        		 
		po::variables_map vm;
		po::store(po::command_line_parser(ac, av).options(desc).run(), vm);
		po::notify(vm);

		if (vm.count("help")) 
		{
			std::cout << desc << std::endl;
			return 1;
		}

        CHECK(vm.count("url") && !vm["url"].empty(), cmn::Exception("'url' must be specified"));
        CHECK(vm.count("method") && !vm["method"].empty(), cmn::Exception("'url' must be specified"));
        CHECK(vm.count("out") && !vm["out"].empty(), cmn::Exception("'out' must be specified"));

        const auto& method = vm["method"].as<std::string>();
        const auto& url = vm["url"].as<std::string>();
        const auto& out = vm["out"].as<std::string>();
        if (method == "raw")
            DownloadRaw(logger, url, out);
        else
        if (method == "wininet")
            DownloadWin(logger, url, out);
        else
            THROW(cmn::Exception("Unsupported method"), method);
	}
	catch(const std::exception& e)
    {
        LERROR(logger, CURRENT_MODULE_ID, "%s") % cmn::ExceptionInfo(e);
    }

	logger.Close();

	return 0;
}

