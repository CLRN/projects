#ifndef stdafx_h__
#define stdafx_h__

#include "db/SQLiteDB.h"
#include "common/Log.h"
#include "exception/CheckHelpers.h"
#include "common/Modules.h"

#include <vector>
#include <string>
#include <iostream>
 
#pragma warning(push)
#pragma warning(disable : 4512)
#include <boost/program_options.hpp>
#pragma warning(pop) 

#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/exception_ptr.hpp>

DECLARE_CURRENT_MODULE(Modules::Tools);

#endif // stdafx_h__
