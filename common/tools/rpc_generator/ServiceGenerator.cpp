#include "ServiceGenerator.h"
#include <google/protobuf/compiler/cpp/cpp_helpers.h>
#include <google/protobuf/io/printer.h>

#pragma warning(push)
#pragma warning(disable: 4127)
#include <google/protobuf/stubs/strutil.h>
#pragma warning(pop)

namespace google {
namespace protobuf {
namespace compiler {
namespace cpp {

ServiceGenerator::ServiceGenerator(const ServiceDescriptor* descriptor)
  : descriptor_(descriptor) {
  vars_["classname"] = descriptor_->name();
  vars_["full_name"] = descriptor_->full_name();
  vars_["dllexport"] = "";
}

ServiceGenerator::~ServiceGenerator() {}

void ServiceGenerator::GenerateDeclarations(io::Printer* printer) {
  // Forward-declare the stub type.
  printer->Print(vars_,
    "class $classname$_Stub;\n"
    "\n");

  GenerateInterface(printer);
  GenerateStubDefinition(printer);
}

void ServiceGenerator::GenerateInterface(io::Printer* printer) {
  printer->Print(vars_,
    "class $dllexport$$classname$ : public rpc::IService {\n"
    " protected:\n"
    "  // This class should be treated as an abstract interface.\n"
    "  inline $classname$() {};\n"
    " public:\n"
    "  virtual ~$classname$();\n");
  printer->Indent();

  printer->Print(vars_,
    "\n"
    "typedef $classname$_Stub Stub;\n"
    "\n"
    "static const ::google::protobuf::ServiceDescriptor& descriptor();\n"
    "\n");

  for (int i = 0; i < descriptor_->method_count(); i++) {
      const MethodDescriptor* method = descriptor_->method(i);
      map<string, string> sub_vars;
      sub_vars["name"] = method->name();
      sub_vars["input_type"] = ClassName(method->input_type(), true);
      sub_vars["output_type"] = ClassName(method->output_type(), true);

      printer->Print(sub_vars,
          "virtual void $name$(const rpc::Request<$input_type$>::Ptr& request,\n"
          "                    const rpc::Response<$output_type$>::Ptr& response);\n");
  }

  printer->Print(
    "\n"
    "// implements Service ----------------------------------------------\n"
    "\n"
    "const ::google::protobuf::ServiceDescriptor& GetDescriptor();\n"
    "void CallMethod(const ::google::protobuf::MethodDescriptor& method,\n"
    "                const rpc::MessagePtr& request,\n"
    "                const rpc::MessagePtr& response) override;\n"

    "google::protobuf::Message* CreateRequest(\n"
    "  const ::google::protobuf::MethodDescriptor& method) const;\n"
    "google::protobuf::Message* CreateResponse(\n"
    "  const ::google::protobuf::MethodDescriptor& method) const;\n"
    "Id GetId() const override;\n");

  printer->Outdent();
  printer->Print(vars_,
    "\n"
    " private:\n"
    "  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS($classname$);\n"
    "};\n"
    "\n");
}

void ServiceGenerator::GenerateStubDefinition(io::Printer* printer) {
  printer->Print(vars_,
    "class $dllexport$$classname$_Stub : public $classname$ {\n"
    " public:\n");

  printer->Indent();

  printer->Print(vars_,
    "$classname$_Stub(rpc::details::IChannel& channel,\n"
    "                 const rpc::details::CallParams& params = rpc::details::CallParams()); \n"
    "~$classname$_Stub();\n"
    "\n"
    "// implements $classname$ ------------------------------------------\n"
    "\n");

  GenerateMethodSignatures(NON_VIRTUAL, printer);

  printer->Outdent();
  printer->Print(vars_,
    " private:\n"
    "  rpc::details::IChannel* channel_;\n"
    "  rpc::details::CallParams params_;\n"
    "};\n"
    "\n");
}

void ServiceGenerator::GenerateMethodSignatures(
    VirtualOrNon virtual_or_non, io::Printer* printer) {
  for (int i = 0; i < descriptor_->method_count(); i++) {
    const MethodDescriptor* method = descriptor_->method(i);
    map<string, string> sub_vars;
    sub_vars["name"] = method->name();
    sub_vars["input_type"] = ClassName(method->input_type(), true);
    sub_vars["output_type"] = ClassName(method->output_type(), true);
    sub_vars["virtual"] = virtual_or_non == VIRTUAL ? "virtual " : "";

    printer->Print(sub_vars,
        "$virtual$rpc::Future<$output_type$> $name$(const $input_type$& request);\n"
        "$virtual$rpc::Future<$output_type$> $name$(const $input_type$& request, const rpc::IStream& stream);\n");
  }
}

// ===================================================================

void ServiceGenerator::GenerateDescriptorInitializer(
    io::Printer* printer, int index) {
  map<string, string> vars;
  vars["classname"] = descriptor_->name();
  vars["index"] = SimpleItoa(index);
  vars["file"] = descriptor_->file()->name();

  printer->Print(vars,
    "const ::google::protobuf::FileDescriptor* file$classname$_descriptor_ = ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(\"$file$\");\n"
    "const ::google::protobuf::ServiceDescriptor* $classname$_descriptor_ = file$classname$_descriptor_->service($index$);\n");
}

// ===================================================================

void ServiceGenerator::GenerateImplementation(io::Printer* printer) {
  printer->Print(vars_,
    "$classname$::~$classname$() {}\n"
    "\n"
    "const ::google::protobuf::ServiceDescriptor& $classname$::descriptor() {\n"
    "  protobuf_AssignDescriptorsOnce();\n"
    "  return *$classname$_descriptor_;\n"
    "}\n"
    "\n"
    "const ::google::protobuf::ServiceDescriptor& $classname$::GetDescriptor() {\n"
    "  protobuf_AssignDescriptorsOnce();\n"
    "  return *$classname$_descriptor_;\n"
    "}\n"
    "\n"
    
    "rpc::IService::Id $classname$::GetId() const {\n"
    "  return descriptor().options().GetExtension(proto::service_id); \n"
    "}\n"
    "\n"
    );

  // Generate methods of the interface.
  GenerateNotImplementedMethods(printer);
  GenerateCallMethod(printer);
  GenerateGetPrototype(REQUEST, printer);
  GenerateGetPrototype(RESPONSE, printer);

  // Generate stub implementation.
  printer->Print(vars_,
    "$classname$_Stub::$classname$_Stub(rpc::details::IChannel& channel, \n"
    "                                   const rpc::details::CallParams& params)\n"
    "  : channel_(&channel)\n"
    "  , params_(params) {}\n"
    "$classname$_Stub::~$classname$_Stub() {\n"
    "}\n"
    "\n");

  GenerateStubMethods(printer);
}

void ServiceGenerator::GenerateNotImplementedMethods(io::Printer* printer) {
  for (int i = 0; i < descriptor_->method_count(); i++) {
    const MethodDescriptor* method = descriptor_->method(i);
    map<string, string> sub_vars;
    sub_vars["classname"] = descriptor_->name();
    sub_vars["name"] = method->name();
    sub_vars["index"] = SimpleItoa(i);
    sub_vars["input_type"] = ClassName(method->input_type(), true);
    sub_vars["output_type"] = ClassName(method->output_type(), true);

    printer->Print(sub_vars,
        "void $classname$::$name$(const rpc::Request<$input_type$>::Ptr&,\n"
        "                         const rpc::Response<$output_type$>::Ptr&) {\n"
        "  THROW(rpc::Exception(\"Method not implemeted\"));\n"
      "}\n"
      "\n");
  }
}

void ServiceGenerator::GenerateCallMethod(io::Printer* printer) {
  printer->Print(vars_,
    "void $classname$::CallMethod(const google::protobuf::MethodDescriptor& method,\n"
    "                             const rpc::MessagePtr& request,\n"
    "                             const rpc::MessagePtr& response) {\n"
    "  GOOGLE_DCHECK_EQ(method.service(), $classname$_descriptor_);\n"
    "  switch(method.index()) {\n");

  for (int i = 0; i < descriptor_->method_count(); i++) {
    const MethodDescriptor* method = descriptor_->method(i);
    map<string, string> sub_vars;
    sub_vars["name"] = method->name();
    sub_vars["index"] = SimpleItoa(i);
    sub_vars["input_type"] = ClassName(method->input_type(), true);
    sub_vars["output_type"] = ClassName(method->output_type(), true);

    // Note:  down_cast does not work here because it only works on pointers,
    //   not references.
    printer->Print(sub_vars,
      "    case $index$:\n"
      "      $name$(boost::dynamic_pointer_cast<rpc::Request<$input_type$> >(request),\n"
      "             boost::dynamic_pointer_cast<rpc::Response<$output_type$> >(response));\n"
      "      break;\n");
  }

  printer->Print(vars_,
    "    default:\n"
    "      GOOGLE_LOG(FATAL) << \"Bad method index; this should never happen.\";\n"
    "      break;\n"
    "  }\n"
    "}\n"
    "\n");
}

void ServiceGenerator::GenerateGetPrototype(RequestOrResponse which,
                                            io::Printer* printer) {
  if (which == REQUEST) {
    printer->Print(vars_,
      "google::protobuf::Message* $classname$::CreateRequest(\n");
  } else {
    printer->Print(vars_,
      "google::protobuf::Message* $classname$::CreateResponse(\n");
  }

  printer->Print(vars_,
    "    const ::google::protobuf::MethodDescriptor& method) const {\n"
    "  GOOGLE_DCHECK_EQ(method.service(), &descriptor());\n"
    "  switch(method.index()) {\n");

  for (int i = 0; i < descriptor_->method_count(); i++) {
    const MethodDescriptor* method = descriptor_->method(i);
    const Descriptor* type =
      (which == REQUEST) ? method->input_type() : method->output_type();

    map<string, string> sub_vars;
    sub_vars["index"] = SimpleItoa(i);
    sub_vars["type"] = ClassName(type, true);

    if (which == REQUEST) {
        printer->Print(sub_vars,
          "    case $index$:\n"
          "      return new rpc::Request<$type$>;\n");
    }
    else {
        printer->Print(sub_vars,
            "    case $index$:\n"
            "      return new rpc::Response<$type$>;\n");
    }
  }

  printer->Print(vars_,
    "    default:\n"
    "      GOOGLE_LOG(FATAL) << \"Bad method index; this should never happen.\";\n"
    "      return reinterpret_cast< ::google::protobuf::Message*>(NULL);\n"
    "  }\n"
    "}\n"
    "\n");
}

void ServiceGenerator::GenerateStubMethods(io::Printer* printer) {
  for (int i = 0; i < descriptor_->method_count(); i++) {
    const MethodDescriptor* method = descriptor_->method(i);
    map<string, string> sub_vars;
    sub_vars["classname"] = descriptor_->name();
    sub_vars["name"] = method->name();
    sub_vars["index"] = SimpleItoa(i);
    sub_vars["input_type"] = ClassName(method->input_type(), true);
    sub_vars["output_type"] = ClassName(method->output_type(), true);

    printer->Print(sub_vars,
        "rpc::Future<$output_type$> $classname$_Stub::$name$(const $input_type$& request) {\n"
        "return rpc::Future<$output_type$>(channel_->CallMethod(*descriptor().method($index$), request, rpc::IStream(), params_));\n"
      "}\n"
      
      "rpc::Future<$output_type$> $classname$_Stub::$name$(const $input_type$& request, \n"
      "   const rpc::IStream& stream) {\n"
      "return rpc::Future<$output_type$>(channel_->CallMethod(*descriptor().method($index$), request, stream, params_));\n"
      "}\n");
  }
}

}  // namespace cpp
}  // namespace compiler
}  // namespace protobuf
}  // namespace google
