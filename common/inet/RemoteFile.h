#ifndef remote_file_h__
#define remote_file_h__

#include "exception/Exception.h"

#include <boost/function.hpp>
#include <boost/exception_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

class ILog;
namespace net
{

DECLARE_SIMPLE_EXCEPTION_CLASS(DownloadException, cmn::Exception);
DECLARE_SIMPLE_EXCEPTION_CLASS(CertVerifyException, cmn::Exception);
DECLARE_SIMPLE_EXCEPTION_CLASS(DownloadOpenException, cmn::Exception);

//! Remote file interface
class IRemoteFile : boost::noncopyable
{
public:

    virtual ~IRemoteFile() {}

    //! Pointer type
    typedef boost::shared_ptr<IRemoteFile> Ptr;

    //! Download callback, invokes when download is finished
    typedef boost::function<void (const std::wstring& fileName, const boost::exception_ptr& e)> DoneCallback;

    //! Progress callback, invokes when download progress is changed
    typedef boost::function<void (std::size_t total, std::size_t completed)> ProgressCallback;

    //! Download file synchronous
    //!
    //! \return std::wstring local file path
    //! \throw exception on error
    //!
    virtual std::wstring Download() = 0;

    //! Download file asynchronous
    //!
    //! \param[in] done - download callback, invokes when download is finished
    //! \return void
    //!
    virtual void Download(const DoneCallback& done) = 0;

    //! Cancel all operations
    virtual void Cancel() = 0;

    //! Create remote file instance
    static Ptr Instance(
        ILog& log,
        const std::wstring& directory, 
        const std::wstring& url, 
        const ProgressCallback& progress = ProgressCallback()
    );
};

} // namespace net

#endif // remote_file_h__
