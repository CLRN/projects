#include "RemoteFile.h"
#include "crypto/Utils.h"
#include "common/AsioHelpers.h"
#include "common/ServiceHolder.h"
#include "conversion/AnyCast.h"
#include "exception/ErrorCodeInfo.h"
#include "common/Modules.h"

#pragma warning(push)
#pragma warning(disable:4244) // warning C4244: '=' : conversion from 'std::streamsize' to 'size_t', possible loss of data
#include <urdl/ssl.hpp>
#include <urdl/read_stream.hpp>
#pragma warning(pop)

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/range/algorithm.hpp>

namespace net
{

namespace fs = boost::filesystem;
DECLARE_CURRENT_MODULE(Modules::Network);
DECLARE_EXCEPTION_INFO_VISITOR(&cmn::ErrorCodeInfoVisitor);

class Impl : public IRemoteFile, public boost::enable_shared_from_this<Impl>
{
    enum { OPEN_TIMEOUT = 60000 };
    enum { READ_TIMEOUT = 30000 };
    enum { MAX_ATTEMPTS = 5 };
    enum { ATTEMPT_DELAY = 1000 };
public:

    Impl(ILog& log, const std::wstring& directory, const std::wstring& url, const ProgressCallback& progress)
        : m_Log(log)
        , m_Service(cmn::Service::Instance())
        , m_Directory(directory)
        , m_Url(url)
        , m_ReadStream(m_Service)
        , m_TimeoutTimer(m_Service)
        , m_Attempt()
        , m_Downloaded()
        , m_ProgressCallback(progress)
    {
        m_ReadStream.set_option(urdl::ssl::verify_peer(true));
        urdl::ssl::verify_callback::cb_t cb = urdl::ssl::verify_callback::cb_t([this](bool preverified, boost::asio::ssl::verify_context& ctx){
            try
            {
                crypto::verify_certificate(ctx);
                m_CertVerifyEC = boost::system::error_code();
                return true;
            }
            catch (const std::exception& e)
            {
                if (const boost::system::error_code* ec = cmn::get_error_info<cmn::ErrorCodeInfo>(e))
                    m_CertVerifyEC = *ec;
                else
                    m_CertVerifyEC = boost::system::error_code(::GetLastError(), boost::system::system_category());
            	return preverified;
            }
        });
        m_ReadStream.set_option(urdl::ssl::verify_callback(cb));
    }

    std::wstring Download()
    {
        try
        {
            const auto promise = boost::make_shared<boost::promise<std::wstring>>();	
	        const auto callback = [promise](const std::wstring& fileName, const boost::exception_ptr& excpt)
	        {
	            if (excpt)
	                promise->set_exception(boost::copy_exception(excpt));
	            else
	                promise->set_value(fileName);
	        };
	
	        // download asynchronous
	        Download(callback);
	
	        // wait for response
	        auto future = promise->get_future();
            PollUntil(boost::bind(&boost::unique_future<std::wstring>::is_ready, &future));
	
	        return future.get();
        }
        catch (const boost::exception_ptr& e)
        {
            boost::rethrow_exception(e);
        }     
    }

    void Download(const DoneCallback& done)
    {
        m_DoneCallback = done;

        // open timeout
        m_TimeoutTimer.expires_from_now(boost::posix_time::milliseconds(OPEN_TIMEOUT));
        m_TimeoutTimer.async_wait(boost::bind(&Impl::OpenTimedOut, shared_from_this(), _1));

        // open url
        m_ReadStream.async_open(conv::cast<std::string>(m_Url), boost::bind(&Impl::OpenSucceeded, shared_from_this(), _1));
    }

private:

    //! Open timeout handler
    void OpenTimedOut(const boost::system::error_code& ec)
    {
        try
        {
            CHECK(ec == boost::asio::error::operation_aborted, DownloadOpenException("open url timed out") << cmn::ErrorCodeInfo(ec), m_Url);
        }
        catch (const std::exception& e)
        {
            OnError(e, L"open url");
        }
    }

    //! Read timeout handler
    void ReadTimedOut(const boost::system::error_code& ec)
    {
        try
        {
            CHECK(ec == boost::asio::error::operation_aborted, DownloadException("read url timed out") << cmn::ErrorCodeInfo(ec), m_Url);
        }
        catch (const std::exception& e)
        {
            OnError(e, L"read url");
        }
    }

    //! Open succeeded
    void OpenSucceeded(const boost::system::error_code& ec)
    {
        try
        {

            // ��� ������
            if (!ec)
            {
                // ������� ������������
                LOG_TRACE("async opening url is successful: %s") % m_Url;

                // ������� �������
                m_TimeoutTimer.cancel();

                // ������
                const std::size_t size2download = m_ReadStream.content_length();
                LOG_TRACE("size of: %s is %s") % m_Url % size2download;

                if (m_ProgressCallback)
                    m_ProgressCallback(size2download, 0);

                // ������ ���� �� ����������
                const fs::path file = fs::current_path() / m_Directory / GetFileExtForSave(conv::cast<std::wstring>(m_ReadStream.last_url()));
                m_FileName = file.wstring();
                {
                    fs::create_directories(file.branch_path());
                    m_OutStream.open(m_FileName, std::ios::binary);
                    CHECK(m_OutStream.is_open(), DownloadException("unable to open file for write"), (m_FileName, "path"), m_Url);
                }
                LOG_TRACE("url: %s after redirect: %s, save as file: %s") % m_Url % conv::cast<std::wstring>(m_ReadStream.last_url()) % m_FileName;

                if (size2download != std::numeric_limits<std::size_t>::max())
                    m_ContentLength = size2download;

                m_Downloaded = 0;

                //  �������� ������
                m_TimeoutTimer.expires_from_now(boost::posix_time::milliseconds(READ_TIMEOUT));
                m_TimeoutTimer.async_wait(boost::bind(&Impl::ReadTimedOut, shared_from_this(), _1));
                m_ReadStream.async_read_some(boost::asio::buffer(m_Data), boost::bind(&Impl::ReadSucceeded, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
            }
            else
            {
                // ��� ������ �� �������� ������� ��-��������� �������� ������ ������� ����������� ����� ��������

                // ���� ��� �� ������������ ��������, �� ������� ���, �.�. �������, ��� � ��� ���� ��� ��� �����
                if (ec != boost::asio::error::operation_aborted)
                {
                    m_TimeoutTimer.cancel();
                }

                m_ReadStream.close();

                // ���� �� ������� �������� ����������, �� ������ ��� ������ ����������
                CHECK(!m_CertVerifyEC, CertVerifyException("certificate verification failed") << cmn::ErrorCodeInfo(m_CertVerifyEC), m_Url);

                // ������� ������ �������� ��������
                if (m_Attempt < MAX_ATTEMPTS)
                {
                    // ��������� �������
                    ++m_Attempt;
                    m_TimeoutTimer.expires_from_now(boost::posix_time::milliseconds(ATTEMPT_DELAY));
                    m_TimeoutTimer.async_wait(boost::bind(&Impl::Download, shared_from_this(), m_DoneCallback));
                }
                else
                {
                    // �������� ������ �������� �� ������
                    THROW(DownloadOpenException("invalid url") << cmn::ErrorCodeInfo(ec), m_Url);
                }
            }
        }
        catch (const std::exception& e)
        {
            OnError(e, L"start download file");
        }
    }

    void ReadSucceeded(const boost::system::error_code& ec, std::size_t length)
    {
        static long short_read_error = 335544539;
        bool is_ssl_short_read_error = 
#ifndef URDL_DISABLE_SSL
            ec.category() == boost::asio::error::ssl_category &&
            ec.value() == short_read_error
#else
            false
#endif
            ;

        try
        {
            // ��� ������
            if (!ec)
            {
                // ������� ������� - ������� �������
                m_TimeoutTimer.cancel();

                // �������� ���������...
                m_OutStream.write(m_Data.c_array(), length);
                m_OutStream.flush();

                m_Downloaded += length;
                if (m_ProgressCallback)
                    m_ProgressCallback(m_ContentLength ? m_ContentLength.get() : 0, m_Downloaded);

                // ...� ���������� ������
                m_TimeoutTimer.expires_from_now(boost::posix_time::milliseconds(READ_TIMEOUT));
                m_TimeoutTimer.async_wait(boost::bind(&Impl::ReadTimedOut, shared_from_this(), _1));
                m_ReadStream.async_read_some(boost::asio::buffer(m_Data), boost::bind(&Impl::ReadSucceeded, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
            }
            // eof
            else if (ec == boost::asio::error::eof || is_ssl_short_read_error)
            {
                // ��� ������� - ������� �������
                m_TimeoutTimer.cancel();

                if (m_ContentLength && (m_Downloaded < m_ContentLength.get()))
                {
                    CHECK(!is_ssl_short_read_error, DownloadException("short_read") << cmn::ErrorCodeInfo(ec));
                    THROW(DownloadException("unexpected EOF") << cmn::ErrorCodeInfo(ec), m_Url);
                }

                LOG_TRACE("file was successfuly read: %s") % m_FileName;
                m_OutStream.close();// ��������� ����

                // �������
                m_ReadStream.close();
                m_TimeoutTimer.cancel();
                if (m_ProgressCallback)
                    m_ProgressCallback(m_ContentLength ? m_ContentLength.get() : 0, m_Downloaded);

                if (m_DoneCallback)
                    m_DoneCallback(m_FileName, boost::exception_ptr());
            }
            // operation_aborted
            else if (ec == boost::asio::error::operation_aborted)
            {
                // �������� �������
                THROW(DownloadException("timeout on reading url") << cmn::ErrorCodeInfo(ec), m_Url);
            }
            // ��� ���������
            else
            {
                // ������� �������, �.�. �������, ��� � ��� ���� ��� ��� �����
                m_TimeoutTimer.cancel();

                THROW(DownloadException("unprocessed error_code") << cmn::ErrorCodeInfo(ec), m_Url);
            }
        }
        catch (const std::exception& e)
        {
            OnError(e, L"read file");
        }
    }

    void OnError(const std::exception& e, const wchar_t* text)
    {
        LOG_ERROR("failed to %s, error: %s") % std::wstring(text) % cmn::ExceptionInfo(e);
        m_ReadStream.close();
        m_TimeoutTimer.cancel();
        if (m_DoneCallback)
            m_DoneCallback(L"", boost::current_exception());
    }

    fs::path GetFileExtForSave(const std::wstring& url)
    {
        // ������� "?" � ����� ����
        fs::path path(url.begin(), boost::find(url, L'?'));

        const std::wstring basename = path.stem().wstring();
        const std::wstring extension = path.extension().wstring();

        std::wstring full_name = basename + L"_" + boost::uuids::to_wstring(boost::uuids::random_generator()()) + extension;

        // ������� "-"
        return boost::algorithm::erase_all_copy(full_name, L"-");
    }

    virtual void Cancel() override
    {
        m_ReadStream.close();
        m_TimeoutTimer.cancel();
        m_OutStream.close();
        ProgressCallback().swap(m_ProgressCallback);
        DoneCallback().swap(m_DoneCallback);
    }

private:

    ILog&                               m_Log;
    boost::asio::io_service&            m_Service;
    const std::wstring                  m_Directory;
    const std::wstring                  m_Url;
    std::wstring                        m_FileName;
    urdl::read_stream                   m_ReadStream;               // ������ ������
    boost::filesystem::ofstream         m_OutStream;                // ���� ����������
    boost::asio::deadline_timer         m_TimeoutTimer;             // ������� �� ��������
    boost::array<char, 100*1024>        m_Data;                     // ����� ������
    boost::optional<std::size_t>        m_ContentLength;
    std::size_t                         m_Downloaded;
    boost::system::error_code           m_CertVerifyEC;             // ��������� �������� �����������
    ProgressCallback                    m_ProgressCallback;         // ������� ��������� ����������
    DoneCallback                        m_DoneCallback;             // ������� ���������� �����
    unsigned                            m_Attempt;                  // ����� ������� ������� ����������
};

IRemoteFile::Ptr IRemoteFile::Instance(ILog& log, 
                                       const std::wstring& directory, 
                                       const std::wstring& url, 
                                       const ProgressCallback& progress /*= ProgressCallback()*/)
{
    return boost::make_shared<Impl>(log, directory, url, progress);
}

} // namespace net