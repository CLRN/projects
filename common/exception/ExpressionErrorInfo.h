#pragma once

#include <boost/exception/info.hpp>

namespace cmn
{
    //! Выражение, которое "стрельнуло"
    typedef boost::error_info<struct tag_expression, const char*> ExpressionErrorInfo;

} // namespace cmn
