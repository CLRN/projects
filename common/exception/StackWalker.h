#ifndef StackWalker_h__
#define StackWalker_h__

#include <windows.h>

namespace cmn
{
namespace utility
{


// special defines for VC5/6 (if no actual PSDK is installed):
#if _MSC_VER < 1300
typedef unsigned __int64 DWORD64, *PDWORD64;
#if defined(_WIN64)
typedef unsigned __int64 SIZE_T, *PSIZE_T;
#else
typedef unsigned long SIZE_T, *PSIZE_T;
#endif
#endif  // _MSC_VER < 1300

class StackWalkerInternal;  // forward
class StackWalker
{
public:

	struct StackWalkOptions
	{
		enum Enum
		{
			// No addition info will be retrived 
			// (only the address is available)
			RetrieveNone = 0,

			// Try to get the symbol-name
			RetrieveSymbol = 1,

			// Try to get the line for this symbol
			RetrieveLine = 2,

			// Try to retrieve the module-infos
			RetrieveModuleInfo = 4,

			// Also retrieve the version for the DLL/EXE
			RetrieveFileVersion = 8,

			// Contains all the abouve
			RetrieveVerbose = 0xF,

			// Generate a "good" symbol-search-path
			SymBuildPath = 0x10,

			// Also use the public Microsoft-Symbol-Server
			SymUseSymSrv = 0x20,

			// Contains all the abouve "Sym"-options
			SymAll = 0x30,

			// Contains all options (default)
			OptionsAll = 0x3F
		};
	};

    StackWalker
	(
		int options = StackWalkOptions::OptionsAll, 
		LPCSTR symPath = NULL, 
		DWORD processId = GetCurrentProcessId(),
        HANDLE process = GetCurrentProcess()
	);

    StackWalker(DWORD processId, HANDLE process);
    virtual ~StackWalker();

    typedef BOOL(__stdcall* PReadProcessMemoryRoutine)
	(
		HANDLE process, 
		DWORD64 baseAddress, 
		PVOID buffer,
        DWORD size, 
		LPDWORD numberOfBytesRead, 
		LPVOID userData  // optional data, which was passed in "ShowCallstack"
    );

    BOOL LoadModules();

    BOOL ShowCallstack
	(
		HANDLE thread = GetCurrentThread(), 
		CONTEXT* context = NULL,
        PReadProcessMemoryRoutine readMemoryFunction = NULL, 
		LPVOID userData = NULL  // optional to identify some data in the 'readMemoryFunction'-callback
    );

#if _MSCVER >= 1300
// due to some reasons, the "STACKWALK_MAX_NAMELEN" must be declared as "public" 
    // in older compilers in order to use it... starting with VC7 we can declare it as "protected"
    protected:
#endif
	enum { STACKWALK_MAX_NAMELEN = 2048 }; // max name length for found symbols

protected:
    // Entry for each Callstack-Entry
    struct CallStackEntry
    {
        DWORD64 m_Offset;  // if 0, we have no valid entry
        CHAR m_Name[STACKWALK_MAX_NAMELEN];
        CHAR m_UndName[STACKWALK_MAX_NAMELEN];
        CHAR m_UndFullName[STACKWALK_MAX_NAMELEN];
        DWORD64 m_OffsetFromSmybol;
        DWORD m_OffsetFromLine;
        DWORD m_LineNumber;
        CHAR m_LineFileName[STACKWALK_MAX_NAMELEN];
        DWORD m_SymType;
        LPCSTR m_SymTypeString;
        CHAR m_ModuleName[STACKWALK_MAX_NAMELEN];
        DWORD64 m_BaseOfImage;
        CHAR m_LoadedImageName[STACKWALK_MAX_NAMELEN];

		enum Enum
		{
			FirstEntry,
			NextEntry,
			LastEntry
		};

    };

    virtual void OnSymInit(LPCSTR searchPath, DWORD symOptions, LPCSTR userName);
    virtual void OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType,
        LPCSTR pdbName, ULONGLONG fileVersion);
	virtual void OnCallstackEntry(CallStackEntry::Enum type, CallStackEntry& entry);
    virtual void OnDbgHelpErr(LPCSTR funcName, DWORD gle, DWORD64 addr);
    virtual void OnOutput(LPCSTR text);

    StackWalkerInternal* m_Walker;
    HANDLE m_Process;
    DWORD m_ProcessId;
    BOOL m_IsModulesLoaded;
    LPSTR m_SymPath;

    int m_Options;

    static BOOL __stdcall myReadProcMem(HANDLE hProcess, DWORD64 qwBaseAddress, PVOID lpBuffer, DWORD nSize,
        LPDWORD lpNumberOfBytesRead);

    friend StackWalkerInternal;
};

} // namespace utility
} // namespace PT
#endif // StackWalker_h__