#pragma once

#include "common/Modules.h"
#include <ostream>
#include <boost/exception/info.hpp>

namespace cmn
{
    //! Категория логирования
    typedef boost::error_info<struct tag_log_category, Modules> LogCatErrorInfo;

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const LogCatErrorInfo& e);

} // namespace cmn
