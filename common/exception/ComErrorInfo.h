#pragma once

#include <ostream>
#include <boost/exception/info.hpp>
#include <Unknwn.h>

namespace cmn
{
    //! ����������� ���������� �� ������, ��������������� COM �������� ����� ��������� IErrorInfo
    typedef boost::error_info<struct tag_com_error, std::wstring> ComErrorInfo;

    //! �������� ���������� �� �������
    ComErrorInfo GetComErrorInfo(IUnknown* obj, REFIID riid);

    //! ������������
    std::wostream& operator << (std::wostream& stream, const ComErrorInfo& e);

} // namespace cmn
