#pragma once

#include <ostream>
#include <boost/exception/info.hpp>
#include <boost/system/error_code.hpp>

namespace cmn
{
    //! Категория логирования
    typedef boost::error_info<struct tag_system_error, boost::system::error_code> SystemErrorInfo;

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const SystemErrorInfo& e);

} // namespace cmn
