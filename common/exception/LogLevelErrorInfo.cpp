#include "exception/LogLevelErrorInfo.h"

namespace cmn
{
    std::wostream& operator << (std::wostream& stream, const LogLevelErrorInfo& e)
    {
        stream << e.value() << " (";
        switch (e.value())
        {
        case ILog::Level::Trace:
            stream << "TRACE";
            break;

        case ILog::Level::Debug:
            stream << "DEBUG";
            break;

        case ILog::Level::Info:
            stream << "INFO";
            break;

        case ILog::Level::Warning:
            stream << "WARN";
            break;

        case ILog::Level::Error:
            stream << "ERROR";
            break;

        default:
            stream << "unknown";
            break;
        }

        stream << ")";

        return stream;
    }

} // namespace cmn
