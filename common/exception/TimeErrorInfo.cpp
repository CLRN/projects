#include "exception/TimeErrorInfo.h"
#include <boost/format.hpp>
#include <windows.h>

namespace cmn
{
    namespace
    {
        inline unsigned long long FileTimeToUInt64(const FILETIME& ft)
        {
            return static_cast<unsigned long long>(ft.dwHighDateTime) << 32 | ft.dwLowDateTime;
        }

        inline FILETIME UInt64ToFileTime(unsigned long long _time)
        {
            FILETIME ft = { static_cast<DWORD>(_time), static_cast<DWORD>(_time >> 32) };
            return ft;
        }
   
    } // anonymous namespace

    TimeErrorInfo GetCurrentTimeErrorInfo()
    {
        FILETIME ft = {0};
        GetSystemTimeAsFileTime(&ft);
        
        return FileTimeToUInt64(ft);
    }

    std::wostream& operator << (std::wostream& stream, const TimeErrorInfo& e)
    {
        FILETIME systemFileTime = UInt64ToFileTime(e.value());
        FILETIME localFileTime = {0};
        SYSTEMTIME localTime = {0};
        
        if (FileTimeToLocalFileTime(&systemFileTime, &localFileTime) && FileTimeToSystemTime(&localFileTime, &localTime))
        {
            unsigned long long systemFileTime2 = FileTimeToUInt64(systemFileTime);
            unsigned long long localFileTime2 = FileTimeToUInt64(localFileTime);
            
            // ������� � ��� ������������� ����������
            long long diff = localFileTime2 >= systemFileTime2 ?
                static_cast<long long>(localFileTime2 - systemFileTime2) :
                -static_cast<long long>(systemFileTime2 - localFileTime2);

            if (diff != 0)
            {
                long diffMinutes = static_cast<long>(diff / 600000000ll);
                long diffHours = diffMinutes / 60l;
                stream << boost::wformat(L"[%04u.%02u.%02u %02u:%02u:%02u] (UTC%+03d:%02d)")
                    % localTime.wYear % localTime.wMonth % localTime.wDay
                    % localTime.wHour % localTime.wMinute % localTime.wSecond
                    % diffHours % (std::abs(diffMinutes) % 60l);
            }
            else
            {
                stream << boost::wformat(L"[%04u.%02u.%02u %02u:%02u:%02u]")
                    % localTime.wYear % localTime.wMonth % localTime.wDay
                    % localTime.wHour % localTime.wMinute % localTime.wSecond;
            }
        }
        else
            stream << e.value() << " (unable to convert filetime)";
        
        return stream;
    }

} // namespace cmn
