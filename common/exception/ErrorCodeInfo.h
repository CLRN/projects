#ifndef ErrorCodeInfo_h__
#define ErrorCodeInfo_h__

#include <boost/exception/info.hpp>
#include <boost/system/error_code.hpp>

namespace cmn
{

typedef boost::error_info<struct tag_error_code, boost::system::error_code> ErrorCodeInfo;
void ErrorCodeInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);

};// namespace n_exception

#endif // ErrorCodeInfo_h__
