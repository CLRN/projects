#pragma once

#include <sstream>
#include <boost/exception/info.hpp>

namespace cmn
{
    //! ������ ������������ �������� ::GetLastError
    typedef boost::error_info<struct tag_win32_error, unsigned long> Win32ErrorInfo;

    //! ��������� ������ ������ ���������� 
    Win32ErrorInfo LastWin32ErrorInfo();

    //! ������������
    std::wostream& operator << (std::wostream& stream, const Win32ErrorInfo& e);

} // namespace cmn
