﻿#pragma once

#include "Exception.h"
#include "ComErrorInfo.h"
#include "ExpressionErrorInfo.h"
#include "Win32ErrorInfo.h"

// Вспомогательный макрос для отладки
#define EXPRESSION_INFO(expr) << ::cmn::ExpressionErrorInfo(expr)

//! Кидает исключение xcpt если выражение expr ложно
#define CHECK(expr, xcpt, ...)																		    \
    {                                                                                                   \
        if (!(expr))												                                    \
            THROW((xcpt) EXPRESSION_INFO(#expr), __VA_ARGS__);                                          \
    }                                       

//! Кидает исключение xcpt в если err != ERROR_SUCCESS expr ложно и добавляет информацию об ошибке err
#define CHECK_WIN32(err, xcpt, ...)                                                                     \
    {                                                                                                   \
        const DWORD DECORATE_NAME(intenalError) = (err);                                                \
        if (DECORATE_NAME(intenalError) != ERROR_SUCCESS)                                               \
            THROW((xcpt) EXPRESSION_INFO(#err " == ERROR_SUCCESS")                                      \
                << cmn::Win32ErrorInfo(DECORATE_NAME(intenalError)), __VA_ARGS__);                      \
    }                                                                                                   \

//! Кидает исключение xcpt в если выражение expr ложно и добавляет информацию ::GetLastError()
#define CHECK_LE(expr, xcpt, ...)                                                                       \
    if (!(expr))                                                                                        \
	{																									\
		const DWORD lastError = ::GetLastError();                                                       \
        THROW((xcpt) EXPRESSION_INFO(#expr)                                                             \
            << cmn::Win32ErrorInfo(lastError), __VA_ARGS__);                                            \
	}

//! Выкидывает исключение xcpt если истино выражение FAILED(expr)
#define CHECK_HRESULT(expr, xcpt, ...)                                                                  \
    {                                                                                                   \
        const HRESULT DECORATE_NAME(internalHresult) = (expr);                                          \
        if (FAILED(DECORATE_NAME(internalHresult)))                                                     \
            THROW((xcpt) EXPRESSION_INFO("SUCCEEDED(" #expr ")")                                        \
                << cmn::HResultErrorInfo(DECORATE_NAME(internalHresult)), __VA_ARGS__);                 \
    }                                                                                                   \

//! Выкидывает исключение xcpt если истино выражение FAILED(expr) и получает расширенную ошибку
//! через интерфейс IErrorInfo объекта obj
#define CHECK_COM_ERROR(expr, obj, xcpt, ...)                                                           \
    {                                                                                                   \
        const HRESULT DECORATE_NAME(internalHresult) = (expr);                                          \
        if (FAILED(DECORATE_NAME(internalHresult)))                                                     \
        {                                                                                               \
            THROW((xcpt) EXPRESSION_INFO("SUCCEEDED(" #expr ")")                                        \
                << cmn::HResultErrorInfo(DECORATE_NAME(internalHresult))                                \
                << cmn::GetComErrorInfo(obj, __uuidof(obj)), __VA_ARGS__);                              \
        }                                                                                               \
    }                                                                                                   \


#define CATCH_AND_PASS_TYPED(xcpt, type, ...)                                                           \
    catch (const type&)                                                                                 \
    {                                                                                                   \
        THROW((xcpt)                                                                                    \
            << NESTED_CURRENT_EXCEPTION(), __VA_ARGS__);                                                \
    }

//! Ловит исключение наследуемое от boost::exception или std::exception,
//! создает новое исключение, добавляет переданные параметры, добавляет пойманное
//! исключение как вложенное и кидает новое исключение
#define CATCH_PASS(xcpt, ...)                                                                           \
    CATCH_AND_PASS_TYPED(xcpt, ::boost::exception, __VA_ARGS__)                                         \
    CATCH_AND_PASS_TYPED(xcpt, ::std::exception, __VA_ARGS__)

