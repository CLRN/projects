﻿#include "exception/Exception.h"
#include <assert.h>
#include <sstream>
#include <boost/exception/get_error_info.hpp>
#include <boost/exception/errinfo_nested_exception.hpp>
#ifndef BOOST_NO_RTTI
#include <boost/units/detail/utility.hpp>
#endif
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/shared_lock_guard.hpp>
#pragma warning(push)
#pragma warning(disable:4244) // 'argument' : conversion from 'boost::locale::utf::code_point' to 'const wchar_t', possible loss of data
#include <boost/locale/encoding.hpp>
#pragma warning(pop)

#include <list>

#include "exception/TimeErrorInfo.h"
#include "exception/LogLevelErrorInfo.h"
#include "exception/ExpressionErrorInfo.h"
#include "exception/LogCatErrorInfo.h"
#include "exception/Win32ErrorInfo.h"
#include "exception/HresultErrorInfo.h"
#include "exception/ParamsErrorInfo.h"
#include "exception/ComErrorInfo.h"
#include "exception/SystemErrorInfo.h"
#include "common/Modules.h"

namespace cmn
{

namespace
{
    struct Indent
    {
        Indent(unsigned value) : m_Value(value) {}
        unsigned m_Value;
    };

    std::wostream& operator << (std::wostream& stream, const Indent& indent)
    {
        unsigned level = indent.m_Value;
        do
        {
            static const wchar_t indent[] = L"  ";
            stream.write(indent, _countof(indent) - 1);
        }
        while (level--);
        
        return stream;        
    }

#ifndef BOOST_NO_RTTI
    void DumpRttiInfo(std::wostream& os, unsigned indentLevel, const char* info);
#endif
} // anonymous namespace

// Visitor'ы
namespace ExceptionInfoVisitors
{
namespace
{
    class Visitors
    {
    public:
        Visitors();

        void Visit(std::wostream& os, const std::exception* se, const boost::exception* be, unsigned indentLevel, unsigned flags);
        void VisitInternal(std::wostream& os, const std::exception* se, const boost::exception* be, unsigned indentLevel, unsigned flags);
        void Register(const ExceptionInfoVisitor& visitor)
        {
            UniqueLock lock(m_Mutex);
            m_Visitors.push_back(visitor);
        }

    private:
        void RegisterDefaultVisitors();

    private:
        typedef std::list<ExceptionInfoVisitor> VisitorsSequence;
        VisitorsSequence m_Visitors;
        ExceptionInfoVisitor m_NestedExceptionVisitor; // Отдельный visitor, выполняющийся после всех остальных
        boost::shared_mutex m_Mutex;
        typedef boost::unique_lock<boost::shared_mutex> UniqueLock;
        typedef boost::shared_lock<boost::shared_mutex> SharedLock;
    };

    Visitors::Visitors()
    {
        RegisterDefaultVisitors();
    }

    void Visitors::Visit(std::wostream& os, const std::exception* se, const boost::exception* be, unsigned indentLevel, unsigned flags)
    {
        SharedLock lock(m_Mutex);
        VisitInternal(os, se, be, indentLevel, flags);
    }

    void Visitors::VisitInternal(std::wostream& os, const std::exception* se, const boost::exception* be, unsigned indentLevel, unsigned flags)
    {
        // std::exception::what
        if (se)
        {
            try
            {
                os << boost::locale::conv::to_utf<wchar_t>(se->what(), "utf8", boost::locale::conv::stop);
            }
            catch (const std::exception&)
            {
                os << boost::locale::conv::to_utf<wchar_t>(se->what(), "cp1251");
            }            
        }
        else
        {
            os << "error text is undefined";
        }

        // Если boost::exception недоступен, выводим информацию о типе std::exception и выходим
        if (!be)
        {
#ifndef BOOST_NO_RTTI
            DumpRttiInfo(os, indentLevel, (BOOST_EXCEPTION_DYNAMIC_TYPEID(*se)).type_->name());
#endif
            return;
        }

        // Visit
        for (VisitorsSequence::iterator i = m_Visitors.begin(), end = m_Visitors.end(); i != end; ++i)
            (*i)(os, *be, indentLevel, flags);

        // Nested exception
        m_NestedExceptionVisitor(os, *be, indentLevel, flags);
    }

    Visitors& GetVisitors()
    {
        static boost::mutex instanceMutex;
        static std::auto_ptr<Visitors> instance;
        if (!instance.get())
        {
            boost::mutex::scoped_lock lock(instanceMutex);
            if (!instance.get())
                instance.reset(new Visitors());
        }
        return *instance;
    }

    // Default visitors
    void TimeVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void LevelVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags);
    void ExpressionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void FileVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void FunctionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);

#ifndef BOOST_NO_RTTI
    void TypeInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
#endif // BOOST_NO_RTTI

    void CategoryVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags);
    void Win32ErrorVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void HResultVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void IErrorInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void ParamsVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned);
    void NestedExceptionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags);
    void BoostSystemErrorVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags);

    void Visitors::RegisterDefaultVisitors()
    {
        UniqueLock lock(m_Mutex);

        m_NestedExceptionVisitor = NestedExceptionVisitor;

        // В таком порядке будут обходиться visitor'ы
        m_Visitors.push_back(TimeVisitor);
        m_Visitors.push_back(LevelVisitor);
        m_Visitors.push_back(ExpressionVisitor);
        m_Visitors.push_back(FileVisitor);
        m_Visitors.push_back(FunctionVisitor);

#ifndef BOOST_NO_RTTI
        m_Visitors.push_back(TypeInfoVisitor);
#endif // BOOST_NO_RTTI
        m_Visitors.push_back(CategoryVisitor);
        m_Visitors.push_back(Win32ErrorVisitor);
        m_Visitors.push_back(HResultVisitor);
        m_Visitors.push_back(IErrorInfoVisitor);
        m_Visitors.push_back(ParamsVisitor);
        m_Visitors.push_back(BoostSystemErrorVisitor);
    }

    // Default visitors implementations
    void TimeVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [Time] = [2012.11.01 20:03:53]
        if (const unsigned long long* time = boost::get_error_info<TimeErrorInfo>(be))
            MakeIndent(os, indentLevel) << "[Time] = " << TimeErrorInfo(*time);
    }

    void LevelVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags)
    {
        // [Level] = 20000 (INFO)
        if (!(flags & ExceptionInfoDetails::SkipTopLevelSeverity))
        {
            if (const ILog::Level::Value* level = boost::get_error_info<LogLevelErrorInfo>(be))
                MakeIndent(os, indentLevel) << "[Level] = " << LogLevelErrorInfo(*level);
        }
    }

    void ExpressionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [Expression]
        if (const char* const* expression = boost::get_error_info<ExpressionErrorInfo>(be))
            MakeIndent(os, indentLevel) << "[Expression] = " << *expression;
    }

    void FileVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [File]
        if (const char* const* file = boost::get_error_info<boost::throw_file>(be))
        {
            MakeIndent(os, indentLevel) << "[File] = " << *file;
            if (const int* line = boost::get_error_info<boost::throw_line>(be))
                os << "(" << *line << ")";
        }
    }

    void FunctionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [Function]
        if (const char* const* function = boost::get_error_info<boost::throw_function>(be))
            MakeIndent(os, indentLevel) << "[Function] = " << *function;
    }

#ifndef BOOST_NO_RTTI
    void TypeInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [RttiInfo]
        DumpRttiInfo(os, indentLevel, (BOOST_EXCEPTION_DYNAMIC_TYPEID(be)).type_->name());
    }
#endif // BOOST_NO_RTTI

    void CategoryVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags)
    {
        // [Category]
        if (!(flags & ExceptionInfoDetails::SkipTopLevelCategory))
        {
            if (const Modules* category = boost::get_error_info<LogCatErrorInfo>(be))
                MakeIndent(os, indentLevel) << "[Category] = " << LogCatErrorInfo(*category);
        }
    }

    void Win32ErrorVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [Win32 error] = 10053 (An established connection was aborted by the software in your host machine)
        if (const unsigned long* win32 = boost::get_error_info<Win32ErrorInfo>(be))
            MakeIndent(os, indentLevel) << "[Win32 error] = " << Win32ErrorInfo(*win32);
    }

    void HResultVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [HRESULT] = 0x80070005 (Access is denied)
        if (const HRESULT* hresult = boost::get_error_info<HResultErrorInfo>(be))
            MakeIndent(os, indentLevel) << "[HRESULT] = " << HResultErrorInfo(*hresult);
    }

    void IErrorInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [IErrorInfo]
        if (const std::wstring* comerror = boost::get_error_info<ComErrorInfo>(be))
            if (!comerror->empty())
                MakeIndent(os, indentLevel) << "[IErrorInfo] = " << *comerror;
    }

    void ParamsVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
    {
        // [Params] = Reason: 1 (Network failure), Session: [In, 127.0.0.1:44112, GUID: e8c346d4-9687-44bf-b6b2-dbdbd59de05d, Local user: [2] PTSECURITY\vgordeychik, Destroy, Peer: [Console, Rev/Bld/Prot=24078071/0.0.0.0/3.0, ExtMsg: Supported]]
        if (const exception_detail::ParamsMap* params = boost::get_error_info<ParamsErrorInfo>(be))
            if (!params->empty())
                MakeIndent(os, indentLevel) << "[Params] = " << ParamsErrorInfo(*params);
    }

    void NestedExceptionVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags)
    {
        // [Nested error] = Failed to read data.
        if (const boost::exception_ptr* nestedException = boost::get_error_info<boost::errinfo_nested_exception>(be))
        {
            MakeIndent(os, indentLevel) << "[Nested error] = ";

            try
            {
                boost::rethrow_exception(*nestedException);
            }
            catch (const Exception& e)
            {
                GetVisitors().VisitInternal(os, &e, &e, indentLevel + 1, flags);
            }
            catch (const boost::exception& be)
            {
                GetVisitors().VisitInternal(os, dynamic_cast<const std::exception*>(&be), &be, indentLevel + 1, flags);
            }
            catch (const std::exception& se)
            {
                GetVisitors().VisitInternal(os, &se, NULL, indentLevel + 1, flags);
            }
            catch (...)
            {
                assert(0);
            }

        }
    }

    void BoostSystemErrorVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags)
    {
        if (const auto* error = dynamic_cast<const boost::system::system_error*>(&be))
            MakeIndent(os, indentLevel) << "[System] = " << SystemErrorInfo(error->code());
    }

} // anonymous namespace

    std::wostream& MakeIndent(std::wostream& os, unsigned indentLevel)
    {
        return os << std::endl << Indent(indentLevel);
    }

    void RegisterExceptionInfoVisitor(const ExceptionInfoVisitor& visitor)
    {
        GetVisitors().Register(visitor);
    }

} // namespace ExceptionInfoVisitors

namespace
{

#ifndef BOOST_NO_RTTI
    void DumpRttiInfo(std::wostream& os, unsigned indentLevel, const char* info)
    {
        if (info)
            ExceptionInfoVisitors::MakeIndent(os, indentLevel) << "[RttiInfo] = " << info;
    }
#endif

} // anonymous namespace

namespace exception_detail
{

CreateParamsHelper::CreateParamsHelper()
{
}

CreateParamsHelper::CreateParamsHelper(const boost::exception& source)
{
    if (const exception_detail::ParamsMap* params = boost::get_error_info<ParamsErrorInfo>(source))
        m_Creator.Set(*params);
}

std::wstring ExceptionInfoImpl(const std::exception* se, const boost::exception* be, unsigned flags)
{
    std::wostringstream ss;
    ExceptionInfoVisitors::GetVisitors().Visit(ss, se, be, 0, flags);
    return ss.str();
}
} // namespace exception_detail


Exception::Exception(const char* msg) : m_Message(msg)
{
}

Exception::Exception(const wchar_t* msg) : m_Message(boost::locale::conv::utf_to_utf<char>(msg))
{
}

Exception::Exception(const std::string& msg) : m_Message(msg)
{
}

Exception::Exception(const std::wstring& msg) : m_Message(boost::locale::conv::utf_to_utf<char>(msg))
{
}

} // namespace cmn
