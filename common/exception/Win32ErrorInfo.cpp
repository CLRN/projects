#include "exception/Win32ErrorInfo.h"
#include "exception/ParamsErrorInfo.h"

#include <windows.h>

namespace cmn
{
    std::wostream& operator << (std::wostream& stream, const Win32ErrorInfo& e)
    {
        stream << exception_detail::GetWin32ErrorDescription(e.value());
        return stream;
    }

    Win32ErrorInfo LastWin32ErrorInfo()
    {
        return Win32ErrorInfo(::GetLastError());
    }

} // namespace cmn