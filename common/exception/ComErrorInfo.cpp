#include "exception/ComErrorInfo.h"
#include <atlcomcli.h>

namespace cmn
{
    ComErrorInfo GetComErrorInfo(IUnknown* obj, REFIID riid)
    {
        if (obj)
        {
            CComQIPtr<ISupportErrorInfo> sup(obj);
            if (sup)
            {
                if (sup->InterfaceSupportsErrorInfo(riid) == S_OK)
                {
                    CComPtr<IErrorInfo> errorInfo;
                    if (GetErrorInfo(0, &errorInfo) == S_OK)
                    {
                        CComBSTR description;
                        if (errorInfo->GetDescription(&description) == S_OK)
                        {
                            return ComErrorInfo((BSTR)description);
                        }
                    }
                }
            }
        }

        return std::wstring();
    }

    std::wostream& operator << (std::wostream& stream, const ComErrorInfo& e)
    {
        stream << e.value();
        return stream;
    }

} // namespace cmn

