#include "exception/SystemErrorInfo.h"
#include "conversion/AnyCast.h"

namespace cmn
{
    std::wostream& operator << (std::wostream& stream, const SystemErrorInfo& e)
    {
        stream << e.value().value() << " (" << conv::cast<std::wstring, conv::Ansi>(e.value().message()) << ")";
        return stream;
    }

} // namespace cmn
