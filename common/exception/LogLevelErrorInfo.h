#pragma once

#include "common/ILog.h"
#include <ostream>
#include <boost/exception/info.hpp>

namespace cmn
{
    //! Категория логирования
    typedef boost::error_info<struct tag_log_category, ILog::Level::Value> LogLevelErrorInfo;

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const LogLevelErrorInfo& e);

} // namespace cmn
