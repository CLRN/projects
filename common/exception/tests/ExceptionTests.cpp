#include "gtest/gtest.h"

#pragma warning(disable:4503) // warning C4503: 'cmn::exception_detail::CreateParamsHelper::Phase2' : decorated name length exceeded, name was truncated
#define EXPEXCEPTION_MAX_PARAMETERS 20

#include "exception/Exception.h"
#include "exception/CheckHelpers.h"
#include "exception/ParamsErrorInfo.h"
#include "exception/Win32ErrorInfo.h"
#include "exception/HresultErrorInfo.h"
#include "exception/LogCatErrorInfo.h"
#include "exception/TimeErrorInfo.h"
#include "exception/LogLevelErrorInfo.h"
#include "exception/ComErrorInfo.h"

#include <ostream>
#include <vector>
#include <list>
#include <utility>
#include <atlcomcli.h>

TEST(Exception, Unknown)
{
    class CustomException : public std::exception
    {
    public:
        CustomException() : std::exception("custom text") {}
    };

    try
    {
        try
        {
            throw CustomException();
        }
        catch (const std::exception&)
        {
            THROW(cmn::Exception("fail") << NESTED_CURRENT_EXCEPTION());
        }
    }
    catch (const std::exception& e)
    {
        const auto text = cmn::ExceptionInfo(e);
        EXPECT_TRUE(text.find(L"Unknown exception") == std::string::npos);
    }
}


void do_throw()
{
    int _int = 1234;
    long _long = 334789012;
    float _float = 123.55f;
    double _double = -23479803958.2342;
    const char* _char = "asdasdasdasd";
    const wchar_t* _wchar = L"sd;hlgsudvjnm;";
    const std::string _str = "asdasdasdasd";
    const std::wstring _wstr = L"sd;hlgsudvjnm;";

    std::vector<float> vec1;
    vec1.push_back(123.43f);
    vec1.push_back(-162.63432f);

    std::vector<std::string> vec2;
    vec2.push_back("1223");
    vec2.push_back("ercd");
    vec2.push_back("sghgsdfwe");

    std::deque<long> deque1;
    deque1.push_back(456378);
    deque1.push_back(-63728);
    deque1.push_back(9898);
    deque1.push_back(0);

    std::deque<std::wstring> deque2;
    deque2.push_back(L"wtyduij");
    deque2.push_back(L"bscvjn");
    deque2.push_back(L"sdjkghsdfkuh");
    deque2.push_back(L"72vnwyeq");

    std::list<std::string> list1;
    list1.push_back("1231");
    list1.push_back("adfe");

    std::list<std::wstring> list2;
    list2.push_back(L"1223");
    list2.push_back(L"ercd");
    list2.push_back(L"sghgsdfwe");
    list2.push_back(L"dfghjkl");

    std::pair<double, std::wstring> pair1 = std::make_pair(5542323.234223, L"asdasdasd");
    std::pair<std::wstring, std::string> pair2 = std::make_pair(L"qwerqw", "sfsjkl;as");

    std::map<int, std::wstring> map1;
    map1[12] = L"1241231231";
    map1[341234] = L"1241231231";

    std::map<std::string, double> map2;
    map2["qwerq"] = 34782.22;
    map2["elrlgjwjek"] = -12431282.2;

    std::multimap<int, std::wstring> mmap1;
    mmap1.insert(std::make_pair(12, L"1241231231"));
    mmap1.insert(std::make_pair(12, L"1241231231asda"));
    mmap1.insert(std::make_pair(341234, L"1241231231"));


    std::multimap<std::string, double> mmap2;
    mmap2.insert(std::make_pair("qwerq", 34782.22));
    mmap2.insert(std::make_pair("elrlgjwjek", -12431282.2));
    mmap2.insert(std::make_pair("elrlgjwjek", -12431282.2));
    mmap2.insert(std::make_pair("elrlgjwjek", -1282.8));


    THROW(
        cmn::Exception("INNNNER"),
        _int, _long, _float, _double, _char, _wchar, _str, _wstr,
        vec1, vec2, deque1, deque2, list1, list2, pair1, pair2, map1, map2, mmap1, mmap2
        );
}

void func5()
{
    try
    {
        do_throw();
    }
    catch(const cmn::Exception& /*xcpt*/)
    {
        // throw new exception
        THROW(cmn::Exception("FUNC5") << NESTED_CURRENT_EXCEPTION());
    }
}

void func4()
{
    try
    {
        func5();
    }
    catch(const cmn::Exception& /*xcpt*/)
    {
        // throw new exception
        THROW(cmn::Exception("FUNC4") << NESTED_CURRENT_EXCEPTION());
    }
}

void func3()
{
    try
    {
        func4();
    }
    catch(const cmn::Exception& /*xcpt*/)
    {
        // throw new exception
        THROW(cmn::Exception("FUNC3") << NESTED_CURRENT_EXCEPTION());
    }
}



void func2()
{
    try
    {
        func3();
    }
    catch(const cmn::Exception& /*xcpt*/)
    {
        // throw new exception
        THROW(cmn::Exception("FUNC2") << NESTED_CURRENT_EXCEPTION());
    }
}

void func1()
{
    try
    {
        func2();
    }
    catch(cmn::Exception& /*xcpt*/)
    {
        // throw new exception
        CHECK(!"false expression", cmn::Exception("FUNC1") << NESTED_CURRENT_EXCEPTION()
            << cmn::HResultErrorInfo(E_ACCESSDENIED)
            << cmn::LogCatErrorInfo(Modules::DataBase)
            << cmn::Win32ErrorInfo(85)
            << cmn::GetCurrentTimeErrorInfo()
            << cmn::LogLevelErrorInfo(ILog::Level::Warning)
            );
    }
}

TEST(Exception, Check)
{
    ASSERT_THROW(
        CHECK(false, cmn::Exception("exception")),
        cmn::Exception
        );

    try
    {
        func1();
    }
    catch (const cmn::Exception& xcpt)
    {
        std::wcout << cmn::ExceptionInfo(xcpt, 0) << std::endl;
    }
}

void wrapStdExceptFunc()
{
    try
    {
        throw std::runtime_error("test runtime exception");
    }
    catch (std::exception e)
    {
        THROW(e);
    }
}

TEST(Exception, StdThrowCheck)
{
    ASSERT_THROW(wrapStdExceptFunc(), boost::exception);
    try
    {
        throw std::logic_error("some logic error");
    }
    catch (const std::exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
    
}

void TestNoThrow()
{
    try
    {
        try
        {
            throw std::invalid_argument("first argument is invalid");
        }
        catch (const std::exception& /*e*/)
        {
            // const char* w = cmn::ExceptionInfo(e);
            THROW(cmn::Exception("Function failed") << NESTED_CURRENT_EXCEPTION());
        }
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
}

TEST(Exception, IncapsulateStdException)
{
    ASSERT_NO_THROW(TestNoThrow());
}

TEST(Exception, ExpChk)
{
    ASSERT_NO_THROW(CHECK(true, cmn::Exception("ExpChk test")));
    ASSERT_THROW(CHECK(false, cmn::Exception("ExpChk test")), cmn::Exception);

    try
    {
        CHECK(false, cmn::Exception("ExpChk test"));
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
    
    
}

TEST(Exception, ExpChkWin32Err)
{
    ASSERT_NO_THROW(CHECK_WIN32(ERROR_SUCCESS, cmn::Exception("ExpChkWin32Err test")));
    ASSERT_THROW(CHECK_WIN32(ERROR_ACCESS_DENIED, cmn::Exception("ExpChkWin32Err test")), cmn::Exception);

    try
    {
        CHECK_WIN32(ERROR_ACCESS_DENIED, cmn::Exception("ExpChkWin32Err test"));
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
}

bool TestFuncSetLastError(DWORD err)
{
    SetLastError(err);
    return !err;
}

TEST(Exception, ExpChkLastError)
{
    ASSERT_NO_THROW(CHECK_LE(TestFuncSetLastError(0), cmn::Exception("ExpChkLastError test")));
    ASSERT_THROW(CHECK_LE(TestFuncSetLastError(1), cmn::Exception("ExpChkLastError test")), cmn::Exception);

    try
    {
        CHECK_LE(TestFuncSetLastError(1), cmn::Exception("ExpChkLastError test"));
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
}

TEST(Exception, ExpChkHresult)
{
    ASSERT_NO_THROW(CHECK_HRESULT(NOERROR, cmn::Exception("ExpChkHresult test")));
    ASSERT_THROW(CHECK_HRESULT(E_PENDING, cmn::Exception("ExpChkHresult test")), cmn::Exception);

    try
    {
        CHECK_HRESULT(E_PENDING, cmn::Exception("ExpChkHresult test"));
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
}


MIDL_INTERFACE("ECCF6173-C26B-4835-97FA-47B1E918DE11")
IMyInterface : public ISupportErrorInfo
{
    virtual HRESULT STDMETHODCALLTYPE DoSomething() = 0;
};

class MyStub : public IMyInterface
{
public:
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppvObject) override
    {
        if (!ppvObject)
            return E_POINTER;

        if (riid == __uuidof(IUnknown) || riid == __uuidof(ISupportErrorInfo))
            return *ppvObject = this, AddRef(), S_OK;

        return E_NOINTERFACE;
    }

    virtual ULONG STDMETHODCALLTYPE AddRef() override
    {
        return 2;
    }

    virtual ULONG STDMETHODCALLTYPE Release() override
    {
        return 1;
    }

    virtual HRESULT STDMETHODCALLTYPE InterfaceSupportsErrorInfo(REFIID riid)
    {
        return (riid == __uuidof(IMyInterface)) ? S_OK : S_FALSE;
    }

    HRESULT STDMETHODCALLTYPE DoSomething()
    {
        HRESULT hr;
        CComPtr<ICreateErrorInfo> error;
        hr = CreateErrorInfo(&error);
        if (FAILED(hr))
            return hr;

        error->SetGUID(__uuidof(IMyInterface));
        error->SetSource(L"source location goes here");
        error->SetDescription(L"error description goes here");
        error->SetHelpFile(L"d:\\docs\\help.chm");
        error->SetHelpContext(123);

        CComQIPtr<IErrorInfo> err(error);
        if (!err)
            return E_UNEXPECTED;

        SetErrorInfo(0, err);
        return E_PENDING;
    }
};

TEST(Exception, ExpChkComError)
{
    MyStub stub;
    IMyInterface* istub = &stub;
    ASSERT_THROW(CHECK_COM_ERROR(istub->DoSomething(), istub, cmn::Exception("unable to do something")), cmn::Exception);

    try
    {
        CHECK_COM_ERROR(istub->DoSomething(), istub, cmn::Exception("unable to do something"));
    }
    catch (const cmn::Exception& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
}

class SpecificException : public cmn::Exception
{
public:
    SpecificException() : Exception("special exception") {}
};

class RootException : public cmn::Exception
{
public:
    RootException() : Exception("root exception") {}
};

void TestThrowStdError()
{
    THROW(std::logic_error("some logic error issued here"));
}

void TestThrowSpecificError()
{
    THROW(SpecificException());
}

__declspec(noreturn) void TestThrowOneOf(bool useStandartException)
{
    const char* ansi = "papapapap";
    const wchar_t* wide = L"w;fvbhadjkvma";
    try
    {
        if (useStandartException)
            TestThrowStdError();
        else
            TestThrowSpecificError();
    }
    CATCH_PASS(RootException(), useStandartException, ansi, wide);
}

TEST(Exception, ExpCatchAndPass)
{
    ASSERT_THROW(TestThrowOneOf(false), RootException);
    ASSERT_THROW(TestThrowOneOf(true), RootException);

    try
    {
        TestThrowOneOf(false);
    }
    catch (const RootException& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }

    try
    {
        TestThrowOneOf(true);
    }
    catch (const RootException& e)
    {
        std::wcout << cmn::ExceptionInfo(e, 0) << std::endl;
    }
    
}

TEST(Exception, IsTuple)
{
    EXPECT_TRUE(PARAMSERRORINFO_IS_TUPLE((param1, "paramName")));
    EXPECT_TRUE(PARAMSERRORINFO_IS_TUPLE(((*it)->getSmth(), "paramName")));
    EXPECT_TRUE(PARAMSERRORINFO_IS_TUPLE(("paramName")));
    EXPECT_FALSE(PARAMSERRORINFO_IS_TUPLE(smth));
    EXPECT_FALSE(PARAMSERRORINFO_IS_TUPLE((*iter).smth));
    EXPECT_FALSE(PARAMSERRORINFO_IS_TUPLE((*iter)->getSmth()));
    EXPECT_FALSE(PARAMSERRORINFO_IS_TUPLE(smth()));
    EXPECT_FALSE(PARAMSERRORINFO_IS_TUPLE(smth));
}
