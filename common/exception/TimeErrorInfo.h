#pragma once

#include <ostream>
#include <boost/exception/info.hpp>

namespace cmn
{
    //! ����� ������������� ����������
    typedef boost::error_info<struct tag_expression, unsigned long long> TimeErrorInfo;

    //! �������� ������� �����
    TimeErrorInfo GetCurrentTimeErrorInfo();

    //! ������������
    std::wostream& operator << (std::wostream& stream, const TimeErrorInfo& e);

} // namespace cmn
