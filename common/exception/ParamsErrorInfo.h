#pragma once

#include <ostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <list>
#include <map>
#include <set>

#include <boost/exception/info.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/type_traits/is_convertible.hpp>
#pragma warning(push)
#pragma warning(disable:4244) // 'argument' : conversion from 'boost::locale::utf::code_point' to 'const wchar_t', possible loss of data
#include <boost/locale/encoding.hpp>
#pragma warning(pop)

namespace cmn
{
    namespace exception_detail
    {
        struct ErrorDescriptionFlags
        {
            enum Enum
            {
                // Будет написано примерно так: "2 (Не удается найти указанный файл.)". Иначе - "Не удается найти указанный файл."
                WriteIntegerValue   = 1 << 0,

                // Если ErrorDescriptionFlags::WriteIntegerValue, то код ошибки в хексе
                HexValue            = 1 << 1,
            };
        };

        std::wstring GetWin32ErrorDescription(unsigned long win32Err, unsigned long flags = ErrorDescriptionFlags::WriteIntegerValue);

        typedef std::map<std::string, std::wstring> ParamsMap;

        struct ParamDescriptor
        {
        private:
            template<typename T, bool isStringable>
            struct Cast;

            template<typename T>
            struct Cast<T, false>
            {
                static std::wstring Do(const T& v)
                {
                    try
                    {
                        return boost::lexical_cast<std::wstring>(v);
                    }
                    catch (const boost::bad_lexical_cast& e) 
                    {
                        return std::wstring(L"cast failed: " + boost::locale::conv::utf_to_utf<wchar_t>(e.what()));
                    }
                }
            };

            template<typename T>
            struct Cast<T, true>
            {
                template<bool isWide>
                struct WideStub;

                template<>
                struct WideStub<false>
                {
                    static std::wstring Do(const std::string& v, const std::wstring& delimeter)
                    {
                        static const std::wstring prefix(L"u8");
                        return prefix + delimeter + boost::locale::conv::utf_to_utf<wchar_t>(v) + delimeter;
                    }
                };

                template<>
                struct WideStub<true>
                {
                    static std::wstring Do(const std::wstring& v, const std::wstring& delimeter)
                    {
                        static const std::wstring prefix(L"u16");
                        return prefix + delimeter + v + delimeter;
                    }
                };

                static std::wstring Do(const T& v)
                {
                    static const std::wstring delimeter(L"'");
                    return WideStub<CheckWideType<T>::value>::Do(v, delimeter);
                }
            };

            template<typename T>
            static std::wstring DoCast(const T& val)
            {
                return Cast<T, CheckStringableType<T>::value>::Do(val);
            }

            template<>
            static std::wstring DoCast(const bool& val)
            {
                return val ? L"true" : L"false";
            }

            template<typename T1, typename T2>
            static std::wstring DoCast(const std::pair<T1, T2>& val)
            {
                std::wostringstream ss;
                ss << L"<" << DoCast(val.first) << L"->" << DoCast(val.second) << L">";
                return ss.str();
            }

            template<typename T>
            struct CheckStringableType
            {
                enum
                {
                    value = boost::is_convertible<T, const char*>::value != 0
                        || boost::is_convertible<T, const wchar_t*>::value != 0
                        || boost::is_convertible<T, const std::string&>::value != 0
                        || boost::is_convertible<T, const std::wstring&>::value != 0
                };
            };

            template<typename T>
            struct CheckWideType
            {
                enum
                {
                    value = boost::is_convertible<T, const wchar_t*>::value != 0
                        || boost::is_convertible<T, const std::wstring&>::value != 0
                };
            };

            template<typename C>
            static std::wstring ContainerToString(const C& container, char openDelimeter, char closeDelimeter)
            {
                std::wostringstream ss;
                ss << openDelimeter;
                bool first = true;
                C::const_iterator it = container.begin();
                C::const_iterator itEnd = container.end();
                while (it != itEnd)
                {
                    if (!first)
                        ss << L",";
                    ss << DoCast(*it);

                    it++;
                    first = false;
                }
                ss << closeDelimeter;
                return ss.str();
            }

            template<typename C>
            static std::wstring AssocContainerToString(const C& container, char openDelimeter, char closeDelimeter)
            {
                std::wostringstream ss;
                ss << openDelimeter;
                bool first = true;
                C::const_iterator it = container.begin();
                C::const_iterator itEnd = container.end();
                while (it != itEnd)
                {
                    if (!first)
                        ss << L",";
                    ss << DoCast(*it);

                    it++;
                    first = false;
                }
                ss << closeDelimeter;
                return ss.str();
            }

            


        public:
            template<typename T>
            explicit ParamDescriptor(const std::string& name, const T& val)
                : m_Name(name)
                , m_Value(DoCast(val))
            {
            }

            template<typename T, typename A>
            explicit ParamDescriptor(const std::string& name, const std::vector<T, A>& val)
                : m_Name(name)
                , m_Value(ContainerToString(val, '[', ']'))
            {
            }

            template<typename T, typename A>
            explicit ParamDescriptor(const std::string& name, const std::deque<T, A>& val)
                : m_Name(name)
                , m_Value(ContainerToString(val, '[', ']'))
            {
            }

            template<typename T, typename A>
            explicit ParamDescriptor(const std::string& name, const std::list<T, A>& val)
                : m_Name(name)
                , m_Value(ContainerToString(val, '[', ']'))
            {
            }

            template<typename T, typename P, typename A>
            explicit ParamDescriptor(const std::string& name, const std::map<T, P, A>& val)
                : m_Name(name)
                , m_Value(AssocContainerToString(val, '{', '}'))
            {
            }

            template<typename T, typename P, typename A>
            explicit ParamDescriptor(const std::string& name, const std::multimap<T, P, A>& val)
                : m_Name(name)
                , m_Value(AssocContainerToString(val, '{', '}'))
            {
            }

            template<typename T, typename P, typename A>
            explicit ParamDescriptor(const std::string& name, const std::set<T, P, A>& val)
                : m_Name(name)
                , m_Value(AssocContainerToString(val, '{', '}'))
            {
            }

            template<typename T, typename P, typename A>
            explicit ParamDescriptor(const std::string& name, const std::multiset<T, P, A>& val)
                : m_Name(name)
                , m_Value(AssocContainerToString(val, '{', '}'))
            {
            }

            std::string m_Name;
            std::wstring m_Value;
        };
    }

    //! Ошибка возвращаемая функцией ::GetLastError
    typedef boost::error_info<struct tag_params, exception_detail::ParamsMap> ParamsErrorInfo;
    
//     class ParamsCreator
//     {
//     public:
//         //! Используется если создаем список с нуля
//         ParamsCreator();
//         //! Используется если к текущему исключению нужно добавить параметры не перетирая старые
//         ParamsCreator(const boost::exception& src);
//         ParamsCreator& operator %(const exception_detail::ParamDescriptor& t);
//         ParamsErrorInfo Value() const;
// 
//     private:
//         exception_detail::ParamsMap m_Params;
//     };
    
    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const ParamsErrorInfo& e);

    //! Вспомогательная функция вытаскивающая параметр и сравнивающая на равенство
    bool ExtractNamedParam(const boost::exception& xcpt, const std::string& name, std::wstring& value);

    template<typename T>
    bool ExtractNamedParam(const boost::exception& xcpt, const std::string& name, T& value)
    {
        std::wstring val;
        if (!ExtractNamedParam(xcpt, name, val))
            return false;

        try
        {
            return value = boost::lexical_cast<T>(val), true;
        }
        catch (const boost::bad_lexical_cast&)
        {
        }

        return false;
    }

    template<typename T>
    bool ExtractNamedParamAndCompare(const boost::exception& xcpt, const std::string& name, const T& value)
    {
        T val;
        return ExtractNamedParam(xcpt, name, val) && val == value;
    }

} // namespace cmn
