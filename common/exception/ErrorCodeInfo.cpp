#include "ErrorCodeInfo.h"
#include "conversion/AnyCast.h"
#include "Exception.h"

#include <boost/function.hpp>

namespace cmn
{
void ErrorCodeInfoVisitor(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned)
{
    if (const boost::system::error_code* erroInfo = boost::get_error_info<ErrorCodeInfo>(be))
    {
        cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel) << "[Error code] = " << erroInfo->value();
        cmn::ExceptionInfoVisitors::MakeIndent(os, indentLevel + 1) << "Message = " << conv::cast<std::wstring, conv::Ansi>(erroInfo->message());
    }
}

DECLARE_EXCEPTION_INFO_VISITOR(&ErrorCodeInfoVisitor);

} // namespace cmn


