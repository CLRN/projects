#include "exception/ParamsErrorInfo.h"
#include <boost/exception/get_error_info.hpp>

#include <Windows.h>

namespace cmn
{

std::wostream& operator << (std::wostream& stream, const ParamsErrorInfo& e)
{
    const exception_detail::ParamsMap& m = e.value();
    bool first = true;
    for (exception_detail::ParamsMap::const_iterator it = m.begin(), itEnd = m.end(); it != itEnd; ++it, first = false)
    {
        if (!first)
            stream << ", ";
        stream << it->first.c_str() << ": " << it->second;
    }

    return stream;
}

bool ExtractNamedParam(const boost::exception& xcpt, const std::string& name, std::wstring& value)
{
    if (const exception_detail::ParamsMap* params = boost::get_error_info<ParamsErrorInfo>(xcpt))
    {
        if (!params->empty())
        {
            exception_detail::ParamsMap::const_iterator it = params->find(name);
            if (it != params->end())
                return value = it->second, true;
        }
    }

    return false;
}

namespace exception_detail
{
    std::wstring GetWin32ErrorDescription(DWORD win32Err, DWORD flags)
    {
        std::wostringstream str;
        if (flags & ErrorDescriptionFlags::WriteIntegerValue)
        {
            // ������� ����� �� ������� ����� ��� hex
            if (!(flags & ErrorDescriptionFlags::HexValue) && !!(win32Err & 0x80000000))
                flags |= ErrorDescriptionFlags::HexValue;

            if (flags & ErrorDescriptionFlags::HexValue)
                str << L"0x" << std::hex << win32Err;
            else
                str << win32Err;
        }

        // ������� �������� ������ � English US, ����� � ��������� ������
        DWORD languages[] = { MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), 0 };

        LPWSTR message = NULL;
        for (size_t langIndex = 0; langIndex < _countof(languages); ++langIndex)
        {
            if (!FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
                FORMAT_MESSAGE_IGNORE_INSERTS, NULL, win32Err, languages[langIndex], reinterpret_cast<LPWSTR>(&message), 0,
                NULL))
                continue;

            wchar_t* messageEnd = message + wcslen(message);
            // ������� ������ �������
            while (('\n' == *(messageEnd - 1) || '\r' == *(messageEnd - 1) || ' ' == *(messageEnd - 1) ||
                '.' == *(messageEnd - 1)) && message < messageEnd)
                --messageEnd;

            if (messageEnd > message) // not empty
            {
                if (flags & ErrorDescriptionFlags::WriteIntegerValue)
                    str << L" (";

                str << std::wstring(message, messageEnd);

                if (flags & ErrorDescriptionFlags::WriteIntegerValue)
                    str << L")";
            }
            LocalFree(message);
            break;
        }
        return str.str();
    }
} // namespace exception_detail

} // namespace cmn

