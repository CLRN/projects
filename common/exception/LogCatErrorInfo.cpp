#include "exception/LogCatErrorInfo.h"
#include "conversion/AnyCast.h"

namespace cmn
{
    std::wostream& operator << (std::wostream& stream, const LogCatErrorInfo& e)
    {
        stream << e.value().index() << " (" << conv::cast<std::wstring>(e.value().str()) << ")";
        return stream;
    }

} // namespace cmn
