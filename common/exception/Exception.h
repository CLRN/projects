﻿#ifndef Exception_h__
#define Exception_h__

#include "ParamsErrorInfo.h"

#include <exception>
#include <string>
#include <sstream>

#include <boost/exception/exception.hpp>
#include <boost/exception/errinfo_nested_exception.hpp>
#include <boost/exception_ptr.hpp>
#include <boost/exception/get_error_info.hpp>

#include <boost/preprocessor/arithmetic/dec.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/control/expr_if.hpp>
#include <boost/preprocessor/if.hpp>
#include <boost/preprocessor/facilities/empty.hpp> 
#include <boost/preprocessor/facilities/expand.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#include <boost/preprocessor/repetition/enum.hpp>
#include <boost/preprocessor/repetition/repeat.hpp>
#include <boost/preprocessor/iteration/local.hpp>
#include <boost/preprocessor/variadic/size.hpp>
#include <boost/preprocessor/variadic/elem.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/tuple/elem.hpp>
#include <boost/preprocessor/tuple/size.hpp>
#include <boost/preprocessor/comparison/equal.hpp>

#include <boost/function/function_fwd.hpp>

#define DECORATE_NAME(name) DECORATE_NAME_I(name, __LINE__)

#define DECORATE_NAME_I(name, line) DECORATE_NAME_II(name, line)
#define DECORATE_NAME_II(name, line) __ ## name ## __ ## line ## __

namespace cmn
{

namespace exception_detail
{

//! Класс для формирования списка параметров
class CreateParamsHelper
{
    class ParamsCreator
    {
    public:
        ParamsCreator()
        {
        }
        ParamsCreator& operator %(const ParamDescriptor& t)
        {
            m_Params[t.m_Name] = t.m_Value;
            return *this;
        }
        ParamsErrorInfo Value() const
        {
            return m_Params;
        }
        void Set(const ParamsMap& m)
        {
            m_Params = m;
        }

    private:
        ParamsMap m_Params;
    };

    typedef std::vector<std::string> Names;
    Names m_Params;
    ParamsCreator m_Creator;

public:

#define PARAMSERRORINFO_PP_NAME_LIST(z, n, data)        const char* BOOST_PP_CAT(name, n)
#define PARAMSERRORINFO_PP_NAME_INIT(z, n, data)        m_Params[n] = BOOST_PP_CAT(name, n);
#define PARAMSERRORINFO_PP_PARAM_LIST(z, n, data)       const BOOST_PP_CAT(T, n) & BOOST_PP_CAT(arg, n)
#define PARAMSERRORINFO_PP_DESCRIPTOR(z, n, data)       BOOST_PP_CAT(%, ::cmn::exception_detail::ParamDescriptor(m_Params[n], BOOST_PP_CAT(arg, n)))

    CreateParamsHelper();
    CreateParamsHelper(const boost::exception&);

#define PARAMSERRORINFO_CONSTRUCT(z, n, data)                                                       \
    CreateParamsHelper& NamesPhase(BOOST_PP_ENUM(n, PARAMSERRORINFO_PP_NAME_LIST, $))               \
    {                                                                                               \
        m_Params.resize(n);                                                                         \
        BOOST_PP_REPEAT(n, PARAMSERRORINFO_PP_NAME_INIT, $)                                         \
        return *this;                                                                               \
    }                                                                                               \
    BOOST_PP_EXPR_IF(n, template<) BOOST_PP_ENUM_PARAMS(n, typename T) BOOST_PP_EXPR_IF(n, >)       \
    ParamsErrorInfo ValuesPhase(BOOST_PP_ENUM(n, PARAMSERRORINFO_PP_PARAM_LIST, $))                 \
    {                                                                                               \
        return (m_Creator                                                                           \
            BOOST_PP_REPEAT(n, PARAMSERRORINFO_PP_DESCRIPTOR, _)                                    \
            ).Value();                                                                              \
    }

//
// Определяет, является ли переданный аргумент Boost.Preprocessor.Tuple
// Примеры:
//      PARAMSERRORINFO_IS_TUPLE(param1) -> 0
//      PARAMSERRORINFO_IS_TUPLE(SomeFunction(param).str()) -> 0
//      PARAMSERRORINFO_IS_TUPLE((*it)->SomeFunction(param).str()) -> 0
//      PARAMSERRORINFO_IS_TUPLE((*it)->value) -> 0
//      PARAMSERRORINFO_IS_TUPLE((param)) -> 1
//      PARAMSERRORINFO_IS_TUPLE((param(), "paramName")) -> 1
//

#define PARAMSERRORINFO_IS_TUPLE(param) \
    PARAMSERRORINFO_IS_TUPLE_IMPL(PARAMSERRORINFO_IS_TUPLE_HELPER_CAT_PARAMS(param))
#define PARAMSERRORINFO_IS_TUPLE_IMPL(...) \
    BOOST_PP_IF(BOOST_PP_EQUAL(BOOST_PP_VARIADIC_SIZE(__VA_ARGS__), 4), 1, 0)

#define PARAMSERRORINFO_IS_TUPLE_HELPER_CAT_PARAMS(param) \
    PARAMSERRORINFO_IS_TUPLE_HELPER_CAT_PARAMS2(param)

// Если параметр начинается и заканчивается не на букву, получим последовательность из трёх идентификаторов _IS_TUPLE_HELPER_.
// Затем к каждому элементу этой последовательности прибавляем PARAMSERRORINFO и EXPAND_IN_TWO_PARAMS() (в начало и конец).
// Получаем три вызова PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_IN_TWO_PARAMS(), в итоге длина последовательности будет 6.
#define PARAMSERRORINFO_IS_TUPLE_HELPER_CAT_PARAMS2(param) \
    PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_VARIADIC(PARAMSERRORINFO_IS_TUPLE_HELPER ## param)

#define PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_IN_TWO_PARAMS() fake, fake

#define PARAMSERRORINFO_IS_TUPLE_HELPER(...) _IS_TUPLE_HELPER_, _IS_TUPLE_HELPER_

// В случае, если param в главном макросе PARAMSERRORINFO_IS_TUPLE начинается и заканчивается
// на скобки, мы получим последовательность из трёх _IS_TUPLE_HELPER_.
// Если после скобок у параметра есть ещё какие-то символы, в последовательности будут присутствовать и они.
// Посредством итерации по variadic'у склеиваем все _IS_TUPLE_HELPER_ в макрос
// PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_IN_TWO_PARAMS()
#define PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_VARIADIC(...)                                        \
    BOOST_PP_REPEAT(                                                                                \
        BOOST_PP_VARIADIC_SIZE(__VA_ARGS__),                                                        \
        PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_WORKER,                                              \
        __VA_ARGS__)

#define PARAMSERRORINFO_IS_TUPLE_HELPER_EXPAND_WORKER(z, n, data)                                   \
    BOOST_PP_COMMA_IF(n)                                                                            \
    BOOST_PP_CAT(                                                                                   \
            PARAMSERRORINFO,BOOST_PP_CAT(BOOST_PP_VARIADIC_ELEM(n, data), EXPAND_IN_TWO_PARAMS())   \
                )

// Является ли переданное кортежом с двумя элементами, нужным нам для формирования параметра
#define PARAMSERRORINFO_IS_PARAM_TUPLE(param) BOOST_PP_IF(PARAMSERRORINFO_IS_TUPLE(param), BOOST_PP_EQUAL(BOOST_PP_TUPLE_SIZE(param), 2), 0)


#define PARAMSERRORINFO_PARAMS_COUNT_IMPL(fake, ...) BOOST_PP_DEC(BOOST_PP_VARIADIC_SIZE(__VA_ARGS__))
#define PARAMSERRORINFO_PARAMS_COUNT(...)          PARAMSERRORINFO_PARAMS_COUNT_IMPL(fake1,fake2,__VA_ARGS__)


#define PARAMSERRORINFO_PARAM_NAME_VARIADIC_WORKER(z, n, data)                                      \
    BOOST_PP_COMMA_IF(n)                                                                            \
    BOOST_PP_IF(                                                                                    \
        PARAMSERRORINFO_IS_PARAM_TUPLE(BOOST_PP_VARIADIC_ELEM(n, data)),                            \
        BOOST_PP_TUPLE_ELEM(1, BOOST_PP_VARIADIC_ELEM(n, data)),                                    \
        BOOST_PP_STRINGIZE(BOOST_PP_VARIADIC_ELEM(n, data))                                         \
        )


#define PARAMSERRORINFO_PARAM_NAMES_VARIADIC(...)                                                   \
    BOOST_PP_REPEAT(                                                                                \
        PARAMSERRORINFO_PARAMS_COUNT(__VA_ARGS__),                                                  \
        PARAMSERRORINFO_PARAM_NAME_VARIADIC_WORKER,                                                 \
        __VA_ARGS__)


#define PARAMSERRORINFO_PARAM_VALUE_VARIADIC_WORKER(z, n, data)                                     \
    BOOST_PP_COMMA_IF(n)                                                                            \
    BOOST_PP_IF(                                                                                    \
        PARAMSERRORINFO_IS_PARAM_TUPLE(BOOST_PP_VARIADIC_ELEM(n, data)),                            \
        BOOST_PP_TUPLE_ELEM(0, BOOST_PP_VARIADIC_ELEM(n, data)),                                    \
        BOOST_PP_VARIADIC_ELEM(n, data)                                                             \
        )


#define PARAMSERRORINFO_PARAM_VALUES_VARIADIC(...)                                                  \
    BOOST_PP_REPEAT(                                                                                \
        PARAMSERRORINFO_PARAMS_COUNT(__VA_ARGS__),                                                  \
        PARAMSERRORINFO_PARAM_VALUE_VARIADIC_WORKER,                                                \
        __VA_ARGS__)


#ifndef EXPEXCEPTION_MAX_PARAMETERS
//! Максимальное количество параметров которые можно передавать в макросы
#define EXPEXCEPTION_MAX_PARAMETERS 15
#endif

#define BOOST_PP_LOCAL_MACRO(n)     PARAMSERRORINFO_CONSTRUCT($, n, $)
#define BOOST_PP_LOCAL_LIMITS       (0, EXPEXCEPTION_MAX_PARAMETERS)
#include BOOST_PP_LOCAL_ITERATE()

#undef PARAMSERRORINFO_PP_DESCRIPTOR
#undef PARAMSERRORINFO_PP_PARAM_LIST
#undef PARAMSERRORINFO_PP_NAME_INIT
#undef PARAMSERRORINFO_PP_NAME_LIST
};

//! Формирует параметры, но не с нуля, а используя параметры в переданном исключении
//! Параметры можно указывать двумя способами:
//! Первый:
//!     Просто указать выражение для значения параметра.
//!     Например:
//!         buildTime
//!         формируется параметр с именем "buildTime" и значением переменной buildTime
//! Второй:
//!     Явно указывая как выражение для значения параметра, так и его имя
//!     Например:
//!         (GetSomethingFromManyOtherParameters(param1, param2)->BuildTime(), "buildTime")
//!         формируется параметр с именем "buildTime" и значением, полученным с помощью длинного выражения
#define EXPTHROW_APPEND_PARAMS(xcpt, ...)                                                       \
    BOOST_PP_EXPR_IF(                                                                           \
        PARAMSERRORINFO_PARAMS_COUNT(__VA_ARGS__),                                              \
        << ::cmn::exception_detail::CreateParamsHelper(xcpt)                                    \
            .NamesPhase(PARAMSERRORINFO_PARAM_NAMES_VARIADIC(__VA_ARGS__))                      \
            .ValuesPhase(PARAMSERRORINFO_PARAM_VALUES_VARIADIC(__VA_ARGS__))                    \
        )

//! формирует параметры с нуля
//! @see EXPTHROW_APPEND_PARAMS
#define EXPTHROW_PARAMS(...)    EXPTHROW_APPEND_PARAMS(BOOST_PP_EMPTY(), __VA_ARGS__)



} // namespace exception_detail


//! Базовый класс для всех исключений, достаточно от него наследоваться.
class Exception : public virtual boost::exception, public virtual std::exception
{
    Exception(); //!< Not implemented
public:
    //!
    //! Constructors
    //!
    Exception(const char* msg);
    Exception(const wchar_t* msg);
    Exception(const std::string& msg);
    Exception(const std::wstring& msg);

    //template<typename T>
    //friend Exception& operator << (Exception& xcpt, const T& e);

    virtual const char* what() const override { return m_Message.c_str(); }

protected:
    std::string m_Message;
};

//! Вспомогательный макрос. Описывает класс с определенным текстом сообщения
#define DECLARE_SIMPLE_EXCEPTION_CLASS(className, baseClass)                \
    class className : public baseClass                                      \
    {                                                                       \
    public:                                                                 \
        className(const char* msg) : baseClass(msg) {}                      \
        className(const wchar_t* msg) : baseClass(msg) {}                   \
    }


//! Макрос для выкидывания исключения. В релизе не добавляется информация о файле, строке и функции.
#define THROW(x, ...)    BOOST_THROW_EXCEPTION((x) EXPTHROW_PARAMS(__VA_ARGS__))

//! Инкапсулировать текущее исключение. Можно вызывать только внутри блока catch
#define NESTED_CURRENT_EXCEPTION()      ::boost::errinfo_nested_exception(::boost::current_exception())


//! Инкапсулировать конкретное исключение
#define NESTED_CONCRETE_EXCEPTION(x)      ::boost::errinfo_nested_exception(::boost::copy_exception(x))


/*!

На примере следующего старого лога показывается во что он должен превратиться
Кусок исходного лога:

[2012.11.01 20:03:53] ERROR Net: Failed to read data from a socket. Socket: 1836, Win32 error: 10053 (An established connection was aborted by the software in your host machine)
[2012.11.01 20:03:53] ERROR ChannelCore: Failed to read data. ChCore status: 14
[2012.11.01 20:03:53] INFO  Sessions: Session is disconnected. 


Формат вывода следующий:

============================================
[2012.11.01 20:03:53] INFO Session is disconnected.
  [Time] = [2012.11.01 20:03:53] (UTC+04:00)
  [Level] = 20000 (INFO)
  [Expression] = 's != s_ok'
  [File] = ..\..\session.cpp(123)
  [Function] = void Session::DoItAgain2(void)
  [Category] =  Transport.Sessions
  [Params] = Reason: 1 (Network failure), Session: [In, 127.0.0.1:44112, GUID: e8c346d4-9687-44bf-b6b2-dbdbd59de05d, Local user: [2] PTSECURITY\vgordeychik, Destroy, Peer: [Console, Rev/Bld/Prot=24078071/0.0.0.0/3.0, ExtMsg: Supported]]
  [Nested error] = Failed to read data.
    [Time] = [2012.11.01 20:03:53] (UTC+04:00)
    [Level] = 40000 (ERROR)
    [Expression] = 's != s_ok'
    [File] = ..\..\core.cpp(24)
    [Function] = void Session::DoItAgain(void)
    [Category] = Transport.ChannelCore
    [Params] = ChCore status: 14
    [Nested error] = Failed to read data from a socket.
      [Time] = [2012.11.01 20:03:53] (UTC+04:00)
      [Level] = 40000 (ERROR)
      [Expression] = 'socket_status != 0'
      [File] = ..\..\core.cpp(23)
      [Function] = void Session::DoIt(void)
      [Category] = Base.Net
      [Win32 error] = 10053 (An established connection was aborted by the software in your host machine)
============================================


Данный вывод характерен для DEBUG версии, в RELEASE версии вывод следующий
============================================
[2012.11.01 20:03:53] INFO Session is disconnected.
  [Time] = [2012.11.01 20:03:53] (UTC+04:00)
  [Level] = 20000 (INFO)
  [Category] =  Transport.Sessions
  [Params] = Reason: 1 (Network failure), Session: [In, 127.0.0.1:44112, GUID: e8c346d4-9687-44bf-b6b2-dbdbd59de05d, Local user: [2] PTSECURITY\vgordeychik, Destroy, Peer: [Console, Rev/Bld/Prot=24078071/0.0.0.0/3.0, ExtMsg: Supported]]
  [Nested error] = Failed to read data.
    [Time] = [2012.11.01 20:03:53] (UTC+04:00)
    [Level] = 40000 (ERROR)
    [Category] = Transport.ChannelCore
    [Params] = ChCore status: 14
    [Nested error] = Failed to read data from a socket.
      [Time] = [2012.11.01 20:03:53] (UTC+04:00)
      [Level] = 40000 (ERROR)
      [Category] = Base.Net
      [Win32 error] = 10053 (An established connection was aborted by the software in your host machine)
============================================
 
Более того параметры Time и Level скорее всего будут отсутствовать потому что обычно схема выглядит следующим образом
- самое нижнее исключение инициирует более высокое исключение
- это исключение инициирует следующее исключение
- сгенерированное исключение ловится и сразу логируется
- все выше перечисленные шаги выполняются относительно малое время и можно считать их за один и тот же временной отрезок

Более упрощенный вариант (и скорее всего самый рабочий) будет выглядеть следующим образом

============================================
[2012.11.01 20:03:53] INFO Sessions: Session is disconnected.
  [Category] =  Transport.Sessions
  [Params] = Reason: 1 (Network failure), Session: [In, 127.0.0.1:44112, GUID: e8c346d4-9687-44bf-b6b2-dbdbd59de05d, Local user: [2] PTSECURITY\vgordeychik, Destroy, Peer: [Console, Rev/Bld/Prot=24078071/0.0.0.0/3.0, ExtMsg: Supported]]
  [Nested error] = Failed to read data.
    [Category] = Transport.ChannelCore
    [Params] = ChCore status: 14
    [Nested error] = Failed to read data from a socket.
      [Category] = Base.Net
      [Win32 error] = 10053 (An established connection was aborted by the software in your host machine)
============================================
 

 Возможны ещё следующие варианты категорий выводимой информации

 [ExtendedMessage] = ErrCode: 0x7b[Level: Info, SubLevel: 0, Action: Undefined, Code: 123], ResId: 321, Params: [Name: 'param', Value: (DName: 'dispname', Type: Schedule, Id: '{10298319128182}')]
 [HRESULT] = 0x80070005 (Access is denied)
 [status_t] = 21 (Invalid license)
 [IErrorInfo] = описание ошибки возвращаемое через COM интерфейс IErrorInfo


*/

//! Флаги вывода информации об исключении
struct ExceptionInfoDetails
{
    enum
    {
        //! При формировании текста исключает из исключения верхнего уровня параметр [Level]
        SkipTopLevelSeverity = 1 << 0,
        //! При формировании текста исключает из исключения верхнего уровня параметр [Category]
        SkipTopLevelCategory = 1 << 1
    };
};

namespace exception_detail
{
    std::wstring ExceptionInfoImpl(const std::exception* se, const boost::exception* be,
        unsigned flags = ExceptionInfoDetails::SkipTopLevelSeverity | ExceptionInfoDetails::SkipTopLevelCategory);
}

//! Visitor'ы для вывода кастомной информации, инкапсулированной в исключение
namespace ExceptionInfoVisitors
{
    //! Функтор, выводящий кастомную информацию, содержащуюся в исключении, в поток
    //!
    //! \param[in] os - поток
    //! \param[in] be - подобъект boost::exception исключения
    //! \param[in] indentLevel - уровень отступа. Для его вывода необходимо использовать функцию MakeIndent
    //! \param[in] flags - флаги ExceptionInfoDetails
    //!
    //! Функтор должен поддерживать повторный вход в него из двух разных потоков
    //!
    typedef boost::function<void(std::wostream& os, const boost::exception& be, unsigned indentLevel, unsigned flags)> ExceptionInfoVisitor;

    //! Зарегистрировать Visitor для вывода кастомной информации об исключении
    void RegisterExceptionInfoVisitor(const ExceptionInfoVisitor& visitor);

    //! Вывести отступ в поток (также добавляет переход на новую строку перед отступом)
    std::wostream& MakeIndent(std::wostream& os, unsigned indentLevel);

    //! Вспомогательный класс, регистрирующий Visitor
    class VisitorRegistrator
    {
    public:
        template<typename Functor>
        VisitorRegistrator(const Functor& functor) : m_Registered(false)
        {
            RegisterExceptionInfoVisitor(functor);
            m_Registered = true;
        }

        bool Registered() const
        { return m_Registered; }

    private:
        bool m_Registered;
    };

    //! Регистрация в декларативном стиле. Должна использоваться в рамках namespace (глобального или любого другого)
    #define DECLARE_CURRENT_EXCEPTION_INFO_VISITOR(visitor, registratorName) \
        static ::cmn::ExceptionInfoVisitors::VisitorRegistrator registratorName((visitor));

    #define DECLARE_EXCEPTION_INFO_VISITOR(visitor) \
        DECLARE_CURRENT_EXCEPTION_INFO_VISITOR(visitor, DECORATE_NAME(ExceptionInfoVisitorRegistrator))
} // namespace ExceptionInfoVisitors

//! Получаем информацию об исключении, можно передавать как std::exception так и boost::exception
template<typename T>
inline std::wstring ExceptionInfo(const T& xcpt, unsigned flags = ExceptionInfoDetails::SkipTopLevelSeverity | ExceptionInfoDetails::SkipTopLevelCategory)
{
    // В зависимости от типа T dynamic_cast<> может быть заменён компилятором на статический каст
    return exception_detail::ExceptionInfoImpl(dynamic_cast<const std::exception*>(&xcpt), dynamic_cast<const boost::exception*>(&xcpt), flags);
}

// Рекурсивный вариант boost::get_error_info
template<class ErrorInfo, class E>
inline const typename ErrorInfo::value_type* get_error_info(const E& e, bool recursive = true)
{
    typedef typename ErrorInfo::value_type ReturnType;
    if (const boost::exception* be = dynamic_cast<const boost::exception*>(&e))
    {
        if (const ReturnType* val = boost::get_error_info<ErrorInfo>(*be))
            return val;

        // В нашем исключении нет нужной инфы.
        // Пробуем вложенное
        if (recursive)
        {
            if (const boost::exception_ptr* nestedException = boost::get_error_info<boost::errinfo_nested_exception>(*be))
            {
                try
                {
                    boost::rethrow_exception(*nestedException);
                }
                catch (const boost::exception& be)
                {
                    return get_error_info<ErrorInfo>(be, true);
                }
                catch (...)
                {
                    return NULL;
                }
            }
        }
    }
    return NULL;
}

} // namespace cmn
#endif // Exception_h__
