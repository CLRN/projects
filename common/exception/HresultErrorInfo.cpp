#include "exception/HresultErrorInfo.h"
#include "exception/ParamsErrorInfo.h"

namespace cmn
{
    std::wostream& operator << (std::wostream& stream, const HResultErrorInfo& e)
    {
        stream << exception_detail::GetWin32ErrorDescription(e.value(),
            exception_detail::ErrorDescriptionFlags::WriteIntegerValue | exception_detail::ErrorDescriptionFlags::HexValue);

        return stream;
    }

} // namespace cmn
