#pragma once

#include <ostream>
#include <boost/exception/info.hpp>
#include <wtypes.h>

namespace cmn
{
    //! Статус операции
    typedef boost::error_info<struct tag_hresult, HRESULT> HResultErrorInfo;

    //! Сериализатор
    std::wostream& operator << (std::wostream& stream, const HResultErrorInfo& e);

} // namespace cmn
