#ifndef Serialization_h__
#define Serialization_h__

#include "ISerializable.h"

namespace cmn
{
namespace srl
{

namespace details
{

void MakePrettyName(std::string& name);

class IField : public ISerializable
{
public:
    typedef boost::shared_ptr<IField> Ptr;
    typedef std::vector<Ptr> List;

    virtual void Adjust(const char* instance) = 0;
};

template<typename T>
class TrivialField;

template<typename T, typename F>
class FactoryField;

} // namespace details

class Serializable : public virtual ISerializable
{
public:
    Serializable(const std::string& name, const details::IField::List& fields);

    template<typename T>
    details::IField::Ptr Field(T& field, const std::string& name)
    {
        return details::TrivialField<T>::Instance(field, name);
    }

    template<typename T, typename F>
    details::IField::Ptr Field(T& field, const std::string& name, const F& factory)
    {
        return details::FactoryField<T, F>::Instance(field, name, factory);
    }

    virtual void Serialize(cmn::xml::Node& node) const override;
    virtual void Deserialize(const cmn::xml::Node& node) override;
    virtual std::string GetNodeName() const override;

private:

    void AdjustFields() const;

private:
    details::IField::List m_Fields;
    std::string m_NodeName; 
};

} // namespace srl
} // namespace cmn

#endif // Serialization_h__
