// Google test library headers
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "serialization/Serialization.h"
#include "serialization/Helpers.h"
#include "xml/Node.h"

#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>

template<typename T, typename Enable = void>
struct Comparer
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        return lhs == rhs;
    }
};

template<typename T>
struct Comparer<boost::shared_ptr<T>, void>
{
    static bool Compare(const boost::shared_ptr<T>& lhs, const boost::shared_ptr<T>& rhs) 
    {
        return Comparer<T>::Compare(*lhs, *rhs);
    }
};

template<typename T>
struct Comparer<T, typename static boost::enable_if<cmn::meta::IsIterable<T>>::type>
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        return boost::equal(lhs, rhs, boost::bind(&Comparer<typename T::value_type>::Compare, _1, _2));
    }
};

template<typename T>
struct Comparer<T, typename static boost::enable_if<boost::is_same<cmn::srl::ISerializable, T>>::type>
{
    static bool Compare(const T& lhs, const T& rhs) 
    {
        return Comparer<Object>::Compare(dynamic_cast<const Object&>(lhs), dynamic_cast<const Object&>(rhs));
    }
};

template<typename T>
void Compare(const T& lhs, const T& rhs)
{
    EXPECT_TRUE(Comparer<T>::Compare(lhs, rhs));
}


class Object : public cmn::srl::ISerializable
{
public:

    typedef boost::shared_ptr<Object> Ptr;

    Object(const std::string& v) : m_Value(v) {}
    Object() {}

    virtual void Serialize(cmn::xml::Node& node) const override
    {
        node.Name("object");
        node.Set("value", m_Value);
    }

    virtual void Deserialize(const cmn::xml::Node& node) override
    {
        m_Value = node.Get<std::string>("value");
    }

    static Ptr Instance(const std::string& name)
    {
        return boost::make_shared<Object>(name);
    }

    virtual std::string GetNodeName() const
    {
        return "object";
    }

    bool operator == (const Object& other) const
    {
        return m_Value == other.m_Value;
    }

    void SetValue(const std::string& v)
    {
        m_Value = v;
    }
private:
    std::string m_Value;
};

class Derived : public cmn::srl::Serializable
{
public:
    Derived() 
        : SERIALIZABLE("derived",
            FIELD(m_String)
            FIELD(m_Int)
            FIELD(m_AnotherString)
            FIELD(m_ObjectPointer, boost::bind(&Object::Instance, "whatever"))
            FIELD(m_ListOfStrings)
            FIELD(m_DataList)
            FIELD(m_SerializableObjectList, boost::bind(&Object::Instance, "empty"))
            FIELD(m_SerializableMap)
            FIELD(m_SerializableObjectMap)
            FIELD(m_EmptyPointer)
          )
    {
    }

    bool operator == (const Derived& another) const
    {
        return 
            m_String == another.m_String && 
            m_Int == another.m_Int && 
            m_AnotherString == another.m_AnotherString &&
            *m_ObjectPointer == *another.m_ObjectPointer && 
            m_ListOfStrings == another.m_ListOfStrings &&
            m_DataList == another.m_DataList &&
            m_EmptyPointer == another.m_EmptyPointer;

        Compare(m_SerializableObjectList, another.m_SerializableObjectList);
        Compare(m_SerializableMap, another.m_SerializableMap);
    }

    std::string m_String;
    int m_Int;
    std::wstring m_AnotherString;
    Object::Ptr m_ObjectPointer;
    std::vector<std::string> m_ListOfStrings;
    std::set<std::vector<char>> m_DataList;
    std::list<ISerializable::Ptr> m_SerializableObjectList;
    std::map<std::string, int> m_SerializableMap;
    std::map<int, Object> m_SerializableObjectMap;
    Object::Ptr m_EmptyPointer;
};

TEST(Serialization, Meta)
{
    EXPECT_TRUE(cmn::meta::IsSharedPtr<boost::shared_ptr<Object> >::value);
    EXPECT_TRUE(cmn::meta::IsSharedPtr<boost::shared_ptr<char> >::value);
    EXPECT_FALSE(cmn::meta::IsSharedPtr<std::unique_ptr<char> >::value);
    EXPECT_FALSE(cmn::meta::IsSharedPtr<char>::value);

    EXPECT_TRUE((boost::is_same<cmn::meta::GetDataType<char>::type, char>::value));
    EXPECT_TRUE((boost::is_same<cmn::meta::GetDataType<std::list<char>>::type, char>::value));

    EXPECT_TRUE((cmn::meta::IsPair<std::pair<char, int>>::value));
    EXPECT_FALSE(cmn::meta::IsPair<char>::value);
}

template<typename T>
void TestAny(T& data)
{
    std::string buffer;
    {
        cmn::xml::Node node;
        node.Name("test");
        cmn::srl::details::SerializeAny(data, node);
        node.Save(buffer);
    }

    T deserialized;
    {
        const cmn::xml::Node node(buffer.c_str(), buffer.size());
        cmn::srl::details::DeserializeAny(node, deserialized);
    }

    EXPECT_TRUE(Comparer<T>::Compare(data, deserialized));
}

template<typename T, typename F>
void TestAnyWithFactory(T& data, const F& factory)
{
    std::string buffer;
    {
        cmn::xml::Node node;
        node.Name("test");
        cmn::srl::details::SerializeAny(data, node);
        node.Save(buffer);
    }

    T deserialized;
    {
        const cmn::xml::Node node(buffer.c_str(), buffer.size());
        cmn::srl::details::DeserializeAny(node, deserialized, factory);
    }

    Compare(data, deserialized);
}

TEST(Serialization, Serializers)
{
    {
        std::list<cmn::srl::ISerializable::Ptr> data = boost::assign::list_of(Object::Instance("first"))(Object::Instance("second"))(Object::Instance("third"));
        TestAnyWithFactory(data, [](){ return Object::Instance("object"); });
    }
    {
        cmn::srl::ISerializable::List data = boost::assign::list_of(Object::Instance("first"))(Object::Instance("second"))(Object::Instance("third"));
        TestAnyWithFactory(data, [](){ return Object::Instance("object"); });
    }
    {
        cmn::srl::ISerializable::Ptr data = Object::Instance("test value");
        TestAnyWithFactory(data, [](){ return Object::Instance("object"); });
    }
    {
        std::list<Object::Ptr> data = boost::assign::list_of(Object::Instance("first"))(Object::Instance("second"))(Object::Instance("third"));
        TestAny(data); 
    }
    {
        std::list<boost::shared_ptr<int> > data = boost::assign::list_of(boost::shared_ptr<int>(new int(1)))(boost::shared_ptr<int>(new int(2)))(boost::shared_ptr<int>(new int(3)));
        TestAny(data); 
    }
    {
        std::list<int> data = boost::assign::list_of(1)(2)(3)(4)(5);
        TestAny(data); 
    }
    {
        Object::Ptr data = Object::Instance("some value");
        TestAny(data); 
    }
    {
        boost::shared_ptr<int> data(new int(32131));
        TestAny(data);
    }
    {
        int data = 3213;
        TestAny(data);
    }
    {
        std::string data("test text");
        TestAny(data);
    }
    {
        std::wstring data(L"test wide text");
        TestAny(data);
    }
    {
        std::vector<char> data(100, 'a');
        TestAny(data); 
    }
}

TEST(Serialization, Simple)
{
    std::vector<Object::Ptr> objects;

    const auto values = boost::assign::list_of("first")("second")("third");
    boost::transform(values, std::back_inserter(objects), boost::bind(&Object::Instance, _1));

    // serialize
    std::string data;
    {
        cmn::xml::Node node;
        node.Name("root");
        cmn::srl::details::SerializeAny(objects, node);
        node.Save(data);
    }

    // deserialize
    std::vector<Object::Ptr> deserialized;
    {
        cmn::xml::Node node(data.c_str(), data.size());
        cmn::srl::details::DeserializeAny(node, deserialized, boost::bind(&Object::Instance, ""));
    }

    Compare(objects, deserialized);
}

TEST(Serialization, Derived)
{
    Derived top;
    std::string data;
    {
        {
            Derived first;

            first.m_AnotherString = L"wideblah";
            first.m_Int = 10321;
            first.m_String = "blah";
            first.m_ObjectPointer = Object::Instance("object through pointer");
            first.m_ListOfStrings.push_back("first value");
            first.m_ListOfStrings.push_back("second value");
            first.m_DataList.insert(boost::assign::list_of(1)(2)(3)(4)(5));
            first.m_DataList.insert(boost::assign::list_of(5)(4)(3)(2)(1));
            first.m_SerializableObjectList = boost::assign::list_of(Object::Instance("5"))(Object::Instance("3"))(Object::Instance("4"));
            first.m_SerializableMap.insert(std::make_pair("key3", 10));
            first.m_SerializableMap.insert(std::make_pair("key2", 20));
            first.m_SerializableMap.insert(std::make_pair("key1", 30));
            first.m_SerializableObjectMap.insert(std::make_pair(2, Object("first")));
            first.m_SerializableObjectMap.insert(std::make_pair(1, Object("second")));

            top = first;
        }

        cmn::xml::Node node;
        top.Serialize(node);
        node.Save(data);
    }

    Derived second;
    {
        cmn::xml::Node node(data.c_str(), data.size());
        second.Deserialize(node);
    }

    EXPECT_EQ(top, second);
}