#ifndef ContainerTraits_h__
#define ContainerTraits_h__

#include <vector>
#include <iterator>

#include <boost/type_traits/integral_constant.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/mpl/or.hpp>
#include <boost/static_assert.hpp>
#include <boost/shared_ptr.hpp>

namespace cmn
{
namespace meta
{

template<std::size_t Size>
struct Sizer
{
	char m_Data[Size];
};

template<typename T>
struct BaseChecker
{
	typedef Sizer<1> FalseType;
	typedef Sizer<2> TrueType;

	BOOST_STATIC_ASSERT(sizeof(FalseType) != sizeof(TrueType));

	static T* Make();
};

#define TYPE_CHECKER(_type)																				\
template<typename T>																					\
struct Check##_type : public BaseChecker<T>																\
{																										\
	static FalseType Check(...);																		\
																										\
	template<typename C>																				\
	static TrueType Check(const C*, const typename C::_type* = 0);										\
																										\
	static const bool value = sizeof(Check(Make())) == sizeof(TrueType);								\
};

TYPE_CHECKER(_Pairib);
TYPE_CHECKER(const_iterator);
TYPE_CHECKER(mapped_type);
TYPE_CHECKER(traits_type);
TYPE_CHECKER(first_type);
TYPE_CHECKER(second_type);

template<typename T>
struct IsString : public boost::integral_constant<bool, Checktraits_type<T>::value>{};

template<typename T>
struct IsDataVector 
    : public boost::integral_constant
            <
                bool, 
                boost::mpl::or_
                <
                    boost::is_same
                    <
                        std::vector<char>,
                        T
                    >,
                    boost::is_same
                    <
                        std::vector<unsigned char>,
                        T
                    >
                >::value
            >
{};

template<typename T>
struct IsInsertExists : public boost::integral_constant<bool, Check_Pairib<T>::value>{};

template<typename T>
struct IsIterable : public boost::integral_constant<bool, Checkconst_iterator<T>::value && !IsString<T>::value && !IsDataVector<T>::value>{};

template<typename T>
struct IsMap : public boost::integral_constant<bool, Checkmapped_type<T>::value && !Checktraits_type<T>::value>{};

template <class T>
struct IsSharedPtr : boost::mpl::false_ {};
template <class T>
struct IsSharedPtr<boost::shared_ptr<T> > : boost::mpl::true_ {};

template<typename T>
struct IsPair : public boost::integral_constant<bool, Checkfirst_type<T>::value && Checksecond_type<T>::value>{};

template<bool InsertExists, bool IsIterable>
struct InserterImpl;

template<>
struct InserterImpl<true, true>
{
	template<typename T, typename C>
	static void Insert(const T& value, C& container)
	{
		container.insert(value);
	}
};

template<>
struct InserterImpl<false, true>
{
	template<typename T, typename C>
	static void Insert(const T& value, C& container)
	{
		container.push_back(value);
	}
};

struct Inserter
{
	template<typename T, typename C>
	static void Insert(const T& value, C& container)
	{
		InserterImpl
		<
			IsInsertExists<C>::value,
			IsIterable<C>::value
		>::Insert(value, container);
	}
};

template<typename Cont, bool InsertExists, bool IsIterable>
struct InsertIteratorImpl;

template<typename Cont>
struct InsertIteratorImpl<Cont, true, true>
{
    typedef std::insert_iterator<Cont> type;
    template<typename C>
    static type Create(C& container)
    {
        return std::inserter(container, container.end());
    }
};

template<typename Cont>
struct InsertIteratorImpl<Cont, false, true>
{
    typedef std::back_insert_iterator<Cont> type;
    template<typename C>
    static type Create(C& container)
    {
        return std::back_inserter(container);
    }
};

struct InsertIterator
{
    template<typename C>
    static typename InsertIteratorImpl
    <
        C,
        IsInsertExists<C>::value,
        IsIterable<C>::value
    >::type Create(C& container)
    {
        return InsertIteratorImpl
        <
            C,
            IsInsertExists<C>::value,
            IsIterable<C>::value
        >::Create(container);
    }
};

template<typename T, bool IsContainer>
struct GetDataTypeImpl;

template<typename T>
struct GetDataTypeImpl<T, true>
{
    typedef typename T::value_type type;
};

template<typename T>
struct GetDataTypeImpl<T, false>
{
    typedef T type;
};

template<typename T>
struct GetDataType
{
    typedef typename GetDataTypeImpl<T, meta::IsIterable<T>::value>::type type;
};

} // namespace meta
} // namespace cmn

#endif // ContainerTraits_h__
