#include "Serialization.h"
#include "xml/Node.h"
#include "Helpers.h"

#include <boost/range/algorithm.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/make_shared.hpp>

namespace cmn
{
namespace srl
{

namespace details
{

void MakePrettyName(std::string& name)
{
    boost::algorithm::erase_all(name, "m_");

    std::string copy;
    boost::transform(name, std::back_inserter(copy), [&copy](const char symbol){
        if (boost::algorithm::is_upper()(symbol) && !copy.empty())
        {
            copy.push_back('_');
            return boost::algorithm::detail::to_lowerF<char>(std::locale())(symbol);
        }
        else
        {
            return symbol;
        }

    });
    name = boost::algorithm::to_lower_copy(copy);
}

} // namespace details

Serializable::Serializable(const std::string& name, const details::IField::List& fields) 
    : m_Fields(fields)
    , m_NodeName(name)
{
    AdjustFields();
}

void Serializable::Serialize(cmn::xml::Node& node) const
{
    node.Name(m_NodeName);
    AdjustFields();
    details::SerializeAny(m_Fields, node);
}

void Serializable::Deserialize(const cmn::xml::Node& node)
{
    CHECK(node.Name<std::string>() == m_NodeName, cmn::Exception("Wrong node"), m_NodeName, node);
    AdjustFields();

    boost::for_each(m_Fields, [&node](const ISerializable::Ptr& field){
        const std::string path = std::string("*[@name='") + field->GetNodeName() + std::string("']");
        const auto targetNode = node.Get<xml::Node::Ptr>(path);
        CHECK(targetNode, cmn::Exception("Failed to find target node by path"), path, node);
        field->Deserialize(*targetNode);
    });
}

std::string Serializable::GetNodeName() const 
{
    return m_NodeName;
}

void Serializable::AdjustFields() const
{
    boost::for_each(m_Fields, boost::bind(&details::IField::Adjust, _1, reinterpret_cast<const char*>(this)));
}

} // namespace srl
} // namespace cmn