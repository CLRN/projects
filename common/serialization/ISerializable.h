#ifndef ISerializable_h__
#define ISerializable_h__

#include <vector>
#include <string>

#include <boost/shared_ptr.hpp>

namespace cmn
{

namespace xml
{
    class Node;
}

namespace srl
{

class ISerializable
{
public:
    typedef boost::shared_ptr<ISerializable> Ptr;
    typedef std::vector<Ptr> List;

    virtual ~ISerializable() {}

    virtual void Serialize(cmn::xml::Node& node) const = 0;
    virtual void Deserialize(const cmn::xml::Node& node) = 0;
    virtual std::string GetNodeName() const = 0;
};

} // namespace srl
} // namespace cmn

#endif // ISerializable_h__
