#ifndef Helpers_h__
#define Helpers_h__

#include "details/ContainerTraits.h"
#include "conversion/AnyCast.h"
#include "xml/Node.h"
#include "Serialization.h"

#include <xutility>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/noncopyable.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/function.hpp>
#include <boost/mpl/or.hpp>
#include <boost/mpl/and.hpp>
#include <boost/mpl/not.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/type_traits/is_abstract.hpp>
#include <boost/type_traits/remove_cv.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/make_shared.hpp>

namespace cmn
{
namespace srl
{

namespace details
{

template<typename T>
class BaseField : public IField
{
public:
    BaseField(const std::string& name, T& data) 
        : m_Name(name) 
        , m_Data(&data)
        , m_Offset()
    {
        MakePrettyName(m_Name);
    }
    virtual std::string GetNodeName() const override 
    { 
        return m_Name; 
    }

    virtual void Adjust(const char* instance) override
    {
        if (!m_Offset)
            m_Offset = reinterpret_cast<const char*>(m_Data) - instance;
        else
            m_Data = reinterpret_cast<T*>(reinterpret_cast<char*>(const_cast<char*>(instance)) + m_Offset);
    }

protected:
    std::string m_Name;
    T* m_Data;
    std::ptrdiff_t m_Offset;
};

//! Serializer template
template<typename T, typename Enable = void>
struct Serializer;

//! Pod specification
template<typename T>
struct Serializer
<
    T, 
    typename boost::disable_if
    <
        boost::mpl::or_
        <
            boost::is_same<ISerializable, T>,
            boost::is_base_of<ISerializable, T>,
            meta::IsSharedPtr<T>,
            meta::IsPair<T>
        >
    >::type
>
{
    xml::Node operator () (const T& value) const
    {
        xml::Node node;
        node.Name("item");
        node.Set(value);
        return node;
    }

    T operator () (const xml::Node& node) const
    {
        T result;
        Serializer<T>()(node, result);
        return result;
    } 

    void operator ()(const xml::Node& node, T& result) const
    {
        CHECK(node.Name<std::string>() == "item", cmn::Exception("Wrong node"));
        result = node.Get<T>();
    }
};

template<typename T, typename Enable = void>
struct FactorySelector;

template<typename T>
struct FactorySelector
<
    T, 
    typename boost::enable_if<boost::is_abstract<T> >::type
>
{
    template<typename F>
    static F GetFactory(const F& f)
    {
        assert(f);
        return f;
    }
};

template<typename T>
struct FactorySelector
<
    T, 
    typename boost::disable_if<boost::is_abstract<T> >::type
>
{
    template<typename F>
    static F GetFactory(const F& f)
    {
        return f ? f : [](){ return boost::make_shared<T>(); };
    }
};

//! Shared pointer specification
template<typename T>
struct Serializer<boost::shared_ptr<T>, void>
{
    typedef boost::shared_ptr<T> Ptr;
    typedef boost::function<Ptr(void)> FactoryMethod;
    Serializer(const FactoryMethod& f = FactoryMethod()) 
        : m_Factory(f)
    {
    }
    xml::Node operator () (const Ptr& value) const
    {
        return value ? Serializer<T>()(*value) : xml::Node().Name("empty_pointer");
    }

    Ptr operator () (const xml::Node& node) const
    {
        if (node.Name<std::string>() == "empty_pointer")
            return Ptr();

        const auto result = FactorySelector<T>::GetFactory(m_Factory)();
        Serializer<T>()(node, *result);
        return result;
    }

    FactoryMethod m_Factory;
};

//! ISerializable not abstract specification
template<typename T>
struct Serializer
<
    T, 
    typename boost::enable_if
    <
        boost::mpl::and_
        <
            boost::is_base_of<ISerializable, T>,
            boost::mpl::not_<boost::is_abstract<T> >
        >
    >::type
>
{
    xml::Node operator () (const T& object) const
    {
        xml::Node node;
        object.Serialize(node);
        node.Set("@name", object.GetNodeName());
        return node;
    }


    T operator ()(const xml::Node& node) const
    {
        T result;
        result.Deserialize(node);
        return result;
    }

    void operator ()(const xml::Node& node, T& result) const
    {
        result.Deserialize(node);
    }
};

//! ISerializable abstract specification
template<typename T>
struct Serializer
<
    T, 
    typename boost::enable_if
    <
        boost::mpl::and_
        <
            boost::is_base_of<ISerializable, T>,
            boost::is_abstract<T>
        >
    >::type
>
{
    xml::Node operator () (const T& object) const
    {
        xml::Node node;
        object.Serialize(node);
        node.Set("@name", object.GetNodeName());
        return node;
    }

    void operator ()(const xml::Node& node, T& result) const
    {
        result.Deserialize(node);
    }
};

//! Pair specification
template<typename T>
struct Serializer
<
    T, 
    typename boost::enable_if
    <
        meta::IsPair<T>
    >::type
>
{
    xml::Node operator () (const T& value) const
    {
        xml::Node node;
        node.Name("item");

        node = Serializer<typename T::second_type>()(value.second);
        node.Set("@key", value.first);
        return node;
    }

    T operator () (const xml::Node& node) const
    {
        T result;
        Serializer<T>()(node, result);
        return result;
    } 

    void operator ()(const xml::Node& node, T& result) const
    {
        typedef typename boost::remove_cv<typename T::first_type>::type First;
        typedef typename T::second_type Second;

        const_cast<First&>(result.first) = node.Get<typename T::first_type>("@key");
        Serializer<Second>()(node, result.second);;
    }
};

template<typename T>
typename static boost::disable_if
<
    meta::IsIterable<T>
>::type SerializeAny(T& field, xml::Node& node)
{
    node = Serializer<T>()(field);
}

template<typename T>
typename static boost::enable_if
<
    meta::IsIterable<T>
>::type SerializeAny(T& field, xml::Node& node)
{
    typedef Serializer<typename cmn::meta::GetDataType<T>::type> CurrentSerializer;
    boost::transform(field, std::back_inserter(node), CurrentSerializer());
}

template<typename T>
typename static boost::disable_if
<
    meta::IsIterable<T>
>::type DeserializeAny(const xml::Node& node, T& data)
{
    data = Serializer<T>()(node); // if you got '<function-style-cast>' here - your factory method 
    // return type does not match to serializablke field type(your member passed with FIELD() macro)
}

template<typename T>
typename static boost::enable_if
<
    meta::IsIterable<T>
>::type DeserializeAny(const xml::Node& node, T& data)
{
    typedef Serializer<typename cmn::meta::GetDataType<T>::type> CurrentSerializer;
    boost::transform
    (
        node, 
        meta::InsertIterator::Create(data),
        CurrentSerializer()
    );
}

template<typename T, typename F>
typename static boost::disable_if
<
    meta::IsIterable<T>
>::type DeserializeAny(const xml::Node& node, T& data, const F& factory)
{
    data = Serializer<T>(factory)(node);
}

template<typename T, typename F>
typename static boost::enable_if
<
    meta::IsIterable<T>
>::type DeserializeAny(const xml::Node& node, T& data, const F& factory)
{
    typedef Serializer<typename cmn::meta::GetDataType<T>::type> CurrentSerializer;
    boost::transform
    (
        node, 
        meta::InsertIterator::Create(data),
        CurrentSerializer(factory)
    );
}

template<typename T>
class TrivialField final : public BaseField<T>
{
public:
    typedef boost::shared_ptr<TrivialField<T>> Ptr;

    TrivialField(T& data, const std::string& name) 
        : BaseField(name, data)
    {
    }

    virtual void Serialize(cmn::xml::Node& node) const override
    {
        node.Name("field");
        SerializeAny(*m_Data, node);
    }

    virtual void Deserialize(const cmn::xml::Node& node) override
    {
        DeserializeAny(node, *m_Data);
    }

    static Ptr Instance(T& data, const std::string& name)
    {
        return boost::make_shared<TrivialField<T>>(data, name);
    }
};

template<typename T, typename F>
class FactoryField final : public BaseField<T>
{
public:
    typedef boost::shared_ptr<FactoryField<T, F> > Ptr;

    FactoryField(T& data, const std::string& name, const F& f) 
        : BaseField(name, data)
        , m_Factory(f)
    {
    }

    virtual void Serialize(cmn::xml::Node& node) const override
    {
        node.Name("field");
        SerializeAny(*m_Data, node);
    }

    virtual void Deserialize(const cmn::xml::Node& node) override
    {
        DeserializeAny(node, *m_Data, m_Factory);
    }

    static Ptr Instance(T& data, const std::string& name, const F& f)
    {
        return boost::make_shared<FactoryField<T, F>>(data, name, f);
    }

private:
    F m_Factory;
};

} // namespace details

#define FIELD(x, ...) (cmn::srl::Serializable::Field(x, #x, __VA_ARGS__))
#define SERIALIZABLE(name, fields) cmn::srl::Serializable(name, boost::assign::list_of fields)

} // namespace srl
} // namespace cmn

#endif // Helpers_h__
