@echo off
cmake -H. -B.out -G "Visual Studio 12" -T v120_xp -DPOKER_BOT=OFF -DTEST_ALGO=OFF -DREALTY=ON
if %errorlevel% NEQ 0 (
    echo cmake exitcode = %errorlevel%
    pause
)