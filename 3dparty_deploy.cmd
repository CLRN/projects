@echo off

SET BinariesDirectory=%CD%\.out
SET MajorNumber=0
SET MinorNumber=0
SET BuildNumber=3
SET ToolsetName=v120_xp
SET VersionString=%MajorNumber%.%MinorNumber%.%BuildNumber%

cmake -H. -B.out -G "Visual Studio 12" -T %ToolsetName% -DPOKER_BOT=OFF -DTEST_ALGO=OFF -DREALTY=OFF -DUPDATE_THIRD_PARTY=ON
if %errorlevel% NEQ 0 (
    echo cmake exitcode = %errorlevel%
    pause
)

call "%VS120COMNTOOLS%\vsvars32.bat"
IF %errorlevel% NEQ 0 (
   call "%VS110COMNTOOLS%\vsvars32.bat"
)

REM Build generated projects.
msbuild /m /p:Configuration=Debug;Platform=Win32 %BinariesDirectory%\Projects.sln
IF %errorlevel% NEQ 0 (
   ECHO BUILD FAILED: %errorlevel% >&2
   EXIT /B %errorlevel%
)

REM Clean up
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.obj
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.pdb
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.log
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.tlog
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.idb
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.filters
del /S %BinariesDirectory%\third_party\%ToolsetName%\inc\*.vcxproj

REM Build NuGet package.
set root=%CD%
cd .out\third_party\%ToolsetName%
tar -zcvf ../../third_party.%VersionString%.tar.gz .
cd %root%