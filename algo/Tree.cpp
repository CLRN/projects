#include <map>
#include <string>
#include <list>

#include <gtest/gtest.h>

#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/make_shared.hpp>

struct Tree
{
    typedef boost::shared_ptr<Tree> Ptr;

    Tree(int v = 0) : m_Value(v) {}

    int m_Value;
    Ptr m_Left;
    Ptr m_Right;
};


void Add(Tree& node, int value)
{
    if (!node.m_Value && !node.m_Left && !node.m_Right)
    {
        node.m_Value = value;
        return;
    }

    if (value < node.m_Value)
    {
        if (node.m_Left)
            Add(*node.m_Left, value);
        else
            node.m_Left = boost::make_shared<Tree>(value);
    }
    else
    {
        if (node.m_Right)
            Add(*node.m_Right, value);
        else
            node.m_Right = boost::make_shared<Tree>(value);
    }
}


void Print(const Tree& node)
{
    struct TreeWithLevel : public Tree
    {
        TreeWithLevel() : m_Level(), m_Empty() {}
        TreeWithLevel(const Tree& t) : Tree(t), m_Level(), m_Empty() {}
        int m_Level;
        bool m_Empty;
    };

    std::list<TreeWithLevel> queue;
    queue.push_back(node);

    int level = 0;
    while (!queue.empty())
    {
        const auto n = queue.front();
        queue.pop_front();
        if (n.m_Level != level)
        {
            ++level;
            std::cout << std::endl;
        }

        if (n.m_Empty)
        {
            std::cout <<  "- ";
            continue;
        }

        std::cout << n.m_Value << " ";

        if (n.m_Left)
        {
            queue.push_back(*n.m_Left);
        }
        else
        {
            queue.push_back(TreeWithLevel());
            queue.back().m_Empty = true;
        }
        queue.back().m_Level = level + 1;

        if (n.m_Right)
        {
            queue.push_back(*n.m_Right);
        }
        else
        {
            queue.push_back(TreeWithLevel());
            queue.back().m_Empty = true;
        }
        queue.back().m_Level = level + 1;
    }
}

int Height(const Tree& node, int length = 0)
{
    int result = 0;

    if (node.m_Left)
        result = Height(*node.m_Left, length + 1);
    if (node.m_Right)
        result = std::max(Height(*node.m_Right, length + 1), result);

    return std::max(result, length);
}

TEST(Algorithm, Tree)
{
    Tree root;

    Add(root, 6);
    Add(root, 5);
    Add(root, 9);
    Add(root, 8);
    Add(root, 1);
    Add(root, 10);
    Add(root, 7);
    Add(root, 4);

    Print(root);

    EXPECT_EQ(Height(root), 3);
}
