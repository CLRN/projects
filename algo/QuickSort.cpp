#include "common/FileSystem.h"
#include "conversion/AnyCast.h"

#include <map>
#include <string>
#include <list>
#include <memory>

#include <gtest/gtest.h>

#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/make_shared.hpp>


template<typename It>
void QuickSort(It begin, It end)
{
    const auto length = std::distance(begin, end);
    if (length < 2)
        return;

    It middle = begin + length / 2;

    It left = begin;
    It right = end - 1;
    for (;;)
    {
        while (*left < *middle)
            ++left;

        while (*middle < *right)
            --right;

        if (*left == *right)
        {
            // recursive
            QuickSort(begin, middle);
            QuickSort(middle, end);
            break;
        }
        else
        {
            if (left == middle)
                middle = right;
            else
            if (right == middle)
                middle = left;

            std::swap(*left, *right);
            std::copy(begin, end, std::ostream_iterator<int>(std::cout, " "));
            std::cout << std::endl;
        }
    }
}


TEST(Algorithm, QuickSort)
{
    const std::vector<int> etalon = boost::assign::list_of(0)(1)(2)(3)(4)(4)(5)(6)(7)(8)(9);
    std::vector<int> data = boost::assign::list_of(9)(6)(8)(3)(4)(7)(1)(2)(0)(4)(5);

    QuickSort(data.begin(), data.end());
    EXPECT_EQ(data, etalon);
}