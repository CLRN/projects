﻿#include "common/FileSystem.h"
#include "conversion/AnyCast.h"

#include <map>
#include <string>
#include <list>
#include <memory>
#include <queue>
#include <limits>

#include <gtest/gtest.h>

#include <boost/algorithm/string/classification.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/weak_ptr.hpp>


struct Vertex
{
    typedef std::vector<Vertex> List;
    Vertex(std::size_t i, std::size_t w) : m_Index(i), m_Weight(w) {}

    bool operator < (const Vertex& other) const
    {
        return m_Weight < other.m_Weight;
    }
    bool operator > (const Vertex& other) const
    {
        return m_Weight > other.m_Weight;
    }

    std::size_t m_Index;
    std::size_t m_Weight;
};

static const std::size_t MAX_WEIGHT = std::numeric_limits<std::size_t>::max();
typedef std::vector<Vertex::List> Graph;

class DejkstraSolver
{
    typedef std::priority_queue
    <
        Vertex,
        Vertex::List,
        std::greater<Vertex>
    > Queue;

public:

    std::size_t Solve(const Graph& graph)
    {
        m_Processed.resize(graph.size());
        m_Weights.resize(graph.size(), MAX_WEIGHT);
        m_Weights.front() = 0; // weight to itself is always zero

        for (; !m_Processed[graph.size() - 1]; )
        {
            // get vertex with minimum weight
            const auto minIndex = GetVertexWithMinimumWeight();

            // get all adjacent vertices
            const Vertex::List& adjacent = graph[minIndex];

            // for each adjacent vertex calculate weight from source vertex
            for (const Vertex& v : adjacent)
            {
                // old weight
                const std::size_t oldWeight = m_Weights[v.m_Index];

                // new weight
                const std::size_t newWeight = m_Weights[minIndex] == MAX_WEIGHT ? v.m_Weight : v.m_Weight + m_Weights[minIndex];

                // relax
                if (newWeight < oldWeight)
                    m_Weights[v.m_Index] = newWeight;
            }

            m_Processed[minIndex] = true;
        }

        return m_Weights[graph.size() - 1];
    }

    std::size_t GetVertexWithMinimumWeight()
    {
        std::size_t index = 0;
        std::size_t weight = MAX_WEIGHT;
        for (std::size_t i = 0; i < m_Weights.size(); ++i)
        {
            if (m_Weights[i] < weight && !m_Processed[i])
            {
                index = i;
                weight = m_Weights[i];
            }
        }
        return index;
    }

private:
    Queue m_Queue;
    std::vector<std::size_t> m_Weights;
    std::vector<bool> m_Processed;
};


TEST(Algorithm, Dijkstra)
{
{
    std::vector<Vertex::List> graph = boost::assign::list_of
        (boost::assign::list_of
            (Vertex(1, 7))
            (Vertex(2, 9))
            (Vertex(5, 14))
            .convert_to_container<Vertex::List>())
        (boost::assign::list_of
            (Vertex(2, 10))
            (Vertex(3, 15))
            .convert_to_container<Vertex::List>())            
        (boost::assign::list_of
            (Vertex(3, 11))
            (Vertex(5, 2))
            .convert_to_container<Vertex::List>())              
        (boost::assign::list_of
            (Vertex(4, 6))
            .convert_to_container<Vertex::List>())                     
        (boost::assign::list_of
            (Vertex(5, 9))
            .convert_to_container<Vertex::List>())            
        (Vertex::List())    
            ;
     
        DejkstraSolver solver;
        EXPECT_EQ(solver.Solve(graph), 11);
    }
    {
        std::vector<Vertex::List> graph = boost::assign::list_of
            (boost::assign::list_of
                (Vertex(1, 7))
                (Vertex(2, 9))
                (Vertex(5, 14))
                .convert_to_container<Vertex::List>())
            (boost::assign::list_of
                (Vertex(2, 10))
                (Vertex(3, 15))
                .convert_to_container<Vertex::List>())            
            (boost::assign::list_of
                (Vertex(3, 11))
                (Vertex(5, 22))
                .convert_to_container<Vertex::List>())              
            (boost::assign::list_of
                (Vertex(4, 6))
                .convert_to_container<Vertex::List>())                     
            (boost::assign::list_of
                (Vertex(5, 9))
                .convert_to_container<Vertex::List>())            
            (Vertex::List())    
                ;
     
        DejkstraSolver solver;
        EXPECT_EQ(solver.Solve(graph), 14);
    }
    {
        std::vector<Vertex::List> graph = boost::assign::list_of
            (boost::assign::list_of
                (Vertex(1, 7))
                (Vertex(2, 9))
                (Vertex(5, 44))
                .convert_to_container<Vertex::List>())
            (boost::assign::list_of
                (Vertex(2, 10))
                (Vertex(3, 15))
                .convert_to_container<Vertex::List>())            
            (boost::assign::list_of
                (Vertex(5, 32))
                .convert_to_container<Vertex::List>())              
            (boost::assign::list_of
                (Vertex(2, 11))
                (Vertex(4, 6))
                .convert_to_container<Vertex::List>())                     
            (boost::assign::list_of
                (Vertex(5, 9))
                .convert_to_container<Vertex::List>())            
            (Vertex::List())    
                ;
     
        DejkstraSolver solver;
        EXPECT_EQ(solver.Solve(graph), 37);
    }
}
