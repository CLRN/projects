#include <map>
#include <string>
#include <list>

#include <gtest/gtest.h>

#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/adaptors.hpp>

class Node
{
    typedef std::map<char, Node> ChildMap;
    typedef std::list<Node> List;

    struct Word
    {
        typedef std::list<Word> List;
        Word(const std::string& value, unsigned w) : m_Value(value), m_Weight(w) {}
        std::string m_Value;
        unsigned m_Weight;
    };

public:

    Node()
        : m_Weight()
        , m_IsTerminationNode()
    {

    }

    void Load(const char* word)
    {
        if (!*word)
        {
            m_IsTerminationNode = true;
            return;
        }

        ++m_Weight;
        m_Childs[*word].Load(word + 1);
    }

    void Complete(const char* word, std::string complete, unsigned weight, Word::List& result) const
    {
        if (*word)
        {
            const auto it = m_Childs.find(*word);
            if (it != m_Childs.end())
            {
                complete.push_back(it->first);
                it->second.Complete(word + 1, complete, weight + m_Weight, result);
            }
        }
        else
        {
            if (m_IsTerminationNode)
                result.push_back(Word(complete, weight + m_Weight));

            for (const auto& pair : m_Childs)
            {
                std::string temp(complete);
                temp.push_back(pair.first);
                pair.second.Complete(word, temp, weight + m_Weight, result);
            }
        }
    }

    void Correct(const char* word, const std::string& complete, Word::List& result, unsigned corrections) const
    {
        if (corrections > 2)
            return;

        if (!*word)
        {
            if (m_IsTerminationNode)
                result.push_back(Word(complete, corrections));
            return;
        }

        for (const auto& pair : m_Childs)
        {
            // try each symbol
            {
                std::string temp(complete);
                temp.push_back(pair.first);
                pair.second.Correct(word + 1, temp, result, pair.first == *word ? corrections : corrections + 1);
            }
        }

        // try to insert each symbol after current
        const auto it = m_Childs.find(*word);
        if (it != m_Childs.end())
        {
            for (const auto& nestedPair : m_Childs)
            {
                std::string temp(complete);
                temp.push_back(*word);

                std::string wordCopy;
                wordCopy.push_back(nestedPair.first);
                wordCopy += (word + 1);

                it->second.Correct(wordCopy.c_str(), temp, result, corrections + 1);
            }
        }
    }

    std::list<std::string> Complete(const char* word) const
    {
        std::list<std::string> result;

        Word::List list;
        Complete(word, std::string(), 0, list);
        Correct(word, std::string(), list, 0);

        list.sort(boost::bind(&Word::m_Weight, _1) < boost::bind(&Word::m_Weight, _2));
        boost::transform(list, std::back_inserter(result), boost::bind(&Word::m_Value, _1));

        return result;
    }

    unsigned GetWeight() const
    {
        return m_Weight;
    }

private:
    unsigned m_Weight;
    bool m_IsTerminationNode;
    ChildMap m_Childs;
};


std::set<std::string> LoadDict(const boost::filesystem::path& dir)
{
    boost::filesystem::recursive_directory_iterator it(dir);
    const boost::filesystem::recursive_directory_iterator end;

    std::set<std::string> result;
    for (const auto& entry : boost::make_iterator_range(it, end))
    {
        boost::filesystem::ifstream ifs;
        //ifs.exceptions(std::ios::failbit | std::ios::badbit);
        ifs.open(entry.path(), std::ios::binary);

        std::list<std::string> strings;
        strings.assign(std::istream_iterator<std::string>(ifs), std::istream_iterator<std::string>());
        boost::copy(strings | boost::adaptors::filtered(boost::bind(&std::string::size, _1) > std::size_t(2)), std::inserter(result, result.end()));
    }
    return result;
}


TEST(Algorithm, Trie)
{
    Node root;

    //const auto list = LoadDict("final");
    const std::vector<std::string> list = boost::assign::list_of("aplet")("application")("another")("apple")("aplesdorf");
    for (const auto& text : list)
        root.Load(text.c_str());

    const auto results = root.Complete("aple");
    EXPECT_TRUE(boost::find(results, "apple") != results.end());
    EXPECT_TRUE(boost::find(results, "aplet") != results.end());
}