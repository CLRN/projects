﻿#include "common/FileSystem.h"
#include "conversion/AnyCast.h"

#include <map>
#include <string>
#include <list>
#include <memory>

#include <gtest/gtest.h>

#include <boost/algorithm/string/classification.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/xpressive/xpressive.hpp>

class IElement
{
public:
    virtual ~IElement() {}
    typedef boost::shared_ptr<IElement> Ptr;
};

class IOperand : public IElement
{
public:
    typedef boost::shared_ptr<IOperand> Ptr;

    virtual ~IOperand() {}

    virtual void Init(const std::string& expr) = 0;
    virtual double Evaluate() = 0;

    static Ptr Instance();
};

class Operand : public IOperand
{
    virtual void Init(const std::string& expr) override
    {
        m_Expression = expr;
    }

    virtual double Evaluate() override
    {
        return conv::cast<double>(m_Expression);
    }
private:
    std::string m_Expression;
};

IOperand::Ptr IOperand::Instance()
{
    return boost::make_shared<Operand>();
}

class IOperation : public IElement
{
public:
    typedef boost::shared_ptr<IOperation> Ptr;

    virtual ~IOperation() {}

    virtual int Priority() const = 0;
    virtual char GetSymbol() const = 0;

    virtual void SetLeft(const IOperand::Ptr& left) = 0;
    virtual void SetRight(const IOperand::Ptr& right) = 0;
    virtual double Evaluate() = 0;
};

class BaseOperation : public IOperation
{
public:
    virtual void SetLeft(const IOperand::Ptr& left) override
    {
        m_Left = left;
    }

    virtual void SetRight(const IOperand::Ptr& right) override
    {
        m_Right = right;
    }

protected:
    IOperand::Ptr m_Left;
    IOperand::Ptr m_Right;
};

class Plus : public BaseOperation
{
    virtual double Evaluate() override
    {
        return m_Left->Evaluate() + m_Right->Evaluate();
    }
    virtual char GetSymbol() const override
    {
        return '+';
    }
    virtual int Priority() const override
    {
        return 0;
    }
};
class Minus : public BaseOperation
{
    virtual double Evaluate() override
    {
        return m_Left->Evaluate() - m_Right->Evaluate();
    }
    virtual char GetSymbol() const override
    {
        return '-';
    }
    virtual int Priority() const override
    {
        return 0;
    }
};
class Multiply : public BaseOperation
{
    virtual double Evaluate() override
    {
        return m_Left->Evaluate() * m_Right->Evaluate();
    }
    virtual char GetSymbol() const override
    {
        return '*';
    }
    virtual int Priority() const override
    {
        return 1;
    }
};
class Division : public BaseOperation
{
    virtual double Evaluate() override
    {
        return m_Left->Evaluate() / m_Right->Evaluate();
    }
    virtual char GetSymbol() const override
    {
        return '/';
    }
    virtual int Priority() const override
    {
        return 1;
    }
};


class Calculator
{
    typedef std::map<char, boost::function<IOperation::Ptr()> > OperationMap;

    struct ParseContext
    {
        struct State
        {
            enum Value 
            {
                Operand = 0,
                Operator = 1
            };
        };
        ParseContext() : m_State() {}
        std::deque<IOperand::Ptr> m_Operands;
        std::deque<IOperation::Ptr> m_Operations;
        std::deque<IElement::Ptr> m_Elements;
        State::Value m_State;
    };

public:
    Calculator()
    {
        Register<Plus>();
        Register<Minus>();
        Register<Multiply>();
        Register<Division>();
    }

    const char* Parse(const char* data, ParseContext& ctx)
    {
        const char* current = data;

        while (*current)
        {
            // skip whitespace
            for (; *current == ' ' && *current; ++current) {}

            if (*current == '(')
            {
                // if open bracket found - calculate recursive
                ParseContext nested;
                current = Parse(current + 1, nested);
                const auto result = EvaluateContext(nested);
                const auto operand = IOperand::Instance();
                operand->Init(conv::cast<std::string>(result));
                ctx.m_Operands.push_back(operand);
                ctx.m_Elements.push_back(operand);
                ctx.m_State = ParseContext::State::Operator;
            }
            else
            if (*current == ')')
            {
                return current + 1;
            }
            else
            if (ctx.m_State == ParseContext::State::Operand)
            {
                current = ParseOperand(current, ctx);
                ctx.m_State = ParseContext::State::Operator;
            }
            else
            if (ctx.m_State == ParseContext::State::Operator)
            {
                const auto functor = m_Operations.find(*current);
                assert(functor != m_Operations.end());

                const auto operation = functor->second();
                ctx.m_Operations.push_back(operation);
                ctx.m_Elements.push_back(operation);
                ctx.m_State = ParseContext::State::Operand;
                ++current;
            }
        }
        return current;
    }

    double EvaluateContext(const ParseContext& ctx)
    {
        ParseContext copy(ctx);

        // filter elements, need operations only
        const auto range = copy.m_Elements | boost::adaptors::filtered([](const IElement::Ptr& e){
            return !!boost::dynamic_pointer_cast<IOperation>(e);
        });

        std::vector<IElement::Ptr> sorted(range.begin(), range.end());

        // sort operations by priority
        boost::sort(sorted, [](const IElement::Ptr& lhs, const IElement::Ptr& rhs){
            const auto left = boost::dynamic_pointer_cast<IOperation>(lhs);
            const auto right = boost::dynamic_pointer_cast<IOperation>(rhs);
            assert(left);
            assert(right);

            return left->Priority() > right->Priority();
        });

        // calculate operations using priority
        for (const auto& operation : sorted)
        {
            // find this operation in the list
            const auto it = boost::find(copy.m_Elements, operation);
            assert(it != copy.m_Elements.end());

            // obtain operation operands
            assert(it != copy.m_Elements.begin());
            const auto left = boost::dynamic_pointer_cast<IOperand>(*(it - 1));
            const auto right = boost::dynamic_pointer_cast<IOperand>(*(it + 1));

            // initialize and evaluate
            const auto op = boost::dynamic_pointer_cast<IOperation>(operation);
            op->SetLeft(left);
            op->SetRight(right);

            const auto result = op->Evaluate();
            
            // create new operand as operation calculation result
            const auto resultOperand = IOperand::Instance();
            resultOperand->Init(conv::cast<std::string>(result));

            // replace right operand with result
            *(it + 1) = resultOperand;

            // erase calculated
            copy.m_Elements.erase(it - 1, it + 1);
        }

        assert(copy.m_Elements.size() == 1);
        const auto operand = boost::dynamic_pointer_cast<IOperand>(copy.m_Elements.front());
        return operand->Evaluate();
    }

    const char* ParseOperand(const char* data, ParseContext& ctx)
    {
        assert(boost::algorithm::is_digit()(*data));
        const char* begin = data;

        // find NAN
        while (*data && (boost::algorithm::is_digit()(*data) || boost::algorithm::is_any_of(",.")(*data))) 
        {
            ++data;
        }

        const auto operand = IOperand::Instance();
        operand->Init(std::string(begin, data));
        ctx.m_Operands.push_back(operand);
        ctx.m_Elements.push_back(operand);
        return data;
    }

    double Evaluate(const std::string& expr)
    {
        ParseContext ctx;
        Parse(expr.c_str(), ctx);
        return EvaluateContext(ctx);
    }

private:

    void SkipWhitespace(std::string::const_iterator& it, const std::string::const_iterator& end)
    {
        for (; *it == ' ' && it != end; ++it) {}
    }

    template<typename T>
    void Register()
    {
        T operation;
        const IOperation& iface = operation;
        m_Operations.insert(std::make_pair(iface.GetSymbol(), [](){ return boost::make_shared<T>(); }));
    }

private:

    std::string m_Expression;
    OperationMap m_Operations;
};

TEST(Tasks, Calculator)
{
    const std::string difficult = "(1 + 2 * 7) / (1 - 2 / 3 + 5 / 7 + 1*2/3) - 7 / 5 + 2 / (5 - 7 * 3/2)";
    const std::string simple = "(1 + 2) / 3";

    Calculator calc;
    EXPECT_EQ(calc.Evaluate(simple), 1);
    EXPECT_EQ(calc.Evaluate(difficult), 6.9863636363636363);
}


TEST(Tasks, Bits)
{
    EXPECT_EQ(-1, 0xFFFFFFFF);
    EXPECT_EQ(-2, 0xFFFFFFFE);

    unsigned int v = 3132154; // count the number of bits set in v
    unsigned int c; // c accumulates the total bits set in v
    for (c = 0; v; c++)
    {
        v &= v - 1; // clear the least significant bit set
    }

    int a[] = { 3, 1, 4, 6, 1, 3 };
    auto begin = std::begin(a);
    auto end = std::end(a);

    std::vector<int> myvector (begin, end);

    // using default comparison:
    std::vector<int>::iterator it;
    it = std::unique (myvector.begin(), myvector.end());   // 10 20 30 20 10 ?  ?  ?  ?
    //                ^

    myvector.resize( std::distance(myvector.begin(),it) ); // 10 20 30 20 10


    end = std::unique(begin, end);
    std::copy(begin, end, std::ostream_iterator<int>(std::cout));
    std::cout << std::endl;
}

struct ListNode
{
    ListNode* m_Next;
    int m_Value;
};

ListNode* Reverse(ListNode* node, ListNode* prev)
{
    if (!node->m_Next)
    {
        node->m_Next = prev;
        return node;
    }
    
    ListNode* result = Reverse(node->m_Next, node);
    node->m_Next = prev;
    return result;
}

ListNode* Reverse2(ListNode* node)
{
    ListNode* current = node;
    ListNode* prev = nullptr;
    while (current)
    {
        ListNode* thisNode = current;
        current = thisNode->m_Next;
        thisNode->m_Next = prev;
        prev = thisNode;
    }

    return prev;
}

TEST(Tasks, ListReverse)
{
    {
        // make list
        std::vector<ListNode> data(10);
        std::list<int> etalon;
        for (unsigned i = 0; i < data.size(); ++i)
        {
            data[i].m_Value = i;
            data[i].m_Next = (i < data.size() - 1) ? &data[i + 1] : nullptr;
            etalon.push_front(i);
        }

        std::list<int> test;
        ListNode* current = Reverse(&data.front(), nullptr);
        for (; current; current = current->m_Next)
            test.push_back(current->m_Value);

        EXPECT_EQ(etalon, test);
    }
    {
        // make list
        std::vector<ListNode> data(10);
        std::list<int> etalon;
        for (unsigned i = 0; i < data.size(); ++i)
        {
            data[i].m_Value = i;
            data[i].m_Next = (i < data.size() - 1) ? &data[i + 1] : nullptr;
            etalon.push_front(i);
        }

        std::list<int> test;
        ListNode* current = Reverse2(&data.front());
        for (; current; current = current->m_Next)
            test.push_back(current->m_Value);

        EXPECT_EQ(etalon, test);
    }
}

typedef std::pair<int, int> Interval;
typedef std::vector<Interval> IntervalList;

struct Compare
{
    bool operator () (const Interval& lhs, const Interval& rhs) const
    {
        return lhs.second < rhs.first;
    }
};

bool IsIntersects(const Interval& lhs, const Interval& rhs)
{
    if (lhs.first <= rhs.first)
        return lhs.second >= rhs.first;
    else
        return rhs.second >= lhs.first;
}

Interval Min(const Interval& lhs, const Interval& rhs)
{
    if (lhs.first <= rhs.first)
        return std::make_pair(rhs.first, std::min(lhs.second, rhs.second));
    else
        return std::make_pair(lhs.first, std::min(lhs.second, rhs.second));
}

Interval GetMax(const IntervalList& list)
{
    struct IntervalAndCounter
    {
        typedef std::vector<IntervalAndCounter> List;
        IntervalAndCounter(const Interval& i, int c = 0) : m_Interval(i), m_Counter(c) {}
        IntervalAndCounter() : m_Interval(), m_Counter() {}
        int m_Counter;
        Interval m_Interval;
    };

    std::vector<IntervalAndCounter::List> counters(list.size());

    for (unsigned i = 0; i < list.size(); ++i)
    {
        for (unsigned j = 0; j < list.size(); ++j)
        {
            if (i == j)
                continue;

            if (IsIntersects(list[j], list[i])) 
            {
                const auto min = Min(list[j], list[i]);

                bool intersects = false;
                for (auto& interval : counters[i])
                {
                    if (IsIntersects(min, interval.m_Interval)) 
                    {
                        ++interval.m_Counter;
                        intersects = true;
                    }
                }

                if (!intersects)
                    counters[i].emplace_back(min, 1);
            }
        }
    }

    int maxCount = 0;
    Interval result;
    for (unsigned i = 0; i < list.size(); ++i)
    {
        for (unsigned j = 0; j < counters[i].size(); ++j)
        {
            if (counters[i][j].m_Counter > maxCount)
            {
                maxCount = counters[i][j].m_Counter;
                result = counters[i][j].m_Interval;
            }
        }
    }

    return result;
}

TEST(Tasks, Intervals)
{
    IntervalList intervals;
    intervals.push_back(std::make_pair(3, 4));
    intervals.push_back(std::make_pair(0, 1));
    intervals.push_back(std::make_pair(2, 4));
    intervals.push_back(std::make_pair(1, 5));
    intervals.push_back(std::make_pair(4, 5));
    intervals.push_back(std::make_pair(5, 6));
    intervals.push_back(std::make_pair(6, 7));
    intervals.push_back(std::make_pair(6, 8));

    const auto result = GetMax(intervals);
    EXPECT_EQ(result, std::make_pair(3, 4));
}

typedef std::map<boost::xpressive::regex_id_type, std::string> RegexpMap;
RegexpMap::value_type MakeRegexp(boost::xpressive::sregex_compiler& compiler, const std::string& name, const std::string& regexp)
{
    const auto result = compiler.compile(std::string("(?$ ") + name + std::string("  = ) ") + regexp, boost::xpressive::regex_constants::ignore_white_space);
    return std::make_pair(result.regex_id(), name);
}

boost::xpressive::sregex Compile(boost::xpressive::sregex_compiler& compiler, const RegexpMap& regexps)
{
    std::ostringstream oss;
    bool first = true;
    for (const auto& pair : regexps)
    {
        if (!first)
            oss << " | ";
        else
            first = false;

        oss << "(?$ " << pair.second << " )";
    }

    return compiler.compile(oss.str(), boost::xpressive::regex_constants::ignore_white_space);
}


void TestXpressive()
{
    RegexpMap regexps;
    boost::xpressive::sregex_compiler compiler;
    regexps.insert(MakeRegexp(compiler, "digits", "\\d+"));
    regexps.insert(MakeRegexp(compiler, "words", "\\D+"));
    const auto expr = Compile(compiler, regexps);

    std::string str("111 dsadsa d 321321");
    boost::xpressive::smatch what;

    if (regex_search(str, what, expr))
    {
        for (const auto match : what.nested_results())
        {
            std::cout << match[0] << " matched: " << regexps[match.regex_id()] << std::endl;
        }
    }
}

TEST(Tasks, Xpressive)
{
    TestXpressive();
}
