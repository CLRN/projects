#include <vector>

#include <gtest/gtest.h>

#include <boost/assign/list_of.hpp>

void Sort(std::vector<int>& data)
{
    if (data.size() <= 1)
        return;

    for (std::size_t i = 1; i < data.size(); ++i)
    {
        const int key = data[i];

        std::size_t j = i;
        for (; j != 0 && key < data[j - 1]; --j)
            data[j] = data[j - 1];

        data[j] = key;
    }
}


TEST(Algorithm, InsertSort)
{
    std::vector<int> sorted = boost::assign::list_of(0)(1)(2)(2)(3)(4)(5);

    std::vector<int> test1 = boost::assign::list_of(2)(3)(1)(5)(2)(0)(4);
    Sort(test1);
    EXPECT_EQ(test1, sorted);
}