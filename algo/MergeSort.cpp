#include "common/FileSystem.h"
#include "conversion/AnyCast.h"

#include <map>
#include <string>
#include <list>
#include <memory>

#include <gtest/gtest.h>

#include <boost/range/algorithm.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/make_shared.hpp>

template<typename T>
class Sorter
{
public:
    typedef boost::shared_ptr<std::iostream> IOStreamPtr;
    typedef boost::shared_ptr<std::istream> IStreamPtr;

    IStreamPtr Sort(std::istream& s, const std::size_t size)
    {
        const IStreamPtr temp(&s, [](std::istream*){});
        return MergeSort(temp, size);
    }

private:

    IStreamPtr MergeSort(const IStreamPtr& s, const std::size_t size)
    {
        // split stream to two sequences
        if (size <= 1)
            return s;

        // middle index
        const std::size_t middle = size / 2;

        // sort left stream
        const auto left = ReadAndSort(s, middle);

        // sort right stream
        const auto right = ReadAndSort(s, size - middle);

        // merge two streams
        return Merge(*left, *right);
    }

    IStreamPtr Merge(std::istream& lhs, std::istream& rhs)
    {
        const auto result = CreateTempStream();

        T left;
        T right;

        bool leftIsNext = true;
        bool rightIsNext = true;
        for (;;)
        {
            if (leftIsNext)
            {
                lhs.read(reinterpret_cast<char*>(&left), sizeof(T));
                assert(lhs.eof() || lhs.gcount() == sizeof(T));
            }

            if (rightIsNext)
            {
                rhs.read(reinterpret_cast<char*>(&right), sizeof(T));
                assert(rhs.eof() || rhs.gcount() == sizeof(T));
            }

            leftIsNext = !lhs.eof() && (left < right || rhs.eof());
            rightIsNext = !rhs.eof() && (right < left || lhs.eof());

            if (!leftIsNext && !rightIsNext && !lhs.eof() && !rhs.eof())
                leftIsNext = true; // equal values

            if (!leftIsNext && !rightIsNext)
                break;

            assert(leftIsNext ^ rightIsNext);

            // write smaller value
            const auto& value = leftIsNext ? left : right;

            result->write(reinterpret_cast<const char*>(&value), sizeof(T));
        }

        result->flush();
        result->seekp(0, std::ios::beg);
        return result;
    }

    IOStreamPtr CreateTempStream()
    {
        //return boost::make_shared<std::stringstream>();
        const auto guard = boost::make_shared<fs::ScopedFile>();
        const std::wstring& name = *guard;

        {
            std::ofstream test(conv::cast<std::string>(name).c_str());
            assert(test.is_open());
        }

        std::unique_ptr<boost::filesystem::fstream> stream(
            new boost::filesystem::fstream(name, std::ios::binary | std::ios::in | std::ios::out)
        );

        assert(stream->is_open());

        const auto result = IOStreamPtr(stream.release(), [guard](const std::iostream* s){
            delete s;
        });

        return result;
    }

    IStreamPtr ReadAndSort(const IStreamPtr& s, const std::size_t count)
    {
        T buffer[1] = {};
        auto out = CreateTempStream();

        for (std::size_t i = 0; i < count; ++i)
        {
            const auto toRead = _countof(buffer) * sizeof(T);

            s->read(reinterpret_cast<char*>(buffer), toRead);
            assert(s->gcount() == toRead);

            out->write(reinterpret_cast<const char*>(buffer), toRead);
        }

        out->flush();
        out->seekp(0, std::ios::beg);

        // sort stream
        return MergeSort(out, count);
    }

private:

};

void SimpleTest()
{
    Sorter<char> sorter;

    const std::string testData = "10230173216718739821";
    std::stringstream ss(testData);

    const auto result = sorter.Sort(ss, testData.size());
    std::copy(std::istream_iterator<char>(*result), std::istream_iterator<char>(), std::ostream_iterator<char>(std::cout));
}

void BigFileTest()
{
    {
        std::ofstream ofs("test.bin", std::ios::binary);

        for (unsigned i = 0; i < 10; ++i)
        {
            int value = rand() % 10000;
            ofs.write(reinterpret_cast<const char*>(&value), sizeof(value));
        }
    }

    std::ifstream ifs("test.bin", std::ios::binary);
    Sorter<int> sorter;
    const auto result = sorter.Sort(ifs, static_cast<std::size_t>(boost::filesystem::file_size("test.bin") / sizeof(int)));

    std::ofstream ofs("sorted.bin", std::ios::binary);
    for (;;)
    {
        int buffer = 0;
        result->read(reinterpret_cast<char*>(&buffer), sizeof(buffer));
        if (result->eof())
            break;
        ofs << buffer << std::endl;
    }
}

TEST(Algorithm, MergeSort)
{
    SimpleTest();
    BigFileTest();
}