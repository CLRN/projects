//
// ssl.hpp
// ~~~~~~~~
//


#ifndef URDL_SSL_HPP
#define URDL_SSL_HPP

#include <string>
#include "urdl/detail/config.hpp"
#include <boost/asio/ssl/context.hpp>
#include <boost/function.hpp>

#include "urdl/detail/abi_prefix.hpp"

namespace urdl {
    namespace ssl {

/// Option to specify verify_peer
class verify_peer
{
public:
  verify_peer()
    : value_(false)
  {
  }

  explicit verify_peer(bool v)
    : value_(v)
  {
  }

  bool value() const
  {
    return value_;
  }

  void value(bool v)
  {
    value_ = v;
  }

private:
  bool value_;
};

/// Option to specify verify_cert_against_host
class verify_cert_against_host
{
public:
    verify_cert_against_host()
        : value_(false)
    {
    }

    explicit verify_cert_against_host(bool v)
        : value_(v)
    {
    }

    bool value() const
    {
        return value_;
    }

    void value(bool v)
    {
        value_ = v;
    }

private:
    bool value_;
};

/// Option to specify verify_callback
class verify_callback
{
public:
    typedef boost::function<bool (bool, boost::asio::ssl::verify_context&)> cb_t;

    verify_callback()
    {
    }

    verify_callback(cb_t& v)
        : value_(v)
    {
    }

    cb_t value() const
    {
        return value_;
    }

    void value(cb_t& v)
    {
        value_ = v;
    }

private:
    cb_t value_;
};

 
} // namespace ssl
} // namespace urdl

#include "urdl/detail/abi_suffix.hpp"

#endif // URDL_SSL_HPP