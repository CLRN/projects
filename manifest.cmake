###############################################################################
# Third party libs.
###############################################################################

set(BOOST_SOURCE_PATH "http://downloads.sourceforge.net/project/boost/boost/1.56.0/boost_1_56_0.zip")
set(MONGO_SOURCE_PATH "https://github.com/mongodb/mongo-cxx-driver.git")
set(LOG4CPLUS_SOURCE_PATH "https://github.com/log4cplus/log4cplus.git")
set(GMOCK_SOURCE_PATH "http://googlemock.googlecode.com/svn/trunk/")
set(GTEST_SOURCE_PATH "http://googletest.googlecode.com/svn/trunk/")
set(PROTOBUF_SOURCE_PATH "http://protobuf.googlecode.com/svn/trunk/")
set(OPENCV_SOURCE_PATH "https://github.com/Itseez/opencv.git")
set(RABBITMQ_SOURCE_PATH "https://github.com/alanxz/rabbitmq-c.git")
set(LEVELDB_SOURCE_PATH "https://code.google.com/p/leveldb/")
set(ZLIB_SOURCE_PATH "https://github.com/madler/zlib.git")
set(BZIP2_SOURCE_PATH "http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz")
set(CRASHRPT_SOURCE_PATH "http://crashrpt.googlecode.com/svn/trunk/")
set(POSTGREE_SOURCE_PATH "http://get.enterprisedb.com/postgresql/postgresql-9.3.5-1-windows-binaries.zip")
set(LIBBSON_SOURCE_PATH "https://github.com/mongodb/libbson.git")
set(CASABLANCA_SOURCE_PATH "https://git01.codeplex.com/casablanca")

# ODB ORM
set(ODB_COMPILER_SOURCE_PATH "http://www.codesynthesis.com/download/odb/2.3/odb-2.3.0-i686-windows.zip")
set(ODB_BOOST_SOURCE_PATH "http://scm.codesynthesis.com/odb/libodb-boost.git")
set(ODB_COMMON_SOURCE_PATH "http://scm.codesynthesis.com/odb/libodb.git")
set(ODB_MSSQL_SOURCE_PATH "http://scm.codesynthesis.com/odb/libodb-mssql.git")
set(ODB_PGSQL_SOURCE_PATH "http://scm.codesynthesis.com/odb/libodb-pgsql.git")

set(THIRD_PARTY_SOURCE_PATH "https://bitbucket.org/CLRN/projects/downloads/third_party.0.0.3.tar.gz")

