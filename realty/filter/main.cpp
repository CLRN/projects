#include "common/Log.h"
#include "common/Modules.h"
#include "inet/RemoteFile.h"
#include "conversion/AnyCast.h"
#include "common/FileSystem.h"

#include "exception/CheckHelpers.h"

#include <boost/program_options.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/format.hpp>
#include <boost/scope_exit.hpp>

namespace po = boost::program_options;

DECLARE_CURRENT_MODULE(Modules::Tools);
Log logger;

std::string DownloadFile(const std::string& url)
{
    // download page
    const auto file = net::IRemoteFile::Instance(
        logger, 
        L"download", 
        conv::cast<std::wstring>(url)
    );

    std::string content;
    {
        const std::wstring fileName = file->Download();
        fs::ScopedFile guard(fileName);

        boost::filesystem::ifstream ifs(fileName, std::ios::binary);
        ifs >> std::noskipws;
        std::copy(std::istream_iterator<char>(ifs), std::istream_iterator<char>(), std::back_inserter(content));
    }
    return content;
}

std::string Utf8UrlEncode(const std::string& input)
{
    std::string output;
    for (const char *p = input.c_str(); *p; *p++) 
    {
        char onehex[5];
        _snprintf(onehex, sizeof(onehex), "%%%02.2X", (unsigned char)*p);
        output.append(onehex);
    }
    return output;
}

std::map<std::wstring, std::wstring> g_Cache;

std::wstring GetCoordinates(const std::wstring& address, const std::string mapsApi, const std::string& mapsRegexp)
{
    const auto it = g_Cache.find(address);
    if (it != g_Cache.end())
        return it->second;

    const std::string encoded = Utf8UrlEncode(conv::cast<std::string>(address));
    const std::string api = (boost::format(mapsApi) % encoded).str();
    const std::string content = DownloadFile(api);

    static const boost::regex regexp(mapsRegexp);
    boost::smatch match;
    boost::regex_search(content, match, regexp);

    std::string resultText;
    for (std::size_t i = 1; i < match.size(); ++i)
        resultText += match[i];

    const auto result = conv::cast<std::wstring>(resultText);
    g_Cache.insert(std::make_pair(address, result));
    return result;
}

void LoadCache()
{
    boost::filesystem::ifstream ifs("address_cache.txt", std::ios::binary);
    ifs >> std::noskipws;

    std::string raw;
    std::copy(std::istream_iterator<char>(ifs), std::istream_iterator<char>(), std::back_inserter(raw));

    std::vector<std::string> content;
    boost::algorithm::split(content, raw, boost::algorithm::is_any_of("\n"));

    for (const auto& line : content)
    {
        if (line.empty())
            continue;

        std::vector<std::string> parts;
        boost::algorithm::split(parts, line, boost::algorithm::is_any_of("="));
        assert(parts.size() == 2);

        g_Cache.insert(std::make_pair(conv::cast<std::wstring>(parts.front()), conv::cast<std::wstring>(parts.back())));
    }
}

void SaveCache()
{
    boost::filesystem::ofstream ofs("address_cache.txt", std::ios::binary);

    for (const auto& pair : g_Cache)
        ofs << conv::cast<std::string>(pair.first) << "=" << conv::cast<std::string>(pair.second) << std::endl;
}

int main(int argc, char* argv[])
{
	try 
	{
        LoadCache();

        BOOST_SCOPE_EXIT(argc)
        {
            SaveCache();
        }
        BOOST_SCOPE_EXIT_END;

        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("url", po::value<std::string>(), "web page url")
            ("regexp", po::value<std::string>(), "regular expression")
            ("filter", po::value<std::string>(), "filter format string")
            ("out", po::value<std::string>(), "output format string")
            ("maps-api", po::value<std::string>(), "maps API")
            ("maps-regexp", po::value<std::string>(), "maps parse regexp");

        po::positional_options_description p;

        po::variables_map vm;

        po::store(po::parse_config_file<char>("filter.cfg", desc), vm);
        po::notify(vm);

        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

		if (vm.count("help")) 
		{
			std::cout << desc << std::endl;
			return 1;
		}

        const std::string mapsApi = vm["maps-api"].as<std::string>();
        const std::string mapsRegexp = vm["maps-regexp"].as<std::string>();

        // read file content
        std::vector<std::wstring> adresses;
        {
            std::string content;
            std::cin >> std::noskipws;
            std::copy(std::istream_iterator<char>(std::cin), std::istream_iterator<char>(), std::back_inserter(content));

            boost::algorithm::split(adresses, conv::cast<std::wstring>(content), boost::algorithm::is_any_of("\n"));

            boost::transform(adresses, adresses.begin(), [&mapsApi, &mapsRegexp](std::wstring address){
                boost::algorithm::trim_all(address);
                return GetCoordinates(address, mapsApi, mapsRegexp);
            });
        }

        const std::string regexpText = vm["regexp"].as<std::string>();
        const std::string filter = vm["filter"].as<std::string>();
        const std::string out = vm["out"].as<std::string>();

        std::vector<std::wstring> lastCollected;
        std::vector<std::wstring> outPrinted;
        for (int i = 1; ; ++i)
        {
            const std::string url = (boost::format(vm["url"].as<std::string>()) % i).str();
            const std::string content = DownloadFile(url);

            // parse
            std::vector<std::wstring> localCollected;
            boost::regex regexp(regexpText);
            boost::sregex_iterator it(content.begin(), content.end(), regexp);
            const boost::sregex_iterator end;
            for (; it != end; ++it)
            {
                const std::string match = (*it)[0];
                const std::wstring filterText = conv::cast<std::wstring, conv::Ansi>(boost::regex_replace(match, regexp, filter));
                localCollected.push_back(filterText);

                const std::wstring outText = conv::cast<std::wstring, conv::Ansi>(boost::regex_replace(match, regexp, out));

                if (boost::find(outPrinted, outText) == outPrinted.end())
                {
                    const std::wstring coordinates = GetCoordinates(filterText, mapsApi, mapsRegexp);

                    if (boost::find(adresses, coordinates) != adresses.end())
                        std::cout << conv::cast<std::string>(outText) << std::endl;
                }
                outPrinted.push_back(outText);
            }

            if (lastCollected == localCollected)
                break;

            lastCollected.swap(localCollected);
        }
	}
	catch(const std::exception& e)
    {
        LERROR(logger, CURRENT_MODULE_ID, "%s") % cmn::ExceptionInfo(e);
    }

	return 0;
}

