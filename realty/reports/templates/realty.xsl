﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
    <h2 style="font-size: 20px;text-align:center">Search Results</h2>
    <table border="0" cellspacing="2" cellpadding="2" style="font-size: 20px">
      <tr bgcolor="#9acd32">
        <th style="text-align:center">Type</th>
        <th style="text-align:center">Price m&#178;</th>
        <th style="text-align:center">Price</th>
        <th style="text-align:center">Address</th>
        <th style="text-align:center">Full</th>
        <th style="text-align:center">Rooms</th>
        <th style="text-align:center">Kitchen</th>
        <th style="text-align:center">Balcony</th>
        <th style="text-align:center">Link</th>
      </tr>
      
      <xsl:for-each select="data/flat">
        <xsl:sort select="concat(document(current())//*[@class='object_descr_td_l']/*[7], document(current())//*[@class='object_descr_price'])"/>
        <xsl:variable name="link" select="current()"/>
        <tr>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_title']"/>
            </td>              
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_td_l']/*[7]"/>
            </td>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_price']"/>
            </td>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_addr']"/>
            </td>           
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_props']/*[5]/*[2]"/>
            </td>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_props']/*[6]/*[2]"/>
            </td>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_props']/*[8]/*[2]"/>
            </td>
            <td>
                <xsl:value-of select="document($link)//*[@class='object_descr_props']/*[10]/*[2]"/>
            </td>            
            <td>
                <a href="{$link}">
                    <xsl:value-of select="current()"/>
                </a>
            </td>
        </tr>  
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>