@echo off

set binary_dir=%CD%
set output_directory=%CD%\reports_tmp
set saxon=%binary_dir%\saxon\transform
set template=%binary_dir%\templates\realty.xsl

rmdir /s /q %output_directory% > nul
mkdir %output_directory% > nul

:: echo dumping data...
echo ^<data^> > %output_directory%\out.xml
"%binary_dir%\dump" | "%binary_dir%\filter" >> %output_directory%\out.xml
echo ^</data^> >> %output_directory%\out.xml
if %errorlevel% NEQ 0 (
    echo failed to dump clients!
    exit %errorlevel%
)
:: echo copying templates...
copy "%template%" "%output_directory%\template.xsl" > nul

pushd %output_directory%

:: echo running saxon xsl transformer...
"%saxon%" -s:"%output_directory%\out.xml" -xsl:"%output_directory%\template.xsl"
popd