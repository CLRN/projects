#include "common/Log.h"
#include "common/Modules.h"
#include "inet/RemoteFile.h"
#include "conversion/AnyCast.h"
#include "common/FileSystem.h"

#include "exception/CheckHelpers.h"

#include <boost/program_options.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/program_options/parsers.hpp>

namespace po = boost::program_options;

DECLARE_CURRENT_MODULE(Modules::Tools);

int main(int argc, char* argv[])
{
	Log logger;

	try 
	{
        po::options_description desc("allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("url", po::value<std::string>(), "web page url")
            ("regexp", po::value<std::string>(), "regular expression");

        po::positional_options_description p;

        po::variables_map vm;

        po::store(po::parse_config_file<char>("dump.cfg", desc), vm);
        po::notify(vm);

        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

		if (vm.count("help")) 
		{
			std::cout << desc << std::endl;
			return 1;
		}

        const std::string regexpText = vm["regexp"].as<std::string>();

        std::vector<std::string> last;
        for (int i = 1; ; ++i)
        {
            const std::string url = (boost::format(vm["url"].as<std::string>()) % i).str();

            // download page
            const auto file = net::IRemoteFile::Instance(
                logger, 
                L"download", 
                conv::cast<std::wstring>(url)
            );

            std::string content;
            {
                const std::wstring fileName = file->Download();
                fs::ScopedFile guard(fileName);

                boost::filesystem::ifstream ifs(fileName, std::ios::binary);
                ifs >> std::noskipws;

                // read file content
                std::copy(std::istream_iterator<char>(ifs), std::istream_iterator<char>(), std::back_inserter(content));
            }

            // parse
            boost::regex regexp(regexpText);
            boost::sregex_iterator it(content.begin(), content.end(), regexp);
            const boost::sregex_iterator end;
            std::vector<std::string> current;
            for (; it != end; ++it)
            {
                std::string address = (*it)[1];
                boost::algorithm::trim_all(address);
                current.push_back(address);

                std::cout << conv::cast<std::string, conv::Ansi>(address) << std::endl;
            }

            if (current == last)
                break;

            last = current;
        }
	}
	catch(const std::exception& e)
    {
        LERROR(logger, CURRENT_MODULE_ID, "%s") % cmn::ExceptionInfo(e);
    }

	return 0;
}

